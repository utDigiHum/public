om	on	ole	0		V	b
nink	ning	ning	0		J	
temma	tema	tema	0		P	sg g
     	tema	tema	0		P	sg n
se	see	see	0		P	sg n
pääle	peale	peale	0		K	
     	peale	peale	0		D	
sis	siis	siis	0		D	
   	siis	siis	0		J	
ärra	ära	ära	0		D	
    	ära	ära	0		V	neg o
olle	ole	ole	0		V	o
päle	peale	peale	0		K	
    	peale	peale	0		D	
mes	mis	mis	0		P	pl n
   	mis	mis	0		P	sg n
ärä	ära	ära	0		D	
   	ära	ära	0		V	neg o
omma	oma	oma	0		P	sg g
    	on	ole	0		V	vad
ollevad	on	ole	vad		V	vad
olli	oli	ole	i		V	s
kül	küll	küll	0		D	
massa	maksa	maks	0		V	o
     	maksta	maks	a		V	da
ülles	üles	üles	0		D	
nenda	nõnda	nõnda	0		D	
ütel	ütles	ütle	0		V	s
ollu	olnud	ole	lu		V	nud
Jürri	Jüri	Jürri	0		H	sg n
annud	andnud	and	nud		V	nud
moisa	mõisa	mõis	0		S	sg g
rahha	raha	raha	0		S	sg g
     	raha	raha	0		S	sg p
pääl	peal	peal	0		K	
sedda	seda	see	da		P	sg p
üts	üks	üks	0		N	sg n
   	üks	üks	0		P	sg n
keik	kõik	kõik	0		P	pl n
    	kõik	kõik	0		P	sg n
perast	pärast	pärast	0		K	
      	pärast	pärast	0		D	
masma	maksma	maks	ma		V	ma
perra	perra	perra	0		D	
     	perra	perra	0		K	
ni	nii	nii	0		D	
lännu	läinud	mine	nu		V	nud
sääl	seal	seal	0		D	
päiva	päeva	päev	0		S	sg p
     	päeva	päev	0		S	sg g
kaibas	kaebas	kaeba	s		V	s
sanu	saanud	saa	nu		V	nud
olna	olevat	ole	na		V	vat
naene	naine	naine	0		S	sg n
est	eest	eest	0		K	
ollo	olnud	ole	lo		V	nud
kõrd	kord	kord	0		D	
ollev	olevat	ole	v		V	vat
perrast	pärast	pärast	0		K	
       	pärast	pärast	0		D	
een	ees	ees	0		K	
   	ees	ees	0		D	
tennu	teinud	tege	nu		V	nud
üttelnu	ütelnud	ütle	nu		V	nud
massma	maksma	maks	ma		V	ma
kogokonna	kogukonna	kogukond	0		S	sg g
nema	nemad	tema	0		P	pl n
päält	pealt	pealt	0		K	
     	pealt	pealt	0		D	
voi	või	või	0		D	
   	või	või	0		J	
   	või	või	0		S	sg n
   	või	või	0		S	sg g
   	või	või	0		V	o
essi	ise	ise	0		P	pl n
    	ise	ise	0		P	sg n
moistetu	mõistetud	mõist	tu		V	tud
enamb	enam	enam	0		D	
masnu	maksnud	maks	nu		V	nud
nännu	näinud	näge	nu		V	nud
kaebatu	kaevatud	kaeba	tu		V	tud
tälle	talle	tema	le		P	sg all
temä	tema	tema	0		P	sg n
moistis	mõistis	mõist	is		V	s
kaivas	kaebas	kaeba	s		V	s
moistus	mõistus	mõistus	0		S	sg n
nee	need	see	0		P	pl n
herra	härra	härra	0		S	sg n
henda	enda	ise	0		P	sg g
tullu	tulnud	tule	lu		V	nud
ärrä	ära	ära	0		D	
    	ära	ära	0		V	neg o
aanud	ajanud	aja	nud		V	nud
aavlid	haavlid	haavel	d		S	pl n
aea	aja	aeg	0		S	sg g
   	aia	aed	0		S	sg g
aeast	ajast	aeg	st		S	sg el
     	aiast	aed	st		S	sg el
aeades	ajades	aja	des		V	des
aeanud	ajanud	aja	nud		V	nud
ajanu	ajanud	aja	nu		V	nud
appellatsiooni	apellatsiooni	apellatsioon	0		S	sg g
ara	ära	ära	0		D	
   	ära	ära	0		V	neg o
arra	ära	ära	0		D	
    	ära	ära	0		V	neg o
arvanu	arvanud	arva	nu		V	nud
avitanu	aidanud	avita	nu		V	nud
ehitanu	ehitanud	ehita	nu		V	nud
eila	eile	eile	0		D	
elumaia	elumaja	elu_maja	0		S	sg g
elumaias	elumajas	elu_maja	s		S	sg in
elumaiast	elumajast	elu_maja	st		S	sg el
enni	enne	enne	0		K	
    	enne	enne	0		D	
esti	esiti	esiti	0		D	
hoidnu	hoidnud	hoid	nu		V	nud
hommen	homme	homme	0		D	
hära	härra	härra	0		S	sg n
jaanipaeva	jaanipäeva	jaani_päev	0		S	sg p
jaanu	jäänud	jää	nu		V	nud
jooksnu	jooksnud	jooks	nu		V	nud
joonu	joonud	joo	nu		V	nud
joua	jõua	jõud	0		V	o
julenu	julgenud	julge	nu		V	nud
julgenu	julgenud	julge	nu		V	nud
jäevad	jäävad	jää	vad		V	sid
jätnu	jätnud	jät	nu		V	nud
jäänu	jäänud	jää	nu		V	nud
kaddunu	kadunud	kadu	nu		V	nud
kadunu	kadunud	kadu	nu		V	nud
kaebanud	kaevanud	kaeba	nud		V	nud
kaebatud	kaevatud	kaeba	tud		V	tud
kaebduses	kaebuses	kaebus	s		S	sg in
kaenu	kaenud	kae	nu		V	nud
kaes	käes	käsi	s		S	sg in
    	käes	käes	0		K	
kaest	käest	käsi	st		S	sg el
     	käest	käest	0		K	
kaotanu	kaotanud	kaota	nu		V	nud
karistud	karistatud	karista	tud		V	tud
kasvatanu	kasvatanud	kasvata	nu		V	nud
katte	kätte	käsi	tte		S	adt
     	kätte	kätte	0		K	
     	kätte	kätte	0		D	
kaubelnuvad	kaubelnud	kauple	nuvad		V	nuvad
kaubelnu	kaubelnud	kauple	nu		V	nud
kerikud	kirikut	kirik	d		S	sg p
       	kirikud	kirik	d		S	pl n
kervega	kirvega	kirves	ga		S	sg kom
kevadi	kevade	kevad	0		S	sg g
kinnitanu	kinnitanud	kinnita	nu		V	nud
kirjutanu	kirjutanud	kirjuta	nu		V	nud
kiskunu	kiskunud	kisku	nu		V	nud
kohtumaeas	kohtumajas	kohtu_maja	s		S	sg in
kohtumaeasse	kohtumajasse	kohtu_maja	sse		S	sg ill
koolimaias	koolimajas	kooli_maja	s		S	sg in
korsnad	korstnad	korsten	d		S	pl n
kropp	ropp	ropp	0		A	sg n
kudagi	kuidagi	kuidagi	0		D	
kuida	kuidas	kuidas	0		D	
kukkunu	kukkunud	kukku	nu		V	nud
kutsonud	kutsunud	kutsu	nud		V	nud
kutsunu	kutsunud	kutsu	nu		V	nud
kuulnu	kuulnud	kuul	nu		V	nud
kõnelnu	kõnelnud	kõnele	nu		V	nud
käinu	käinud	käi	nu		V	nud
käsnu	käskinud	käski	nu		V	nud
külles	küljes	küljes	0		D	
      	küljes	küljes	0		K	
      	küljes	külg	s		S	sg in
kündnu	kündnud	künd	nu		V	nud
küsinu	küsinud	küsi	nu		V	nud
laadel	laadal	laat	l		S	sg ad
laenanu	laenanud	laena	nu		V	nud
laimaia	laimaja	laimaja	0		S	sg g
lasknu	lasknud	lask	nu		V	nud
leidnu	leidnud	leid	nu		V	nud
litseks	litsiks	lits	ks		S	sg tr
lubanu	lubanud	luba	nu		V	nud
lubbanu	lubanud	luba	nu		V	nud
lugenu	lugenud	luge	nu		V	nud
lõhkunu	lõhkunud	lõhku	nu		V	nud
lõiganu	lõiganud	lõika	nu		V	nud
löönu	löönud	löö	nu		V	nud
löönuvad	löönud	löö	nuvad		V	nuvad
maeaomaniku	majaomaniku	maja_omanik	0		S	sg g
maeasse	majasse	maja	sse		S	sg ill
maeast	majast	maja	st		S	sg el
maganu	maganud	maga	nu		V	nud
maiade	majade	maja	de		S	pl g
maiadesse	majadesse	maja	desse		S	pl ill
maiad	majad	maja	d		S	pl n
maiaga	majaga	maja	ga		S	sg kom
maial	majal	maja	l		S	sg ad
maia	maja	maja	0		S	sg g
    	maja	maja	0		S	sg n
    	maja	maja	0		S	sg p
maiase	majasse	maja	sse		S	sg ill
maias	majas	maja	s		S	sg in
maiasse	majasse	maja	sse		S	sg ill
maiast	majast	maja	st		S	sg el
maksnu	maksnud	maks	nu		V	nud
moisale	mõisale	mõis	le		S	sg all
Moisa	mõisa	mõis	0		S	sg g
     	mõisa	mõis	0		S	adt
     	mõisa	mõis	0		S	sg p
moisapolitsei	mõisapolitsei	mõisa_politsei	0		S	sg g
moisas	mõisas	mõis	s		S	sg in
moisavalitseja	mõisavalitseja	mõisa_valitseja	0		S	sg g
moisavalitsuse	mõisavalitsuse	mõisa_valitsus	0		S	sg g
moistab	mõistab	mõist	b		V	b
moistetud	mõistetud	mõist	tud		V	tud
moistnud	mõistnud	mõist	nud		V	nud
moistusele	mõistusele	mõistus	le		S	sg all
muiale	mujale	mujale	0		D	
murdnu	murdnud	murd	nu		V	nud
mõistnu	mõistnud	mõist	nu		V	nud
mõõtnu	mõõtnud	mõõt	nu		V	nud
müija	müüja	müüja	0		S	sg n
müinud	müünud	müü	nud		V	nud
müinu	müünud	müü	nu		V	nud
müünu	müünud	müü	nu		V	nud
Naad	nad	tema	d		P	pl n
naesega	naisega	naine	ga		S	sg kom
naesele	naisele	naine	le		S	sg all
naese	naise	naine	0		S	sg g
naeste	naiste	naine	te		S	pl g
naest	naist	naine	t		S	sg p
nakanu	nakanud	nakka	nu		V	nud
neljapaev	neljapäev	nelja_päev	0		S	sg n
niitnud	niitnud	niit	nu		V	nud
nimmelt	nimelt	nimelt	0		D	
nõudnu	nõudnud	nõud	nu		V	nud
näidanu	näidanud	näita	nu		V	nud
odre	odrad	oder	e		S	pl p
olnu	olnud	ole	nu		V	nud
oppuse	õppuse	õppus	0		S	sg g
ostnod	ostnud	ost	nod		V	nud
ostnu	ostnud	ost	nu		V	nud
otsinu	otsinud	otsi	nu		V	nud
paevad	päevad	päev	d		S	pl n
paevaks	päevaks	päev	ks		S	sg tr
paeval	päeval	päev	l		S	sg ad
paeva	päeva	päev	0		S	sg g
     	päeva	päev	0		S	sg p
paevas	päevas	päev	s		S	sg in
paevi	päevi	päev	i		S	pl p
paev	päev	päev	0		S	sg n
pakkonod	pakkunud	pakku	nod		V	nud
pakkunu	pakkunud	pakku	nu		V	nud
palanu	palanud	pala	nu		V	nud
palgide	palkide	palk	de		S	pl g
palitod	palitut	palitu	d		S	sg p
pallelnu	palunud	palu	nu		V	nud
pannu	pannud	pane	nu		V	nud
parandanu	parandanud	paranda	nu		V	nud
peksetud	pekstud	peks	tud		V	tud
peksnu	peksnud	peks	nu		V	nud
peksnuvad	peksnud	peks	nuvad		V	nuvad
pesnu	peksnud	peks	nu		V	nud
pesnuvad	peksnud	peks	nuvad		V	nuvad
petnu	petnud	pet	nu		V	nud
piddanu	pidanud	pida	nu		V	nud
pidanu	pidanud	pida	nu		V	nud
pitk	pikk	pikk	0		A	sg n
poea	poja	poeg	0		S	sg g
    	poja	poja	0		S	sg g
püidmas	püüdmas	püüd	mas		V	mas
rechnungid	rehnungid	rehnung	d		S	pl n
          	rehnungit	rehnung	d		S	sg p
rechnungi	rehnungi	rehnung	0		S	sg g
         	rehnungit	rehnung	0		S	sg p
rechnung	rehnung	rehnung	0		S	sg n
reedisel	reedesel	reedene	l		A	sg ad
riielnuvad	riielnud	riidle	nuvad		V	nuvad
saanu	saanud	saa	nu		V	nud
saapaste	saabaste	saabas	te		S	pl g
saatnu	saatnud	saat	nu		V	nud
sadanu	sadanud	sada	nu		V	nud
saivad	said	saa	ivad		V	sid
salanu	salanud	salga	nu		V	nud
Seesinnane	seesinane	see_sinane	0		P	sg n
seisnu	seisnud	seis	nu		V	nud
seletanu	seletanud	seleta	nu		V	nud
Sesinane	seesinane	see_sinane	0		P	sg n
sõimanu	sõimanud	sõima	nu		V	nud
sõimelnuvad	sõimelnud	sõimle	nuvad		V	nuvad
sõitnu	sõitnud	sõit	nu		V	nud
söönu	söönud	söö	nu		V	nud
söötnu	söötnud	sööt	nu		V	nud
süidlane	süüdlane	süüdlane	0		S	sg n
süidlaseks	süüdlaseks	süüdlane	ks		S	sg tr
sündinu	sündinud	sündi	nu		V	nud
tahtab	tahab	taht	b		V	b
tahtnu	tahtnud	taht	nu		V	nud
tallitanud	talitanud	talita	nud		V	nud
tallitanu	talitanud	talita	nu		V	nud
taloperemees	taluperemees	talu_pere_mees	0		S	sg n
tapelnuvad	tapelnud	taple	nuvad		V	nuvad
teatre	teatri	teater	0		S	sg g
teene	teine	teine	0		O	sg n
     	teine	teine	0		P	sg n
teesed	teised	teine	d		O	pl n
      	teised	teine	d		P	pl n
teeseks	teiseks	teine	ks		O	sg tr
       	teiseks	teine	ks		P	sg tr
tegia	tegija	tegija	0		S	sg g
tehnud	teinud	tege	nud		V	nud
tembeldanu	tembeldanud	tembelda	nu		V	nud
teotanu	teotanud	teota	nu		V	nud
toonu	toonud	too	nu		V	nud
toovri	toobri	toober	0		S	sg g
tulli	tuli	tule	i		V	s
     	tuli	tuli	0		S	sg n
tulnuvad	tulnud	tule	nuvad		V	nuvad
tundnu	tundnud	tund	nu		V	nud
tunnistanu	tunnistanud	tunnista	nu		V	nud
tunnud	tundnud	tund	nud		V	nud
tõmmanu	tõmmanud	tõmba	nu		V	nud
tõrelnu	tõrelnud	tõrele	nu		V	nud
tõstnu	tõstnud	tõst	nu		V	nud
tõuganu	tõuganud	tõuka	nu		V	nud
tõusnu	tõusnud	tõus	nu		V	nud
täitnu	täitnud	täit	nu		V	nud
uherdiga	oherdiga	oherdi	ga		S	sg kom
vabatiko	vabadiku	vabadik	0		S	sg g
vabatik	vabadik	vabadik	0		S	sg n
vahetanu	vahetanud	vaheta	nu		V	nud
varastanu	varastanud	varasta	nu		V	nud
varrastanu	varastanud	varasta	nu		V	nud
vastanu	vastanud	vasta	nu		V	nud
vedanu	vedanud	veda	nu		V	nud
veddanu	vedanud	veda	nu		V	nud
viinu	viinud	vii	nu		V	nud
vinu	viinud	vii	nu		V	nud
voera	võõra	võõras	0		A	sg g
     	võõra	võõras	0		S	sg n
voib	võib	või	b		V	b
voida	võida	võit	0		V	o
voimalik	võimalik	võimalik	0		A	sg n
voinud	võinud	või	nud		V	nud
võinu	võinud	või	nu		V	nud
võtnu	võtnud	võt	nu		V	nud
ähvardanu	ähvardanud	ähvarda	nu		V	nud
ärkitanud	ärgitanud	ärgita=nu	d		S	pl n
öölda	öelda	ütle	da		V	da
ööldes	öeldes	ütle	des		V	des
ööldut	öeldud	ütle	dut		V	tud
üleültse	üleüldse	üle_üldse	0		D	
ütelnu	ütelnud	ütle	nu		V	nud
üttelnu	ütelnud	ütle	nu		V	nud
poig	poeg	poeg	0		S	sg n
võlgo	võlgu	võlgu	0		D	
     	võlgu	võlg	u		S	pl p
tallo	tallu	talu	0		S	sg adt
     	talu	talu	0		S	sg n
     	talu	talu	0		S	sg g
     	talu	talu	0		S	sg p
moisteti	mõisteti	mõist	ti		V	ti
massab	maksab	maks	b		V	b
kohtomees	kohtumees	kohtu_mees	0		S	sg n
heino	Heino	Heino	0		H	sg n
moistnu	mõistnud	mõist	nu		V	nud
hobese	hobuse	hobune	0		S	sg g
pandnu	pannud	pane	nu		V	nud
mõtsast	metsast	mets	st		S	sg el
käen	käes	käsi	s		S	sg in
    	käes	käes	0		K	
    	käes	käes	0		D	
mõtsa	metsa	mets	0		S	sg g
     	metsa	mets	0		S	adt
     	metsa	mets	0		S	sg p
wälja	välja	väli	0		S	adt
     	välja	väli	0		S	sg g
     	välja	väli	0		S	sg p
     	välja	välja	0		D	
     	välja	välja	0		V	o
walla	valla	vald	0		S	sg g
     	valla	valla	0		D	
agga	aga	aga	0		J	
kohto	kohtu	kohtu	0		V	o
     	kohtu	kohus	0		S	sg g
Kohto	kohtu	kohtu	0		V	o
     	kohtu	kohus	0		S	sg g
weel	veel	veel	0		D	
    	veel	vesi	l		S	sg ad
tedda	teda	tema	da		P	sg p
pärrast	pärast	pära	st		S	sg el
       	pärast	pärane	t		A	sg p
       	pärast	pärast	0		D	
       	pärast	pärast	0		K	
trahwi	trahvi	trahv	0		S	adt
      	trahvi	trahv	0		S	sg p
      	trahvi	trahv	0		S	sg g
      	trahvi	trahvi	0		V	o
temmale	temale	tema	le		P	sg all
wõtnud	võtnud	võt	nud		V	nud
wakka	vakka	vakk	0		S	adt
     	vakka	vakk	0		S	sg p
     	vakka	vakka	0		D	
Kohhus	kohus	kohu	s		V	s
      	kohus	kohus	0		S	sg n
waid	vaid	vaid	0		D	
    	vaid	vaid	0		J	
kohhus	kohus	kohu	s		V	s
      	kohus	kohus	0		S	sg n
temmal	temal	tema	l		P	sg ad
wastu	vastu	vastu	0		D	
     	vastu	vastu	0		K	
nemmad	nemad	tema	d		P	pl n
sanud	saanud	saa	nud		V	nud
taggasi	tagasi	tagasi	0		D	
       	tagasi	tagasi	0		K	
ülle	üle	üle	0		D	
    	üle	üle	0		K	
Kirjotaja	kirjutaja	kirjutaja	0		S	sg n
         	kirjutaja	kirjutaja	0		S	sg g
mahha	maha	maha	0		D	
wasto	vastu	vastu	0		D	
     	vastu	vastu	0		K	
polle	pole	ole	0		V	neg o
kuulutud	kuulutatud	kuuluta	tud		V	tud
wanna	vana	vana	0		A	sg n
     	vana	vana	0		A	sg g
     	vana	vana	0		A	sg p
     	vana	vana	0		S	sg n
     	vana	vana	0		S	sg g
     	vana	vana	0		S	sg p
wõtta	võta	võt	0		V	o
     	võtta	võt	a		V	da
ühhe	ühe	ühe	0		S	sg n
    	ühe	üks	0		N	sg g
    	ühe	üks	0		P	sg g
pääwa	päeva	päev	0		S	adt
     	päeva	päev	0		S	sg g
     	päeva	päev	0		S	sg p
wanem	vanem	vanem	0		C	sg n
     	vanem	vanem	0		S	sg n
ollema	olema	ole	ma		V	ma
olewat	olevat	ole	vat		V	vat
läbbi	läbi	läbi	0		D	
     	läbi	läbi	0		K	
     	läbi	läbi	0		V	o
päewal	päeval	päev	l		S	sg ad
järrele	järele	järele	0		D	
       	järele	järele	0		K	
kokko	kokku	kogu	0		S	adt
     	kokku	kokku	0		D	
egga	ega	ega	0		D	
    	ega	ega	0		J	
wannem	vanem	vanem	0		C	sg n
      	vanem	vanem	0		S	sg n
wasta	vastu	vastu	0		D	
     	vastu	vastu	0		K	
     	vasta	vasta	0		V	o
jures	juures	juur	s		S	sg in
     	juures	juures	0		D	
     	juures	juures	0		K	
päewa	päeva	päev	0		S	adt
     	päeva	päev	0		S	sg g
     	päeva	päev	0		S	sg p
igga	iga	iga	0		P	sg n
    	iga	iga	0		P	sg g
    	iga	iga	0		P	sg p
    	iga	iga	0		S	sg n
    	iga	iga	0		S	sg p
päwal	päeval	päev	l		S	sg ad
hääks	heaks	hea	ks		A	sg tr
     	heaks	hea	ks		S	sg tr
     	heaks	heaks	0		D	
     	heaks	heaks	0		K	
perremees	peremees	pere_mees	0		S	sg n
nimetud	nimetatud	nimeta	tud		V	tud
wilja	vilja	vili	0		S	adt
     	vilja	vili	0		S	sg g
     	vilja	vili	0		S	sg p
jänud	jäänud	jää	nud		V	nud
wõi	või	või	0		D	
   	või	või	0		J	
   	või	või	0		S	sg n
   	või	või	0		S	sg g
   	või	või	0		V	o
es	ei	ei	s		V	neg s
peäle	peale	pea	le		S	sg all
     	peale	peale	0		D	
     	peale	peale	0		K	
üttelnud	ütelnud	ütle	nud		V	nud
Walla	valla	vald	0		S	sg g
     	valla	valla	0		D	
man	man	man	0		D	
   	man	man	0		K	
tarwis	tarvis	tarvis	0		D	
      	tarvis	tarvis	0		K	
Kohtuwanem	kohtuvanem	kohtu_vanem	0		C	sg n
          	kohtuvanem	kohtu_vanem	0		S	sg n
Tulli	tuli	tule	i		V	s
     	tuli	tuli	0		S	sg n
nimmetud	nimetatud	nimeta	tud		V	tud
wäärt	väärt	väärt	0		A	
     	väärt	väärt	0		D	
wõlga	võlga	võlg	0		S	adt
     	võlga	võlg	0		S	sg p
minno	mind	mina	0		P	sg p
     	minu	mina	0		P	sg g
Josep	Joosep	Josep	0		H	sg n
Karel	Kaarel	Karel	0		H	sg n
xxx	xxx	xxx	0		Y	?
Rbl	rbl	rbl	0		Y	?
ollid	olid	ole	id		V	sid
Maddis	Madis	Maddis	0		H	sg n
Juhhan	Juhan	Juhhan	0		H	sg n
Kop	kop	kop	0		Y	?
Hindrik	Hindrik	Hindrik	0		H	sg n
rubl	rubl	rubl	0		Y	?
Rubl	rubl	rubl	0		Y	?
rub	rubl	rubl	0		Y	?
Se	see	see	0		P	sg n
Koggokonna	kogukonna	kogukond	0		S	sg g
Kohtomees	kohtumees	kohtu_mees	0		S	sg n
Pääkohtumees	peakohtumees	pea_kohtu_mees	0		S	sg n
Marri	Mari	Marri	0		H	sg n
Tomas	Toomas	Tomas	0		H	sg n
Tallitaja	talitaja	talitaja	0		S	sg n
         	talitaja	talitaja	0		S	sg g
Adam	Aadam	Adam	0		H	sg n
koggokonna	kogukonna	kogukond	0		S	sg g
Joh	Joh	Joh	0		Y	?
Michel	Mihkel	Michel	0		H	sg n
wak	vakk	vakk	0		S	sg n
öölnud	öelnud	ütle	nud		V	nud
middagi	midagi	miski	da	gi	P	sg p
isse	ise	ise	0		P	sg n
    	ise	ise	0		D	
    	ise	ise	0		P	pl n
rahhul	rahul	rahu	l		S	sg ad
      	rahul	rahul	0		D	
järrel	järel	järel	0		D	
      	järel	järel	0		K	
ollewad	olevat	ole	vad		V	vat
Cop	kop	kop	0		Y	?
lönud	löönud	löö	nud		V	nud
Tõnno	Tõnu	Tõnno	0		H	sg n
pääwal	päeval	päev	l		S	sg ad
ennam	enam	enam	0		C	sg n
     	enam	enam	0		D	
jure	juurde	juur	de		S	adt
    	juurde	juurde	0		D	
    	juurde	juurde	0		K	
kaebtuse	kaebuse	kaebus	0		S	sg g
woi	või	või	0		S	sg n
   	või	või	0		S	sg g
   	või	või	0		J	
   	või	või	0		D	
   	või	või	0		V	o
päiwa	päeva	päev	0		S	sg g
     	päeva	päev	0		S	sg p
tehha	teha	tege	a		V	da
mehhed	mehed	mees	d		S	pl n
kunni	kuni	kuni	0		K	
     	kuni	kuni	0		J	
naad	nad	tema	d		P	pl n
issa	isa	isa	0		S	sg n
    	isa	isa	0		S	sg g
    	isa	isa	0		S	sg p
An	Ann	An	0		H	sg n
Hindrek	Hindrek	Hindrek	0		H	sg n
Rahha	raha	raha	0		S	sg n
     	raha	raha	0		S	sg g
     	raha	raha	0		S	sg p
Tawet	Taavet	Tawet	0		H	sg n
Widrik	Vidrik	Widrik	0		H	sg n
polest	poolest	pool	st		N	sg el
      	poolest	pool	st		S	sg el
      	poolest	poolene	t		A	sg p
      	poolest	poolest	0		K	
tahha	taha	taha	0		K	
     	taha	taha	0		D	
     	taha	taht	0		V	o
tahhab	tahab	taht	b		V	b
abbimees	abimees	abi_mees	0		S	sg n
rbla	rbl	rbl	0		Y	?
pantud	pandud	pane	tud		V	tud
Rbla	rbl	rbl	0		Y	?
päwa	päeva	päev	0		S	sg g
piddi	pidi	pida	i		V	s
     	pidi	pidi	0		K	
jäeb	jääb	jää	b		V	b
lubbab	lubab	luba	b		V	b
Liso	Liisu	Liso	0		H	sg n
    	Liisu	Liso	0		H	sg g
wötnud	võtnud	võt	nud		V	nud
ningk	ning	ning	0		J	
küssitud	küsitud	küsi	tud		V	tud
paljo	palju	palju	0		D	
räkis	rääkis	rääki	s		V	s
mehhe	mehe	mees	0		S	sg g
hopi	hoopi	hoop	0		S	sg p
Pridik	Priidik	Pridik	0		H	sg n
lubbanud	lubanud	luba	nud		V	nud
Tannil	Tanel	Tannil	0		H	sg n
jo	ju	ju	0		D	
  	juba	juba	0		D	
Abbi	abi	abi	0		S	sg n
    	abi	abi	0		S	sg g
    	abi	abi	0		S	sg p
Kripson	Kripson	Kripson	0		H	sg n
Abram	Aabram	Abram	0		H	sg n
Mik	Mikk	Mik	0		H	sg n
Möistus	mõistus	mõistus	0		S	sg n
Kohtowannem	kohtuvanem	kohtu_vanem	0		S	sg n
lubbas	lubas	luba	s		V	s
Temma	tema	tema	0		P	sg n
     	tema	tema	0		P	sg g
Kaibas	kaebas	kaeba	s		V	s
lubba	luba	luba	0		S	sg n
     	luba	luba	0		S	sg p
     	luba	luba	0		V	o
     	lubab	luba	0		V	b
Herra	härra	härra	0		S	sg n
     	härra	härra	0		S	sg g
tallitaja	talitaja	talitaja	0		S	sg n
         	talitaja	talitaja	0		S	sg g
kohhe	kohe	kohe	0		D	
lepping	leping	leping	0		S	sg n
abbi	abi	abi	0		S	sg n
    	abi	abi	0		S	sg g
    	abi	abi	0		S	sg p
nüid	nüüd	nüüd	0		D	
kahjo	kahju	kahi	0		S	sg g
     	kahju	kahi	0		S	sg p
     	kahju	kahi	0		S	adt
     	kahju	kahju	0		S	sg n
     	kahju	kahju	0		S	sg g
     	kahju	kahju	0		S	sg p
kirjutud	kirjutatud	kirjuta	tud		V	tud
Agga	aga	aga	0		J	
oliwad	olid	ole	ivad		V	sid
kaebtust	kaebust	kaebus	t		S	sg p
möda	mööda	mööda	0		K	
    	mööda	mööda	0		D	
ommale	omale	oma	le		P	sg all
käiaid	käijaid	käija	id		S	pl p
ollewat	olevat	ole	vat		V	vat
sepärrast	seepärast	see_pärast	0		D	
päiwal	päeval	päev	l		S	sg ad
sullane	sulane	sulane	0		S	sg n
saap	saab	saa	p		V	b
selletada	seletada	seleta	da		V	da
palusiwad	palusid	palu	sivad		V	sid
peap	peab	pida	b		V	b
Januaril	jaanuaril	jaanuar	l		S	sg ad
kahhe	kahe	kahe	0		N	sg g
al	all	all	0		K	
  	all	all	0		D	
perremehhe	peremehe	pere_mees	0		S	sg g
tonud	toonud	too	nud		V	nud
üllesse	üles	üles	0		D	
jubba	juba	juba	0		D	
