# Võrgustikuanalüüsi töövoo kirjeldus

Nimeüksuste automaatne märgendamine võimaldab tehniliselt võrdlemisi hõlpsasti uurida vallakohtuprotokollides esinevate isikute vahelisi suhteid, ent ei anna sisuliselt täielikult rahuldavaid tulemusi. Selle põhjuseks on nimeüksuste mitmesus. Sama nimega võidakse viidata mitmele eri isikule ning isikuid on võimalik tuvastada sageli vaid mingite kaasuvate tunnuste alusel ning teiste ajalooallikate (nt kirikuraamatute) abiga.

Seetõttu ongi siinse võrgustikuanalüüsi sisendfailid koostatud käsitsi nelja valla protokollide põhjal. 

Nii võrgustikuanalüüsi sisendfailid, skriptid kui ka väljundfailid asuvad kaustas *vorgustikud_python*.   


## Andmestike koostamine

Rahvusarhiivi ühisloome käigus sisestatud Kiltsi, Kirna, Parila ja Võnnu valla kirjetest on eemaldatud  

- pereliikmete omavahelised hagid ja lepingud (v.a lapsendamised),  
- riiklike ja kogukondlike maksude sissenõudmine (v.a maksuvõla pinnalt tekkinud uued tülid ehk üleastumised),  
- juhtumid, kus midagi polnud arutada,  
- jätkuprotokollid (kus sama kaasust menetletakse edasi).  

Kõrvale jäeti ka külade vastu esitatud kaebused, kui peremehi eraldi loetletud pole (nt Pala küla pm vastu jne).

Kõik allesjäänud kohtulood on jagatud kolme **teemagruppi**:  

- varalised tehingud (teemagrupi kood 1),  
- töö- ja teenistussuhted (teemagrupi kood 2),  
- üleastumised ja kriminaalasjad (teemagrupi kood 3).  


Valdade protokollidest on tuvastatud kõik hagejad ja kostjad, lisaks nende  

- **sugu**  
	- mees \- 1  
	- naine \- 2  
- **amet**  
	- vallaametnik \- 10  
	- kõrtsmik \- 11  
	- poolmõisa/karjamõisa omanik v rentnik, karjamõisa valitseja \- 12  
	- metsavaht \- 13  
	- mõisnik, mõisarentnik, mõisapolitsei \- 14  
	- mõisaametnik (valitseja, aidamees, kubjas) \- 15  
	- koolmeister \- 16  
	- kõik ülejäänud \- tühi  
- **päritolu**  
	- kohalik (Kirna protokollid) \- 1  
	- muu vald (Kirna protokollid) \- 2  
	- kohalik (Kiltsi protokollid) \- 4  
	- muu vald (Kiltsi protokollid) \- 5  
	- kohalik (Võnnu ja Jõesse protokollid) \- 6  
	- muu vald (Võnnu ja Jõesse protokollid) \- 7  
	- kohalik (Parila protokollid) \- 8  
	- muu vald (Parila protokollid) \- 9  
	- linn \- 3  


### Päritolu

Kohalikeks on loetud need, kelle kohta protokolli tekstis märgitakse kuulumine sellesse valda või kes personaalraamatu järgi on märgitud selle valla alla. Personaalraamatud on koostatud mõisate kaupa. Kui hagejate puhul on üldjuhul päritoluvald protokollis kirjas, siis kostjate puhul sageli mitte. Kui kostja nime järgi otsustades pole kostja nimetatud vallast, aga teksti järgi võib arvata, et ta seal elas ja töötas, on ta märgitud kohalikuks. Osa kostjate puhul jääb aga elukoht teadmata, nt kõtsikaklustes osales hulk inimesi, sageli mittekohalikke. Hetkel on nende puhul valla lahter tühi. Samuti on päritolu kahjutasunõuete puhul ebaselge, nt kelle loomad rikkusid kirnalaste maid (naaberküla, aga mis vallas?). Mõisatöölised olid liikuvad ja ka talusulased on liikunud, nende seas on palju isikuid, kes pole personaalraamatus Kirna all.
Kostjate osas võib pilt olla nihkes kohalike kasuks: kohalikke võis olla vähem kui tabelis märgitud. 


### Amet  

Ka ameti tunnus ei ole päris probleemitu, sest sageli protokollis ametit ei märgita (eriti kostjate puhul). Kui ühel ja samal isikul on sama amet märgitud *x* ja *y* aastal, lsati kõigile temaga seotud juhtumitele sama amet, kui juhtumid jäid *x* ja *y* vahele.  


### Samanimelised isikud

Samanimelised naised esinevad hageja või kostjana harva, aga meeste puhul oli see suur probleem. Samanimelised mehed on koondatud ühe ID alla, kui neid on nimetatud sama ametiga või seostatud sama taluga, nt *peremees*, *talitaja*, *Lassi*. Samuti püüti aimata kohtulugude sisu põhjal, kas need isikud on juba varem omavahel kohut käinud. Kui jah ja muidu tundusid ka omavahel riidlevat, said sama nime. Kuna enamasti protokollis ametit (*peremees*, *sulane*, *moonakas*, *karjus* jne) ei märgita ja kui personaalraamatus oli sobivas vanuserühmas kaks või kolm Hans X-i, Juhan Y-t, siis on jäetud erinevad ID-d, kuigi osalt võib olla tegemist samade isikutega. Kahjuks on Jüri, Hansu, Jaani ja Juhani puhul isikute tuvastamine ja kokkuviimine äärmiselt keeruline ja paljudel juhtudel isegi võimatu.


## Andmestike analüüs

Sisendfailid on siinses repositooriumis csv-failide kujul (vt kausta `vorgustikud_python`).  

Pythoni skript `vorgustikud_python/00_construct_graphml_files_and_outputs.py` loeb kõikide valdade csv-failid sisse, puhastab ja korrastab päiste ja kirjete infot, dekodeerib teemagrupid, osaliste rollid, päritolu, soo ja ameti ning ehitab võrgustiku graafid. Salvestab graafid _graphml_ laiendiga failidesse. Rakendab töös skripti `vorgustikud_python/01_read_data_and_build_sna_graph.py`.

Pythoni skript `vorgustikud_python/02_draw_sna_graphs.py` joonistab _graphml_ failide põhjal võrgustike ajagraafid (joonised 6-9), mis kujutavad eri isikute vahelisi kohtusuhteid (hagemissuhteid) ajateljel. Salvestab ajagraafid _png_ laiendiga failidena. Joonised on saadaval kaustas `vorgustikud_python`.
   
