#
#   Repeatedly calls out script "01_read_data_and_build_sna_graph.py"
#   to construct graphml files and analysis output files for all 
#   input files.
#
#   For details, see "01_read_data_and_build_sna_graph.py".
#

import os, os.path
import subprocess

if __name__ == "__main__":
    script = '01_read_data_and_build_sna_graph.py'
    if not os.path.exists(script):
        raise FileNotFoundError(f'(!) Missing script {script}')
    input_and_output_files = [('Kiltsi 170922_parandatud.csv', 'Kiltsi_170922_parandatud.out.txt'),
                              ('Kirna vald 040521.csv', 'Kirna_040521.out.txt'),
                              ('Parila 170922 parandatud.csv', 'Parila_170922_parandatud.out.txt'),
                              ('Võnnu Jõesse 110221.csv', 'Võnnu_Jõesse_110221.out.txt')]
    for (input_fname, output_fname) in input_and_output_files:
        if not os.path.exists(input_fname):
            raise FileNotFoundError(f'(!) Missing input file {input_fname!r}')
        print(f'Running python {script} {input_fname} ...')
        result = subprocess.run(["python", script, input_fname, 'aggregate'], capture_output=True, text=True)
        if len(result.stderr) > 0:
            raise Exception('(!) Error on running script:\n'+str(result.stderr))
        with open(output_fname, 'w', encoding='utf-8') as out_f:
            out_f.write(result.stdout)