#
#   Reads SNA data from a CSV file, builds the graph 
#   and prints out statistics about the graph.
#   Additionally, writes out graph data in .graphml 
#   format for visualization in some tool, and outputs 
#   a variant of a dynamic graph, where nodes have 
#   temporal (start/end) boundaries (for experimental 
#   animation in Gephi).
#
#   Requirements:
#   * networkx >= 2.5
#   * termplotlib >= 0.3.2
#   * python-dateutil
#

import re
import csv
import sys
import os, os.path
from collections import Counter
import random
import copy
from statistics import mean

import networkx as nx

import dateutil.parser
from dateutil.relativedelta import relativedelta

import termplotlib as tpl

from collections import defaultdict
from collections import namedtuple

# ===================================================================
#   Data import and pre-processing
# ===================================================================

Kohtuline = namedtuple('Kohtuline', ['id', 'sugu', 'vald', 'amet'])

# Reads SNA data from a CSV format input file, decodes data values and 
# returns data in a form of a list of court cases 
def import_data_from_csv( fname, decode_data=True, compress_data=True, 
                                 delimiter=';', encoding='Windows-1257', **fmtparams ):
    assert os.path.exists(fname), f'(!) Invali input file {fname!r}'
    items = []
    case_id_missing = False
    with open(fname, 'r', newline='', encoding=encoding) as in_f:
        fle_reader = csv.reader(in_f, delimiter=';', **fmtparams)
        header = next(fle_reader)
        if "Kiltsi" in fname:
            for i, label in enumerate(header):
                if label == "K5 vald" and i-1 > 0 and header[i-1] == 'K6 Id':
                    print('Parandus:', header[i], '->', "K6 vald" )
                    header[i] = "K6 vald"
                if label == "K5 sugu" and i-1 > 0 and header[i-1] == 'K6 vald':
                    print('Parandus:', header[i], '->', "K6 sugu" )
                    header[i] = "K6 sugu"
        if "Kirna" in fname:
            for i, label in enumerate(header):
                if label == "K1 sugu" and i-1 > 0 and header[i-1] == 'K2 ID':
                    print('Parandus:', header[i], '->', "K2 sugu" )
                    header[i] = "K2 sugu"
        norm_header = [re.sub('^[Hh]([0-9]+)_(\S+)$', 'hageja \\1 \\2', label) for label in header]
        norm_header = [re.sub('^[Kk]([0-9]+)_(\S+)$', 'kostja \\1 \\2', label) for label in norm_header]
        norm_header = [re.sub('^[Hh]\s*([0-9]+)', 'hageja \\1', label) for label in norm_header]
        norm_header = [re.sub('^[Kk]\s*([0-9]+)', 'kostja \\1', label) for label in norm_header]
        if "Parila" in fname:
            norm_header = [re.sub('^\s*koht\s*$', 'kostja 3 vald', label) for label in norm_header]
            norm_header = [re.sub('^date$', 'day', label) for label in norm_header]
        if "Kirna" in fname:
            norm_header = [re.sub('^date$', 'day', label) for label in norm_header]
        norm_header = [re.sub('[Tt]eemagrupi kood', 'teema', label) for label in norm_header]
        norm_header = [re.sub('[Tt]eemagrupi_kood', 'teema', label) for label in norm_header]
        norm_header = [re.sub('protokolli id', 'protocol_number', label) for label in norm_header]
        norm_header = [re.sub('protokolli_id', 'protocol_number', label) for label in norm_header]
        if "Võnnu" in fname and "Jõesse" in fname:
            norm_header = [re.sub('^kust$', 'kostja 2 vald', label) for label in norm_header]
        norm_header = [label.lower() for label in norm_header]
        case_id_missing = 'id' not in norm_header
        from pprint import pprint
        print('Tabeli väljad:')
        pprint( norm_header )
        hageja_max  = max([int(key.split()[1]) for key in norm_header if key.startswith('hageja ')])
        hageja_min  = min([int(key.split()[1]) for key in norm_header if key.startswith('hageja ')])
        kostja_max  = max([int(key.split()[1]) for key in norm_header if key.startswith('kostja ')])
        kostja_min  = min([int(key.split()[1]) for key in norm_header if key.startswith('kostja ')])
        print(f'hageja {hageja_min}..{hageja_max};  kostja {kostja_min}..{kostja_max};')
        case_counter = 0
        line_id = 0
        for row in fle_reader:
            assert len(row) == len(header), '(!) Problem at row {} : Unexpected number of elements in a row: {!r}'.format(line_id+1, row)
            data_dict = { key:row[kid] for kid, key in enumerate(norm_header) }
            line_id += 1
            # Check if the row is totally empty
            if all([data_dict[k] is None or len(data_dict[k])==0 for k in data_dict.keys()]):
                # Skip empty row ...
                continue
            if decode_data:
                for k in data_dict.keys():
                    decoded = decode_field( k, data_dict[k] )
                    if decoded != data_dict[k]:
                        data_dict[k] = decoded
            if compress_data:
                #
                # 'hageja 1', 'hageja 2', ... -> 'hagejad'
                #
                hagejad = []
                for i in range(hageja_min, hageja_max + 1):
                    id   = data_dict.get(f'hageja {i} id', '')
                    sugu = data_dict.get(f'hageja {i} sugu', '')
                    vald = data_dict.get(f'hageja {i} vald', '')
                    amet = data_dict.get(f'hageja {i} amet', '')
                    if len(id) > 0:
                        hagejad.append( Kohtuline(id, sugu, vald, amet) )
                    # remove old fields
                    for field_name in ['id', 'sugu', 'vald', 'amet']:
                        if f'hageja {i} {field_name}' in data_dict.keys():
                            del data_dict[f'hageja {i} {field_name}']
                data_dict['hagejad'] = hagejad
                #
                # 'kostja 1', 'kostja 2', ... -> 'kostjad'
                #
                kostjad = []
                for i in range(kostja_min, kostja_max + 1):
                    id   = data_dict.get(f'kostja {i} id', '')
                    sugu = data_dict.get(f'kostja {i} sugu', '')
                    vald = data_dict.get(f'kostja {i} vald', '')
                    amet = data_dict.get(f'kostja {i} amet', '')
                    if len(id) > 0:
                        kostjad.append( Kohtuline(id, sugu, vald, amet) )
                    # remove old fields
                    for field_name in ['id', 'sugu', 'vald', 'amet']:
                        if f'kostja {i} {field_name}' in data_dict.keys():
                            del data_dict[f'kostja {i} {field_name}']
                data_dict['kostjad'] = kostjad
            #
            #  Lisame kirjesse puuduva juhtumi id
            # 
            if case_id_missing:
                assert 'id' not in data_dict.keys()
                data_dict['id'] = str( case_counter )
            #
            #  Jätame välja juhtumid, kus nö vastaspoolel pole ühelgi osalejal ID
            #  määratud, st vastaspoolel sisuliselt polegi kedagi ...
            #
            add_item = True
            if compress_data:
                if len(data_dict['kostjad']) > 0 and len(data_dict['hagejad']) == 0:
                    print( '(!) Problem at row {} : No hageja id-s available in row {!r}. Skipping the row. '.format(line_id+1, ';'.join(row)) )
                    add_item = False
                elif len(data_dict['kostjad']) == 0 and len(data_dict['hagejad']) > 0:
                    print( '(!) Problem at row {} : No kostja id-s available in row {!r}. Skipping the row. '.format(line_id+1, ';'.join(row)) )
                    add_item = False
            if add_item:
                for k in data_dict['kostjad']:
                    if k.sugu == '':
                        print( '(!) Problem at row {} : Unable to get kostja sugu from {!r}.'.format(line_id+1, ';'.join(row)) )
                for h in data_dict['hagejad']:
                    if h.sugu == '':
                        print( '(!) Problem at row {} : Unable to get hageja sugu from {!r}.'.format(line_id+1, ';'.join(row)) )
                items.append( data_dict )
            case_counter += 1
        pass
    return items

# Decodes field's value: replaces numeric value with text value
def decode_field( field, value ):
    if field.lower().endswith(' id') or field.lower() == 'id':
        if len(value) == 0:
            return value
        elif value.isnumeric():
            return value
        else:
            print(f'(!) Exceptional value: {field}={value!r}')
            return value
    elif field.lower().endswith(' sugu'):
        if len(value) == 0:
            return value
        elif value.lower() in ['mees', 'naine']:
            return value.lower()
        elif value == '1':
            return 'mees'
        elif value == '2':
            return 'naine'
        else:
            raise Exception(f'(!) Cannot decode {field}={value}')
    elif field.lower() == 'teema':
        if len(value) > 0 and not value.isnumeric():
            # Attempt to parse a value from a combination of values
            multivalued_item = re.match('(\d+)\s*;\s*(\d+)$', value)
            if multivalued_item:
                id1 = multivalued_item.group(1)
                id2 = multivalued_item.group(2)
                new_multiple_values = []
                for val in [ int(id1), int(id2) ]:
                    if val == 1:
                        new_multiple_values.append( 'varalised tehingud' )
                    elif val == 2:
                        new_multiple_values.append( 'töösuhted' )
                    elif val == 3:
                        new_multiple_values.append( 'üleastumised / kriminaalasjad' )
                new_multiple_values_str = '; '.join(new_multiple_values)
                #print(f'(!) Multi-valued group: {field}={new_multiple_values_str!r}')
                return new_multiple_values_str
            else:
                print(f'(!) Invalid value: {field}={value!r}')
                return value
        elif len(value) == 0:
            return value
        if int(value) == 1:
            return 'varalised tehingud'
        elif int(value) == 2:
            return 'töösuhted'
        elif int(value) == 3:
            return 'üleastumised / kriminaalasjad'
        else:
            raise Exception(f'(!) Cannot decode {field}={value}')
    elif field.lower().endswith(' amet'):
        if len(value) == 0:
            return 'kõik ülejäänud'
        elif value.isalpha():
            print(f'(!) Exceptional value: {field}={value!r}')
            return value
        elif int(value) == 10:
            return 'vallaametnik'
        elif int(value) == 11:
            return 'kõrtsmik'
        elif int(value) == 12:
            return 'karjamõisa rentnik v valitseja'
        elif int(value) == 13:
            return 'metsavaht'
        elif int(value) == 14:
            return 'mõisnik, mõisarentnik, mõisapolitsei'
        elif int(value) == 15:
            return 'mõisaametnik'
        elif int(value) == 16:
            return 'koolmeister'
        else:
            raise Exception(f'(!) Cannot decode {field}={value}')
    elif field.lower().endswith(' vald'):
        if len(value) == 0:
            return value
        elif int(value) == 1:
            return 'kirna'
        elif int(value) == 2:
            return 'muu vald'
        elif int(value) == 3:
            return 'linn'
        elif int(value) == 4:
            return 'kiltsi'
        elif int(value) == 5:
            return 'muu vald'
        elif int(value) == 6:
            return 'võnnu ja jõesse'
        elif int(value) == 7:
            return 'muu vald'
        elif int(value) == 8:
            return 'parila, kessu jne'
        elif int(value) == 9:
            return 'muu vald'
        else:
            raise Exception(f'(!) Cannot decode {field}={value}')
    return value

# Check for possible conflicts between two entries with identical id-s
def check_for_conflicting_kohtuline( kohtuline1, kohtuline2 ):
    report_conflict = False
    for fid, field_name in enumerate(['id', 'sugu', 'vald', 'amet']):
        if kohtuline1[fid] != kohtuline2[fid]:
            if field_name in ['id', 'sugu', 'vald']:
                report_conflict = True
            elif field_name == 'amet':
                # Do not report conflict for profession
                # (it can change over time)
                pass
    if report_conflict:
        print(f' Conflicting entries: {kohtuline1} != {kohtuline2}')


# Merges duplicate or conflicting entries into one entry
def converge_conflicting_entries( entries, apply_set_for_ambiguous=False ):
    assert len(entries) > 1
    entries_unpacked = [ e._asdict() for e in entries ]
    id   = [(e['id']   if len(e['id'])   > 0 else '_') for e in entries_unpacked ][0]
    if not apply_set_for_ambiguous:
        sugu = '|'.join([(e['sugu'] if len(e['sugu']) > 0 else '_') for e in entries_unpacked ])
        vald = '|'.join([(e['vald'] if len(e['vald']) > 0 else '_') for e in entries_unpacked ])
        amet = '|'.join([(e['amet'] if len(e['amet']) > 0 else '_') for e in entries_unpacked ])
    else:
        sugu = '|'.join(list(set([(e['sugu'] if len(e['sugu']) > 0 else '_') for e in entries_unpacked ])))
        vald = '|'.join(list(set([(e['vald'] if len(e['vald']) > 0 else '_') for e in entries_unpacked ])))
        amet = '|'.join(list(set([(e['amet'] if len(e['amet']) > 0 else '_') for e in entries_unpacked ])))
    return Kohtuline(id, sugu, vald, amet)


# Builds a networkx graph based on court cases and participants
def build_graph( cases, participants, apply_set_for_ambiguous=False  ):
    G_asymmetric = nx.DiGraph()
    # Lisame kõik kohtulised ja nende andmed
    for part_key in participants.keys():
        cur_participants = participants[ part_key ]
        chosen_entry = cur_participants[0]
        if len(cur_participants) > 1:
            # In case of multiple entries, merge all of their values into a single entry
            chosen_entry = converge_conflicting_entries( cur_participants, 
                                                         apply_set_for_ambiguous = apply_set_for_ambiguous )
        G_asymmetric.add_node(str(chosen_entry.id), 
                              sugu=chosen_entry.sugu, 
                              vald=chosen_entry.vald,
                              amet=chosen_entry.amet)
    print('Building graph: initial number of nodes:', len( G_asymmetric.nodes(data=False) ))
    if apply_set_for_ambiguous:
        print('Applying set on ambiguous node attribute values for merging ...')
    # Loendame, mitu korda keegi kedagi hages
    prosecution_counts    = {}
    prosecution_dates     = {}
    prosecution_topics    = {}
    prosecution_protocols = {}
    prosecution_case_ids  = {}
    for court_case in cases:
        hagejad = court_case['hagejad']
        kostjad = court_case['kostjad']
        date = '-'.join([court_case['year'], court_case['month'], court_case['day']])
        topic = court_case['teema']
        protocol_nr = court_case['protocol_number']
        case_id = court_case['id']
        for h in hagejad:
            for k in kostjad:
                hid = str(h.id)
                kid = str(k.id)
                if (hid, kid) not in prosecution_counts:
                    prosecution_counts[(hid, kid)] = 0
                    prosecution_dates[(hid, kid)] = []
                    prosecution_topics[(hid, kid)] = []
                    prosecution_protocols[(hid, kid)] = []
                    prosecution_case_ids[(hid, kid)] = []
                prosecution_counts[(hid, kid)] += 1
                prosecution_dates[(hid, kid)].append( date )
                prosecution_topics[(hid, kid)].append( topic )
                prosecution_protocols[(hid, kid)].append( protocol_nr )
                prosecution_case_ids[(hid, kid)].append( case_id )
    i = 0
    higher_weight_nodes = []
    total_number_of_nodes = len( prosecution_counts.keys() )
    for (hid, kid) in sorted(prosecution_counts.keys(), key=prosecution_counts.get, reverse=True):
        G_asymmetric.add_edge( hid, kid, 
                               weight = prosecution_counts[(hid, kid)],
                               dates = prosecution_dates[(hid, kid)],
                               topics = prosecution_topics[(hid, kid)],
                               protocols = prosecution_protocols[(hid, kid)],
                               case_ids = prosecution_case_ids[(hid, kid)] )
        if i < int(total_number_of_nodes / 2):
            higher_weight_nodes.append( hid )
        i += 1
    print('Building graph: Final number of nodes:', len( G_asymmetric.nodes(data=False) ))
    return G_asymmetric
    #return G_asymmetric, higher_weight_nodes

# ===================================================================
#   Graph analysis utils
# ===================================================================

# Finds and returns the percentage of all nodes that the given degree covers.
def get_degree_percentage( graph, degree ):
    all_nodes = len(graph.nodes)-1  # all nodes except self (cannot sue self)
    return 100.0*degree/all_nodes

# Finds the percentage of all nodes that the given degree covers.
# Returns a formatted percentage string only if it exceeds the threshold.
# Otherwise, returns an empty string.
def get_degree_percentage_with_threshold(graph, degree, threshold=1.0):
    per = get_degree_percentage(graph, degree)
    if per > threshold:
        return f'({per:.1f}%)'
    else:
        return ''

# Returns graph's all nodes sorted by their degrees
def graph_nodes_by_degrees( graph, degree_type='IN', weight=None ):
    assert degree_type in ['IN', 'OUT']
    nodes_by_degrees = []
    for node in graph.nodes():
        degree = graph.in_degree(node) if degree_type=='IN' else graph.out_degree(node)
        degree_weighted = degree
        if weight is not None:
            degree_weighted = graph.in_degree(node,weight=weight) if degree_type=='IN' else graph.out_degree(node,weight=weight)
        nodes_by_degrees.append( (node, degree, degree_weighted) )
    return sorted(nodes_by_degrees, key=lambda x:x[1], reverse=True)

# Returns a list of all attributes of ingoing or outgoing arcs of a node
def get_all_edge_attributes( graph, node, attribute, degree_type='IN' ):
    assert degree_type in ['IN', 'OUT']
    if degree_type == 'IN':
        itemlist = list(graph.in_edges(node,data=True))
    else:
        itemlist = list(graph.out_edges(node,data=True))
    return [attr_val for edge in itemlist for attr_val in edge[2][attribute]]

# Returns set of all attributes of ingoing or outgoing arcs of a node
def get_all_edge_attributes_set( graph, node, attribute, degree_type='IN' ):
    assert degree_type in ['IN', 'OUT']
    if degree_type == 'IN':
        itemlist = list(graph.in_edges(node,data=True))
    else:
        itemlist = list(graph.out_edges(node,data=True))
    return set([attr_val for edge in itemlist for attr_val in edge[2][attribute]])

# Returns sorted counts of topics associated with the given node
def get_node_topics_count( graph, node, degree_type='IN', only_repeated=False, only_nonrepeated=False ):
    assert degree_type in ['IN', 'OUT']
    if degree_type == 'IN':
        itemlist = list(graph.in_edges(node,data=True))
    else:
        itemlist = list(graph.out_edges(node,data=True))
    if not only_repeated and not only_nonrepeated:
        topics = [attr_val for edge in itemlist for attr_val in edge[2]['topics']]
    elif only_repeated and not only_nonrepeated:
        topics = [attr_val for edge in itemlist for attr_val in edge[2]['topics'] if edge[2]['weight'] > 1 ]
    elif not only_repeated and only_nonrepeated:
        topics = [attr_val for edge in itemlist for attr_val in edge[2]['topics'] if edge[2]['weight'] == 1 ]
    else:
        raise ValueError('(!) Conflicting arguments only_repeated=True, only_nonrepeated=True')
    topics_counter = Counter( topics )
    return [(t, c) for (t, c) in topics_counter.most_common()]

# Finds and outputs TOP N nodes sorted by outgoing and ingoing arcs
def top_outgoing_and_ingoing( in_graph, n = 5):
    node_data = { k:v for (k, v) in in_graph.nodes(data=True) }
    nodes_by_out_degrees = graph_nodes_by_degrees( in_graph, degree_type='OUT', weight='weight' )
    print()
    print( f'Hagejate TOP {n} graafi põhjal (out_degrees): ' )
    for (node, degree, degree_w) in nodes_by_out_degrees[:n]:
        protsesse = len(get_all_edge_attributes_set( in_graph, node, 'case_ids', degree_type='OUT' ))
        degree_percentage_str = get_degree_percentage_with_threshold(in_graph, degree, threshold=1.0)
        print( f'  id = {node}    hagetud_eri_isikuid: {degree} {degree_percentage_str}   hagetud_isikuid(sh_korduvad): {degree_w}    hagemisjuhtumeid: {protsesse}     {node_data[node]}' )
        print( f'              teemad:', end=' ')
        for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='OUT'):
            print( f'{teema!r} {sagedus}', end='  ')
        print()
        print()
    nodes_by_in_degrees  = graph_nodes_by_degrees( in_graph, degree_type='IN', weight='weight' )
    print()
    print( f'Kostjate TOP {n} graafi põhjal (in_degrees): ' )
    for (node, degree, degree_w) in nodes_by_in_degrees[:n]:
        protsesse = len(get_all_edge_attributes_set( in_graph, node, 'case_ids', degree_type='IN' ))
        degree_percentage_str = get_degree_percentage_with_threshold(in_graph, degree, threshold=1.0)
        print( f'  id = {node}    kostetud_eri_isikute_vastu: {degree} {degree_percentage_str}   kostetud_isikute_vastu(sh_korduvad): {degree_w}    kostmisjuhtumeid: {protsesse}    {node_data[node]}' )
        print( f'              teemad:', end=' ')
        for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='IN'):
            print( f'{teema!r} {sagedus}', end='  ')
        print()
        print()
    print()
    print()

# Finds and outputs TOP N nodes sorted by repeated outgoing and ingoing arcs
def top_repeated_outgoing_and_ingoing( in_graph, n = 5):
    node_data = { k:v for (k, v) in in_graph.nodes(data=True) }
    repeated_outgoing = []
    for node in in_graph.nodes():
        itemlist = list(in_graph.out_edges(node, data=True))
        # non-repeated 
        nr_outgoing = [(edge[1], edge[2]['weight']) for edge in itemlist if edge[2]['weight'] == 1]
        # repeated 
        r_outgoing  = [(edge[1], edge[2]['weight']) for edge in itemlist if edge[2]['weight'] > 1]
        if len(r_outgoing) > 0:
            # r_topics = 
            repeated_outgoing.append( (node, len(r_outgoing), sum([v for _,v in r_outgoing]), len(nr_outgoing) ) )
    repeated_outgoing = sorted(repeated_outgoing, key=lambda x:x[1], reverse=True)
    print( f'Korduvhagejate TOP {n} (repeated_out_degrees): ' )
    for (node, different, total_repeated, non_repeated) in repeated_outgoing[:n]:
        print( f'  id = {node}    korduvhagetuid: {different}    korduvhagemisi_kokku: {total_repeated}  ||  kordumatuid_hagemisi: {non_repeated}    {node_data[node]}' )
        print( f'              korduvhag.  teemad:', end=' ')
        for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='OUT', only_repeated=True):
            print( f'{teema!r} {sagedus}', end='  ')
        if non_repeated > 0:
            print()
            print( f'              kordumatute teemad:', end=' ')
            for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='OUT', only_nonrepeated=True):
                print( f'{teema!r} {sagedus}', end='  ')
        print()
        print()
    print()
    repeated_ingoing = []
    for node in in_graph.nodes():
        itemlist = list(in_graph.in_edges(node, data=True))
        # non-repeated 
        nr_ingoing = [(edge[0], edge[2]['weight']) for edge in itemlist if edge[2]['weight'] == 1]
        # repeated 
        r_ingoing = [(edge[0], edge[2]['weight']) for edge in itemlist if edge[2]['weight'] > 1]
        if len(r_ingoing) > 0:
            repeated_ingoing.append( (node, len(r_ingoing), sum([v for _,v in r_ingoing]), len(nr_ingoing) ) )
    repeated_ingoing = sorted(repeated_ingoing, key=lambda x:x[1], reverse=True)
    print( f'Korduvkostjate TOP {n} (repeated_in_degrees): ' )
    for (node, different, total_repeated, non_repeated) in repeated_ingoing[:n]:
        print( f'  id = {node}    korduvkostetuid_(st_eri_isikud): {different}    korduvkostmisi_kokku: {total_repeated}  ||  kordumatuid_kostmisi: {non_repeated}    {node_data[node]}' )
        print( f'              korduvkost. teemad:', end=' ')
        for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='IN', only_repeated=True):
            print( f'{teema!r} {sagedus}', end='  ')
        if non_repeated > 0:
            print()
            print( f'              kordumatute teemad:', end=' ')
            for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='IN', only_nonrepeated=True):
                print( f'{teema!r} {sagedus}', end='  ')
        print()
        print()

# Finds and outputs TOP N nodes sorted by out_degrees and in parallel by weighted out degrees
def top_aggregated_outgoing( in_graph, n = 5 ):
    node_data = { k:v for (k, v) in in_graph.nodes(data=True) }
    nodes_by_aggregated_outgoing = []
    for node in in_graph.nodes():
        itemlist = list(in_graph.out_edges(node, data=True))
        # non-repeated outgoing
        outgoing_unique = len(itemlist)
        # outgoing including repeats
        outgoing_with_repeats = sum( [edge[2]['weight'] for edge in itemlist] )
        nodes_by_aggregated_outgoing.append( (node, outgoing_unique, outgoing_with_repeats) )
    print()
    print( f'Hagejate koond-TOP {n} (out_degrees + weighted out_degrees [incl repeated outgoing edges]): ' )
    sorted_by_uniq = sorted(nodes_by_aggregated_outgoing, key=lambda x: x[1], reverse=True)
    sorted_by_reps = sorted(nodes_by_aggregated_outgoing, key=lambda x: x[2], reverse=True)
    for i in range(n):
        item_1 = sorted_by_uniq[i]
        item_2 = sorted_by_reps[i]
        print( f'  id = {item_1[0]}    hagetud_eri_isikuid: {item_1[1]} ||   id = {item_2[0]}    hagetud_isikuid(sh_korduvad): {item_2[2]}' )
        print()
    print()

# Converts date string to iso format (yyyy-mm-dd)
def convert_date_value_to_iso_format( date_str ):
    # Normalize date:  '1867-1-1' --> '1867-01-01'
    date_str = re.sub('-(\d)-', '-0\\1-', re.sub('-(\d)$', '-0\\1', date_str))
    date_str = re.sub('/(\d)/', '/0\\1/', re.sub('/(\d)$', '/0\\1', date_str))
    # Convert '01/01/1867' format to '1867-01-01' format
    if date_str.count('/') == 2:
        dd, mm, yyyy = date_str.split('/')
        date_str = yyyy+'-'+mm+'-'+dd
    else:
        assert date_str.count('-') == 2
    return date_str

# Returns all edges between two nodes in their temporal ordering
def get_all_edges_in_temporal_order(in_graph, node_a, node_b):
    # 1) Get all edges in packed form,
    #    i.e. edge data contains lists like:
    #    {'weight': 2, 'dates': ['1875-10-20', '1876-6-14'], 
    #     'topics': ['varalised tehingud', 'varalised tehingud'], 
    #     'protocols': ['26899', '27058'], 
    #     'case_ids': ['65', '75']}
    all_edges_packed = []
    a_outgoing = list( in_graph.out_edges(node_a, data=True) )
    for edge in a_outgoing:
        assert node_a == edge[0]
        if edge[1] == node_b:
            all_edges_packed.append( edge )
    a_ingoing = list( in_graph.in_edges(node_a, data=True) )
    for edge in a_ingoing:
        assert node_a == edge[1]
        if edge[0] == node_b:
            assert len(edge) == 3
            all_edges_packed.append( edge )
    # 2) Unpack the edges and convert dates to ISO format;
    all_edges_unpacked = []
    for edge in all_edges_packed:
        packed_item_keys = [k for k in edge[2].keys() if k != 'weight']
        for i in range( int(edge[2]['weight']) ):
            new_edge_data = dict()
            new_edge_data['weight'] = 1.0
            for k in packed_item_keys:
                items = edge[2][k]
                assert len(items) == int( edge[2]['weight'] )
                new_value = items[i]
                if k == 'dates':
                    new_value = convert_date_value_to_iso_format(new_value)
                new_edge_data[k] = new_value
            new_edge = (edge[0], edge[1], new_edge_data)
            all_edges_unpacked.append( new_edge )
    # 3) Sort edges by dates
    return sorted(all_edges_unpacked, key=lambda x:x[2]['dates'], reverse=False)
    

# Find TOP N conflict pairs (pairs of persons suing/defending several times)
def top_suing_defending_pairs( in_graph, n = 5):
    node_data = { k:v for (k, v) in in_graph.nodes(data=True) }
    pairs_with_weights = []
    seen_pairs = set()
    for node_a in in_graph.nodes():
        node_a_out = list(in_graph.out_edges(node_a, data=True))
        for node_b in in_graph.nodes():
            if node_a != node_b:
                total_relations = 0
                node_b_out = list(in_graph.out_edges(node_b, data=True))
                for edge in node_a_out:
                    if edge[1] == node_b:
                        assert edge[0] == node_a
                        total_relations += int( edge[2]['weight'] )
                for edge in node_b_out:
                    if edge[1] == node_a:
                        assert edge[0] == node_b
                        total_relations += int( edge[2]['weight'] )
                if total_relations > 0:
                    pair_key = '_'.join(sorted([node_a, node_b]))
                    if pair_key not in seen_pairs:
                        pairs_with_weights.append( (total_relations, node_a, node_b) )
                        seen_pairs.add( pair_key )
    print()
    print( f'Konfliktipaaride TOP {n} graafi põhjal: ' )
    sorted_pairs_with_weights = sorted(pairs_with_weights, key=lambda x:x[0], reverse=True)
    for pair in sorted_pairs_with_weights[:n]:
        hageja = pair[1]
        kostja = pair[2]
        print( f'{hageja} ja {kostja}, kohtuskäimisi: {int(pair[0])} ' )
        print( f'  A: {hageja} {node_data[hageja]}' )
        print( f'  B: {kostja} {node_data[kostja]}' )
        edges_chronologically = get_all_edges_in_temporal_order(in_graph, hageja, kostja)
        for pair in edges_chronologically:
            label_1 = f'A' if pair[0] == hageja else f'B'
            label_2 = f'A' if pair[1] == hageja else f'B'
            print(f'     {pair[2]["dates"]}:  {label_1} -> {label_2}  teema: {pair[2]["topics"]!r}   protokoll: {pair[2]["protocols"]}  istung: {pair[2]["case_ids"]} ')
        print()

# Finds a number of edges if the given graph would be a full graph
def find_number_of_edges_in_a_full_graph( reference_graph ):
    nodes = len(reference_graph.nodes)
    return nodes*(nodes-1)

# Finds the percentage of edges this graph has from the full graph edges
def find_percentage_from_the_full_graph_edges( reference_graph ):
    full_graph_edges = find_number_of_edges_in_a_full_graph(reference_graph)
    this_edges = len(reference_graph.edges)
    percentage = (this_edges*100.0)/full_graph_edges
    return f'{percentage:.3f}%'

# Constructs a random directed graph with the same number of edges and arcs 
# as in the reference graph
def construct_random_directed_graph( reference_graph, seed=1 ):
    random.seed(seed)
    random_graph = nx.DiGraph()
    all_nodes = []
    for node in reference_graph.nodes:
        assert not random_graph.has_node(node)
        random_graph.add_node(node)
        all_nodes.append(node)
    # Generate all possible edges (full graph)
    possible_edges = []
    for uid in range(len(all_nodes)):
        for vid in range(uid+1, len(all_nodes)):
            u = all_nodes[uid]
            v = all_nodes[vid]
            possible_edges.append((u,v))
            possible_edges.append((v,u))
    random.seed(seed)
    # Pick randomly the same amount of edges as in the 
    # initial graph
    random_pick = random.sample(possible_edges, \
                                k=len(reference_graph.edges))
    for (a, b) in random_pick:
        random_graph.add_edge(a, b)
    assert len(random_graph.nodes) == len(reference_graph.nodes)
    assert len(random_graph.edges) == len(reference_graph.edges)
    return random_graph

# Constructs a new graph by adding randomly the given amount of edges to
# the reference_graph. Only missing edges will be added.
def add_randomly_edges( reference_graph, amount, seed=1 ):
    # Make a copy from the reference graph
    # (only need to copy nodes and edges, nothing more)
    random_graph = nx.DiGraph()
    all_nodes = []
    all_edges = []
    for node in reference_graph.nodes:
        assert not random_graph.has_node(node)
        random_graph.add_node(node)
        all_nodes.append(node)
    for u,v in reference_graph.edges:
        random_graph.add_edge(u,v)
        all_edges.append((u,v))
    # Generate all missing edges
    missing_edges = []
    for uid in range(len(all_nodes)):
        for vid in range(uid+1, len(all_nodes)):
            u = all_nodes[uid]
            v = all_nodes[vid]
            if not random_graph.has_edge(u,v):
                missing_edges.append((u,v))
            if not random_graph.has_edge(v,u):
                missing_edges.append((v,u))
    random.seed(seed)
    # Pick randomly the given amount of missing edges
    # and add to the graph
    random_pick = random.sample(missing_edges, k=amount)
    for (a, b) in random_pick:
        random_graph.add_edge(a, b)
    assert len(random_graph.nodes) == len(reference_graph.nodes)
    assert len(random_graph.edges) == len(all_edges) + amount
    return random_graph

# Constructs a new graph by increasing randomly the number of edges 
# in the reference_graph to the given percentage of edges from the full graph
def increase_edge_amount_to_percentage(reference_graph, percentage, seed=1):
    full_graph_edges = find_number_of_edges_in_a_full_graph(reference_graph)
    cur_percentage = (len(reference_graph.edges)*100.0)/full_graph_edges
    assert len(reference_graph.edges) < full_graph_edges
    assert cur_percentage < percentage
    target_amount = int(full_graph_edges*percentage/100.0)
    return add_randomly_edges(reference_graph, target_amount, seed=seed)

# Finds and outputs TOP N nodes sorted by local clustering coefficient
# Local Clustering Coefficient ~~ is the fraction of pairs of the node's connections that are connected with each other. 
# [ somehow indicates the "communication density" around the node, but does not necessarily indicate that the node 
# itself is important -- it may just be connected to important nodes ...]
# See also: https://www.datacamp.com/community/tutorials/social-network-analysis-python
def top_local_clustering_coefficient( in_graph, n = 5):
    clustering_coefficients = []
    for node in in_graph.nodes:
        clustering_coefficients.append( (node, nx.clustering(in_graph, node) ) )
    clustering_coefficients = sorted(clustering_coefficients, key=lambda x:x[1], reverse=True)
    print()
    print('''* Local Clustering Coefficient:''')
    print(f'Average clustering coefficient:  {nx.average_clustering(in_graph)} ')
    print(f'EXPLORING REARRANGEMENT -- randomly redistribute edges in graph:')
    # Explore graph rearrangement
    # Redistribute graph's edges randomly, while keeping the same number of edges
    # Perform experiment 5 times, with different seed values
    for seed_value in [1, 2, 3, 4, 5]:
        print(f'Average clustering coefficient (RANDOM EDGES, seed={seed_value}): {nx.average_clustering(construct_random_directed_graph(in_graph,seed=seed_value))} ')
    print()
    # Explore graph enlargement
    # Add randomly % of edges from the full graph
    print(f'EXPLORING GRAPH ENLARGEMENT by adding edges randomly:')
    for from_full in [1, 5, 10]:
        print(f'Average clustering coefficient (ADD RANDOM EDGES UNTIL {from_full}% of FULL GRAPH EDGES): {nx.average_clustering(increase_edge_amount_to_percentage(in_graph,from_full,seed=1))} ')
    print()
    print(f'TOP {n} nodes sorted by local clustering coefficient (lcc): ')
    node_data = { k:v for (k, v) in in_graph.nodes(data=True) }
    for (node, coefficient) in clustering_coefficients[:n]:
        kostmisi = in_graph.in_degree(node,  weight="weight")
        hagemisi = in_graph.out_degree(node, weight="weight")
        print( f'  id = {node}    lcc: {coefficient:0.3f}   kostmisi: {kostmisi}   hagemisi: {hagemisi}    {node_data[node]}' )
        if kostmisi > 0:
            print( f'              kost. teemad:', end=' ')
            for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='IN'):
                print( f'{teema!r} {sagedus}', end='  ')
        if hagemisi > 0:
            if kostmisi > 0:
                print()
            print( f'              hage. teemad:', end=' ')
            for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='OUT'):
                print( f'{teema!r} {sagedus}', end='  ')
        print()
    print()

# Finds and outputs TOP N nodes sorted by betweenness centrality
# The Betweenness Centrality ~~ represents the frequency at which a point occurs on the geodesic (shortest paths) that 
# connected pair of points. The nodes with high betweenness centrality play a significant role in the communication/information 
# flow within the network. The nodes with high betweenness centrality can have a strategic control and influence on others.
# The Degree Centrality ~~ for a node v is the fraction of nodes it is connected to.
# See also: https://www.datacamp.com/community/tutorials/social-network-analysis-python
def top_betweenness_and_degree_centrality( in_graph, n = 5):
    node_data = { k:v for (k, v) in in_graph.nodes(data=True) }
    betweenness_centrality = nx.algorithms.centrality.betweenness_centrality( in_graph )
    print()
    print('''* The Betweenness Centrality:''')
    mean_betweenness_centrality = mean([v for v in betweenness_centrality.values()])
    above_avg = len([v for v in betweenness_centrality.values() if v > mean_betweenness_centrality])
    print(f'  Average:  {mean_betweenness_centrality}')
    print(f'  {above_avg*100.0/len(betweenness_centrality.values()):.3f}% nodes above avg')
    # Adding weights changes results only slightly:
    #betweenness_centrality_weighted = nx.algorithms.centrality.betweenness_centrality( in_graph, weight='weight' )
    #mean_betweenness_centrality_weighted = mean([v for v in betweenness_centrality_weighted.values()])
    #above_avg_weighted = len([v for v in betweenness_centrality_weighted.values() if v > mean_betweenness_centrality_weighted])
    #print(f'  {above_avg_weighted*100.0/len(betweenness_centrality_weighted.values()):.3f}% nodes above avg (weighted betweenness centrality)')
    print()
    print('''* The Degree Centrality:''')
    degree_centrality = nx.algorithms.centrality.degree_alg.degree_centrality( in_graph )
    mean_degree_centrality = mean([v for v in degree_centrality.values()])
    above_avg = len([v for v in degree_centrality.values() if v > mean_degree_centrality])
    print(f'  Average:  {mean_degree_centrality}')
    print(f'  {above_avg*100.0/len(degree_centrality.values()):.3f}% nodes above avg')
    print()
    print(f'TOP {n} nodes sorted by betweenness centrality: ')
    for node in list(sorted(betweenness_centrality.keys(), key=betweenness_centrality.get, reverse=True))[:n]:
        kostmisi = in_graph.in_degree(node,  weight="weight")
        hagemisi = in_graph.out_degree(node, weight="weight")
        print( f'  id = {node}    betweenness_centrality: {betweenness_centrality[node]}   kostmisi: {kostmisi}   hagemisi: {hagemisi}    {node_data[node]}' )
        if kostmisi > 0:
            print( f'              kost. teemad:', end=' ')
            for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='IN'):
                print( f'{teema!r} {sagedus}', end='  ')
        if hagemisi > 0:
            if kostmisi > 0:
                print()
            print( f'              hage. teemad:', end=' ')
            for (teema, sagedus) in get_node_topics_count( in_graph, node, degree_type='OUT'):
                print( f'{teema!r} {sagedus}', end='  ')
        print()
    print()
    print()

# Makes a termplot about time periods between the first and the last 
# court case of a person
def node_temporal_statistics( in_graph ):
    nodes_by_period = defaultdict(int)
    c = 0
    for (node, data) in in_graph.nodes(data=True):
        # Get all dates from ingoing and outgoing relations
        in_edges = list(in_graph.in_edges(node, data=True))
        in_dates = [ date for edge in in_edges for date in edge[2]['dates']]
        out_edges = list(in_graph.out_edges(node, data=True))
        out_dates = [ date for edge in out_edges for date in edge[2]['dates']]
        all_dates = in_dates + out_dates
        # Normalize dates:  '1867-1-1' --> '1867-01-01'
        all_dates = [re.sub('-(\d)-', '-0\\1-', re.sub('-(\d)$', '-0\\1', date)) for date in all_dates]
        # Fetch unique dates and sort them chronologically
        all_dates = sorted(list(set(all_dates)))
        period = 'XXXX'
        delta = None
        if len(all_dates) == 1:
            period = '1 päev'
        elif len(all_dates) > 1:
            start = dateutil.parser.parse( all_dates[0] )
            end   = dateutil.parser.parse( all_dates[-1] )
            delta = relativedelta(end, start)
            if delta.years > 1:
                period = f'> 1 aasta(t)'
            if delta.years == 1:
                period = f'~ 1 aasta'
            elif delta.months > 6:
                period = f'> pool aastat'
            elif delta.months == 6:
                period = f'~ pool aastat'
            elif delta.months > 1:
                period = f'< pool aastat'
            elif delta.months == 1:
                period = f'~ 1 kuu'
            elif delta.days > 0:
                period = f'< 1 kuu'
        nodes_by_period[period] += 1
        c += 1
    labels = []
    counts = []
    for (key, value) in sorted(nodes_by_period.items(), key=lambda x:x[1], reverse=True):
        labels.append( key )
        counts.append( value )
    fig = tpl.figure()
    fig.barh(
        counts,
        labels,
        force_ascii=True
    )
    fig.show()

# Makes a termplot about the distribution of court cases over years
def make_dates_termplot( count_dist, sorted_by_date=True ):
    years_sorted  = sorted(count_dist.keys(),key=lambda x:int(x))
    first_year    = years_sorted[0]
    last_year     = years_sorted[-1]
    assert first_year.isdigit(), f'(!) Unexpected first_year value {first_year}'
    assert last_year.isdigit(), f'(!) Unexpected last_year value {last_year}'
    bucket_labels = [ first_year ]
    while True:
        cur_year = bucket_labels[-1]
        if cur_year == last_year:
            break
        else:
            bucket_labels.append( str(int(cur_year) + 1) )
    labels_and_counts = []
    for key in bucket_labels:
        if key in count_dist.keys():
            labels_and_counts.append( [key, count_dist.get(key)] )
        else:
            labels_and_counts.append( [key, 0] )
    if not sorted_by_date:
        labels_and_counts = sorted( labels_and_counts, key=lambda x:int(x[1]), reverse=True )
    labels = []
    counts = []
    for [key, value] in labels_and_counts:
        labels.append( key )
        counts.append( value )
    fig = tpl.figure()
    fig.barh(
        counts,
        labels,
        force_ascii=True
    )
    fig.show()

# ===================================================================
#   Output generation and formatting
# ===================================================================

# Converts graph to the graphml format. Basically, we just need to get rid 
# of lists in the edge attribute values ...
def convert_to_graphml_format( graph, apply_set_for_ambiguous=False ):
    if apply_set_for_ambiguous:
        print('Applying set on ambiguous edge attribute values for merging ...')
    new_attrib_values = {}
    for edge in graph.edges(data=True):
        u, v = edge[:2]
        edge_data = edge[2]
        assert isinstance(edge_data, dict)
        for k in edge_data.keys():
            if k not in new_attrib_values:
                new_attrib_values[k] = {}
            val = edge_data.get(k)
            if isinstance(val, list):
                if len(val) == 1:
                    new_attrib_values[k][(u,v)] = val[0]
                else:
                    if not apply_set_for_ambiguous:
                        new_attrib_values[k][(u,v)] = '|'.join(val)
                    else:
                        new_attrib_values[k][(u,v)] = '|'.join(list(set(val)))
            else:
                new_attrib_values[k][(u,v)] = val
    # Update the graph with new edge attributes
    for attr in new_attrib_values.keys():
        nx.classes.function.set_edge_attributes( graph, new_attrib_values[attr], name=attr )


# Converts graph to the dynamic graphml format. Basically, adds start dates and end dates
# to the each of the nodes ...
def convert_to_dynamic_graphml_format( graph, convert_date_format=True ):
    new_attrib_values = {}
    for node in graph.nodes(data=False):
        # Get all dates from ingoing and outgoing relations
        # Note: the format depends whether convert_to_graphml_format() 
        # has been previously applied or not ...
        in_edges = list(graph.in_edges(node, data=True))
        in_dates = []
        for edge in in_edges:
            if isinstance( edge[2]['dates'], str):
                in_dates = edge[2]['dates'].split('|')
            elif isinstance( edge[2]['dates'], list):
                in_dates = [date for date in edge[2]['dates']]
            else:
                raise Exception('(!) Unexpected date format: {}'.format(edge[2]['dates']))
        out_edges = list(graph.out_edges(node, data=True))
        out_dates = []
        for edge in out_edges:
            if isinstance( edge[2]['dates'], str):
                out_dates = edge[2]['dates'].split('|')
            elif isinstance( edge[2]['dates'], list):
                out_dates = [date for date in edge[2]['dates']]
            else:
                raise Exception('(!) Unexpected date format: {}'.format(edge[2]['dates']))
        all_dates = in_dates + out_dates
        # Normalize dates:  '1867-1-1' --> '1867-01-01'
        all_dates = [re.sub('-(\d)-', '-0\\1-', re.sub('-(\d)$', '-0\\1', date)) for date in all_dates]
        # Fetch unique dates and sort them chronologically
        all_dates = sorted(list(set(all_dates)))
        if convert_date_format:
            all_dates = [(d.split('-')[-1])+'/'+(d.split('-')[1])+'/'+(d.split('-')[0]) if len(d.split('-'))==3 else d for d in all_dates]
        start_date = all_dates[0]
        end_date   = all_dates[-1]
        if 'start' not in new_attrib_values.keys():
            new_attrib_values['start'] = {}
        if 'end' not in new_attrib_values.keys():
            new_attrib_values['end'] = {}
        new_attrib_values['start'][node] = start_date
        new_attrib_values['end'][node]   = end_date
    # Update the graph with new node attributes
    for attr in new_attrib_values.keys():
        nx.classes.function.set_node_attributes( graph, new_attrib_values[attr], name=attr )

# ===================================================================
#   Main
# ===================================================================

if __name__ == "__main__":
    if len(sys.argv) > 1:
        fname = sys.argv[1]
        if not (fname.lower()).endswith('.csv'):
            raise ValueError('(!) Invalid input file type. Expected CSV file.')
        aggregated_top_outgoing = False
        # Collect other cmd line parameters
        for arg in sys.argv[1:]:
            if arg == fname:
                continue
            if 'aggregate' in arg.lower():
                # Collect aggregated TOP outgoing edges statistics
                aggregated_top_outgoing = True
        # Parse data and get a list of court cases
        items = import_data_from_csv( fname, decode_data=True, compress_data=True )
        print('Items loaded: ', len(items))
        # Extract all unique participants from court cases
        kohtulised = dict()
        mehed  = 0
        naised = 0
        hagejad_top = defaultdict(int)
        kostjad_top = defaultdict(int)
        ajad        = defaultdict(int)
        for item in items:
            hagejad = item['hagejad']
            for h in hagejad:
                hagejad_top[h.id] += 1
                if h.id not in kohtulised:
                    kohtulised[h.id] = [h]
                    if h.sugu == 'mees':
                        mehed += 1
                    else:
                        naised += 1
                else:
                    if h not in kohtulised[h.id]:
                        kohtulised[h.id].append( h )
                    old_entry = kohtulised[h.id][-1]
                    check_for_conflicting_kohtuline( h, old_entry )
            kostjad = item['kostjad']
            for k in kostjad:
                kostjad_top[k.id] += 1
                if k.id not in kohtulised:
                    kohtulised[k.id] = [k]
                    if k.sugu == 'mees':
                        mehed += 1
                    else:
                        naised += 1
                else:
                    if k not in kohtulised[k.id]:
                        kohtulised[k.id].append( k )
                    old_entry = kohtulised[k.id][-1]
                    check_for_conflicting_kohtuline( k, old_entry )
            if len(item['year']) > 0 and len(item['month']) > 0:
                #date = '-'.join([item['year'], f'{int(item["month"]):02d}', f'{int(item["day"]):02d}'])
                #date = '-'.join([item['year'], f'{int(item["month"]):02d}'])
                date = '-'.join([item['year']])
            else:
                #date = 'XXXX-XX'
                date = 'XXXX'
            ajad[ date ] += 1
        print()
        print('='*100)
        print()
        print('Juhtumite ajaline jaotus: ')
        make_dates_termplot( ajad )
        print()
        print('Erinevaid juhtumeid:       ', len(items) )
        print('Erinevaid kohtulisi kokku: ', len(kohtulised.keys()), f'(sh mehi {mehed} ja naisi {naised})' )
        
        graaf = build_graph( items, kohtulised, apply_set_for_ambiguous=True  )
        print()
        print(graaf)
        print(f'Number of edges: {len(graaf.edges)} ({find_percentage_from_the_full_graph_edges(graaf)} of full graph edges)')

        top_outgoing_and_ingoing( graaf, n = 10 )
        top_repeated_outgoing_and_ingoing( graaf, n = 10 )
        if aggregated_top_outgoing:
            top_aggregated_outgoing( graaf, n = 10 )
        top_local_clustering_coefficient( graaf, n = 10 )
        top_betweenness_and_degree_centrality( graaf, n = 10 )
        top_suing_defending_pairs( graaf, n = 10 )
        
        print()
        print('Kohtuskäimise ajaline periood (isiku esimese ja viimase protsessi vahele jääv periood):')
        node_temporal_statistics( graaf )
        
        convert_to_graphml_format( graaf )
        
        outfname = (fname.replace(' ', '_')).replace('.csv', '.graphml')
        print()
        print('Writing graph to {} ...'.format(outfname))
        nx.readwrite.graphml.write_graphml(graaf, outfname, encoding='utf-8', prettyprint = True)
        
        convert_to_dynamic_graphml_format( graaf, convert_date_format=True )
        
        outfname_dynamic = (fname.replace(' ', '_')).replace('.csv', ' dynaamiline.graphml')
        print()
        #print('Writing dynamic graph to {} ...'.format(outfname_dynamic))
        #nx.readwrite.graphml.write_graphml(graaf, outfname_dynamic, encoding='utf-8', prettyprint = True)
        
    else:
        print('(!) Input missing! Please give CSV file as input.')
