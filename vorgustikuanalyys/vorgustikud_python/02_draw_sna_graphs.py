#
#   Draws graph's nodes and arcs in temporal order and suing order.
#
#   Default representation:
#   *) node's y position dependes on its outgoing arcs: the more outgoing 
#      arcs the node has, the higher it will be 
#      (~ persons who sue more will appear higher)
#      (~ persons who are more sued against will appear lower)
#   *) x-axis is a timeline;
#   *) node's x position dependes on time periods the person was in court: 
#      it is the middle point between person's first and last court case;
#
#   Flipped representation:
#   *) If x-axis and y-axis are filpped (see the parameter "flip"), then:
#      ~ persons who sue more will appear left;
#      ~ persons who are more sued against will appear right;
#      ~ y-axis is a timeline and node's y position dependes on time 
#        periods the person was in court:
#        it is the middle point between person's first and last court case;
#
#   This is an extended and improved version of the old script 
#   "02_draw_sna_graph.py":
#
#       *) by default, generates graphs for all figures of the paper.
#          use command line parameters to generate figures only for 
#          specific inputs, e.g. 
#          
#          python  02_draw_sna_graphs.py  "Kirna_vald_040521.graphml"
#          
#       *) positions of year labels have been corrected;
#       
#       *) male and female nodes are encoded by crosses (m) and dots (f);
#       
#       *) by default, node sizes are enlarged;
#
#       *) allows to flip x-axis and y-axis of the figure;
#
#       *) allows to make a zoomed graph: zoom in to a subgraph centered 
#          on specific nodes (specific for Kirna and Võnnu);
#       
#       *) pass parameter "label" via command line to draw ID labels to 
#          all nodes of the graph (for detailed exploring);
#
#   Requirements:
#   * numpy
#   * networkx >= 2.5
#   * matplotlib
#   * python-dateutil
#

import re
import csv
import sys
import os, os.path
import random

from collections import defaultdict

import numpy as np

import networkx as nx
import matplotlib.pyplot as plt

import dateutil.parser
from dateutil.relativedelta import relativedelta

output_dir = 'joonised'

if output_dir is not None and not os.path.exists( output_dir ):
    os.makedirs( output_dir )

def find_dist_in_norm_days( delta ):
    '''Converts time period (delta) into days.'''
    if delta.years == 0 and delta.months == 0 and delta.weeks == 0 and delta.days == 0:
        return 1
    else:
        return delta.years*365 + delta.months*30 + delta.weeks*7 + delta.days


def fetch_node_dates( in_edges, out_edges ):
    '''
    Finds all dates (from both ingoing and outgoing arcs) of a node.
    Normalizes dates and returns them in chronological order.
    '''
    in_dates = []
    for edge in in_edges:
        if isinstance( edge[2]['dates'], str):
            in_dates = edge[2]['dates'].split('|')
        elif isinstance( edge[2]['dates'], list):
            in_dates = [date for date in edge[2]['dates']]
        else:
            raise Exception('(!) Unexpected date format: {}'.format(edge[2]['dates']))
    out_dates = []
    for edge in out_edges:
        if isinstance( edge[2]['dates'], str):
            out_dates = edge[2]['dates'].split('|')
        elif isinstance( edge[2]['dates'], list):
            out_dates = [date for date in edge[2]['dates']]
        else:
            raise Exception('(!) Unexpected date format: {}'.format(edge[2]['dates']))
    all_dates = in_dates + out_dates
    # Normalize dates:  '1867-1-1' --> '1867-01-01'
    all_dates = [re.sub('-(\d)-', '-0\\1-', re.sub('-(\d)$', '-0\\1', date)) for date in all_dates]
    all_dates = [re.sub('/(\d)/', '/0\\1/', re.sub('/(\d)$', '/0\\1', date)) for date in all_dates]
    # Convert '01/01/1867' to '1867-01-01' (otherwise we can't sort them ...)
    new_all_dates = []
    for date in all_dates:
        if date.count('/') == 2:
            dd, mm, yyyy = date.split('/')
            new_all_dates.append( yyyy+'-'+mm+'-'+dd )
        else:
            assert date.count('-') == 2
            new_all_dates.append( date )
    all_dates = new_all_dates
    # Fetch unique dates and sort them chronologically
    all_dates = sorted(list(set(all_dates)))
    return all_dates


def compute_temporal_layout( in_graph, nodelist=None, fix_earliest_and_latest_dates=True, flip=False, debug=True ):
    '''
    Computes temporal & suing order layout for a SNA graph.
    
    If flip=False (default), then draws layout where 
    timeline is on the x-axis / horizontal axis and 
    suing relations follow the y-axis / vertical axis.
    (~ persons who sue more will appear higher)
    (~ persons who are more sued against will appear lower)
    
    If flip=True, then draws layout where timeline is 
    on the y-axis / vertical axis and suing relations follow 
    th x-axis / horizontal axis.
    (~ persons who sue more will appear right)
    (~ persons who are more sued against will appear left)
    
    Returns computed node coordinates and year label coordinates.
    '''
    earliest_date = None
    latest_date   = None
    nodes_by_norm_days = defaultdict(int)
    start_dates = dict()
    end_dates   = dict()
    edges = dict()
    for (node, data) in in_graph.nodes(data=True):
        if nodelist is not None and node not in nodelist:
            continue
        # Get node's edges
        in_edges  = list(in_graph.in_edges(node, data=True))
        out_edges = list(in_graph.out_edges(node, data=True))
        if nodelist is not None:
            # Filter edges:
            in_edges = \
                [(u, v, d) for (u, v, d) in in_edges if u in nodelist and v in nodelist]
            out_edges = \
                [(u, v, d) for (u, v, d) in out_edges if u in nodelist and v in nodelist]
        edges[node] = [len(in_edges), len(out_edges)]
        # Get node's dates
        all_node_dates = fetch_node_dates( in_edges, out_edges )
        start = all_node_dates[0]
        end   = all_node_dates[-1]
        start_date = dateutil.parser.parse( start )
        end_date   = dateutil.parser.parse( end )
        # Record earliest/latest date
        if earliest_date is None:
            earliest_date = start_date
        if latest_date is None:
            latest_date = end_date
        if start_date < earliest_date:
            earliest_date = start_date
            #print('New eaerliest:', start_date, node, len(in_edges), len(out_edges))
        if latest_date < end_date:
            latest_date = end_date
        # Record start/end dates and time intervals
        start_dates[node] = start_date
        end_dates[node]   = end_date
        delta = relativedelta(end_date, start_date)
        norm_days = find_dist_in_norm_days( delta )
        nodes_by_norm_days[node] = norm_days
    # Find length of the maximal span (from the earliest date to the latest date)
    # Fix: Expand earliest and latest dates to corresponding year ends and beginnings
    if fix_earliest_and_latest_dates:
        # debug: initial time span:
        earliest_date_iso = earliest_date.strftime('%Y-%m-%d')
        latest_date_iso = latest_date.strftime('%Y-%m-%d')
        print(' Initial time span:', earliest_date_iso, latest_date_iso if latest_date_iso!=earliest_date_iso else '' )
        corrected_earliest_date = dateutil.parser.parse( '{}-01-01'.format( str(earliest_date.year) ) )
        corrected_latest_date   = dateutil.parser.parse( '{}-01-01'.format( str(latest_date.year+1) ) )
        earliest_date = corrected_earliest_date
        latest_date = corrected_latest_date
    max_delta = relativedelta(latest_date, earliest_date)
    longest_days_span = max_delta.years*365 + max_delta.months*30 + max_delta.weeks*7 + max_delta.days
    assert longest_days_span > 1.0
    #if longest_days_span < 1.0:
    #    # Fix longest span
    #    longest_days_span = 1
    x_space = 150
    x_unit = 2.0 / (longest_days_span * x_space)
    if fix_earliest_and_latest_dates:
        earliest_date_iso = earliest_date.strftime('%Y-%m-%d')
        latest_date_iso = latest_date.strftime('%Y-%m-%d')
        print(' *corrected time span:', earliest_date_iso, latest_date_iso if latest_date_iso!=earliest_date_iso else '' )
    print(' Longest_timespan_in_days:  ', longest_days_span, '  x_unit:', x_unit, 'flip:', flip)
    all_ingoing  = []
    all_outgoing = []
    for k in edges.keys():
        all_ingoing.append( edges[k][0] )
        all_outgoing.append( edges[k][1] )
    max_ingoing = max(all_ingoing)
    max_outgoing = max(all_outgoing)
    y_space = 25
    y_unit = 2.0 / ((max_ingoing+max_outgoing)*y_space)
    print(' Node_in_out_degrees_range: ', (max_ingoing*-1, max_outgoing), 'y_unit:', y_unit, 'flip:', flip)
    # Calculate temporal x-coordinates and court relationship y-coordinates for each node
    random.seed(1)
    coordinates = dict()
    min_base_y = 1.0
    for node in in_graph.nodes(data=False):
        if nodelist is not None and node not in nodelist:
            continue
        # Calculate horizontal placement
        start_date = start_dates[ node ]
        end_date   = end_dates[ node ]
        length_in_days = nodes_by_norm_days[node]
        # Find distance from the earliest date
        delta = relativedelta(start_date, earliest_date)
        norm_days = find_dist_in_norm_days( delta )
        base_x = -1.0 + (x_unit*norm_days*x_space) + ((x_unit*x_space*length_in_days)/2)
        # Calculate vertical placement
        ingoing_arcs = edges[node][0]
        outgoing_arcs = edges[node][1]
        base_y_ingoing = -1.0*(ingoing_arcs*y_unit*y_space)
        base_y_outgoing = 1.0*(outgoing_arcs*y_unit*y_space)
        base_y = 0.0 + base_y_ingoing + base_y_outgoing + random.uniform(y_unit*y_space*-1.0, y_unit*y_space)
        min_base_y = base_y if min_base_y > base_y else min_base_y
        if not flip:
            # x-timeline, y-relations
            coordinates[node] = np.array([base_x, base_y])
        else:
            # x-relations, y-timeline
            coordinates[node] = np.array([base_y*-1.0, base_x*-1.0])
    # Calculate year temporal coordinates
    year_coordinates = dict()
    last_date = earliest_date
    i = last_date.year
    assert longest_days_span > 1.0
    #if longest_days_span == 1:
    #    last_date = dateutil.parser.parse(str(earliest_date.year)+'-01-01')
    #    latest_date = dateutil.parser.parse(str(earliest_date.year+1)+'-01-01')
    #    i = last_date.year + 1
    while i <= latest_date.year:
        delta = relativedelta( dateutil.parser.parse(str(i)+'-01-01'), last_date )
        norm_days = find_dist_in_norm_days( delta )
        if str(i-1) not in year_coordinates:
            if not flip:
                # x-timeline
                year_coordinates[str(i)] = (-1.0 + (x_unit*x_space*norm_days), min_base_y - 0.5*y_unit*y_space)
            else:
                # y-timeline
                year_coordinates[str(i)] = (min_base_y - 0.5*y_unit*y_space, (-1.0 + (x_unit*x_space*norm_days))*-1.0)
        else:
            if not flip:
                # x-timeline
                year_coordinates[str(i)] = (year_coordinates[str(i-1)][0] + (x_unit*x_space*norm_days), min_base_y - 0.5*y_unit*y_space)
            else:
                # y-timeline
                year_coordinates[str(i)] = (min_base_y - 0.5*y_unit*y_space, (year_coordinates[str(i-1)][1] - (x_unit*x_space*norm_days)))
         #print(' year_coordinates >> ', i, year_coordinates[str(i)])
        last_date = dateutil.parser.parse(str(i)+'-01-01')
        i += 1
    return coordinates, year_coordinates


def fix_layout_for_labels( nodes_layeout, mplt_axis=None, x_correction=0.0, y_correction=0.0 ):
    '''
    Recomputes nodes_layeout for displaying labels in a customized position with respect to the nodes. 
    The customized position is determined by changing position of each node by x_correction and y_correction.
    This is required if we want to place labels on top of nodes or on bottom of nodes, because 
    nx.draw_networkx_labels interface does not provide a good support for such customization.
    '''
    new_layout = dict()
    if mplt_axis is not None:
        (xmin, xmax, ymin, ymax) = mplt_axis
        y_full_delta = ymax-ymin
        y_correction = y_full_delta * y_correction / 100.0
        x_full_delta = xmax-xmin
        x_correction = x_full_delta * x_correction / 100.0
    for node in nodes_layeout.keys():
        if node.endswith('_a'):
            new_layout[node] = nodes_layeout[node]
            continue
        old_pos = nodes_layeout[node]
        base_x = old_pos[0]
        base_y = old_pos[1]
        new_layout[node] = np.array([base_x+x_correction, base_y+y_correction])
    return new_layout


def draw_year_coordinates( in_graph, year_coordinates, ax=None, flip=False, font_size=24, mplt_axis=None, 
                           y_adjustment=1.0, x_adjustment=2.0, corrected_node_offsets=None, add_a_suffix=False ):
    '''
    Writes year labels to the graph. 
    
    If flip=False, assumes layout where timeline is on x-axis 
    (from left to right). Otherwise, assumes timeline being on 
    the y-axis (from up to down).
    
    Locations of year labels will be adjusted based on mplt_axis.
    '''
    nodelist = []
    colors = []
    coordinates = dict()
    if mplt_axis is not None:
        # Find what is the unit corresponding to 1% 
        # in x-axis and y-axis of the drawing
        (xmin, xmax, ymin, ymax) = mplt_axis
        y_full_delta = ymax-ymin
        y_percentage = y_full_delta * y_adjustment / 100.0
        x_full_delta = xmax-xmin
        x_percentage = x_full_delta * x_adjustment / 100.0
    for k in year_coordinates.keys():
        new_node_key = str(k)+'_a' if add_a_suffix else str(k)
        # Fix coordinates using the axis
        (year_x, year_y) = year_coordinates[k]
        if mplt_axis is not None:
            if not flip:
                # Recalculate y
                year_y = ymin + y_percentage
                # Recalculate x (do this only in 
                # very specific case, when we have 
                # an extreme zoom in)
                if len(year_coordinates.keys()) == 1:
                    if year_x < xmin:
                        year_x = xmin + x_percentage
                    elif year_x > xmax:
                        year_x = xmax - x_percentage
            else:
                # Recalculate x
                year_x = xmin + x_percentage
                # Recalculate y (do this only in 
                # very specific case, when we have 
                # an extreme zoom in)
                if len(year_coordinates.keys()) == 1:
                    if year_y < ymin:
                        year_y = ymin + y_percentage
                    elif year_y > ymax:
                        year_y = ymax - y_percentage
        coordinates[ new_node_key ] = (year_x, year_y)
        in_graph.add_node( new_node_key )
        nodelist.append( new_node_key )
        colors.append( "white" )
    nx.draw_networkx_labels( in_graph, pos = coordinates, ax=ax, font_size=font_size, labels = {n:n for n in nodelist} )


def expand_nodelist_with_data_gender_profession_and_marking( fname, in_graph, depth=None, aggregate=False, debug_print=False ):
    '''
    Expands graph's nodelist by making explicit data, and normalized gender, profession and marking information of each node.
    Returns a list of tuples (node, data, norm_gender, norm_profession, marking).
    norm_gender='_' and norm_profession='_'  marks a meta node (a year label).
    Marking information is very specific, depends on the file and what we want to "zoom in" for.
    '''
    nodelist = []
    exp_nodelist = []
    markings_found = 0
    central_node = ''
    for (node, data) in sorted(in_graph.nodes(data=True), key=lambda x:x[0]):
        # Extract gender
        sugu = data.get('sugu', 'mees')
        # Remove ambiguities
        if '|_' in sugu:
            sugu = sugu.replace('|_', '')
        elif '_|' in sugu:
            sugu = sugu.replace('_|', '')
        # Extract profession
        #
        # Kodeering:
        # 10 -- vallaametnik                          
        # 11 -- kõrtsmik                              
        # 12 -- karjamõisa rentnik v valitseja        
        # 13 -- metsavaht                             
        # 14 -- mõisnik, mõisarentnik, mõisapolitsei  
        # 15 -- mõisaametnik                          
        # 16 -- koolmeister                           
        #       
        #       kõik ülejäänud                        
        # 
        amet = data.get('amet', 'kõik ülejäänud')
        if amet == 'kõik ülejäänud|_':
            amet = 'kõik ülejäänud'
        elif amet == '_|kõik ülejäänud':
            amet = 'kõik ülejäänud'
        if node.endswith('_a'):
            sugu = '_'
            amet = '_'
        # Marking
        marking = '_'
        if 'kirna' in fname.lower():
            if 'kõrtsmik' in amet:
                marking = 'kõrtsmik'
                markings_found += 1
            elif 'karjamõisa' in amet:
                marking = 'karjamõisarentnik'
                markings_found += 1
        if 'võnnu' in fname.lower():
            if '1021' in node:
                marking = 'Jaan Jõk'
                markings_found += 1
        if debug_print:
            print( '>>', (node, sugu, amet, marking) )
        nodelist.append( node )
        exp_nodelist.append( (node, data, sugu, amet, marking) )
    if markings_found > 0 and depth is not None:
        print(f'Computing paths at depth {depth} for {markings_found} marked nodes...')
        if not aggregate:
            # yield one sub graph for every central node
            for nid, (node1, data, sugu, amet, marking) in enumerate( exp_nodelist ):
                if marking != '_' and nid:
                    central_node = f'{marking}_{node1}'
                    selected_nodelist = []
                    selected_exp_nodelist = []
                    # remember the node
                    selected_nodelist.append(node1)
                    selected_exp_nodelist.append( (node1, data, sugu, amet, marking) )
                    # compute distances from this node to all other nodes
                    for nid2, node_info2 in enumerate( exp_nodelist ):
                        if nid != nid2:
                            # TODO: optimize this ?!
                            s_path1 = None
                            if nx.has_path(in_graph, node1, node_info2[0]):
                                s_path1 = nx.shortest_path_length(in_graph, node1, node_info2[0])
                            s_path2 = None
                            if nx.has_path(in_graph, node_info2[0], node1):
                                s_path2 = nx.shortest_path_length(in_graph, node_info2[0], node1)
                            if (s_path1 is not None and s_path1 <= depth) or \
                               (s_path2 is not None and s_path2 <= depth):
                                # remember the node
                                selected_nodelist.append(node_info2[0])
                                selected_exp_nodelist.append( node_info2 )
                    assert len(selected_exp_nodelist) > 0
                    yield selected_nodelist, selected_exp_nodelist, central_node
        else:
            # Aggregate all central nodes into one and yield as one sub graph
            selected_nodelist = []
            selected_exp_nodelist = []
            selected_node_ids = set()
            central_node = 'k6ikkoos'
            for nid, (node1, data, sugu, amet, marking) in enumerate( exp_nodelist ):
                if marking != '_' and nid not in selected_node_ids:
                    # remember the node
                    selected_node_ids.add(nid)
                    selected_nodelist.append(node1)
                    selected_exp_nodelist.append( (node1, data, sugu, amet, marking) )
                    # compute distances from this node to all other nodes
                    for nid2, node_info2 in enumerate( exp_nodelist ):
                        if nid != nid2:
                            s_path1 = None
                            if nx.has_path(in_graph, node1, node_info2[0]):
                                s_path1 = nx.shortest_path_length(in_graph, node1, node_info2[0])
                            s_path2 = None
                            if nx.has_path(in_graph, node_info2[0], node1):
                                s_path2 = nx.shortest_path_length(in_graph, node_info2[0], node1)
                            if (s_path1 is not None and s_path1 <= depth) or \
                               (s_path2 is not None and s_path2 <= depth):
                                if nid2 not in selected_node_ids:
                                    # remember the node
                                    selected_node_ids.add(nid2)
                                    selected_nodelist.append(node_info2[0])
                                    selected_exp_nodelist.append( node_info2 )
            yield selected_nodelist, selected_exp_nodelist, central_node
    else:
        # yield full graph, without zoom in
        yield nodelist, exp_nodelist, central_node


def draw_all_graphs_from_file(input_file, output_file, flip=False, depth=None, draw_labels=False, aggregate=False, professions=False, 
                                                                   extreme_node_size=False, debug_print=False):
    '''
    Draws all graphs of the input_file based on the given parameters.
    Saves picture(s) into output_file(s). Alters output file name depending on the parameters.
    Parameters:
    * input_file             -- input .graphml file where graph data will be loaded.
    * output_file            -- output picture (PNG) file name. Can include path.
    * filp                   -- bool. If False (default), then x-axis is timeline, and y-axis is relations. Otherwise, x-axis is relations, and y-axis is timeline.
    * depth                  -- integer. If depth is given, zoom in to neighbourhood of specific nodes at the given depth. Only implemented for Kirna and Võnnu.
    * draw_labels            -- bool. Draw node labels
    * aggregate              -- bool. Zoom in and make many small graphs vs one big aggregate graph. Only implemented for Kirna.
    * professions            -- bool. Node shape: professions and statuses instead of male and female.
    * extreme_node_size      -- bool. Use node size 2000 instead of 600.
    * debug_print            -- bool. Use debugging print.
    '''
    if input_file is None or not os.path.exists(input_file):
        raise ValueError('(!) Missing or non-existent input file. Expected .graphml file as an input.')
    if output_file is None or len(output_file)==0:
        raise ValueError('(!) Missing or empty output file name. Expected a picture file name with PNG extension.')
    if not (output_file.lower()).endswith('.png'):
        raise ValueError('(!) Unexpected a picture file name {!r}. Expected a picture file name with PNG extension.'.format(output_file))
    print('='*80)
    print('-'*80)
    graph = nx.readwrite.graphml.read_graphml(input_file)
    print('Loaded SNA graph from file {!r}:'.format(input_file))
    print(graph)
    # Make multiple graphs, one for each file
    drawn_output_files = []
    for nodelist, exp_nodelist, central_node in expand_nodelist_with_data_gender_profession_and_marking(input_file, graph, 
                                                                                                                    depth=depth, 
                                                                                                                    aggregate=aggregate, 
                                                                                                                    debug_print=debug_print):
        print()
        print (f'Computing graph drawing for {len(nodelist)} nodes ...')
        temporal_layout, year_coordinates = compute_temporal_layout( graph, nodelist=nodelist, flip=flip )

        fig, ax = plt.subplots()
        
        if depth is not None and depth < 3 and not aggregate:
             # zoomed in graph: a snapshot of a subgraph centered on specific nodes
            fontsize = 24
            year_fontsize = 10
            if 'võnnu' in input_file.lower() and depth is not None and depth < 3:
                fontsize = 10
            if not flip:
                #fig.set_size_inches(11.7, 8.3)    # A4 size in inches
                fig.set_size_inches(16.5, 11.7)    # A3 size in inches
            else:
                #fig.set_size_inches(8.3, 11.7)    # A4 size in inches
                fig.set_size_inches(11.7, 16.5)    # A3 size in inches
            node_size=350; font_size=10; arrowsize=20; arstyle=None;
            if extreme_node_size and 'kirna' in input_file.lower():
                node_size=2000
                arrowsize = arrowsize*2
                arstyle = '-|>,head_length=.6'
        else:
            # full graph
            fontsize = 36 if not aggregate else 10
            if 'kirna' in input_file.lower() and depth is not None and depth < 3:
                fontsize = 36
            year_fontsize = 36
            if not flip:
                #fig.set_size_inches(46.8, 33.1)                # A0 size in inches   --> 2 MB files
                fig.set_size_inches(46.8*2, 33.1*2)             # A0 size*2 in inches --> 4 MB files and beyond
            else:
                #fig.set_size_inches(33.1, 46.8)                # A0 size in inches   --> 2 MB files
                fig.set_size_inches(33.1*2, 46.8*2)             # A0 size*2 in inches --> 4 MB files and beyond
            node_size=600; font_size=fontsize; arrowsize=20; arstyle=None;
            node_size=2000 if extreme_node_size else 600
            arrowsize = arrowsize*2 if extreme_node_size else arrowsize
            arstyle = '-|>,head_length=.6' if extreme_node_size else arstyle

        print('start_axis:', ax.axis())  # xmin, xmax, ymin, ymax
        
        assert depth is not None or len(nodelist) == len(temporal_layout)
        # Paint white nodes:
        # make all colors white 
        #white_colors = ['white' for node in nodelist ]
        #nx.draw_networkx_nodes(graph, pos = temporal_layout, nodelist=nodelist, node_color=white_colors, node_size=150)
        # https://matplotlib.org/stable/api/markers_api.html#module-matplotlib.markers
        node_color = 'black'
        if professions:
            # mark non-peasant nodes 
            officials_nodes = [ node for (node, n_data, n_sugu, n_amet, n_mark) in exp_nodelist if n_amet!='kõik ülejäänud' and n_amet!='_' ]
            node_color = 'black'
            nx.draw_networkx_nodes(graph, pos = temporal_layout, nodelist=officials_nodes, node_color=node_color, node_size=node_size, node_shape='X')
            peasant_nodes = [ node for (node, n_data, n_sugu, n_amet, n_mark) in exp_nodelist if n_amet=='kõik ülejäänud' or n_amet=='_']
            node_color = 'black'
            nx.draw_networkx_nodes(graph, pos = temporal_layout, ax=ax, nodelist=peasant_nodes, node_color=node_color, node_size=node_size, node_shape='o')
        else:
            # mark male nodes 
            male_nodes = [ node for (node, n_data, n_sugu, n_amet, n_mark) in exp_nodelist if n_sugu=='mees' ]
            node_color = 'black'
            nx.draw_networkx_nodes(graph, pos = temporal_layout, nodelist=male_nodes, node_color=node_color, node_size=node_size, node_shape='X')
            # mark female nodes 
            female_nodes = [ node for (node, n_data, n_sugu, n_amet, n_mark) in exp_nodelist if n_sugu!='mees' and n_sugu!='_' ]
            node_color = 'black'
            nx.draw_networkx_nodes(graph, pos = temporal_layout, ax=ax, nodelist=female_nodes, node_color=node_color, node_size=node_size, node_shape='o')
            
        # Select only edges appearing in nodelist
        edgelist=[]
        labelsdict=dict()
        edgelabelsdict=dict()
        edgeweights=dict()
        for (u,v,e_data) in graph.edges(data=True):
            if u in nodelist and v in nodelist:
                edgelist.append((u,v))
                labelsdict[u] = u
                labelsdict[v] = v
                if 'võnnu' in input_file.lower() and depth is not None and depth < 3:
                    if u != '1021':
                        del labelsdict[u]
                    if v != '1021':
                        del labelsdict[v]
                # fix by markings
                u_marking = [n_mark for (node, n_data, n_sugu, n_amet, n_mark) in exp_nodelist if node==u][0]
                v_marking = [n_mark for (node, n_data, n_sugu, n_amet, n_mark) in exp_nodelist if node==v][0]
                edgeweights[(u,v)] = e_data['weight']
                if 'võnnu' in input_file.lower() and depth is not None and depth < 3:
                    prev_weight = 0 if (v,u) not in edgeweights else edgeweights[(v,u)]
                    cur_weight  = e_data['weight']
                    if prev_weight+cur_weight > 1:
                        edgelabelsdict[(u,v)] = prev_weight + cur_weight
                if depth is not None and depth < 3:
                    for n, n_marking in [(u, u_marking), (v, v_marking)]:
                        if n_marking.startswith('kõrtsmik'):
                            #labelsdict[n] = f'kõrtsmik {n}' if not aggregate else f'kõr {n}'
                            labelsdict[n] = f'kõrtsmik {n}'
                        elif n_marking.startswith('karjamõisa'):
                            #labelsdict[n] = f'karjamõis {n}'
                            labelsdict[n] = f'karjamõisarentnik {n}'
                        elif n_marking.startswith('Jaan Jõk'):
                            labelsdict[n] = f'Jaan Jõk {n}'
                            labelsdict[n] = f'Jaan Jõk'
                        else:
                            if 'kirna' in input_file.lower():
                                # delete labels of other nodes
                                del labelsdict[n]
        assert len(edgelist) > 0
        nx.draw_networkx_edges(graph, pos = temporal_layout, 
                                      ax=ax, 
                                      edgelist=edgelist, 
                                      arrows=None, 
                                      arrowsize=arrowsize, 
                                      arrowstyle=arstyle )
        if len(edgelabelsdict.keys()) > 0:
            nx.draw_networkx_edge_labels(graph, pos = temporal_layout, ax=ax, edge_labels=edgelabelsdict,
                                                font_color='black', font_size=fontsize,\
                                                bbox=dict(boxstyle='round', ec=(1.0, 1.0, 1.0), fc=(1.0, 1.0, 1.0)), 
                                                verticalalignment='baseline' )
        if draw_labels:
            if depth is not None:
                # zoomed in graph: a snapshot of a subgraph centered on specific nodes
                y_correction = -3
                if aggregate:
                    y_correction = -0.8
                    y_correction = 1
                fixed_temporal_layout = fix_layout_for_labels( temporal_layout, mplt_axis=ax.axis(), y_correction=y_correction )
                nx.draw_networkx_labels( graph, pos = fixed_temporal_layout, labels=labelsdict, ax=ax, font_color='black', font_size=fontsize,\
                                         bbox=dict(boxstyle='round', ec=(1.0, 1.0, 1.0), fc=(1.0, 1.0, 1.0)), verticalalignment='baseline' )
            else:
                # node labels for full graph
                nx.draw_networkx_labels( graph, pos = temporal_layout, ax=ax, labels=labelsdict, 
                                         font_color='white', font_weight='bold',
                                         font_size=6,\
                                         verticalalignment='center' )
                
        y_adjustment = 1.0
        if depth == None:
            y_adjustment = 2.0
        draw_year_coordinates( graph, year_coordinates, ax=ax, flip=flip, font_size=year_fontsize, mplt_axis=ax.axis(), y_adjustment=y_adjustment )

        print('average clustering coefficient (**in whole graph):  ', nx.average_clustering(graph))
        print('final_axis:', ax.axis())  # xmin, xmax, ymin, ymax
        fig.tight_layout()
        #plt.show()
        out_fname = output_file
        if len(central_node) > 0:
            out_fname = out_fname.replace('.png', f'_väljavõte_{central_node}.png')
        elif depth is not None:
            out_fname = out_fname.replace('.png', f'_väljavõte.png')
        if draw_labels:
            out_fname = out_fname.replace('.png', f'_tahistega.png')
        if professions:
            out_fname = out_fname.replace('.png', f'_ametid.png')
        if extreme_node_size:
            out_fname = out_fname.replace('.png', f'_x2000.png')
        if flip:
            out_path, out_fname = os.path.split( out_fname )
            out_fname = 'pooratud_'+out_fname
            out_fname = os.path.join(out_path, out_fname)
        print(' Saving temporal graph to file: ', out_fname)
        
        fig.savefig( out_fname )
        plt.close()
        drawn_output_files.append(out_fname)
    return drawn_output_files



if __name__ == "__main__":
    # First, try to load parameters from command line (assuming single file analysis)
    input_file  = None   # input .graphml file where graph data will be loaded.
    output_file = None   # output picture (PNG) file name. Can include path.
    default_filp        = False  # bool. If False (default), then x-axis is timeline, and y-axis is relations. Otherwise, x-axis is relations, and y-axis is timeline.
    default_depth       = None   # integer. If depth is given, zoom in to neighbourhood of specific nodes at the given depth. Only implemented for Kirna and Võnnu.
    default_draw_labels = False  # Draw node labels.
    default_aggregate   = False  # Zoom in and make many small graphs vs one big aggregate graph. Only implemented for Kirna.
    default_professions = False  # Node shape: professions and statuses instead of male and female.
    default_extreme_node_size = False # Use node size 2000 instead of 600.
    drawn_graphs = []
    if len(sys.argv) > 1:
        # Collect cmd line parameters
        for arg in sys.argv:
            if arg == sys.argv[0]:
                continue
            # input file (and output picture file)
            if (arg.lower()).endswith('.graphml'):
                input_file = arg
                output_file = input_file.replace('.graphml', '_ajagraaf.png')
                if output_dir is not None and os.path.isdir( output_dir ):
                    output_file = os.path.join( output_dir, output_file )
            # graph depth (for zooming in)
            elif arg.isnumeric():
                default_depth = int( arg )
            # draw node labels
            elif 'label' in arg.lower():
                default_draw_labels = True
                print('Drawing labelled graph.')
            # (zoom in and make many small graphs vs one big aggregate graph; only works for Kirna)
            elif 'aggregate' in arg.lower():
                default_aggregate = True
            elif 'flip' in arg.lower():
                default_filp = True
            # "extremely big" node size
            elif 'extreme' in arg.lower():
                default_extreme_node_size = True
            # node shape: professions and statuses instead of male and female
            elif 'professions' in arg.lower():
                default_professions = True
        if input_file is not None:
            # If input file was provided, then draw graph only for the given file
            drawn_graphs = \
                draw_all_graphs_from_file(input_file, output_file, depth=default_depth, 
                                                                   draw_labels=default_draw_labels, 
                                                                   aggregate=default_aggregate, 
                                                                   professions=default_professions, 
                                                                   extreme_node_size=default_extreme_node_size )
    # If not input paremeters were given, then try use default settings to draw all graphs at once
    if len(drawn_graphs) == 0:
        # default settings for the paper
        default_settings = [ \
           {'in':'Kirna_vald_040521.graphml',        'out': 'joonis_6a_Kirna_ajagraaf_040521.png', 'params': {'extreme_node_size':True} },
           {'in':'Kirna_vald_040521.graphml',        'out': 'joonis_6b_Kirna_ajagraaf_040521.png', 'params': {'depth':1, 'aggregate': True, 'draw_labels': True, 'extreme_node_size':True}},
           {'in':'Võnnu_Jõesse_110221.graphml',      'out': 'joonis_7a_Võnnu_ajagraaf_110221.png', 'params': {'extreme_node_size':True}},
           {'in':'Võnnu_Jõesse_110221.graphml',      'out': 'joonis_7b_Võnnu_ajagraaf_110221.png', 'params': {'depth':1, 'draw_labels': True, 'extreme_node_size':True}},
           {'in':'Kiltsi_170922_parandatud.graphml', 'out': 'joonis_8_Kiltsi_ajagraaf_170922.png', 'params': {'extreme_node_size':True}},
           {'in':'Parila_170922_parandatud.graphml', 'out': 'joonis_9_Parila_ajagraaf_170922.png', 'params': {'extreme_node_size':True}},
        ]
        for setup in default_settings:
            flip        = default_filp
            depth       = default_depth
            aggregate   = default_aggregate
            professions = default_professions
            draw_labels = default_draw_labels
            extreme_node_size = default_extreme_node_size
            in_file  = setup['in']
            out_file = setup['out']
            if not os.path.exists(in_file):
                raise ValueError( ('(!) Cannot create graph from default input file {!r}: the file does not exist. '+\
                                   ' If .graphml files are missing, use the script 01_read_data_and_build_sna_graph.py '+\
                                   ' to get .graphml files from the input .csv files.').format(in_file) )
            if 'params' in setup:
                for key, value in setup['params'].items():
                    if key=='flip':
                        flip=value
                    if key=='depth':
                        depth=value
                    if key=='aggregate':
                        aggregate=value
                    if key=='draw_labels':
                        draw_labels=value
                    if key=='extreme_node_size':
                        extreme_node_size=value
            if output_dir is not None and os.path.isdir( output_dir ):
                out_file = os.path.join(output_dir, out_file)
            draw_all_graphs_from_file(in_file, out_file, flip=flip, depth=depth, 
                                                         draw_labels=draw_labels, 
                                                         aggregate=aggregate, 
                                                         professions=professions, 
                                                         extreme_node_size=extreme_node_size )

