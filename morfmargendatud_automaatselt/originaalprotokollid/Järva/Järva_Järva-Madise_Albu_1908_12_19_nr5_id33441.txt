1908 goda Dekabrja mesjatsa 19 dnja javilis v Alpskii volostnoi sud kr-ne Alpskoi volosti Jogan Jurjev Bauman i Adam Janov Mella, litšno izvestnõje semu volostnomu sudu i prosili o vnesenii v aktovuju knigu sego Suda slovesnago ih dogovora sledujuštšago soderžanija:
I - Lammasküla talukoha omanik Juhan Baumann rendib oma põllust poole wakamaad (200 ruutsülda) Adam Mõllale maja aluseks ja ajamaaks 25 aasta peale, arwates 1 Septembri k.p. 1908a. kuni 1 Septembri k.p. 1933 aasta.
II - Selle ülemal tähendatud maa tükki eest maksab Adam Mõlla Juhan Baumannile iga aasta 1 Septembril 6 (kuus) rubla renti iga eestulewa aasta peale ette.
III - Renditud maa tükk on juba Adam Mõllale kätte antud ja raja märkidega märgitud. Adam Mõllal on juba renditawa maa peal oma maja; üle jäedawat maad wõib ta wiljas pidada.
IV - Adam Mõllal ei ole mitte õigust oma maja wälja üürida, ega ka üürilisi sisse wõtta ilma maa omaniku lubata. Niisama ei ole temal ka õigust missugust kauplust asutada.
V - Kui Mõlla tahab oma maja ära müüa, siis on maa omanikul eesõigus osta, aga kui hinnaga kokku ei saa lepida, siis wõib mõne teisele müüa ära wiimiseks ehk ka paigale jäemiseks, kui maa omanik lubab.
VI - Kui Mõlla peaks enne ära surema, kui tema naine, siis jäeb naine tema asemikuks kõige õigustega ja kohustustega.
VII - Peale Adam Mõlla ja tema naise Miina surma peawad nende pärijad uue lepingu tegema ehk kui kokku ei lepi, siis maja ära wiima. Müümise kohta, kui tahawad maja ära müüa, maksawad need samad tingimised, mis Adam Mõllale 5 punktis on nimetatud.
VIII - Adam Mõllal on õigus maa omaniku kaewust wett wõtta.
IX - Kui see renditud maa tükk ühes Lammasküla kohaga uue omaniku kätte peaks minema, peab käesolew lepping maksma jäema kuni Adam Mõlla ja tema naise surmani.
Juhan Bauman [allkiri]
Adam Mella, a za negramotnostju jego i litšnoi o tom prosbe rospisalsja: Jüri Trakmann [allkiri]
Nastojaštšii dogovor po vnesenii v aktovuju knigu protšitan storonam i sobstvennorutšno podpisan kr-nami Alpskoi volosti Joganom Jurjevõm Bauman i za negramotnostju Adama Janova Mella i po litšnoi jego o tom prosbe kr-nom Alpskoi volosti Jurijem Jakovõm Trakmanom, litšno izvestnõmi semu volostnomu sudu. JaG [initsiaalid]
Predsedatelj suda: JaGrosmann [allkiri]
Tšlenõ: H Lindeman [allkiri]   M Seebek [allkiri] Ju. Valter  [allkiri]
Pisar: R.Reinvald [allkiri]
