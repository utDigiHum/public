Prottokoll sel 1sel Tetsembris 1882
Koos olid kohtu Paawanem: Jaan Reemann
Hans Simsohn
Mart Sopp
Kirjutaja H. Saarmu
№ 55.
Kohtu laua ette astus Reopalu karjamõisa perenaene  Mai Iob ja kaebas, et  Mari Pauli olla tema peale wale juttu rääkinud, et tema olla oma peremehe 13 naela wõid salaja ära müünud.
Mari Pauli  astus ette ja ütles: et tema olla seda juttu  Mihkel Tammanni kääst kuulnud.
M. Tammann astus ette ja ütles: et tema olla seda juttu kõrtsumees Jaagup Sanderi kääst kuulnud ja et  Ewa Sookael ja Hans Krabi olla seda pealt kuulnud, kui  J. Sander on seda wõi wargust temale rääkinud. 
Kõrtsumees J. Sander astus ette ja ütles, et tema ei ole seda juttu mitte  M. Tammannile rääkinud, ega tema pole seda juttu weel kuulnudki.
 Ewa Sookael ja Hans Krabi astusid ette ja ütlesid: et nemad ei ole seda mitte kuulnud, kui  Jaagup Sander on seda wõi warguse juttu Mihkel Tammannile rääkinud ja et nemad ei tea sest midagi tunnistust anda..
Mõistetud sai: et  Mihkel Tammann peab 1 Rubl. trahwi walla laekasse maksma, seepärast, et tema on seda tühja juttu Mai Iobi peale rääkinud.
Semõistmine sai kuulutud ja olid sellega rahul kaebaja  Mai Iob kui kaa tema wastane Mihkel Tammann.
On kaa täidetud. (1 Rubl. walla laekasse)
Kirna walla kohtu nimel,
Pääkohtumees: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Hans Simsohn 
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
