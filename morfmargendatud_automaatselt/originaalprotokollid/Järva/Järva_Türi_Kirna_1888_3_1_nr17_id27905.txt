№ 17
Prottokoll 1sel Märts 1888.
Kous olid kohtu Peawanem  Hans Pawelts.
Jaan Willewelt
 Hans Kreedeman
Kirjutaja J. Parrikas.
Kohto ette astus  Hans Reinkort ja ütles: et tema olla seda raha 13 Rbl, mis kasu lapse pärast tema üle on mõistetud,  Hendrek Saarmo kätte ära maksnud.
 Hendrek Saarmo astus ette ja ütles: et tema ei ole mitte üks koppik Hans Reinkorti käest wastu wõtnud. Nendasama mitte ühtegi raha, mis kohto laudas on mõistetud saanud.
Mõistetud sai: seda asja suurema kohto üle kuulamise alla anda, on ka prottokoll wälja antud.
Kirna walla kogukonna kohto nimel:
Peawanem:  Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
