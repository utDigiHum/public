Prottokoll sell 19mal Webruaris 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Mart Sopp
Hans Simsohn
Kirjutaja H. Saarmu
№ 11.
Kohtu laua ette astus peremees Jaan Laim ja kaebas, et  Rein Kredemann  on Nääri pääwa õhtu tema majasse tulnud, kui tema on kottu ära läinud ja olla tema perenaise kääst tule ära kustutand ja olla sealt ühe terwe leiwa, panni täie praetud liha, poole keedetud sea pead ja 2 Tsetwerikud linnasid ära warastanud ja et  Rein Mihkeltee on temale selle juures kaa abiks olnud ja et tema olla kaa linnased  Reinu kääst kätte saanud.
 Rein Kredemann astus ette ja ütles: et tema ei ole ise mitte neid linnasid warastanud, waid temal on teised mehed kaasas olnud ja nimelt:  Kustas Kuulberg, Jüri Särg, Kaarel ja Adu Kallmann ja et need olla selle linnase kotti tema reepeale pannud.
 Rein Mihkeltee astus ette ja ütles: et tema on koeruse pärast selle liha ja leiwa säält Kaasiksaarest ära toonud ja nende ülema nimetud seltsimeestega ühes koos ära söönud.
Kustas Kuulberg, Jüri Särg, Kaarel ja Adu Allmann astusid ette ja ütlesid: et nemad ei ole mitte seda linnase kotti Reinu ree peale pannud, ega kaa Rein Mihkelteega seda warastud liha söönud, et nemad sest kumagist wargusest midagi ei tea.
Mõistetud sai: et Rein Kredemann peab 3 R. walla laekasse trahwi maksma ja Rein Mihkeltee peab 2 Rubla walla laekasse trahwi maksma, ja 75 kop Jaan Laimile leiwa ja liha eest; aga Kustas Kuulberg Jüri Särg  Kaarel ja Adu Kallmann peawad iga üks 50 kop. kaa trahwi maksma, seepärast, et nemad on ööse Reinu Kredemanniga ümber hulkunud.
See mõistmine sai kuulutud ja olid sellega rahul kaebaja  Jaan Laim, kui kaa Rein Kredemann ja Rein Mihkeltee, Kustas Kuulberg, Kaarel ja Adu Kallmann ja Jüri Särg.
Täidetud. 7 Rubl. walla laeka heaks.
Kirna walla kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Hans Simson XXX
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
