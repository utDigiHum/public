№ 33
Prottokoll sell 1misel Augustil 1884.
Koos olid kohtu Peawanem: Jaan Reemann
Mart Sopp
Mihkel Ellmann
Kirjutaja H. Saarmu
Mõisa põlitsei poolt sai kaebdus ette toodud, et Tori walla mehed  Hans Tendermann ja  Jaan Titsu olla mõisal üks tük rukist ära tallanud, kelle kahju 3 Rubla wäärt olla ja et Tallitaja olla kaa seda kahju järele waatanud.
Tallitaja  Kustas Landberg   astus ette ja ütles: et mõisa põlitsei kaebdus tõsi on.
  Hans Tendermann ja  Jaan Titsu astusid ette ja ütlesid: et nemad olla seda joobnuse peaga teinud.
Mõistetud sai: et  Hans Tendermann ja  Jaan Titsu peawad kumgi 1 Rubla 50 kop. mõisale kahju tasumiseks maksma ja peale selle weel kumgi 1 Rubl. trahwi  walla laekasse, seepärast, et nemad on mõisa rukid ära tallanud. 
Seemõistmine sai kuulutud ja olid sellega rahul, kaebaja mõisa politsei, kui kaa  Hans Tendermann ja  Jaan Titsu.
On kaa täidetud. (2 Rubl. walla  laeka heaks.)
Kirna walla kogokonna kohtu nimel:
Pääwanem:  Jaan Reemann/allkiri/
Mart Sopp  /allkiri/
Mihkel Ellmann  
Kirja tunnistuseks kirjutaja: H. Saarmo /allkiri/
