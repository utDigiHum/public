Prottokoll sell 1misel Aprilis 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Mart Sopp
Hans Simsohn
Kirjutaja H. Saarmu
№ 20.
Kohtu laua ette astus Laupa walla mees  Rein Kiwi  ja kaebas, et temal on 5mal Märtsil  Kirna Wissuwere kõrtsu juures, ühed waljad hobuse pealt, paar kindid, pakk tubagast, üks piip, hööwel pakk rauaga ja 18 Rub. raha ära warastud; need rahad olnud 3 wiie Rublast ja üks 3 Rublane.
See wargus on 21 Rubla 70 kopik wäärt olnud.Hobuse waljad			2 Rubla 50 kopik		Paar kindid			30 kopik		Pakk tubagast			5 kopik		Piip			35 kopik		Hööwelpak			50 kopik		Puhast raha			18 Rubla		Summa			21 Rubla 70 koppik		
Tema olla kaa selle hööwel pakku Rein Mihkeltee ja piibu Jüri Särge kääst kätte saanud.
Rein Mihkeltee astus ette ja ütles: et tema olla selle hööwel pakku kõrtsust pingi pealt leidnud; ja et tema olla kaa näinud need  Rein Kiwi hobuse waljad ja 3 Rubl. raha  Hans Wanaisaaki  käes olewad.  Rein Kredeman olla kaa sääl juures olnud, kui Hans on sest raha wargusest temale rääkinud.
 Jüri Särg astus ette ja ütles: et tema olla selle piibu  Rein Mihkeltee kääst saanud.
 Hans Wanaisaak astus ette ja ütles: et tema olla  Rein Kiwi hobuse waljad ja 3 Rubla raha ära warastanud ja et Reinul ei ole mitte enam raha olnud, kui 3 Rubla ja et tema olla selle 3 Rubla ära raiskanud ja waljad lume sisse matnud ja lubas neid teine kord Kiwile tagasi anda; aga tubagast, kinnastest, ja sest puuduwast rahast ei pea tema mitte teadma. Ja et Rein Kredemann on ka seal juures olnud, kui tema sest raha wargusest Rein Mihkelteele on rääkinud.
Rein Kredemann astus ette ja ei salga seda hoopis ära. 
Rein Mihkeltee astus weel ette ja ütles: tema olla  Rein Kiwi piibu  J. Särge kätte annud, aga kust ta seda piipu on saanud, ei teada tema mitte.
Seepärast, et wargad seda puuduwad raha, se on: 15 Rub, tubagast ja kindid mitte üles ei tunnista, sai mõistetud: et see warguse asi jääb Hakenrehtri kohtu üle kuulamise alla.
On kaa Prottokoll wälja antud.
Kirna walla kogokonna kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Hans Simsohn XXX
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
