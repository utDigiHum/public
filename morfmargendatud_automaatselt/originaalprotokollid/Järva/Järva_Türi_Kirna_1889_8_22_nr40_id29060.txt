№ 40
Prottokoll 22sel Augustil 1889.
Koos oliwad kohtu peawanem:  Hans Paawelts
 Mihkel Simson
 Hans Ubak
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Mai Peialpas  ja kaebas, et  Jüri Ehrenwerth  olla tema käest 29 Rbl. raha laenanud. Aga nüüd ei pea Jüri Ehrenwerth temale mitte seda wõlga ära maksma.
 Jüri Ehrenwerth astus ette ja ei salganud seda mitte ära, waid ütles, et raha puuduse pärast ei olla temal mitte wõimalik olnud seda wõlga ära maksta.
Leppisiwad selle mooduga ära, et  Jüri Ehrenwerth maksab sest wõlast 1889. aasta sees 14 Rbl 50 kop. ja 1890 aasta sees 14 Rbl. 50 kop. Mai Peialpas'ile ära.
Mõistetud sai: et  Jüri Ehrenwerth peab oma lubamist truuiste ära täitma. 
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Mihkel Simson
 Hans Ubak
Kirja tunnistuseks Kirjutaja: Hans Paawelts /allkiri/
