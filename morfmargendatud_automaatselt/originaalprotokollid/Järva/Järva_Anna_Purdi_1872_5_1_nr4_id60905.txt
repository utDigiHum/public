4) Se moisa kirri ja selgem otsus mis sel 27mal Abril sisse tulli. Nimmelt nenta, Al nimmetud asja pallutakse Koggokonna kohtul ärra selletada
(1) Moisa sullane Mik Korjus on Jürri päewast satik töölt ärra jäenud, ilma et wallitsussega selle ülle middagi räkinud olleks. Kubja wata agga päe enne Jürri päewa ütlenud, et ennam ei tulla, ollen Herrale ülles üttelnud, Herra wastus on agga se, kes omma nimme ülles pannud, ärra minnemisse parrast, on ülles ütlend et Mik Korjusse nimme ei olle mitte kirjas egga pölle ka pärrast küündla päewa sest ühhelegi rääkinud, ei aasta selletamis se aial ka mitte ja tööristad kätte handmatta on peab siis omma ülles ütlemist ja ärra minnemest selgeks teggema.
Mik Korjus tunnistas minno terwis ei kanna sedda tööd tehja kui ma moisa kauplesin siis piddin tisleri toas tööl ollema agga nüüd ei petud, ja Herrale ütlesin Küündlapäe ülles tahhan ärra minna ja Herra ütles minne panne Opmanni jures nimmi kirja ma käisin 2 kord Opmanni ei olnud koddo siis ma ennam ei läinud
Selle peale moistis kohhus et Mik Korjus lähheb 4mal Mail tenistusse taggasi ja selletab moisa wallitsussega ommad asjad ärra.
(2) Pikkakülla karjatütruk Kai Käsper piddi minnewa sui sealt Purti wöetud saama, et perre sömakram olli laiale saatnud, palwete peale jäi weel sinna senni kui temmaga rahhul piddi oltama Jürri päewast satik piddi nüüd Purti tullema agga on ärra jäenud, ütles et olla teata hannud ärra minnemisse pärrast. Se teata handminne olli agga Martsi ku algo, ning sai siin kohhe ööltud, et sedda kuulta ei wöeta.
Nimmetud tütruk Kai Käsper on wallast wälja tenistusse ei tea kus läinud saab kohto poolt temma issale käsko hantud, et tedda ülles otsib ja kohto ette toob. seks 15mak Mai ku päewaks
(3) Sellisaare Jaak Holli on 13 1/2 teopäewa enne Jürri päewa wölga jätnud ja saab pallutud tedda neid rahhaga ärra maksta, lassa 50 koppik päe. Pärrast Jürripäewane näddal on ka wölga, ned tehkp eddes piddi ärra.
Jaak Holli tunnistas, minna  palgasin Eiwerrest Hans Newe ommale teomehheks ja maksin palga aastase teo päewate ette temmale wälja ja selle aasta ette maksis temmale jo ette 15 rupla rahha 1 Setwert rkkid ja 1 Setwert Kaero, et temma minno teo päewad piddi ärra teggema.
Hans Newe tunnistas minnul olli nörk hobbone selle müsin ärra nüüd ostas teise ja teen need wölga jäenud päewad ärra.
Selle asja sees moistis koggokonna kohhus, et 15mal Mail on tännawused päewad tassa ja lähhete nüüd kohhe Jaak Holli ja Hans Newe moisa ja selletate wanna aasta päewad ka ärra.
(4) Holli Mik Käsber on mönda käsko omma abbi teo pärrast sanud, agga siiski mitte ärra teinud, siis saab pallutud, temma käest se rahha pärrita 3 1/3 eina päewa, päe 30k. ja 6 wina kögi päewa päe 50 koppik sest teomees ommi pärrast Jürri päewa weel sees, ja hobone tallis 7 Korra päewa päe 15 koppik 5 1/2 Masina päewa päe 20 kop. ühte kokko 6 Rupla 15 Kop.
Mik Käsber tunnistas saatsin poisi wina kögi ei wöetud wasto, tütruk olli haige ei saand korrale
Selle asja sees moistis koggokonna kohhus, et Mik Käsber lähheb tänna moisa ja maksa need wölla päewad seal rahhaga ärra.
(Ja läks)
(5) Kagowerre Jürri Södil on rent maksmatta, saab pallutud tedda suntida teomees ja waim wälja sata ning 8 1/2 wanna aasta teopäewa, 3 eina päewa ja 2 1/2 Masina päewa rahhaga ärra maksa.
Ei olle kohtusse tellitud Jäeb polele.
(6) Seppa Jaan Risikamp on waimo päewad maksmatta saab pallutud tedda wail wälja sata suntida.
Ei olle kohtusse tellitud Jäeb polele.
(7) Saab pallutud Tallittajale teata hanta, et Haggendrehtli Herra mele tulletab, et kästud saaks, mis Ado Kuulbergi poia pärrast kästud on, ke Mäeküllas minnewa aasta teomees olli.
Tallittaja Mart Tougo läbbi sai Käsk hantud et sel 5mal Mail peab Ado Kuulberg Haggendrehtli kohto ees ollema.
(8) Jaan Wenno ja Hans Hawik on Kalmaste metsawaht kinni wötnud, kui moisa puud wankrel ollid puud on käes.
Ei olle kohtusse tellitud jäeb polele.
