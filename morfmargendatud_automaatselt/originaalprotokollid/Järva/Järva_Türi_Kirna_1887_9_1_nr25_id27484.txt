№ 25
Prottokoll 1.sel September 1887.
Koos olid kohtu Peawanem Hans Pawelts.
Jüri Ehrenwerth.
Jaan Willewelt.
Kirjutaja J. Parrikas.
Mõisa herra poolest sai walla kohtu läbi, Mõnnaku, Mäeperemehe Hans Palmbergi, ja Reinu peremehe Mart Kaermani käest küsitud, kas nemad ostawad  oma kohad, selle hinnaga ära, mis wõerad nende eest on kaubelnud.
 Hans Palmberg ja Mart Kaerman astusid ette ja ütlesid, et nemad selle hinna eest mitte ei wõi, neid kohtasid ära osta.
Otsus: Kui nemad oma eest õiguse pärast, seda tasumist on kätte saanud, mis seadus käsib, siis peawad nemad oma kohtatest wälja minema.
Kirna walla kogukonna kohtu nimel:
Peawanem:  Hans Pawelts /allkiri/
Kõrwamehed Jüri Ehrenwerth ja Jaan Willewelt
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
