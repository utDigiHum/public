№ 48
Prottokoll 17mal Oktober 1888.
Kous olid kohtu Peawanem  Hans Pawelts.
 Jaan Willewelt
Hans Kreedeman.
Kirjutaja J. Parrikas.
Mõisa politsei poolest sai  Juhan Hermansoni peale kaebatud, et tema olla härge söötmise tarwis teenistuse kaupa teinud, ega käsi raha wasta wõtnud, sepärast et palk selle aga Juhan Hermanson ei mitte teenistusse tulnud, nenda kui kaup tehtud olnud.
 Juhan Hermanson astus ette ja ütles: tema ei olla mitte täieliku kaupa weel ära teinud, ega käsi raha wasta wõtnud, sepärast et palk selle teenistuse jauks liiga wäha on. Ja tema olla ka seda nõudnud, et perre laudas süija saaks.
Mõisa politsei andis weel ette ja ütles: et Juhan Hermanson ei olla kaupa tegemise aego mitte sööki tellinud. Aga hommiko kui tööse tulnud siis hakkanud weel sööki tellima.
Mõistetud sai: et  Juhan Hermanson maksab 50 Cop trahwi walla laeka, se pärast, et tema teenistuse kaupa on teinud, aga mitte teenima läinud.
Se mõistmine sai kuulutud, ja olid sellega rahul.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
