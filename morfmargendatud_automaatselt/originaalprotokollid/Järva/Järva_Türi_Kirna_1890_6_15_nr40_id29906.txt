№ 40
Protokoll 15. Juunil 1890.
Koos oliwad kohtu peawanem:   Hans Paawelts 
 Mihkel Simson 
asemel  Hans Reinkort
ja Kirjutaja: H. Paawelts.
Kohtu ette astus  Jüri Kandeliin ja andis üles: et tema sest 400 Rbl. rahast, mis tema ema  Ewa Kandeliin  on kihelkonna kohtusse oma poja  Jüri Kandeliin'i jäuks kaswama pannud lubab tema oma ema  Ewa Kandeliin'i 100 Rbl. /sada rubla/ sest rahast wälja wõtta.
Otsus: et see asi peab kindlaks jäema, mis  Jüri Kandeliin on kohtu ees oma ema  Ewa Kandeliin'ile lubanud. 
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem:   Hans Pawelts /allkiri/
 Mihkel Simson
 Hans Ubak ja 
Kirja tunnistuseks kirjutaja asemik:  Jüri Parrikas /allkiri/
