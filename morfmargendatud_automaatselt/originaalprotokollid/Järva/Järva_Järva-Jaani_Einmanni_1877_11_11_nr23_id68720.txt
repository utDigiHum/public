Kohto ette tulli selle walla mees Ködeküla Karjane Hindrik Udelep ja kaebas: et Kalda Jaakob Reinbarg ei maksa mulle suist karja palka mis ma temma Lehma hoidmisse palgaks pean saama, 1 Rb.
Jakob Reinbärg tunnistas et minna polleks tema palka kinni piddand aga ma ollen selle Lehma ette mis tema hoole all  hoida olli 4 Rb Trahwi maksnud Roosna walda ja kui tema sedda mulle taggasi maksab saab omma palga kätte.
Jakob Reinbärg ja Hindrik Udelep lepesid ärra nenda et mollemad tassa teinedeisega.
Koggukonna Kohto nimmel: M. Holland xxx H. Mein xxx J. Anni xxx
