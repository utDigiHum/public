Kokkuolid tulnud kohtu korra pärast kõik alamal nimetud kohtu liikmed:
Peawanem: Kaarel Woormund XXX
Kõrwamehed: Mart Tiiswelt XXX
Hans Stember XXX
Kaebdust ei tulnud sel pääwal ühtigi ette ja nõnda sai se kohtu pääw lõppetud.
Peawanem: Kaarel Woormund XXX
Kõrwamehed: Mart Tiiswelt XXX
Hans Stember XXX
Kirjutaja: K.Kahlberg [allkiri]
