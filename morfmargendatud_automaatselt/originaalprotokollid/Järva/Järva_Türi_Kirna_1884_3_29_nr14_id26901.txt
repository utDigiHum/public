№ 14
Prottokoll sell 29mal Märtsil 1884.
Koos olid kohtu Pääwanem: Jaan Reeman
Mart Sopp
Mihkel Ellmann
Kirjutaja H. Saarmu
Kohtu laua ette astus peremees  Jaan Reinkort ja kaebas, et Mäeküla walla mees  Mendi Sakalof olla minewa sui heina aegu tema keldrist 14 naela wõid ära warastanud ja tema olla kaa muist wõid Sakalofi kääst kätte saanud, mis juba olnud haisema läinud ja põle mitte enam sündinud pruukida, ja tema nõuuab iga naela wõi eest 20 kop.
 Mendi Sakalof  ei ole mitte ise kohtu ees olnud, seepärast, et Mäeküla Tallitaja abimees ütles teda kadunud olewad.
Mõistetud sai, et Mäeküla walla walitsus peab  Jaan Reinkortile 2 Rubl. 80 kop. selle wõi eest maksma, mis  Mendi Sakalof on ära warastanud. (Trahw ei ole mitte mõistetud).
Seemõistmine sai kuulutud ja Mäeküla walla Talitaja abimees ütles: et tema ei wõi seda mitte teada, kas Pää-Tallitaja sellega rahul on wõi ei. 
Kirna walla kogokonna kohtu nimel
Pääwanem:  Jaan Reemann/allkiri/
Mart Sopp  /allkiri/
Mihkel Ellmann  XXX
Kirja tunnistuseks kirjutaja: H. Saarmu /allkiri/
