№ 86
Protokoll 6mal Nowembril 1890.
Koos oliwad kohtu peawanema asemel:  Mihkel Simson
 Hans Ubak 
asemel  Jaan Reinkort 
ja kirjutaja:  H. Paawelts.
Kohtu ette astus  Juhann Traugott ja kaebas: et  Mart Pass olla teda ilma aego raha wargaks sõimanud. See on sell kombel olnud, M. Pass annud 2 Rbl. raha J. Traugott'i kätte ja tema pannud seda sahtlasse ja purjutanud hulk aega edasi. Wiimaks küsinud  J. Traugott , et maksa minu 3 Rbl. sula raha ära, mis mina sinule andsin kui sa Pärnust silku läksid tooma, et ometegi üks kord tasa saaks.  M. Pass wõtnud 10 rublase ja wiskanud laua peale ööldes: "Wõtta tõmba see 3 Rbl sealt maha, sest 2 Rbl. on juba enne sinu käes ja 1 Rbl. 30 cop. oli juba selle aja sees ära joodud, on kokku 4 Rbl. 30 cop. see 12 Rbl. wõtta maha jäeb 7 Rbl 70 cop. järele ja selle raha annud tema  Mart Pass'i poja  Mihkle kätte ööldes: tema kätte ei anna ma mitte, sest ta on juba liiga purjus. Aga  M. Pass wõtnud poja natuke awal raha ja joonud ikka edasi.
Mart Pass astus ette ja ütles: et tema olla ainult 6 Rbl. 76 cop. poja käest raha saanud ja sellega ei ole tema mitte rahul, et 12 Rbl. rahast nii palju puudu on.  Hans Marmann ja  Hans Bormann olla ka see kord kõrtsis olnud kes seda asja on ka pealt näinud.
 Hans Marmann ja Hans Bormann astusiwad ette ja ütlesiwad: et nemad olla seda selgesti näinud, kui J. Traugott on Mart Pass'ile 7 Rbl. paber raha annud ja hõbe raha on ka weel peale seitsme olnud, mis tagasi on antud poja kätte aga seda ei tea nemad mitte, kui palju seda on olnud.
Keiseliku Majesteedi käsu peale on Kirna walla kogukonna kohus mõistnud: et J. Traugott'il ja Mast Pass'i asi teine teise wahel jäeb tühjaks nõudmiste asjus. Peale seda maksab  Mart Pass mõlemi käemehele s.o.  Hans Marmann'ile 30 cop ja Hans Bormann'ile 30 cop. = kokku 60 cop. päewa palka, sellepärast et tema läbi on nemad ilma aegu kohtusse kutsutud.
See mõistus sai ette kuulutud ja kõik oliwad sellega rahul.
Kirna walla kogukonna kohtu nimel:
Kohtu peawanema asemel:   M. Simson XXX
 Hans Ubak
asemel  Jaan Reinkort ja 
Kirja tunnistuseks kirjutaja:  Hans Paawelts /allkiri/
