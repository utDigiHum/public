№ 73
Prottokoll 20mal Detsembril 1889.
Koos oliwad kohtu peawanem:   Hans Paawelts
 Mihkel Simson
asemel: Jaan Reinkort
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Juhann Linnmann ja kaebas: et  Hans Weileril olla 2 Rbl. raha, mis wiina ja tubaka wõlg olla, maksta aga nüüd ei pea H. Weiler seda mitte ära maksma.
 Hans Weileri naene Liisu astus ette ja ei salganud seda mitte ära, waid ütles: "Kes kässib waese räetsepa meistrile wõlgu anda?"
Mõistetud sai: et Hans Weiler maksab 2 rublast poole s.o. summa: 1 Rbl. ära ja kohtuskäimise päewa eest 30 cop. seepärast et ta oma wiina ja tubaka wõlga mitte ära ei salganud.
Mõistus sai ette kuulutud, J. Linnmann oli sellega rahul, aga tema wastane nõuab suuremat kohut.
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem:  Hans Pawelts /allkiri/
 Mihkel Simson
asemel:  Jaan Reinkort
Kirja tunnistuseks kirjutaja: Hans Paawelts /allkiri/
