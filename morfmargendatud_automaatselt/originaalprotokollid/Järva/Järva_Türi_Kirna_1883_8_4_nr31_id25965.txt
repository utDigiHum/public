Prottokoll sell 4mal Augustis 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Mihkel Elmann
Mart Sopp
Kirjutaja H. Saarmo
№ 31
Kirna walla kohtu poolest sai  Jaan Katta peale kaebatud, et tema on kaks korda walla kohtu käsu wastu pannud ja ei ole mitte oma Prottokolli ära täitnud, mis tema peale sai mõistetud, waata № 22.
 Jaan Katta astus ette ja ei wõinud seda mitte ära salata, sest Lõõla küla Tallitaja olla need käsud temale annud.
Mõistetud sai: et   Jaan Katta peab 1 Rubl. trahwi walla laekasse maksma, seepärast, et tema kohtu käsu wastu pannud.
Seemõistmine sai kuulutud ja Jaan Kattal ei olnud selle wastu midagi rääkimist; on kaa täidetud. (1 Rubla walla laeka heaks.)
Kirna walla  kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mihkel Ellmann XXX
Mart Sopp /allkiri/
Kirja tunnistuseks kirjutaja H. Saarmo /allkiri/
