Sel 3mal Junil aastal 1870 ollid kohto päewal koos Purdi koggokonna kohto wannem Andres Juntsohn. Körwa mehhed Mart Wenno ja Jaan Södi.
Se heina Kraam mis sel 20mal Mail ees olli tunnistas Mik Käsber et minna töin sealt künist heino kewwade ei saan ommale metsast ja piddin süggise täis pannema egga ma neid warguse pärrast ei wötnud neid heino on arwad 10 lesikast ja räkisin kohhe Jürrile kui temma tulli, temma olli minno jures sullaseks, agga enne ma ei küssinud.
Jürri Mihkli tunnistas et on 3 sado neid heino sealt kaddund ja temma räkis mulle kohhe ülles ma töin kui minna senna läksin. Kui temma selle küni jälle ni sammo heino täis pannem kui enne ollen rahhul
Jaan Kütti tunnistas se küün olli Jürri Mihkli heino täis.
Selle peale moistesid körwamehhed et Mik Käsper panneb suggise selle küni nisamma sugguste heindega täis kui enne ja selle ette et temma pölle Jürri Mihkli käest enne küssinud maksab 1 rupla trahwi. Selle ette ei olnud mitte Kohtowannem. Andres Juntsohn otsust tegemas et se Jürri Mihkli temma sullane on.
Mik Käsbe tunnistas et Jürri Mihkli laenas minno käest 4 rupla 57 koppik rahha ja 2 külimetto 3 tobi rukkid.
Jürri Mihkli tunnistas minna teggin temmale 26 päewa ja naene olli temmal lapse hoidjaks.
Selle asja leppisid Jürri Mihkli ja Mik Käsber ärra, et ei olle ennam keddagi teineteise käest pärrimest tunnistust egga kirja selle ülle ei olnud kummalgi.
Kohtowannem. A.Juntsohn [allkiri]
Körwamehhed Mart Wenno XXX
ja Jaan Södi XXX
Kirjotaja M Arrak [allkiri]
