№ 13
Prottokoll 27mal Webr. 1889.
Kous olid kohtu Peawanem  Hans Pawelts.
 Jaan Willewelt.
asemel  Jaan Marman.
Kirjutaja J. Parikas.
Kohtu ette astus Kolo walla mees  Thomas Wiitman  ja kaebas: et tema olla 2½ aasta eest, ühe lehma 19 Rbla eest oma äia  Hans Paweltsile ära müünud, ja 10 Rbl olla sest hinnast kätte saanud, ja 9 Rbl olla alles saamata. Aga peale selle olla tema äi ära surnud, ja se lehm olla tema äia naese kätte jäänud. Nüüd nõuab tema se 9 Rbl ja selle intres 2½ aasta eest, mis 90 Cop wälja teeb, se on summa 9 Rbl 90 Cop oma äia naese käest, et tema peab seda ära maksma.
 Hans Paweltsi lesk astus ette, ja ei salganud seda mitte ära, waid ütles: et tema mees Hans Pawelts olla peale selle lehma ostmist ära surnud, ja tema mehe esimese naese lapsed olla keik seda raha, mis tema mehel olnud, kohtu poolest lasknud kinni panna, sepärast ei ole temal nüüd enam midagid selle maksuga tegemist, enne kui suurema kohtu poolest tema mehe raha üle on seletus tehtud.
Mõistetud sai: et  Liisa Pawelts peab se puutuw lehma raha 9 Rbl 90 Cop Thomas Wiitmanile ära maksma, omast warandusest, sepärast et Liisa Pawelts on seda lehma peale oma mehe surrma ära müünud, ja seda raha oma kätte saanud. Aga Thomas Wiitman peab selles asjas nenda kaua ootama, kui suurem kohus Liisa Paweltsile oma jagu warantust on kätte annud.
Se mõistmine sai kuulutud, ja olid sellega rahul kaebaja Thomas Wiitman, kui ka tema wastane Liisa Pawelts.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
Hans Kreedeman
asemel Ado Teps.
Kirja tunistuseks kirjutaja J. Parikas /allkiri/
