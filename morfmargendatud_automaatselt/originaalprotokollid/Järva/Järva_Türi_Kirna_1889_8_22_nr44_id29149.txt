№ 44
Prottokoll 22sel Augustil 1889.
Koos oliwad kohtu peawanem:  Hans Paawelts
 Mihkel Simson
 Hans Ubak
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Juula Limberg  ja kaebas: et  Mari Saanberg  olla teda üks hoop käega löönud ja seda löömist olla tallitaja abimees Jaan Pearda pealt läinud.
Mari Saanberg  astus ette, salgas seda löömist ära ja ütles: et ta olla aga Juula Limberg'i ainult üle wälja aja lükkanud.
Tallitaja abimees Jaan Pearda astus ette ja tunnistas, et see õige olla, mis Juula Limberg on kohtu ees üles annud.
Mõistetud sai: et Mari Saanberg maksab 1 Rbl trahwi walla laekasse, sellepärast, et Tallitaja abimees Jaan Pearda seda löömist on selgesti pealt näinud.
See mõistmine sai kuulutud ja kaebaja Juula Limberg ja tema wastane Mari Saanberg oliwad sellega rahul.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Mihkel Simson
 Hans Ubak
Kirja tunnistuseks Kirjutaja: Hans Paawelts /allkiri/
