№ 24
Prottokoll 1sel Mail 1889.
Kous olid kohtu Peawanem  Hans Paawelts
 Jaan Willewelt
 Jaan Reinkort
Kirjutaja asemel Jüri Saarmo
Kohtu ette astus  Hans Polberg  ja kaebas, et tema olla Jüri Nahkmann'i omale sulaseks kaubelnud ja sellepeale 3 rubla käsiraha maksnud, aga nimetud  Jüri Nahkmann  pole tingitud ajal mitte sulaseks läinud, ega ka käsi raha tagasi maksnud.
 Jüri Nahkmann astus ette, ja ütles: et tema oma isa käsu peale on ennasrt pidanud teise peremehe juurde kauplema.
Leppisiwad niimoodi: et  Jüri Nahkmann maksab  Hans Polbergi 3 rubla tagasi.tema peale. Aga tema läinud kõrtsi tagasi.
Mõistetud sai: et  Jüri Nahkmann  peab seda ära täitma.
Kirna walla kogokonna kohtu nimel:
Peawanem: Hans Paawelts
 Jaan Willewelt
 Jaan Reinkort
Kirjutaja asemel: Jüri Saarmo /allkiri/
