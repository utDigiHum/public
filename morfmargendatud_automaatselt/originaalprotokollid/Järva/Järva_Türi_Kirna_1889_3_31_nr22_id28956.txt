№ 22
Prottokoll 31sel Märtsil 1889.
Kous olid kohtu Peawanem  Hans Pawelts.
Jaan Willewelt
 Hans Kreedeman
Kirjutaja J. Parikas.
Kohtu ette astus Reegla peremees  Hans Eilart  ja kaebas, et Wäätsalt  Jaan Kukejus olla temaga aasta peale teenistuse kaupa teinud, ja tema olla selle kauba kinnituseks 2 Rbl raha  Jaan Kukejusele kätte annud. Aga  Jaan Kukejus  ei ole mitte teenima tulnud, ega ka käsi raha tagasi toonud.
 Jaan Kukejus astus ette, ja ei salganud seda mitte ära, ja ütles, et tema ei olla tahtnud mitte seda teenistust omale wasta wõtta. 
Mõistetud sai: et  Jaan Kukejus peab seda käsi raha 2 Rbl Hans Eilartile tagasi maksma, ja trahwiks 1 Rbl walla laeka.
Se mõistmine sai kuulutud, ja olid sellega rahul kaebaja Hans Eilart, kui ka tema wastane Jaan Kukejus.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parikas /allkiri/
