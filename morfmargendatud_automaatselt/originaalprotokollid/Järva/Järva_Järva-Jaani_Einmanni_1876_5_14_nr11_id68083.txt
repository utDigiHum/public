Kohto ette tulli Einmanni mõisa metsawaht, Andres Kolga, ja andis ülles, et 29mal Aprili Kuu päwal, on Ümmarkusse augu mets, põllema hakkanud, agga sedda ei tea kust ta on pollema läinud, ja ses metsa põllemises on, Baggu herra puid, 9sa sülda ärra põllenud.
Koggukonna Kohto nimmel M. Holland xxx H. Mein xxx J. Anni xxx
