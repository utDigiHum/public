№ 63
Prottokoll 15mal Nowembril 1889.
Koos oliwad kohtu peawanema asemel:   Jüri Saarmo
 Mihkel Simson
 Hans Ubak
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Jaan Epmann ja kaebas: et temal olema 47 sülda kiwi aeda jäenud  Hans Siidam'i krundi peale, sell ajal, kui [wanad] kohad krunti saiwad pantud ja mille ette Hans Siidam on Jaan Reemann'i kuuldes lubanud 10 Rbl. maksta aga seia ajani ei ole Hans Siidam mitte oma lubamist ära täitnud. Ja nüüd sundida Hans Siidam seda kiwi aeda ära widama.
 Hans Siidam astus ette ja ei salganud mitte oma lubamist ära, waid wabandab ennast sellega, et kui tema lubanud seda aeda ära osta, olnud rendi koht, aga sest saadik kui koht on ära ostetud, ei taha tema seda aeda enam mitte ja sellepärast kässib tema Jaan Epmann'i seda aeda ära widada.
Mõistetud sai: et  Hans Siidam peab tingimata see lubatud 10 Rbl Jaan Epmann'ile ära maksma, sellepärast, et tema selle aja eest on Jaan Reemann'i kuuldes seda raha lubanud maksta, mis läbi siis Jaan Epmann'il seda aeda ühe teisele ei ole wõimalik enam kaubelda olnud, pealegi on tema seda aeda sealt saadik oma tarwituseks - wilja wahel aeja asemel pruukinud. Peale seda maksab trahwi 1 Rbl. walla laekasse.
Mõistus sai ette kuulutud: Jaan Epmann oli sellega rahul aga tema wastane Hans Siidam nõuab suuremad kohut. (On ka protokol wälja antud)
Kirna walla kogukonna kohtu nimel:
Kohtupeawanema asemel:  Jüri Saarmo XXX
Mihkel Simson
Hans Ubak
Kirja tunnistuseks kirjutaja: Hans Paawelts /allkiri/
