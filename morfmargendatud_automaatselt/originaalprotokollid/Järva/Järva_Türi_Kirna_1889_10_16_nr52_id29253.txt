№ 52
Prottokoll 16mal Oktobril 1889.
Koos oliwad kohtu peawanem:  Hans Paawelts
 Mihkel Simson
 Hans Ubak
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Ann Siidam ja kaebas, et tema olla Tumma sauna juure läinud aga uksest sisse minnes olla selle sauna koer tema kallale tulnud ja tema mokka lõhki kiskunud. Jaa selle koera kiskumise üle on siis koera peremees  Mihkel Läets ütelnud: ega sest suurt lugu ei ole, kui wana tüdruku mok lõhki kistud on.
 Mihkel Läets  astus ette ja ei salganud teda mitte ära. 
Mõistetud sai: et  Mihkel Läets peab  Ann Siidamile 5 Rbl maksma ja 1 Rbl trahwi walla laeka. Sellepärast et tema on kiskuja koera lahti pidanud. (Täidetud)
See mõistmine sai ette kuulutud,  Ann Siidam kui ka tema wastane  Mihkel Läets oliwad sellega rahul.
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem: Hans Pawelts /allkiri/
 Mihkel Simson
 Hans Ubak
Kirja tunnistuseks kirjutaja: H. Paawelts /allkiri/
