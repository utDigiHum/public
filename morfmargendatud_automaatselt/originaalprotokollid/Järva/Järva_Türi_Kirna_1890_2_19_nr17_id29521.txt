№ 17
Protokoll 19mal Weebruaril 1890.
Koos oliwad kohtu peawanem:   Hans Paawelts
 Mihkel Simson
 Hans Ubak
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Jaan Reemann ja kaebas: et  Hans Saarmann olla teda Risti kõrtsis kaks korda kaela peale rusigaga löönud ja kus juures weel öölnud: "Sinu käest peab see raha wälja tulema."
 Hans Saarmann astus ette ja ütles: et kätte ära tasuma, aga Jaan Reemann ei olla seda protokolli mitte sundinud ära täitma, mis üle temal nüüd alles see raha saamata olla. Ja nüüd nõuab tema seda nimetud wõlga Jaan Reemann'i käest.
Kohtu ette astus Jaan Reemann ja salgas seda ära.
 Hans Lassel astus ette ja ütles: et tema olla seda selgesti näinud kui Hans Saarmann olla J. Reemann'i õla peale kaks wõi kolm korda löönud ja kõik see asi sündinud raha jägelemise pärast.
Leppisiwad ära selle mooduga: et  Hans Saarmann maksab Jaan Reemann'ile walo raha 1 Rbl. 50 cop. 
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem:  Hans Pawelts /allkiri/
 Mihkel Simson
 Hans Ubak
Kirja tunnistuseks kirjutaja: Hans Paawelts /allkiri/ 
