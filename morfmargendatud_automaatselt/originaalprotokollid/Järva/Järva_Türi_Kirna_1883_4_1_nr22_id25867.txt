Prottokoll sell 1misel Aprilis 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Mart Sopp
Hans Simsohn
Kirjutaja H. Saarmu
№ 22.
Kohtu laua ette astus  Jaan Palberg (Kolust) ja kaebas, et tema on  Jaan Kattaga saapaid ja uurisid wahetanud ja et Jaan Katta on temale 6 Rubla peale lubanud anda ja olla kaa 5 Rubl. temale kätte pakkund, aga tema ei ole mitte poolikud raha tahtnud wastu wõtta seepärast olla tema selle 5 Rubla jälle Katta ette laua peale tagasi pannud; aga Katta ei pea mitte kopikud nüüd temale seda pealis raha tahtma maksta. Ja Jüri Kuurmann olla kaa seäl juures olnud.
 Jaan Katta  astus ette ja ei salganud seda mitte ära, waid ütles: et tema olla saabaste ja uuri wahetamise aegu 5 Rubl. kohe  Palberg'ile kätte pakunud, mis tema ei ole mitte wasta wõtnud, waid tema ette laua peale tagasi pannud ja see 5 Rubl. olla säält laua pealt ära kadunud ja tema ei tea mitte kus see 5 Rubl. säält on saanud, seepärast ei tema seda 5 Rubla mitte ära maksta.
Jüri Kuurmann astus ette ja ütles: et Jaan Palbergi kaebdus tõsi on ja et tema on näinud, kui Jaan on selle Katta ette laua peale tagasi pannud.
Mõistetud sai: et  Jaan Katta peab selle 6 Rubl. pealis raha  Jaan Pallberg'ile wälja maksma.
Seemõistmine sai kuulutud ja olid sellega rahul kaebaja  Jaan Palberg, kui kaa tema wastane  Jaan Katta ja peab kaa 2 Mail täidetud olema.
(täitetud)
Kirna walla kogokonna kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Hans Simsohn XXX
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
