1916 goda Fevralja mesjatsa 26 dnja javilis v Alpskii volostnoi sud krestjane Alpskoi volosti Madis Joganov Leets i Madis Madisov Leets, litšno izvestnõje semu volostnomu sudu i prosili o vnesenii v aktovuju knigu Suda slovesnago ih dogovora sledujuštšago soderžanija:
I - Madis Juhani p. Läets annab oma päralt olewa "Allika" talukohast, mis seisab Arawete külas Albu wallas, Seidla mõisast ära lahutatud ja suur on 32 desj., poole oma poja Madis Läetsa kätte rendi peale 3 (kolmeks) aastaks, arwates 23 aprilli k.p. 1916a. Pool koha maadest, see on põllust, heinamaast ja karjamaast, on lepingu tegijate wahel juba wälja mõedetud ja nendel mõlemil nende rajad heaste teada ja rentnikule juba kätte antud.
II - Rentnik maksab rendiandjale aastas 100 (ükssada) rubla renti, millest 50 rubla kewadel, 1sel Märtsil, ja 50 rubla sügisel, 1sel Septembril iga aasta tagant järele maksetud peab saama.
III - Rentnik on kohustatud koha maksudest pool osa maksma, nimelt kroonu tiinu rahast, tulekinnituse rahast ja kiriku maksudest ja peab niisama wallawaeseid, kui rendi andjagi. Rentnik peab poole koha ajad ka korras hoidma ja neid tegema.
IV - Rentnikul on kõikidest koha hoonetest pool tarwitada ja rentnik on kohustatud oma poole peal elu hoone korras pidama, nagu ahjud, uksed, aknad ja korstnad ja katust. Rentnik on kohustatud tarwituse korral wanarehe hoone katust parandama ja korras hoidma. Rendiandja asi on teiste õuue hoonete korras hoidmine.
Madis Juhani p. Läets kirja ei oska, tema palwe peale kirjutas alla: Joh. Reinbaum [allkiri]
Madis Lääts [allkiri]
Nastojaštšii dogovor po vnesenii v aktovuju knigu protšitan storonam i sobstvennorutšno podpisan kr-nom Alpskoi volosti Jogannesom Janovõm Renbaum  podpisavšisja za negramotnago kr-na toi že volosti Madisa Joganova Leets, i kr-nom Alpskoi volosti Madisom Madisovõm Leets, litšno izvestnõm semu sudu. 
Predsedatel suda:  M. Štal [allkiri]
Tšlenõ: O Matizen [allkiri] Ja Kroon  [allkiri] Ju Kautbah [allkiri]  
Pisar: R.Reinvald [allkiri]
