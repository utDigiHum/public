№ 74
Prottokoll 20mal Detsembril 1889.
Koos oliwad kohtu peawanem:   Hans Paawelts
 Mihkel Simson
asemel: Jaan Reinkort
Kirjutaja:  Hans Paawelts
Kohtu ette astus  Hans Saarmann ja kaebas: et temal olla  Hans Mardo käest 1 Rbl. 20 cop. sula laenatud raha saada, mida H. Mardo  ei pea mitte ära maksma.
 Hans Mardo astus ette ja ei wõinud seda mitte ära salata. 
Leppisiwad ära selle mooduga, et H. Mardo maksab H. Saarmann'ile see 1 Rbl 20 cop. 6mal Webruari k.p. 1890 ära.
Mõistetud sai: et Hans Mardo peab seda kindlasti ära täima. mis tema kohtu ees on lubanud.
Mõistus sai ette kuulutud, J. Linnmann oli sellega rahul, aga tema wastane nõuab suuremat kohut.
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem:  Hans Pawelts /allkiri/
 Mihkel Simson
asemel:  Jaan Reinkort
Kirja tunnistuseks kirjutaja: Hans Paawelts /allkiri/
