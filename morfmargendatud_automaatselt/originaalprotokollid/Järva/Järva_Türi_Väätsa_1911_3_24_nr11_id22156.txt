Delo № 11 1911 
ob opeke nad nasledsvom i naslednikami umeršago kr-na Jakoba Janova Šteinberga Salmoju, Jogannoju i Avgustom Šteinberg.
24 marta 1911 g. po pritšine maloletstva.
[Eestkostetavad:]
1) Salme  16 l., služit v Meksgofskoi volosti.
2) Joganna 14 l. , služit v Vjatsskoi volosti.
3) Avgust, 10 l., v g. Veissenšteine, poseštšajet utšilištše.
[Eestkostjaks määratud:]  Juhan Janov Šteinberg, nedvižimosti net.
[Eestkostmise all olev vara:] Nedvižimosti net. Dvižimosti, krome kapitala v 132 rub. 69 k., võrutšjonnogo ot prodaži nasledstva, net.
[Eestkostjate aruannete esitamine:]
za 1912 g predstavlen i utverždjon.
za 1913 g "  "  "  "
za 1914 g " " " " "
za 1915 g " " " " "
Opeka nad  Salme za võhhodom zamuž, prekratilass i snjata soglasno opredeleniju suda ot 27. fevralja 1914 g..
Predsedatel suda 
Pissar suda Pommer /allkiri/
