1903 goda Janvarja 3go dnja proživajuštšije v Alpskoi volosti kr-ne toi že volosti Otto Gansov Romberg i poverennõi kr-ne toiže volosti Gansa Romberga Gans Janov Trummer prosili o vnesenii v aktovuju knigu sego Suda slovesnago dogovora sledujuštšago soderžanija:
I - Hans Romberg annab Radikumäe talu koha oma poja Otto Hansu p. Rombergi kätte rendi peale 6 (kuueks) aastaks arwates 1 Januarist 1903 aastast kunni 1 Januarini 1909 aast. järgmiste tingimiste all
II - Otto Romberg maksab selle koha eest Hans Rombergile 50 (wiiskümmend) rubla aastas renti. Peale selle peab Otto Romberg kõik maksud maksma, mis Radikumäe koha pealt maksta tulewad ja täidab kõik kohustused
III - Hans Romberg jätab omale õiguse Radikumäe koha peal endist wiisi elada nagu tema wiimastel aastatel on elanud, kui see koht juba Otto Rombergi käes on pruukida olnud.
Hans Rombergi usalduse mees wolikirja järele mis Albu w. walitsuse poolt 29 Dets k 1902 N 2006 all on kinnitatud 
H Trummer [allkiri] O Romberg [allkiri]
Nastojaštšii dogovor po vnesenii v aktovuju knigu protšitan storonam i podpisan imi sobstvennorutšno. Oba dogovorivajuštšijasja storonõ litšno izvestnõje volostnomu sudu.
Predsedatelj: M. Štal [allkiri]
Tšlenõ suda G. Trummer [allkiri] Ju. Tamberg [allkiri] J Lustmann [allkiri]
Pisar: L. Iv. Treide [allkiri]
