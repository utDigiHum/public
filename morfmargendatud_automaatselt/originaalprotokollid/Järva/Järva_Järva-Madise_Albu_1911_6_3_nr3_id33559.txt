1911 goda Ijunja mesjatsa 3 dnja javilsja v Alpskii volostnoi sud proživajuštšii v obštšestve Orgmets kr-n Alpskoi volosti Jan Siimov Niggulis, litšno izvestnõi sudu i prosil o vnesenii v aktovuju knigu Suda slovesnago svojego duhovnago zaveštšanija sledujuštšago soderžanija:
Mina Jaan Siimo p. Niggulis oma selge mõistuse ja hea arusaamise juures olen oma waranduse kohta, mis minul on, ehk peale surma järele jäeb, järgmiselt oma wiimase seaduse teinud:
Warandust minul rohkem ei ole, kui 100 (ükssada) rubla puhast raha. Lapsi on minul ainult kaks tütart: Juulie Fischer ja Mari Roos. Pojad on juba surnud. Ülemal tähendatud 100 rubla luban mina peale oma surma oma tütre Juulie Fischerile, kelle juures kuni siiamaale elanud olen ja tema on ka kohustatud selle eest mind kuni surmani oma juures pidama ja minu eest hoolt kandma ja lõppuks ka maha matma. 
Tütre Mari Roosile ei luba mina midagi, sest et ta juba oma jäu, mis minul wõimalik anda oli, kätte on saanud. 
Jaan Niggulis ei oska kirja, sellepärast kirjutab tema palwe peale alla: Siim Soberg [allkiri]
Selle testamendi tegemise juures oliwad tunnistajateks Albu walla talupojad Kaarel Liiw ja Otto Roop., kes tõendawad, et Jaan Niggulis selge mõistuse ja hea arusaamise juures oli ja et se testament tema oma sõnade järele on kirja pandud.
O Roop [allkiri]
K Liiv [allkiri]
Nastojaštšeje duhovnoje zaveštšanije po vnesenii v aktovuju knigu protšitano zavištšatelem i sobstvennorutšno podpisano za negramotnostju jego kr-nom Alpskoi volosti  Siimom Kustavovõm Soberg i svideteljami Otto Roop i Karelem Liiv. 
Predsedatelstvujuštšii:  A. Hansen [allkiri]
Tšlenõ: A Soo  [allkiri] J Kolk [allkiri]  
Pisar: R.Reinvald [allkiri]
