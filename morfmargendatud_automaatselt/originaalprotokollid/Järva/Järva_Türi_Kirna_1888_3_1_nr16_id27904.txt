№ 16
Prottokoll 1sel Märts 1888.
Kous olid kohtu Peawanem  Hans Pawelts.
Jaan Willewelt
 Hans Kreedeman
Kirjutaja J. Parrikas.
Kohto ette astus  Jaakup Luste ja kaebas: tema olla Jüri Ehrenwerthile rätsepa tööd teinud, mis eest temal töö raha 5 Rbl peab saada olema, aga Jüri Ehrenwerth ei pea temale mitte seda raha ära maksma.
 Jüri Ehrenwerth astus ette ja ei salganud seda mitte ära waid ütles: et raha puuduse pärast ei ole temal wõimalik ära maksta olnud.
Leppisid sellemoodoga ära, et Jüri Ehrenwerth maksab seda 5 Rbl 2sel Mai k.p. se aasta ära.
Mõistetud sai: et Jüri Ehrenwerth  peab seda truiste ära täitma, mis tema kohto ees on lubanud ära täita.
Kirna walla kogukonna kohto nimel:
Peawanem:  Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
