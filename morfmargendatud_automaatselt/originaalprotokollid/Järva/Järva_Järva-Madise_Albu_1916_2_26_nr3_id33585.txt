Minu waranduseks on Albu wallas seisaw Albu-Lehtmetsa Talurahwa Põllupanga maadest ära lahtutatud mõisa maakoht "Kaskmäe", mis suur on 25 desj. 1088 ruut [märgina] sülda ja ostetud 1450 rubla eest, mille peal, peale wõla Põllu pangale, mingit muud wõlga ei ole. Koha peal on ka tarwilised hooned. 
Peale selle on minul waranduseks weel kõik koha liikuw warandus, nagu loomad, põllutööriistad, wili j.n.e. Puhast raha ega wäärt paberid minul ei ole. 
II - Minu pärijad on: poeg Aleksander, tütred Aliida Hiiob, Pauline Widse ja tütar Elisabeth Kaasik ja naine Marie. 
III - Oma waranduse peapärijaks nimetan mina oma wanema tütre Aliida Hiiobi, kes terwe "Kaskmäe" koha ühes kõige hoonete ja liikuwa warandusega pärib. Oma waranduse peapärijaks nimetan mina oma wanema tütre sellepärast, et tema kõige sündsam kohapidaja ja parem laps on ja ka kohustud on mind toitma. Oma poja Aleksandrile ei luba mina kohta sellepärast, et ta pillaja ja liialt wiina wõtja on ja minu eluajal kolm korda minu kallale on tunginud, et minu juures wägiwalda tarwitada.
Minu tütre Aliida kohus on mind minu surmani toita ja mind armsaste maha matta.
IV - Minu tütre Aliida kohus on peale minu surma iga ühe oma kaaspärijale, wenna Aleksandrile, õdede Pauline Widsele ja Elisabeth Kaasikule ja oma wõera ema Marie Kaasikule maksta, iga ühele 100 (ükssada) rubla, Aleksandrile ja wõera ema Mariele ühe aasta jooksul peale minu surma ja õdedele Pauline Widsele ja Elisabeth Kaasikule kahe aasta jooksul peale minu surma ilma protsentideta.
Jaan Kaasik [allkiri]
Selle testamendi tegemise juures oliwad tunnistajateks Albu walla talupojad Jüri Tamberg ja Mart Saks, kes tõendawad, et Jaan Kaasik selge mõistuse ja hea arusaamise juures oli ja et see testament tema oma enese sõnade järele on kirja pandud. 
Jüri Tamberg [allkiri]
M Saks [allkiri]
Nastojaštšeje duhovnoje zaveštšanije po vnesenii v aktovuju knigu pritšitano zavištšatelju i sobstvennorutšno im podpisano, a ravno i svideteljami Jurijem Tamberg i Martom Saks. 
Predsedatel suda:  M. Štal [allkiri]
Tšlenõ: O? ...bett? [allkiri] J Kraupmann [allkiri] J Libbek? [allkiri]  
Pisar: R.Reinvald [allkiri]
