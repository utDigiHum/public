№ 38
Prottokoll 1sel September 1888.
Kous olid kohtu Peawanem  Hans Pawelts.
 Jaan Willewelt
Hans Kreedeman.
Kirjutaja J. Parrikas.
Kohtu ette astus Anna Pahmas ja kaebas: et Laupa walla mees Jüri Särg lubanud teda ära wõtta; ja selle läbi olla tema  Jüri Särje nõuse heitnud, ning kokko elanud, mis läbi tema lapse on saanud. Aga Jüri Särg ei pea teda nüüd mitte ära wõtma, ega haitama ka last toita.
 Jüri Särg astus ette ja salgas seda ära, ja ütles: tema ei olla mitte lubanud Anna Pahmast ära wõtta, ega ole temaga kokko elanud, sepärast ei olle temal midagid selle lapsega tegemist.
Seepärast, et nemad ühte peremeest on teeninud, ja  Jüri Särg ei ütle kedagid muud meeste rahwast näinud seal tüdruku juures käiwad. Ja  Anna Pahmas ütleb, et tema kellegi muug meesterahwaga tegemist ei ole teinud, kui aga üksnes Jüri Särjega.
Mõistetud sai: et  Jüri Särg peab maksab lapse toitmiseks 30 Rbl, se on 10 aastat iga aasta 3 Rbl Ja peale selle weel 10 Rbl Kihelkonna kohtuse lapse heaks sisse.
Ja kumbgi maksawad 3 Rbl trahwi walla laeka.
Se mõistmine sai kuulutud, ja  Jüri Särg ei olnud sellega rahul, waid nõuab suuremat kohut.
On ka prottokoll wälja antud.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
