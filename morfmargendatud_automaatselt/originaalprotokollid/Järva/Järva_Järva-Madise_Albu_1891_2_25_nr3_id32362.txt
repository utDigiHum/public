1891 goda fevralja 25 dnja krn. Aljpskoi volosti Jakob Sikkenberg i krn Aggerskoi volosti Gans Bether litšno izvestnõje volostnomu Sudu prosili o vnesenii v aktovuju knigu etogo suda pismennoi dogovor sledujuštšago soderžanija:
Tõsjatša vosem sot devjanosto pervago goda fevralja 25ogo dnja ja krestjanin volosti Aggers Gans Adamov Bether zanjal u Aljpskago kr-na Jakoba Sikkenberga serebrjannoju monetoju dvesti /200/ rubljei za pjatj protsentov godovõh srokom vpred na devjatj let t.je. po 25 fevralja 1900 goda, na kotoruju ja dolžen vsju etu summu spolna zaplatitj; za eti dengi zõložil svoje dvižimoje imuštšestvo zapljutšajuštšisja v sobstvennom krestjanskom dvoru Tjanavaotsa, Aggerskoi volosti. A bude tšego ne zaplatšu, to volenj on, Sikkenberg prosit o vzõskanii i postuplenii po zakonam.
Dogovor etot po pisanii ego v aktovuju knigu bõl protšten storonam i podpisan samim Betherom i za negramotnago Sikkenberga Jurjem Vergom po litšnoi prosbe Sikkenberga.
Predsedatelj suda Ju. Grasberg [allkiri]
Volostnõje sudja: Ja. Bahman [allkiri], J. Krüger [allkiri], M. Ljusii
Pisar: L. Iv. Treide
[Laenu kohta ääremärkus: Polutšil: marta 23 dnja 1891. Jakov Sikenberg [allkiri]]
