Prottokoll sell 19mal Webruaris 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Mart Sopp
Hans Simsohn
Kirjutaja H. Saarmu
№ 9.
Kohtu laua ette astus Kirna Raudemetsa kõrtsu päris rendnik  Peeter Saar Arutsaarest ja kaebas, et tema pois  Mart Pihlak  on tema rendikohast, mis tema on mõisa herra lubaga  Mart Pihlaka kätte rendinud, sada puuda heinu ära müünud. 
 Mart Pihlak astus ette ja ei salganud seda mitte ära, waid ütles: et  Peeter Saar on ise temale luba annud neid heinu ära müüa ja et tema olla kaa need heinad mõisa herrale müünud.
 Peeter Saar astus weel ette ja ütles: et tema ei ole mitte luba annud Mart Pihlakale heinu müüa, mis nende kondrat, nende kahe wahel kaa ära keelab.
Mõisa politsei tunnistus on nii: Mõisa herra olla enne heinte kauplemist M. Pihlaka kääst küsinud, kas temal kaa oma peremehe poolt on luba heinu müüa. Mart Pihlak olla kostnud: jah on. Seepärast olla herra siis need heinad M. Pihlaka kääst kauplenud.
Mõistetud sai: et Mart Pihlak peab nende heinde eest 40 Rubla  P. Saarele wälja maksma, nõnda, et P. Saar jälle 100 puuda heinu peab ostma ja Raudemetsa kõrtsu juures ära söötma ja peale selle weel 3 Rub. trahwi.
Seemõistmine sai kuulutud ja  Mart Pihlak ütles: et tema seda mitte ei täida, waid nõuab suuremat kohut.
On ka Prottokoll wälja antud.
Kirna walla kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Hans Simson XXX
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
