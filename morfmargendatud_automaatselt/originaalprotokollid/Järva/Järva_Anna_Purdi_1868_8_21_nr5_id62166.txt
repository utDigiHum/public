5) Se opmanni lömesse assi mis sel 14mal Augustil ees olli. Mai Rebbas tunnistas, Minna näggin et Hans Turman istus sömawahhe otsa maas, agga sedda ma ei näinud kui opmann tedda löi.
Hans Truberg tunnistas, et meie sullane ütles ennäe et Opman löi poisi mahha ja minna läksin 1/2 tundi pärrast sedda sinna ja Hans Turman ütles et Opman löi keppika süddame kohta ja ei wöi ülles töusta ja Hans Turman seissis söma wahhe otsa nabre jures.
Kusta Hiir tunnistas minna näggin, kui Opman löi Hans Turmannile otseti keppika wasto rindo ja pois kukkus mahha, ja olli sömawahhe otsa maas, minna leikasin teise tündrema peal odra. jäeb polele
Peawannem Hindrek Käerdi XXX
Abbimehhed Mart Hinnow XXX
ja Hindrek Waino XXX
Kirjotaja Mart Arrak
