Prottokoll sell 1misel Webruaris 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Hans Simson
Mart Sopp
Kirjutaja H. Saarmu
№ 5.
Kohtu laua ette astus  Mihkel Tammann ja kaebas: et  Ann Järwekülg    (Kabalast) olla tema kääst ühe hobuse ostmise juures 16 Rubla raha laenanud ja ei tahta seda raha mitte enam ära maksta.
 Ann Jerwekülg astus ette ja ei salganud seda raha laenamist mitte ära, waid ütles: et tema olla selle hobuse läbi wäga palju kahju saanud, mis tema  M. Tammanni rahaga on ostnud ja seepärast ei taha tema seda 15 Rubla mitte ära maksta.
Mõistetud sai: et Ann Jerwekülg peab selle 16 Rubl. kohe kohtu ees  M. Tammannile wälja maksma.
Seemõistmine sai kuulutud ja olid sellega rahul kaebaja  M. Tammann, kui ka tema wastane Ann Jerwekülg.
On kaa täidetud.
Kirna walla kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Hans Simson XXX
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
