№ 2
Protokoll 9mal Januaril 1890.
Koos olid kohtu peawanem:   Hans Pawelts
 Mihkel Simson
asemel: Jüri Saarmo
Kirjutaja asemel:  H. Sarmo
Kohtu ette astus  Jüri Ehrenwerth ja kaebas et temal olla Lõõlast Hansu peremehe  Mihkel Rõõs'i käest 1 Rbl. 70 cop. sada, keda aga  M. Rõõs ei pea temale mitte ära maksma.
 Mihkel Rõõs astus ette ja ei salganud seda mitte ära.
Leppisiwad ära selle mooduga, et M. Rõõs maksab Jüri Ehrenwerth'ile see 1 Rbl. 70 cop. ära.
Mõistetud sai: et  M. Rõõs peab truuwiste seda kohtu ees leppimist ära täitma. /Täidetud.
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem:  Hans Pawelts /allkiri/
 Mihkel Simson
asemel:  Jüri Saarmo
Kirjutaja asemel: H. Saarmo 
