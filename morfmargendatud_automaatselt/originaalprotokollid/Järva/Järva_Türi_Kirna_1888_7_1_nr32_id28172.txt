№ 32
Prottokoll 1sel Juuli 1888.
Kous olid kohtu Peawanem  Hans Pawelts.
 Jaan Willewelt
Hans Kreedeman.
Kirjutaja J. Parrikas.
Kirna walla walitsuse poolest sai  Jaak Mandle ja  Ann Lutsossooni peale kaebatud, et nemad on ilma abielota kasu lapse ilmale sünnitanud, ja seega  Kirna walda teodanud.
 Jaak Mandel ja Ann Lutsosson astusid ette ja ei wõinud seda mitte ära salata. Aga Ann Lutsosson ütles, et se laps mis temal Jaak Mandlega olnud, on nüüd ära surnud, ja tema olla üksi seda last maha matnud, ja matused teinud, ja selle tarwituseks 4 Rbl raha töö peale laenuks wõtnud. Ja nüüd nõuab tema Jaak Mandle käest seda 4 Rbl, kellega tema seda laeno ära maksab.
Mõistetud sai: et  Jaak Mandel peab seda 4 Rbl Ann Lutsossonile ära maksma, ja matmise waew jäeb Anu Lutsossoni kanda. Aga peale selle maksawad kumbgi 3 Rbl trahwi walla laeka, sepärast, et nemad seega on Kirna walda teodanud.
Se mõistmine sai kuulutud, ja olid sellega rahul Jaak Mandel ja Ann Lutsosson. (Täitetud 6 Rbl walla laeka.)
Kirna walla kogukonna kohto nimel:
Peawanem: Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
