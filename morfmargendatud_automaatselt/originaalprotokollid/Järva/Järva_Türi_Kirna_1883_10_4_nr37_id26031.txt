Prottokoll sell 4mal Oktoobris 1883.
Koos olid kohtu Pääwanem: Jaan Reemann
Mart Sopp
Mihkel Ellmann
Kirjutaja H. Saarmu
№ 37
Kohtu laua ette astus  Jaan Reinkort ja kaebas, et Mäeküla walla mees  Mendi Sakalow olla temal 14 naela wõid ära warastanud ja tema olla ka muist wõid  Sakalowi kääst kätte saanud, mis olla juba hukka läinud, nõnda, et see mitte enam pruukida ei ole sündinud.
Mäeküla walla Tallitaja ei ole mitte kolme kortse Kirna walla kohtu kutsumise peale walla kohtusse tulnud, seda lehma ja wõi lugu seletama, waid on käskinud oma peale suurema kohtusse kaebada.
Seepärast, et Mäeküla walla Tallitaja mitte ei ole Kirna walla kohtusse tulnud, waid on wastu pannud, palub walla kohus need mõlemad asjad Kihelkonna kohtu üle kuulamise alla wõtta.
On kaa Prottokollid wälja antud.
Walla kogokonna kohtu nimel,
Pääwanem: Jaan Reemann /allkiri/
Mart Sopp /allkiri/
Mihkel Ellmann XXX
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
