№ 22
Prottokoll sell 16mal Mail 1884.
Koos olid kohtu Pääwanem: Jaan Reemann
Mart Sopp
Mihkel Ellmann
Kirjutaja H. Saarmo
Kohtu laua ette astus  Tõnis Kalberg Kolust ja kaebas: et Kirna Tepma päris koha omanik  Adu Tepp  olla 23mal Märtsi kuu pääwal 1884 temale ühe heinama, mis tema kohast eemal on "ristikiwist ristikiwini puh. ja kaa need küüned, mis selle heinama peal on, 500 Rubl. eest ära müünud. Kõik kulud on lubanud Adu Tepp kanda, mis selle heinama ümber kirjutamise pärast tulewad maksta; ja et tema on kaa 100 Rubl. Adule käsi raha kätte annud. Ja tema olla kolmandama pääwal pärast ostmist kõik selle heinama raha, see on: 500 Rubl. tahtnud A. Teppile wälja maksta, mis Adu ei ole mitte wastu wõtnud; aga Adu Tepp ei pea nüüd temale seda käsi raha tagasi maksma, ega heinamad kaa andma. Selle heinama kauplemise jures olnud käemeheks: Jaagup Sander, Hans Nuhkat ja Jaan Neier (Ärwitalt). Kui tema kõik selle heinama raha on tahtnud Adule wälja maksta, siis olnud selle juures käemeheks: Jaagup Sander ja Kirna walla Tallitaja Kustas Landberg.
 Jaagup Sander, Hans Nuhkat astusid ette ja ütlesid: et se heinama kaup kõik on nõnda olnud, kuidas Tõnis Kalberg on kaebanud. Jaan Neier ei ole mitte walla kohtu ees olnud.
Jaagup Sander ja Kustas Landberg astusid ette ja ütlesid: et Tõnis Kalberg on Adule kõik raha kätte pakkunud, aga Adu ei ole mitte wastu wõtnud, ega ka Tõnisele käsi raha tagasi annud.
1misel Mail, sellesama asja pärast ei ole Adu Tepp mitte kohtu ette tulnud, waid on wastu pannud. 16mal Mail on Adu Tepp Tallitaja abimehe ja ühe Wolimehega kohtu maja juure toodud, aga ei ole mitte kohtu peegli ette tulnud. Selle wastu panemise pärast on kogukonna kohus Adu Teppi tahtnud puuri panna, aga Adu on wastu pannud ja ei ole mitte puuri läinud. Selle pärast palub kogukonna kohus seda asja auuliku Hakenrihtri kohtu üle kuulamise alla wõtta.
On kaa Prottokoll wälja antud.
Kirna walla kogukonna kohtu nimel
Pääwanem:  Jaan Reemann/allkiri/
Mart Sopp  /allkiri/
Mihkel Ellman  XXX
Kirja tunnistuseks kirjutaja: H. Saarmo /allkiri/
