№ 5
Prottokoll 30mal Januar 1889.
Kous olid kohtu Peawanem  Hans Pawelts.
 Jaan Willewelt
 Hans Kreedeman.
Kirjutaja J. Parikas.
 Kurru Kogukonna kohtu kirja 16mal Januarist sub  № 11. peale sai  Hans Pawelts  kohtu ette kutsutud, ja nende wõlgade üle otsust nõutud. Kas se nenda on, kuidas ülewel nimetud kohus omas kirjas on Kirna Kogukonna kohtule üles annud.
 Hans Pawelts astus ette ja andis üles, et tema wõlad mitte sel kombel ei seista, kuida Kurru Kogukonna kohus seda Kirna Kogukonna kohtule on üles annud, waid tema wõlg seista nenda:						Rbl			Cop		1.			Douglaselle			18			92		2.			Jaan Altofile			12			28		3.			Hans Linde sepp			6			50		4			Johan Nitorff			2			50					Summa			40			20		
Aga kaupmees Johanes Kustlega ei olla temal midagid tegemist. Ja seda 40 Rbl 20 Cop lubab Hans Pawelts siis ära maksta, kui tema teenistust saab. Ja maksab sel kombel kuida sedadus käsib, iga teenija käest wõlgasid sisse nõuda.
Mõistetud sai: et  Hans Pawelts peab oma lubamist truiste ära täitma.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Jaan Willewelt
Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parikas /allkiri/
