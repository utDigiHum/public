4) Liggodi Ado naene Anno tunnistas meie ei teadnud et meie ka sedda aeda piddime teggema, Wöölmolter öölnud kui teie tahhade ka sinna Matta siis tehke aeda, kohto küspinenne miks perremees kohtusse ei tulnud Anno tunnistas Pois ei hannud muud käsko kui koli toa jure tulla Eiwerre naene ütles et tänna peab tullema, Ado läks teole, selle tühja jutto peale aias kohhus naese ärra ja ütles Ado peab teisel kohto päewal isse tullema ja sedda asja selletama
Tallittaja Mart Tougo tunnistas minna ütlesin nelja päe ollid kirriko aeda teggemas Ado ütles Neljapäe on rehhi, siis ma ütlesin rede ja laupäe olled seal teggemas, Tönno Saar olli körwas kui ma käsko hantsin Wöölmölde ütles Maggasi aida jure Adule olled kirriko aeda teggemas, kohto körwa mees olli jures
Kohto wannem A.Juntson [allkiri]
Körwamehhed Mart Wenno XXX 
ja  Jaan Södi XXX
Kirjotaja M.Arrak 
