№ 66
Prottokoll 1sel Detsembril 1889.
Koos oliwad kohtu peawanem:   Hans Paawelts
 Hans Lassel
 Hans Ubak
Kirjutaja:  Hans Paawelts
Kohtu ette astus tisler  Hans Saar ja kaebas: et tema olla puusepa  Mart Runge, Kirna mõisas puu töö tegemises, 11 luhwti 3½ Rbl. ja 2 luhwti 8½ Rbl. mis summa 55 Rbl. ja 50 cop. töö raha maksma tulnud, aga nüüd ei pea  Mart Runge  temale mitte seda kaubeltud hinda ära maksma.
 Mart Runge astus ette ja ütles: et tema kaup ei ole mitte sel kombel kaubeltud luhwti pealt olnud, kudas Hans Saar seda on üles annud, waid see olla nõnda olnud, et Hans Saar pidanud 13 luhwti tegema ja saab iga luhwti pealt 3 Rbl. 50 cop., mis summa 45 Rbl. 50 cop. wälja teeb ja temal olla ka selle kauba juures tunnistus, mis tema Hans Saar'ega on teinud.
Kohtu ette astus Mart Pass ja kaebas, et tema olla Mart Runge't Kirna mõisas 17 päewa teeninud ja iga päewa palk on 75 cop. olnud, mis siis summa 12 Rbl. 75 cop. wälja teeb ja sest rahast olla tema 2 Rbl. kätte saanud ja 10 Rbl. 75 cop. olla weel saamata, kedas Mart Runge temale mitte ei pea ära maksma.
Mart Runge astus ette ja ütles, et Mart Pass'i päewa palk ei olla mitte 75 cop. päewa eest olnud, waid aga 70 cop. mis siis summa 11 Rbl. 90 cop. wälja teeb ja sest rahast olla tema 5 Rbl kätte maksnud ja 6 Rbl. 90 cop. olla weel Märt Passil saamata. Ja seda 6 Rbl. 90 cop ei maksa tema sellepärast mitte Märt Passile ära, et see töö, mis tema teinud ei pea kõlblik olema ja mõisa härra on selle töö eest raha ära wõtnud.
Mõistetud sai: seda asja tulewa kohtu päewaks jätta, seepärast et tunnismehed ei olnud mitte kohtusse tulnud.
Kirna walla kogukonna kohtu nimel:
Kohtu peawanem:  Hans Pawelts /allkiri/
 Hans Lassel
Hans Ubak
Kirja tunnistuseks kirjutaja: Hans Paawelts /allkiri/
