1914 goda Dekabrja mesjatsa 12 dnja javilis v Alpskii volostnoi sud krestjane Alpskoi volosti Gustav Tenov Kalme i Kidaskogo volosti Teno Gindrekov Kalme, litšno izvestnõje semu volostnomu sudu i prosili o vnesenii v aktovuju knigu Suda slovesnago ih dogovora sledujuštšago soderžanija:
I - Tõnu Kalme annab oma poja Gustaw Kalmele laenu wiisil pruukida ühe hobuse, 3 sarwlooma, põllutööriistad, wilja ja karduli seemne ja 50 rubla raha, kokku 300 (kolme saja) rubla eest liikuwat warandust. Tähendus: hobuse hind on 150 rubla, loomade hind 60 rubla, muu materjali hind 40 rubla ja 50 rubla raha koha eest sisse maksuks. 
II - Selle laenu wiisil antud waranduse ja raha pealt ei maksa  Gustaw Kalme oma isa Tõnu Kalmele mingit renti ega protsenti, kohustab aga tarwituse korral Tõnu Kalmele seda summat wähe haawal 5 (wiie) aasta jooksul tagasi maksma. Kui aga Gustaw Kalme "Eest-Palginõmme" koha ära müüb, peab ta selle summa oma isale terwelt tagasi maksma, wälja arwatud p.3. sees nimetatud 50 rubla, mis Gustaw Kalme oma õe Annale maksab ja mida Tõnu Kalme Gustaw Kalme käest tagasi nõuda ei saa, nõnda et temal õigus on ainult 250 rubla eest warandust nõuda.
III - Gustaw Kalme wõttab oma peale kohustuse oma õe Anna Kalmele 50 (wiiskümmend) rubla nelja aasta jooksul wälja maksta. Peale selle on tema kohustatud oma isale ja emale prii korterit ja ahju kütti andma nii kaua, kui nad seda tarwitawad.
IV - Kui Tõnu Kalmel oma antud warandust ei peaks tarwis olema tagasi nõuda, siis jäeb see kõik Gustaw Kalme omanduseks ja kellelgi teisel Tõnu Kalme pärijal ei ole õigust tema käest midagi tagasi nõuda. 
Tonu Kalme [allkiri] Gustaw Kalme [allkiri]
Nastojaštšii dogovor po vnesenii v aktovuju knigu protšitan storonam i sobstvennorutšno podpisan krestjanami Kidaskoi volosti Teno Gindrekovõm Kalme i Alpiskago volosti Gustawom Tenovõm Kalme.  
Predsedatel suda:  M. Štal [allkiri]
Tšlenõ: Ja Kroon  [allkiri] O Matizen [allkiri] Ju Kautbah [allkiri]  
Pisar: R.Reinvald [allkiri]
