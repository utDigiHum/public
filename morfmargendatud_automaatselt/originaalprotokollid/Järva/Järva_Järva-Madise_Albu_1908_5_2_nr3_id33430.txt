1908 goda Maja mesjatsa 2 dnja javilis v Alpskii volostnoi sud kr-ne Alpskoi volosti Siim Gansov Erdman i Serreferskoi volosti Gindrek Janov Lindberg, litšno izvestnõje sudu i prosili o vnesenii v aktovuju knigu sego Suda slovesnago ih dogovora sledujuštšago soderžanija:
I - Siim Erdmann annab Hindrek Lindbergi kätte üheks aastaks rendi peale oma päralt olewa Jänekse N 4 talukoha,  arwates 23. Aprillist 1908 a. kuni 23 Aprilli k.p. 1909a.
II -  Hindrek Lindberg maksab selle aasta eest 150 (ükssada wiiskümmend) rubla renti, mis kohe selle kontrakti tegemise juures ette saab maksetud.
III - Rentniku kohus on selle koha põldusid selle korra järele hoida, kuidas omanik neid tänini on harinud. Heinu ja põhku ei tohi ta ära wedada. 
IV - Kõik maksud ja kohustused, mis selle koha peal on, maksab ja täidab koha omanik. 
V - Et koha omanik kui rentniku Hindrek Lindbergi tööline Jänekse koha peale elama jäeb siis jäewad ka tarwilised elu ja kõrwalised ruumid omaniku tarwitada, kuid tarbe korral peab ta ka rentnikule elamiseks ruumi andma. 
VI - Jänekse talukoht on rentniku kätte antud 23 Aprilli k.p. 1908a. 
Siim Erdmann [allkiri]
Hindrek Lindberg [allkiri]
Nastojaštšii dogovor po vnesenii v aktovuju knigu protšitan storonam, litšno izvestnõmi sudu i sobstvennorutšno podpisan kr-nami Alpskoi volosti Siimom Gansovõm Erdman i Serreferskoi volosti Gindrekom Janovõm Lindberg.
Predsedatelj suda: JaGrosmann [allkiri]
Tšlenõ Valter [allkiri]  M Seebek [allkiri] 
Kandidat: A Soo [allkiri]
Pisar: R.Reinvald [allkiri]
