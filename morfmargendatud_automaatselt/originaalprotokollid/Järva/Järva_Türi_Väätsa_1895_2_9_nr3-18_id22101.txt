Delo № 3 . 18
Ob opekajemõhh Avgust i Aleksandr Erenvertah.
Postanovlenije suda ob utšreždenii opeki sostojalos 9 fevralja 186 g.  po pritšine maloletstva opekajemõhh.
Opekajemõje Avgust Erenvert rodilsja  8 l. (v 1898), a Aleksandr Erenvert 5 l. (v 1898 g.) živut v volosti Kirna u materi., nigde jestšjo ne obutšajutsja.
Opekunom naznatšen kr/n Ado Erenwert, nedvižimosti on ne imejet, a opeka obespetšena na jego imuštšestve.
V opeke imejetsja denežnõi kapital v 50 rublei.
Opeka prekraštšena i snjata soglasno tsirkuljaru Sjezda Mirovõhh Sudei ot 9 Marta 1896 g. sa № 1264.
Predsedatel A. Erenwerth /allkiri/
