№ 45
Prottokoll 17mal Oktober 1888.
Kous olid kohtu Peawanem  Hans Pawelts.
 Jaan Willewelt
Hans Kreedeman.
Kirjutaja J. Parrikas.
Kohtu ette astus  Juhan Hermansoon ja kaebas: et tema olla Mihkle pääwa laada ööse Poakal oma kodus hooste talli peal magamas olnud, aga ööse kella kahe aegu tulnud tema talli pealt maha, ja näinud, et kaks  meest seisnud talli ukse peal, tema küsinud nende käest, mis mehed teie olete. Aga selle peale löönud teine mees, se on Särewerest   Adam Urbanus  temale ühe nahast tehtud kantsikuga, kus tina tük sees on, ühe korra päha, nenda et tema on huimaseks jäänud, ja peale selle weel kaks korda, selle löömise läbi on tema pea keik lõhki olnud, ja palju werd jooksnud. Ja teine mees  Jaan Wiktor peksnud teda selja tagast. 
 Adam Urbanus ja Jaan Wiktor astusid ette, ja Adam Urbanus ütles: tema olla Jaan Wiktoriga Paide laatalt tulles ööseks Poakale oma tädi juurde öömajale läinud, ja tahtnud hommiku wara sealt ära minna. Aga sel korral kui nemad talli uksest wälja tulnud, löönud Juhan Hermansoon teda lugemata korrad. Ja selle pärast löönud tema Juhan Hermansonile wasta. Jaan Wiktor ütles: tema ei olla ühte kordagi Juhan Hermansoni, ega J. Hermanson pole ka teda löönud.
 Anna Mourus ja Ano Konks astusid ette, ja ütlesid: et nemad ei ole seda tüli mitte näinud, aga seda on nemad kül talli peale ära kuulnud, kui  Juhan Hermanson on appi karjunud, aga seda mitte, et Adam Urbanus on appi karjunud.
Sepärast, et Juhan Hermanson werine, ja Pea sees augud olid, kui oma peksmise lugu kohtu Peawanemale üles andis. Ja Anna Mourus kui ka Ano Konks seda selgeste on kuulnud kui Juhan Hermansoon on appi karjunud, aga Adam Urbanus ja Jaan Wiktor mitte. Ja peale selle Adam Urbanusel ja Jaan Wiktoril mingisugusid löömise märkisid näha ei olnud.
Mõistetud sai: et  Adam Urbanus maksab  Juhan Hermansoonile 5 Rbl tema Pea hoopide eest ja trahwiks 20 hoopi witsu. Ja  Jaan Wiktor maksab 3 Rbl Juhan Hermansoonile selle peksmise eest, ja trahwiks 15 hoopi witsu. Ja peale selle maksawad Adam Urbanus ja Jaan Wiktor kumbgi 35 Cop tunismeestele nende pääwa palka. 
/Täitetud. Kaks inimest ihu nuhtlust saanud/
Se mõistmine sai kuulutud, ja olid sellega rahul, kaebaja  Juhan Hermanson kui ka tema wastased.
Kirna walla kogukonna kohtu nimel:
Peawanem: Hans Pawelts /allkiri/
 Jaan Willewelt
 Hans Kreedeman
Kirja tunistuseks kirjutaja J. Parrikas /allkiri/
