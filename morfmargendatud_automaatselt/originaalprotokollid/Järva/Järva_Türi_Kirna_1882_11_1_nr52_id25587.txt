Prottokoll sel 1sel Nowembris 1882
Koos olid kohtu Peawanem: Jaan Reemann
Hans Simsohn
Mart Sopp
Kirjutaja H. Saarmu
№ 52.
Kohtu laua ette astus  Mari Pauli ja kaebas, et  Johan Wolberg olla kiuste pärast tema peale wale juttu rääkinud, et tema olla Reopalu karjamõisa lehma salaja metsas ära lüpsnud.
 Johan Wolberg astus ette ja ütles: et tema on küll näinud, kui Mari Pauli on metsas salaja karjamõisa lehma lüpsnud, aga et tema ei ole sest Marile midagi lüpsmise aegu rääkinud, waid pärast on seda teiste wastu paha meelega rääkinud.
Mõistetud sai: et Johann Wolberg peab 1 Rubla trahwi walla laekasse maksma, sepärast, et tema lehma lüpsmise aegu Marile midagi ei rääkinud, ega kaa teda kinni ei ole wõtnud, waid pärast seda kiuste pärast teiste inimeste wasta rääkinud.
Semõistmine sai kuulutud ja olid sellega rahul, kaebaja Mari Pauli, kui kaa tema wastane  Johan Wolberg.
Täidetud. (1 rubla walla laekasse)
Nimede alla kirjutus:
Kohtu Pääwanem: J. Reemann /allkiri/
Hans Simsohn XXX
Mart Sopp /allkiri/
Kirja tunnistuseks kirjutaja H. Saarmu /allkiri/
