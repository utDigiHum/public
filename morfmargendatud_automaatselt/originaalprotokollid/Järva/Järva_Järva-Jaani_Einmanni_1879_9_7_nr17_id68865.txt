Kohto ette tulli Rasiku Uuedalo Kohha peremes, Juhhan Anni, ja kaebas: et minnu Loiosed ollid läinud Otto Hans Rosilehe Kople, ja temma Naene aianud sealt ärra, ja aianud Andres Wiineri Kople, Odrase; sedda teab minnu sullane tunnistad. Ja Andres Wiiner nõudis minnu käest: selle kahju tassumisseks, kahheks pääwaks tutruku leikama, ja se trahw sai ka tassutud.
Hans Rosilehe Naene Liso, tunnistas: et minna se pääw kül Koples käisin, Wassikaid watamas, ja Loiosed ollid ka seal, ja ma saadsin need Lojosed nende endi Kople, ruttuga; ja tõtasin Wäljale põllo tööse.
Tunnismees Adam Kirrik, tunistas: et meie kisgusime seal liggidal Linna, minna, perre Naene, tütruk, ja tüttar; kui Liso Rosileht Lojosed omma Koplest wälja aas, agga sedda ma ei wõi tunistada, kuhhu temma need Lojosed aas.
Tunnismees Juhan Anni Naene Kai, isse ka, et sedda ma kül nägin, et Lojosed wälja aas, agga sedda ma ei tea, kuhu ta naad aias.
Koggukonna Kohhos ei sanud selged tunnistust ja ei wõinud midagi mõista.
