Ette tulli Kassimaja perremees Peter Kinsiwa ja kaibas Risu Jaan Waino päle kes ollewat edimelt tema mõtsa hendale kinitanu, ja päle se kui kogokona kohtu ja wallawallitsuse poolt temale luba antu ütte tõist metsa tükki mes tema krundi sisen nink Waino perralt olli selle asemele mes Waino krundi sisse jäi ärrapruuki. Nüüd olli tema seda maha ragonu nink Waino olli neid maha raotusi hendale ärrawedanu. Pälegi trotsip ja sõimap wargas nink ei lasse ka neid muid mes Kassi majal tema krundi sisse jäi ärraragota.
Saie selle kaibuse päle Jaan Waino ette kutsutus kes wastutab mina lassi omi puid ärrawedada nink Kassi Peter warrast neid weel mant ära. Se mõts mes tema hendale perrip ei olle temal om terwe Kolga jago tema saap üts jago neist puist nisama kui tõise Kolga peremehe. Ütte jao om tema jo säält ärraragonu, nink temal ei olle säält paljo ennamb sada.
Wallawanemb ütlep et tema om mööda länu sügisel Octobri rendi massu ajal kigile walla  perremeeste käsku andnu et Jürri päiwani se aasta ei olle tõisel tõise krundiga enamb tegemist ei metsa ei ka maaga.
Otsus:
Kogokonna kohus mõistap et Jaan Waino metsa om Kassi ma sisest ärawedanu, et tema seda ülle kogokona kohtu ja wallawallitsuse käsü om tenu sis peab tema ne weetu pu tagasi andma. Kui seda ei taha tetta sis peab Kassi Petrile tema metsa oma ma sisest kätte andma mes Kassi majal om sada Waino krundi sisest.
Otsus kuluteti § 773 j. 774 T.S. kohtukäijile. Jaan Waino es olle rahul ja sai oppus kiri sels. paiwal antus.
Ära leppinu.
Päkohtumees Jaan Luik
Kohtumees Peter Tallomees 
    "   Johhan Punnak
    "  Jaan Hinngo
10. April 1878
leppitu
Kassi Peter jättab puntsu mõtsa 30 puud Jaan Waino hääs, künetud wasto, mes üle neide om wõttab temä
 ärä, niida et sellega kõik mõtsa asi neide wahel tasa ja lepitu om.
Jaan Waino [allkiri]
Petre Kintsiwa [allkiri]
