Kaibus.
Ette aste Jaan Hindow nink kaibas, temma ollew Jürripäiwast s.a. se No   pole tallo maad rendile wötnu, ent se röa nurm ei ollew sest enditse rendnikult Wido Narruskowist, mitte wakkama arwole täweste külweto, nink ollew pudus 1 wak ja 3me kappa alla; nink töiselt ollew 4 1/2 wakkama röa körs Wido Narruskowi poolt ülleskündmata jänu, ja kolmandamalt 1/2 sülda maja puid putus, nink nöuwap selle eest kahjotassomist.
wäljanöudminne:
Wido Narruskow endine No    Kapsimäe ma rendnik, sai ette kutsutus nink wastutas ni palju ei ollewad sest külwetu röa nurmest mitte pudus, ent arwab et 1/3 wakkama sest külwist pudus woib olla, nink selle ülleskundmata röa körra ja puduwa puje perrast ei tija temma middagi, sest temmal ei ollew kontrakt ollu.
Otsus:
Koggokonna kohto poolt olli se röa nurm ortega ülle mötetu, nink sai se rehnung ehk arw sel 4. Julil s.a. Wido Narruskowil kätte antu, seddasama ühhe mamõtja läbbi wakkama perra wälja arwata, nink siis jälle sedda sel 18. Julil s.a. sija kohtole ette panda, kunnas agga se Wido Narruskowi polt mitte tännani tettu ei olle, siis peab Wido Narruskow järgit seiswad Jaan Hindowile kahjotassomises tassoma:1,			puduwa röa külwi eest (1 wakama)			13 Rb.		2,			4 1/2 röa körre kundmise eest			4 Rb. 50		3,			1/2 süld puduwa puije eest			2 Rb.		Summa			19 Rb. 50 Kop.		
Se raha olgo katte näddala perrast Wido Narruskowist sija sisse makseto.
Päle otsuse ette kulutamist tunnist Wido Narruskow sellega mitte rahul nink arwap et temmal ollew sellega liga tettu, nink pallel protokolli wälja, siis sai §772 ja 773 ette selletetud ja temmal sel 8. Augustil s.a. oppusleht wälja antu.
Päkohtomees Gustav Lepson
Kohtomees Paap Sabbal
Kohtomees Josep Jöks
