Kaiwas Wõika karjamõisa Rentnik A. Zärenes et 15 aasta eest om Mõisal kohtumeistega leppitu et Mõisa tee jaggo mes enne Kirmsi Külla wahhel om ollu, läbbi Mõisa tette jääb, selle perrast et Wald om ütelnu et Mõisa peabessi se Mõisa wahhe tee teggema selle et Mõisa Kärni se tee wäiga ärra jäkob. Ja nüüd om Mõisa se tee Kiwwega ja Krusaga kõwwas täitnu, ja nüüd om wald se tee Mõisa käest jälle ärra wõttnu, ja Leppingi aigo om ni leppitu ei ilma Mõisa tahtmata ei sa se tee konnagi Mõisa käest ärra wõedus.
Tunistaja Jakob Rämman kes to kord om PaKohtumees om ollu üttel ni õige ollewad kui Zärens kõnnelnu om Rein Oinberg kes to körd Abbi Kohtumees om ollu üttel ka ni õige ollewad ent J. Rämman ja R. Oinberg ütliwa et sedda nemma ei mälleta et ni olles lubbatu et konnagi käest ärra ei wõeda.
Kohus mõist et se tee jaggamine peab ni jääma kui 3 aasta eest wannust kohtumeistest se tee jaedu om, mi kui körrast om tullu.
Peter Urgard
Jakob Parmson
Gusta Söugand
