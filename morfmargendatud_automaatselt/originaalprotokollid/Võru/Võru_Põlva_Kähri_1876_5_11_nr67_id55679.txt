Saije see kaibus mes mõisa Herr v. Dehn 12 März s.a. Kadaku Karl Matzen perijide wasta sisse oli kaiwatu selletetus.
Mõisa Herr v. Dehn oli ette tulnu.
Kaiwatawa Marri Matzin niisama ka Hans Matzen oliwa ette tulnu, ja aniwa ütte Kopik Herr v. Dehn kwittungitest ette, mes Preester oli wälja kirjotanu, aga Kontrahti mitte.
Mari Matzen ütlep et temäle see asi ei puttuwad, et tälle warandus ei olewad jäänu, et ta kaibusest pris jääs.
Jääb poole kuni Kontrahti ette saab.
Pääkohtumees Andres Nikopensius
Kohtumees Peter Nemwalz
   "   Jaan Leib
