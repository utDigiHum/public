Sel samal Päiwal
Need samad Kohtomehed
Tulli ette Adam Raig, ja kaibas et tema piirimees Johann Ratnik olewa üle piiri tulnu, noort mõtsa arwata üts Wakkamaa pikusen , ja 1,2,5 sammo laiosen üles kündnu, kos kiige wähemb üts 100 puud om hukka länno, ja nõuan , 25 Rubla oma noore mõtsa raiskamise eest kahjotasomist.
Toiselt kaibas, et Johann Ratniko käen om mino heinamaad, üts tükk, kos kiige wähemb 10 saandu heina iga aasta saab, ja nüüd 14 ajasaigu tema käen om pruuki olnu, nõwan tolle eest ka 25 Rubla kahjotasomist.   
Selle et Maamõtja Jacoby` pandis kupatsi, ja tegi piiri selges, ent Ratnik wõtse haina iks ära, ilm et mulle midägi tassos.
Kaibus jäije tõise kohto päiwa pääle selletada, sest et Ratnik wastoses andis, et tema üle piiri üttege sammo kündnu ei ole, woib Maamõõtjast piir wallale ajada laske, mes Johann Ratnik ka katten nädalin lubas tetta laske.
Allkirjad
