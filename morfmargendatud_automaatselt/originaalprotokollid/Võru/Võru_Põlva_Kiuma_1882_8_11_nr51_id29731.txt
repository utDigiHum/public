Kaibas siit mõisa  popsi naene Liis Aabel eestkostjaga, et siit mõisa pops, kes temaga ühes majas eläb, olla teda selle eest tõuganud ja mitu korda nuiaga löönud, et ta oma karjuse eest, keda nim. Mustmann olla sõimanud, kõnelema läinud. Pääkohtumees J. Nilbe olla löökide mõjudust pärast ülewaatanud ja Peedo Soukandi naene olla seda löömistnäinud. Kaebaja oma eestkostjaga nõudsiwad 4.kordse löömise eest a 10 rubl. = 40 rubl.
Karl Mustmann wabandas, et see naene olla teda juba mitukõrd sõimanud, sellepärast olla ta teda küll tõuganud enesest eemale, aga mitte nuiaga löönud, waid see nui olla selleläbi senna platsi peale saanud, et ta rabelemise läbi seina wahelt, kus ta olnud, mahakukkunud.
Pääkohtunik Joh. Nilbe, kellele Liis Aabel oma löökide aset näitamas oli käinud, tunistas, et Liis Aabel ütelnud tälle: kakskorda olla teda pähe ja kakskord õla peale löödud, õlga olla ta küll näitanud, aga seäl ei olla mitte katki ja paistetanud olnud.
Liis Sõukand tunistanud: Karl Mustmanni naene olla, nagu seda ennegi tihti sündinud, ka sellkorral jälle Liis Aabeli karjuste üle nurisenud, kelle all ka tema lehm hoita olnud, nagu ei söödaks see tema lehma heasti. Liis Aabel olla ka senna läwepeale tulnud ja ütelnud: "Kui sa nõnda ühtepuhku nuriset, siis meie edaspidi enam sinu lehma enese karja sekka ei wõtagi." Sellepeale kohe olla Karl Mustmann L. Aabeli läwe pealt wäljätõuganud, tema aga olla ennast jälle oma tallituse juure pöörnud ja olla weel paljalt ükskord wälja juhtunud waatama, kui ta näinud, et K. Mustmann Liis Aabelil peal olnud ja teda olla rubinud.
Kohtu poolt soowiti lepingut, mis aga mitte korda ei läinud ja sellepärast
Otsus:
Liis Aabelil on niipalju süüdi, et ta ise Karl Mustmanni poole läks ja ka see löömine ei ole wist heasti kõwaste juhtunud, sest kui seesuguse nuiaga, nagu kaebaja siia näha oli toonud, kord täiesti lüüa oleks saanud, siis ei oleks enam palju elu juure jäänud. Wõib küll ka olla, nagu K. Mustmann ütles, et tema naist ju lüüa ei olla saanud, sest naene wõtnud nuia ära (wistist wõttis nuiast kinni). Et aga nim. Mustmann nõnda häkiline oli ja oma jõuudu trahwima läks, siis on täl 2. näd. a dato 2 rbl. trahwi selle waestekassa heaks äramaksta. Kaebaja jääb aga omast walurahast ilma.
See otsus sai nendele seäduslikult kuulutatud.
Pääkohtunik: J. Nilbe [allkiri]
kohtunik: A. Luk xxx
d.: P.Aabel xxx
Kirjut.as. Zuping [allkiri]
