Kahkwa kohtumajan sel 7 April 1869.
Kaiwas mõtsawat Jaan Leppikow, et Timmusk Kirillow om mõtsast 8 peddajat raggonu ja temma om Timuski mant üte ladwa tükki kätte saanu.
Ette tulli Timmusk Kirrilow ja ütel, et temma 2 toorest ja ütte kuiwa peddaja raggunu om, särratsit mes säljaga om tõsta jõudnu.
Kohus mõist, et Timmusk omma lubbaga om mõtsa lännu 10 witsu
Peter Urgard
Jakob Parmson
Gusta Sõugand
