Kohto mees Jaan Holsting kaibas et Jaan Simmulson om höidse man Karli Hawakatzi poia Gustawil wäitsega külle sissi lönud hawa kindi mähhiä pois om Mihkli Häide kind olno essiteks
Teises om Jaan Simmulson Tolama Möldre poisi Tannil Kaursonil 1 paar wastsid saapid warrastano ja röä sissi pandno
Kolmandas, om Jaan Simmulson Rappina kiwwi Möldre weski tütrikkel Marja Parmsonil ja Lina Dobroskil 3 rätti ja 1 hündrik ärrä warrastanod
Nelländas om Jaan Simmulson Räppina kirriko möisast ägli sissest raud warba ja 1 Talli luk ärrä warrastanud
Wijendäs om Jaan Simmulson Jama küllast Jaan Oijowil 6 R 58 kop rahha ärrä warrastanod kust kohto mees Jaan Holsting 2 R 57 kop om taggasi kätte sanud.
Kuwwendas om Jaan Simmulson omma poiga nink tüttärt tagga aijano ja wäitsega sissi tahtno löwwä.
Seitsmendas om Jaan Simmulson majasid ähwärdänud pallutada
Jaan Simmulsoni käest sai küssitu üttel temmä minna kottiga rops on poisikessi kes minnu turwastega pilliwad agga mul es olle wäist mitte kottin tühhi kott olli raud naggel olli sissen
Kohhus arwas et wõimalik ei olle Jaan Simmulsoni siin mitte trahwi eggä wallale laske et temmä suremat kurja ei sa tettä. Se perräst om kohhus Jaan Simmulsoni kui rauduga wangi läbbi moisast moisa Keiserlikko Werro Kreisi ma kohto ette saatnu kus temmä süid ülle kulleldas ja temmä omma trahwi saap wasto wõtma.
Pä kohto mehhe abbi Kristian Wössoberg
Kohto mees Jakob Türkson
Kohto mees Paap Kress
Kohto mees Hindrik Toding
