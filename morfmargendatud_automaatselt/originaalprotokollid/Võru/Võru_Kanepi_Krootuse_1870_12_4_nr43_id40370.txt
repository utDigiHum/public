Kolmas kohhus selsam(m)al päiwal.
Pigastest An Jaht tulli ette, kaiwas: Temma poig om Adam Raig man karjan olnu, sääl om tõine perremees Juhhan Seme tulnu ja temma poiga kolm kõrd lönu, et ta karja man tulle temma ma päle om ülles tennu.
Juhhan Seme kutsuti ette, küssiti sedda asja perra? Ent Juhhan es ütle tedda mitte koggoni putwat; enge üttel karja enne ärra ajawat.
Koggokonna kohto ülle kaemisse perra löiti poisil märgi küllen ollew, mes läbbi awwalikkus sai, et Juhhan tedda wistest om lönu.
Kohtu otsus: Juhhan Seme peap 1 Rbl. hõbb. selle poisile Mihkle Jaht katte näddali sissen ärra masma.
Sai kohtu otsus kulutedus. Juhhan Seme es olle rahhul ja pallus Prottokolli, medda mitte es lubbatu. 
                                                             Kirjutaja  H. Undritz
