Kohus sel samal pääwal.
Polele jäänud kaibuse asjas sest 24. Juli N. 120 tulli Hindrik Tuhande poolt üles antud tunnistaja Liis Loodus Krootusest ette, kes wälja ütel. "Seal Karaski mõtsast muud haina põimnud ei ole, kui mina, Jüri Wisnapuu naine, Molloka tütrik ja karjus. Meije kül se kõrd Mõtsawahti es näe, ja Kaibataw Jaan Piisko naine Krootusest ütel. "Mina Karaski mõtsast haino põimnud ei ole."
Kaibataw Liis Mollok Krootusest ütel. "Mina sest kaibusest mingi sugust ei tia."
Kaibataw Warika naine ütel. "Ei ole põimnud, ei tia sest kaibusest midagi."
Kaibataw Anna Wisnapuu ütel. "Paksu metsa all olli mina kül, aga mul teadmata, Kas Karaski wai Krootuse mets. ei ole ka haina põimnud."
Kaibaja, mõtsawaht Hindrik Tuhand leppis kaibatawatega nida ära, et Anna Mallok, Anna Wisnapuu nink Liis Loodus maksawad igaüks H. Tuhandele 10. Augustil 1886 1 R. walja.                                                                                       
Kohus kinnitas seda leppingut.
                                                                                     Peakohtomees   J Roht
                                                                                      Kohtomees   M Wõso
                                                                                                  "          J  Mandli
                                                                                    Kirjotaja A. Muhle
