No 2 Nekrut Jakob Tolstberg (Tootsmann) kaibas wasto Hindrik Dobrowi 15 Rubla wõlla perräst
Hindrik Dobrow üttel õige ollewa, et temmäl om 15 Rubla massa Jakobil, Agga nüüd ei pea suggugi rahha massa ollema.
Kohhus mõistis, et kui Jakob Nekrudas jääb sis peab Hindrik Dobrow 12 Märzil rahha ärrä masma, ja kui ei massa sis saap 14 Märzil Oksioni wisil kraam müüdo ja masseto.
Agga kui Jakob wäljä tulleb, massap Hindrik 23 Aprillil 1873 se 15 Rubla ärrä.
