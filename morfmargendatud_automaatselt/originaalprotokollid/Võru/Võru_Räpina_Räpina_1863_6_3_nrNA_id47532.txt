Kolmeister Jaan Warre kaebas se rukki nurmi mis ta Radaman Zongo Jani kaest om wastu wotno, seisap ni alwast, ehk temmast middake ei woi lota, sellest ta panneb Zongo Janile süd. selle perrast ehk teised nurmed ürbre zöri ommawa häste kassono.
Kohtomees Tannil tunnistas ehk se oige on, rukki nurme saistus om santiks.
Zongo Jaan wastotas küssimisse paela ehk rukki nurm om mitte omma siist ni alwast kassono, ta om kik sitta nurme paele wino, ja pöld körra perrast arrino.
Sullane taggase maksa. Hanso Daniel ollis sellega mitte rahhul, ja sai temma oppetud kuida wies ta sis peab suuremba kohto paele peab kaiwama, kui ta ei tahhab mitte omma orjus errakaoma.
