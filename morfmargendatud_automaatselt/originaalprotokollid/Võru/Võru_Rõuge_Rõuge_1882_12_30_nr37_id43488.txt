Ette tuli Ann Ploom ja kaibas, et peremees Jaan Koobakene olewat teda wälja mõistnu, nink olewat tema poiga wastu maad pasnu. Mino lats oli karjusses nink tema es massa seni ajani weel mitte palka ära. Rõiwa käsk tema minul anda, et siis annap tema selle eest mulle hainu. Ka süwwa ole mina henda latsele peaaigu kõige suwwe andnu.
Jaan Koobakene ütleb, et tema ei olewat kunagi Ann Ploomil käsknu wäljaminna, Ploom olewat esi Ludwigi poole lännu.
Ploomi poiga ei olewat tema mitte pesnu, enge Ploomi poig Johan olewat tema pojale löönu. Karjusse palgas olewat leppitu olnu 1 wak rükki ja üts wak keswi, aga et pois enne Michkli päiwa olewat äralännu, selle ei olewat tema mitte palka kätte andnu. Rõiwas ja kängitse pidi ema poolt olema.
Hainu olewat lubanu tema Ploomile anda, nink saawat ka andma, kui tema luhalt haina kodu toob. Haina pidiwa selle antus saama, et poisile es ole rõiwast antus saanu.
Kobakene oli ka lubanu 1 mõõt keswi maha külwata, seda ei olewat tema ka mitte külwanu ega selle eest midagi tasunu.
Mõisteti:
Jaan Kobakenne peab karjussele wäljamasma:
moonas     1 wak rükki
    "             1 wak keswi
ja palgas, et lubatud keswi es külwa, kats rbl. raha nink rõiwa eest lambale hainas 30 punda hainu.
Seda kraami peab Koobakene 14 päiwa seen wälja masma, mida kohtumees wastawõtab nink Ann Ploom kätte annab.
Karjusse enneaigu äraminekit ei ole Kobakene mitte kohtule teeda andnu nink om sellega sis rahul olnu, ent karjusse emma ei tohi wasta talwe mitte majast wäljaaatus saada enge peab maija tagasi wõetama.
Pääkohtumees: J. Kroon [allkiri]
kohtumees: J. Pruli [allkiri]
        "            J. Meister [allkiri]
