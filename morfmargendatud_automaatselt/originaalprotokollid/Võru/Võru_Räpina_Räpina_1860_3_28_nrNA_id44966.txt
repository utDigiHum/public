Mäggiotsa neitsik Ann Some kaibab, et perremees Schmalts et temma küll omma astak om erra tenenu, erra ei lasse
selle peel wastut Schmalts, selle tütrigu aig sap wiel tembetsep paewas 3 1/2 Naddäl, sus om aasta teno, ja sus om ta tütrik ütte teise tütriku Lowise Sok läbbe wahtest temmal palgatu ja 25 kop höb. käerahha sanu.
selle peel wastut Ann Some tu ei olle mitte eige, et temma kaerahha om sanu, lada peel temma küll om Lowisa Sok keest 25 kop höb sanu lade rahhast, et mitte kaerahhas.
moistetu: Tütrik Ann Some peap nie 3 1/2 Naddalit mes astas pudus om erra tenma, ning sus wahtse perremehhele minnema selle eest et ta om Februarill kuu Schmaltsil üttelnu et temma ennep ei geh.
Ягор Кратс &lt;allkiri&gt;
