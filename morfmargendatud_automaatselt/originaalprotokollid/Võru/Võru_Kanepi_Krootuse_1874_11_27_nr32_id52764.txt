Kohtu een kaibas moisa perra kaeja Peter Kuhhi et Tille moisa rentniku omma omme hobbeste ja zikkoga kats Moisa kaara rõuku, ärra söötnu, mes jobba Kohhus ülle om kaenu, nink nõwwap selle eest 15 Rbl Kahjo tassomist, nink palles kohhut sedda asja perra kullelda nink säduse perra moista.
Tille Rentnik Juhhan Annom ette kutsuti nink kaibus ette loeti, kes wastut et temma mitte alla moisa pääle ei puttu, ja ka sest rõugu söötmisest midagi ei tija.
Tille rentnik Juhhan Sarri ette kutsuti kes wastut, et temma ei olle essi sedda rõuku söötnu, ja ei olle ka mitte muid nännu.
Tille rentnik Adam ette kutsuti kes ni sammuti wastut kui Juhhan Sarri.
Juhhan Kõiwer ette kutsuti, kes wastut, et temma ei tija sest kaara rõugu söötmisest midagi muud kui mõnd wäikest põrsast olle minna sääl pääl nännu.
Gusta Mõts ette kutsuti kes wastut, et minna sinna ligi ei puttu, ja ei tija ka sest midagi.
Rentnik Jaak Armulik ette kutsuti kes wastut, et minnol ei olle mitte zikka, ja hobbesega ei olle minna ka mitte söötnu sest minno ristik hain ei olle sääl mitte ligi ja muid ei olle minna ka mitte nännu.
Tille mölder Peter Kowit ette kutsuti kes wastut, et ka temma sest asjast midagi ei tija.
Jürri Kramp xxx
M. Soldan xxx
G. Rautsep xxx
