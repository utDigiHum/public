Jaan Tagel tulli kohto ette ja kaibas, et Peeter Naaber ollew temä omast niidust, temä poja, kes sääl haino põimnu kinni wõtnu ja mõisahe wangi wiinu.
Peeter Naaaber wastutas selle kaibduse pääle, et temä selle perast selle poisikese kinni wõtnu, er arwanu teda mõisa niidun ollew.
Lepsewa tõine tõisega ärä selle mooduga, et P.Naaber massap J. Taglale 1 Rbl. selle eest et temä poja kinni wõtnu.
A.Org xxx
G. Kolk XXX
G. Asi XXX
