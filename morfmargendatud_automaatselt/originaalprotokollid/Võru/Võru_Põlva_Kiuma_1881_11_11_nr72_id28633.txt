Kaibas mõisawalitsuse nimel mõtsawaht Gustav Oper, et siit walla talurentnik Juhan Lepwaltz olla 24.Octobr. õhtul kell 6 aeal mõisa puusülest puid tahtnud warastada. Tema aga olla manujuhtunud, warga kinniwõtnud ja mõisasse wiinud. Mõisawalitsus nõuda 5 rubl.
Juhan Lepwaltz ütles, et tema kül seda mitte nimelt ei olla tennü, waid olla mööda tulles süle pealt mahakukkunud halgusi mõned peale pandnud.
Otsus:
Juhan Lepwaltzil on 3 rubl. mõisawalitsuse heaks kinniwõtmise eest ja 2 rubl. selle kog. waeste kassa 2 nädalani a dato seie ärämaksta.
See otsus sai nende kohtokäiatele Liiwl. Talor. Säd. §§ 773 ja 774 järele kuulutatud. Mõlemad jäiwad rahule.
Pääkohtomees: J. Nilbe
Kohtomees: A. Luk XXX
d.:P. Abel XXX
Kirjutaja as. Zuping [allkiri]
