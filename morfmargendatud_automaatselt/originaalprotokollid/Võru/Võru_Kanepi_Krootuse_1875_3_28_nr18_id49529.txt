Wiendalt   Tulli ette Hindrik Mardimäggi, ja kaibas et Ado ja Peter Plado ellaja ollewa temma kaara ärra söönu ja sõknuwa, mes kohtomees Tannil Wõsso ollewa ülle kaenu, ja nowwab kahju tassomist, 2. wakka kaaro. -
Kutsuti ette kohtomees Tannil Wõsso, kes sedda kahjo olli ülle kaenu. Kes wälja ütles, et se kahju keige wähhemb poolteist wakka karo om.
Kutsuti ette Peter Plado kes ka üttel õige ollewat, et olnu kül kahjo tettu. Ent Ado Plado, ei olle kohtomehhe kutsumisse pääle mitte lännu karu kaema ei olle ka kohto kässu pääle mitte kohto ette tullnu. -
Otsus   Kohhus moistis süggeseste hinna pääle, üts rubla wakka est, teep ütte kokko 1 1/2 wakka est 1. rubla 50. kop. sis tullep kumbalgil 75. kop Hindrik Märdimäggil massa, nink nimmelt Ado 75. kop ja Peter Plado 75. kop mes peap 8 päiwa sissen massetu ollema. - nink lätsiwa rahhuga wälja. -
                                                           (Allakirri)
