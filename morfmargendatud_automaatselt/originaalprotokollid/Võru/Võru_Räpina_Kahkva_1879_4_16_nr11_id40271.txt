Ütte leiwa ellama Jakop Jöks omma poja Janiga ja Jaan andis omma warrandust essa Jakopi kätte nimmelt 2 1/3 wakka Rükki; 6 1/3 wakka keswi 1 1/2 wakka keswa suurmid, 1 wakk tatrekut 1 wakka linnaseemnid 97 nakla sola üts Lehm 10 ajasta wanna 2 Emmalammast ka 4 wanakest.
Jakop Jöks lubbap poja Janile kui ärralahkuwa ajasta ehk pikkemba aja perrast pole töw ehk suwwewiljaseemnest ja pole wakkala linna mahha tetta palgas.
Jakop Jöks XXX poig Jaan Jöks XXX
Päkohtomees G. Lepson
Kohtomees P. Sabbal
Kohtomees Josep Jöks
