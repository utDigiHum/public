Selle tämbatse Kohto Protokolli  №     Katarina Dulder  kaibus asjun wasta Paul Akkelit raho rikumise perast, taas Paul Akkel oma süüdi, et kiigelt poolt essinu, nink palus K. Dulderit temal andis anda Kos  tolle pääle ütles, mina tahas temal hää meelega kiik andis anda, ent et tema minno mitmelt tõisest asjust teotanu om, mes mitte õige ei ole. 
Sai Kohto poolt Paul Akkel käest küsitus kas se õige om et olet emand Duldrit teotanu, kes ütles et tema mitte ütte sõnnaga ei ole teotanu, ei ka mingi sugust teotamise wäärt asja ei tea, ainult ütsinda seda mes tian et Peter Plado ütles minnol et om Katarina Dulder käest peksa saanu. 
Nink om se kaibus ära lepitu.
Pääkohtomees  J. Rocht [allkiri]
Kohtomees  M Wõso [allkiri]
Kirjotaja as. R. Grünberg [allkiri]
