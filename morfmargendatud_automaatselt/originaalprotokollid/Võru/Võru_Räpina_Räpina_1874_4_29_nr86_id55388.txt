Radama kõrtsimees Fompka kaibas kohto een, et temmä om 2 kuud aigo taggasi Jula Häidsoni Kõnno külläst hendä manno näidsikus palgano 1874 aastas, ja 1 Rubla ollewa käerahha anto, agga nüüd ei ollewa Jula näidsikus länno, ja nüüd nõuwap Fompka Julat hendä manno näidsikus.
Jula Häidson üttel küssimisse päle, et temmä om eddimält palgano Michel Griggulil, ja sis 1 näddäli perräst ollewa käerahha taggasi saatno ja Michel ollewa käerahha taggasi wasto wõtno. Ja sis ollewa 2 näddälid pri olnud, sis om temmä Fompkale palganud, ja om 1 Rubla käerahha wõtnud.
Peter Griggul Mihkli poig üttel, et nemmäd ollewa ütte teise innimisse läbbi käerahha lauwa päält kätte sanud, ja sis päle selle ollewa Jula neide manno teenmä länno, ja om 6 näddäid neideman olnud, ja sis ollewa nemmäd ka teist körd Julale käerahha andno.
Fompka nõuwap Julat tenistuste tulle. Ni samma nõuwap ka Peter Griggul Julat teenmä.
Kohhus mõistis et Jula Häidson Fompkale 2 käerahha peab taggasi masma, ja Jula issi peab Peter Grigguli manno teenmä jämä.
§773 om ette kulutedo
Fompka pallel prottokoli.
Päkohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Writs Konsap
Kohtomees Josep Ritsman
Kohtomees Writz Solnask
