Soldat Johann Karjus sai kohto ette kutsutud ja temmale käsk antud, tämba omma naise latsiga ja kramiga ommast majast weljaminda.
Johann Karjus wastotas et temmale sedda külma aigan mitte woimalik ei om, siis piddisewa kohtomehhed nau, ommen temma kotta kohto mehhe läbbi omma kramiga küllan wija, ja temmale sel üts ellopaik murretsama.
