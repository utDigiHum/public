Tulli ette jälleki Hendrik Tuhand ja kaibas jälleki mes sel 5 Septembril polele jaije ja Protokolli No 67 tunniastap. -
Sai Johann Sõõrd ette kutsotus ja küssitus mes sinna ollet üttelno et mõtsawaht Hindrik Tuhhand om sulle haino müno tolle pääle ütles Johann Sõõrd minna ei olle mitte todda üttelno ennegi kohtomees Jakop Uibo om essi tood kulno kui et Johann Sõõrd ollewa kõnnelno et sain küll Hindrik Tuhhand käest haino osta 150 kop selget rahha ja 50 kop töö rahha. -
Otsus  Et Johann Sõõrd ni sugost tühja jutto ajap ja wimate ärra salgap, ja teist tolle läbbi trotsip, massap Johann Sõõrd 1 rubla waeste laati trahwi mes 8 päiwa sissen peap ärra masto ollema
Sai kohto Otsus ja § 773 ja 774 kohton käiadel ette loetus ja lätsiwa rahhon wälja
                               Karraskij Kohtomajan sel 19 Septembril 1875
                                                             Pääkohtomees Jaan Tullos  XXX
                                                              Kohtomees Jakop Uibo  XXX
                                                          teine    do       MIchel Wõsso  XXX
