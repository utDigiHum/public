Kaibas siit Peeter Mustmann, et mõisamees Kusta Lubi olla tema 9 siga, keda ta mõisamaa pealt kinni oli aeanud, ligi päew aeegu, 21 tundi, nälgitanud. Tema nõuda 4 1/2 rubl. kahjutasumiseks.
Kusta Lubi wabandas, et tema olla naesel küll kord heinu etteanda lasknud, muud tall nendele ei ole anda olnud.
Peeter Mustmannil ei olnud oma kaebduse tõenduseks mitte tunnistajaid ülesanda, sellepärast
Otsus:
On tema kaibus tühjaks arwata.
Otsus on seaduslikult ettekuulutatud.
Pääkohtunik: J. Nilbe /allkiri/
kohtunik: H. Lukats /allkiri/
d.: J. Aripmann /allkiri/
Kirjut.as. Zuping /allkiri/
