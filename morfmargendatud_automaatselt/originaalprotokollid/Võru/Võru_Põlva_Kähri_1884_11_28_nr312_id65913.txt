Mihkel Kukk tulli ette ja andis üles et temale on Ann Nikopensiuse käest 2 rub. 1883 ja 5 rub. 1884 a. renti saada ja palub seda wälja nõuda ja 1885 a, pool renti ette ehk kui Ann Nikopensius mite renti ei maksa siis kewadi lubada ma tema käest ärawõtta.
Ann Nikopensius ütleb omale  see üles antud rent kül maksmata olema lubab 1 Küünla kuu pääwal äramaksa. 1885 a I pool renti 5 rubla lubab Mihkel Kukke nõudmise pääle 23 Aprillil 1885 ettemaksa ja 1 oktobril 2se poole 5 rubla. Niisuguse tingimisega ja rendiga lubab MIhkel Kukk selle Ann Nikopensiuse käes olewa maa tema kätte 3 aastad rendi pääle. Se on kuni 1888 a. 23 Aprillini.
Niisugust protocolli sissekirjutada.
Eesistuja Johann Waks
Kohtumees Jaan Salomon
do: Johann Wasser [allkiri]
Kirjutaja M. Bergmann [allkiri]
