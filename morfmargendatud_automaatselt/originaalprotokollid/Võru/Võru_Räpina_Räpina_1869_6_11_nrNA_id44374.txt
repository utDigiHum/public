Kui 26 Mai prottokol sai ette loeto
Reinhold Jakobson tunnistas, et Schmalz om Peter Mähäkäril kontrahti ette lugge wimäse rehnungi pedamisse aig, ja Petrel om weel Schmalzi kätte sissi jänod 50 kop, ja päle selle jäi weel Petrel Schmalzil 28 jalksi päiwä tettä, ja 1 wak röäd Maggasi wölg mis Peter Schmalzi nimme päle om wötnod ja kige selle selletamisse aig om Peter ja üttelno ja perrä kütno et öige om se rehnung olno, kus ma Reinhold Jakobson om seisno.
Ni samma tunnist Iwan Morrosow oige ollewad et Petrel 29 Jalksi päiwä om tetta jänod, Schmalzile ja 1 wak röäd Maggasihe Schmalzi käe päle maksa ja 3 leisikat jahho nink 50 kop jäi Petrel Schmatzi käest sada ni olli neide wimäne rehnung mis Peter Mähhäkär tunnist öige ollewad.
Peter Mähhäkäri pärräst jääs polele 25 Junini
Pä kohtomees Paap Kiudorw
Kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
