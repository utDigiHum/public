Rudolph Härmän Pallo moisast kaibab kohto een, et Schröder Heidhofist moisast om temmä päle ilma asjanda teotusse kõnne tõstno ja Härräle üttelno et temmä om Mosikatzi lado pääl Hiob Suurmanni käest Lehmä ostno agga mitte Rudolph Härmän keddä Herrä laado päle ostma om saatno. Rudolph Hörman üttel et temmä om ostno Lehmä Hiob Surmani käest.
Friedrich Schröderi käest sai küssitu üttel temmä et temmä om Herräle üttelno et temmä om seddä Lehmä ostno, Hiob Surmani käest Rudolph Hörmanniga ütten Schröderi pakmisse päle om kaup tettus sano 17 R 50 k
Agga kubjaso om seddä Lehmä ommale wõtno ja om ütte teist Lehma Herräle selle Hiob Surmani käest osteto Lehmä assemale andno, agga kust se Lehm om osteto kelle käest ja mis hinna eest seddä Schröder ei pea teedmä.
Hiob Surman Mosikatzist tunnistas, et temmä om omma Lehmä nüüd ütte naisele 18 Rubla eest eddimält ja perräst ei olle se naine wõtno siis om teist körd naine 17 ja 1/2 Rubla eest osnto seddä Lehma poig Rudolph om rahha masno, ja käe kokko lönod Lehmä ostmisse päle,
Ja perrä körd om se Saksama mees länno senna ja om üttelno, et se Lehm om pikkä wahhega ja ei kölba osta.
Schröder üttel et temmä om eddimält 17 Rubla pakno ja perräst 17 ja 1/2 Rubla pakno siis om Rudolph käe kokko lönod, selle hinna päle.
Rudolph Hörman üttel et temmä ütsindä ilma Schröddi mann ollemisseta 1 Lehma moisale om ostno ja Schröderiga 2 Lehma kokko 4 tükki om mõisale Lehmi osteto.
Kohhus mõistis et Rudolph om Lehmä ostno jägo se Lehm temmnäle ehk wötko moisa hendale selle perräst et Rudolph ka om moisale Lehmä ostja olno.
