Kaibas siit wallast Juhan Mandli, et Kähri Lepa karjamõisa rentnik Jacob Laar olla juba minewa sügisest saadik tema kartohwli kuhja sigadel ära lasknud sungida. Tema olla küll mitu korda jälle kinniaeanud  ja ka Laarile ütelnud, et omad sead paremini hoiaks. Laar aga ei olla sellest midagi hoolinud, waid kahju edasi teha lasknud, nõnda et selle tsungimise läbi sealt kohalt umbes 10 wakka kartohwlit ära olla külmanud. Kohtunikul Joh. Nilbel lasknud tema seda kahju ülewaadata. Pääle selle wõiwad tarwilisel korral tema üteluste tõenduseks siit wallast Mari Rihm ja Kähri mõisa jäust Jaan Hingo tunnistust anda.
Jacob Laar waidles selle wastu, et seal olla ka muide sigu käinud ja tema sigu ei olla ju Juhan Mandli sealt kunagi kinniwõtnud. Pääle selle ei olla sugugi kindel, kas kartohwlite külmamine tsungimisest wõi muust wigast on tulnud.
Jaan Hingo tunnistas, et tema ei teadwat midagi, sest et tähele ei olla pandnud, kas Laari sead senna olid puutunud wõi ei.
Mari Rihm tunnistas, et tema olla mitu korda näinud, kui Laasri sead on seal kohal kuhja tsunginud, kust kohalt kartohwlid ära olid külmanud.
Pääkohtunik Joh. Nilbe, kes nüüd Mai kuu keskel seda kahju ülewaatamas oli käinud, ütles, et tema arust umbes 4 wakka kartohwlit põhja puolt küllest ära olnud külmanud. Tema kutsunud sissama Mari Rihmi senna juure ja see seletanud küsimise peale niisama, nagu nüüd siingi.
Mõisteti:
Et Juhan Mandli mitte sigu kinni ei ole wõtnud ja kohtunikule kahju ennemalt ei näidanud, kui juba kuhi lahti oli wõetud ja üks ainus tunnistus oli, siis arwati pool kahjutasumist , s.o. 2 waka kartohwlite hind, 150 kop. Jacob Laaril Joh. Mandli heaks 2. nädalani a dato siia äramaksta.
See sai nendele seaduse järele kuulutatud. Wastalised ei olnud rahule. Juhan Mandli 18. Juunil Kopia wäljawõtnud.
Pääkohtunik: Joh. Nilbe
kohtunik: H. LUkats
d.: J. Aripmann
Kirjutas Zuping [allkiri]
