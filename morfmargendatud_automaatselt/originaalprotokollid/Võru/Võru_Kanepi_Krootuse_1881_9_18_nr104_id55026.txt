Alexander Parw, kui wolitedu herrä von Goffroni asemik, kaiwas et herrä von Goffroni moonamehe ommi naisi mitte rehele ei laskwat minna ja ka muu töö man wastopanewad. Pallel kogukonna kohut neid selle eest trahwitada.
Moonamees Kusta Mõts wastut selle kaibduse pääle, et temä esi ei ollew kunagi wastopandnu, ei ollew ka temä naine wastopandnu, lubas esi ja omal naisel ega kõrd töö man olla, kohes enne pruugitas.
Kusta Jahu wastut niisama kui Kusta Mõts.
Peeter Luuse wastut niisama kui Kusta Mõts.
Jaan Mähar wastut niisama kui Kusta Mõts.
Otsus: Moona mehe peawa egakõrd sõnakuuleliko olema oma herräle, niisama ka neide naise.
Kui kogukonna kohto otsus ja Sääduse §§ 773 ja 774 kohtun käüjile ette saiwa loetu, sis olliwa kohtun käüja selle otsusega rahul.
Pääkohtomees John Pähn.
Kohtomees Joh. Purason.
do: M. Laine.
Kirjotaja W.Luik [allkiri]
 
