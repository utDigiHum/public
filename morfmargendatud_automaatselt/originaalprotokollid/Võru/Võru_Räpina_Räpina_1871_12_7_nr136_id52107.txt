Jakob Karrindas andis kohto een ülles et Räppinä Preester I Lebedew om ilma temmä teedmatta temmä 15 10/12 aastaiga wanna tüttär 5 näddälid aigo taggasi wenne öige usso sissi ärrä ristno wasto temmä tütre Wijo tahtmist, perremees Jaan Häidson sundmisse perrä ärrä ristno.
Wijo Karreindasse käest sai küssitu üttel temmä et temmä eddimäst körd kül ei olle tahtno ja perräst om temmä issi hendä ärrä tahtno laske risti selle perräst et perremees Jaan Häidson om lubbano laske rõiwa ja keik tettä laske mis tarwis om, wäggisi ei olle keddägi ristmä winud.
Perremees Jaan Häidsoni käest sai küssitu üttel temmä et se tüdruk Wijo om Räppinäl temmäl üttelno et minna ei lähhä enne koddo kui minnä ärrä ristitu saan, siis om Jaan teddä ütten wõtno ja Preestre manno ristmä länno.
Wijo om Jaan Häidsoni mann eolno 1 1/2 aastaiga.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Gustaw Tolmusk
Kohtomees Kristian Rusand
