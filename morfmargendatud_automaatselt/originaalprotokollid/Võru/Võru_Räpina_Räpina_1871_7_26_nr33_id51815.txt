Räppinä moisa wallitsusse polest kaibati et Körtsimees Ainsoni tüdruk Ann Jürgenson Ahja wallast om Räppinä moisa hernen käino Hernid kakman 25 Julil pühhäpäiw.
Ann Jürgensoni käest sai küssitu, üttel temmä, et temmä wäljä pool aida Herne weren wõtno pää wallo perräst ja siis om moisa mona mees Pareut rätti pääst ärrä tõmmano. Agga hernid ei olle temmä wõtno.
Moisa wallitsusse nõudminne om, pantmisse 1 Rubl kahjo tassomist 1 Rubla.
Ann üttel et Pareutil om temmägä wihha ja selle perräst om temmä rätti ärrä tõmmano ja üttelno et sinna tahhad hernede mindä. Ja Ann om krawi perwe pääl wälja pool herne aida istno.
Pareud Petters tunnistis et Ann Jürgenson om läbbi aja hernid sönod agga kätte körjato ei olle temmäl olno.
Kohhus mõistis, et Ann Jürgenson peab 1 Rubl moisale masma selle eest et temmä läbbi aia Hernid om putno 
26 peab masseto ollema.
