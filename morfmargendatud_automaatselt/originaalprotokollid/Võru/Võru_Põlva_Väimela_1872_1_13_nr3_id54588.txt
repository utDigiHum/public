N 3. Wäimellän  koggokonna kohtu majan sel 13 Januaril 1872
Koggokonna kohtu mehhe kes man olliwa Pä kohtu mees Jürri Waask 2 Peter Pallo 3 Jaan Zuhna
Sel nimmitetu päiwäl tulli Jakkop Kooskora ette, ja kaibas, et Kärnä mäe Jaan Peddäi om temmä kirstu omma tarre ette lasknu panda kon temmä piddi allale ollema, ja om se kirst ärrä widu, ja nõwwap omma kirstu eest 2 Rubla 50 kop
sai sullane Peter Pähn ette kutsutus, ja temmä ütles et tütrekku olliwa kuulnu, et üts om kirstu tarre eest wäljä winu, ja om Josu pole kige kirstuga lännu, saije Jaan Peddai ette kutsutus ja temma ütles, et minna ei tija sest kirstu ärrä wimissest middagi, sest et temma es ütle henda sel ööl kotton ollewat kui se kirst ärrä widi 
koggokonna kohhus mõiste kahjo poles et Jaan Peddai massap 1 Ruubli 25 kop
Kohdu loppo tusseni man olnu Paa kohdumees Jürri Waask [allkiri]
2 Peter Pallo [allkiri]
3 Jaan Zuhna +++
koggokonn kirjotaija S Mattison [allkiri]
