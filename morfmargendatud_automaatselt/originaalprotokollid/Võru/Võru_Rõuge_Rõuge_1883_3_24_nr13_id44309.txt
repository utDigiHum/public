järg protokoll No 5 j. 7 manu.
Kuna Wõrro maakohus T. r. S.r. 1860 prg 674 perra seda kaibust mitte wasta es wõtta, sis sai selle kogukonnakohtu poolt otsus niida tettu.
Mõisteti:
Kuna Johan Saal käest üts puttel ja üts klaas om kätte saadu, sis massab tema kõrtsimees Elias Silsonile 3 rbl. kahjutasumist nink läheb prg. 605 perra kolmes päiwas trahwis türmi.
Jaan Ermel om letti põrutanu nink selle läbi Silsonile kahju tennu, klaasi katski tegemise läbi, massab tema 2 rbl. kahjutasumist Silsonile nink läheb 3. päiwas türmi.
Ent Josep Aplinile massab löömise eest Johan Saal 5 rbl. nink Jaan Ermel 5 rbl. Ent et tüli Rõuge walla piire seen om sündinu, massawa Johan Saal ja Jaan Ermel kumbki 3 rbl. Rõuge walla waeste laadikohe trahwi.
Se kohtumõistus saije kohtun käijile kuulutedus nink T. S. r. prg. 773 ja 774 ära seletedus n.n.e.
T. Kõiw [allkiri]  I. Juul [allkiri]  F. Häusler [allkiri]
Johan Saal 09. Jan 86 11 Rbl. äramasnu
Jaan Ermel 9. Jan. 86 10 Rbl. äramasnu.
