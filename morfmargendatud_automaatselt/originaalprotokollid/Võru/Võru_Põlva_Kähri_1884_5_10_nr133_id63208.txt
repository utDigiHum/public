Keiserliku Balti Domeni Walitsuse Kirja järele 25 Webruarist s.a. № 335, sai Kerstna talu № 2, peremehe Peter Talomees soovimine protocolli üleswõetud:
Peter Talomees soovib oma talu ostmise pääle ära wõtta ja tahab seda ostmise hinda 49 aastaga protsendi wiisil, ehk kui jõud kannab, ka enne seda aega kroonu kassa äramaksa.
Peter Talomees palub alandlikult, et küla teede pruukimine ka wõiks kontraktis tähendatud olla, teiste külaperemeestega ühes.
Seda oma soovimist tunnistab Peter Talomees oma käe alla kirjutamisega tõeks.
Peter Talomees [allkiri]
