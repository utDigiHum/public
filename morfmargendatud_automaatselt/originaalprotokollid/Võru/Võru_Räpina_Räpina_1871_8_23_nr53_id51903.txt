Kohto polest
Kui Peter Kaswandul käst kätte olli anto 4ta Augustil kohto möistmisse perrä 20 witsa lögi trahwi eest 4 Rublsa massa, üttel Peter Kaswand kohto een, et se om õigus mis temmä om sõimano Jaansoni, Ja se om öige et temmä horaga tallitas, Peter Kaswando käest sai küssitu et kes se hor om üttel temmä Taraweri tüttär Wiskä, seddä minna tean selgeste.
Need söimamisse sannad om 4ta Augustil prottokolis ja kohtomõistusse sissen ärrä mõistedo kui Peter 3ta Augustil Jaansoni omma maija ulitse päält (temmä maija) om söimano kurradi hora pälik ja päle selle ulitse pääl om üttelno ni kui sinna omma naise laulatamisse sörmusse litsil ärrä annit, ja törmä majan Wischkä nusjas om üttelno, ehk söimano.
Et Peter Kaswand wastsest kohto een ette om wõtno ütteldä ja ommale õigusses om arwano söimata, selle päle pallel Jaanson kohhut, ja üttel minna pallun kohhut mõista, et Peter minno hommen jälle sõimama ei hakka.
Peter andis tunnistajed ülles, et se assi selges saap, Jaan Kruse, Prakofia, Wallusnikko Wanka, Kuse Petre poig Jaan, Kraupmann Kallo Teppan kige perregä, Platon Risikow.
Tunnistusse perräst jääs teise Septembreni polele.
Kohto polest sai Petrel ütteldo et kui tunnistajed ilma asjanda kutsub kigide päiwäd wäljä peab masma.
