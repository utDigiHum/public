Kohtumees Jaan Salomon kaibas: Kui mina abikohtumehe Johan Nikopensiusega Peter Raig poole läksin magasi wõla sisseriisumise pärast tema kraamist midagi üleskirjutama siis hakkas Peter hoopliku sonadega mind trotsima üteldes: Mis sina ka oled et Sina tuled minu kraami üles kirjutama? Minule on rohkem raha alati kui Sinule ja ähwardas mind üteldes: Ma tahan Sulle näidata. Sepärast palun mina et tema trahwitud saaks.
Peter Raig wastab: Mina Kohtumees Jaan Salomoni trotsinud ei ole.
Abikohtumees Johann Nikopensius tunistab: Peter Raig ütles kül kohtum. Jaan Salomonile: Ah sa oled nüüd uhke et sa ´kohtumees oled ja  tuled sellesama weikse wõla pärast mind riisuma; Minule on ju alati rohkem wilja maksa kui Sinule ongi.
Mõistus. Kogukonna kohus mõistab tunistuse põhja pääl: et Peter Raig peab trotsliku wastutamise eest kohtu mehele kes ameti talituse pärast tema juure on pidanud minema 3 rubla trahwi waeste laadikusse maksma. Trahw peab 2 nädali sees wäljamakstud saama.
Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtuskäijatele etteloetud; olid mõlemad rahul. Raig maksis 3 rubla trahwi wälja.
Eesistuja: Johann Waks
Kohtumees. J
Kirjutaja M.Bergmann [allkiri] 
