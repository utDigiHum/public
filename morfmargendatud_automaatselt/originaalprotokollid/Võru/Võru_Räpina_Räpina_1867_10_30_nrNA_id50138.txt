Kottlieb Röbson tulli kohto ette ja kaibas et temmäl om rahha sada Peter Kirjuta käest 1 rubla enne anto perrä anto weel 3 rubla rahha ja 1 korma haino 2 rubla eest ja 1 wak kartowlid 50 kop eest 1 1/2 Pudt haino wotno kolm aastaiga aego taggasi 50 kop. Ja siis kui minna seddä wõlga läksin otsma siis temmä lits minno nurka ja löi mulle wasto suud.
Ette sai kutsutu Kirjuta Peter ja sai küssitu üttel temmä et 4 rubla om mul wõeto selget rahha mitte muud middägi Ja se wõlg om tögä tassoto 2 Rbla linna kakmisse eest 50 kopka eest aggo raggoto 30 kopka eest sitta päle panto 20 kopka eest haino widdämän 1 rubla hõbbe Essä hauwa kaibmisse eest. 17 kosset kattuse tarbis 1 Rbla 70 kop
Peter Rööbson üttel et temmä ei olle mitte lepno sedda hinda
Essä hauwa kaibisse eest üts rublat. Koski om 7 tükki olno
Gottlieb Röbsonil om Kirjuta Petre käest sada 7 Rbla ulle kige
Peter om ärrä tassono mis kohhos om ärrä arwono 3 R 30 kop
Peter peab weel Kotlib maksma taggasi 3 R 70 kop
Kohhus moistis et Peter Kirjuta peab Gotlil Röbsonil se rahha 3 R 70 kop 2 näddäli aja sissen ärrä maksma ollema.
Pä kohto mees Rein Pabusk
Kohto mees Gustaw Narrosk
Kohto mees Jaan Klimann
