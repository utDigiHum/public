Kohtu ette tullid Kolo talu peremees Jaan Talomees oma wanema poeg Hindrikuga ja lepisid oma wahel järgmiselt:
Hindrik Talomees jättab see 400 rub., mis temale Hoowikohtu otsuse järele isa käest saada, isale, mis eest isa temale Kolo talu annab. See talu jääb aga nii pitkalt isa kätte kuni isa elab.
Isa annab aga selle maja maast poeg Hindrik'ule nii kaugeks pruukida kui tema enese käes maja on: Sikalise maa, mida 6 wakamaad ja Kõiwoaru niit mis 23 Aprillist s.a. Hindrikule kätte saab. 1887 aastal annab isa majast Hindrikule 16 koormat sitta ja Sikalise maast 1 wakamaa rükid.
Tare tarwis, mis Hindrik enesele ehitab elamiseks kuni isa käes maja, saab tema isa käest 50 palgi puud ja teeweerest nurmest 1/2 wakamaad maad tareasemeks ja saab talometsast palotamise puud. Hindriku lehm jan lammas käiwad talu karjas ja talu karjus peab neid hoidma. Praegu ei ole selle maja pääl wõlga sugugi. Kui isa pääle selle aja maja pääle wõlga peaks tegema siis wastutab selle wõla eest maja kraam ja noorem poeg selle maaga, mis Hindrik temale rendi pääle anda on lepinud siis kui maja oma kätte saab. Ait mis praegu Hindriku käes jääb ka Hindriku kätte kuni isa elab ja ka see kolme seinaga ait. Pääle isa surma saawad kõik hooned Hindrikule muud kui üks ait kus praegu wili sees saab nooremale poeg'le Jaanile.
Kõige selle eest, mis isa Hindrikule lubanud  ma ja lehmakarjas käimise eest ei ole Hindrikule midagi maksa.
Pääle lepingu tegijate surma lähab see leping nende pärandajate pääle üle.
Seda lepingut tunistawad lepingu tegijad oma käe allakirjutamisega
Peremees: Jaan Talomees XXX
tema poeg: Hindrik Tallomees [allkiri]
Kogukonna Kohtu poolt saab see leping tunistud
Peakohtumees
Kohtumees
do:
Kirjutaja: M. Bergmann [allkiri]
