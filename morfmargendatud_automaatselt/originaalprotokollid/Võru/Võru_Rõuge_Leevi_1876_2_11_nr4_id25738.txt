Sel sammal päiwal
Ette tulli Timmo walla mees Hindrik Saarnitz ja kaibas et Peter Look temmale 6 rubla wõlgu ollewat nink pallep kohhut sedda wälja otsida. Peter Look ütles sedda kaibust õige ollewat, ent nüüd mitte woimalik massa ei ollewat.
Mõistet:
Peter Look peap sedda rahha 6 rubl ammuh 24 Webruar sa. Hindrik Saarnitz hääs wälja masma nink kaep kogu kon kohhus Peter Look warra ülle et temma sedda ärra ei wea omma wastse paiga päle wennemale.
Se moistminne om § 773 ja 774 perra kulutet
Pääkohtomees [peab olema allkiri] Jaan Thalfeldt
kohtomees Johan Kausaar XXX
" Johan Suits XXX
Kirj. [allkiri] Jõggewa [kirjutatud loetamatult].
N 4.
