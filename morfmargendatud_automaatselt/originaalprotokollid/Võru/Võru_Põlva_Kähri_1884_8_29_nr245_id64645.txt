Et Hindrik Nikopensius kogu konna kohtu peegli ees ennast kroppul ja mõistmatal kombel üles pidanud on ja Jacob Paidrat säälsammas joodikuks, prohwuseks sõimanud ja ütelnud Paidrat hoora jahti pidawad ja niisugust trotsimist järele jätta mite kohtu keeldu ei ole kuulanud, siis mõistab kogukonna kohus Hindrik Nikopensiust see eest 3 ööks ja pääwaks kogu konna türmis kinni istuma. 
Äratäidetud.
Eesistuja Johann Waks
Kohtumees: Jaan Salomon
do Peter Kolk
Kirjutaja M.Bergmann
