Johann Kirjutus kaibas kohto een, et temmä om münud omma Sila tallo ja maad Kristjan Rusandil ärrä 125 Rubla ollewa tadre hind olno leppito 1874 kewwädi, 10 Aprillil, agga perräst Kristjan ei ollewa rahha masno 50 Rubla ollewa Kristjan sis masno kui temmä ma om ostno. Seddä ärrä müdo maad ollewa 10 2/3 tadre maa olno.
Nüüd nõuwap Johann perrä kas Kristjan wõttab Ma ostap seddä maad wõi ei osta.
Kristjan Rusand üttel et selle perräst ollewa se maa ostminne polel, et ei ollewa puhhast kontrahti. Kristjan Rusand üttel, et temmäl ollewa 10 2/3 tadre maa rendi pääl olno naise Essä Johann Kirjutusse käest.
Kristjan aasta rent ollewa olno 70 Rubla. Johanil olli massa Kristjanil 11 Nowembreni 1874, 18 Webroaril Kohto mõistusse perrä Rent wõllast mahha arwato jääp weel Johanil Kristjanil massa 107 1/2 Rubla
23 Aprillini 1875 wõlg massa 120 Rubla kohto mõistusse perrä.
Johann Kirjutus üttel et temmä tahhab nüüd maad ärrä müia poia Josepil ehk annap rendi päle Nüüd nõuwap Johann 10 Rubla tadre päält renti. Rendi päle Johann ei lubba nüüd ennämb anda, enge tahhab poia Josepil ärrä müwwa.
Kristjan Rusand üttel et temmäl kui tallo maa rentnikkul ei olle mitte ülles ütteldu, ja et temmä selle läbbi sure kahjo sissi jääp kui ma käest ärrä wõetas. Kristjanil om kahjo arwato 4 lapsega 4 innimisse moon, Kristjan nõuwap mona aasta päle24 wakka röäd			48 R		16 wakka keswi			32 R		4 wakka Hernit			8 R		8 Pudta Sola			6 R		24 wakka kartowlid			12 R		korter			30 R		Elläjä söök 1 hobbene			100 R		4 Lehma Lambat Zead		Summa			236 Rubla		
8ma aasta sissen mis se maa Kristjani käen rendi pääl on olno ollewa temmä tenno1 kiwwi keldre hind			5 Rubla		1 Aitt tetto hind			10 Rubla		1 Kambre 3 sainaga			3 Rubla		2 linna tüki tetto'			2 Rubla		2 1/2 wak röäd rohkemb külweto			5 Rubla		Röä körs künneto mis enne ei olle olno tetto 5 wakkamaad			5 Rubla		Summa			30 Rubla		
Johann üttel et se ei pea õige ollema
Ülle kaemisse ja tunnistusse perräst jääs polele 25 Nowembrini.
Johann Kirjutus om täembä 107 1/2 Rubla Kristjan Rusandil wõlg rahha ärrä masno.
