Ette tulli Karl Sultz ja kaiwas et se Kog. Kohtu mõistmise pääle sel 26 Januar 1870 No 6 al et Mihkel Runing temma penni olli ärra hukanu piddi M. Runing töine penni assemele ostma mes M. Runing ka täidnu om. Ent nüüd om se penni jälle mes assemele ostetu ärra kaddunu, mes kulmise perra et Peter Runing se Penni Wõika Karjamõisa winu ja sääl ülles puwa lasknu, mes Wõika moona naise olles Karl Sultzile ütelnu et Wija Peep om se Penni nülgnu.
Ette tulli Mihkel Runing kes üttel et temma sest Pennist middagi ei tija.
Peter Runing üttel et temma sest Pennist ka middagi ei tija.
Ette kutsuti Peep Wijar ja ütel et Peter Runing om se Penni ülles poonu ja temma om P. Runinge kässo pääle ärra nülgnu ja se nahk om Wõika Rehte kujuma pantu ja säält om arra warrastetu.
Sel 2 Augustil ütel Peter Runing et temma om Wõikal töön ollu ja Penni om 3 päiwa temma man nälgunu ja Peep Wija olles üttelnu lubba mina poon temma ülles sis om temma ka lubbanu.
Kohhus mõist et Peter Runing om eddimalt sallanu et temma Pennist middagi ei tija ja perrast ütel et temma om se Penni ülles puwa lasknu et Penni temmale perra om tullu peab Peter Runing se Penni eest 1 Rbl 50 Kop ka et om sallanu ja Karl Sultzi waiwanu se Penni otsmisega 1 Rbl trahwi masma Kokko 2 Rubla 50 Kop.
Päkohtumees Kristjan Rämman
Abbi Kusta Hanson
Abbi Fritz Runhanson
