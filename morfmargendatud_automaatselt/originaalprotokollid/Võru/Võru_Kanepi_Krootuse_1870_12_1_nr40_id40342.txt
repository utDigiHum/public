Jusal sel 1. Dezembril 1870 olli Karraskij koggokonna kohhus koon, nimmelt:
                                                                                    Pä kohtumees Mihkel Raig
                                                                                    Kohtumees Jaan Leppasoon
                                                                                                 do    Juhhan Mandli
Karraskij perris herra nimmel tulli mõtsawaht Jaan Uibo ette ja andis eenkondrahhi, kon herra Jaanba maija Gotthardt Grünbergile om ärra münu ja pallus eenkontrakt ette luggeda nink ülles anda, kas nemma essi sedda maija kige neide eenkondrahhin leppitu tingimistega tahtwa wõtta, sis herrale se rahha nelja näddali sissen sisse massa ent kui sedda ei te, peawa Jürripäiwal 1871 om(m)ast majast wäljaminnema.
Kutsuti Jaanba perremehhe Jaan Mandli ja Jaan Läwwi ette, sai eenkontraht ette loetus ja ütteldus, kui nemma essi nelja näddali sissen kige neide eenkontrahhin leppitu tingimistega ei wõtta ja herrale se rahha sisse ei massa, sis omma nem(m)a om(m)a een õigusse kaotanu nink peawa Jürripäiwal 1871 om(m)ast majast wälja minnema. -
                                                         Kirjutaja H. Undritz
