Jaan Mötsar ja Johan Plakso omawa henda wahel ära lepnu niida, et Jaan Mötsar massab Johan Plaksole 2 rubla pesmise eest ja Johan Plakso massab Kustaw Mötsarile Jaani pojale 1 rubla pesmise eest. See pesmine on nüüd mööda ja ära leppitu lubawa nema henda selle eest hoita, et niisugust enamb ette ei tule.
Pääkohtumees Joh. Lepland
Kohtumees J. Kirhäiding
Kohtumees J. Oinberg
