Kaiwas Orawalt Katri Kannel ???? et tema ostnu Wassil Hamkowi käest tarre 20 Rubla eest sel 25. Märtsil 1883. a. ja andnu eestmalt 10 Rubla ärra ja perrast töist 10 Rubla Ent Wasil Hamkowil olewat se tarre enne töisele nimega Hökla Tihanowil müdu, nink ei laskwat tedda Katrit sinna tarre sisse elama nink nöwap kui tarre ei sa 40 Rubla. temma Katri tagasi massa.
Ette kutsuti Wasil Hamkow nink tunnist seda tötte olewat et temma kattele tarre münu enne Hökla Tihhonowile ja perrast Katri Kannelile
Otsus
Moistis kohus et Wasil Hamkow peap Katri Kannelile sisse mastu rahha 20 Rubla tagasi masma ütte nädali aja sissen nink 3 Rubla Katri Kannelile manno masma ja 2 Rubla waeste ladiko trahwis et tema kattele on tarre münu. Nink ei woi Katri Kannel muud tasumist nouda seperrast et temma wöra walla lige om ilma Wallawalitsuse lubbata walda sisse tulnu tarre ostnu ja elule lönu.
Pääkohtomees P. Urtson
Kohtomees J. Oinberg
Kohtomees K. Rämman
See otsus sai sellsammal päwal neile Tallorahwa säduse 1860 a. §§. 772 ja 773 pöhjuse pääl kulutatud ja ärra seletatud ja suremba kohtu käimises lubba täht wälja antus  sanu Katri Kannelile kes selle otsusega rahul es ole.
