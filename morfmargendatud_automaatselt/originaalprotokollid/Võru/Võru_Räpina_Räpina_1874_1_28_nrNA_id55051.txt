No 18, 21 Januaril
28 Januaril päle ette luggemisse tunnistas Peter Mähhäkär et wõttatamist ei olle kül mitte kulu, ja ei olle ka muud middägi nänno kui se tüdruk Marri Pärli sääl rehhemann krawi nukka pääl maan om olno ja om üttelno pinni om purrenu, agga ei olle mitte pinni nänno eggä helli kulu.
Jaan Möksi tunnist, et temmä ei olle middägi nänno, pinni om hakkano haukma, temmä om kaeno, et pinni om kaugel olno tüdrukomant.
Karli Lökku tunnistas, et temmä om koddon olno käegä häige ja ei olle rehhemann olno, tarreman om temmä nänno sel tüdrukul ütte kripso päle põlwe mis Werrine om olno ärrä saistano Sel aial om nilbe aig käijä olno
Marri Parli welli Michel Parli andis ülles, et Mäggiotsa pinni ollewa ka Mäggiotsa mõtsawaht Ado Uibo ärrä kiskno.
Ado Uibo üttel et se peab koggoni wõls ollema ja mäggiotsa pinni ei ollewa teddä mitte kiskno,
Michel Pärli andis weel tunnistajad ülles 1 Jaan Kissand, 2 Ann Anniots Jani tüttär.
Tunnistusse perräst jääs polele.
