Kaibas Mötsasaks J. Roosneck, et Otto Warend Werriorralt moisa raotost ollew haina pöimno ehk niitnu, ning neide hainoga ühhes ollew 53 kassopu ollu, nisama ollew temma üks pak lahaspol 2 arsin 2 tolli pik ja 22 tolli paks mötsast henda pole weddanu.
wäljanöudminne:
Otto Warend tulli ette ja wastutas, temma ei olle mitte Kahkwa ent Werriorra mötsast ne haina ning ka sedda pakko tonu, ent mötsasaks Roosneck tullu temma pole otsma ning tunnistanu ne haino Kahkwa raoto pält ning se lahhaspool pak ka säält ollewa, ning laskno mötsasaks se pakko ja haino Kahkwahe taggasi wija.
Tomas Woronow Werriorra wöörmünder tulli ette ja andis tunnistust, et sel korral, kui mötsasaks Otto Warendi poolt ne haina löutnu, siis ollew Otto Warrend ütelnu, temma ollew ne haina Werriorra mötsast tonu, ni sama ka sedda pakko.
Moisteto:
Tomas Woronowi tunnistuse päle, on se assi tühjas arwato.
Pääkohtomees G. Anderson
Kohtomees Paap Sabbal
Kohtomees Josep Parmak
