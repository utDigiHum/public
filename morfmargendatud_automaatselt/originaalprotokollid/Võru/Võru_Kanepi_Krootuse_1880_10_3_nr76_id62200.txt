Tulli ette Tannil Kantsik ja kaibas et Kokkeri tallo karri om temma tatriko põllo wäega ärra söötnowa ja nõudis kahjo-
tassomist 3 mõõtu tatrike. -
Sai ette kutsotus Kokkeri tallo perremehhe ja kaibus ette loetus kes ütliwa et se jutt mitte õige ei ollewat ei olle kahjo middägi tettä lasto. -
Kaibus jäije tõise kõrra pääle konni kahjo saap kohto poolt ülle kaetus. -
                                                    Karraskij Kohtomajan sel 3 oktobril 1880
                                                                  Pääkojhtomees Jaan Tullus
                                                                     Kohtomees Jaan Leppäson
                                                                  teine     do    Samoel Raig 
