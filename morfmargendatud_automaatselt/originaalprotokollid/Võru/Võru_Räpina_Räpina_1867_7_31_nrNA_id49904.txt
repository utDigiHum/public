Tannil Narrosk Wöbsost tulli kohto ette ja kaibas et temmä 29 Julil se om poolpäiw olno om temmä Räppinä podi lanno ja podi Saks Tepper om temmäle warsti üttelno et sinna kurradi warras sinna olled essi warras ja sullased ommawa ka wargad sääl om tubbakud ja peeglid pudus sinna olled keik warrastanod.
Podi Saks Tepper tulli kohto ette ja üttel et temma om üttelno seddä kül agga et temmä koggokonna kohto allune ei olle ei tahha temmä ka suremad selletust et Tannil peab suremba kohtohe kaibama nink se päle om ka kohhos piddäno polele jätma.
Päkohtomees Rein Pabusk
Gustaw Norrosk
Karli Jagoson
