Ette tulli kaibaja Reola möldre Reilhartd, nink temma wastane Ihhamarro kõrdsimees. Möldre kaibap:
Ehhin om tälle 100 wakka keswi mes 102 naala piddano wak kaalma, sedda wisi kuis nende Kondrat wälja saap nõutma müüma, nink selle päle 25 ruplat käserahha wõtno, ent Jaan om säratsid  tono mes 90 naala omma kaalnowa, ent möldre ütles, et temma neid mitte ei wõiwat wasto wõtta.
Kui küssitus sai, mes sa sis nõwwat?
Möldre küsse Toppelt käerahha, 10 rupla käumisse waiwa, eddespäididse käiki eest 5 ruplat käigi pääle.
Et Jaan möldrega mi rummalat kaupa  mes temma täüta es jõwwa om tenno, 
moist Koggokonna Kohhos:
omma käerahha taggasi ja 5 ruplat trahwi.
