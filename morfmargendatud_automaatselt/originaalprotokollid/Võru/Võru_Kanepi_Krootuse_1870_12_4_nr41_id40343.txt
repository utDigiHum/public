Jusal sel 4. Dezembr 1870 olli Karraskij koggokonna kohhus koon. Man olliwa: Pä kohtumees  Mihkli Raig
                                                                                                                                        Kohtumees Jaan Leppasoon
                                                                                                                                             do            Juhhan Mandli
Leesmitti perris mapiddaja Sõrreste herra A.v Kiel, kes kaddunu Grüjeri käest Leesmitti  No 2 ja 3 tallo kike temma õigustega om ostnu, - kaiwas omma kubbija läbbi: Kaddunu Grüjeril olli Juhhan Seemnega lepping tettu, et ta tälle peap palke weddama ja masnu rahha ette ärra nink üttelnu: Säe illuste rist palgi kihhile alla, et ärra ei mäddane; ent Juhhan ollew kiik palgi palja ma päle pilnu ja nimodu ollew 20 tükki ärra mäddanu.
Juhhan Seme kutsuti ette küssiti selle asja perrast. Temma koste: Minnule ütteldi, et tarre nakkatas tullewa näddali teggema ja selle es holi minna ja nüüd nakkati tarre weel neljandal aastal teggema, kunna ommeti muidoki polelist ärra mäddasiwa.
Kohhus mõist: Et neid palgest muidoki rist palgi kattel kihhil se om 5 palki olles ärra mäddanu selle jääp sis Juhhan Seemnel ennegi 15 palki wastudada ja peap tükki eest 50 cop., - et 3 aastad saiaiwa ja sis muidoki pool ärra mäddasiwa, - kette näddali sissen Sõrreste herrale A. v. Kiel ärra masma. -
Sai kohtu otsus  ja § 773 ja 774 kohtun käüjalle kulutedus, Juhhan Seme es olle rahhul ja pallus Prottokolli, mes ka sai lubbatus.
                                                                         Pä kohtumees Mihkel Raig  XXX
                                                                         Kohtumees  Jaan Leppasoon  XXX
                                                                                  do        Juhan Mandli  XXX
Prottokoll om sel 9. Dezembr 1870 wälja kirjutet.
                                                                            Kirjutaja   H. Undritz
