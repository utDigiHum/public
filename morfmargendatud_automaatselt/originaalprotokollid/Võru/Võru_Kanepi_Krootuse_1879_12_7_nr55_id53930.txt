Kohto ette tulli Mihkli Orras (Põlgastest) ja kaibas, et temä röä kuhiliko, mis temäl Hurmimõisa maa pääl olnu ärä ollew söödetu. Nõuab 2 wakka rükki. Mõisa, Hauka ja Koolitare elaja ja hobese ollew sellel kahjotemäle tennu.
Mõisawalitseja Samuel Rõigas kutsuti kohto ette ja kui temäle kaibdus ette loeti wastutas seesama, et temä mitte ei tiia kes selle kahjo om tennu.
Johan Asi wastutas selle kaibduse pääle, kui temäle kaibdus ette loeti, et temä ei ollew kedagi nännu kelle hobese ehk elaja sääl kuhilike pääl omma olnu.
Karjapois Aadam Hõim, ütles niisama kui Johan Asi, et temä elajä ei ollew mitte neide kuhilike pääle saanu, ei ollew ka kellegi tõise hobesid ega ejajid nännu sääl kuhilike pääl.
Mõisa karjusse Kusta Jahu ja Leena Herne ollew pääkohtumehele tunnistanu, et mõisa eläjä ollew mõne kõrra neide kuhilike pääle puttunu ja koolmeistre hobese ollew katskõrd kuhiliko pääl olnu kuna ütskõrd koolmeistre oma pois Johan Asi ja tõistkõrd nemä (mõisa karjusse) nee hobese kuhiliko päält ärä ollew ajanu.
Otsus: Kohtomeeste ülekaemise perrä, kes selle kahjo üle om kaenu arwab kogokonna kohus selle kahjo 1 mõõt rükki wäärt ollew, mida mõisa karjusse peawa masma, selle et nemä esi hendä süüdlases omma tunnistanu.
Kui kohton käüjule kogukonna kohto otsus ja Sääduse §§ 773 ja 774 ette saiwa loetu tunnistas Mihkli Oras, et temä selle otsusega rahul om niisama ka mõisa karjusse.
Peräst otsuse kuulutamist ütles Mihkli Oras, et temä ei nõua mitte midagi.
J. Tagel xxx
A. Org xxx
G. Kolk +++
