Peeter Asi kaibduse asjan Hurmimõisa rahwa wasto, rõugo söötmise perast / vide № 56 1879/ oli täämba kohto ette  tulnu
Andres Thal /Heisrist/ tunnistajas, kes ütles et mõisa tsea omme kül Peeter Asi rõugu pääl sööman käünü mittu päiwä, selle et temä 5 päiwä Hurmi mõisan töön olnu. Ei teedwa mitte kelle tsea nee olnu, kas moona rahwa ehk walitseja tsea, seda aga nännu, et tsea mõisast tullu rõugu pääle lännu ja säält jälle mõisa tagasi.
Joh. Pähn xxx
Joh. Purasson xxx
Mihkl Laine xxx
