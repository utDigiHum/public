Saije see poolele jäänu  № 88 ette wõetus ja Johann Sulg üles antu tunistaia kulleldus.
Tunistaia Peter Perli tunnistab: et tol kõrral kui tu Moraste warandus oli kirjotetu ei ole Paidrat sääl olnu, pääle tuud tõistkõrda om sääl maian seletetu sis üttelnu Paidra, (olnuwa ka toise kohtomehe man) Jaan Kirber ja Karl Weiko oma siist ja Johann Sulg om kõige lähemb sugulane, oma Moraltse latse wöörmündri.
Tunistaia Johann Juur tunistab, et tu kõrd om Johann Sulg saanu Moraste wöörmündris nimitetus. Paidra üttelnu, et temä wõiwad egän paigan kohut pidada.
Jaan Hämäläne tunistab, et temä ei olewad midagi kuulnu ei ka nännu kui tuud kraami kirjoteti ehk wöörmündri oliwa säetu.
Tunistaijis Jakob Kahre ja Jüri Jõgewa tunistawa et nemä mitte ei teedwad kas Sulg om wöörmündres nimitetu ehk mitte.
Jääb nii kauges kuni tunistaia saawa.
Pääkohtumees Andrus Nikopensius
Kohtumees Peter Nemwalz
   "  Joh. Nikopensius
