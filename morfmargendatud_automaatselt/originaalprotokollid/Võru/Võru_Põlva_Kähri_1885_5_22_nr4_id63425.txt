Ette tuli Wiru Weski mölder Otto Leis ja palus oma kontrakti, mis tema nimetud weski üle Punaku küla peremeestega maha on teinud lepingu raamatusse üleskirjutada ja kontrakti tegiatest teda ärakinnitada lasta ja siis seda kinnitust (allakirjutust) Kogukonna Kohtu poolt kohtu allkirjaga tunistada.
Kontrakt
Tänasel pääwal tegid Punaku Kolga peremehed: Peter Ussin, Andres Nikopensius, Johann Sokk Jaan Holz, Jaan Mandli, Peter Jõgewa ja Carl Pillberg Erastwere wallast Otto Leis, ka järelseiswa kontrakti maha.
Eespool nimetud peremehed annawad Wiru Weski, mille eesõiguse praegune mölder Johann Link, Otto Leis'le 310 rubla eest äramüünud on järelseiswa tingimistega Otto Leis kätte.
§ 1.
Otto Leis wõttab Wiru weski Lingi käest, nende oma wahel tehtud lepingu järele 19mal Decembril 1884 oma kätte kuni 23ma Aprillini 1914 (tuhat üheksasada neliteistkümmend)
2.
Weski on Leisi käes 23mast Aprillist 1884 pääle 15 aastad ilma rendita see on kuni 23 Aprillini 1899. 23mast Aprillist 1899 pääle kuni 23 Aprillini 1914 maksab aga Otto Leis peremeestele igaaasta (100) sada rubla renti.
3.
23mal Aprillil 1914 lõpeb see kontrakti aeg otsa ja peremehed wõiwad Otto Leis'le uued tingimised ette panda. On Otto Leis nende tingimistega rahul, siis ei wõi weski mite teiste kätte äraantud saada waid jääb Leis kätte edasi. Ei taha aga Leis uuesti ettepantud tingimistega mite rahul olla, siis annab tema weski käest ära ja masinad mis tema weskile on juure ehitanud jääwad weskiga peremeestele millede eest aga peremehed Leis'le nende wäärt olew hind peawad wälja maksma./Praegune weski on üheainukese jahwatuse kiwiga/. Weski peab äraandmise juures hääs korras olema.
4.
Otto Leis lubab selle weski juure pääle jahwatamise ehitada: linaajamise ja willakraasimise masinad, linaseemne puhastus, kruubi masin ja rõiwawanutus ja kui aerwab ka püügli löömise.
§ 5.
Kõigile eespool nimitud peremeestele ja ka Koolmeistrile jahwatab Leis nende jahwatuse, ajab linad, kraasib willad, ja wanutab Kangad ilmamaksuta. Muude masinate töö mis Leis ehk weski juure ehitama saab ei saa mite peremeestele ilmamaksuta tehtud. Korra ootamise õigus on peremeestele teiste jahwatajatega üteline. Jahwatab keegi nendest peremeestest wõõrast jahwatust ehk teeb muud tööd oma nime all ilmmaksuta, ja tuleb see awalikuks siis maksab süüdlane 25 rubla trahwi möldre hääks. Niisama maksab ka mölder, kui kellegile midagi wilja juurest ehk muu juurest ära wõttab ja see awalikuks tuleb 25 rubla kahju saaja hääks trahwi.
§ 6.
Weskiga ühe saab Leis 1 wakamaa maad, 3 wakamaad rendiwad weel peremehed juure ja selle 3 wakamaa eest maksab mölder igaaasta 9 rubla renti.
§ 7.
Möldre lehm käib suwel külakarjas. Karjatse palga maksab mölder karjatsele ise nii kui karjussega oma wahel lepiwad. Hobust wõib mölder külakoplis pidada ja kui niidud lahti on siis ka niidu pääl.
§ 8.
Järwe wesi wõib nii korgeks tõstetud saada, et küla maije seest mite wälja ei läha.
§ 9.
Mis järwe weeläbi keegi kahju saab wastutawad külaperemehed ja nimelt Peter Usinale, kes selle wee läbi kahju saab õijendawad külaperemehed omast ütsilisest koplist heinamaaga Johan Sokka aija nukka juurest ütsijakku edasi selle suure kiwi pääle.
§ 10.
Selle kontrakti wäärtus lähab niihästi peremeeste kui ka Leisi poolt nende pärandajate pääle üle.
§ 11.
Seda kahju mis Andres Nikopensius ehk selle läbi saab, et wesi tema nurme perwe sissemurrab lubab mölder tema wäärtust mööda tasuda.
Seda kontrakti tunistawad kontraktitegijad oma käe allakirjutamisega.
Kähris, 19 November 1884.
Weski rendi pääle andjad
Johann Sokk [allkiri]
Karl Pillberg XXX
Peter Jõgewa XXX
Jaan Holz XXX
Jaan Mandli [allkiri]
Rendi pääle wõtja. Otto Leis [allkiri]
Et Johann Sokk Karl Pillberg, Peter Jõgewa, Jaan Holz, Jaan Mandli seda kontrakti oma käega allakirjutanud saab kogukonna kohtupoolt tunistud.
7.August 1886
Pääkohtumees Jaan Paabo [allkiri]
Kohtumees: Peter Kirrepa [allkiri]
do: Kusta Seba [allkiri]
Kirjutaja M. Bergmann [allkiri]
 
 
