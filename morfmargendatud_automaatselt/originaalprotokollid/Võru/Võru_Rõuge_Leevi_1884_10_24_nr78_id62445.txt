N 78.
Lewi kogokona kohus sel 15 ma Nowembril 1884
Koos olliwa Pääkohtumees Jaan Thalfeld
Kohtomees Kusta Moistus
" Johann Suits
Selle 24 ta Octobri N 77 kaebuse päle sai üles antu tunnistaja Jaan Lattik Pallomoisa sulane ettekutsutud kes tunnist et sel õhtul kui paletu om warastud om temma Pallomoisa polt tulnu ja sis om Simon Sulg temale wasta tulnu kelle tema om üttelnu kos sa pois nüüd lähat selle päl om tema wastanu et mina lään nüüd wälja hulkma. selle kõnelemise man om siis Jaan Lattik tähele pandnu et Simon Sulg om üts must palito kaska ala aetu. Tõist kõrd om Jaan Lattik jälle temaga kokko saanu ja om tema käest küsinu et sina kül paleto olet wõtnu ent ütle mes sina temaga teed ehk kos tema pannit. Selle päle om Simon Sulg üttelnu Mes mina temaga nüüd teen minna panni sinna Samma O ina   poole karpa ütte walge lina sisse nüüd käwwe kaeman olle küll weel sääl saman. Päle selle tulli weel ette Jaan Kaosaar kes ka sest paleto wargusest teedwad ja se andse wastust et temma ka nisamma paljo teap kui Jaan Lattik kellest Jaan Kaosar ka nisama karja mehe tütre Anu Hemmale nisama om kõnelnu. Selles päiwas ette kutsutud Hendrik Raudsaar es tija sest paleto wargusest midagi üttelda.
Moistmine
Kuna Simon Sulg päle warguse koddus es olle ja Jaan Lattik ja Jaan Kaosaar tunnistuse utte läwa peab Simon Sulg paletu omaniko nõudmise perra 20 Rubla paletu eest ja 3 Rubla waeste kassa 2 nädali aja sees ärra masma. Moistmine saije § 773 ja 774 ette kuulutud.
Pääkohtomees [allkiri] Jaan Thalfeld
Kohtomees [alliri] Johan Suits
" [allkiri] Kusta Moistus
Kirjotaja [allkiri] P Perle
Appellati on perra antu sel 22 Nowembril  1884.
