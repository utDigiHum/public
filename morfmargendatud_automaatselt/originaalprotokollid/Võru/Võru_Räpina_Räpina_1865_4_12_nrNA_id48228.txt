Maddisson Wöbso linna Möldre tullis kohto ette ja pallus temma tule Wesk Wöbso linnan mis ta Jagrile pambs om andno, temmale taggasi moista, ta tahhab temmale omma wölg erratassutama ta andis kats rechnungi sisse, üts rehnung mis temma Jaegri kaest nauda, toine mis ta Jaegre kaest on sano, ta pallus Michle Podder tunnistus kulda.
Michel Pödder wannemast tunnistas: Maddison om Jagri 2 teomehhet kik talwe ja suwwe ajal sötno.
Jaeger esse sai kohto ette kutsutud, aga mitte tulno: selle perrast sai moistetud tullewa eespaew Jaeger ja Maddison jalle kohto ette kutsuda.
