Kadunu Peeter Mõttuse waeste laste ja läsja wöörmundri Hindrik ja Jaan Tagel  olliwa täämba kohto ette tulno
kos Hindrik Tagel ütles, et see perra jäänu warandus kõik alale om ja kõik muido olek ka õige kõrran om.
Jaan Tagel wastutas ka niisama kui Hindrik Tagel, et warandus kõik alale om ja muido asi ka joonen.
J. Tagel xxx
A. Org xxx
G. Kolk xxx
Kirjotaja Luik [allkiri]
