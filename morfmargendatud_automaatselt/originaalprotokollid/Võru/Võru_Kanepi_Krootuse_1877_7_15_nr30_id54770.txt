Tulli ette Hans Lodus (Zolgilt) ja kaibas, et Hilba tallo zikku ollnu 7 tükki ja zonginuwa temma nisso ja tatriko koggoniste ärra, nink niida jo mitto kõrda om sündinu, mes kohtomees, Johan Raig ollewa ülle kaenu, nink nõüdis kahjotassomist 1. wakk nisso ja 1. mõõt tatriko
Kutsuti ette Hilba mölder A. Treffner, ja tem(m)a rentnik Jaan Wõsso, ja sai kaibus ette loetus kes ka es salgawa, ent ütliwa wist om ikkes karjusse hooletusse läbbi johtunu.
Sai ka kohtomees Johan Raig käest küssitus kes olli ülle kaenu, nink ütles, nisso wois ikkes kuwwe seemne pääle arwata, ja kuwwes ossa om kahjo, nink tatriko olli 6 sammo kiigipiddi zongitu, ent muidu olli kik terwe. -
Otsus  Kohhus moistis, et kahju omma lasknuwa tetta ja kohtomehhe ülle kaemisse perra, massap Hilba tallo, Hans Loodus´el
1, mõõt nissu, ja (1/6) üts kuwwendik  wakk tatrikko, mes puhhas willi peap ollema, süggise ilm wasta pan(n)ematta kahjotassomis-
ses wälja, nink peawa eddespäidi omma ellaja perra kaemisse eest hoolt kandma, et mitte kahju ei sünni, ja lätsiwa rahhun wälja. -
                                                         (Allkirri)
