Gustav Oinas tullis kohto ette ja andis ülles ehk Jacob ja Jaan Heidow ommawa selle majast mis Adam Kesselmann se aasta sai ehhitud, moisa palkit om warrastano ja errawino ja ehk ta omma silmaga om näino kui Jacob Heidow 7 palkit Wöbson om wino.
Jaan Narruske majan ehhitamesse tarwis, ja ehk 2 teised warrastud palkid Jacob Heidowe murru paele ommawa mahha panto.
Pea Kohtomees Rein Pabusk ja Kohtomees Jacob Zengind tunnistawa ehk ka ommawa se Jaan Naruske uus ehhitud maja Wöbsun üllekaeno ja leidno ehk se ommawa erraprukitud 6 talla peddaja palkit, mis moisa warrastud palkid ühtis seltsis kiik teised palkid mis Jaan Naruske majan ommawa kulluno, ommawa wennemalest todud, ja teise seltsid Neid kats palkid mis Jacob Heidowe murro paele mahha ommawa pandud saiwa walla kubja polest üllekaetud, agga ta es woi mitte selge tunnistus anda kas na ommawa neid warrastud moisa palkid woi mitte.
Gustav Oinas tunnistas ehk ta om essi neino kui neid kats palkit ommawa warrastud ja sinna widud.
Sila Pebo tunnistas ehk sel ajal kui ta Kesselmanni maja teol om olno, ommawa sest 2 palket errakaono.
Nisamma tunnistas Jaan Silats
Peter Teppaskow tunnistas ehl 3 palkid ommawa selt majast errakaono kui ta sel teol om olno.
sai moistetu. Jaan ja Jacob Heidow peawa 6 moisa palkidi est mis ommawa Jaan Naruske majan erraprugitud ja mis na ommawa warrastano, moisale 8 Rubl kahjo rahha massa, ja waiste ladiko 6 Rubla straf.
