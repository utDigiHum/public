1. Kusta Lattik Koorastest Luteri uski tuli siia kohtu ette ja kaebas, et minul on Hugo Kolk käet 8 Rbl saada, mida tema juba 1879 aastal 2 Augustil wõtnud ja lubas ka 5% all ära maksa. Kaks kord olen mina ilma asjata kohtu ees käinud, kuna Kolk ette es tule kelle eest mina 4 Rbl jalawaewa nõuan. Sellepärast palun mina kogukonna kohtu Hugo Kolka sundi seda 12 Rbl mino heaks ära maksa laske.
2. Hugo Kolk Luteri usku wastutas selle kaebduse peale, et mina olen kül 8 Rbl Gusta Lattik käest laenanud, aga siis olin kohalt kooli laps ja es tohi Lattik ilma mino wanemate loata, nagu kooli lapsele, laenata. Mida wanemad ära on keelanud maksa.
Kogkohtu mõistmine
"Hugo Kolk peab 8 Rbl Kusta Lattik heaks ja 1 Rbl jalawaewa 14 päewa sees tähe järele wälja maksma."
Mõistmine kuulutadi §§ 773 ja 774 põhjusel, siis es ole Hugo Kolk sellega rahul, waid palus opuslehte wälja edasi kaebamiseks.       /opusleht tagasi antud:
Peakohtunik: J. Narusk
Kohtunik: T. Weits [allkiri]
   "  T. Päkko
Kirjutaja: J Kets [allkiri]
1892 aastal Juuni kuu 17 paewal Kusta Lattikle täitmise leht N   all walja antud. Eesistuja:
