Niisama kaibab ka Kusta Kurre oma kadonu esä wõlla peräst, kes 16 rubla wõlgo olewad.
10 rubla andnu kadonu esä soola osta; kümne aasta tagasi, 5 rubla andnu hainu osta.
Dawit Pito ette kutsutu ja see kaibus kuulutetu ütlep et temä ei olewad Johann Kurre käest midagi lainanu, 5 rubla oli täl kül olnu Kurrele massa haino eest, tuud wõtno Kurre Serreste mõisast wälja kelle eest temä  sülle kiwwe oli wedänu. And tunistaia Kusta Weitz.
Jääb kuni tunistaia saab.
