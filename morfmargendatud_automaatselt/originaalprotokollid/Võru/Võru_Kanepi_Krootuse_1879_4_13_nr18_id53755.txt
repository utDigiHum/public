Johan Akkel tulli kohto ette ja kaibas, et kõrdsimees Jaan Sik ollew temä käest 5 Rbl. wõlas wõtnu, kellest ta 4 Rbl ärä masnu ja 1 Rbl. jätnu masmata. Pallep kohut kõrtsimeest sundi, et temä selle ütte ruubli ka ärä massas.
Jaan Sik wastutas selle kaibduse pääle, et temä selle perast seda 1 Rbl. mitte ei ollew masnu, et Akkelil temale 77 kop. kõrtsiwõlgo massa ollew.
Otsus:
Kõrdsimees peap 90 kop. Akkelile ära masma, ennegi 10 kop. mis Akkel tunistas wõlga ollew jääp masmata.
J. Tagel XXX
A.Org XXX
G.Kolk XXX
