Kaibas Kusta Parmson, et 5/6 Septembril öösel olewat tema aidast luku tagast, lukk jäänu rikmatus, ärawarastetu:3 paltod			wäärt 30 rbl.		1 pensak			wäärt 4 rbl.		3 undrukut			wäärt 12 rbl.		2 paari saapit			wäärt 9 rbl.		1 kübar			wäärt 2 rbl.		1 paar sukke			wäärt 40 K.		1 paar püksi			wäärt 2 rbl.		1 raamat			wäärt 1 rbl. 40 K.		1 wöörätt ja paar kindit			wäärt 1 rbl.		1 west			wäärt 1 rbl. 50 K.		Summa			63 rbl. 30 K.		
Selle warguse kätte andjas arwab tema
1) Daniel Ruusand seda saawad tunistama Jaan Nädal ja Mari Oinas.
2) Wiido Punnisoon, kes om ähwardanu teda, Parmsoni "kitsku", seda tunistawa Mari Oinas, Mari Lepson ja Peter Rämman ja Anu Kriguland.
3) Jaan Helfer, kes tema, Parmsoni, wiha mees olewat, selle perast et neide wahel om piiri protses olnu, mida Jaan Helfer om pidanu kaotama.
Daniel Ruusandit es ole woimalik kohtu ette kutsuda, selleperast tema om üle piiri wallast wälja lännu.
Wiido Punnisson wastutas selle kaibuse pääle, et tema ei olla kunagi Kusta Parmsoni ähwardanu ja ei teedwat sest wargusest midagi.
Jaan Helfer wastutas selle kaibuse pääle, et tema ei tija midagi sest wargusest.
Tunistas Jaan Nädal: Mari Oinas, Daniel Ruusand ja tema, Nädal, tulnuwa marju kormast, sääl ütelnu Mari Oinas:"Maske mulle üts rubla, siis mina juhata Kusta Parmsoni warastetu rõiwad ära, aga temale hendale ei taha ma juhatada, sest tema Parmson, om halb mees."Selle pääle ütelnu Daniel Ruusand:"Mina esi andsin need rõiwad kätte." Jaan Nädal ütelnu selle pääle, et mina wöi sinnu selle söna eest kohtu kätte anda. Daniel Ruusand wastutanu: "Ja, söna köidab, söna pästab."
Tunistas Mari Oinas: Jaan Nädal, Daniel Ruusand ja tema, Oinas, tulnuwa marju korjamast. Daniel Ruusandil olnu tingelpung ja 90 Kop. raha käen, tema, Oinas, küsinu nalja perast:"Anna mulle 10 rbl., siis mina juhata Kusta Parmsoni warastetu röiwad ära"? Selle pääle wastanud Ruusand:"Sulle anna woi 20 rbl. siis ka sina ei tija, kos need röiwad omawa."
Peter Rämmani talgun könelnu Wiido Punnisson Kusta Parmsonist ja mehitsist, aga ähwardamist ei ole temal, Oinasil, midagi meelen ja ei tija midagi eest tunistada. Anu Kriguland nöudnud küll üts körd tema, käest, kas Punnisson om Parmsoni ähwardanu, aga tema ei ole nõudjale midagi teednu seletada.
Tunistas Anu Kriguland: Mari Oinas om temale ütelnu, et see warguse kahju om köik Wiido Punnisooni ähwardamistest tulnu, nii kui ta Peter Rämmani talgun Parmsoni om ähwardanu. Wiido Punnisson olewat Mari Oinasi ütlemise perra üts neist wargadest.
Tunistas Peter Rämman: Wiido Punnisson olnu küll tema talgun, aga ähwardamist ei ole tema mitte määrastki kuulnu.
Tunistas Mari Lepson: Peter Rämmani talgun söögi lawwa manu könelnu Mari Oinas Wiido Runnissoniga, sellest könnest ei ole temal muud meelde jäänu kui Punnissoni ähwardamise sönnu:"Kusta Parmson üks saab saama, küll ma teda tunne"! Mari Oinas kaitsnu Parmsoni ütelden:"Sina ei tee Kusta Parmsonile midagi!" Muud ei ole temal sest jutust meelde jäänu, kui et W. Punnison ähwardas Kusta Parmsoni ja Mari Oinas kaitsis teda.
Otsus.
Kohus tegi otsuses, seda asja Kusta Parmsoni nöudmise pääle Keiserliku Wõru Maakohtu kätte anda.
Pääkohtumees Joh. Lepland
Kohtumees J. Kirhäiding
Kohtumees J. Oinberg
