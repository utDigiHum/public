Hans Warres kaibas kohto een, et temmä om andno 1873 suwwe Pedo Naggelmanil wiljä alla maad 1/2 wakka olla röggä, 1/2 wakka keswä, 1/2 wakka olla kartowle alla maad, ja 1/3 olla ajamaad, mis Pedo päiwigä ärrä ei olle teenno ja Hans ollewa Pedol päiwi päle andno selle maa ärrä teenä ja Hanso Karjan ollewa Pedol käino 3 aastaiga 1 Lehm, ja 1 aastaig 2 Lehma, ja 1 rubla suwwe päält ollewa Lehmä eest Karjan käimisse hinda olno leppito, mis ka masmatta peab ollema, ja 1 aastaig ollewa ka 6 Lammast Pedol Hanso karjan käinoMaa eest nõuwap Hans			7 1/2 Rubla ja		Lehmä karjan käimisse			5 Rubla		Lamaste eest			1 Rubla		Kokko			13 1/2 Rubla		
mis Hans Pedo käest wäljä massetus nõuwap sada.
Pedo Naggelmann üttel küssimisse päle, et 1 1/2 wakka alla maad mitte ei olle olno, enge wähhämb olno ja 12 jalksi päiwä ollewa selle maa eest ärrä tenito olno 1873, ja leppito ei ollewa middägi olno ja Pedo Naggelmann üttel et temmä ei ollewa mitte Hanso käest maad wötno agga Jaan Kirruton ollewa selle maa teggia ja wõtja olno, ja se willi ollewa kül nemmäd ütten Jaan Kirrutoniga ärrä sönod.
Pedo Naggelmanil ollewa 1/2 wakka olla röä orrasid parilha Hanso maa pääl mis 1874 suwwel põimo saap.
Pedo Naggelmann üttel et enne ollewa temmä Hansol Lehmä karjan käimisse eest päiwäd tenno ja 1873 suwwel ei ollewa ennämb temmä Lehm Hanso Karjan kaino enge ollewa Tolama maa pääl käino.
1873 Suwwel ollewa Hans Pedo naise tömant ärrä aiano ja ei olle laskno ennämb tööd tettä.
Kohhus mõistis Hans Warrese kaibost tühjäs, essiti selle perräst et Lehmä Karjan käiminne päiwigä tenito ollewa ja wimäsel suwwel ennämb karjan käiä ei olle laskno, ja teisis et Hans Pedo naise tömant ärrä om ajano ja päiwi ärrä ei olla laskno teeno.
§773 om ette kulutedo
Hans Warres pallel prottokoli.
