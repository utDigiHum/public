Protokoll Nr 10
Lewi kogokonna kohus, sel 17 Mail 1878
Man olliwa päkohtumeees Jaan Thalfeldt
kohtumees Jaan Thaal
" Jakob Kriwa
Wanna Koijola walla walitsuse üllesandmise perra sest 24 Aprilist 1878 Nr 84 perra om tenistuste ärra lännu soldat Johan Wissel selle walla mehe Aleksander Järwpõllu käest 20 rubla raha, nink pallep sedda raha Wisse pallemise päle sisse nõuda temmale perra saatmises. Johann Suitz wabandab selle eest et Järwpõld täämba haiguse perrast mitte ette es woi tallitedus sada.
Otsus: et Johann Järwpõld täämba siin ei olle, sis es woi weel selle asja ülle middagi mõista, nink saije sel [ -] sunil ette kutsutus.
Päkohtumees [allkiri] Jaan Thalfeldt.
kohtumees [allkiri] Jakkop Kriwa.
" [allkiri] Jaan Thal.
Kirjotas [allkiri] Kirop
