Asi 20. Febr. c. sub  № 14 järeldatud:
Juhan Kransmann tunnistanud, et Sibul ütelnud Päiwale enne: "Sa tahad seda koeera wist ärawarastada." Päiw kostnud:" Otsi mu maja läbi kui tahad, sina wõit ise teda warastada tahta."
Mõistus:
Tunnistuste järele on mõlemad süüdlased. Sibul aga weel rohkem, sest et ta enne oli sõimanud, sellepärast 
Otsus:
Sibulil in 3 rubl. ja J. Päiwal 2 rubl. trahwi selle waestekassa heaks 2. nädalani a dato äramaksta.
See otsus sai nende kohtukäiatele Liiwl. Talur. Säd. §  773 järele kuulutatud. Kristian Sibul ei olnud sellega rahule ja andis üles, et suuremat kohut tahab käia.
Kopia antud 9. Märtsil c.
Pääkohtunik: J. Nilbe [allkiri]
Kohtunik : J. Aripmann [allkiri]
Kirjut. Zuping [allkiri]
