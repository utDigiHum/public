No protocolli eddesiminnek.
Tulli ette Ants Liwak nink üttel et temma olli keswi külwanud, ja karjus olli slönna toonud et kolm innemist om mötsan, ja temma olli ka mötsa läinud, agga es olle keddagit mötsan olnud ja eddesin lännun olli nännu et Marta olli zikku pääk olnu ja Marta olli höikanu et piasi zigga ärrä ajama ja karjuse olliwa ka Zigga ärräajanud, ja sus olli temma (Liiwak) temma (Marta Saweljew) mant ärrä läinud mötsa kaema, nink kui tagasi olli tulnu sis olli Marta hainu pöimnu, ja olli lasknud kottist ärrä puistata.
Se ollew kül tötte et katz innemist ommawa Juhan Kirristaja ussaita lannud se olli Kirrisataja naine olnud kes latsega olli Kikkalt leiba toonud. Selle ütteluse pääle et nüt se uses ollewad olnuollew temma Werriorra wallawalitsuse manno läinud nink ollew tedda palunud Juhan Kirristajat läbbiotsima tulla, nink ollew ka Werriorra wallawalitsus temma (Kirristja) hooned läbi otsnud enge ei olewad midagit läudnud.
Pääkohtunik: Peter Urtson
kohutnik: Kusta Rämman
kohtunik: Joosep Oinberg
kirjotaja F Wreimann &lt;allkiri&gt;
