Kristjan Jürgenson kaibas et Johann Sääsk sõimawad tedä wargas, mes mitte õige ei olewad.
Johan Sääsk ütlep et Kristjan olewad kül mittund temä asja putnu, olewad temä kapstidgi warastanu.
Hindrik Raig tunistab et temä om nännu et Kristjani naise emä om olnu Johan Sääsk kapstin.
Otsus:
Et selget tunistust ei ole mõistab kogokonna kohus sedä kaibust tühjas, ent sedä kinnitab et edespidi ei tohi enämb tapelust ei ka sõimamist olla.
