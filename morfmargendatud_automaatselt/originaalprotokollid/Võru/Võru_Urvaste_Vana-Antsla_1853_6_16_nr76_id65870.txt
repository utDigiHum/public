Wastsemõisa mees Hans Hirse kaiwas selle kohtu een, et sellemoisa nurmewaht Johan Otz temma poiga Jaani pesnu olles, nink se ollew selle ollnu, et temma poig om moisa nurme päält ristikhainu kaknu.
Jaan Hirse tunnist, et temma om kül moisa nurme päält ristikhainu kaknu ja koddu tahtnu wassikili wija. 
Johann Otz tunnist et temma mitte sedda Jaani lönu ei olla, ennege om tedda Ristikhaina wargusse pääl tabbanu ja temma särgi säljast ärra wõtnu, et kül Johann olli Jaani käsknu temmaga liggi moisate minna, etnt Jaan olli wasta pandnu. 
Otsus:
Nida kui Jaan Hirse essi tunnist, et temma moisa nurme pält ristikhainu warrastanu om, ja sedda üttegi tunnistussega selges es jouwwa tetta et Johann tedda pesnu olles, sis sai temma 15 witsa lögika karristedus nida kui sädusse Ramat § 1137 kinnitab nurmewargaka. 
ja selle särgi pantmisse hinda 50 kop. hõb. ni kui §1131 kinnitab. 
