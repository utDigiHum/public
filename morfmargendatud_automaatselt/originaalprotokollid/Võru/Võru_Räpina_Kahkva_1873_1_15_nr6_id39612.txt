Kaibus.
Kaibas Jakob Parmson, temma ollewat Peter Rämmanile 20 punda ölgi wölgo andnu, ning saap nüüt 1 1/2 aastaja taggasi.
wäljanöudminne.
Ette tulli Peter Rämman ja wastutas, se küll öige ollewat, ent temma ei olle selle perrast neid lainatu ölgi mitte tassunu, selle et Jakob Parmsoni ellajet ommawa keik se suwwi temma nurme wilja ärra riknu, kui Jakob Parmson temmale sedda kahjo tassup, siis massao ka temme ne olle taggasi; Selle pääle üttel Peter Rämman, neid 20 kubbu olgi ei olle temma, ei ka J. Parmson kaloga möötnu, ning üttel et igga kubbo olles ?? 10 naela kalota.
Moisteto.
Peter Rämman peab 10 leisikat ölgi taffasi massma, selle jääb 10 leisikat Peter Rämmani hääs et tema J. Parmsoni Lehma on wiljast kinni wötnu.
Möllemba jaiwa sellega rahhul.
Kristjan Rämmann Päkohtumees
Kusta Hanson Abbi
Fritz Runhanson Abbi
