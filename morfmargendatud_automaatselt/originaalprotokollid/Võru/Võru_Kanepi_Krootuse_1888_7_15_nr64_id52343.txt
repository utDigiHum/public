1. Kadunud Adam Teder perantaja Jüri Teder Erastwerest Luteri usku tuli siia kogukonna kohtu ette ja kaebas, et kaheksa aasta eest tagasi, wõttis Johan Sulg 10 wakka kartohwlid 'a wakk 70 kop kokko 7 Rbl. Sellepärast palun kohut, seda minu heaks kohe ära maksa laske.
2. Johan Sulg Luteri usku wastutas selle kaebduse peale, et see wõlg olla 14 aastad wana ja olen ära maksnud Jüri Tedrele, ei ole enamb midagi maksa.
Kog.kohtu mõistmine;
Jüri Teder kaebdus on tühjaks arwatud, sellepärast, et see wana wõlg on ja Sulg ära maksnud olla.
Jüri Teder peab 1 Rbl trahwi waeste laekale maksma, sellepärast, et tema ilma asjata on siia kaebanud ja J. Sulge waewanud.
Mõistmine kuulutadi Seaduse järele; sis es ole  Jüri Teder sellega rahul; waid palus protokolli wälja, edasi kaebamiseks. / Es wõtta wälja/
