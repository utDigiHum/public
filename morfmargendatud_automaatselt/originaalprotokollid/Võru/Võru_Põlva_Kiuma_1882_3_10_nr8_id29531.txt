Sai selle kogukonna walitsuse nõudmise peale selle walla Tartu kliinikusse põdemise peale saadetud ja seäl m.a. Novembri lõpul ärasurnud poismehe Andres Moistu järelejäänud waranduse seletamine ettewõetud. Seäduslikuks asja kindlakstegemiseks oli siitpoolt proklam sub № 2, 9. Jaanuarist c ametlikus lehes "Talurahwa kuulutajas" kolme kuu peale äramääratud, s.o. 9. Jaanuarist kunni 9. Märzini c. Sellel proklaama aeal ollid ennast kui wõlanõudjad ülesandnud: Selle wallawalitsus tema põdemise ja matmise peale kulutanud:Wõrule hospitali tohterdamise ja põetamise kulu			56 rbl.			66 k.		Wallas ülespidamine ja põetamine			   9  "			50 "		Tartu kliinikusse makstud			    1  "			40  "		Matmise peale kulunud			    5  "			92  "		                                                                      Summa			 73  rbl.			48 k.		
   
Jakob Aripmann Wana-Koiolast ülesandnud:
ühe kasuka eest 9 rbl. 3me kuuse lehma söötmise eest 9 ja ühe särgi eest 2; ülekõige 20 rubl. saada
Nõnda on ülekõige wõlanõudjate nõudmised 93 rbl. 48 kop.
Sellewastu aga ei olnud Andres Moistul sugugi warandust, aga kül tema enese põdemise aeal järelseiswalt nimitatud wõlglased, kes aasta eest, kui ta Wana-Koiola Wisse Kõrtsis müüjaks oli, temale seäl weel wõlgu olla jäänud, ülesantud: Wana-Koiolast Jaan Kooskord 971 kop. Jaan Oina 342 kop. Saarepuu 72 k. Labbi An 55 k. Moona Kaarel 52 k. Ootsi Joh. Haidak 411 k. herra Jukku 92 k. üle Soldani sõsar 30 k. Jaan Westberg 422 k. Jaan Kääring 206 k. Kolmann 16,01 k. Timost Joosep Raudsep 761 k. Wastse-Piigandist maamõetaja herra Jacoby 100 k. Perilt Reidhardt 124 k. Põlwast M. Kalk 25. pudelite eest 125 k. Kiomast Jaan Matson rüki eest 630 k. Proklaama aeal ennast ülesandnud Wõrust Jacob Kübarsep 6 rbl., mis sissama ka ära maksis
Nõnda on sellejärele ülekõige 67 rbl. 44 kop wõlgasi.
Otsustati: Kõik, nii heasti need ülesantud wõlanõudja kui ka wõlglased 24. Märtsiks  siia seletamisele ettekäsutada lasta.
Pääkohtom: J. Nilbe [allkiri]
kohtom. A. Luk xxx
d.:          P. Aabel xxx
Kirjut.as. Zuping [allkiri]
                                                               
