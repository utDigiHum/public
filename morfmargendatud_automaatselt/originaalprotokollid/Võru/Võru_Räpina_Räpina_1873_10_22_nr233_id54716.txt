Johann Hindrikson kaibas kohto een, et Jürri Suik ja Jakob Pedoson ommawa temmä käest, Nahhalt köndo minnen tee pääl 6 Rubla rahha ärrä wõtnowa sis kui temmä maggama om jänud wankre pääl, ja perräst om temmä kulda ja nätta sanod omma ninna rätti ja wööd Jürri käen mis ka sel körral ärrä om kaddono temmä käest
Jakob Pedoson üttel et temmä ei olle temmä rahha silmägä nanno ja ei olle ka wõtno ja ei tea ka middägi.
Jürri Suik üttel, et temmä silm ei olle mitte nänno ja ei tea ka sest rahhast middägi. HIndriko ninnä rätti ei pea Jürri teedmä, agga wö ollewa temmä te pääl kül ärrä wõtno selle eest et  Hindrikson temmä kala salli om katki kiskno pahheli.
Josep Häidson ei tea middägi tunnistada.
Kohhus mõistis et Jürri Suik ja Jakob Pedoson kumbagi 24 tundi törman peawa ollema Ja wö peab Jürri Johann Hindriksonil taggasi andma.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Press
Kohtomees Kristjan Rusand
Kohtomees Gustaw Tolmusk
