Aadam Jahu tulli kohto ette ja kaibas, et temäl ollew Johan Kuhi käest, kes temä maja äräostnu, 95 Rbl. eestmineki raha saada, kes sedä aga temäle mitte seni ajani ärä ei ole masnu. Pallep, et Kog. kohus wõttas J. Kuhja sundi sedä raha temäle ärä massa.
Otsus: Et Juhan Kuhi kohto ette ei ole tulnu, sis saap tedä weel kõrd kutsutu, kui temä tõisel kohtopäiwäl ka mitte ei tule, sis saap ilma temäta kohto otus kuulutedu.
J. Tagel xxx
A. Org xxx
