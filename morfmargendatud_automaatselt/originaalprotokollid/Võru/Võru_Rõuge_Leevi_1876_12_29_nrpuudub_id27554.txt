Sel 29. mal December 1876
Pähkohtumees Jaan Thalfeld
Abbi kohtomees Johan Kausarw
" Johan Suitz.
Et tulli Pallo wallast Willem Jürgenson ja kaibas, et Jaan Meus om tedda Ruskuga Rundu ja läwest wälja wissanu. Kui ta ommi olgi om küssinu mis temma om Meusse pol arra pesnu suggise - ja Jaan Meus om nõudnu oma wölka tema käst, Willem Jürgeson nöwwap ütte lögi est 3 Rubla ja omma olle kotte. Jaan Meus tulli ette ja ütle et temma olgi ei anna enne kui Willem Jürgeson temma wõlg peap ärra masma sis sa olle kätte, 3 hobbesse ja 2 jalksi päiwe  ülle kätte jalksi ja 3 hobbese päiwa est 2 Rubla nõwwap. -
I Tunnistaja Hendrik Karbläne ütles et Jaan Meus om Willem  Jürgenson Rinda piddi kinni wõtnu ja wasta läwwe wissanu ja üttelnu et kassi wälja. -
Mostet
et üts hobbesse päiw wina tuwwa 25 Go ja tõine hobbese päiw linnol käinu 25 Go ja Praili tüa teggemisse est 50 Go ja katte jalksi päiwa 50 Gob ütte kokko 1,50 Gob. - et kohhus arwanu toukasisse est 1 Rubla Willem Jürgenson massa, sis peap weel Willem Jürgenson Jaan Meussele masma neide paive ist 50 Go sis sa Willem Jürgenson omma olle katte.
Se moistminne om 773 ja 774 perra mõllembile kulutedu.
Pähkohtumees Jaan Thalfeldt [allkiri]
Abbi kohtumees Johan Kausarw
" Johan Suitz
XXX
XXX
