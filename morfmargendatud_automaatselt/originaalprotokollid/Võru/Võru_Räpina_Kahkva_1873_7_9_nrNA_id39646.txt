Kaibus.
Kaibas Johan Otsing temma ollu minnewa aasta wellega ütten majan, ning ollu temmal sest maast 4jas jaggo piddada, ent enne temma wälja minnekit tennu temma omma jaggo rükki sitta pääle mahha, ning olla weel küttise röa mötsma pääle tettu, keddas temma welli Karl nüüd hendale perritseta tahhab ja mitte temmal põima ei tahha laske.
wäljanöudminne:
Karl Otsing tulli ette ja wastutas, temma wellil ei olle neid rükki mitte oiguses saija, selle et temma wäljalännu om, siis ei anna ei ka lasse mitte ne röa Johanil põima.
Moisteto.
Johan Otingele jääwa ne küttise röa, ja kui Karl Otsing ne röa omman majan Johanil pessa lassep, siis wõiwa ne olle Karlile jääta, ent kui mitte, siis wöib Johan mujal pessa, ning tekko nende olletega mes temma essi arwas.
K. Rämmann
K. Hanson
F. Runhanson
