Lepping Nr 41
Täämba sel 16e März 1870 om Sep Ants Samoson ja potsep meister Jaan Lill Korastest, wahhel sesinnane al kirjotetu lepping nida sündinu. Potsep Jaan Lill wottap Ants Samoson welja Jaan Samosoni henda manno ammetit opma, ja massap Jürripäiwast, Mardi päiwani eddimõtse 3 suwwe egga suwwe eest 20 Rubla ja neljas, se wimane suwwe eest 30 Rubla. Se opmisse aig, om nelli suwwe, allostap Nüid Jürripäiw 1870 päle, ja Künip 1874 aastani. Kats näddalit enne Jürripäiw om meistrel lubba tedda, henda manno wõtta, ent kui meistre Jaani päle sell aja henda man peas tarwitama, sis luppap meistre egga päiwa eest 50 kopka massa. Selle wastu luppap wöölmöldre Ants Samoson, et temma welli Jaan, eggan asjan meistre sõnna wõtliv ja allahetlik om, ei ka enne ommast ammeti opmisse ajast ei taggane, kui se aig täüs om. Selle tunnistusses, et neile se lepping om ette loetu, ja kik nida om, kui neide lepping om olnu, tõmbawa mõlemba omma kaega risti omma nimme taha. 
Meister Jaan Lill xxx
Oppipoisi wöölmöldre A. Samoson xxx
Walla wannemb Adam Sulg
Wöölmöldre Michel Samoson
Wöölmöldre Jakob Pahl
