1899 a 3 nowembril Ilmusid kohtu ette Kioma wallast Hiire talust surnud Hindrik Kaur pärijate eestkostja Johann Aadami p. Jahu ja Krootuse wallast Alasaija tallust Adami Kaur Jaani poeg ja palusid järgmist lepingud akti raamatuse üles wõtta ja kinnitata.
1. Adam Kaur on temal surnud Hindrik Kaur pärijatele weksli ja wõla kirja järele makssa olewa summa 52 rbl 25 kop tasumiseks omast warantusest järgmise waranduse 24 oktobril s.a. Hindrik Kaur pärijatele ära andnud.1. ühe puuteljedega wankri wäärt 			  6 rbl		2. Lina ajamise masin             "			12  "		3. Kaks werewat wasikat         "			  8  "		4. üks werew õhwake              " 			10  "					
5. kolm kiriwat põrsast          "						  9   "		6. üks werew ruun hobune    "			  7  "     25 kop		                                                 Summa 			52 rbl  25 kop		
2. See warantus on nimetatud paewal Johann Jahule katte antud ja sellest Ala- Saija talusse surnud Hindrik Kauri lese Sohwi Kaur hoole alla jäetud.
3. Et see kõik õige, warantusel asja kohane hind, et leping mitte wälja moeltud pole ja lepingu tegijad kõik waidlused selle wastu kõrwale jättawad tunnistawad nemad oma allkirjatega.
(J. Jahu A. Kaur (allkirjad))
Tunnistajad warantuse ära andmise juures ollid Krootuse wallast Jaan Kuhi ja Jakob Katalsep
(J. Jahu  A. Kaur (allkirjad))
J. Jahu [allkiri]  A. Kaur [allkiri]
Üks tuhat Kaheksasada üheksakümne üheksamal aastal Novembri kuu kolmandamal päewal on Krootuse wallakohus Krootuse wallamajas eesseiswat suusõnalist lepingut, mis Kioma wallast Hiire talust surnud Hindrik Kaur pärijate eestkostja Johann Adami poeg Jahu ja Krootuse wallast Alasaija talust Adami Jaani poeg Kaur wahel tehtud - kinnitanud, mis juures wallakohus tunnistab, et lepingu tegijad kohtule isikulikult tuttawad, et neil seaduslik õigus aktisi teha ja et peale selle lepingu ette lugemise temale, lepingule, Johann Jahu ja Adam Kaur tõeste oma käega allakirjutanud on.
Kohtueesistuja S. Raig [allkiri]
Kirjut ckuddo [allkiri]
