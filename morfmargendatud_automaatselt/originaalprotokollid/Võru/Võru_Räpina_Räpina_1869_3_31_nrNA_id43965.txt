Kui 17 Märzi prottokol ette olli loeto ja Peep Norroski käest küssitu uttel temmä kaeno üts weidikesse aiga om länno kui Hindrik körtsin om olno ni et keddägi wõrast selle aja sissen rahha es olled sanod ärrä wötta.
Hindrik Kiudorw tunnistas issi siin kohto een et temmä om nänno kus Peter Kerow rahha om pandnokoddon kus taskohe, et Peter Kerowil rahha olli, üttel Hindrik teedno, ja Hindrik üttel ka warsti ärrä nänno kui se rahha om ärrä kaddono
Kohhus möistis et Hindrik Kiudorw olli walla polest Peter Kerowi worimehhes anto ja et temmä näggi kui Peter jobnus päi ja ka seddä warsti teese kui rahha ärrä kaddus peab temmä poolt kahjo kandma Hindrik Kiudow peab 16 Rubla Peter Kerowil maksma 7 Aprillil wäljä anto §773 om ette loeto
Pä kohtomees Paap Kiudorw
Kohtomees Kristian Wössoberg
Kohtomees Jaan Holsting
Kohtomees Jakob Türkson
7 Aprillil wälja anto
N 153
