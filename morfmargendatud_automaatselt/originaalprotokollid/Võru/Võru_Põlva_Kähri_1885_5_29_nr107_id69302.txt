Suurest Cambjast Kusta Kalk ühes korwalseisja oma isa Martiga tulli ette ja kaebas et temale on 22 rub. 90 kop. Carl Kruuse käest 1884 aasta palka saada ja palub seda wälja nõuda.
Kährist her. Karl Kruuse wastab see pääle: See wõib olla et temale weel palka saada on aga nii palju temale saada ei ole. Jüri pääwal pidasime rehnungi ja siis tulli temale päält 9 rub. wälja. Jüri pääwa hommikul käsksin mina temale hobust ettepanda kus ma temale ka see raha 9 rub. pidin walja maksma, aga tema ei kuulanud mite minu käsku, seepärast ei annud mina temale ka palka enam kätte.  Praegu ei ole mulle palga wäljaandmise raamatud juures, seepärast palun seda seletamist kuni tulewa kohtu pääwani jätta.
Otsus.   Tulewaks kohtupääwaks uuesti ette talitada kus Kruuse palgawäljaandmise rehnungi ette toob.
Pääkohtumees Johann Waks
Kohtumees Jaan Salomon
do: Juhann Wasser [allkiri]
Kirjutaja M. Bergmann [allkiri]
