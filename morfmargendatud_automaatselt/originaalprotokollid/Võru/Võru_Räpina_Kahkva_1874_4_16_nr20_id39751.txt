Moisteto.
Kaddunu Josep Rämmanni perra jäänu lastele sai sest kigest maja hääst ja warrast, mes parhilla majan on, sedda wiisi tettus, ülle kolme poija
1., tütre Annele saap keige esiti päält woetus 1 Lammas 1 Lehm ja üks kirst
2., poija Kristjani naisele 1 Lehm ja 1 Lammas
3, poija Widole naisele 1 Lehm ja 1 Lammas
Siis wiimselt saap se paerrajanu ülle kolme welle 3mes jaos tettus, ni et egga üks 3mas ossa saap.
Ne linna massina mes nende kaddunu essa enne omma surma eggale ühhele poijale om jätnu, ja ni sama kui essa lubbanu, eggale poijale omma kätte. 
Pääkohtumees G. Anderson
Kohtomees P. Sabbal
Kohtomees J. Parmak
Sel. 19 Aprilil tunnist s.a. Wido Rämman, temma om omma welle Gustawiga selle otsusega muido kül rahhul, ent temma nöuwap sest 120 rub. Linna rahhast, mes temma kaddunu essast, temma welle Kristjani katte om sanu, ni samma kolmas ossa.
