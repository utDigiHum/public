Johann Sippul tahhap tõise abbielo sisse astuda nink lubbap omast eddimätsest abbielost perrä jänü lastele warrandust:
 1. Tüttär Katri saap 1 lehm ja 1 lammas,
2.    "       Marri saap 1 kirst,
3.    "       Ann saap 1 särk ja 1 lammas
laste wöörmondre om Peter Põld
Pääkohtumees K. Lõhmus [allkiri]
Kohtumees
        "             Peter Kallmn [allkiri]
Kirjotaja J. Jõgewa
