Nr 24.
Walla wannembaga nink Kohtumeestega, om Moisa Herra, alla nimmetetu peiwal, sedda Leppingut tennu, et Magasi Ait, jäb ligutamata, nink Wallale ei olle waya ütte töist soetada, selle et see wanna Ait, om koggoni sünza Kohha päle ehhitetu.
Weimren sel 22 Mai 1868.
Ev. Richter
Moisa Perrisherr.
