Wido Küitsoni naine Dora kaibas et Daniel Küitson om temmä sõlle ärrä warrastano 5 aastaiga aigo taggasi ja om jälle päle warrastamisse 3 Rubla eest ärrä müno Jaan Narroski läbbi Werro kuldseppäl.
Daniel Narroski käest sai küssitu üttel temmä õige ollewa ja tunnist seddä sõlge warrastano ollewad ja üttel 3 Rubl eest ärrä müno ja se rahha om nemmäd ladol ärrä jonod.
Jaan Narroski käest sai küssit üttel temmä seddä sõlge müno agga mitte ei mälleta kui paljo om maksnod.
Kohhus mõistis et Daniel Küitson peab maksma se sõlle eest sado 3 Rubla taggasi ja weel päle selle 3 Rubla trahwi Wido Küitsoni naise Dorä kätte ütte näddäli sissen.
Pä kohto mees Paap Kiudorw
Kohto mees Jaan Holsting
Kohto mees Hindrik Toding
