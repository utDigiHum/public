Koon olliwa nesamma üllewan nimmitetu kohtomehhed. 
Tulli kaibussega moisa wallitsus ette lihha wargusse pärrast, et Kema Hindrik om moisa aidast lihha warrastanu 12 nakla. Kema Hindrik Jeret es salga omma kurja tööd, enge üttel selgeste et temma olnu joobnu nink om kül warrastanu.
Kohhus moistis tälle temma kurja tö eest: 1) lihha hind; se om /:1 ütte rubla hõbbega Herrale massa; nink 2/ wargusse sü eest üts ihho trahw 25 witsa lögiga.
