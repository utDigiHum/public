Sell 6damal Octobril 1839 kaibas Ähijerwe Karli Naine Lisa et Ähijerwe Juck tedda wäga rängast pessnu ja temmale pangika pähe lönu.
Kutsuti Juck Kochto ette ja küssiti temma käst, kass temma sedda Naist om lönu?
Juck kosti et temma ei olle tedda lönu.
Too Naine tunnistas wäka kõwwaste et Juck tedda om lönu, ja sest et selle Naise Silma olliwa paistetanu ja werritse siis arwas Kohus et Juck tedda icks om lönu, ja panni neid leppima, agga see Naine es tahtno leppida, siis Kohus
moistis:
et Juck peab wiisteist kümmend löki selle pessmise est saama ja peab selle Naisele Kolm Rubla p.R. wallo Raha massma. 
See Naine ess ollnu selle Mõistmiseka rahu, ja palles Kirja Auwoliko Kihelkonna Kohto ette minna. 
Wanna Ans Mois sell 6mal Octbril 1839. 
Märt Silber
Koggokonna Wannemb
XXX
Karl Zimmir
Abbi Kochtomees
XXX
Johann Masick
Abbi Kochtomees
XXX
