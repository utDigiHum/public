Protokolli Nr 17 manno.
Lewi kogokonna kohus, sel 6 Septbr. 1878
Man olliwa päkohtomees Karl Lauk
kohtomees Jaan Thaal
abi " Hindrik Raudsaar.
Jaan Meos ütel kül Hindrik Kikka karja Jakob Kriwa karja ma pääl ollewat ent mitte wiljan. Selle päle es olle kohtun kauja ennamb middagi ütlemist ollewat sis
Mõisteti
et Hindrik Kikkas peap selle karräma pääl umma karjaga käümisee perrast tunnistuse perra 2 rubl. trahwi Kriwa hääs 14 pääwa sisen a dato sellest kohto otsuse kindmas jämisest ärra massma.
Päkohtumees [allkiri] Karl Lauk
kohtumees Jaan Thaal XXX
abi " Hindrik Raudsaar XXX
Kirjotj [allkiri] JKirrop
