Kui sel 4ta Dez 1867 aasta prottokol Wasseri Emmända ja Daniel Kangro wahhel selletusse asjon olli ette loeto üttel Daniel Kangro et temmäl om 50 Rubla rahha kappi jäänod ja rahha pabberid kost rahha sada om olno kelle käest wõlgo om olno sada, ja kappin om ka weel olno 2 habbena wäist ja 2 Sõrmust.
1.) om olno Milleri käest üts pabber seggäne
2.) om Jaan Narroski käst üts pabber 2 Rubla eest
3.) om Kaddaja Adam kaest üts pabber 10 Rubla eest
4.) om Loritz mõtsa küllä koli maja kontraht kindi jano nink eet säät 4 Rubla wäljä ei anta enne kui kontraht sääl om ; Siis saap see 4 Rubla kätte
Daniel Kangro üttel 2 aastaiga wasseri majan elläno.
Tunnistusse perräst jääs polele
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Jaan Holsting
