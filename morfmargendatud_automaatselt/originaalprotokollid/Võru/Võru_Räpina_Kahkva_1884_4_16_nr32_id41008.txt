Kaibas Juhan Naggelson et Prits Sabbal om Wiirkso körtsin körtsinaisega 5 rbl. ärräwahhetanud ja oli korten wiina wötnu ja olli se taggasi antud wahhetedu rahha 2 körd läbbi lugenud ja wäljalännud.
Kui tagasi olli tulnud sis olli ütelnu et  15 Cop puudus om, ja temma (Juhhan Naggelson) olli üttelnu et minna es wahheta rahha enge minno naine; sis olli Prits Sabbal temmale kats körd löönud, pääle se oli naine üttelnu et:"Mis sa meest pessat minna wahheti rahha," sis oli Prits Sabbal ka naisele lönu ja olli tahtnu tedda Juhhan Naggelsoni letti taggast wälja tömmata.
tunnistaja selle man omma Wido Juusar, Joosep Jöks, Märt Sepp, Joosep Parmak 
Wäljanöudmine
Kutsuti ette Prits Sabbal ja küssiti ka se om tõtte et temma om Juhan Naggelsoni nink naist pesnu, ja temma wastat et oli körtsin ütte kortina wiina wötnu ja 5 rbl. wahhetanud ja Naggelson olli tagasi andnud 1. 3 rbl paberi raha ja 1 rbl 75 Cop wask rahha Summa 4 rbl. 75 Cop. ja üttel et temma mitte ei ole löönud. 
selle man omma tunnistaja Fritz Leppisk Wido Usning, Joosep Parson, Wiido Oinas nink Kristjan Kübbar.
Otsus.
Juhan Naggelson ja Prists Sabbal leppisiwa henda wahhel niida wiisi ärrä et Prits Sabbal massap Juhan Naggelsonile 3 rbl. selle löömise eest.
Pääkohtumees: Peter Urtson
kohutnik: Kusta Rämman
kohtunik: Joosep Oinberg
