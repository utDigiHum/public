Ette tuli Johan Kõiw ja kaibas, et tema peremees Jaan Kuus olewat tema 2 lehma ja 3 lamba sitta ärawõtnu, arwata 40 koormat.
15 rbl. eest osti ma (Kõiw) haino mano, ka röa assemele palgas and ta mulle nii halwa röa, mis kahro kaara täüs oli, et kui ära puhastedu sai, sis tuli üts wak puhast kahro kaara wälja, nink niida saije mina sis ennegi 2 wakka rükki. Töö ole mina kõik ausaste äratennu.
Jaan Kuus tuli kohto kutsmise pääle ette, nink ütleb, et tema ei olewat Johan Kõiw'le mitte sitta anda lepnu. Kui rükki halw oli, milles sis Kõiw wasta wõt, nink kui naid kohtomehe man puhastedu sai, milles sis mino mano es kutsuta.
Mõistus
Jaan Kuus peab Johan Kõiwle 8 rbl. sitta tegemise eest siin 14. päiwa seen wälja masma.
Ent rükki om Johan Kõiw tool kõrral wastawõtnu nink peab sellega rahul olema.
Se kohtumõistus saije kohtunkäijile kuulutedus nink Tallor. S. r. 1860 prg. 773 j. 774 äraseletedus n.n.e.
Pääkohtumees: J. Kroon [allkiri]
Kohtumees: J. Pruwli [allkiri]
        "             J. Kõiw [allkiri]
Kirjutaja: J. Treu
Kuus
Edasi kaebanu Protokoll No 132 al ärasaadetu.
