Kaibas Mötsaesand Jakob Rosneck et eggale üttele perremehele om 3 süld puid antu nimmelt rehhe puid ja niida ka Johan Käisil, ja Käis ollew nüüd 2 süld hendale wötnud ja 1 sülle ollew Peter Kirhäidingel annud sedda ollew Jakob Urgard temmale selletanud, ja Paap Parmann tiidwad ka et se assi om tötte.
Tulli ette Jakob Urgard nink tunnist et nemmad olliwad ütten 3 süld puid raggonud, ja Johann Käisil olli 2 süld saanud ja Peter Kirhäidingel 3 süld.
Tunnist Paap Parmann et Peter Kirhäiding olli mötsast 3 süld toonud ja sedda temma ei tija paljus Johann Käisile widud seda temma om puist mis Peter Kirhäidinge morro pääle weetud nännud et 3 süld om olnud.
Pääkohtunik: Peter Urtson
Kohtunik: Kusta Rämman
Kohtunik: Joosep Oinberg
Kirjotaja F Wreimann &lt;allkiri&gt;
