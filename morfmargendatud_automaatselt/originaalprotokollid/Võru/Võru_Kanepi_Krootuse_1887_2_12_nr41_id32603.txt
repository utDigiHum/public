Poolele jäänut asjus sest 29 Januarist s.a. niida kui Protokoll No 39 tunnistap, lisas Grünberg weel jurde et tema weel tämba päiwani  oma raha kätte saanu ei ole, nink Jaan Läwi Warandus Ouksioni wiisil om ära müüdut, ja nüüd tema Jaan Läwwi wõlglaisi endit üles andwa, kel üttegi wõlla tahte ei ka tunnistust ette näidata ei ole, nink mina temal nimelt rendi masmise tarbis olen andnu, kelle pääle mina julge olin et rent een muid wõlglaisi käib.
Andis tunnistajas Walla Wöörmunder Jaan Lepäson nink Jaan Wõsu Karraskist.
Sai ette kutsotus Jaan Lepason kes ütles et Jaan Läwi R Grunberg käest rendi massmise tarbis raha om lainanu
Sai ette kutsotus Jaan Wõsu, kes wälja ütles, et R. Grünberg ja tema isa Tannil Wõsu, Jaan Läwwe eest perrajatse rendi masso tarbis omawa raha andnu.
Otsus   Kohus mõistis R Grünberg kaebust tühjas selle perast et tema mitte essi tood Jaan Läwe renti Kokkeri maja eest omanik Jakob Uibo kätte ei ole massnu, enge Jaan Läwel rendi massmise tarbis lainanu. -
Sai Kohto moistmine ja §§ 773 - 774 Kohtol kaiadel ette loetus ent R. Grünberg es ole selle perast rahul, et kiik Wana wõlglase kel rohkemb kui 10 ajastaiga saada, nink mõnel kel Wilja wõlga antu, woinu oma wõlg en(n)emba nouda ent nüüd peran Ouksioni hennast üles andwa palus Protokolli mes kohus ka lubas anda. -
                                                                       Pääkohtomees   J. Rocht
                                                                            Kohtomees
                                                                             Kohtomees
                                                                         Kirjotaja as. R. Grünberg
