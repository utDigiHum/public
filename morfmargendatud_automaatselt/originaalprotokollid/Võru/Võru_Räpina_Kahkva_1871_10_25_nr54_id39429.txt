Kaiwas Mötsasaks J. Rosneek et Otto Lemming om Mõisa mötsast omma Saima tarwis 10 Palki 9 Jalga pikka ja 8 Pud keige ossega ärra toonu. Neide paksus om
3 tükki a 8 tolli			16		4 tükki a 5 tolli			20		1 tük			4		2 tükki a 11 tolli			22		1 tük			10		1 tük			7		3 tükki a 8 tolli			24		1 tük			9 1/2		18 tük			130 1/2		
Ette tullu kaiwatawa Otto Lemming ja üttel et ne eddimatse 10 tükki ja perrast 4 tükki om temma kül Mõisa mõtsast toonu ja keige perramatse 4 tükki ei ollewat temma mitte Mõisa mõtsast toonu. Ja Mõtsawaht Johhan Dobre olles temmale 2 tükki lubbanu. Mõtsawaht Johhan Dobre ütel et temma ei olle O. Lemmingele mitte üts pind lubbanu.
Mõtsasaks pallel kohhut et ne hone ni kui Laut kelle ülle ka jo kohhus om ollu Kohtu keelo alla jääwa, ja se Sannale annam Mõtsasaks weel ni palju kui sinna waja peas ollema palke mannu et se Sann kogoni walmis saab.
Kohhus mõist et Otto Lemming essi omma suga om tunnistanu et temma 14 Palki warrastanu om peab 5 Rubla trahwi masma ja se San jääb ni kui ka Mötsasaks lubbanu om Koggokonna hääs ja Laut jääb weel perra kullemise pääle.
Kristjan Rämman
Kusta Hanson
Fritz Runhanson
Se 7 Februaril 1877 aastal tunnist Kristjan Rämman, et 5 Rbl Otto Lemminge raha Mötsasaks J. Roosnek temma käest nõuda jääs.
Pakohtomees G. Lepson
Kohtomees P. Sabbe J. Jöks
