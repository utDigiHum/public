Ette tuli Peter Punsson (Pung) ning palles kohut et kohus ülles kirjotas oma hääks arwamist, mes tema omile pojele nüüd ning peran surma jaoks lubab omast raudwarast.
Nimelt om tema poijale Mihklile 6 taadre maa  kätte andnu mes ka peran mino surma poeg Mihklile igawetetses omanduseks jääb, ning tõine pool 6 Taadred luban omale pojale Danielile peran oma surma igawetses omanduseks.
Kui peas olema et kumbgi mino poest tõine tõise käest seda 6 Taadrit maad peas omale tahdma siis massab tema tõisele 1000 (üks tuhat) rbl. wälja.
Kiik oma liikuwa waranduse olen mina neile ära jagganu, ning hoone kiik mõlembile pooles, mes mino peralt oma.
See on mino wiimne lepping ning lubamine omi laste hääks, mida oma nime otsa kolme risti tõmbamisega õiges tunnistan
Peter Punson XXX
Seda Peter Punson oma käelist kolme risti tõmbamist tunnistab kogukonna kohus õiges.
Pääkohtumees Jaa Järw [allkiri]
kohtumees Peter Jõks [allkiri]
kohtumees P. Sõrg [allkiri]
Kirjot. S. Härms [allkiri]
