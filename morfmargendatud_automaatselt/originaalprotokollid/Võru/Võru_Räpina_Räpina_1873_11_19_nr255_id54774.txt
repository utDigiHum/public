Jula Sultzmann kaibas kohto een et Jaan Otsman Meeksi mees om temmä käest 3 aastaiga aigo taggasi 1 Rubla rahha lainano mis Jaan nüüd mitte täembä päiwäni ärrä ei olle masno.
Jaan Otsmann üttel, et temmä mitte ei olle lainano Jula käest 1 Rubla rahha wölgo
Tijo Üllimson tunnistas nänno, kui Jula Otsmann om 1 Rubla lainano Jaan Otsmanil 3 aastaiga aaigo taggasi ja Tijo ollewa omma kastist andno seddä rahha Jula kätte Janil anda Jaan ollewa sel körral Räppinä weskilt tulno.
Kohhus mõistis, et Jaan Otsmann peab se 1 Rubla Jula Sulzmanil ärrä masma täembä päiw.
