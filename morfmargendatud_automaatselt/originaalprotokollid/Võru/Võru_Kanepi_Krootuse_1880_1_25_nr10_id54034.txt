Peeter Asi kaibduse asjan Hurmi mõisarahwa wastu /vide № 56 1879/  sai täämba kohto otsus kuulutedu.
Otsus:
 Et mõisa walitseja weel kõrra kohto ette om kutsutu, kon temä om üttelnu, et moonameeste tsea sel ajal joba kinni om olnu, aga mõisal ja Aadam Jahul weel, edimätsel 5 ja tõisel 3 tsikka wällan om olnu, sis
M õistse kogokonna kohus:
Aadam Jahu massap 1 wak keswi ja mõisawalitseja 1 wak keswi Peter Asjale.
Kui kogokonna kohto otsus ja Sääduse §§ 773 ja 774 kohtun käüjile ette saiwa loetu, sis ütles Peeter Asi ja mõisawalitseja Rõigas omma selle otsusega rahul ja Aadam Jahu ei ole mitte rahul, waid annap weel tunnistajat üles, et temä tsea mitte ei ole olnu ja pallep kohut asja edesi selletada.
Joh. Pähn xxx
Joh. Purasson xxx
Mihkl. Laine xxx
