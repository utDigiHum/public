Ette tulli Kallasse tallo eestkostja Hendrik Lewwask ja kaibas, et tõine perremees Hendrik Kallasse ei ollew leppingut piddanu mõtsa raggumisse perrast, waid ollew ka hao kiik hendale weddano ja nõudis 10 Rubla.
Sellepäle kutsuti Hendrik Kallasse kohtu ja loeti een saiswa kaibus ette ja temma ütles ehk kats koormat haggu wino ollewat ent mitte rohkemb, sest nogi ollew ladwa jaggo, selleperrast ehk ei pruugwat temma kül trahwi massa.
Kohtomõistus.
Kunna Hendrik Kallasse ei olle leppingut piddano, waid siski ülle kelo hendale haggo toost mõtsast koddo tonu, sis peap Hendrik Kallasse masma 14 päiwa sissen 60 kopkat Hendrik Kallassele trahwi.
Se kohtomõistus sai kohtomkäüijle kulutedus ja prg. 773- 774. Tal. säd. ram. 1860.a. ärra selletedus, mis oppust andwa suremba kohton käumissest ja Hendrik Lewwask es jä tolleperrast selle mõistmissega rahhul, et mis weidi trahwi mõistedi, ent Hendrik Kallasse jäije rahhul ja wimate sai neile prg. 881 perra se trükkito oppusse leht wälja antus.
Pääkohtomees Peter Pedras
Kohtomees Hendrik Hallop
Abbi   "         Ado Reimann
Kirjotaja P. Haberland
