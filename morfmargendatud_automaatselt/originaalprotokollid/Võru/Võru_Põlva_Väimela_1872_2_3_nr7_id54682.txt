N 7. Wäimellän koggokonna kohtu majan sel 3 Februaril 1872
Olli täwwelik koggokonna kohhus koon, tulli ette Soldat Jürri Sulg omma naise assemel et Jürri naise tunnistus om wõlsis lännu, sis mõistse koggokonna kohhus, et Marri Sulg peäp 3 Rubla trahwi masma waeste ladikohe, ja se Maja Perre naine Ann Sulg üttel, et Marri om ka Lits, sis mõistse koggokonna kohhus, et Ann Sulg peäp ka 1 Rubla koggokonna waeste ladikohe masma.
Man olliwa Pä kohtu mees Jürri Waask [allkiri] 2 Peter Pallo [allkiri] 3 Jaan Zuhna [allkiri]
Koggokonna kirjotaja S. Mattissoon
