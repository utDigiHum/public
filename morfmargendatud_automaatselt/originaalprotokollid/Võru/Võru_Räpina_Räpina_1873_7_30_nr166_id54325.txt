Peter Werwik kaibas kohto een, et Gustaw Mäus om teddä pesno Räppinä moisa kõrtsiman tee arrude pääl, ja om 1/2 toopi winä ka tahtno käest ärrä wõtta, Ja Gustaw Mäus ollewa teddä nuiaga pesno, werritses, ja om ninnä päält katki lönod se pesminne om sure te pääl olno. Aga kui paljo Gustaw teddä om pesno seddä temmä ei tea, ja ütleb henda pesmisse läbbi hullus olno jänod.
Gustaw Mäus ei olle kohto manno tulno.
Josep Ritsland tunnistas nänno kui Peter Werwik om länno, ja Gustaw Mäus om te orrade pääl perrä länno, ja om Petrel hakkano middägi käest ärrä kiskma, ja sis om tülli lännowa ja Gustaw om Peter Werwikul rindo lönod ja om Petre mahha lönod, ja om tohhiga pesno sis Peter om happi hakkano tännitämä Ja kui Josep appi om länno om Mäus üttelno et mis teil asja om ja om Josepil ka lönod.
Peep Songe koolmeister tunnistas kulu kui Gustaw Mäus om lönod 2 wõi 3 kor ni kui ratta peelt wasto kõwwasti om lönod, Petrel.
Jaan Jagomann tunnistas ka ni samma kui Josep Ritsmann, ja üttel et selle perräst om appi josnowa et ärrä tahtis tappa.
Gustaw Zernad tunnistas ka kulu kui plaksno om olno kui Mäus Petrel om lönod.
Se pesminne om olno öddago arwata kellä 9 aig.
Gustaw Mäuse perräst jääs polele 6 Augustini.
