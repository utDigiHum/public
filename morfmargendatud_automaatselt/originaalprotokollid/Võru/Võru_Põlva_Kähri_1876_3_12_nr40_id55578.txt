Keiserliko Kihelkonna kohto käsu pääle saije Ann Räss kaibus Peter Westberg wasta ette wõetus.
Ann Räss kaibab Peter Westberg pääle kes temäga olewad kokko elänu ja sis olewad nüüd lats saanu, ja nõuwab sellepääle, et Peter Westberg oli üttelnu kui mina sino ärä ei wõtta 200 Rubla massa, ja kui poeg saab, nüüd om poig, Ja nema elänuwa nii kokko, olnu üts kast ja rõiwa pandmine ni kui mees naisega.
Peter Westberg ette kutsutu ja see kaibus kuulutetu ütlep, et temä olnu Maritsa mõisan kos ka Ann Räss olnu, sis nännu kui Wenneläse awalikult hooramist Annega pidänuwa, sis oli Ann Räss temä kraami warastanu,  sis nakkanu temä takkan otsma ja saanu kätte. Sellepääle nakkanu Ann Räss teddä kiusama, ja ei et minol temäga tegemist ei ole.
Otsus: 
Kogokonna kohus mõistab, et neil selget tunistust ei ole, selle lubatu lepingo man, selle raha pooles massa, nii et kümme aastat järgi mööda ega aasta 10 rubla Peter Westberg Ann Räss peäb masma.
Otsus kuulutati §  773, 774.
Peter Westberg and üles, et temä otsusega rahul ei ole.
Pääkohtomees Andres Nikopensius
Kohtomees Peter Nemwalz
 do: Jaan Leib
