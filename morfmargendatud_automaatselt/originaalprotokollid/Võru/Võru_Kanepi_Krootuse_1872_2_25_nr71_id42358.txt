Juusal sel 25 Webruaril 1872 olli kogokonna kohus koon nimelt: Päkohtomees Jaan Tulus
                                                                                                                    Kohtomees Jaan Wärs
                                                                                                                     tõine      Peter Mandli
Ette tulli Sõreste Herra nink kaiwas: et temal ollew lamba wälla lastu, nink sis ollew Johan Seeme  n           lammast kinni ajanu nink sis olli Herra nee lamba Johan Seeme käest lasknu ära aija, nink Johan Seemele 50 kopka sinna saatnu; ent Johan ei ole neid lambid kätte andnu, nink nimelt ollew Johan 1 rubla rohkemb wõtnu, kui õigus wõtta olli, mis ka Sõreste herra olli perra saatnu.
Nink pääle see, kui lamba ara olliwa ära lugenu, olli üts lammas puudus olnu.
Sõreste Herra nõwwap omma 1 rubla tagasi; nink ka selle lamba eest tasumist 5 rubla.
Johan Seeme kutsuti ette: nink küsiti: Kost sa nu lamba kinni aijet, nink konas? Ta ütles: peran Jüri päiwa oma risdik haina päält aije ma nema oma karja manu, säält aije Kutsar  nink karja naine Liis Rattas noo lamba ära, nink Rattas masse kül 150 kop mulle ära, ent seda, kui paljo neid lambid olli seda mina es tija. 
I  Et se nüüd awalik olli, et Johan Seeme olli 1 rubla üle sääduse rohkemb  wõtnu, seda Johan es salga,
Mõist kohus: Johan Seeme peep se 1 rubla Sõreste herrale tagasi masma.
Sai ka kohtun käiale kohto otsus nink ka § 773,774 kuulutedus
