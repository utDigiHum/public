Neljas kohhos sel sammal päiwal  Kohtomehhe ne sam(m)a.
Tulli ette kaibaja känksep Jaan Just nink tõstis omma kaibust rentnik Heinrich Undritz pääle kes nida ütles
1malt et H Undritz om temmale katte nädali eest taggasi ülles üttelno kos Jaan Just todda ulles ütlemist wäega hillatses arwas, sest et neil aijastaija pääle ollo leppito sis pidano H Undritz temmale ennembide ulles ütlema. -
2selt kaibas et om H Undritz temma pikka õlle 171 kubbo ärra wõtno ja no õlle Jaan Just om(m)a olnowa. -
3dalt kaibas et H Undritz om 1 wakkamaa maad mes J Just käen om olno nink ka ülles künneto tem(m)a käest ärra wõtno
4dalt kaibas et H Undritz om temma käest üts tük hainamaad kos arwas kige wähhamb 1 rugga haino sawat ka ärra wõtno
5dalt et lubbap minno omma sitta ärra wõtta mes minnol ommist õlgidest tetto om. -
Kaibus jäije tõise kohto päiwa pääle selletada konni H. Undritz saap kohto tallitedos. -
                            Karraskij Kohtomajan sel 26dal Märtsil 1876
                                                     Pääkohtomees  Jaan Tullos
                                                         Kohtomees    Jaan Leppäson 
                                                   abbi        do          Johann Raig   
