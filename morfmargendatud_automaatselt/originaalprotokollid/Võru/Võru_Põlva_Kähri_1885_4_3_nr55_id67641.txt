Kähri mõisa walitsus tulli ette ja andis üles et Lepa karjamõisa rentnik Lille Rihm wõlgu on moisawalitsusele: renti 17 rub. ja 6 waka keswi 9 waka kaaru ja 5 Tsetweriku tatriku ja palub seda Lilo Rihma käest wäljanõuda.
Lilo Rihm tunistab seda nõudmist õigeks.
Mõistus.    Kogukonna kohus mõistab et Lilo Rihm peab 17 rub. renti, 6 waka keswi, 9 waka kaaru ja 5 Tsetweriku tatriku 2 nädali sees Kähri mõisa Walitsusele wälja maksma.
Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtuskäijatele etteloetud. Lilo Rihm ei olnud mite otsusega rahul.
Lilo Rihmle opuse leht 5 Aprillil c. № 119 al wälja antud.
Ärakiri №130 al  V. T.Õ. Kihelk.k. ettepantud.
 
 
