Jaan Narusk Luteri usku tuli siia kogukonna kohtu ette ja kaebas, et Mari Kiwi on oma elajatega nimelt 3 elajat, niidu peal olnud, kelle eest mina 1 Rbl 50 kop heina kahju nõuan. Sellepärast palun mina kogukonna kohut Mari Kiwi sundi seda mino heaks ära maksa laske.
2. Mari Kiwi Luteri usku j.s. Joh. Purasson wastutas selle kaebduse peale, et kolm elajat oli kül Jaan Narusk niidus.
"Kog kohtu mõistmine"
Mari Kiwi peab 1 Rbl 50 kop Jaan Narusk heaks 14 päewa sees wälja maksma.
ära makset
Mõistmine kuulutadi Seaduse järele; siis olid mõlemat sellega rahul.
Peakohtunik as. T Weits [allkiri]
Kohtunik T. Päkko
Walla wöörmdr. K. Sibul
Kirjutaja: JKets [allkiri]
