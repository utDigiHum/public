Protokoll Nr 15 manno
Lewi kogokonna kohus, sel 15 Septembril 1879.
Man olliwa päkohtumees Karl Lauk
kohtumees Jaan Taal
" Jakob Kriwa
Kutsmise perra olliwa ette tulnu Ann Meltz, Josep Urgand ja Peter Mattus. Ann Meltz and tunnistust, et temma sellest Johan Thalfeldti maja warrandusest middagi ei tija, ent sääl majan olnu kül tolkõrral 3 hobbest, 7 lehma, 4 zigga, 1 põrsas, 8 lammast ja päle selle om weel rohkemb ellajid sinna manno tudu, ent kelle perralt ne elaja´ kik olnu, sellest es ütle temma ka middagi teedwat, kui ennegi Johan Thalfeldt om kül temmale palka massnu. Josep Urgand and tunnistust, et temma sellest middagi ei tijä, kelle perralt se maja warrandus olnu, kui temma tenistusest ärra om lännu, sis om Jaan Thalfeldt temmale palka masnu. Peter Mattus Wastsest-Koijolast and tunnistust, et temma ka sedda warrandust kül nännu om, ent kelle perralt se olnu, sellest es ütle temma midagi teedwat.
Otsus: kohtun käujit Johann Thalfeldt ja Jaan Thalfeldt sel 27 Septembril ette kutsu.
Päkohtumees
kohtumees
"
kirjotji [allkiri] J Kirrop
