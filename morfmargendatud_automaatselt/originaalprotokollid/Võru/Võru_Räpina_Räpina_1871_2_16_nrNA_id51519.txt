Iwan Hiliporits Hersowa külläst Kulja kontori alt Petserimaakonnast Pihkwa kubbermangust kaibas kohto eenm et Mihkli &amp; Paap Warrusk 15 Webr öddago teddä om Radama körtsiman pesno nuiega ja kollakoga ja om hjusid piddi kiskno, siis om körtsimees Fompka katte wõra wennelessegä käest ärrä pästno, agga süidi ei tea mis eest om pesseto muud kui Hilip om Micjel Warruski käest piipo pallelno tõmmata mis Michel issi om andno ja perräst om üttelno et sinna tahhid pibo ärrä warrastada.
Ja pärräst 15 Webr hommingo om Pawel Warrusk körtsimant 4a werstä maad ärrä tullen Josep Tilksoniga perra josno, ja wennelesse hobbese kige pakla kormagä käest ärrä wõtno ja perräst omma koddo winowa ja koddost Räppinä kohto maja manno tonowa.
Ja hobbese käest ärrä wõtmisse aig om Josep Tilkson henda kohto mehhe tunnistano ollewad, öigussega hobbest käest ärrä wõtta Hilip ütlep ka kormast paklid pudus ollewad.
Pawel Warruski käest sai kussitu, üttel temmä et temmä ei olle mitte Hilipid pesno, ja Hobbest üttel temmä kül wõtno tee pääl wennelesse käest hobbest ärrä.
Josep Tilkson üttelno et temmä ei olle kohto mees ennäst üttelno ollewad.
Radama körtsimees Fompka tunnistas, et wennelene om Mihkli Warruski käest pibo tõmmata pallunud ja pärräst om Mihki wennelisse hjüsis kindi hakkano ja perräst pesma hakkawo Pawliga siis ei olle körtsimees ütsindä appi julgeno minna ja om 2 wennelest tee käijät appi pallunu Hilipid nende käest wallale pästma.
Mihkli Warruski käest sai küssitu, üttel temmä et must olli no monni pesja ma annin pibo tõmmata läksin piipo puhhust ärrä wõtma siis wennelenne hakkas pesma ja oli rindo pääl.
Wennelanne arwas 5 Pudt paklid pudus ollewa pudt 40 kop kokko 2 Rubla.
Mihkli Warrusk üttel et temmä majan ei olle wennelesse paklid ärrä wõeto wennelesse koormast.
Et Pawel Warrusk ja Josep Tilkson Losimehhed, täembä Tartus läwä jääs se kohhus polele 22 Webroarini.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Gustaw Tolmusk
