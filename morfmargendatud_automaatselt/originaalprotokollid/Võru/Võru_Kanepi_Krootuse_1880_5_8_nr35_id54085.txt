Kohto ette tulli Jaan Mõts ja kaibas, et Joh. Moistus, kelle man temä teenistusen ollu, temä palka mitte ärä ei maswat. Palka olnu temäl 25 Rbl. weel saada. 15 Rbl. ollew kätte saanu.
Joh. Moistus kutsuti kohto ette ja kui temäle kaibdus ette loeti wastutas seesama, et temä ollew 15 Rbl. raha andnu ja 1 Rbl. käeraha, kokko 16 Rbl. ärä andnu. 24 Rbl. ei maswat mitte selleperast et pois Jaan Mõts Nowembri kuul joba temä teenistusest ärä lännu ja ka kohto moistmise pääle enamb tagasi ei ole tulnu.
Otsus: Et Jaan Mõts 5 kuud enne aasta lõpetust oma peremehe mant ärä om lännu ja ka mitte kog.kohto otsuse ja sunduse pääle enamb teenistuste tagasi ei ole lännu, sis 
mõistse Kog. kohus et Jaan Mõtsal ei ole midagi enamb nõudmist J. Moistuse käest.
Kui kogokonna kohtu otsus ja Sääduse §§ 773 ja 774 kohton käüjile ette saiwa loetu, sis üttel Joh. Moistus et temä selle otsusega rahul ei ole, waid nõuab, et pois Jaan Mõts temäle see ärä massetu 16 Rbl. tagasi peap masma ja weel aasta palk 40 Rbl. selle et temä enne aigo teenistusest ärä om lännu, misläbi temä wäga paljo kahjo ollew saanu. Jaan Mõts ütles ka, et temä kog. kohto otsusega rahul ei ole. Pallessiwa mõlemba oppus lehte surembale kohtule edesi kaibamises, mida neile ka lubati.
Joh. Purasson xxx
Mihkli Laine xxx
Jaan Narusk xxx 
