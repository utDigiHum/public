Sel tännasel päiwal olliwa walla ammetniku kohtomaja mannu koko tallitedo nöuwötmisse päle, nink kesk noupiddamist ja läbbi rägimist, astse Pääkohtomees Gottlieb Anderson müts peas Kohtotarre sisse, nink kissend sure hälega, nende sonnadega: "tutriko nusminne jätke maha."
Selle päle sai temma uksest wälja heitedu, ent tulli jälle sisse nink larmits Kohto tarren ni hirmsaste et Koggokonna wallitsusse markmise asja takkistuse piddi jäma.
Kohtomehe olliwa sel samal päiwal perremehe Wido Narruskowi rendi kottuse ehhituse perrast koko tarwitud ja olli se käsk antu, et olles Koggokonna kohus piddanu Wido Narruskowile omma otsust teggema, et agga Pääkohtomees ilda ja joobnut kohtomaja tulli kunnas keik toise ammetniku aigsast koon olliwa, siis jäi se Wido Narruskowi assi ilmtäitmata.
Päle se teot Pääkohtomees, Koggokonna kirjotajatnende sönnadega:"sinno Konna ollen minna tonu ja tahan sind häwitada."
Siis wimselt nöuse temma omma petsatit (arwata Koggokonna kohta ommandus), mes temma omma karmarin arwas piddata, ent sedda es sa temmale mitte perra antu.
Se on jubba tihti ette tullu, et Pääkohtomees mitto korda kohtoasjo takkistuse on pannu, siis palluwa, ne siin allan nimmitedo ammetniko Ütte Keiserliko Kihhelkonna kohut, temma assemele üht töist Pääkohtomees säda laske.
Kohtomees Paap Sabbal
Kohtomees Josep Parmak
Abbi Rein Oinberg
Wallawannemb Rein Parmatzow
