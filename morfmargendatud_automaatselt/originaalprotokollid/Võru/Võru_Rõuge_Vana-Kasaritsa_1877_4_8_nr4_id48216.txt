Ette sai kutsutus Peter Peltzap ja temmale käsk antus et temma om Kreistohtri polest läbbi kaetus sanu nink kõlblik löitus ommi Koggokonna masse masma, ent Peter Peltzap om tolle päle kostnu et minnole ei olle kostki massa, nink weel et kas Kreistohtri sis pimme olli.
Moistus.
Et Peter Peltzap wasta pand ja kuri olnu nink Kreistohtrit weel pälegi trotsnu sis mõistap koggokonna kohhus tedda 24 tunnis koggokonna wangi.
Mõistmine sai kohtun käujile ette loetus T.S. ramatu 1860 a. waljaantu prg. 773 prg. 774 ärra selletedus nink prg. 881 perra toimetedus.
Päkohtumees  Peter Pedras XXX
Abbi                  Ado Reimann [allkiri]
dito                   Jaan Mikheim XXX
