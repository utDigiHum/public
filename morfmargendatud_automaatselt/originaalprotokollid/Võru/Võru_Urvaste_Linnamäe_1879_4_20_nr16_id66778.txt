Kaibaja. Wastsetarre Peter Kägo tõije kaibust sedawisi ette, et temma poik Karl Kägo olli kolimaja man kolis, sääl üttel õdagul lännu koolmeistri kodust ärra nink jätnu latse ütsida nink perran koolmeistri welli Widrik Mikkelsaar wõtnu koolmeistri poole pääl suremba tüttarlastega tandsmist ette ja Andres Haller ollo mängomehes woetu. Kui muud ne tõise latse kes kolitarren olliwa - seda pillimängmist kuulnuwa ja kaema ehk kullema lännu - sis tullu Widrik Mikkelsaar ommast tarrest ja löönu minno piga kogoni ilma asjanda nida rängaste - et se õkwa maha pillenu; kunna sesamma kui priimees ehk perrakaeja piddanu kolimaja ussesid kinni pannema. Nidasama om ollew ka kolipois Hindrik Toldsep rassetaste ütte rihmaga lüwwä sanu. Säratsen kõrran pea kui wannemb omma latse ülle kullema - nink pallema - et ülle kulleldus saas - sest särane kõrd ka sugugi sündsa ei woi olla - kui koolmeistri ööse kodust ärra lähab - ja seda nida sagede teeb - nink latse ommapääd jättab ja weel sis ütte säratse kätte weel õigust annab kolilatsi pessa ja rassetaste tõugata.
Kaiwataw Widrik Mikkelsaar tulli ette ja ütles, et temma Karl Kägot mitte ei ollew lönu - enge kül üttelnu - kui wöörusen weel üllewan kõndwat nännu - et magama peab minema - ; muud temma ei tija. Hindrik Tõldsepa ütlep kül Widrik Mikkelsaar rihmaga lönu ollewat nink magama ajanu - kunna weel mitte käsku kuulnu ja lännu.
Latsist saijewa ette kutsutus: Hans Lang poig Hans 
Ado Kägo poig Johan 
Jakob Haller poig Jürri
Mihkel Kängsep p. Peter
Ne andsiwa seda tunnistust, et Widrik Mikkelsaar kül Karl Kägot rassetaste mahatõuganu - ommeta tõise latse saije weel ette tõmmatus: Mina Abel - Widrike t. 
Mai Kängsep - Ado t.
Liso Talw - Jaani t.
Ann Seppul - Petre t.
Jakob Hans - Petri p.
Peter Haller - Petre p.
Jürri Haller - Karli p.
Ne es ütle pilli mängmist ei ka üttegi löömist nännu ollewat, ehk kük nemma surema tüttarlatse koolmeistri poole pääl maganu, kas se assi üllesandmise perra sündinu olli. ütsinda koolmeistri wäikesen magamistarren kül weidi Widrik Mikkelsaar ütte lõõtsa pilli kiitsutanu ja Andres Haller ka sääl ollu. 
Kuna tunnistajid weel ülles antus saije - arwas kohus otsuses ja kinni panna - üllesantud tunnistajid eenpool kohtus ette kutsu.
