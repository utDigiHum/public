Ette tulli Jaan Kolk ja kaibas, et Pitre Udras ollew minnewa sügise tema wastse krundi päle, mis kül enne temal oli, kost tõuwilja ärra wõtno ja ilma sitata 1/3 wakkala rükka külwnu ja tahtwat nüüd rükka hendale perrida ja ütlewat, meil om lepping tettu, kas wanna ehk wastset krundi külwame, enge tasomist ei olle, ent temma ei ollew leppingut tennu, ni häste ei ollew ka tõise wastse krundi päle middagi külwnu, selleperrast perritsewat rükka hendale.
Selle päle kutsuti Pitre Udras kohtu ja loeti eensaiswa kaibus ette ja temma andse wastust, et temma ollew todda modi kül ütte kolmandikku wakkala Rükka, mis nüüd Jaan Kolka wastne krund om, külwno, ennegi tolleperrast et lepping olno, et kas olgo wastse ehk wanna krundi päle külweto, ent tasomist ei olew, sis ei jätwat temma ka külwetut rükka mitte tälle.
Kohtomõistus:
Kunna Jaan Kolkal ei olle leppingut, ni häste ka to rügga tõuwilja nurme päle jo tettu ja ilma sittata külwetu, mis wasta sädust om, sis jääp to 1/3 wakkala rükka Jaan Kolkale ja masap Pitro Udrale ennegi 1 1/2 semend taggasi.
Se kohtumõistus sai kohtonkäüijle kulutedus ja prg. 773-774 Tal.säd.ram. 1860 a. ärraselletedus, mis oppust andwa suremba kohton käumisest ja Jaan Kolk jäije selle mõistmisega rahhul, ent Petre Udras tolleperrast mitte, et mis tälle rükka ei mõistet ja wimate sai neile prg. 881 perra ka se trükkito oppuse leht wälja antus.
Päkohtomees Pitre Pedras XXX
Kohtomees Hendrik Hallop XXX
         "           Jaan Mikheim XXX
Kirjotaja P. Haberland
Copia kihhelkonna kohtu sadetu ja sääl sel 7 Julil 1875a tõiselde moistedo.
