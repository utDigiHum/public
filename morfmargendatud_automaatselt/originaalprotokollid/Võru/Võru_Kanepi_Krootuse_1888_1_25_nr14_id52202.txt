1. Johan Teder warandus 12 Januaril s.a. wõlgnewa mõisa rendi perast üleskirjutada: 15 wakka otre, 12 wakka rukkid, 100 wakka kartohwlid, 1 hobene, 1 must lehm, 2 lauda ja üts kapp.
2. Tannil Tartu warandus 12Januaril s.a.7 Tschetwerti rukkid, 2 wakka linna seemend, 4 Tschetwerti otre, 4 Tschetwerti kaero, 2 Tschetwerti otre, kolm (tõrdut) kirstu, 1 kapp, 2 tõrtod, 2 werrewat lehma, 2 lammast, 1 must mära hobene, 1 walge ja 1 werrew lehm, 1 must õhwakene, 1 must ruun hobene, 2 nahk riista, 30 wakka kartohwlid, 100 kubu aeamata linna, 2 wokki, 2 tsikka, 2 sängi, 2 pada, 2 toorid, 4 pangi, 30 wakka kartohwlid.
3. Karl Liin warandus 18 Januaril 1 säng, 1 laud, 1 koomot, 1 pada, 1 toori, 1 ader, 1 kirst, 1 peele, 1 sari, 14 tükki aetusi linna, 1 tõrdu, 50 wakka kartohwlid, 4 wakka kaero, 5 wakka keswi, 1 Tschetwert rukkid, 1 tsetwert linna seemend, 2 mõõtu tatrike, 1 kiriw ja 1 werrew lehm, 1 õhwakene, 1 must mära hobene, 1 regi ja 2 tsikka.
4. Jaan Wool 15 Januaril s.a. warandus: 3 sängi, 1 laud, 1 seina kell, 1 ristsaag, 1 käsisaag, 1 kohwi massin, 2 tooli, 1 wakk, 4 leisikat kuhtid linna, 12 tükki aetosi linno, 1 regi, 1 werrew mära hobene, 1 must mära hobene, 1 raud telle wankri, 2 pada, 1 toori, 1 tõrd, 1 wee raat, 2 werrewät lehma, 1 must lehm, 1 walge õhwakene, 1 pull, 1 õhwakene, 40 kubu aeamata linnu, 10 wakka kaero, 5 wakka linna seemend, 10 wakka keswi, 5 wakka rukkid, 3 wakka nisse, 2 mõõtu tatrike, 2 kirstu, 2 sarja, 2 wikkatid, 30 wakka kartohwlid, 2 hobese riista, 1 saan ja 4 lammast.
Otsus: Seda üleskirjutada.
Jaan Wool +++
Tannil Tartu +++
Karl Liin +++
Peakohtunik: J. Narusk.
Kirjutaja J Kets [allkiri]
