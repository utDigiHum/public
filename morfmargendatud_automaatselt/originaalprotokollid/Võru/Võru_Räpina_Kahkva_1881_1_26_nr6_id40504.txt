Kaibas Jaan Sell, et Kusta Zernak pesnu tema poiga Ernsti piitsaga, jalaga ja saapaga, selle perast et Ernst karja manu elajad Kusta Zernaski röa pääle lasknu. Selle pesmise eest nöwwap tema Kusta Zernaski kaest 15 Rbl. Tunistaja Peter Kirhäiding, Kusta parmann ja Kusti Parmow.
Wastutas Kusta Zernask, et tema ei ole sugugi Ernst Sellile löönu. Tema lännu elajid röa päält kinni ajama, sääl ähwardanu teda Ernst Sell nuiaga ja pääle see wisanu pois kiwiga temale külge.
Tunistas Peter Kirhäiding (13 aastat wana), tema nännu kawwest, et Kusta Zirnask löönu Ernst Sellile 4 körd piitsaga.
Tunistas Kusta Parmann, et tema karjus Ernst Sell olnu pääle pesmise 3 päiwa haige.
Tunistas Kusti Parman, et Kusta Zirnask wõtnu Ernst Selli kinni lehmi röa päält ajamise manu ja nakanu teda pesma, nii et karjuse tänitamine ja pesmise piitsa plaksna olnu temale kuulda; seda pesmist nännu tema oma silmaga.
Otsus.
Kohus möistis, et Kusta Zirnask peab Ernst Sellile masma pesmise eest 3 Rbl.
Pääkohtumees Johan Lepland
Kohtum. Jaan Kirhäiding
Kohtum. Joosep Oinberg
See otsus sai sellsamal päiwal mölembile kohtukäijatele Talorahwa Sääduse Raamatu 1860 aast. §772 ja §773 pöhjuse pääl kuulutetus, mispääle Jaan Sell sellsamal päiwal kohtule kuulutas, et tema selle otsusega rahul ei ole; Kusta Zirnask jäi selle otsusega rahule.
