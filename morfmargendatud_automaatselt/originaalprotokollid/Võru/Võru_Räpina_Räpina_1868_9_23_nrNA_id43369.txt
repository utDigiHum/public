Kristian Kiudorw tulli kohto ette ja kaibas et Wido Punnissoni hobbene om temmä kara röugo pääle karad söman olnod. Kohto mees Paap Kress om ülle kaeno ja 1 wak karad kahjo arwanod.
Wido Punnissoni käest sai küssitu temmö es salga seddä
Et Wido 4 Eespäiwa ei olle kohto ette tulno kutsumisse perrä ja ei olle ka kohto mehhe ülle kaemistegä rahhol olno nink om lubbano teist ülle kaejad tuwwa mis temmä mitte ei olle tenno. Et aig se läbbi ilm pesmatta röugul se läbbi Wido perräst nurme pääl ni pikkäel om länno et röugule jubba paljo sutremb kahjo om sanod.
Kohhus mõistis et Wido peab Kristianile 2 wakka karad maksma kahjo tassomist, ja weel päle selle 4 körd kohtule wasto pandmisse eest 2 Rubla höbbe waeste ladikus maksma
30 Septembril peab masseto ollema
Abbi kohto mees Kristian Wössoberg
Kohto mees Jakob Türkson
Kohto mees Jaan Holsting
Kohto mees  ​​​​​​Hindrik Toding
Kohto mees Paap Kress
