Tu wallakubjas Jaan Hörmann üttell ülles 
Nurkse Pedo kaibab, temma Zigga om karjuse mötsa getnu sus om Süsse erra söno.
Karjuse läsk Mai Blum ning nie teise Gustaw Dobrow nink Jaan Silmason wastutiwa tu sigga om ja ammus ahjas aige olnu, nemma om Perremehhel mittu körd üttelnu et Sigga ei woi minna, temma peasse koddu getma temma ei olle kulnu nuid olle ta erra wessinu ja mötsa getma,
Tu lehidele perremees Widrik Parring tunnist ka et su Sigga ahmust aijast aige om olnu.
moistetu: Et tu Zigga nie kui tu Tunnistaja ütles amust ahjast aige om olnu sus om perremees ka südlenne et ta todda Sigga karja aije, sus om todda Zigga hind 1 Ruv 50 Kop pantu selle berrast et ta aige ja erra lopnu olle sest hinnast massawa Karjuse pool teine pool gep Perremehhe Kahjust.
