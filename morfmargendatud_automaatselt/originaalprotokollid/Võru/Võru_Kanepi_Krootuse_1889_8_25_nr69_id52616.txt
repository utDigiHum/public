1. Jaan Asi Luteri usku kaebas siin kohtu een, et minul on kadunud Adam Asi eestkostja Johan Asi  käest 20 Rbl palk raha saada mida seesama ära maksa ei taha. Sellepärast palun mina kogukonna kohut Johan Asi sundi, seda minu heaks ära maksa laske.
2. Johan Asi wastutas selle kaebduse peale, et see raha mes minu käes 20 Rbl oli, oli Adam Asi raha ja seda oleme esi Adam Asiga ära selletanud. Jaan Asi palgast ei tea midagi.
Kog. kohtu mõistmine:" 
Et aga Johan Asil mitte juttu kohtu ees kokko es lää, waid ütles, et see on mino raha, see on Adam Asi raha  ja kord weel Miina Asi raha. Sellepärast peab Johan Asi 20 Rbl Jaan Asi heaks 14 päewa sees wälja maksma.
Mõistmine kuulutadi Seaduse järele; siis es ole Johan Asi sellega rahul; waid palus opuslehte wälja,mis ka lubati.
/Ära kiri 25 Aug s.a. K.V T.k.k. ära saadetud:/
