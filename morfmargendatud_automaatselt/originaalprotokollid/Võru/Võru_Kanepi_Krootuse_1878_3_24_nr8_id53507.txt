Hindrik Raha tulli kohto ette ja kaibas, et temä ollew Jaan Hindoalale 6 Rbl. raha lainanu, kes nüüd mitte tagasi ei tahtwat massa; pallep kohut, et kohus sunnis Jaani temä raha tagasi masma.
Jaan Hindoala wastutap selle kaibduse pääle, et temä ei ollew mitte Hindrik Raha, waid Jaan Kruus käest selle raha lainanu ja lubap niipea kui Jaan Kruus seda raha nõuab teda tagasi massa.
Otsus: Hindrik Rahal ei ole midagi õigust seda raha nõuda waid Jaan Kruus wõip teda nõuda.
G. Weitz XXX
A. Org XXX
G. Kolk XXX
