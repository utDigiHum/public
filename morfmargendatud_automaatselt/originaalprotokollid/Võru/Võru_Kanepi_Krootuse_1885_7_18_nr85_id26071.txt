Kogokonna kohus 18. Julil 1885
Man olliwa.  Peakohtomees             T. Wõso
         "           Kohtomees            J. Ratnik
         "                  "         abi         H. Lukkats
Kusta Praggi kaibuse asjan wasta Ado Plaado oma Wia Hendriko talu tagasi Noudmist, tullid Kusta Praggi Praggi wallast nink Ado Plaado ette, nink sai nendele järgmine otsus ette loetud.
Kohtootsus.  Selle Wia Hendriko Talu müümise Contrahti leppingu järgi aastast 1880 Jaan Praggi wahel ühelt poolt, kes sel ajal oma täije meele ja tarkuse man olli, nink teiselt poolt Ado Plaado, kui ostja wahel, jääb Ado Plaado Karasky´ Wia Hendriku talu omanikus, nink saab selle perast Kusta Praggi kaibus tühjas moistetud, kuna Ado Plaado keik omas Contrahtis noutawad sundimised ja maksud wiimse copikani ära on maksnud, ja Jaan Praggil ja tema Wöörmunditel praego enamb kedagi wõlga ei ole.
Se kohto otsus kulutati kohton käijatel &amp; 773 ja 774 järgi ette, enge Kusta Praggi es ole selle otsusega mitte Rahul, nink palus Protocolli wälja saada mis temal ka 8 pääwa sees lubati anda.
Lisa!  Perast kohto otsuse kuulutust andis Kusta Pragi siin kohto een weel üles, et tema oma Wia Hendriko talu pidamise Ado Plaadole üles ütleb, ja tema nimitud Wia talu 23. April 1886 kätte tahab sada.
    Copia sel 19 Juli                                                                                               Peakohtomees     T. Wõso
    Kusta  Praggi kätte antud                                                                                          Kohtomees          J. Ratnik
                                                                                                      "     abi               H. Lukkatz
                                                                                                      Kirjotaja as       /allkiri/
