Ette tulli Jaan Wasser ja kaibas oma niidu söötmise peräst kõige Kõrrista külla perremeeste ja kodapoolitside pääle, kes temä niidu nii hirmsast oma karjäga ärä olewad sõknu, kost enämb haina loota ei olewad.
Kedas kohtomehe üle olewad kaenu; ja nõwab 35 rubla.
Pääkohtumees Luik tunistab et niit olnu kül wäga sõkkutu.
Saijewa nee kaiwatawa ette kutsutus:
Jaan Truumann, ütlep et sinnä niidu pääle käüwad kõik, olnu ka Zähkna kari kats päiwa pääle üle kaemise pääl. Temmä karri ei olewad kül pääle saanu, suwel olnu karja tee õkwa üle.
Jüri Perli ei teedwad midagi tunistada, sest et tuu niit olewad kogonist karja ma sisen, temä üts elaj ei jõudwad paljo sõkku, ja ei puttuwad ka sinna.
Jakob Liblik küsitu karjaga sõkmiseperäst kes ütlep et temä ei teedwad, sedä teedwä karjusse.
Karjus Leena Samo tunistab et Kõrristaia karri ei olewad kunagi Jaan Wasseri niidu pääle puttunu, enämb kui suwel haina tegemise ajal kui nemä esi man niidul olnuwa, muid ka ei olewad nännu.
Karjus Marri Weiko tunistab, et nemä ei olewad oma karjaga jalg niidu pääle saanu, muid sõkjid ka ei teedwad.
Jaan Wasser end weel tunistaia, Johann Wisk.
Jääb poolele kuni tunistaia saawa.
