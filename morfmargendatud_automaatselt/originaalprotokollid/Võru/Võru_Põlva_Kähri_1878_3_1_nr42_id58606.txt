Ette tulli Jaan Rattas ja kaibas Krootus mõtsawaht Jüri Hakk wasta, kes temä kartofli lasknu ärä süwa ja nõwab kahjo 10 rubla. Ja kapsta neide eest 5 rubla.
Et Jüri Hakk kolme kõratse ette kutsmise pääle ette ei ole tulnu
Kapstide peräst olnu jo leppito 1 1/2 rubla.
Kartoflide peräst tunistab kohtumees Joh. Punnak et mõne puhma sisest olnu tsungitu arwata paarwakka kartoflid.
Otsus:
Kogokonna kohus mõistab et Jüri Hakk peäb 5 rubl Jaan Rattale kahjo tasomisest masma.
Jüri Hakkile saije see otsus sel 15.Märzil ette kuulutetus § № 773 ja 775 perrä T.S. Saije selsamal päiwal opus täht antus.
