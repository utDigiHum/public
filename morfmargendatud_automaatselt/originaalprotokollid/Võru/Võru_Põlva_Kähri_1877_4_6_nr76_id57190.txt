Ette pand Serreste moisa walitsuse nimel Herrä kaibust kandima pidaja Johann Lutz pääle kes olewad 5 wakka maad rükka tegemata jätnu; nõudmine 10 rubla wakka ma eest ja olewad ka sit wedämata. Tuust olnu üts wakka ma küttusse rükka.
Johann Lutz sis kaibusen ette kutsutu, ütlep et temä oles kül rükka tennü oles Herräl sitta maale panda olnu; et sitta panda es oles es woi ka rükka tettus saanu, küttise tegemist ei olewad lepitu olnu. Ei ole ka kätte näütetu tettä.
Otsus:
Kogokonna kohus mõistab et kontrati ma pidamise peräst ei ole, ja ka et Herrä sitta maale panda andnu ei ole, õigel ajal es wõi Johan Lutz rükka tettä, sis peäb nüüd enegi Johann Lutz nelja wakka ma seeme tagasi masma. Nink muido ei sa tö eest midagi mõistetus.
Otsus kuuluteti §  773 ja 774 perrä.
Herrä es ole otsusega rahul.
Opus kiri selsamal päiwal wälja antu.
19.April Prokoli.
