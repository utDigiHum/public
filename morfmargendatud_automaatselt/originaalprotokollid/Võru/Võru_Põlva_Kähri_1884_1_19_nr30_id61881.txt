Karl Pillberg tuli ette ja andis üles et temale on Jacob Sokka käest 3 rubla 30 kop. wõlga saada ja palub seda wälja nõuda.
Jacob Sokk wastab et temale olla kül wõlgu olnud aga tema olla kõik jälle äramaksnud.
Mõistus. Et Jacob Sokk ütleb enast Pillbergile wõlgu olewad olnud ja seda mite selgeks tunistuse läbi ei wõi teha et ta see wõla puhtas ära on maksnud ja et ka Karl Pillbergile tunistust ei ole selle üle anda et Jacob Sokk temale 3 rub. 30 kop. wõlgu on siis mõistab kogu konna kohus et Jacob Sokk Carl Pillbergile poole sellest nõutud summast s.o. 165 kop. 2 nädali sees peab wäljamaksma.
Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtuskäijatele ette loetud. Sokk ei olnud mite otsusega rahul ja palus ärakirja mis ka lubatud sai.
Eesistuja Johann Waks
Kohtumees: Johann Wasser
do.:
Kirjutaja Bergmann [allkiri] 
