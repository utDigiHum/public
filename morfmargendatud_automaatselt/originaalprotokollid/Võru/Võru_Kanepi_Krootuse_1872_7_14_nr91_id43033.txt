Tõine kohhus sel 14. Julil 1872.     Päkohtumees: Jaan Tullus  XXX
                                                           Kohtumees:  Jaan Wärs  XXX
                                                                    do      :   Peter Mandli  XXX
Keisriliku Tartu Vma Kihhelkonna Kohtu, kirja päle mes sel 23. Mail s.a. No 1454 al kästi, sai Karraskij ja Kakku tallo piri assi jälle selletedus: Karraskij tallo perremehhe Ado ja Peter Plado tahtsewa selle ma ülle piri piddamist, Kakku Adam Lukkatsi käest: 15 rublat, 5 aasta eest se om, egga aasta päält 3 rublat. - Ent Kakku Adam Lukkats, es leppi sellega mitte kokko. ---
Kohtu otsus: Et Karraskij tallo perremehhe Ado ja Peter Plado selle ma läbbi  üttege kahjo (kohtu ülle kaemise perra) ei olle saanu, selle et tinnawoaasta om piir looka künnet nimmelt: Põllumaad 200 künart pik, 1 künar lai, nink söödun ka 200 künart pik 1 künar lai, kos kannarik pääl kaswap, - mõist kohhus: Kakku Adam Lukkats massap 3 rublat piri looka kündmisse trahwi, 8. päiwa sissen, koggokonna ladikude; ent Karraskij tallo perremehhe ei sa ka seddagi 1. rublat, mes neile mõistet olli, ja kohhus arwas, et Karraskij Ado ja Peter Plado tahtwa ennegi Kakko Adamit kiusata. - Kohtumõistmin(n)e ja § 773n. 774. sai kohtun käüjile ette loetus, Karraskij perremehhe es olle sellega mitte rahhu ja palsiwa Protokolli wälja, mes kohhus lubbasi anda. -
                                   Protokoll wälja wõetu sel 18nel Julil 1872
                                                                      Pä kohtumees Jaan Tullus
                                                                       Kohtumees Jaan Wärs
                                                                                 do      Peter Mandli
                                                                                    
