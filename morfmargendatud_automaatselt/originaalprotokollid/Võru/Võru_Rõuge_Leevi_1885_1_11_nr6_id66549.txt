N 6.
Lewi kogokona kohus sel 11 Januar 1885
Koos olliwa Pääkohtomees Jaan Thalfeld
Kohtomees Kusta Moistus
" Johann Suits.
Liwi kohto moistmise ajal kus Simon Sulg ülle kohut moistedi ütles tema essa kokona kohto peegli ees et seda ei tohi olla et kogokona kohus mino poiga pessap ja mina pessa ei lasegi sil kogokona kohto trotsimise eest
Moisteti
kona se trotsimine kohto peegli ees om olnu saap Johann Sulg 20 witsa lööki ehk massap 4  selle kogokkuna kassa hääs 2 nädali sees wälja
Moistmine saije § 773, 774 perra ette kuulutud
Paakohtumees [allkiri] Hindrik Kikkas
Kohtomees Juhan Suits XXX
" Kusta Moistu XXX
Kir [allkiri] Perli
