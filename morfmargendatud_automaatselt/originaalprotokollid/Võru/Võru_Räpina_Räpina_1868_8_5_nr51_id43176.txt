Josep Hörsberg tulli kohto ette ja kaibas et Gustaw Ziugandi hobbesed Meeksi wallast temmä nido pääl om sömän om olno saggedaste ja nido ärrä söötno.
Gustaw Ziugand üttel et temmä hobbesed ja karri ei olle olnod mitte Joosepi nidon. Josep üttel et temmä om Gustawi hobbesid ja lehmä kindi wötnod.
Nemmäd issi om teine teisega ärrä lepno se päle et eddespiddi ei tohhi ennämb kahjo tettus sada.
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Jaan Holsting
