Karraskij Koggokonna Kohhus, Jusal sel 30al Aprilil 1876.
                                                                            Mann olliwa Eenistja, Jaan Tullus.
                                                                                "         "      Mannistja, Jakob Uibo.
                                                                                 "        "      Mannistja, Jaan Leppason.
Tulli ette moisawallitsusse assemnik Jaan Uibo, ja kaibas et känksep Jaan Juust ollewat Musti moisa maa päält saadu põhhust, arwata rohkem  10. koormat sitta wargusse wiisil Krotusse maa pääle, wälja weddanu, nink ütte unnikutte mahha pandnu, nink pallel kohhut temmal (Juust) käsku anda et se sitt saas taggasi toodus, ja säädusse perra trahwitus
Kutsuti ette Jaan Juust nink sai kaibus ette loetus, kes selle pääle ütles /ilma salgamatta/ minna olle omma suwwi sitta ennegi wälja weddanu, ja min(n)ol jääp weel arwata 20. koormat sitta sinna, nink olle ennegi 3. koormat saanu, Kohtul om minno wõõra maa päält toodu, põhk wist wäega teeda mes min(n)a sinna sisse olle wiinu, essi weel mes ostetu n: n.e:
Otsus   Kohhus moistis, /selle et kohtul se assi wäega tutwa olli/ et J. Juust´il wõõra maa pääl ka om wilja olnu, ja 3. koormat sitta ennegi sisse minnen om saanu,  Se sitt mes wälja weetu om, jääp J. Juustil, ent selle et temma ilma lubbata om wälja weddanu, massap 1. rubla waeste laati trahwi, mes 8 1/2 päiwa sissen peap ärra massetu ollema,
Sai kohtomoistminne ja  § 772.n: 773. kohtun käüjattelle ette loetus, ent mõllemba es olle rahhul et arwasiwa hendal liiga tettu ollewat nink pallelsiwa Protokolli, mes kohhus es lubba anda. -
                                                     Karraskij Kohtomajan sel 30al Aprilil 1876.
                                                                                       Eenistja, Jaan Tullus  XXX
                                                                                        Man(n)istja, Jakob Uibo  XXX
                                                                                        Manistja, Jaan Leppason  XXX
 
