Kahkwa kohtumajan sel 17 Webr 1869.
Kaiwas mõtsawahd Jaan Lepland, et Hendrik Leppisk om 4 peddajad mõtsast warrastanu kos temma sullane Josep Säkna om jälgi piddi perra lännu ja Hendrik Leppiski poolt leidnu. Ja Paap Sabbal om ka se wargusse seen ollu. 4 peddajat om ollu 6 tolli paksu ja 3 pedajat om 9 tolli ollu. Jaani sullane Josep Säkna ütel, et kui temma om otsma lännu, sis om ollu üts tüwwe tük ja üts ladwa H. Leppiski riida ees, ent kui Rämmani Jaaniga om kaema lännuwa, sis om enne üts laddu ütsinda ollu.
Ette tulli H. Leppisk ja ütel, et temma om se latwa haoga mõtsast toonu ja peddajist ei tija temma middagi.
jäi polele
