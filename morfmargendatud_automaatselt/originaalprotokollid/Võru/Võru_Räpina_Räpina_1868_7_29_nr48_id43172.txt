Wilip Narrosk tulli kohto ette ja kaibas et Meose poig Jakob om wötnod Wilipi tütre Anne käest tulle tikko ja om mõtsa pallama pandno Wilipi krundi sissen.
Meose poig Jakob ütteö issi ärrä et temmä om 4 tikko wõtnud Jakob Meos üttel et Wilipi poig om issi pallama pandno.
Kohhus möistis et Meose poig 5 ja Wilipi poig 3 witsa lögiga trahwitus peawad sama.
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Jaan Holsting
Kohto mees Jakob Türkson
Kohto mees Paap Kress
Kohto mees Hindrik Toding
Kirjutaja J. Jaanson
