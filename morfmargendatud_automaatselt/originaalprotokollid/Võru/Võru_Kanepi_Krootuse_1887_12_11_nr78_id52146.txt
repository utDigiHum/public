1. Wallawöörmünder Kristjaan Sibul tulli siia kohtu ette ja kaebas, et Samuel Assi wõtnud 1887 a. kewadel 1 Tsehtwert rukkid ja 2 karnist Hurmi magasist ja ei tahtnud mitte magasi ära maksa. Mida magasi kladde raamat näitab.
2. Samuel Assi Luteri usku wastutab selle kaebduse peale, et minul ei olle mitte 1 Tsehetwert 2 karnist rukkid maksmata, mes mina ollen wõtnud, ollen maksnud.
Kog. kohtu mõistmine 
"Samuel Assi peab 1 Tsehetwert 2 karnist rukkid Hurmi magasi aita ära maksma. Magasi kladde raamatu järele 8 päewa see."
Mõistmine kuulutadi §§ 773 ja 774 põhjusel, siis es olle  Samuel Assi sellega rahul; waid palus opuslehte wälja edasi kaebamiseks.
Ära kiri 12 Decembril s.a. ära saadetud.
Peakohtunik: J. Narusk [allkiri]
Kohtunik : T. Weits [allkiri]
   "  T.  Päkko 
Kirjutaja J Kets [allkiri]
