N 61 Waimelan sel 11 Octobril 1874
Pakohtomees P Pihl
Abbi J Waiso
dito M. Traks
Konna Moisawallitsus om Adam Danneri om Koggokonna kohto ette tallitano sel 4 Octobril ja temma es olle tulno.
Temma om sel 11 ette tulno ja ei olle märastki wabbandamist muud andno kui temma om kartohwlit wotno:
Moistus
Adam Danner lääp 24 tunnis koggokonna wangi.
Pakohtomees: P. Pihl [allkiri]
Abbi: M. Traks [allkiri]
dito: J. Wäiso [allkiri]
