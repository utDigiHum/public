Kaibus.
Kaibas Peter Söörd, Mötsasaks J. Roosneck ollew tedda törw ahjo man wäega rängaste löönu, selle et törw ahjust korrast tult wälja ajanu. Tunnistaja selle man peawa ollema:
Jaan Rikkand ja
Jaan Kesselmann.
wäljanöudminne.
Mötsasaks J. Roosnek sai ette kutsutus ja wastutas, Peter Söörd ei olle törwahjo perrast perra kaenu, ning temma holetuse läbbi olles se wastne törw ahhi pallama lännu kui temma mitte appi ei olles jöudnud, sedda sa,a kustutama, ning selle man om temma tedda tõuganu ning tõrrelenu, agga mitte löönu.
Jaan Rikkand sai ette kutsutus ja üttel selgest ei woi temma mitte sedda tunnistada kas olles Mõtsasaks Peter Söördu löönu, ent ni wõrra om temma pimmetusen nännu, et Mötsasaksolle ni kui P. Söördu tõuganu, ja tedda torrelenu omma holetuse eest.
Jaan Kesselmann, tulli ette ja üttel, kui törwahjo kormast tuld wälja ajanu, lännu temma sedda tuld lummega ärra lammatudu siis tullu Mötsasaks mannu ja tõrrelenu Peter Söördu köwwaste selle eest, et mitte häste ei olle perra kaenu, ning muud ei olle temma nännu.
Möisteto.
tunnistajete perra om se assi tühjas moisteto.
Pääkohtumees Gottlieb Anderson
Kohtumees Josep Parmak
Kohtumees Paap Sabbal
