Kogokonna kohus 20. Märtsil 1886.
Man ollid.      Peakohtomees   J. Roht
      "                Kohtomes     M. Wõso
       "                           "          J. Mandel
Kohus olli ära surnud Kadri Leppasoni lapse wöörmündre ette kutsunud, nende käest teada saada, kuidas nemad oma ameteid on pidanud ja kas laste warandus weel nida terwelt koos on, kuidas se nende haale alla ustud sai.? -
Laste isa, 1. Wöörmünder Jaan Lepason ütel. "Keik mis Protocolli Ramatun üles tähendut om, on alal ja puudumata."
Sedasama ütel teine wöörmünder Sa. Raig.
 
/. All kirjad./
