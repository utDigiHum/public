Gustaw Nikke Wenne pole päält Rainowa kõrtsimees mees, andis kohto een ülles et siis kui temmä 2 Oktobril Kahkwalt om tulno läbbi Wõbso ja kui temmä Wõbso om sano om temmä Wõbson omma katte hobbese kramist kormast Pottisseppa meister Heindrichsoni pole poolt kormat mahha pandno, ja teise pole katte Hobbese kormaga om temma Wõbso parwe ülle wija lubbaga ülle länno minnemä, agga kui temmä parwe päle om sanod om temmä al warsti parw ärrä waoma hakkano, ja om wee sissen ligges sano keik temmä kraam Selle perräst om se eddiminne pool kormat Heindrich pole mahha panto et sääl weike ow ja umber käänminne om olno.
Ligges sano kraam om1 1/2 wak röa jahho			4 R kahjo		1 pallito mis ligges sano			30 R kahjo		1 Särk ligges sano			15 R kahjo		1 paar püksid			12 R kahjo		2 westi ligges sano			10 R kahjo		1 Surwe müts			5 R kahjo		piberi ja muido mütsid mis ligges			6 R kahjo		1 Ordinä särk			3 R kahjo		1 par wastsed Wix saapad			5 R kahjo		2 Rätti 1 särk ligges sanod			15 R		Olletalid kirjäd ja ramatud kast mis ligges sanod								105 R kahjo		
Wette om länno ja jäno kahjo1 1/2 w eöä jahho					6 keesi 60 naela 10 kop nael			6 R		1 1/2 wak kartowlid wessi ärrä wino kottiga			1 R 90 kop		2 w karod kige kottiga Wessi ärrä wino			2 R 50 kop		1 Armonik ärrä ligguno ja leono			6 R		3 naeila püssirohte ärrä ligguno			3 R		1 koffi weski limist wallale leono			3 R		Kokko			22 R 40 kop		
Et Wõbso politse wallitsus koddon es olle om Räppinä kohtomees Hindrik Toding olli neid asjo üllekaema kirjutaja Jansoniga
Ramowa karner Dawid Kangrow tunnistas nänno ja sääl mann olno kui Gustaw Nikke kraam sissi om länno ja üttel et se parw mitte ei olle kandno ni kui hobbesed päle om länno om parw sissi waijono, hobbeste al kus ka innimissed poleni kihhäni sissi om waijono
Matli Türkson parwega ülle weddäjä üttel et parw om sissi länno selle perräst et 2 hobbest päle om länno. Päle selle om weel Gustaw Nikkel wanker ärrä rikkutu ja penikessi hamid wee läbbi hukka länno mis 3 Rubla tük.
Et se Wõbso parw ilm kõlbmatta olli ülle widelädä om tõtte.
Et Mõisa Herrä käest Wõbso parwe rentnik Jürri Reijär kohto een es olle jääs se assi polele.
Et Jürri Reiär kohto manno es tulle pallub Räppinä koggokonna kohhus allukko palwega Keiserlikko Werromakohhut seddä asja selletädä wõtta.
Pä kohtomees Jaan Holsting
Kohtomees Kristian Wössoberg
Kohtomees Jakob Türkson
