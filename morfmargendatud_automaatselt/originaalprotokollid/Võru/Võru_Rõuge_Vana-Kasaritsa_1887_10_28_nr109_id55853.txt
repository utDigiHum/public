Ette tulli Petre poig Jaan Hillep ja kaibas, et kogukonna kohtu poolest Protokolli perra sest 10. maist 1868 aastast N 11 ollew tema esa talo N 63 all Hint Miksoni kätte pruuki antu, nii kaugu aja pääle, kuni tema (Jaan Hillep) 21 aastat wanas om saanu, ent kuna tema (Hillep) jo nii wanas om saanu, sis nõudwat tema oma esast peritu Kollo tallo N 63 Kollo Hint Miksoni käest kätte saada.
Wastut kaibuse pääle Kollo Hint Mikson, et Jaan Hillep tälle õige aja pääle selle kõnnen saiswa talo kätte saamise ja nõudmise perast üles ei ollew andnu ja selle perast ei wõiwat ka tema seda nimitedu Kolo tallo N 63 Jaan Hillepile kätte anda.
Selle wastu tõent Jaan Hillep, et temal kogukonna kohtu Protokolli perra 10. maist 1868 aastast N 11 Hint Miksonile ülesütlemist tarwis ei ollew olnu, enegi kui tema (Hillep) 21 aastat wanas om saanu sis pidawat Hint Mikson ilma üles ütlemiseda tälle selle nimitedu Kollo talo N 63 kätte andma. Päälegi ollew tema Hint Miksoni wasta oma talo nõudmise perast kaibuse 29. Julil s.a. kogukonna kohtu man sisse andnu.
Otsus:
Kuna Petri poig Jaan Hillep 21 aastat wanas om saanu sis peap Hint Mikson kogukonna kohtu protokolli perra sest 10. maist 1868 aastast N 11 selle nimitedu Kollo talo N 63 Jüri päiwal 1888 aastal 23. Aprilil Jaan Hillepile kätte andma.
See kohtu otsus saije kohtun käüjile kuulutedus § 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus. Hint Mikson nõwwap Appellationi.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
         "            P. Klais [allkiri]
Appellation Hint Miksonile 31. Oktbr. 1887 wälja antu.
