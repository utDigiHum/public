Hindrik Nikopensius tuli ette ja kaebas Jacob Paidra pääle kes tema käest olla pilli ärawõtnud mis tema joba 22 rubla mängimise rahaga juba osast olla tasateeninud ja nõuab seda raha 22 rubla tagasi saada.
Jacob Paidra wastab. Mängimisega ei ole Hindrik Nikopensius pilli raha tasateeninud kui 17 rub. 43 kop. kust weel mõned muud kulud tulewad mahaarwata ülepea oli temale pilli eest maksa 29 rub. 93 kop.
Otsus.   Tulewaks kohtupääwaks mängukoori rehnungid ettetuua.
Eesistuja Johann Waks XXX
Kohtumees: Jaan Saloman XXX
do: Juhann Wasser [allkiri]
Kirjutaja M Bergmann [allkiri]
