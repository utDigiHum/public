Kaibas kohtu een Jürri Arras Et Hindrik Nõggel olla temmale selle samma Kogokonna Kohtu een temmale üttelnu, et Kähri Kogokonna Kohhus olla temmal wiina pudeliga õigost moistnu.Tuuperrast palles temma kohhut sedda asja perra kullelda ja säduse perra moista.
Hindrik Nõggel ette kutsuti ja kaibus ette loeti, kes wastutas: Et minna ei olle mitte kõrtsi mees et minna wiina pudeliga ümbre käü ni kui Jürri Arras käüp iks Kähri Kohtuhe ja oniks wina puddel karmanin sedda wisi ütli minna.
Kui neil ennamb middagi selle asja wahhel ütlemist es olle
Moistis Kohhus:
Et Hindrik Nõggel peap selle Kähri Kogokonna Kohtu trotsimise perrast 4 öö ja päiw Kogokonna türmin ollema.
Moistmine ja § 773 sai kohtun käüjile ette loetus kon nemma ka mõllemba sellega rahhule olliwa.
Gusta Weitz xxx
Mihkel Soldan xxx
 Adam Jahhu xx
Kirjotaja August Hallop
