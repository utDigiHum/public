Ette tulli wana lesk Mai Kirber ütte mehega, ja nõwab oma üle pidamise tarbis poig Jaan Kirber käest moona nii paljo kui temä ärä wõib elädä ja ütlep et temä Jaaniga ütte lauda ei löö.
Jaan Kirber ette kutsutu ja see kaibus kuulutetu ütlep: mina ole temäle jo kõrd kogokonna kohtu een kõik temäle lubanu, nink ei keela kunagi tedä omast lawast söömast. Kui emä moona nõwab, sis peäb ka tõine poig Kaarli moona masma.
Jääb niikauges kuni wöörmündri saawa.
