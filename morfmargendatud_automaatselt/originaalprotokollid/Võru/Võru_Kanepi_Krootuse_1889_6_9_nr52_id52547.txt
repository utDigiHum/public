Jaan Järw Luteri usku tuli siia kogokonna kohtu ette ja kaebas, et minul on Dawit Möksi käest järgmiselt saada, naguPalka raha saada 			10 Rbl		Linade eest   "			15   "		                                   Summa			25 Rbl       		
Sellepärast palun mina kogukonna kohut, Dawit Möksi sundi, seda nimetatud raha minu heaks ära maksa laske.
II Dawit Möksi Luteri usku wastutas selle kaebduse peale, et minul ei ole rohkemb palka Jaan Järw heaks maksa kui 2 Rbl 55 kop. ei mitte 10 Rbl. 15 Rbl on küll lina raha maksa, aga see on Jakob Järw heaks maksa, mitte Jaan Järw heaks.
"Kog. kohtu mõistmine:"
Dawit Möksi peab 2 Rbl 55 kop palka Jaan Järw heaks 14 päewa sees wälja maksma.
Lina raha ei wõi mõista, sellepärast et see Jakob Järw linnad olnud.
Mõistmine kuulutadi Seaduse järele; siis es ole Jaan Järw sellega rahul; waid palus protokolli wälja, mis ka lubati.
/:Pr wõtta wälja:/
