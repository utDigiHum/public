Leno Simmulman Jakobi tüttär Räppinä wallast kaibas kohto een, et temmä om länno, omma sõssare pole kostile kus temmä enne om teeno, nink sääl om sis Michel Tilgand temmäga kiusano ja om temmäle lapse tenno. Ja Leno ollewa mõnni körd ülle näddälide käino, ja eggä körd ollewa Michel Tilgand temmä mann käinud. Nüüd pallub Leno lapse ülles piddämist ehk wötko Michel laps ärrä
Michel Tilgandi käest sai küssitu üttel temmä et temmä ei tea middägi.
Leno üttel et kölgosin om eddimält allusatano.
Kohhus mõistis et Michel Tilgand peab 10 Rubla eggä lapse ello aastal masma seni kui laps 10 aastaga wannas saap. 14 Mail eggä aasta massu tärmin
§773 om ette kulutedo.
Michel pallel prottokoli.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Kristjan Rusand
Kohtomees Rein Lambing
Kohtomees Gustaw Tolmusk
