5 Webroaril kui 29 Januaril Marri Kaddaka kaibus wasto pool welle Adami ette olli loeto.
Tunnistaja Wilip Lamanoss üttel, et kaddono wanna Mik Kaddak om temmäle mittu körd könnelno et minna olle nüüd henda kotsilt ütsinda maja urtsiko tenno, ja kui minna ärrä kole jääp se maia Marrile
Wido Wöikmann tunnistas, ni samma et Wanna kaddono Mik Kaddak om kõnnelno temmäle et temmä om selle maja hendä kotsilt ütsindä tenno, ja kui temmä ärrä kolep jääp se maja tütre Marril
Jaan Häidesk tunnistas, et näddä aiga enne koolmist om Mik üttelno et nüüd om nemmä ?? et nüüd om Pede Adami Maia, ja tullewa aasta teine.
Keik tunnistaja tunnistiwa et Adamil om olno Essägä maja ütte tetto kui nemmäd Rassinalt om tulno, ja päle selle kui nemmäd selle ütte tetto maja ärrä om münowa om nemmäd issi hendä kotsilt teine teise maja ommale tenno.
Adam Kaddak üttel et se maja peab temmä rahhaga tetto ollema.
Tunnistusse perräst jääs 12 Webroarini polele.
