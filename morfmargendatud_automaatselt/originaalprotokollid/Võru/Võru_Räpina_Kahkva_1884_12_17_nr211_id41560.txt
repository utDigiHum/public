Tulli ette August Anderson nink kaebas et Joosep Parson om tedda pesnud, ja lubbanud tedda ka weel edespidi pessa, selle eest et temma ollew Joosep Parson tuwikesed Peter Kübara poole petnud, ja nöwap selle pessmise eest 25 rbl. tunnistajad selle man ommawa Mik Wöikson, Joosep Hanson, Joosep Söugand, Peter Wähk.
Tulli ette Peter Wähk nink tunnist et temma ei ole mitte midagi nännud.
Tunnist Joosep Söugand et temma on nännud et Joosep Parson om tulba pääle koputanud, ja selle koputamise man om ka August Andersonile putunud, ja perrast seda ollew körwu pitte löönud ja rindu pääle, ja August  Anderson ollew iknud, ja selle perrast ollew tedda pessnud et August Anderson ollew Joosep Parson tuwikesed ärräpetnu.
Pääkohtumees: Peter Urtson
Kohtumees: Kusta Rämman
Kohtumees: Joosep Oinberg
Kirjotaja F Wreimann &lt;allkiri&gt;
