Kaibas siit wallast Karl Konts (elab praegu Ahja wallas), ennast siinse protokolli peale 18. Aprilist s № 22 juhtides, kus see asi juba siin seletamise all oli, et Mari Tubli (elab nüüd Wõru linnas) olla nüüd hiljuti ise oma suuga ütelnud, et ta Kontsi käest 16 rubl. olla saanud lapsekaswatamiseks. Seda kuulnud Anne Tubli ja pois Juhan Nemwaltz.
Mari Tubli salgas seda ära.
Tunnistajad Anne Tubli ja Juhan Nemwalts ütlesiwad Karl Kontsi ülesandmist tõeks, et Mari Tubli olla küll nii ütelnud.
Otsus:
Protokolli 18. Aprilist c. sub № 22 täienduseks otsustatud, et 16 rubl. üleüldisest kaswatusrahast maksetuks arwatud.
Otsus kuulutati seaduse järele ette.
Pääkohtunik: J. Nilbe
kohtunik: H. Lukats
d.: J. Aripmann
Kirjutaja as: Zuping [allkiri]
