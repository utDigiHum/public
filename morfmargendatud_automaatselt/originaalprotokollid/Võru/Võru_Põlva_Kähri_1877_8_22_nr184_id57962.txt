Ette tulli Kassi Peter ja kaibas et mõisamapidaja Jüri Puusep olewad temä niitu üles kündnu põllus tennu ja pruuknu, ja olewad ka weel üts jago niitnu, nink sedä kahjo olewad temä jo kolm aastat tennü ja arwab ega aasta nelja rubla eest kahjo saanu olewad. Ma arw om 200 kruut sülda kos kahjo om tettü. Kost temä üts ruga haino oles saano.
Saije Jüri Puusep selle kaibuse pääle ette kutsutus ja see kaibus kuulutetu, ütlep et tuud temä ei tija kost piir lät, mina ole tuust saadik niitnu ja kündnu kost minole Herrä piiri näütas, mina ei tija kas oli mõisa ma ehk peremeeste ma, mina ei tija kos piir ehk kupatsi oma, see ei ole mino asi, see om Herrä asi.
Wallawanemb Lepäson tunistab, Kassi Peetre kaibuse õige olewad, niida et Jüri Puusep om kül nii wõrra üle tennu kui Peter kaibab. Ja et kolme rubla eest om kül Kassi Peetri kahjo, ega aasta.
Otsus:
Kogokonna kohus mõistab et Jüri Puusep peäb Kassi Peetri kahjo ega aasta 3 rubla kolme aasta eest 9 rubla Kassi Peetrile masma.
Otsus kuuluteti § 773 ja 774 perrä T.S.
Jüri Puusep es ole otsusega rahul ja saije opus kiri antus, selsamal päiwal.
Pääkohtumees Jaan Luik
kohtumees Peter Tallomees
   " Jaan Hingo
Kihelkonna Kohtu nõudmise pääle ärä kiri 24 Septr 1879 antu.
