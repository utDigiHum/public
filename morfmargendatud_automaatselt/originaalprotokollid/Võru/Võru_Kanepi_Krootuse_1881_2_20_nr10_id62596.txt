Tulli ette Saksa mõtsawaht Jaan Uibo, nink andis perris herrä kässo pääl, Kogokonna Kohtole teeda et saas Kohto poolt Üllegeemaa kottosse piddajal lesk Marri Raig, ent nüüd wastse abbiello astmisse läbbi Marri Tarrend ülles ütteldus saas, et perris herra sedda maad nüüd een tullewal jürripäiwal 1881 tahhap ärra wõtta nink tõisele anda, selle et om tedda Marri Raig nüüd Tarrend ennegi üttes ajastajas armo läbbi maija jätnu, ja om ka weel wanna renti massmatta, ja et sis saaduslikkult temmäle saas se ülles ütleminne kuulotedus. -
Ülles ütleminne jäije tõise kohto päiwa pääle kulotada selle et Marri Tarrend es olle kohto ette tallitedo
                                               Karraskij Kohtomajan sel 20 webruaril 1881
                                                                       Pääkohtomees Jaan Tullus  XXX
                                                                         Kohtomees Jaan Leppäson  XXX
                                                                        teine    do      Samoel Raig  XXX
