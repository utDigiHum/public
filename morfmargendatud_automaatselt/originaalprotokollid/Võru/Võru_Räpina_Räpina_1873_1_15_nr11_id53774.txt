Kristjan Muski kaibas kohto een et temmä om 4 aastaiga aigo taggasi Fritz Zupsmanil hobbese 20 Rubla eest ärrä müno selle kauba mann om Jakob Türkson käemees olno kes Fritz Zupsmani käest rahha wasto wõttab ja Kristjani kätte ärrä annap, agga 10 Rubla om käemees Jakob Türkson ärrä andno, ja teine 10 Rubla peab weel andmatta ollema, Sedda rahha pallub Kristjan Munski kohto läbbi ärrä nõuda temmäle kätte
Käemees kohtomees Jakob Türkson üttel et 10 Rubla om Fritz Zupsman kül ärrä masno, agga teist kümmend rubla ei olle Fritz mitte temmä kätte ärrä masno.
Fritz Zupsman üttel ,et temmä om se teine 10 Rubla issi ilma Jakobita Kristjan Munski kätte ärrä masno.
Kristian Munski üttel et Fritz Zupsman mitte temmä kätte 10 Rubla ei olle masno.
Kohhus mõistis, et Fritz Zupsman peab weel se teine 10 Rubla läbbi käemees Jakob Türksoni Kristjan Munskil ärrä masma
§773 om ette kulutedo.
Fritz Zupsman pallel prottokoli.
