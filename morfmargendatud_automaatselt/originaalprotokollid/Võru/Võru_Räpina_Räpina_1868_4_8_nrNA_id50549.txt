Hindrik Luk ja Hindrik Punnmani lepping mis nemmäd Janikessi küllän Silukesse tallo ülle kontrahti herramän om tennowa ja lepno. Et nüüd Hindrik Luk omma maja ärrä ei tahha anda ja omma leppingut tühjäs tahhab tehhä om kohhos moistno herräst anto kontrahti perrä mis nemmäd issi om teine teisega wastastikko lepno. Neide omma lepping om nida olno Hindrik Punnisson massa Hindrik Lukkul 190 Rubla maa kontrahti ja tallo rendi kohha eest 100 Rubla om Hindrik Luk wasto wõtno.
Herrä kirjutedo kontraht om ni modo
Räppinä moisa herrä om Hindrik Luk nink Tolamamehhe Hindrik Punnmanni palwed kulda wõtno, nink lubbab et Hindrik Luk omab Janikesse Silakesse maa keige kontraktiga Hindrik Punnmanni kätte.
Hindrik Punnmann wottab 1866 tehtud ma rendi kondrakt wasto lubbab keik täitä mis Hindrik Luk om hendä päle wõtno tehhä.
Räppinä herr lubbab ni samma kui Hindrik Luk wahhel olli leppitud keik maa rent seni kui Jürri päiwäni 1872 mahha jättä, nink mõtsast palgid anda mis maja tarwis lät, ent Hindrik Punnmann ei tohhi Päle selle mitte keddägi hone ehhitamisse eggä maa arrimisse hinda küssidä
27 März 1868 herrä nimmi
Kohhos mõistis et Hindrik Luk maksko 200 Rubla taggasi ehk andko tallomaa Hindrik Punnmannil.
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Hindrik Toding
Kohto mees Jaan Holsting
Kohto mees Paap Kress
