Peeter Kahre tuli ette ja kaebas et Jakob Sokk on temale 260 kop. wõlga ja palub kohut seda wälja nõuda. 1.50 kop üle on mulle täht ette näidata.
Jacob Sokk wastab: 1 rubla olen mina kül Kahre käest wõtnud ja olen ka temale jälle äramaksnud, mis üle selle on sellest ei tää mina midagi.
Jaan Lobuna tunistab: Tõdu kõrtsis wõtis Sokk 1 rubla Kahre käest laenuks muud ei tää ma midagi.
Jacob Kahre tunistab: Tõdu kõrtsis wõtis Jacob Sokk 1 rubla Kahre käest wõlgu ja ütles et temale enne ka tema käest 160 kop. wolgu on ja lubas siis ühekorraga 260 kop. Kahrele äramaksa.
Mõistus: Tunistuse ja wastu antud tähe järele mõistab Kogukonna kohus, et 
Jacob Sokk peab 260 kop. kahe nädale sees Peeter Kahrele wäljamaksma.
Otsus sai T.r.s.r. §§ 773 ja 774 järele etteloetud. Jacob Sokk ei olnud mite otsusega rahul.
 
