Suureruga Peeter Kuus ja Peeter Kann talo No äraandmise man oliwa Rõuge mõisawalitsuse nõudmise järgmise:Tare kattusse parandamise eest			  1 Rbl. 50 Kop.		tare otsan olewa lauda eest			  5  "      -     "		wana kõlkuse kattusse eest			  -  "     50 Kop.		aida ümber tõstmise eest			 10 "      -     "		1 wackama puuduwa rükki eest			 15 "      -     "		Summa:			32 Rbl.  00 Kop.		
Peeter Kuus ja Peeter Kann nõudsiwasääduse perra tasumist                          			125 Rbl.  _ Cop.		kolme aasta protsent			  18   "     75  "		ristikheina seemne eest			   -    "     75  "		Summa:			144         50 Kop.		
Mõisawalitsuse nõudmine mahaarwatu  32           -
Om Peeter Kuus'el ja Peeter Kann'il saada:  112      50 Kop.
Akna henge ja lukku saiwa lubatu ärawõtta.
Tõiselt oli Kuus'e ja Kann'i nõudmine:Ahjo ja katla truuba tegemine ja kiwid			  22 Rbl.  - Cop.		Wie usse eest			    3  "     50   "		174 sülla kraawi eest 3 1/ lai			  14  "     70    "		kartohwli koopa eest			    5  "       -    "		kate truuba eest			    2  "       -    "		kate kambrikese eest			    5  "       -    "		aija eest			  30 "        -    "		 Summa:			  82 Rbl. 20 Kop.		
Mõisteti:
Selle summa 82 rbl. 20 Kop. sai tühjas arwatu, et seda mitte kontraktin üles ei ole kirjutedu.
Peeter Kuus'le sai 56 Rbl. 25 Kop. wäljamassetu nink Peeter Kann'le II pool, se om 56 Rbl. 25 Kop. arwatu, ent Peeter Kann osa raha (56 Rbl. 25 Kop.) magasi wõla eest wallawalitsuse poolt kinnipantu, selleperast, et Peeter Kann Peeter Kuus poolnik om olnu nink Kuuse wastutamise pääle wallawalitsus magasist wilja om wälja andnu.
Pääkohtumees: J. Kron [allkiri]
kohtumees: J. Pruli [allkiri]
     "               J. Meister [allkiri]
Protokoll No 168 al sissesaadetud.
