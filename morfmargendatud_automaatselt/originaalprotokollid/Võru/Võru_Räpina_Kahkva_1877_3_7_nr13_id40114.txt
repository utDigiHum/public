Kaibus.
Kaibas potsepp Wido Punberg, temmal ollew pumeister Adam Kaddaka käest pottiseppe töö eest 20 Rbl. höb. nöuda. Töö on ollu: Kahkwa Kohtomaja ja Wallawannemba Wido Ritzinge maja, nink Adam Kaddak on neide maja ehhituse podrätsik ollu.
wäljanöudminne:
Adam Kaddak tulli ette nink üttel, temmal ollew mitte ennamb kui 9 Rbl. Wido Punbergile maksta, ent temmal ei ollew ennamb mitte melen kui paljo Wido Punberg temma käest sanu on, nink töiselt ollewa Wido Punbergil Wallawannemba maja ahjo man weel mõnda parrandata, et ahi naekas tömbama
Otsus:
Päle selletamise tulli nidate wälja, et Adam Kaddak 11 Rbl. Wido Punbergi hääs peab wälja maksma.
Päkohtomees Gustav Lepson
Kohtomees Paap Sabbal
Kohtomees Josep Jöks
