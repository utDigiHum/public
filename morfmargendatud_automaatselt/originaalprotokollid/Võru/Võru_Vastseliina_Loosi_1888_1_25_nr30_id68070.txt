Ette tulli Loosi mõisa politsei Armin Birkenberg ja and ülles, et Johann Ribba Ostrowast tullen tee pääl minnes sõnna es kuule ja päälegi trotse ja sõimas temma minno kõige moodu siis palle tedda selle trotsmise eest temmale 25 witsa hoopi anda.
Johann Ribba oli kutsumise pääle ette tulnu ja ütel, et wõib olla, et minna ka halwaste temmale ütelnu aga minna ei mäleta mittagi, sest minna olin täwweste napsi saanu.
Mõisapolitsei andse weel tunnistejid Mõisa xxx Johann Eiche ja Jaan Lokk, et Johann Ribba minno trotsnu on.
Johann Eiche oli ette tulnu ja üttel et Johann Ribba wõtse küll mõisa politseile wastu ja sõimas zeas.
Jaan Lokk olli ette tulnu ja üttel, et minna ka nõnda samma kui Johann Eiche.
Selle pääle es ütle kohtun käuja ennamb mittagi ütlemist olewad, sis
Mõisteti
et selle wastapandmise ja trotsmise eest peab Johann Ribba 15 witsa lööki ehk 3 rubl. masma.
Se otsus saije ette loetus ja ärra selletetu.
Päkohtomees: Johhan Konz [allkiri]
Kohtomees: Jaan Jäärw [allkiri]
      "               Peter Juk [allkiri]
Kirjotj: Piirisild
