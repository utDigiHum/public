Jusal sel 9. Märsil 1873. olli kohus koon P. kohtum. J. Tullus, kohtumees P. Mandli, Jaan Wärs ja abbikohtumees Johan Raig.
Keisriliku Kihhelkonna Kohtu kässu päle saiwa Karraskij mõisa mona mehhe sija kohtu ette tallitedus, - et nema tunistust andwa, kes Karraskij mõisan perrisperremees om.
Eddimanne tunnistaja, Hendrik Sibbul, üttel küssimise pääle: Jaan Sirach om perremees, Hendrik Sirach om ka kül man olnu, kui wanna Sirach teda om sulases kaubelnu. Ja Jaan Sirach ollew oma tütre mehhele Weissnerile ka mõisa nurmest 25 waka alla maad wälja rentnu.
Tõine tunistaja, Rein Pallaw, üttel: Wanna Jaan Sirach om min(n)o ja Jaan Keppi sulases kaubelnu ja mulle ka palga masnu; - Mona wälja andmise man saisap wanna Sirach man ja Hendrik Sirach triigip wakka. Jaan Sirach om ka oma wäimehhele Weissnerile 25 waka alla maad wälja rentnu, mõisa nurmest.
Kolmas tunnistaja, wäikoma mees, Jaan Raig üttel: Haina tükki and mule Jaan Sirach kätte, röa osa mano juhhat tema ja kara kokkopandmise man kaubel ka Wanna Sirach palgaga; nink üts kõrd om Jaan Sirach mulle ka suu sisse üttelnu: Ma olle mõisa pääl perremees kui tulp, ja ma wõi sinno siist tarest wälja wissata,- kos Hendrik Sibbul ka man kuulja om olnu.
Neljas tunnistaja, moonamees, Samuel Zinkson üttel: Wanna, Jaan Sirach om iks ülle wallitseja perremees mõisan, minno om ta sulases kaubelnu, - kui rahha wälja andas, seda anap wanna Sirach ja Hendrik kirjotap ülles, nisama ka monaga, muido ei sa üttegi wakka wilja kui wanna ei lubba anda. - Kui kohegi minnegi lubba Hendriki käest palleldas, sis ütlep wana Sirach: Kes om ülle minno siin mõisa pääl? Kes lubbada tohhip, kui ennegi minna. - Maa asjast üttel Zinksoon, et Weissneril ollew eggan paigan oma jago olme nimelt: Wilja puhastamise man. - Et se nida tõtte om tunnistawa kiik kohtuman istja essi oma nime tade ristitõmbamistega:
                                    Pä kohtumees Jaan Tullus  XXX
                                    Kohtumees Peter Mandli  XXX
                                    abbi kohtumees Johan Raig  XXX
Protokol wälja, Keisriliku Kihelkona
kohtule antu sel 10 Märzil 1873 
                                                             Kirjut. Joh. Leppik
