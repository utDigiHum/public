Ülles ütleminne.
Karraskij moisa perrisherra Alexander von Moller, käskmise päle and Wallawan(n)emb J. Ratnik, koggokonna kohtu läbbi Kutsari tallo perremehhile: Johan Troon ja Johann Warrile ülles, Et Karraskij koggokond, se Karraskij mõisa Kutsari tallo, suur 20 Taadret ja 8 krossi, om ostnu; - nink kässep nüüd koggokonna kohhut, sedda Kutsari maja rendiperremehhile teda anda: Johan Troon ja Juhan Warrile, et nem(m)a Jürri päiwal 23 Aprilil 1873 ennamb sääl majan ei olle sis, kui na omma maja ostmise kaupa perrisheraga, nelja nadaliga selges ei te, se om: sest 27 Nowembrist 1872 kooni 27 Webrini. Sis om neide een õigus selle maja ülle kaddonu.
                                                            Pä kohtumees Jaan Tullus  XXX
                                                             Kohtumees Jaan Wärs  XXX
                                                                    do         Peter Mandli  XXX
                                                     Kogg. kirjot. Joh. Leppik
