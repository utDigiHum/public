Kaibas Wiido Oinas et Toomas Kärtmann ollew temma teenistusest ärräajanud eespäiw ödangu tunnistaja selle man ommawa Joosep Wöikson nink Joosep Kärtmann
Toomas Kärtmann tulli ette nink wastat et Wiido Oinas ei kuulwad temma sönna ja hulkwad öse ümber ja ka poolpäiw ödangu ollew temma ärrä lännud ja eespäiw taggasi tulnud sis ollew temma selle pääle pahandanud ja ollew tedda noomnud et temma peass mitte niidawiisi ümberhulkuma aga Wiido Oinast ei ole temma mitte teenistusest ärräajanud.
Otsus.
Joosep Wöikson nink Joosep Kärtmann ses 23 Oktobris s.a. ette tallitata
Pääkohtunik: Peter Urtson
Kohtunik: Kusta Rämman
Kohtunik: Joosep Oinberg
Kirjotaja F Wreimann &lt;allkiri&gt;
