Karl Pillberg tuli ette ja andis üles et temale on Kusta Riimi käest saada 1 laua eest=1 rub. ja luku eest =50 kop
ja palub seda wälja nõuda. 80 kop olla temale Riimule maksa ja nõnda on siis 70 kop Riimi käest saada.
Kusta Riim ütleb et temale on kül 80 kop Pillbergi käest saada aga maksa ei olewad temale midagi. Sest tema olla laua ja luku eest äramaksnud mida Rein Hammas tõeks wõib tunistada.
Tulewaks kohtupääwaks Rein Hammas ettetalitada.
Eesistuja Johann Waks XXX
Kohtumees Jaan Salomonn XXX
do: Juhann Wasser [allkiri]
Kirjutaja M. Bergmann [allkiri]
