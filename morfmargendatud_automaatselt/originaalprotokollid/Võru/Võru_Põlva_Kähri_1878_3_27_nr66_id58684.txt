Ette tulli kadonu Kõrista Jaan Troon lesk Marri Troon oma Wöörmündri Jakob Liblik ja Jaan Seeba man olemisen, ja tegewa lepingot selle Kõrista poole tallo pidamise peräst:
Lesk Marri Troon anab sedä poolt tallo oma poja Karli Seeba kätte pidada, kõige selle kraamiga mes sääl maijan om, nink jääb esi ka Kaarliga ütten eläma; nink jääb elo kõik ütte ni et emä käen om keetmine ja maia perrenaise hool, ja saab Karli käest 8 rubla egäl aastal ja 2 punda linnu rõiwas latsega kokko. Ent maia wälimaiste asjo tallitaia om Kaarli. Jalla känkitse muretamine saab Kaarli läbi.
Maija jääb Kaarli kätte niikauges kui Mari lats Anna Troon eälises saab, sis saab Kaarli käest see maija selle kraamiga mes temä kätte nüüd saab jälle tagasi Anna Troonile.
Kui sünnib et rahul olemisega ei sa elletus jääb Kaarli majast prii. Ja Kaarlil ei olle minkisugust nõudmist ei palka ei ka kahjo tasomist maijast, olgu et kui kraam suuremb om sis sedä.
Kaarli Seeba wõttab sedä poolt tallo pidamist henda pääle kõige walla nink kroonu massu ja selle tasomises, ette pääle mes emä pidamise lät, ja ka hoone parandamise ehk kõrran pidamises hendä pääle. Ja anab ka terwelt ja hää kõrra man käest ära.
Oma lepingo kinituses kirjotawa hendä 
Lesk Marri Troon XXX
Wöörmünder Jaan Seeba XXX
   "  Jakob Liblik XXX
Karli Seeba XXX
Otsus:
Kogukonna kohtu poolt saab een olew leping kinnitetus.
Pääkohtomees Jaan Luik
Kohtumees Johhan Punnak
   " Jaan Hingo
