Wallawanemb J. Leppäson oli Peter Kärbe ette tallitanu ja kaibas et seesama tegewad hendäle wastse maia wanale üttitsele majale, mes Jaan Hämäläse jaos jääwad, kogoni manu tegewad nii et paljalt 4 sülda wahet jääwad. Ja nõwab et Kärbe saas keeltus sedä tegemast, ta wõiwat kawembahe tettä, kos näütetus saije.
Peter Kärbe wastutab et temäl ei olewad wõimalik muijale kohegi tettä, ei olewad kohegi sündlik, sest Jaan Hämäläne wõiwad ka sedä wana tarre, mes ka ümbre tettä tullep, tõiste paika kawembahe tettä.
Otsus:
Kogokonna kohus mõistab, et Peter Kärbe ei woi oma wastet maja mitte wanale majale külge tettä, enegi temä peäb tõiste paika tegemä, kos sündlik, kui temä esi sündliku kottust ei läwa saab wallawalitsuse poolt ette näütetus kottusse pääle ärä tegemä.
Otsus kuuluteti § 773 ja 774 perrä T.S.
Peter Kärbe es ole rahul ja saije opus kiri antus selsamal päiwal.
Pääkohtumees Jaan Luik
Kohtumees Peter Tallomees
     "   Jaan Hingo
