Juhhan Seme tul ette ja kaibas, kui temma wango pääl olli haina kaonu sis läts temma Peter Urberg manno ja pand temmale warkusse süüs. Sis om Peter Urberg temma sõimanu , ja 4 wori russikuka rindo lonu.
Petre Urberg and wastust, kui Juhhan om temma warkas nimmitanu, ja om temma Kunnaki weel warrastanu, sis sai temma meel pahhas soimas temma wasto ja om temma Juhhanit tõukanu kaukele kui temma päle lits.
Tunnistaja Märt Müürsep ja Juhhan Leer ütlewa wälja kui ne naijewa kui Peter Urberg om 4 wori russikuka lonu.
Om moistetus sanu mollemba sawa 30 loki nink woip Peter neid loke rahhaka tassuma.
