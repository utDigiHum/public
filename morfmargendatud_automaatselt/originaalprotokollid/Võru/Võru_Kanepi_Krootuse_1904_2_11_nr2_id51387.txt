1904 a, 11 weebruaril ilmusid ette Jurjewist kase mnt N 1 majas Paul Johani p. Ratnik ja Krootuse wallast Ewerdi talust Johann Johani p. Ratnik ja palusid akti  raamatusse kirjutada, et esimene, s.o. Paul Ratnik on teise lepingu osalise Johann Ratniku käest isa testamendi järgi kakssada wiiskümmend /250/ rubla täiesti kätte saanud ja ei ole Paul Ratnikul Johann Ratniku käest enam midagi nõudmist.
Johann Ratnik [allkiri]   Paul Ratnik [allkiri]
Üks tuhat üheksa sada neljandamal aastal weebruari kuu üheteistkümnedamal päewal on Krootuse wallakohus Krootuse wallamajs eesseiswat suusõnalist lepingut, mis talupojad Johann ja Paul Ratnikud oma wahel teinud, kinnitanud, mis juures wallakohus tunnistab, et lepingu osalistel seaduslik õigus on aktisi teha, et nemad kohtule isiklikult tuttawad ja et peale selle lepingu ette lugemise temale Johann Ratnik ja Paul Ratnik oma käega alla kirjutanud on.
Kohtueesistuja as D Paloson [allkiri]
Kirjut kuddo [allkiri]
