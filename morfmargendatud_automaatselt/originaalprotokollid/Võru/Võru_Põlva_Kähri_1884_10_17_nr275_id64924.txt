Leena Kelt tulli ette ja kaebab et Peter Westberg olla teda ähwardanud maapäält ärakautada ja palub teda niisuguse ähwardamise eest trahwida.
Leena Kelt andis tunistajateks üles Liis Olesk ja Jaan Holz.
Jaan Holz tunistab: Mina juhtusin ükskord Leena Kelt ja Peter Westbergi elutusest mööda mine kus Leena Kelt ja Peter Westberg üks teisega tülitsesid ja Leena Kelt minule siis kaebas, et Peter Westberg olla ähwardanud teda maapäält ärakautada.
Siis küsisin mina Westbergilt: Mis pärast sina teda ähwardanud oled? siis ütles Westberg: Ähwardasin jah."
Peter Westberg wastab sellepääle, et tema ei olla millaski Leena Kelti millaski ähwardanud ära häwitada.
Otsus.   Tulewaks kohtupääwaks Liis Olesk ette talitada.
Lepisid oma wahel sellega ära, et edespidi ei tohi enam teine teist sõimata ei ka ähwardada.
