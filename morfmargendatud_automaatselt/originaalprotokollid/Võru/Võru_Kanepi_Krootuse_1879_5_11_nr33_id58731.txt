Kuwendalt.  Tulli ette Michel Järg ja Johann Punna nink sai Johann Punnale kohto poolt kõwwa käsk antus, et peäp Michel Järg ja temma poja Tannil eest kroono ja koggokonna masso ülle kige 8 kirjota kattessa rubla kattessa päiwa aja seen Karraskij wallawan-
nemba Michel Wõsso  kätte ärra massma selle et Johann Punna Michel Järg wõlla eest käemehes om lööno, ja tolle eest wastotada lubbas et masso kiik mastos saawa mes ennegi Michel Järg ja tem(m)a poig wõlgo ommawa. -
Nink Michel Järg peäp se rahha Johann Punnale ses 3 februaris 1880 kirjota 18 kattessakümmend ärra massma. -
Sai neile mõllembille ette loetus mes kirjotedo olli ja lätsiwa rahhon wälja minnema kos Johann Punna ütles minna küll pean se rahha hendä käest wälja massma, ent ma palle et sis ka wistist kündle päiwas kätte peän saama. -
                                                        Pääkohtomees  J. Tullus  XXX
                                                             Kohtomees  J. Uibo  XXX
                                                    teine           do      J. Leppäson  XXX
