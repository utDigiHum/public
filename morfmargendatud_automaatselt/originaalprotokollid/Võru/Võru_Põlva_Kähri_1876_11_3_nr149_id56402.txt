Ette tulli mõisa walitseja J. Krentz ja kaibas karjamees pääle, kes kõigin asjon mõisa walitsuse käsu wasta panneb, oli kats kõrda karja jootmata jätnu, nink kui midagi saab ütteldus sis annab niisugutsed roppud sõnnad wasta, mes häbi om üttelda. Wallawanemb nägi ka sedä päält kui temä karja es jooda, üts päiw jättes kari kogoni lauta ja tõine päiw laskis kül pool karja walja, aga ajas niisama ilma jootma tagasi.  Ja nõwab Herrä käsu pääle kogokonna kohtu sundmise perrä trahwi.
Karjamees Jüri Aasweld ette kutsutu ja see kaibus kuulutetu, ütlep et temä olewad kõik kül tennu mes tetta olnu, siski jäänu ütskõrd kül karri jootmata, sest et aig lõpnu muu tarwituste pääle ära. Nink tõistkõrd lõpnu wesi kaiwost ära.
Wallawanemb Lepäson tunistab, et edimäne lauda täüs lännu kül ilma jootmata tagasi toine lauda karri joonuwa tiigist, eä weere alt, lasknuwa põlwale maha ja joonuwa. Ja pääle sedä tülitsenuwa weel walitsejaga mes walitseja meest manu ei aija.
Otsus:
Kogokonna kohus mõistab, sellepääle et karjos Jüri Aaswelt karja mitte ei wiisi tallitada, saab temä karja ametist prii, ja mõisawalitsus woib üts tõine mees karja pääle panda, ja Jüri Aasweld peäb töö pääle jääma. Ja selle eest et temä karja jootmata om jätnu ja mõisawalitsuse wasta krop om olnu, peäb temä 3 rubla trahwi masma waeste laatikuhe.
Otsus kuuluteti § 773 ja 774 perrä T. Sääd.
Jüri Aasweld es ole selsamal päiwal otsusega rahul.
Neesama kohtomehe.
