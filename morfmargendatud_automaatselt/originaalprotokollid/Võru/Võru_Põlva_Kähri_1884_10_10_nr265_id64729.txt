Sai protocoll № 250 edasi seletud. Tulnud olid kaebaja Ann Kaur ja kaebtuse alune Jaan Wiga. Tunistaja Mari Subul ei olnud mittetulnud.
Ann Kaur ja Jaan Wiga lepisiwad oma wahel nõnda ära: Jaan Wiga maksab Ann Kaurile 80 rubla lapsekaswatimise raha järgmiselt: terminidel:  See aasta 10mal Nowembril   maksab 			15 rubla ja tulewa aasta		23mal Aprillil 1885     "			  5     "     ja		10mal Nowembril 1885  "			10   "        ja pääle selle		iga aasta 10mal Nowembril			10  rub. nõnda pitkalt		kui 80 rub. täis saab.					
 Otsus:   Niisugust protocolli sissekirjutada.
Eesminew leping saab kogukonna kohtu poolt kinnitud ja tunistud.
             
