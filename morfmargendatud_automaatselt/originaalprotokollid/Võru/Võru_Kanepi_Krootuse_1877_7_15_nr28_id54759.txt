Karraskij Koggokonna Kohhus, Jusal sel 15.al Julil 1877.
                                          Mann olliwa Eenistja, Jaan Tullus.
                                              "          "     Mannistja, Jakob Uibo.
                                              "          "   , Mannistja, Jaan Leppason
Tulli ette, Leesmitti perrisommanikko lesja Miina Seeme wöörmünder Johan Karrask, Põlgastest, ja kaibas et Serristo moisa kandi-
mees Karel Aerman, ollewa lesja Miina Seeme mõtsast 57 puud, ennambiste kik keo puu, arwatta läbbistikko 2 1/2 tolli jämme, mahha raggonu, ja henda wilja ette aijas tennu, nink nõüdis selle eest 38 rubla 75. koppik Miina Seeme hääs, kahjotassomist. - Sest et Karel Aerman katte kõrra kutsumisse pääle, kohto ette es olle tullnu, sis jäije kohtomoistminne teise kohto päiwa pääle. - Nink wöörmünder Johan Karrask pallel kohhut, sest et tem(m)a essi ka kohtomees om, nink just üttel päiwal, Põlgasten, ja Karraskin, kohhus koon om, nink mitte woimalik ei olle Karraskij kohto ette tulla, henda assemelle Adam Raig Karraskijst tallitada laske, mes kohhus ka lubbas hääs wõtta. -
                                                                           (Allkirri)
