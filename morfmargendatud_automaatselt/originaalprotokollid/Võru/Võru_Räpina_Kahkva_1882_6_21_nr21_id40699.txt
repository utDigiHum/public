Kaibas Kusta Parmson, et tema piirimees Kusta Helfer olewat 5 aastat tema maad 3 Kapp ala üle piiri pidanud. Tema om pidanu maa möötjat kutsma, kes nüüd see piiri asja om nii äraseletanu, ja selleperast et see piiri süüd om Kusta Helferi pääle sadanu, nöwwab tema Helferi käest maamõõtja kulu 12 rbl. ja üle piiri maapidamise eest 13 rbl. Kokku 25 rbl. kahju tasumist.
Kusta Helfer wastutas selle kaibuse pääle, et mis perast Kusta Parmson joba warembide seda piiri seletust es nöwwa. Lubab pool maamõõtja kulu massa.
Neil kohtukäijatel es ole enam midagi ütlemist ja kohus tegi
Otsust.
Kohus möistis: Kusta Helfer pea Kusta Parmsonile maa mõõtja kulu piiri seletamise perast masma 5 rbl.
See otsus sai sellsamal päewal mölembile kohtukäijatele Talurahwa sääduse 1860 aastast §772 ja §773 pöhjuse pääl kuulutetud, mis pääle Kusta Parmson 23 Juunil s.a. kohtule kuulutas et tema selle otsusega rahul ei ole.
Pääkohtunik Joh. Lepland
Kohtunik J. Kirhäiding
Kohtunik J. Oinberg
