N 4. Waimela kogokonna kohus sel 14 Febr. 1880. Koon olliwa: 
Pääkohtomees Adam Sulg
Kohtomees Peter Rikkand
d-o Karl Liin
Tulliwa ette Ann Zopp nink Adam Rikkand ja teijewa siin peran tulewat leppingut niida, et Ann Zopp and täwweliku oma toitmise ja üles pidamise al oma zõdse vollmachti teda surnu kassada sisse sisse masmise raha kui ka kassadest saadawat matmise abiraha wastu wõtta, nink Adam Rikkand luppas kui tema zõdse Ann Wõitra üts kõrd kooles tälle matmises raha 70 rubla massa. Et nema niida lepnu, tunnistawa oma käega ala kirjotamise läbi:
Leppja Adam Rikkand xxx Ann Zopp +++
Pääkohtomees: Adam Sulg. [allkiri]
Kohtomees: Peter Rikkand. [allkiri]
d-o Karli Liin [allkiri]
