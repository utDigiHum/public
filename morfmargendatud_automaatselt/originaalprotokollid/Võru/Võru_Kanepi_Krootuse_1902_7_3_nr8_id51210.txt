1902a. 3 Julil ilmusid Krootuse wallakohtu ette Krootuse wallamajasse talupojad Hurmi mõisa all olewa Mustjärwe Taluomanik August Georgi poeg Hilb ja sealtsamast talust Johann Asi Kusta poeg ja palusid järgmist heaste läbi mõteltud ja kokku kõneltud lepingut kinnitada.
§ 1
August Hilb annab Jüripäewast 1903a. peale Mustjärwe talu maast Punso talu ja Walgjärve mõisa piiri wastu neli wakkamaad rendi peale kolmeteistkümnes järjestikku aastas. Selle maa tükki piirid on lepingu osalised tunnistajatte Jaan Paris ja Johann Haak juures olekul üle waadanud.
§ 2
Johann asi maksab selle maa eest August Hilble renti esimestel wiiel aastal iga aasta 20 rbl ja järgmistel 8 aastal iga aasta 25 rbl.
§ 3
Rendile wõtja wõib selle maa tükki peale elu hoone ühes kõrwaliste hoonetega üles ehitada ja nende aja lõpul peab ta , Asi, need hooned ära wiima ja maa rendile andjale kätte andma. Hoonete üles ehitamine rehkendatakse Jüri päewast 1903 aastast peale. Peas olema, et Johann Asi renti järgi ei maksa, warastas ehk wargust wastu wõttab mis awalikuks tuleb, siis on see leping katski ja Johann Asi peab juba siis oma maja ja kõrwalised hooned Mustjärwe talu maa päält ära wedama. Rent on Asil iga aasta järgi maksta.
§ 4
Tema August Hilb lubata ei ole Asil õigust oma maja kellegi edasi müüja ehk kedagi senna korterisse wõtta. Asi peab oma piirid puhtad hoidma ja ei tohi õlgi ehk sõnnikut wälja anda. See leping jääb ka kindlas - lepingu osaliste pärijate kohta ja ka kui Mustjärwe  talu teise omaniku kätte läheb.
Lõppeks lepingu osalised oma allkirjadega tunistawad, et lepingu hind mitte üle 300 rubla ei käi, et leping mitte wälja mõteltud ja et kõik waidlused tema wastu kõrwale jättawad.
Johhan Assi  [allkiri]  August Hilb [allkiri]
üks tuhat üheksa sada teisel aastal Juuli kuu kolmandamal päewal on Krootuse wallakohus Krootuse wallamajas ees seiswat suusõnalist lepingut, mis August Hilb ja Johann Asi wahel tehtud, kinnitanud, mis juures wallakohus tunnistab, et lepingu osalised kohtule isikulikult tuttawad, et nendel seaduslik õigus aktisi teha ja et peale selle lepingu ettelugemise lepingule August Hilb ja Johann Asi täieste rahul olles tõesti oma käega allakirjutanud on.
Kohtueesistuja J Kuhi [allkiri]
Kirjutakkuddo [allkiri]
1907a.4. aprillil ärakiri August Hilbile wälja antud ja 15 kop. tempelmaksu sisse nõutud.
Kohtueesistuja J. Kuhi [allkiri]  Kirjutaja Kuddu [allkiri]
