Ette tulli Johani poig Jakob Raudsepp ja kaibas, et Tannila poig Jakob Raudsepp tema kartohwli koobast kartohwlid warastanu arwada 3 wakka, tema nõudwat kahjo tasumist 50 kop. wak kokko 1 Rbl. 50 kop. Tunnistaja ollew Jaan Kall ja Jaan Raudsepp. Wöörmünder Peter Hillep ollew warguse üle kaenu.
Wastut kaibuse pääle Tanila poig Jakob Raudsepp, et tema Johani poig Jakob Raudseppa kartohwlid ei ollew warastanu.
Tunnistaja Jaan Kall 24. aastat wana Lutteri usku tunnist, et Tannila poig Jakob Raudsep tulnu tälle tee pääl ööse wasta ja olnu kartohwli kottiga säljan, ent kost ne toodu, ei teedwat tema.
Tunnistaja Jaan Raudsepp Johani poig Jakob Raudseppa weli 24 aastat wana Lutteri usku tunnist nisamate kui Jaan Kall.
Wöörmünder Peter Hillep kes warguse üle kaenu üttel wälja, et Tannila poig Jakob Raudseppa kartohwli Johani poig Jakob Raudseppa kartohwlidega kokko sündinu ja olnu ka Tannila poig Jakob Raudseppa zuwwa jäle kartohwli kooba lähital kost warastedu olnu, nätta.
Nema es lepi.
Otsus:
Tannila poig Jakob Raudsepp sai süüdlases arwatus ja peap Johani poig Jakob Raudseppale 4 nädali aja sisen a dato 1 Rbl. kahjo tasomist masma ja trahwis lähap 24. tunnis oma kulu pääle kogukonna türma.
See kohtu otsus sai kohtun käüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus. Tannila poig Jakob Raudsep nõwwap Appellationi.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
     "                P. Klais [allkiri]
