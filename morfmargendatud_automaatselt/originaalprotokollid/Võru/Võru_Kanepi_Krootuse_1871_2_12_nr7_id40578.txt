Neljas kohhus sel sam(m)al päiwal.
Krotusmees Taniel Raig tulli ette kaiwas, täl ollew Jaan Ehhina käest 50 kp saija. 
Ehhin sallas sedda ärra enge üttel et Tanieli naine ollew kül tem(m)a naise käest kui ta Ehhamarro kõrdsin om olnu wõtnu:
                                                    3 looti sinne kiwwi  27 cp
                                                    2 puddelid pairastit  20 -
                                                    1 mõddu                     10 -
                                                    1/2 naela kimmelid     8  -
                      ----------------------------------------------------
                                                   Summa                      65 cp
Ja nimodu tullep weel 15 cp manno massa. -
Kohtu otsus: Ehhin peap Taniel Roijale se 50 kp ärra masma ent selle 65 kp perrast kaibaku Krotusse koggokonna kohtule.
                                                                           P. Undritz
