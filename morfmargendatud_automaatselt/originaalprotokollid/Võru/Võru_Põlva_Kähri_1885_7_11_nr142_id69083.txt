Kährist Daniel Wäiko tulli ette ja kaebas et Mart Pillberg on tema maast kus wõsu pääl kaswanud 1/2 wakamaad üleskünnud ja põlluks teinud ja nõuab selle maa eest 25 rub. metsa ärarikkumise kahju tasumist.
Mart Pillberg Kährist wastab seepääle: Mina kündsin Andres Wals lubaga selle Daniel Wäiko maatüki üles sest et tema ütles selle oma maa tükki olewad ja seepärast ei jätnud ka mina teda Daniel Wäiko käsu pääle kewadi teda põlluks tegemata.
Lepisiwad oma wahel nõnda: Daniel Wäiko lubab 2 aastad seda maad Pillbergile pidada mis eest Pillberg Waikole 1 rub 50 kop. aastas renti maksab. See on kuni Juripääwani 1887. Kus siis Pillberg selle maa maha peab jätma.
Kogu konna kohus kinnitab niisugust lepingut.
Pääkohtu mees: Johann Waks
Kohtumees Jaan Salomon
do:
