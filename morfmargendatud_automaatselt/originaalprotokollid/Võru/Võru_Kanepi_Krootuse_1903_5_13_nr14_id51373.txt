1903 a. Mai kuu 13 päewal ilmusid Krootuse walla kohtu ette Krootuse mõisast Krootuse mõisa omaniku Johan Kurrikoffi wolinik Tõnis Kurrikoff ja Jaan Herne Hendriku p. ning palusid järgmist heaste läbi mõteldud ja kokku räägitud suusõnalist lepingut kinnitada:
§ 1
Jaan Herne saab Krootuse mõisa maast 1 (üks) wakamaa ja 1 (üks) kappa oma maja ümber maad pidada ja pruukida, seeni kui tema, Jaan Herne, ja tema praegune abikaas elab. Selle maa eest peab Jaan Herne ehk ta naene (10) kümme rubla renti maksma iga aasta.
§ 2
Jaan Herne ehk tema naene ei tohi omas korteris ühtegi wõerast, weel wähemalt kahtlasi inimesi pidada, peale ausaste ja õiglaste ennast üles pidama ja kõik seaduse wastalised sündmused oma ligikonnas mõisa walitsusele üles andma.
§ 3
Kui Jaan Herne ehk tema praegune abikaas warastawad ehk wargusi wastu wõtawad ehk midagi seaduse ja korra wastalist elu elawad, ehk omas majas sündida lasewad, niisama iga aasta 1seks Aprilliks ja 1seks Oktobriks maa ja koha pidamise rendist poolt ette ära ei maksa, siis wõib mõisa omanik ehk tema asemik teda sellest majast wälja ajada ja ehitus maha lõhkuda laske ning pruugitaw maa käest ära wõta ilma kohtuda.
§ 4
Pääle Jaan Herne ehk tema praeguse abi kaasa surma ei ole tema perijatel enm ühtgi õigust seda maja edasi elamiseks pidada. Nemad Jaan Herne pärijad on kohustud peale tema, Herne ehk praeguse abikaasa surma maja kohe mõisa maa pealt ära wiima, wundament wälja kaewama ja maa mõisale tagasi andma, ilma et neil pärijatel mingi sugust õigust oleks kohta pidada ehk omandada.
§ 5
§§ 3 ja 4 öeldud põhjustel ei ole Jaan Hernel, tema abikaasal ehk pärijatel ühtegi õigust kohtulikul teel alles jäämise , wälja heitmise ehk maja maha lõhkumise korral mõisa omaniku käest kahju tasu nõuda.
Kustutud täht "a" nitte lugeda.
T Kurrikoff [allkiri]    Jaan Herne [allkiri]
Uks tuhat üheksa sada kolmandamal aastal Mai kuu kolmeteistkümnedamal päewal on Krootuse wallakohtule Krootuse wallamajas oma koosolekut pidades eesseisew suusõnaline leping kinnituseks  ettepandud Krootuse mõisa omaniku Wiljandi linna kodaniku Johann Ado poeg Kurikoffi wolinikust Tenis Kurikoffilt, kes tallitab wolikirja järgi, mis ustawaks on tunnistatud Wõru Notariuse Julius Friedrichi poeg Tielsterni poolt 1899a. 23 oktobril № 820 all ja wallakohtole ärakirjas ühtlasi alguskirja ette näidates, ette on pantud ja Krootuse mõisas elawast talupojast Jaan Hindriku poeg Hernest mis juures wallakohus tunnistab, et lepingu osalised kohtule isikulikult tuttawad, et nendel seaduslik õigus on aktisi teha ja et peale selle lepingu ette lugemise lepingule Tenis Andrese poeg Kurrikoff ja Jaan Herne oma käega allakirjutanud on.
Kohtueesistuja: J Kuhi [allkiri]
Kirjut KKuddo [allkiri]
