Ette tulli Potsep Peter Orraw ja kaibas, et kandimees Jakob Hallopil ollew weel temmale 22 Rubl 50 Kopkat tolle maja eest massa ja termin ammo möda.
Selle päle kutsuti kandimees Jakob Hallop kohtu ja loeti kaibus ette ja temma ütles kül ni pallo massa ollew, ent tolleperrast ei maswat, et mis Potsep Peter Orraw ülle leppingo haino ja Õlgi ärra wino kelle man temmal tunnistaja ollew.
Lepping.
Potsepp Peter Orraw ja Jakob Hallop tulliwa ette ja Peter Orraw kulutas leppingut sedda modi, et Jakob Hallop maswat 10 rubla Janipäiwa ja 10 rubla Märdipäiwa s.a. ja Jakob Hallop ütles ka se leppingo õige ollewat ja lubbas ausaste täuta.
Sedda leppingut kinnitawa
Päkohtumees Peter Pedras XXX
Kohtomees Jaan Mikheim XXX
         "           Hendrik Hallop XXX
Kirjotaja P. Haberland
