Kaibas Juula Urgard, et umbes 9 Oktobr. 1880 olewat Widrik Urgard temaga kokko elanu, sellest olewat temale 11 Juulil 1881 laps sündinu ja Widrik Urgard olewat selle wäärlatse esa. Tema nöwwab Urgardi käest latsekaswatamises 100 R.
Wastutas Widrik Urgard selle kaibuse pääle salgamisega ent pääle wastastiku seletamisi tulli wälja, et Urgard olli süüdlane, mida ta ka tunistas.
Otsus.
Lepsiwa ära niida, Widrik Urgard massab Juula Urgardile latsekaswatamises 1882 aastal 7 Rb. 1883 7 Rbl. ja 1884 6 Rbl. Kokko 20 Rbl.
Pääkohtumees J. Lepland
Kohtumees J. Kirhäiding
Kohtumees J. Oinberg
ära masnu 1 Rbl. köige edimalt
ära masnu 3 rbl. sügisel 1882
ära masnu 3 rbl. 28 Weebr. 1883
ära masnu 3 rbl. 7 Novembril 1883
ära masnu 1 rbl. 13 Maerzil 1884
ära masnu 3 rbl. 14 Januaril 1885
ära masnu 5 rbl. 18 Februaril 1885
