N 64 Waimela koggokunna kohto sel 11 Decemb 1873 
Päkohtomees Jürri Waask
Abbi Peter Pallo
dito Jaan Zuchna
Moisawallitsus kaiwas Tallipois Albert Witholtsi päle et nimmitedu kaest üts hobbese taose omma ärrä kaddano hinna perra 6 Rubl. ja temma mitte taosata ei tahha.
Sepäle om Albert Witholts wastutanno, kui wanna tallipois Peter Jaakson temma kätte talli krami andse sis olli 13 taose ja 1 piddiwa karjose käen ollema, agga karjose käen taosit es olle. 
Michel Pauts se karjos om sepäle kostno. (temma ka) tunnistano se olli õkwa lõuna aig kui Albert Witholts Peter Jaaksoni käest hobbese rista wasta wõt, ja olli ütte taose pudos konna olli tema laskno Michli Pautsi nimme päle kirjota. 
Sepäle om Peter Jaaksoon kostno päev se kui temma hobbese rista käest ärrä and sis olli temmal ütte taose kõrtsimehhe Kallinge käen, kombe kewwade nore herra olli lubbano Kallingele ütte korra päle, päle se temma ka neid ennamb taggasi es nowwa.
Kalling om sepäle kostno päle se kui Jaakson rista olli käest ärrä andno, om Jaakson küssino kas nore herra om neid taosit takkan otsno mis temma kätte omma sano, ja temma om kostno: ei olle otsitu mitte takkan, sis om Jaakson kostno: ne taose woide teije pitta iggawes, sest neid teije käest keagi ennamb ei otsi. Selleperrast et Jaaksoon neid taosit om Kallingele om lubbano:
Moistus:
Peter Jaakson saab selle taose warguse eest 20 witsa löki. 
Pakohtomees Jurri Waask [allkiri]
Abbi Peter Pallo [allkiri]
dito Jaan Zuhna [allkiri]
Peter Jaakson ei olle wõtno sedda trahwi mitte wasta, selle perrast et temma terwisi ei  kanna ja temma pallep sedda asja Keiserliko Werro makohto kätte sada. 
Kirjotaja JohKangru [allkiri]
