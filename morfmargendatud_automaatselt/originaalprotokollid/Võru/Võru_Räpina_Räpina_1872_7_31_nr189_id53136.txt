Räppinä Sare moisa Herrä assemal Fritz Mälberg kaibas et säält tallopiad om linna tenno ülle kelo senna põllo päle kus neil lubba es olle tettä
1., Hindrik Tilkson om 2 wakka maad linna tenno kolmanda nurme päle kus temmäl lubba es olle mitte tettä, ja 1/2 wakkamaad peab rohkemb ollema linna tetto kui ristk haina om olno.
2 Kristian Kristowask om 1 1/2 wakka alla linna rohkemb tenno kui ristkhaina, ja peab ka keik kolmada nurme päle ollema tetto
3 Kristian Sila Norruskil om 1 wakkama linna rohkemb tetto kui Ristk haino, ja peab ka ossast kolmanda nurme päle ollema tetto.
4 Josep Kõps Pallo külläst peab 2 1/2 wakka alla linna rohkemb ollema tetto kui lubba olles olno, ja peab ka ossast kolmanda nurme päle ollema tetto.
1 Küssitus sai, üttel Hindrik Tilkson et nurmed ei olle weel jonen enne kui teise aasta sawad nurmed lahko ja södä, selle perräst om se teggeminne ni olno.
2 Kristian Kristowask üttel: et om kül rüggä alle päle tetto, selle perräst et nurmed ei pea jonen ollema enne kui teise aasta sawad nurmed joonde, ja selle eest peab lisnod maad ka rohkemb saisma ollema.
3 Kristian Sila Norrusk üttel, et temmä om kül tetto linna, gga temmäl peab rohkemb saisman ollema maad.
4 Josep Köps Pallo külläst üttel et kodda politsil om 1 1/2 wakkal alle tetto, ja temmäl issi peab 3me wakka alla tetto ollema õigussema pääl.
Soldat Gustaw Kangro üttel hendäl 1 mööt tetto ollewa
Kohhus mõistis et kohto polest hommen saap (hommen) neo linna põllod ülle kaeto ja mis ülle kelo ehk kontrahti om tetto saap Ocksioni wisil ärrä müüdo.
