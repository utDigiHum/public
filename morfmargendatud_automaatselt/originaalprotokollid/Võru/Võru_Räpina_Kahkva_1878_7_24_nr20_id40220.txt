Kaiwas Peter Kübbar et täl olnu Kahkwa mötsa ülle waatja Jakop Roosnek käest haina pole päle tetta, mes Wido Rämman temma wäimees wötnu, ent nemma möllemba ütten tennu; Nink ollnu se kuhhi hainu eesalt pssa arwata pool ärra warrastetu, nink temma sedda ei tija kes sedda tennu om; ent töist körd sanu temma warga kätte kes sedda perra jänu jaggo ka ärra winu nink ta olnu Jakop Roosnek, nink nöwwap neide hainu eest 20 höbbe Rub ja üttel neid kats koormat ollewat.
Ette kutsuti J. Roosnek nink wastut et temma ei olle neid hainu ilma teedmata winu, enge saatnu Jakop Woiksoni läbbi sönna Wido Rämmanile ent ei olle tulnu; ent saatnu töist körd sönna, et Wido Rämman tulles hainu wima, ent ka ei olle tulnu, nink temma Jakop Roosnek, lännu omma sullassega kuhja manno nink löidnu kuhja ärra kakkutu ollewat, ja lasknu kohto meest Paap Sabbalit kutsu ülle kaema, nink se arwanu et pole haina omma ärra widu nink andtu lubba haine ärra wija mes ülle olli jänu, nink temma winu ka ne haina kik ärra koddo kolmanda körra aigo, ja nöwwap 100 Rubla et Peter Kübbar tedda wargas üttelnu.
Ette tulli kohto m. P. Sabbal nink arwas ka et pole haina ärra wöetu olliwa; ent es ütle mitte lubba andwat J. Roosnekile hainu ärra wija koddo; enge üttel J. Roosnekit kutswat kohtu päle selsammal kolmapäiwal hainde perrast selletama.
Ette kutsuti Wido Rämman nink wastut et eddimalt hainu toma kutsmisse sönna olli ilda öddango todu nink selle ei olle temma woinu minna; ent töise kutsmisse päle lännu temma nink jänu weidi ildas, nink seni olnu J. Roosnek säält mant ärra lännu; ent kolmanda körra aigu ei olle tedda keski kutsnu hainu wima.
Otsus
Et assi ennamb 10 Rubla wäärt pallelda Keiserlikul Wörro makohtul wimese otsust anda.
Päkohtomees G. Lepson
Kohtomees P. Sabbal
Kohtomees J. Jöks
Päkohtomees G. Lepson
Kohtomees P. Sabbal
Kohtomees J. Jöks
Protokol wälja antu sel 31 Julil 1878
Kirjotaja J. Käis &lt;allkiri&gt;
