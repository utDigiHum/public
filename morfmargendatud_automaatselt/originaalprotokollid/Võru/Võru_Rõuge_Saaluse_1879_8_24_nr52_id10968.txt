Ette tulli J. Jassik ja kaibas et Simon Rosenblat ollewat temma hainamaa ommi partse läbbi 1/6 wakkala wäega ärra sõkkuda lasknu ja nüid ka weel läbbi käwwa ja nõwwap 5 rubla kahjo tassomist.Simon Rosenblatt ütles et sääl ei olle suurt kahjo sanu ja olli ka se monameeste käen niita. Wüürmündre omma sedda kahjo ülle kaenu ja arwanu et 2 rubla kül kahjo wäärt om.
Mõistet: Simon Rosenblatt peap 2 rubla Jaak Jassiku hääs  4 näddali sissen wälja masma.
Se mõistmine om § 773 ja 774 kulutedu ja oppusleht antu.
Pääkohtumees Jürri Waldmann
Kohtumees Tannil Oinas
Kohtumees Tannil Purge
Kirjotaja /Jõggew/
Protokoll wälja sanu sel 20 Octob. 
