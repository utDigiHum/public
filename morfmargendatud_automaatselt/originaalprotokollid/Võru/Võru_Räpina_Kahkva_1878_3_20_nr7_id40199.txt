Kaiwas Heibrich Zärens et sel 16 wasta 17 Märtsi Öse om ree jälgi löutu Rehhe ümbre nink wimate Rehha ahjo sisse mulku tettu ollewat ja ärra warrastetu 26 punda linnu, nink neid ree jälgi möda seni takkan lännus seni kui Rassina walda kost neid mötsast omma löutu.
Ette kutsuti Gustaw Otsing nink wabband eddimält et temma omma welle Pebule ei ollu middaki könnelnu; nink tija ka wargussest middagi; nink tunnist et kui temma wellele middaki ütlep sis om ni hä kui ärraki tettu Ent tunnist perrast et kui temma 27 Februaril omma welle pool käunu sis om Peep Otsing temma käest nöudnu mes teije tete? Meije rappame linnu Rehhen ent kas näid ka sääl hoijetas temma Gustaw üttelnu et säälsaman Rehhen ka hoijetas ni wõrd temma tijap.
Gustaw Lepson
Paap Sabbal
Peter Wosmi
