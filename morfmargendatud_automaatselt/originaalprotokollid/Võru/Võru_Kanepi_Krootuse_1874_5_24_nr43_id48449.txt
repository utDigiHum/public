Teine kohhos sel sam(m)al päiwal  Kohtomehhe ne samma
Tulli ette Jürri Paales ja kaibas Heinrich Undritz pääle et ollewat temmale 1 nimmelt tellito õlle waat wõlgo andno ja nüüd ei andwat ennamb mitte kätte, ja üttel et minna olle temma eest 8 rubla essi masno ja tahhan ka tood samma kui temma minno waat ärra toop sis annan minna temmale 8 rubla jälle kätte. -
Kutsoti ette Heinrich Undritz ja sai Jürri Paales kaibus ette loetus, tolle pääle üttel H. Undritz om küll õige et temma käest olle 1 õlle waat saano, enge ollen se waat Märt Mollek kätte andno, ja temma om Wanna Pigasten õlle perran käino nink waat om sinna jääno nõudke temma käest perra. temma om ka siin. -
Kutsoti ette Wanna Mäe Musti kortsimees Märt Mollok ja sai temmalt kohto poolt küssitus kas H. Undritz om sinno kätte üts õllewat andnao, sis üttel Märt Mollok om küll õige too waat saisap kohto all küll minna too eest hoolt kanna et peap kätte saama. -
Otsus   Selle et Märt Mollok mitte es salga et om H. Undritz käest Jürri Paales õlle waat saano. Moistis kohhos et Märt Mollok peap 8 päiwa sissen 8 rubla Jürri Paaleselle ärra masma, ja kui õlle waat temma kätte saap sis peap Jürri Paales waat taggasi wõtma ja 8 rubla wärsti taggasi masma.- 
Heinrich Undritz sai toost asjast obis pris mõistetus. -
Sai kohto Otsus nink § 773-774 kohton käiadelle ette loetus ja olliwa mõllemba tollega rahho. -
                                                                          (Allakirri nago üllewälgi)
