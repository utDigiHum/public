Jakob Karrindus kaibas kohto een omma tütre Wijo perräst, et temmä tüttär om Jaan Häidsoni manno kaswandus länno 1869 aastal ja om sääl olno 3 aastaiga, agga nüüd 1873 20 Aprillil om Josep Häidson teddä säält ärrä aiano, ja ei ollewa middägi palka andno Wijo om 14 aastaiga wanna olno kui temmä sennä om länno ja om keik se 3 aastaiga ilma palgata sääl olno, perremehhe rõiwas ja söök om olno Wijol.
Josep Häidsoni käest sai küssito üttel temmä et temmä om Wijot Wett aiano toma agga, mitte ärrä ei olle aiano, Wijo ollewad essi ärrä länno, Wijo üttel, et Josep om temmä tarrest wäljä wissano ja om läwwe wasto lönod.
Wijo nõuwap hendäle eggä ärrä tenito aasta eest 3 rubla palka kokko 9 Rubla
Wijo om 2 aastaiga Jaan Häidsoni ja 1 aastaig Josep Häidsoni teeno.
Jaan om Wijot küllä kolin kolitano Josep om päkolin kolitano.
Peter Petrowi ja temmä naise tunnistusse perräst jääb polele 21 Maini.
