Weel sel samal päiwal tulli Mõtsa vahd Jaan Uibo ette kaiwas Adam Wähk om 7 tolli jämme ütte kuiwa peddaja ärra raggono, ja nõwwap 10 kopik tolli pääld üllegige 70 kopikat. Adam es woi sedda sallata enge üttel et ta kogematta, mõttelden et oma maija mots om olno, mes ku sääl liggi oli om raggono.
Kohhos moist. Adam Wahk peap 50 kopik katsa päiwa sissen ärra masma Herrale.
Protokol wälja kirjotet s. 17. Januaril 1872.
