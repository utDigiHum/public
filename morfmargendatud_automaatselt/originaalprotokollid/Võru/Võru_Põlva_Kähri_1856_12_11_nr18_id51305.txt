18. Kaibati et se tüdruk Leno Troon, kes karrilatsi kõrtsin teenman, mitto asja ni häste söki, ku mu asja polest om wargusse wisil ommal sõssarel Marri Paidrasonil andnu, ni kui ka mittond asja sis selle wimatse pool om löitu. Ka ollew Marri Paidrasen omma sõsarel Lenol sedda nõwwo andnu, wõtta mes agga kätte saap ja temmale anda. Kaibatu, kes ette kutsuti, es salga enge tunnistawa ülles ni õige olewat.
Moistetu:
kumalgi 1 rub õb waeste kassa sisse  massa ja 15 löki witsu. Agga et Leno wäga pallus ja parrandada lubbas, sis sai temmal se ihho trahw mahhajättetus.
Päkohtomees Peter Seba
kohtomees Jürri Jõggewa
   "   Märt Porrila
woormünder Peter Nikopensius
     "   Johan Leppason
