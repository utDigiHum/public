Herra Kubli kaebas Jacob Kuus pääle et temale olla poodi wõlga Jacob Kuus käest 14 rubla 50 kop. saada; 7rub. 50 kop. olla juba 5-6 aasta eest ja 7 rubla olla praegusel aastal jäänud ja palub nüüd kogu konna kohut temale seda summa sundi wälja maksta.
Sai Jacob Kuus selles kaebtuses küsitud ütles õige olewad ja palles Kubli herrat endaga kannatada kuni tulewa aasta 25 Märtsini siis maksab 7 rubla 50 kop. ära, poole aga 25 oktobr. t.a.
Herra Kubli oli sellega rahul ja palles seda kogukonna kohtul kinnitada ja Jacob Kuusel oma sõnna pidada.
1. Eesistuja Johann Waks
Kohtumees Jaan Salomon
do: Johann Wasser
do:  Peter Kolk
kirjutaja M. Bergmann [allkiri] 
