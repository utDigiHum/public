Üks tuhat kaheksasada üheksakümne üheksamal aastal Decembri kuu wiieteistkümnedamal päewal on Krootuse WallaKohtu poolt surnud Kioma Walla talupoja Jaan Ado p.Joru päranduse õigustes kinnitud ainsate pärijate wahel Jaan Jorust järgi jäänud Selli N 6 talu ja liikuwa waranduse ärajautamise leping ustawaks tunnistud, missuguse lepingu järele Jaan Jaani p. Joru ja Leena Sikul sündinud Joro nimetud talu ja liikuwa waranduse omandawad.
Kohtueesistuja S. Raig [allkiri]
Kirjut CKuddo [allkiri]
