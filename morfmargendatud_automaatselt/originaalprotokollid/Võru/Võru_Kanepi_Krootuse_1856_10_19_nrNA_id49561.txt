Härra kaibas nellas woor Koggokonna Kohtole kui Lismendi Jaan Walge ei tulle orjusse pääl wälja, Kutsminne Sunminne ja hirmo anminne ei awwita mitte middagi, Kohto moistmisse poold peäs temma Mihkli päiwast Jürri paiwani jargi moda waljän ollema, täämbatse om temma 128 1/6 Weitse paiwa wolgo.
Om moistetu Jaan Walge teo orjusse wasto pannemisse eest  Kui ei olle mittu naddalit walja tunu 30 loki ja wastne naddal peap egga päiw wäljan ollema.
Pa Kohtomees Peter Urberg xxx
         d.:  d::  Juhhan Lukkatz
d.: Jaan Mandel xxx
Kirjotaja J. Raig (allkiri)
