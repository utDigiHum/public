№ 214   Juhan Lätlane Wõru wallast ette tulnu ning wastud, mina ei ole mitte Peter Orrasoni palki wiinu, aga Juhan Puk möije minole ühe sae palgi mida ma ka Paidrale, muud ma ei tija.
Peter Orrason nõwab omi laude Paidralt kätte kelle eest ma ka lõikamise raha ära massin, ning omi paewa palka ja taggan otsimise kulu.
Mõistetud:
Et Peter Orrason Paidralt oma palgi kätte sai mes temal warastati ning moldre kirjalikult tunnist et Juhan Lätlane see palgi wija om olnu, kea isi ka pera anab et tema Juha Pukk käest ostnu kea üks teenja inimine oli ning palki müija es ole.
Otsus:
Wõib Peter Orrason sest palgist saadu lawa Paidralt ära tuwa ning Juhan Lätlane peab 1 rbl. 50 kop Orasonile paewa palka ja kantselei hääks 1 rbl. 50 kop ning waeste kassa hääks 2 rbl. 2 nädali sees wälja masma ning wõib müüja Juhan Pukk käest oma kahjo nõuda.
See otsus §§ 773 ja 774 järele kuulutetu. 
 
