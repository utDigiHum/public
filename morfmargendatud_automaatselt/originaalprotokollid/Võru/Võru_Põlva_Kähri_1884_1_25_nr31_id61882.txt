Seriste Herra von Kiel tuli ette ja andis üles et temale on Peter Sari käest 14 rub.80 kop raha saada ja 200 sülda puid olla Peter Sari mis kontrakti järele pidid ragutud saama, ragumata jaanud, kus iga sülla eest 40 kop. tulla Peter Sarjale maksa.
Peter Sari tunistab seda õigeks ja lubab see wõlgu olew raha 14 rub 80 kop. äramaksa ja 100 sülla puie eest lubab 40 rubla maksa aga teine sada sülda lubab ise äraraguda. Peter Sari maksis 14 rub. 80 wõlga praegu herra von Kielile wälja. 40 rub lubab kuni 1se Martsini äramaksa ja ka 100 sülda puid seni äraraguda.
Herra von Kiel oli sellega rahul ja palub et Peter Sari kraam saaks kuni selle lubamise termini üleskirjutud.
Otsus. Peter Sari Kraam üleskirjutada niipalju mis 80 rubla wälja teeb ja kinni panna kuni tema Herra v. Kieli wõla on äramaksnud.
Peter Sari lubab omast kraamist üles kirjutada:
1 hobune = 50 rub wäärt
1 lehm      = 30  "     "
