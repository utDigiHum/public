Ette tulli Möldre Hans Sassiand ja pand ette Punagu Kolga peremeeste wasta, ette temä sedä üles andmist ei taha wasta wõtta, selle eest et weski temä om ja jääb ka temäle.
Saije Punagu Kolga peremehe ette kutsutus ja see wasta pandmine ette kuulutetus; ütlewa et tekko möldre meile sedä wälja mes om lubatu. Ei jahwata jahwatus, ei aja ka linno ärä.
Ja nõwawa et jahwatagu jahwatus, ajagu linna ja tekko kõik mes om leppit ehk masku rent mes om nõwetu.
Andres Nikopensius kaibab ütsinda oma kahjo, et temäl nõwab oma krundi kätte, ilma et weskid sääl om krundi pääl peäb.
Andres Nikopensius ja Sassiand lepisiwa ärä ja lubas kõik Andrese tegemise ärä tettä.
Otsus:
Kogokonna Kohus mõistab, neide lepingot kindmas, nii et nee Punagu Kolga peremehe ei woi Hans Sassiand kohegi ajada, ei ka temäle ülles üttelda, ei ka temä weski tami laotada.
Jättiwa aastas proomi pääle.
Pääkohtumees eest Jaan Lepäson
Kohtumees Peter Nemwalz
  "  Jaan Leib
