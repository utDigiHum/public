Täämba olli kog. kohus kadunu Peeter Mõtuse perijate wöörmündri Hindrik Tagel ja Jaan Tagel Põlgastest siia kohto ette kutsutu, et nemä siinsam aru pidiwa andma, kas see kadunu P. Mõtusest perrä jäänu warandus, mis siist kohto poolt üles ja Hurmi kog. wöörmündri raamatohe kirjotedu, weel alale om ja kuidas läsk ja waese latse hennast ülespidawa.
Wöörmünder Jaan Tagel olii kohto ette tulnu ja ütel, et kraam kõik alale om ja perrä jäänu läsk ja waese latse õige häste eläse.
Otsus: Kohus om selle perra kaemisega rahul.
