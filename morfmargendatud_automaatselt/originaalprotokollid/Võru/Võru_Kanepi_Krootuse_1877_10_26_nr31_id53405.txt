Hurmi mõisa perisherra von Rothi kaibuse asjan Hurmi mõisamaarentnike wasto sub №30, wastutawa maarentniko: 
Kusta Org wastutap, et tema hobene ollew kül üts öö uibo aijast wälja lännu, aga ei teadwat mitte kas ta ristikhaina pääl om olnu, ehk mitte.
Johann Teri wastutap, et tema om kül ütskõrd oma hobese ristikhaina pääle pandnu ja ollew ka seda nännü, et Joh. Seebach hobene om ristikhaina pääl olnu. Johan Org tunnistap sedasama wiisi.
Johann Seebach wastutap, et tema omma hobest ei ole ristikhaina pääle pandnu, ei ollew ka muide hobesid sääl nännü.
Et Hurmi mõisa maarentniko ommil tseol niido ärä om lasknu tsongi, sis nõuap Aadam Jahu mõisawalitsuse nimel maarentnike käest niido tsongmise eest 3 Rbl. ja et maarentniko peawa enne lume maha tulekit niido ärä tasoma ja ega hobese eest, mis ristikhainan olnu 3 Rbl. hõb.
Otsus:
Egaüts, kelle hobene ristihainan om olnu, see om: Kusta Org, Johan Teri ja Johan Seebach, peawa 1 Rbl. 50 kop. masma ja et kõigil maarentnikel tsea omma niito tsongu, sis peawa ülekõige see 3 Rbl. ärä masma ja niido ilosaste tasatses tegema.
G. Weits XXX
A.Org XXX
G. Kolk XXX
