Ette tulli Johann Seeba ja kaibas Jaan Lepwals pääle kes temale maarenti ära ei massa 6 rubla temal olnu ka enne küll hainamaad mes lepwals ära andnu ja lubanu ka põllu maad ära anda.
Sai Jaan Lepwals sen kaibusen kusitu ütles kui paljo tema mino käest nõwwab mina  temale raha küll ei ole andnu enge minewal aastal tõi tema mino kaest ree ka wõtse tema mino käest hainamaa ära päälegi oli minol hainamaa eest 6 rubla ja pollu maa eest 4 rubla minul om tunistaja Jaan Seeba meil om 6 aasta leping.
Sai Jaan Seeba kusitu ütles et mina häste ei mäleta, muud kui et 2 rubla lepisiwa pollu maas dessatini.
Johann Seeba ütles Mina tõi kull ütte ree tema käest, ent see olli 1881 aasta renti ette ega sina minule ka raha kunagi minule es massa.
Mõistus Kogokonna kohus mõistab
Jaan Lepwalsil Johann Seebale weel pääle ree 2 rubla perra massa 8 päiwa seen seda 1882 aasta renti pera ära massa, et tunistust ei ole.
Otsus kuulutedi § 773, 774 perra ette.
