Kähri Mõisa Walitsus tulli ette ja andis üles et rentnik Jaan Mikson on 22 rub. 9 kop. renti wõlgo ja palub seda wäljanõuda:
Jaan Mikson tunistab seda õigeks ja palub mõisa walitsust temaga kannatada sest temale ei olla praegu mite wõimalik maksa.
Mõistus.   Kogukonna kohus mõistab, et Jaan Mikson peab 22rub. 9 kop. 2 nädali sees mõisawalitsusele wõlgnewad renti wäljamaksa.
Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtus käijatele etteloetud. Olid otsusega rahul. Mikson lubab 18 Aprillil s.a. 22 rub. 9 kop Mõisawalitsusele äramaksa.
 
