Marja Karri kaiwas Lambameistre Franki pääle, et temma lamba püggamisse man om ollnu, nink om jogi hädda kannatanu, ja om joma lännu, selle pääle ollew Frank sedda tütrikku 3 kõrda lönu, 2 kõrda Russikuga kukruhhe ja 1 kõrd kõrwa pääle.
Frank tunnist essi et temma sedda Marjat om üts kõrd (lönu) ehk tõukanu.
Otsus: Nie kui se selle Marja Karri silma man weel selgete nätta olli, et temma lödu olli, ja Frank essi tunnist et temma Marjat ni kurjaste olli tõukanu sai moistetus et Frank 4 R. hõb massap, 2 R. Marjale wallu-rahha ja 2 R. walla-ladi.
Näddali perrast ärra massa. 
Marja Karri sai 2 R. sel 9 Septbr. kätte. 
