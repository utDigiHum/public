sel samal päiwal.
Kaibas Hindrik Leppisk, temma nöuwap Peep Lepmanni käest selle No 44 Obrami pole tallo eest, wäljaminneki tassomist 64 Rb.
wäljanöudminne:
Peep Lepmann tulli ette ja wastutas temma ollew selle pole tallo krundi eest 34 Rbl. 50 C. ostmise raha rohkemb sissemaksnu, ning Hindrik Leppisk ollew üks laut kattusega terwini hendale perrinu, nink nisama ollew Hindrik Leppisk keik ne tarwiliko maja puid hendale wötnu.
Otsus:
et, Hindrik Leppisk sedda lauta hendale perrinu, on kahjotassomissest 10 Rb. ja nende puije eest 3 Rb. maha arwatu, ni et siis Peep Lepmannil 16 Rb. 50 Kop. Hindrik Leppiskile perra massa tullep.
Pääkohtomees Gottlieb Anderson
Kohtomees Paap Sabbal
Kohtomees Josep Parmak
Kirjotaja J. Maddisson &lt;allkiri&gt;
