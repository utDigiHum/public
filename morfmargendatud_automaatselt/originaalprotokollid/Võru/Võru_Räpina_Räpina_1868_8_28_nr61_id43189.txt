Jakob Rautberg Wöbsost tulli kohto ette ja kaibas et temmä 1867 aastal Buile asjo tallitaja Kruse leppingo perrä om Buile parwe tenno Wöbsö jöe päle mida kui se leppingo kirri tunnistan ja et sest rahhast weel 10 Rubla höbbe samata om mis mitte tö teggemisse eest ärrä ei masseta.
Buile Herrä käest sai küssitu üttel temmä et temmä om 12 Rubla seddä parwe parrantamisse rahha Kruse kätte ärrä andno sel körral ja et Kruse ei olle seddä rahha mitte Jakobi kätte ärrä maksnod.
Kruse käest sai küssitu üttel temmä et Buile ei olle mitte temmä kätte rahha andno ja se rahha mis Buile temmä kätte om andno om temmä maja tarwidusse ostmisse ja ka parwe matterjali päle ärrä maksnod nida kui se rehnungi täht temmal tunnistab.
Üts ütleb temmä om andno teine ütleb temmä ei olle andno ja möllemba tunnistiwa et tö teggiä ilma palgata o,
Jakob om 5 körd Buile man küssimän käino agga Buile om iks üttelno oda ni kauwa kui meije ärrä selletäme
Kohhus möistis et Buile nink Kruse kumbagi 5 Rubla peawad maksma ni et se tö teggemisse palk käen om ja 2 Septembril peab masseto ollema
Pä kohto mees Paap Kiudorw
Kohto mees Jaan Holsting
Kohto mees Paap Kress
Kohto mees Hindrik Toding
Kohto mees Kristian Wössoberg
