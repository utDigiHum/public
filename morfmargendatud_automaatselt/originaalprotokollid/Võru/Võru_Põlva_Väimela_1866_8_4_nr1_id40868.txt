N 1. 
Koggokonna kohhus ja wöölmöldre Anti Muld omma sijn Johann Jaaskaga lepnu, et Johann Jaaska wõttap Peter poig Jürri Muld henda kätte kaswatada, selle leppingu päle: et Jürri Muld saap Johann Jaaska hole kandmisse all kaswatatus, kigen tarwilikkun üles piddamissen ja koli asjon, ja selle eest peap Jürri Muld, kui temma jobba ealisski saap, et jöwwap jõudu möda tööd tetta, Johann Jaaskat ni kawwa orjama, kui temma kattesjatäiskümme aastat wannad saap, ilma poegata, päle selle, kui Jürri Muld nimmetedu ealisses om sanu, sis om temma Johann Jaaska wõimu alt wallal. Jürri Muld om see leppingu ajal seitse aastat wanna.
Waimel 4 August 1866.
Pä Kohtumees Hindrik Trumm xxx
Abbi Kohtumees Peter Pihl xxx
Abbi Kohtumees Peter Musto xxx
Wöölmöldre Peter Pahl xxx
Lats wöölmöldre Anti Muld xxx
Johann Jaaska xxx
