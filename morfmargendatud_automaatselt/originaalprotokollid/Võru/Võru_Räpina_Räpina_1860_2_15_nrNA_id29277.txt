Sila Mik kaibab temma om Kasse Josepe keest Werro lade hobbene 23 Rubla eest kaubelnu ja 10 Rubla kaerahha andnu tunnistaja ollu  Kero Paap poeg Widu
Kasse Josep wastutap küssimisse peel temma ei olle Sila Mikkul hobbest muinu ei olle ka rahha masnu sell om üts wooras inniminne Sepp Winde , Adam Parmask man, ka olnu tu tunnistaja mes Sila Mik om ülles andma om Sila Mikku suggulenne, ja siis paele lade, om Sila Mik Kirjuta Jaaniga temma aidan tulnu ning 19 Rub hobb. saina seest erra wutnu,
Kero Widu tunnistap rahha ta ei olle nennu, enna Sila Mik om rahha Kask Josepille andnu ja üttelnu 10 Rubla om rahha Kasse Josep om gohbunnu olnu.
Sila Mik wastutap temma om hobbese perra lennu ja perra rahha 13 Rub paknu, enne neis 19 Rublas ta mitte ei tija.
Silu Mik annep wihl üts tunnistaja Kandsa Karl poig
Punni Rusa Ado peap 10 erra rautu keivo eest 40 kop höb. masma.
Lamba Jaan selle eest et ta kole puu 2 jalga wanna körtsi man ei olle oma wihnu sap 15 löge witsu
