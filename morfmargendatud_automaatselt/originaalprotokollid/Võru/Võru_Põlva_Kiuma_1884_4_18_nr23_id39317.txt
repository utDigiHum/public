Sai Hindrik Nageli pärandajalt Juhan Nagelist siia proklaama aeal kadund Nagla järelnimetatud wõlglased üles-antud, kes siia juba tõistkorda oliwad kutsutud, aga ikka tulemata jäänud, nagu Wastse-Kuustest: Antsla Kiisla, wõlgu 2 1/2 rubl. Daniel Kruuse 5 rubl., Märt Kits 4 rubl., J. Grosberg 3 rubl., Joh. Mark 2 rbl., Jaan Lok 3 rubl., ja Otto Manglus 5 rubl.
Otsustatud:
Neid weel kord 16. Mais.c. ettekäsütada ja kui siis ei tule, saab seaduse järele otsus äratehtud.
Pääkohtunik: J. Nilbe [allkiri]
Kohtunik: H. Lukats [allkiri]
d.: J. Aripmann [allkiri]
Kirjutas. Zuping [allkiri]
