Gusta Asi ja Mihkli Asi tulliwa kohto ette ja kaibsiwa, et nemä ei saawat oma kodapoolitsega läbi, selle et temä  neid sõimawat ja neide latsi tõukawat ja lööwat. Pallese kogokonna kohut neid sellepoolest awitada.
Kodapooline Mari Thaler kutsuti kohtu ette ja kui kaibdus ette loeti wastutas tema:" Mina ei ole latsi löönu, ei sõimanu, ei ka tõuganu.
Otsus: Kui tõine kõrd weel niisugusne asi kohto ette tulep, sis saap süüdlane kangeste trahwitus.
A. Org XXX
G. Kolk XXX
G. Asi XXX
Kirjotaja asem. G Daniel [allkiri]
