Ette tulli Johan Udras ja kaibas, et Lena Kasak, Katri Udras Hanni tütar ja Leno Kelt tema haina kuhja ära palotanu mes 2 koorma suurune olnu, tema nõudwat kahjo tasomist 20 Rubla. Wöörmünder Peter Pedras kaenu kahjo üle.
Wastut kaibuse pääle Leeno Kasak mansaisja esak Jaan Haagiwang, et tema ei teedwat kes kuhja palama pandnuw ei ollew ka esi seda kurja tennu, enge Leno Kelt hõiganu et kuhi palap, ja üttelnu, et tema wõtnu kannukesega tule ja lännu ümbre haina kuhja nink kuhi lännu palama.
Katri Udras mansaisja esa Hann Udras wastut kaibuse pääle, et tema Johan Udrase kuhja ei ollew palotanu, enge Leeno Kelt ollew hõiganu, et haina kuhi palap ja üttelnu, et tema tulitunglega ümbre kuhja lännu ja kuhi lännu palama.
Wastut kaibuse pääle Leno Kelt mansaisja kasak Peter Kõiw, et tema lännu haina kuhja päält elajid ära ajama ja olnu tule tungel käen ja sellest lännu kuhi palama.
Otsus:
Johan Udras kaibust tühjas arwata selle perast, et kuna Leeno Kelt nõrgameeleline om ja Wallawalitsuse poolt tema poole korteri olli pantu, es ole teda mitte waija karja saata.
See kohtu otsus sai kohtun käüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus. Johan Udras nõwwap Appellationi.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
Abiline: J. Mikheim [allkiri]
