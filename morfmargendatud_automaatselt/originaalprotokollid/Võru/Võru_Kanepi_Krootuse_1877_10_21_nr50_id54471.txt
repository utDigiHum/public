Neljandalt  Tulli ette Ehamarro kõrtsi tütrik Ann Loodos temma wöölmünder Tannil Kärner ja kaibas et em(m)and C. Dulder ollewa tedda kotton paigan aida wärrete man eddimält kõrwo mööda nink tõist kõrda jälle kortsin ni samma kõrwo mööda lööno, nink nõudis omma kõrwo mooda pesmisse eest 5 rubla, kaibus jaije toise kohto paiwa pääle selletada selle et C Dulder es olle kohto tulno. -
                                   Karraskij Kohtomajan sel 21 oktobril 1877
                                                              Pääkohtomees J. Tullos  XXX
                                                              Kohtomees  J. Uibo  XXX
                                                          teine      do      J. Leppason  XXX
