Kaibas Kährist (Heimadrast) Peeter Kärbe, et siit wallast talunik Juhan Kiristaja olla 1873. aastal tema käest 106 rubl. raha selle tingimise all taluostmiseks saanud, et kui Kiristaja tälle mitte ära ei maksa, siis see talu tema kätte langeb. Pääle selle olla ta weel Kiristajat heintega aidanud: annud tälle igakord, kui senna läinud, sugu heinu ja talitanud ja käinud  selle talu ostmise asjus mitmed päewad ümber. Kiristaja olla 4. aastat tagasi selle raha küll peaaegu tälle kõik äramaksnud, aga oma heinte ja aeawiitmiste eest kui ka raha eest protsentisi ei olla ta sugugi saanud. Sellepärast nõuda tema nüüd Kiristaja käest, kas andku see talu ära temale ehk makskiu kuni 200ja rublani tasumiseks kõige saadud hea eest. Kõik see asi olla nende eneste wahel suusõnalikult lepitud olnud ja mitte kusagil awalikus kohtas.
Johan Kiristaja pani sellewastu, et seesuguseid tingimisi ei olla nende wahel talu pärast wõimalik olnud. Laenatud raha olla ta P. Kärbele 4.aasta eest juba ära-maksnud, mis seesama ka järele andis. Raha protsentide pärast ei olla midagi lepitud olnud ja ka heinad olla tasa.
Peeter Kärbel ei olnud rohkem midagi ettepanna kui ennegi.
Mõistmine:
Peeter Kärbe oma põhjendamata nõudmistega tagasi-lükata.
See mõistmine kuulutadi nende kohtukäiatele seaduse järele ette. Peeter Kärbe ei olnud rahule ja andis üles, et suaremat kohut tahab käia. Oppusleht sissama antud.
Ärakiri protokollist 23. s.k.pw. ülewaataja kohtule wäljawalmistatud.
Pääkohtunik J. Nilbe [allkiri]
Kohtunik: H. Lukats [allkiri]
d.: J.Aripmann [allkiri]
Kirjutas Zuping [allkiri]
