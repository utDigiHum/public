No 105, 27 Mail
3 Junil päle ette luggemisse Lisa Kliman üttel, et se om wõls temmä ei olle mitte märästki 28 Rubla rahha andno, et 6 Rubla temmä andis ja se om ka ärrä masseto wõlga ei olle middägi. Ja se rahha 6 Rubla ollewa temmäl seddä wisi ärrä sano masseto 2 hammet om andno Lisa		1 paar pükse		1 paar kindid		1 kätte ratt		
Lisa üttel, et Hindrik om ösi maggamisse eest temmäle seddä rahha andno.
Hindrik üttel, et temmä om			
1 hamme sanod					1 paar pükse		2 pari kindit		1 kätte rätt		
Rein Griggul tunnistas kulu kui Lisa om üttelno et kas sinna 28 Rubla eest minnu wannas tüdrukus tahhad jättä ja Lisa ollewa teddä ka käemehhes tahtno kui temmä Jaak Ainsoni käest rahha olles lainada sanud Hindrikul massa.
Soldat Rein Raudnask tunnistas et Hindrik Liiskman om temmäle üttelno kui temmä ütten Wõsso pole om länno et temmäl om 1 Rubla Lisa käen, ja teist körd om üttelno et temmäl om 5 Rubla, ja kolmat körd om üttelno et temmäl om 28 Rubla Lisa käen ja keik neo 3 körd ütlemised om olno päle selle kui jo Lisä ülles om kulutedo kirrikon et temmä Jakob Klimanil mehhele lähhäb.
Kohhus mõistis, kaibust tühjäs, selle perräst et õiget põhja ei olle.
§773 om ette kulutedo
Lisa eest kostja om mees Jakob Klimann
Hindrik Liiskman pallel prottokoli.
Hindrik Liiskmanil sai Appellatsion wäljä anto
