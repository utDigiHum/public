Ette tulli Loosi Mõisa mõtsawalitsus Armin Birkenberg ja tõst kaibust Tanno Jakobi Horni pääle, et wiimane on Mõisa mõtsast 1 peddai 10 tolli jämme 60 jalga pikk (palk) ärra warastanu, kedda wanna mõtsawaht Jaan Omri kinni on wõtnu ja Woolmundre J. Rautik ülle kaenu on palle sedda 4 rubl. 11 kop. wälja mõista.
Jakob Horn oli kutsmise pääle ette tulnu ja ütel, et minna ei ole mõisa mõtsast mitte middagi weenu,- ja ei tija sellest mittagi.
Mõtsawaht Jaan Omri andse tunnistust et minna wõtsen wöölmundre J. Rautike ossa ja latse mõtsa kon see puu ossa kokko sündsiwa.
Wöölmündre Rautik andse tunnistust et sääl es ole tüwwe es ka latwa, aga 1 oss sündse kull sellega kokko.
Selle pääle es ütle kohton käija ennamb mittagi ütlemist olewad, sis
Mõisteti
et üttegi täwwelist tähte mõtsa waht anda es wõi ja üttegi tunnistust es ole et Jakob Horn se peddaja weenu on siis se mõtsa walitsuse kaibus puuduwa tunnistaja perrast tühjas tettu.
Se otsus saije ette loetus ja ärra selletedus.
Päkohtomees: Johhan Konz [allkiri]
kohtomees: Jaan Jarw [allkiri]
     "                Peter Juk [allkiri]
Kirjot.: Piirisild
