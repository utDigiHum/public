Ette tulli Jaan Harrak Räppolt ja kaibas, et Ado Pai tema tütre Liso ja Mari karjuse palka 32 Rbl. ära ei maswat, tema nõudwat seda raha kätte saada.
Wastut kaibuse pääle Ado Pai et Jaan Harraka tütre Marile olnu aasta eest 5 Rbl. ja tõisele tütrele suwe päält 10 Rbl. 5 Rbl. Mari palka ollew tema ära masnu, ent Liso ei olew aastat ära teennu ja ollew ka paljo kahjo karjal lasknu tettus, selle perast ei wõiwat tema palka wälja massa.
Jaan Harrak tõent, et tütre Marile olnu 20 Rbl. palka lepitu ja tõisele tütrele Lisole 12 Rbl. ja ei olew weel täämbatse päiwani mitte ütte kopikatki  Ado Paio kaest saanu, kes tema tütre Liso ära ajanu.
Ado Pai tõent, et wiis Rbl. ollew tema Jaan Harraku wõla pääle ennitse pääkohtu mehe Michel Jõgewa kätte masnu.
Otsus:
Michel Jõgewa ette kutsu.
otsus om ette loetu.
Pääkohtumees J. Kabist [allkiri]
Kohtumees P. Sillaots [allkiri]
      "             P. Klais [allkiri]
