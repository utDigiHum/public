Ette tuli Jaan Kõiw ja kaibas, et tema olewat Jakob Aplein kaupmees Augskaln kaupa Riast tooma lepnu. Egaüts woorimees pidi kolm kaalu linu Riiga wiima ja jälle kolm kaalu kaupa tagasi tooma. Kõik tõise oma seda leppingut täutnu, aga Jakon Aplein ei ole mitte sugugi kaupa Rian Augskalni käest pääle wõtnu, ega tagasi toonu. Selleperast om Augskaln sellest woorirahast 750 kop. kinni pidanu.
Kõiw palleb Jakob Aplein käest Augskalni kahjo wälja nõuda, et tema raha oma 750 kop. Augskalni käest kätte saase.
Jakob Aplein ütleb henda kül Riiga woori pääle minna J. Kõiwga leppnu, 30 kop. puuda wiimise eest, olli ka kül tagasitoomisest kõneldu saanu, et 25 kop. puuda päält saawat, aga sellest es ole meil mitte juttu et peat tgasi tooma.
Kui ma liinan oli, sis es näe ma ei kaupmeest ega ka poderetsikut saal. Mina oles kül weel 3 kaalu Augskalni kaupa wõinu pääle panna, ennegi õdango, kui ma juba Silsoni kaupa oli pääle pandnu, sis weel Augskaln küsse: Kas sa mino kaupa pääle paned wõi ei pane? Ma kosti: Kus ma enamb pane, mul om jo esige kaup pääl.
Se Augskalni kraami päälepakkumine oli teise päiwa õdangu päiwa minegi aig, nink minul es olegi rohkemb pääl, kui 3 kaalu.
Kaupmees Augskaln ütleb henda Jakob Apleinile kaupa pääle pakkunu olewat, aga Aplein olewat kostnu: Mul om esige kaup pääl. Üts jago kaupa sai kül äratoodu, üts rbl kaalu päält rohkemb massetu, aga osast kaupa jäi weel Riiga /üts kaal/.
Aplein ütleb, et kui kaupa weel oles olnu, sis oles se wõinu äratuwwa, nimelt Jakob Kõiw ja Jaan Kopli oles häämeelega weel rohkemb pääle pandnu, aga es ole enamb kaupa.
Augskaln ütleb 15 rbl. eest kahjo saanu olewat.
Mõistus:
Selleperast et Augskaln woorimeestele kaubatoomise eest puuda päält 10 kop. wäljamasnu enamb kui tõistele oli leppitu, peab Aplein Jaan Kõiwle Augskalnile wäljamasmises 2 rbl. kahjotasumist  14. päiwa seen wäljamasma.
Pääkohtomees: J. Kroon [allkiri]
Kohtomees: J. Pruwli [allkiri]
          "           J. Meister [allkiri]
Kirjutaja: J. Treu
