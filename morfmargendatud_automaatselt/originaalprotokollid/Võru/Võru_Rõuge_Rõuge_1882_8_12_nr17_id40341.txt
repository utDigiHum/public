Kui Peeter Siskale ja Johan Kadak'le protokoll No 16 mõistus ette sai loetu, sis ütel Siska: Ega ma selle mõistusega rahul ei ole. Kadak arwab jootmise läbi ja palgatuide tunnistuste läbi õigust saawat.
Mõistus: Et se kogukonna kohus mänestki jootmist Kadaku poolt ei tunne nink palgatud tunnistajist midagi ei tija - peab Siska sääntse üteluse eest kohtu peegli een 24 tunnis türmi minema.
Pääkohtumees: J. Kroon [allkiri]
Kohtumees: J. Pruwli [allkiri]
         "            J. Meister [allkiri]
Täüdetu Aug. 82.
