Karraskij Kogokonna Kohhos Jusal sel 23 Septr 1877
                                               Mann olliwa Pääkohtomees Jaan Tullos
                                                    do     do    Kohtomees Jakop Uibo 
                                                    do      do  teine    do    Jaan Leppason
Sai ette kutsotus Perri wallawannemb Jaan Neümann nink sai Jaan Mark kaibus sest 9 septembrist s.a No 44 ette loetus, kes ütles henda Jaan Markiga se Wija Hindrik tallo Mäe maa ülle kaup tenno ollewat, nink nimmelt sedda wisi Jaan Mark saap se mäe maa suu-
sõnnaga leppingo perrä kuwwe kümne rubla eest aasta rent nink peap nida piddäma katte wakkamaa egga aasta röan katte wakkamaa kessan katte wakkamaa ristikhainan nink katte wakkamaa tõwwo all ent kui kauge aja pääle seddä ei olle minnol mitte meelen. 
Pääkohtomees Jaan Tullos olli jo ka mann. -
                                   Karraskij Kohtomajan sel 23 septembril 1877
                                                      Pääkohtomees Jaan Tullos  XXX
                                                        Kohtomees   Jakop Uibo  XXX
                                                          teine   do     Jaan Leppason  XXX
