Writs Simmulman tulli kohto ette ja kaibas et temmäl om Järwe werest perwe päält 1 terwe kuhhi häddälä haino ärrä warrastedo se kuhhi om 1 korma olno. Kuldmisse perrä om Writs länno Pallo walda Hindrik Punmanni pole haino otsma ja om säält promi perrä kätte sanod weidikesse haina kus mann Pallo walla kohto mehhed Wido Iwwask, Widrik Wija, om olno.
Hindrik Punsoni käest sai küssito üttel temmä et temmä Säsepinjä küllä mant om ostno katte wennelesse käest kus walmis säeto korma om olno, agga ei olle mitte warrastano neid haino. Ja häddälä haino temmäl ei olle olno, ja neo wennemäält todo haino om issi olno teine korma teist karwa mis temmä Säsepinjä küllä mant tee päält om ostno.
Wido Iwwask Pallo walla kohtomees üttel et temmä om päkohtomehhe Jakob Niklaosega kaeno et 1 korma om häddala haino olno ja teine korma om suwwitsid tetto haino olno ja sel körral om siis Hindrik Punnson üttelno Mehhikorma mant ostno ollewad. Ja kohto mees Iwwask üttel ka et Hindeik Punson om issi omma su sannaga sel körral üttelno et temmäl 1 korma häddälä haino om.
Widrik Wija üttel et meije es tea nõuda muud kui neo hainad mis meije loo päält mahha lassime wissata neo sündsiwad Meeksi mehe hainoga kokko.
Hindrik Punsoni käest sai küssitu et mis perräst sinna siin kohto een wölsid sinna ütlid et sul häddälä haino suggugi ei olle olnod ja et sinna Säsepinjä küllä mant olled ostno ja siis sel körral olled sinna kohto mehhel üttel issi omma su sannaga et sul 1 korma häddälä haino om olno ja et sinna neo hainad Mehhikorma mant olled ostno üttel Hindrik et kohto mees es kule waggomaad kaugel mis minna kõnnelin. Hindrik Punson üttel 10 Rubla neist kattest kormast andno Neo wennelesse hobbesed om kelle käest temmä hainad om ostno 1 Werrew teine must olno.
Tunnistusse perräst jääs polele sullase Jakob Minntingi perräst
Pä kohtomees Paap Kiudorw
Kristian Wössoberg Kohtomees
Jaan Holsting Kohtomees
Jakob Türkson Kohtomees 
Kirjutaja J. Jaanson &lt;allkiri&gt;
Pallo walla mehhe haino proom mis Prists Solnask Räppinä kohto mees säält tõi sündis kokko ni kui ütte haina selle Räppinä mehhe Iwan Jakkimo hainoga kokko
