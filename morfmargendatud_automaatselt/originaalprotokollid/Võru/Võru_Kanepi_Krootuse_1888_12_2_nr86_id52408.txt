Tanil Tartu kraam üleskirjutatud, mõisa wõlgnewa rendi pärast, 1 werew ruun hobene, 1 nahk riista, 1 wankri, 2 tsikka, 2 lammast, 3 lehma, 1 wasik, 200 kubu linnu, 100 wakk kartohwlid, 20 wakka rükki, 6 koormat kaero arwata 12 wakka, 2 tsetwerti keswi, 1 regi, 1 hark ader, 1 kirst, 1 kapstitõrdu, 1 kapp, 1 laud, 4 sängi, 1 toori, 2 pangi ja 2 pada.
Adam Leis kraam:  4 lauda 1 peegli, 3 sängi, 1 lett, 1 klaas puhwetikapp, 1 rõiwa kapp, 1 mära hobene 5 aastad wana, 3 lehma, 1 wasik, 4 lammast, 3 tsika, 1 raud telle wankri, 2 raud äkle, 1 suur ader, 1 hark ader, 80 kubu linnu, 100 wakka kartohwlid, 7 wakka rükki, 20 wakka keswi, 5 koormat kaero, 1 kaal 5 Rbl wäärt, 6 korwi õlle pudelit, 2 pada, 2 toorit, 2 pangi, 1 kohwi massin, 1 wiina ankri, 1 keeduse kirst, 1 seina kell, 1 mõhk, 1 kolmjalg, ja 1 peele.
Otsus: Seda üleskirjutada.
Tanil Tartu +++
Peakohtunik: J. Narusk [allkiri]
