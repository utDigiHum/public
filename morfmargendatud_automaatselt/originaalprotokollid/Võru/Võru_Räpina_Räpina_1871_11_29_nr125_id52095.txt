29 Nowembril kui prottokoll ette olli loeto sai küssitu Friedenbergi käest üttel temmä et hamid om Grünfeldi hoida lasko temmä söwwä kui temmä tahhab se ommawa kontrahtin temmä hoita, hobbene olli üts körd wallale päsno ja olli nurme pääl ümber aeto siis om üttelto et karan om olno, agga wargas üttelno eo mälletä temmä, Agga keldrest om ärrä kaddono 4 Lamba kintso ja penikest karmi
Kohto man üttel Fridenberg et Grünfeld issi warras ei olle. Friedenberg üttel et Grünfeld temmä tunnistajad Jula osteto om 1 paar Saapa eest.
Juhhan Hindrikson tunnistas et hobbene om nissun terwe öd olno ja om nisso läbbi sönod, ja 2 körd om karast hobbest wäljä ajano ja om ka öd karan olno.
Ja om ka kulu kui Friedenberg om üttelno Grünfeldil sinna warras kassi ärrä.
Kontrahi perräst jääs polele.
