Ahja wallast Tarto Kreisist tulnuwa Kersä kõrtsimees Johann Maddison ja peremees Johann Kutsar ja aniwa tunistusega üles, et neil olewad mööda lännu suwwel augusti kuul warastetu, ja nüüd olla nemä kuulda saanu, et neide kraami siin Kähri wallan löüdu olla, ja nimelt soldat Johann Kelt käen. Nink et üts wanri rattet olewad mõtsast löütü.
Saije neide pallemise pääle soldat Johann Kelt elotus läbi otsitus, nink saije löütüs, 1 tarre akna, mes nüüd pooles om lõikatu, 1 ratta ehk wanri juhhitrettist (poodi kaplast) mustas wärmitu, adra rautpu, üts wikkat, ütte toos luwwa, pääle seda andnu Johann Kelt esi kätte, 1 paar künni raudu, mes õkwa rautpuule otsa sündinu ja atra kast, raud telgiga wankri edimätse telle mutre kõige juhhi rawwaga. Ja ütte puu telle juhhi rawwa. Nee juhhi rawa olnuwa neide rattaste külest, mes mõtsast löütüwad. Keda kõik nee peremehe omas tunistanuwa.
Nink ütte puutelgiga wedämise wankri, kedä ene sedä otsimist, mõtsast oliwa löüdnüwa Karl Kahre, Peter Kahre nink Peter Kahre Jakobi poig, Johann Kahre ja kats poisikest jahhiga manu saanuwa, Neid wankrid tunistiwa neesama peremehe ilma nägemäta omas, tähti perrä.
Saije soldat Johann Kelt ette kutsutus, ja nõwetus, kost temä ehk kellega temä sedä kraami om warastanu, ütlep et temä olewad tuud kraami tee päält ütte mehe käest ostnu, tu mees üttelnu hendä Walgesuust olewad.
Otsus:Kogokonna kohus mõistab, et nee warastetu asja hinna perrä enämb kui 10 rubla wäärt oma Tallorahwa Sääduse § 1070 und 1071 perrä, Johann Kelt Keis. Werro Makohtu mõistmise alla. Nink niidade et Johann Kelt ka selget tunistust ei anna, tedä kinni panda.
Pääkohtomees Jaan Luig [allkiri]
Kohtumees Johhan Punnak [allkiri]
Wallawanemb J. Leppäson [allkiri]
 
