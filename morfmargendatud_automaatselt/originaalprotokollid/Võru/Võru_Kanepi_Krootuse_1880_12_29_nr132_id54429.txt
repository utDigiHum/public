Mõisawalitsuse kaibdusen Jüri Ketz wasto, nõudmise perast /№ 116,123,127 ja 131/ olliwa täämba mõlema kohtu käüja kohto ette tulnu ja omme kontrakti üten toonu, niisama näidas ka Tille kõrtsimees Jüri Ketz oma kwiitungi ette.
Otsus:
Jüri Ketz /Tille kõrtsimees/ peap 1ses Januaris see puuduw 100 Rbl. renti mõisawalitsusele ärä masma, selleperast et 100 Rbl. kwitung mitte rendikwitung ei ole, enge kautioni Kwitung ja kui temä 1ses Januaris ärä ei massa, sis kaotap temä oma kontrakti õiguse ja peap Jüripäiwal 1881 kõrtsist wälja minema. 1ses Januaris 1881 ei wõi see kogokonna kohus Tille kõrtsi õigust mitte Jüri Ketz käest ärä  mõista, selleperast et kontraktin sedä ette kirjotedu ei ole.
Kui kogokonna kohto otsus ja Sääduse §§ 773 ja 774 kohtun käüjile ette saiwa loetu, sis es ole mõisa walitsus selle otsusega rahul ja pallel opusleht wälja, mis temäle ka lubati.
Pääkohtom. Joh. Pähn. [allkiri]
Kohtom. Mihkel Laine [allkiri]
Abi kohtom. Jaan Narrusk [allkiri]
 
