Oli mõisapolitsei ülesandmise ja nõudmise pääle selle walla pääkohtunik ja kirjutaja kogukonna kohtu nimel ja selle poolest 31.Mail mõisas käinud, selle hiljuti ärasurnud kärneri Jaan Sulby warandust seaduselikult üleswõtma ja otsystamiseni hoiu alla panema. Üksikud tükid sellest oliwad:
üks wana laud
üks seina kell
kaks pilti
üks tool
üks wäike kast wanakraamiga
üks säng
üks hõbedane doppelkapsliga taskukell
üks aianuga
üks mapp mitmesuguste paberite ja moonaraamatuga
üks waskpäsul
üks wähwm kärnerinuga
üks pakk raamatuid
habemenuga
paar tinast supilusikaid
üks paletat ?
üks ületõmmatud kasukas
üks suur wastne paletat?
üks wana kasukas
 üks wana paletat
wiis paari pükse
kolm lühikest kuube
kolm westi
üks paar kindaid
üks wöö
üheksa särki ja 2 manisihkat
üks kangas
neli shalli
säitse lambanahka
kaks tekki
kaks magamisekotti
üks padi
üks peegel
par mõõtu linaseemneid kotiga
paar wiltsaapaid
paar wanu kinngi
kaks pada
20 leisikut willaseid lõngu
üks plekist piimakann
kaks paari sukke
üks lammas kahe poegadega
üks lehm
Peale nende waranduse tükkide oli Jaan Sulbyl weel raha:
1. 100 (Sada) rublane Wene pankpilet (3. jauskonnast) № 62142.
2. Saja rublane hommikumaa laenupilet № 350,833,Decbr. Coupon. ?
3. Wiis rubl. sularaha.
4. Üks kartohwlitäht Grünbergi käest, kellelt tal 50 rubl. saada on.
5. üks sedel № 269,420. kop peale.
Eelnim. warandusetükid panti esiteks mõisa ja peale selle wallakohtu pitseri alla, aga kõik rahapaberid ja tähed (wiis tükki) anti herra von Schwebsi  hoiu alla, seniks kui kohus waranduse seletamise ja jagamisega walmis on saanud.
Peale selle otsustadi, kõik eelnim. waranduse asjad 16.s.k.pw. oksjoni teel rahaks ümbermuuta kui ka seadusline kuulutus ametlikus lehes "Talurahhwa kuulutajas" kolme kuu peale wäljapanna lasta.
Pääkohtuni. J.Nilbe /allkiri/
kohtunik: J. Aripmann /allkiri/
abiline: W. Wisse
Kirjutas: Zuping
