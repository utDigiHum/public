Nellandalt, tulli ette Hindrik Traat (Krõta poig) ja temma wöörmünder Hinn Traat ja kaibas et temma perremees Johan Kuhhi ollewa tedda hao tüwwega pessnu, selle perrast et hao kubbu olnu halwaste tettu. - n: n: e: -
Kutsuti ette Johan Kuhhi ja sai kaibus ette loetus, kes selle pääle ütles, pesnu minna tedda mitte ei olle se om wõlts, ent wallale olle minna tedda, temma omma küssimisse pääle lubbanu, tem(m)a es kule üttegi kassu mann minno sõnna, minna panni tedda karja, sis lask zea kik kartohwlihe ja teiwa suure kahjo, panni hakko käändma, sedda ka es tee ni kui tarwis olli, et mitte ütte kubbo paikast ärra tõsta es woi, sis laggosi ärra. -
Otsus   Ent leppisiwa essi henda wahhel ärra, nink kuulutiwa kohtole om(m)a leppingut, sest et Hindrik Traat es tahha ennamb tee-
nida, nink J. Kuhhi es tahha wäggisi piddada, nink ni jäiwa lahhutedus, et kumbaltki poolt midagi nõüdmist ei olle, ent Krõta Messi kartohwli wak mes mahha pantu om, saap tema kige täwwega kätte ilma hinnata. - nink lätsiwa rahhun wälja. -
                                      Karraskij Kohtomajan Jusal sel 28al Mai´l 1876.
                                                                                                Eenistja, Jaan Tullus  XXX
                                                                                                 Mannistja, Jakob Uibo  XXX
                                                                                                 Mannistja, Jaan Leppason  XXX
