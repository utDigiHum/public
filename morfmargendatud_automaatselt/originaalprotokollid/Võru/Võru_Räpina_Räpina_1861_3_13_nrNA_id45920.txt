Jama Peter Ritsmann kaibab et temma om hainu aig tu neitsik Ewa Indrikson palganu ning kaerahha andnu, ning paele see om 5dal Septembri gelle Juhak Lorits endale palganu ning kaerahha andnu.
Juhan Lorits wastutap temma om küll 5dall Septbr todda tütrigu palganu, ja tu tütrik ei olle talle mitte üttelnu et ta Peter Ritsman oma palganu.
tütrik Ewa Hindrikson wastutap temma ei olle Juhan Lorits käest rahha tahtnu wutta, temma om weggese puhhhu pandnu tu sullane Leppa Jaan om ka kulnu.
Sullane Leppa Jaan wastut temma om kulnu ja nennu kui Naitsigulle Juhan Lorits weggigi too kaerahha andnu, rehhallutse mahha kendnu ja kaerahha andnu, ja tu kaerahha Peter Ritsmanni keest om temma welle wutnu.
Moistetu: Et Juhan Lorits om weggisi tolle tütrigulle Kaerahha andnu, gep temma tast ilma, ning gep seel kos ta haemehlega enne om künnelnu.
