N 18. Koggokonna kohto een om kaiwano Josu moisawallitsusse nimmelt Michel Torrop et selle koggokonna inneminne Ado Kehrberg ei anna mitte Herr von Molleri wõllatähte wälja komb om suur 500 Rbl (wiissadda Rbl. Ado Kehrberg om sepäle kostno, et Herr von Moller ei olle kikke rahha ärrä masno, 450 Rubla om masto, sis om weel masmada 50 Rbl ja põle aasta Protsent 500 Rubla päält. ja kui Herra massap se pole aasta Protsenti 500 Rubla päält nink mes se 50 Rubla om 2 aasta saisno selle protsenti ka, sis annap temma wõlla tähhe wälja. 
Herra ei massa selle perrast sedda 50 Rub et üts Bankbillet om tol kõrral saddano kui se rahha saije wälja mastus, ent kui sai Billet sisse antus sis [mahakriipsutatud: saije] kantse iks protsenti. Kui se Bankbillet ei olle tol kõrral saddano kui saije antus. 
Moistus
Kui om rahha keik ärrä masto sis piab Ado Kehrberg se wõlla kirja Herr von Molleri kätte ärrä andma.
Pakohtomees Jürri Waask [allkiri]
Abbi Peter Pallo [allkiri]
dito Juhhan Traks [allkiri]
