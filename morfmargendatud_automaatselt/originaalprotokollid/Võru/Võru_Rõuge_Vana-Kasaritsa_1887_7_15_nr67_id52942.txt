Protokoll N 62 manu 1887 a.
Kutsmise pääle olli ette tulnu Liso Pang oma man saisja ja mehe Jaan Pangiga ja and wastust kui tälle kaibus ette sai pantus, et tema ja tema näitsik Katri Krelowits ollew kül kunagi ütte rüppi täwwe haina Mõisa haina maa päält põimnu, enne neid ollew ka jo selle niidu päält, kost nema põimnu põimetu olnu.
Katri Krelowitz oma man saisja Jaan Jakobsoniga and kaibuse pääle wastust, et tema oma perenaise Liso Pangiga kül kumbgi ütte rüppi täwwe hainu mõisa haina maa päält põimnu, perenaine ollew teda ütten kutsnu.
Wöörmünder Peter Pedras, kes kahjo üle kaenu, üttel, et mõisa niitu olnu kül õige rohkeste põimetu olnu kottussite, ent Jaan Pangi naisel ja selle näitsikul olnu kummalgi katte puuda osa hainu põimetu olnu.
Nema es lepi.
Otsus:
Jaan Pangi naine Liso Pang sai süüdlases arwatus ja peap Mõisawalitsusele kahjo tasomist masma 2 Rbl. 14 päiwa sisen a dato ja trahwis peap Liso Pang 24. tunnis kogokonna türma minema oma kulu pääle.
See kohtu otsus sai kohtun käüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus. Mõisawalitsus nõwwap Appellationi.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
    "                P. Klais [allkiri]
Appellation mõisawalitsusele 21. Julil 1887 wälja antu.
