Jaan Häidesk kaibas kohto een wasto Jaan Puksandi et temmä om tenno Jaan Puksandil päiwi tögä mis ilma masmatta om, nüüd nõuwap Jaan neide päiwi eest massetus sada. Need päiwät ollewa 1871 aasta suwwel tetto olno.2 päiwä linnu köitnu			60 kop		6 päiwä Nekrute wimisse aig tenno			1 R 80 kop		4 päiwä linno köitno			1 R 20 kop		3 päiwä maad kördaman			90 kop		3 päiwä kaplo tenno			90 kop		3 päiwä rüggä õlge kündno			90 kop		6 päiwä wankre telgi tenno			1 R 80 kop		1 päiw külwno ja kündno			30 kop		1 päiw rodja puid tono			30 kop		Kokko			8 Rubla 70 kop		
29 päiwi
poig Rein Janil ollewa ka Jaan Puksandi teeno Talwel 8 näddälid selle eest nõuwap Jaan 8 Rubla Jürri päiwäst Jakob Päiwäni 13nt näddälid selle eest nõuwap Jaan
26 Rubla
34 Ruba kokko
Tütre Helse palka ollewa weel samata jäno 1 leisikas linno ja 2 naela willo.
Kresla eest ollewa sada jänud 1 1/2 Rubla Seddä rahha nõuwap nüüd Jaan Häidesk Jaan Puksandi käest kätte sada.
Jaan Puksand üttel et linna köitmisse eest om ärrä masno 5 Rubla Tütre palga ollewa ka temmä ärrä andno Ja Jaan ollewa 5 wakka kartowlid tenno mahha Jaan Puksandi maa päle kus Jaan Puksandi sitt om panto, ja Jani hobbbese ja leiwägä tetto selle eest om Jaan poiaga päiwi tenno temmäl, ja talwel ollewa 1 1/2 kormat haino ja 4 kotti agganid om andno ja Lehma om karjan laskno käwwä Jani Poig ollewa kül aasta päle Jani manno olno leppito teenma agga Jaan om enne Jakob päiwä poia ärrä wõtno teenistussest tö aigo.
Jaan Puksand üttel et Jaan Häideski poig om talwel 3 päiwä hobbest olno aiaman massina mann agga mitte kattese näddälid ei olle olno.
Jaan ollewa ka 1 nael willu tütrel andno ja 1 Rubla rahha 10 naela linno eest mis sis kohto polest kästo om ja selle perräst om 1 nael willo andmatta jänud et tüttär lapse samisse perräst enne aigo tenistussest mant ärrä om länno.
Soldat Karl Munsk tunnistas nänno kui Jaan Puksand om 1 Rubla Jani tütrel rahha andno
Jani tüttär om nüüd kolo
2 Ratta telge om Jaan terwe näddäli tenno ja lakkad om wannad olno. Ja 1 ö ja päiw om Jani Hobbene ka Jaan Häideski käen olno.
Kohhus mõistis, et Jaan Häideski assi om 20 Septembril 1871 ärrä mõisteto ja et temmä nüüd ilma asjanda om kaibano peab temmä 24 tundi törman ollema ilma asjanda kaibusse eest.
§773 om ette kulutedo
Jaan Häidesk pallel prottokoli
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Josep Ritsmann
Kohtomees Writz Konsap
Kirjutaja J. Jaanson &lt;allkiri&gt;
