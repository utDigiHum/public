N 3. Selsamal päiwal.
Peter Lilloson tegi omi poege wahe leppingot, Jaan Lilloson wõttab oma sõssare Katre, ja Liiso, oma piddada, kui mehele lääb lubab sajad piddata, ja temmale jaos anta üts lehm, lammas, ja kuhwri ühes anda. Michel Lilloson wõttab Mari, Miina oma pidada kui mehele lääb jaos nentele üts lehm lamas üts kuhwri ja sajad piddata.
Ne lepijad tunnistiwa selle leppingo rahul olewat. 
Pea kohtunik H Mitt
kohtumees. A. Liin
kohtumees. Jan Soosaar
Kirjutaia A Saremõts.
[ääremärkus: Lep. 17/VIII 916 N 8.]
