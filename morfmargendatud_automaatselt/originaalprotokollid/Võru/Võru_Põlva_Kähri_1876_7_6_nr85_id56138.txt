Ette tulli Kähri õige usu Preestri Herrä ja pand ette, et mööda lännu aastal om Kähri wallaperemees Jakob Kibbena neide woorimehes löönu Pihkwahe sõitma ja lubanu oma wankri nii sääda et midagi kahjo ei saa, sis saatnu oma poisi ütten, ja see pois oli sis ütte Preestri palitu ärä kaotanu, mes 30 rbl. masnu. Ja nõwwab 15 rubl. ehk ka 10 rubl.
Jakob Kibena wastutab et temä ei ole Pihkwan käünü enegi temä sulane, ja temä ei tija sest kaotamissest midagi, sedä tijab pois. Ja preester käsknu enegi wankri pääle ütte warjo tettä tuule warjos ja päälegi om Preester üts jago mino woorirahast maha aewanu, ja ärä lepinu.
Jakobi sullane Jüri Ollesk ütlep et temä ei ole nännügi Preestri pallitud, ja preester ei ole ka temäle asjo näütanu mes temäl oles hoita olnu. Minol oli oma hobene perrä kaeda, aga mitte preestri ei ka neide roiwaste hoita.
Otsus:
Kogokonna kohus mõistab et kahjo tegija peäwa pool kahjo kahjo saajale masma, ni et Jakob Kibena 2 rubl. 50 kop. ja sullane Jüri Ollesk 2 rubl. 50 kop. Preestrile kahjo tasomist msm.
Otsus kuuluteti  § 773 ja 881 perrä T. S.
Jakob Kibbena es ole otsusega rahul.
Päält kiri 17 Juli.
Pääkohtomees Andres Nikopensius
Kohtomees Peter Nemwalz
  "   Johann Nikopensius
  "  Jaan Leib
 
