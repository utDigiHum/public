Moisa wallitsusse polest kaibati et Krensthali rentnik Wido Jonas ei olle omma kontrahti leppingit täitno, nink pallub koggokonna kohhut temmä kontrahti murda.
1, om Wido piddano iggä aasta 1 nurm kiwwist puhhastama ni et nüüd sel aastal 3 nurme peasi prilha puhtad ollema, punkt 2 agga se ei olle tetto punkt 1 kartowle maa pääl kus linna piddi ollema om teiste nurme wasto kontrahti manno tetto linna.
4 ta punktis et Wido Jonas ei olle mona meestele ni paljo maad andno kui tarwis om
Wido Jonasse käest sai küssitu üttel temma et süggisini saap 4 nurme puhtas kiwwidest.
Linnu om 5 wakkamaad ni kui kontrahtin tetto et teiste nurme olleme 1/3 tenno sest olleme kül rummalad.
Wöölmünder Gustaw Zupson, ja kohtomees Jakob Türkson kes ülle käisid kaemann tunnistiwa et neil om 2 nurme puhtad muido om seggiti puhhastedo. Koggoni puhhast ei olle üttegi nurme parraja weetawa kiwwist.
Kohhus möistis et Wido ei olle kontrahti punkte täitno selle perrast möistis kohhos neide kontrahti tühjas et Wido 1870 kewwädi majast wäljä lähheb.
§773 om ette kulutedo
Pä kohtomees Paap Kiudorw
Kohtomees Kristian Wössoberg
Kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Hindrik Toding
