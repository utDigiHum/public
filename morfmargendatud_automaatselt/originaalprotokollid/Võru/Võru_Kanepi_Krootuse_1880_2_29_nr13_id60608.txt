Karraskij Kogokonna Kohhos sel 29 febr 1880
                                      Mann olliwa Päämees Jaan Tullus
                                         do       do   Kohtomees Jaan Leppäson
                                         do        do abbi   do      Johan(n) Raig
Tulli ette Peter Wisnapuu (Krotussest) ja kaibas et Kusta Sibbol wõtno kewäja 1879 temmä tütter An(n) Wisnapuu hendal karjosses, nink nimmelt terwe aasta pääle, palgas ollo leppito 1 rubla käe rahha 10 rubla palgas 10 nakla linno 1 wak kartohwlit mahha panda perremehhe seemnega 2 kosti ja 1 naggel willo, nink kodapolitse lehma hoitmisse est palk 1 rubla weel pääle ja nüüd lasknu sel
12 nowembril süggise wallale muido olle kiik katte saano, kui ennegi 10 nakla linno om saamata, ja nelja kuu latse söötmisse eest nõwwa 12 rubla, selle et enne leppitut aiga om wallale ehk ärra laskno. -
Kutsuti ette Kusta Sibbol sai kaibus ja nõudminne ette loetus, kes selle pääle ütles, meil olli suwwe pääle leppito, nink palk olli kümme rubla rahha üts wak kartohwlit mahha P. Wisnapuu seeme kats kosti ja üts naggel willo nink muud ei middägi ja palle kohhot et minno kartohwli wakk taggasi saas ni samma ka se koddapolitse käest wõeto üts rubla rahha. -
Sai weel kohto poolt küssitus kas keddagi kauba mann kuulgat olli wai tunnistajat kostusses ei keddägi. -
Otsus.  Kohhos moistis et tunnistust kummagil es olle ulles anda, nink karjuste wõtminne koolitamisse perrast ikkes suwwes om, ni samma ka kartohwli seeme karjusse poolt om Peter Wisnapuu kaibus tühjas, nink Peter Wisnapuu peäp se seemnes woeto üts wak kartohwlit kattessa päiwa aja seen Kusta Sibbolal ärra massma. -
Sai kohto Otsus nink §§ 772   773 nink 774 kohton kaiadelle ette loetus et Peter Wisnapuu es olle rahhol et arwas hendal liiga tetto ollewat pallel Protokolli mes kohhos ka lubbas anda. -
L.S.                                                            Pääkohtomees   Jaan Tullus  XXX
Protokoll wälja anto                                Kohtomees     Jaan Leppäson  XXX
sel 6 märtzil 1880                                 abbi       do      Johann Raig
