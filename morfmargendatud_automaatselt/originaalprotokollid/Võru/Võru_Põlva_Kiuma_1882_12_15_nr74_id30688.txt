Sai Liiwl. Talor. Säd. §965 äratäitmiseks kõik selle walla waestelaste ja läski naiste wöölmündrid ettkutsutud omast selleaastasest ametipidamisest aruandma. Nad on järgmised ja on igaühe kohta iseäranis tähendada:
I. Liis Matsoni wöölmünder H. Lukatsil leiti asjad kõik korras.
II. Kristjan Matsoni wöölmünder Joh. Nilbel leiti asjad korras.
III. Leena Molok wöölmündr. Jaan Jorrol niisama.
IV. Kadund Jüri Wanke laste ja Jacob Wanke (An Wank poja) wöölmündril Jacob Wankel ei olnud ka midagi ütlemist. Kõik jõulisemad lapsed on korrapärast teenimas.
V. Liis Tanni wöölmünder Juhan Rammu'l oliwad asjad korras. Üksnes seda häda kaebas ta ja nagu siin ka teada on, et Liis Tannil praegu siin mõisas natuke kehwa läbisaamine olla, sest palk olla wäga odaw ja mõisawalitsus ei tahtwat pealegi õiget korterit anda.
Wöölmõndrile tehti siitpoolt kohuseks, et kui mõisas läbisaamine enam wõimalik ei ole, siis seda siin kohe ülesanda, et asi parema korrale saaks.
VI. Liisa Aintsi wöölmündril Joh. Aintsil leiti asjad korras: raha kõig protsendidega täiesti käes ja korras.
VII.Kadund Paap Kimaski laste wöölmündr. H. Peedmannil asjad korras, kuid seda kahjatses ta, et laste ema omal wolil neid teenima pandwat kuhu tahtwat ja kuida tahtwat. Kohtu poolt lubati naesterahwale ära keelata, täitsa ilma wöölmündri teadmiseta lastega tallitada.
VIII. Mari Kantso wöölmünder Joosep Zuping andis üles, et seesama liig kehwaks terwise poolest olla jäänud ja ei jõudwat iseenesele enam midagi saada. - Kohtu poolt kästi seda kogukonna walitsusele ülesanda.
IX. Kadund Kusta Tiidemanni läsja ja laste wöölmündrid Iwan -  ja Michl. Tiidemannil leiti asjad korras.
X. Kadund Joh. Tanni laste wöölmündr.  Jacob Kibenal leiti asjad korras.
XI. Kadund soldat Joh. Nemwaltsi lesja ja  laste wöölmündrid Jaan Wasser Tilsest ja Jaan Nemwalts Kährist andsiwad üles, et naene wigaline olla ja ei saawat kusagilt abi puude juuremuretsemiseks etc. Seda kästi wallawalitsusele otsustamiseks ettepanna.
Puudus on Märt- ja Joh. Porila, Jaan Nilbe, Karl Aints, Jaan Urb ja Kusta Naba. Need on 30. Decembr. ettekäsütada.
Pääkohtunik: J. Nilbe [allkiri]
kohtunik: A. Luk xxx
d: P.Aabel xxx
Kirjut.as. Zuping [allkiri]
