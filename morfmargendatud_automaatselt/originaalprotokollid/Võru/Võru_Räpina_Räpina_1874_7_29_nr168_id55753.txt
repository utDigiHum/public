Räppinä Wabriko wallitsusse polest R. Pillmann kaibas kohto een omma Sõssa Amalie Pillmani eest, kes wabrikon rahha wäljä anda om, et Arrola wallast närdso müia Johann Jürgenson ollewa närdso mümissemann närdso wasto anto Wentlaudist anto rahha tahhede numbred ümber tenno suremba rahha arwo päle.
11 Junil 7me Puda täht om tetto 63 Puda, ja 23 Nagla päle, närdso pudt om masno 1 Rubla 25 kop, Wõls numbride perrä rahha 
Johann rohkemb tenno 70 Rubla 71 kop mis Johann Jürgenson ollewa omma essägä kantoris saatno, ja säält ollewa Amalie Pillamann se wõls rahha Johani Essäl 70 Rubla 71 kop wäljä masno.
Johann Jürgenson üttel küssimisse päle et temmä ei ollewa mitte seddä närdso tähte ümber tenno enge temmä ollewa seddä 7me Puda eest anto närdso tähte essä kätte andno kantorist rahha wõtta, ja temmä ollewa seddä 7me Puda eest anto tähte wasto 8 Rubla 75 kop sanud ka Essä ollewa seddä rahha tonod kantorist selle 7me Puda närdso tähhe wasto 8 Rubla 75 kop.
Essä Jaan Jürgenson üttel et temmä om selle närdso tähhe kantoris winud, ja säält ollewa 9 Rubla tükki anto, ja 25 kop ollewa temmä taggasi andno, sis ollewa mamsel 5 kop napsi rahha andno.
Rudolph Pillmann üttel, et se päiwäl ei ollewa suggugi 7 Puda tähhe eest kantorist rahha anto
Friedrich Wentland tunnist et temmä mitte ei olle 63 Puda ja 23 nagla tähte andno enge ollewa 7 Puda tähhe Johanil andno.
Rudolph Pillmann üttel, et kantorist ollewa Johani essä Janil 70 Rubla 71 kop wäljä anto wõls rahha. Ja kige õigusse rahhaga kokko Kontorist anto 79 Rubla 46 kop. ja 14 kop napsi rahha.
Johann Jürgenson üttel et temmä om teisi paiw 11ne Junil andno neid närdso.
Räppinä walla kohto polest antas seddä asja allandlikko palwega Keiserlikko Werromakohto ette selletädä neide Tarto Kreisi Ottepä Kihhelkonna Arrola walla meistega ja Räppinä wabriko wallitsussega.
