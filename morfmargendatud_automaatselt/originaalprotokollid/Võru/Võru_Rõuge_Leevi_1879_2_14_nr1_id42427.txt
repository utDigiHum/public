Protokoll Nr 1.
Lewi kogokonna kohus, sel 14 Webruaril 1879.
Man olliwa päkohtumees Karl Lauk
kohtumees Jaan Thal
" Jacob Kriwa
Ette tulli Pindi wallast Jaan Plado nink tõst Lewi weskiwwi möldre Jacob Lauri päle kaibust, et se temmale 2 näddali ja katte päiwa tenistust wälja ärra ei massa,
nink näddal olnu lepitu 1 rubl. 50 Cop. teep ülle kige wälja 3 rubl 50 Cop. Jacob Laur and selle päle wastust et temma om selleperrast Jaan Plado palka kinni piddanu, et se ilm temma teedmata teenistusest ärra om lännu, ja aasta teenistus om lepitu Jürri päiwani, kelle ülle ka Peter Härmsson ja J. Uibo Wastsest Koijolast tunnistust woiwat, ent mitte ütsinda nädali päle.
Otsus: Peter Härmson ja J. Uibo Wastsest Koijolast sel 7 Märtsil ette kutsutus.
Päkohtumees [allkiri] Karl Lauk
kohtumees [allkiri?] Jakob Kriwa
"  [allkiri?] Jaan Taal
