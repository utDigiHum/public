Saie leping tettus küla maia tegemiste peräst wastset wiisi:
Wana wiisi küla poolt tegemise mano jalksi päiwi andmine jääb hoopis maha, selle eest saab maia tegija külla poolt, oma maasuuruse perrä nii kui nüüd Thadri arw om 4 rubl. Thadri päält, kui see wana arw ära lõppep sis saap ka wastse wiisi perrä niisama säetus masmine massetas ka Thadri arwo päält, ei mitte tallode päält. Palgi wedamine ja kattusse pandmine jääb wana wiisi, kattusse katmine saab külla abiga kattetus.
Kõrra kaemine maia tegemise peräst jääb kogokonna hooles, kelle kogokond luba saab woib maia tettä.
Wallawanemb Peter Paidra [allkiri]
Wöörmünder Jakob Waino [allkiri]
Kohtomees Peter Nemwalz XXX
Nõumees Peetre Kintsiwa [allkiri]
   "   Johann Wasser XXx
Peremees Johann Wasser
    "  Johann Nikopensius
   "   Johann Tennäsilm
   "   Jaan Nemwalz
Kogokonna kirjotaia P. Nemwalz [allkiri]
