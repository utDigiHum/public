Kaibas siit mõisa kantnik Jacob Parmann et siit walla peremehe Karl Kõrwe 2 hobused olla üle piiri tema hainamaa sisse tulnud. Tema wõtnud nad kinni ja nõuda selle eest 2 rubl. ja tõise hobuse eest, keda ta järgmise päewani kinnipidanud, 50 kop. söötmise raha.
Karl Kõrwe seletas sellewastu, et J. Parmann olla tema ütelusest hoolimata hobuse ärawiinud ja seeläbi temal 7 inimese tööd tõise päewani takistanud, sest et hobust heinarugade wälja wedamiseks tarwis olnud. Parmannil ei oleks tema arwamise järele mitte tarwis olnud, hobust  ära wiia, sest et ta ju tundmata ei olnud.
Otsus:
Kõrwel on Parmannile selle ühe hobuse kinniwõtmise eest 1 rubl. 2 nädalani a dato äramaksta.
See sai seaduse järele kuulutatud.
(:Allkirjad:)
