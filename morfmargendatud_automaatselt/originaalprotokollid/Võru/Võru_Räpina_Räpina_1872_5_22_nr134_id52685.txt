Gustaw Norruks Tositest perrismaa piddäjä kaibas kohto een, wasto, wasto Wösso Petert, ja Jaan Norruski selle perräst, et nemmäd 1871 süggisi omma karjaga om lasno Gustawi nidon Järwe weren käwwä ja om nido häddälä haina ja haino kuhjas ärrä om söötno, ja et karri om nidon olno, om Gustaw Norrusk Peter Wössobergi ja Jaan Norruski karjuse kõbbäräd ärrä wõtno, agga et nemmäd kahjo ei olle tassono om neo kübbäräd temmä kätte jänud.
Peter Wõssoberg üttel, et temmä ei tea muido kui karjus om üttelno et elläjäd om nidon kaino. Jaan Norruskiga om jo Gustaw ärrä lepno.
Kohhus mõistis, et Gustaw Norrusk peab karjuse kübbärä taggasi andma, ja Peter Wössoberg peab 1 Rubla karja kindi wõtmisse eest pant rahha masma.
Pä kohtomees Jaan Holsting
Kohtomees Kristian Rusand
Kohtomees Rein Lambing
Kohtomees Paap Kress
