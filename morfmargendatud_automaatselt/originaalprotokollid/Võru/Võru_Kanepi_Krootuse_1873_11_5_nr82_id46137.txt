Keiserlikko V Kihhelkonna Kohtule allandlik palwe
Jusal sel 5dal Nowembril 1873.
Kihhelkonna Kohto herra kässo pääle, lask wallawannemb Johann Ratnik kiik walla wollmehhe kokko tallitada.
                               Nink nimmelt     Heinrik Undritz
                                                           Johann Raig
                                                           Jakop Uibo
                                                           Jaan Loodos
                                                           Jaan Raig
                                                           Michel Loodos
                                                           Michel Järg
                                          nink          Jakop Troon 
Selle ülle Keiserlikko Kihhelkonna Kohhot allandlikkolt pallelda ja teada anda. -
Et Karraskij walla maggasin ülle liga wanna wilja om, ja keaki tedda ennamb wälja ei wõtta, ja ta iks aiga pitti obis hukka lähhap 
Sis omma kiik walla wollimehhe sedda nõwwo piddano Keiserlikko Vda Kihhelkonna Kohto käest sedda sedda lubba pallelda et wõisi (150) üts sadda wiiskümmend tschetwerti rükki (60) kuus kümmend tschetwerti keswi nink (100) üts sadda tschetwerti kaero müwwa. 
Et se nida om tunnistawa kiik walla wannemb nink kiik wollimehhe omma nim(m)e taade risti tõmbamissega.
                                                     Wallawannemb  Johann Ratnik  XXX
                                                      Wollimehhe       Heinrik Undritz  XXX
                                                        do      do           Johann Raig  XXX
                                                         do     do           Jakop Uibo  XXX
                                                         do     do           Jaan Loodus  XXX
                                                           do   do           Jaan Raig  XXX
                                                            do  do           Michel Loodos  XXX
Keiserlikko Kihhelkonna Kohtohe sadet
                                                             do  do          Michel Järg  XXX
                                                            do   do          Jakop Troon  XXX
