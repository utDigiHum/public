Läsk Kristi Punberg kaibas kohto een, et temmä om länno Daniel Kaursoniga Tartohe 1871 Dezembri 16nel ja siis kui nemmäd Tartho om sano on kell 3me aig olno, kell 4 om nemmäd õddago Tarton tohtre man olno ja kui säält korteri om lännowa, om Daniel Kaurson hobbest länno tallitama, paika pandma, ütte teise Liina mehhegä ja om 2 tundi aigo ärrä wiitnno ennego taggasi om tulno ja om üttelno et hobbene om ärrä warrastedo.
Läsk Krista Punberg om selle perräst Daniel Kaursoni Tartos ütten wötno, et temmä hobbese hoidja ja tallitaja ja poisi assemal om olno.
Daniel Kaursoni käest sai küssitu üttel temmä küssimisse päle, et temmä om Kristi Punbergil poisi assemal olno Tarton käimisse pääl söitman ja hobbest hoidman ja söötmän ja siis om hobbene allale olno kui nemmäd Tohtre mant korteris om lännöwä Treppist ülles korteris minneki aig 1/4 tunni aja sissen om hobbene howi päält akna alt ärrä warrastedo taggan otsmissega om paljo aigo länno ennego temmä Kristi Punbergil om weel üttelno et hobbene om ärrä kaono olno.
Essiti om Daniel Kaurson Rathausi pole ja linna mees Karlowa pole otsma länno ja liggi maja kus hobbene olli ärrä warrastedo om nemmäd jälle kokko sanowad.
Se maja kust hobbene om ärrä warrastedo peab ollema porri uletsen Maddis Pirgi majan olno ja siis kui nemmäd kokko om sanowa, om nemmäd jälle ärrä koddo taggasi lännowa.
Ja Ratsansi pääl üttel ka Daniel Kaurson ärrä käinud, ja säält Soldati käest trummeldamist küssinu, ja Soldat om hommen kellä 10ne aig kutsno.
Hobbene om wäärt olno 70 Rubla
Ja riistad wäärt 15  Rubla
Hobbese hinda arwas Jakob Punberg
Daniel Kaurson om wastse nõu koggokonnast minnewäl aastajal Tähtweren elläno, ja nüüd se talwe om Räppinäl Läsk Kristu Punbergi mann olno linno rabbaman.
Tunnsitusse perräst jääs polele.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Gustaw Tolmusk
Kohtomees Rein Lambing
Kirjutaja J. Jaanson &lt;allkiri&gt;
ja liggi maja kus hobbene olli ärrä warrastedo om nemmäd jälle kokko sanowa
Se maja kust hobbene ärrä warrastedo peab ollema porri ulitsan Maddis Pirgi majan olno.
ja siis kui nemmäd kokko om sanowa om nemmäd jälle ärrä koddo taggasi lännowa Ja Rathausi pääl üttel ka Daniel Kaurson ärrä käinod ja säält Soldati käest trummeldamist küssinu Soldat om hommen kell 10ne aig kutsno.
Hobbene om wäärt olnon 70 Rubla
Ja riistad kokko wäärt 15 Rubla
Hobbese hinda arwas Jakob Punberg
Daniel Kaurson om Wastse nõu koggokonnast minnewäl aastajal Tähtweren ellano ja nüüd talwe om Räppinäl Läsk Kristi Punbergi mann olno linno rabbaman.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Gustaw Tolmusk
Kohtomees Rein Lambing
