Lewil sel 30 Junil 1876.
Man olliwa: Pääkohtomees: Jaan Thalfeldt
abbikohtomees: Joh Kausar
" Johan Suits
Protokolli N 24 perra sel 16 Junil s. a. Peter Lane kaibuse wasta Michel Mattus ja tõiset tulli ette Michel Mattus ja ütles et minna käwwe kül õddangu Karraskil, ent ei mitte tütrege man nink es olle ka Peter Lane minnoga ütten. Juhhan Lissit ütles et minna ei olle Peter Lane ei ka Michel Mattussega tütregu man käunu nink ei olle ka kuulnu kui Michel Mattus Peter Lanet kutsus nink läts Michel Mattus enne hobbeste mant ärra ja perrast Peter Lane. Peter Thalfeldt ütles et minna ei olle keddagi pesnu ennegi näije minna omman künin tulle walgust.
Otsus:
Tunnistaja Johan Thalfeldt tõises kohto päiwas tallitada.
Se moistminne om § 773 ja 774 perra tallitedu.
Pääkohtomees [allkiri] J. Thalfeldt
abbikohtomees Joh. Kausaar XXX
" Johan Suits XXX
Kirjot. [allkiri] Jõggewa
N 29
