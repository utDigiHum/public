Ette tulli Kahru Jaan Kirber oma katte pojaga ja lupab poig Johannile anda 1 hobene 1 lehm ja 1 lamas. Nink poig Samo kui temä maia wõlla ära massab edimatses perijas.
Johann Kirber oli esä lubamisega rahul enegi nõwab kes temä waiwa mes 200 rubla om massab.
Leppiwa:
Samo Kirber massab welli Johann Kirberil 100 rubla, niida et Johanil enämb majast ei ka esa kramist enämb nõudmist ei ole, ja Johanni een õigus jääb temäle.
Johann Kirber om sellega rahul, mes esä ja welli Samo temäle oma lubanuwa ja ei luba enämb midagi nõuda, ei maja õigusest ei ka maja kraamist pääle selle ei esä ei ka welitside käest, ja et temä een õigus jääb welle Saamo omas.
Sedä lepingot kinnitawa
esä Jaan Kirber XXX
poeg Johan Kirber XXX
  "  Samo Kirber XXX
Pääkohtumees Jaan Luik
Kohtumees Peter Tallomees
  "  Johan Punnak
Kirjotaia PNemwalz [allkiri]
