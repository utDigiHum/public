Ette tulli Karrilatzizt kadonu Mõtsawaht Jakob Nikopensius wanemb poig Johann Nikopensius ja and ülles, et temäst Kroonu Mõtsawahti oma kadonu esä asemele ei sa, nink sellega üten oma emä ja wanembide sõsaride toidja ei woi olla, ja pallep et temä eest wõisi ta nooremb welli Peter Nikopensius wõis saada mõtsawahis säetus. Sest ta olla hendä ka jo mõtsa Herrä poolt wallale pallelnu.
Peter Nikopensius tahab sedä Kroonu Mõtsa wahi ametid wasta wõtta ja lubab kroonu mõtsa wahti ja kõik käsu ja sääduse kroonu mõtsa peräst täweste täüta ja ka oma emä nink sõsarid hendä man pidada ja neid toita.
Selle lepimise pääle kirjotawa hendä
Johhan Nikopensius [allkiri]
Peter Nikopensius [allkiri]
Emä om ka sellega rahul.
Kogokonna kohtul ei ole selle wasta pandmist ja kinitawa Peter Nikopensiuse Kroonu mõtsawahis.
