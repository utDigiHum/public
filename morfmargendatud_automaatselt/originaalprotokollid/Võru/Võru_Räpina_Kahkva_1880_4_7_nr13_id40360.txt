Kaiwas J. Kuus kaupmehe Untuwaldti sell, et Peter Lepson olewat temaga lina müümise kaupa tennu 20ne punna pääle a' 2 rubla pund ja wõtnu ette wälja 40 rubla. Seda wölga olewat Lepson linnoga ära tasonu 30 1/2 rublani ja jääb weel temal massa 9 1/2 rubla.
Wastutas Peter Lepson ja es saa salata, et temal 9 1/2 rubla wõlga kaibajale ei ole. Lubas massa, ent jalamaid ei olewat seda wõimalik.
Otsus.
Peter Lepson lubas 9 1/2 rubla ära massa kaibajale sell 10dal Nowembril 1880.
Pääkohtomees Joh. Lepland
Kohtomees Jaan Kirhäiding
Kohtomees Josep Oinberg
Ära massetu.
Kirjotaja D. Otsing &lt;allkiri&gt;
