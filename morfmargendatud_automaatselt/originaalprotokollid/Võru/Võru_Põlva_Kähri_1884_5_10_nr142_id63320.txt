Kähri mõisawalitsus tuli ette ja andis üles, et pooleteramees Mihkel Karask olla mõisast üks koorm heinu wälja wiinud ja ära müünud mis tema leping mõisawalitsusega mite ei luba ja nõuab neid heinu ühes trahwiga tagasi tuua ehk 25 rubla selget raha. Heinat on tema Tillemõisa walitsejale Hõrmanile äramüünud kes tõeks võib tunnistada.
Mihkel Karask wastab: Kähri mõisa heinu ei ole mina Tillemõisa walitsejale ühte kõrtki müünud. Tilsist Mihkel Püwi käest ostetud heinu 40 punda olen mina kül Tillemõisa müünud.
Otsus:   Tulevaks kohtupäevaks Tilsist Mihkel Püwi ette talitada.
