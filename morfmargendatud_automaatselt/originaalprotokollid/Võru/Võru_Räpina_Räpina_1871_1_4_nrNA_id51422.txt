Adam Sultson kaibas kohto een et Jama külläst Peter Wöikowil säräne kurri pinni olno ja om temmä naose ärrä hirmutano taggast perrä josno kainlast pakki mahha tõmmano ja rapputano ja naine Ann om ärrä heitono ja rosi rindo lönod nink peab haige ollema.
Peter Wöikowi käes sai küssitu üttel temmä et et tea et kas pinni om kurri keddägi ei olles ärrä kisno.
Adam Sultsoni naine Ann üttel issi et pinni om pusa külge kindi aijano, ja rapputano ja säält om siis ärrä heitno ja haiges janod.
Tunnistajad es ütle kumbagi ollewad.
Tohtre tunnistusse perräst jääs polele.
