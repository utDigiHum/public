Karraskij kogokonna kohhos Jusal sel 3 Octobril 1875
                                             Mann olliwa Pääkohtomees Jaan Tullos
                                              do       do    Kohtomees   Jakop Uibo
                                              do       do  teine  do        Michel Wõsso
Tulli ette mõtsawaht Hindrik Tuhhand ja kaibas et Peter Linker om üllege motsast ragono 16 tükki kuusi ja peddagit mes ülle kige 98 tolli wälja teep. -
Kutsoti ette Peter Linker ja sai motsawahhi kaibus ette loetus tolle pääle ütles temma mulle om Saksa motsawaht Jaan Uibo lubba andno et woin ragoda  Selle et Jaan Uibo es olle tallitet jaije assi tõise kohto päiwa pääle selletadfa. -
                                (Allakirri nago üllewalgi)
