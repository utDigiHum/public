Ette tulli Peter Kolk ja kaibas: Gusta Assi ja Michkli Assi naise olla edimatsel Jõulu pühhal kerrikost tullen ülle tema mõtsa soitnu ja mõts olli kinipantu, ja siis wõtti mina mõlembide käest tekki ree päält ärrä nink perran lõunat kui ma kollitarre manu lätsi siis nakkasiwa Michkli ja Gusta Assi mino haugutama ja lubbasiwa kerriko tulpan kuuluta, nink pallep nüüd Peter Kolk sedda asja selledata ja neil ärräkeelda säält käumast.
Gusta Assi ja Michkli Assi ütten naistega tulliwa ette ja ütliwa et nema kül säält ülle oma sõitnu ja Kolk om neide tekki ärräkisknu ja tahtnu ka hobbest eest ärrä wõta nink oma nema selleman ni heitunu et nema mitto nädalit perrast haige oma olnu ja pallewa ka Kogokona Kohut neile abbis olla et nema oma rohho ja terwüse rikmise raha kätte saasi ja nõuab Gusta naine Liso Assi sadda Rubla selle eest.
Michkli naine Ann Assi leppisiwa Kolgaga ärrä.
Gusta naise Liso kohhus jäije tõises kohtupäiwas.
Gusta Weits+++
Adam Org xxx
Kusta Kolk +++
Kirjotaia ass: G Daniela [allkiri]
