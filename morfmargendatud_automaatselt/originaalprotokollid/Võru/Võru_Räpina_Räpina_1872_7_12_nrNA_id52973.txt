12 Julil kui 29 Mail Gustaw Otsmani kaibus langa kaddomisse wõi wargusse perräst ette olli loeto
Tunnistas Jaan Ritsland et siis kui langad Tartust taggasi om tudu om neo langad 2 nakla raskemba olno kui sis kui Jakob Warrep neid Gustaw Otsmani kätte ärrä om saatno, ja nimmelt 1 tük langa om wähhämbäs jänod. Ja Jakob Warrepi käen om neo langad 2 nakla kergembäs jänod.
Kohhus mõistis et Jakob Warrap, peab 1 Rubla masma Gustaw Otsmanil neide 2 nakla lango eest mis ärrä kaono.
