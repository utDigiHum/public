Ette tulli Wana Kasseritsast Märt Kadak ja kaibas, et Jaan Suurmägi tälle 2 mõõtu linaseemnid ära ei maswat, tema nõudwat seda kätte.
Wastut Jaan Suurmägi, et tema ollew kül Märt Kadaka käest 2 mõõtu linaseemnid lainanu hinna perra 2 1/2 rubla.
Tõent, Märt Kadak et tema raha eest ei ollew müünu, tema nõudwat seemnid tagasi ja ei ollew mitte raha eest müünu. Tunnistaja Johan Jantra üttel, et tema ollew ka Kadaka käest linaseemnid ütte mõõdu ostnu ja 1 rubla 35 Kop. mõõdu eest masnu, Jaan Suurmägi ollew kaubelnu ütte wakka ja lubanu oodnus 1/3 ala lina maad ja nõudnu teda wastutajas, Suurmägi ollew ilma temata perast lännu ja Kadak ollew 2 mõõtu andnu.
Nema ei lepi.
Otsus:
Jaan Suurmägi peap Märt Kadakale masma 4ja nädala sisen a dato 2 rubla 70 Kop.
See kohtu Otsus sai kohtun käüjile kuulutedus.
Pääkohtumees J. Kabist [allkiri]
Kohtumees P. Sillaots [allkiri]
         "           P. Klais [allkiri]
