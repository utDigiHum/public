Protocollis № 283 tõstetud kaebtuse pääle wastab  Kaarli Kepp, et tema ei olla mite Jacob Laari maa päält keswa niitnud.
Pääkohtumees Johann Waks tunistab: Mina olin juures kui Mõisa walitsus Keppi ja Laari keswa wahele piiri pandis, kus ka Kepp ja Laar ise juures olid ja olen ka selle keswa mis Laar ütlep oma maa päält Keppi olewad äraniitnud ülewaadanud ja leidsin seda õige oliwad, et Kepp laari maa päält 1/3 keswa on äraniitnud.
Mõistus.   Et Kaarli Kepp Jacob Laari maapäält 1/3 keswa äraniitnud on siis mõistab Kogu konna kohus et Kepp peab Laarile 5 rubla kahju tasumist maksma.
Otsus.   Kepp maksab Laarile 5 rubla 2 nädali sees wälja.
Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtus käijatele etteloetud. Kumbki ei olnud otsusega rahul ja palusid ärakirja wälja mis ka lubatud sai.
Pääkohtumees Johann Waks
Kohtumees: Jaan Salomon
do: Johan Wasser 
Kirjutaja M. Bergmann [allkiri]
27Nowembril s.a. ärakiri Kaarli Kepple wäljaantud.
 
