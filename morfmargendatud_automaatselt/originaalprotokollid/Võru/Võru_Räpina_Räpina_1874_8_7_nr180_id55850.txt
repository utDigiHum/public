Haigusse perräst parranemisse päle wälja lasto, ärrä kaddono pilleti Soldat Gustaw Grünbergi taggan otsmisse ja nõudmisse perräst om kohto ette kutsutu temmä liggimbä suggulasse ja teedja.
Soldat Gustawi welli Ludig Grünberg tunnist et temmä welle Gustawist middägi ei tea sest 1874 aasta keskmisse suwwise pühhäst sadik kui temmä Werole läks, päle Jürri päiwä ollewa temmä Meeksin olno, wiggalinne om temmä olno. paigal ollewad korteri ei olle temmäl olno.
Jaan Lepmansoni naine Hels tunnistas, et Gustaw Grünbergil ollewa temmägä tarre ütte olno osteto ja 2 suwwe om sääl olno, 1873 süggisi om se 1/2 tarre ärrä münu kui ta hobbese wahhi ammetid Tosite küllän ärrä om andno sest sadik Hels middägi ei tea. Mud rahwas ka ei pea teedma.
Räppinä walla wallitsus ja walla kohhus tunnistap sellega et meije Soldat Gustaw Krünbergi mitte kätte ei sa, ja et temmä 1874 kewwädist sadik kige omma pilletiga kaotsin om.
Hels Lepmanson tunnist et Soldat Gustaw om tõtteste haige olno.
Päkohtomees Jaan Holsting
Kohtomees Paap Kress
Kohtomees ​​​​​​​Josep Ritsmann
