Kaibas selle mõisa pärisherra F. von Schwebs, et siin elaja Ahja mees Jacob Peelowas tälle juba mõnda aastat 17 rbl. 35 kop. wõlga olewat, mida ta ka nüüdgi weel ärä ei makswat ja nõudis, et nim. Peelowas siitpoolt sunnitud saaks, seda wõlga peawõimalikult ärätasuda.
Jacob Peelowas ei salganud, et täl herrale niipalju maksta olewat, waid palus kannatada, sest et täl praegu wõimalik ei olla.
Herra lubas 10. Novembr.c. kannatada ja lepisiwad sis mõlemad sedawiisi ühte, kuna J. Peelowas seks termiiniks wõlga ärä lubas maksta.
Eesistnik: J. Nilbe (allkiri)
Manistnik: A. Luk XXX
d.: P Abel XXX
Kirjutaja as. Zuping
