Tulsewa kohto ette retsep Kristjan Melning ja Hindrik Toom ja andsewa ülles ehk na ommawa endawahhel leppino kui siin alla om kirjotud.
Hindrik Toom annab omma poig Hindrik retseppa Kristjan Melninge oppipoises 6 aasta paele, arwatud Janipaiwast 1864, Reetsep Melning lubbab selle est oppipoisele kiik pälmise ridet, sög ja saapat, ta lubbab temma Koli saata ja pärahha massa ja kogi wisega old kanda ehk oppipois saap häste erraoppitud.
Kui 6 aasta ommawa möda länno ja õppiaig möda on siis retsep Melning lubbab oppipoisale Hindrik Toom net ridit kingimes.
Kristjan Meningsemel &lt;allkiri&gt;
Htom &lt;allkiri&gt;
