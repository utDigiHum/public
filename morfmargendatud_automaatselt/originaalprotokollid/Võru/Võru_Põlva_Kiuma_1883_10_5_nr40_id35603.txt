Kaibas siit mõisa pops Jaan Kallas, et siit tõine pops Hindrek Tubli ei tahtwat tälle 3. hobuse tööpäewade eest mitte 3 rubl. wäljamaksta, nagu neil lepitud olnud.
Hindrek Tubli wabandas, et neil olnud wiie rubla peale kaup tehtud, mis eest Kallas tälle tema paariwaka alla põllumaa harimiseks ja muu tallituste toimetuseks terweks suweks hobust anda lubanud. Tema olla tedä esiteks kolm päewa pidanud, aga et ta temaga suuremat tööd ei ole saanud teha, sest et hobune tiine olnud, siis ei ole ta pääle selle enam wõtnudki. Tema olla siiski walmis nende 3. päewade eest  wäljamaksma, aga  3 rubla olla liig palju palja hobuse eest ja tema oma söögi peal.
Mõisteti:
et Hindrek Tublil on 1 1/2 rubl Jaan Kallasile 2 nädalani a dato wäljamaksta.
See mõistmine kuulutadi seaduse järele ette. Jaan Kallas ei lubanud rahule olla.
Pääkohtunik: J. Nilbe /allkiri/
kohtunik: H. Lukats /allkiri/
d.: J Aripmann
Kirjutaja: Zuping /allkiri/
