Ette tulli Jaan Adamson ja kaibas, et Ado Paio naine Katri selle tee, mes Pääkohtumees Jaan Kaabst ja kohtumees Peep Klais temale wallale tennu ja piiri pandnu, kinni pandnu ja tee tähe wälja pilnu; tema nõudwat, et tee tälle wallale saas tettus.
Wastut kaibuse pääle Katri Pai oma mansaisja ja mehe Ado Paioga, et tema kül ne tee tähe mes ameti mehe pandnu, wälja pilnu.
Ado Pai wastut, et tema neist tee tähti wälja pilmisest midagi ei teedwat, sest tema ei ollew seda nännu, kui tema naine neid wälja pilnu.
Nema es lepi.
Otsus:
Ado Pai peap Jaan Adamsonile see kohtumehist näidatu ja wallale tettu tee hommen päiwi s.o. 16. Julil s.a. wallale tegema ja Ado Pai naine Katri, kes oma lubaga kohtumehist pantu tee tähe wälja pilnu see tee kinni pandnu sai süüdlases arwatus ja peap 24 tunnis kogokonna türma minema nüüd sama oma kulu paale.
See kohtuotsus sai kohtun käüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
     "                P. Klais [allkiri]
