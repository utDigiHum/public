Jaan Paidrow Kährist kaebab, et Jaan Purason ja Peeter Nemwalts ja Jaan Olesson käiwad ilma tema lubata oma karjaga üle tema Platsi ja palub kohut, et seda ära keelaks.
Jaan Olesson wastutab: Nii kui mina üle Paidrowi ma käin niisama käib ka Paidrow üle minu maa ärgu käigi tema üle minu siis ei käi ka mina üle tema maa.
Jaan Purason wastab niisama.
Moistus. Kogukonna kohus mõistab, 
et peawad iseeneste wahel karjateedega ära õiendama ehk lepingu tegema mis tingimesega keegi üle teise maa wõib käia ja kui oma wahel sellega lepida ei saa, ei tohi keegi il lema lubata üle teise maa käia.
