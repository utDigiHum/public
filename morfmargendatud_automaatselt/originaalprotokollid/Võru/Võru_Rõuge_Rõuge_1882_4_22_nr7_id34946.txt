Ette tuli Jakob Aid ja andse üles, et temal olewat Jakob Aplini käest nõuda:Sula raha lainatu, aastal 1879			  6 Rbl.  -Cop.		selle protsent 3 aasta eest			  1 Rbl.  8 Cop.		hobbese ostmise raha 10 rbl. protsent 3.a. eest			  1 Rbl. 80 Cop.		üte woorin käümise eest Walgutale			  1 Rbl. 50 Cop.		Pihkwa kiwitee raha			             25 Cop.		1 wahtrase massina trulli puu eest			  1 Rbl. 50 Cop.		1 Sänna surnukassa kõrramas			  1 Rbl.		18 karnist nissu ja selle 4. aasta oodus 2 Kr.			  3 Rbl. 50 Cop.		Summa:			16 Rbl. 63 Cop.		
Kohto kutsmise pääle tuli Jakob Aplin ette ja ütel, et temal ei olewat Jakob Aid'le midagi muud massa, kui üts wahtrane massina trullipuu 25 Kop. kiwitee raha nink 2 mõõtu nisse. Muust rehnungist tema ei teedwat midagi. Kui kohus oles mulle pääle pandnu selle 10 rbl. raha protsenti massa, sis oles ma seda ka masnu, ent mis minu käest om nõwwetu, seda ole ma ka masnu.
Mõistus:
Et Jakob Aid'l selle lainatu 6. rbl. üle, wooriraha üle nink Sänna surnu kassa massu üle mänestki tunnistust ei ole, sis omma ne kaibuse ja üles andmise tühjas mõistetu niikawwa, kui Aid neides asju üle tunnistuisieiole toonu. Kihelkonna kohtu mõistuse perra om Jakob Aid oma 10 rbl. wasta wõtnu, nink ei ole temal sis enamb midagi nõuda.
Ent Jakob Aplin peab Jakob Aid'le 14. päiwa seen wäljamasma:
Kiwitee pääl lainatu raha       - Rbl.  25 Kop.
massina trulli puu eest           1 Rbl.     - Kop.
18 Karnitsa nisu eest               3 Rbl. 50 Kop.
      Summa:                               ¤ Rbl. 75 Kop.
Pääkohtumees: J. Kroon [allkiri]
Kohtumees: J. Pruwli [allkiri]
         "            J. Meister [allkiri]
Kirjutaja: J. Treu
Aid edasi kaibanu.
Protokoll No 97 all sissesaadetu.
