Juhann Raikant orrigo mehhe kutsmise pael, ja 
palles Juhann Raikant protokollis ülles wõtta mis temma Mihel Kattussele on wõlgo andnut, ja palleb temma kui peas üts kõrd Mihel Kattussele auktion saama tettus kui temmale sis saab § 960 neljas jaggo pt. 5 perra kige muid korwalisse wõlgonandja een tost perrajänu konkursi jagos saas erramassetu:
Johann Raikant and ülles kui temma on andnu 23 rubla obbedas
sai wastus: kui Mihel Kattus todda tõttes tunistab woib temma üllewankirjutut tahtmes perra anda.
Karrasky 10l May 1857
Juhann Raikant xxx
