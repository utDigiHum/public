Aste ette selle mõisa kantnik Gusta Tiidemann ja kaibas, et Jaan Roosi karjus Aleksander Rihm olla omal koeral tema lamba nõnda äräkiskuda lasknud, et sest enam midagi saada ei olnud ja tema teda ärä pidanud tapma. Tema nõuda 4 rubl. kahjutasumiseks.
Aleksander Rihm (eestkostjaga) wastutas, et Gusta Tiidemanni oma karjus , kes ka selle koeraga tuttaw olla, teda oma lambale peale olla aeanud, koer aga, kes muidugi wäga kiskuja olla, olla lamba mõtsa aeanud ja seält ikka edasi, ilma et karjuse keelust oleks hoolinud. Temal ise olla wiimati korda läinud, teda tagasikutsuda.
Otsustati:
Eestulewaks istumiseks mõlemad wastanikud kui ka selle koera peremees Jaan Roos ettekutsuda lasta. 9. Septembriks.
Pääkohtomees: J. Nilbe (allkiri)
kohtomees: A. Luk XXX
d.: P. Abel XXX
Kirjutaja as. Zupping
