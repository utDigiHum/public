Mihkli Laine kaibduse asjan Jaan Asi wasto ( № 26) olli täämba kohto ette tulnu Juhan Asi, kes wastutas kui temäle Jaan Asi ja Mihkli Laine ütlemise ette saiwa loetu, et temä ollew nee lawwa kül Tille weski pääle wiinu ja kogemata sinna maha unetanu, kost nemä wiimäte ärä ollew kadunu.
Otsus: Johan Asi peap nee lawwa 2 nädali sisen ärä wiima, ehk niipaljo raha masma kui nee lawwa wäärt omma.
Joh. Pähn xxx
Joh. Pursson xxx
Jaan Narusk xxx
Kirjotaja Luik [allkiri]
