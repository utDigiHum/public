Schmalz kaibas kohto een, et Fompko om temmä sullase Alexander Bärlin Tormast teise passi mureteno ja rahha andno, ni et temmä teist passi om Törmast sanod Alexander Barlin om passi aig om weel Malzi käen 31 Märzini eddesi wanna passil ja Alexander om Malzil kahjo tenno ja warrastano kokko 27 Rubl 3 kop kahjo mis Malz om ülles andno.
Fomka käest sai küssitu üttel temmä, et temmä om andno 5 Rubla Alexandrel rahha passi tuwwa Tormast. Agga Fompka üttel et Alexander mitte temmä tenistussen ei olle ja issi ärrä wennemale omma paperrä assend om otsma länno.
Kohtomees Kristian Rusand om Fompkale käino käsko andman et Fompka peab Alexandre 8 Märzil Räppinä kohto manno saatma Malzi wõlla perräst agga Fompka es olle mitte saatno.
Kohhus mõistis, et Fompka peab Alexander Pärlini 15 Märzi kuu päiwäni 1871 sija Räppinä kohto manno tallitama, ja kui Fompka Alexander Parlini sija Räppinä kohto manno ei tallita wastutap temmä selle Malzi kahjo eest.
§773 om ette kulutedo.
