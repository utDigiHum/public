Tulli ette Widrik Urgard nink kaebas et Karli Leplandil ollew temma maa pääl hooned ja ei tasowat selle eest temmal midagi, ja ollew ka Karli Lepland hobene röa olle pääl waste ristikhaina pääl olnud ja sis olli temma üttelnud et piasi hobese ärräajama, ja sis om poeg se hobese wötnud ja perast om naine tulnud ja om see hobese ärrä wiinud ja om tedda trotsnu et sinna olled minno hobese pää söönu sis wõid ja perse süwwa, ja nöwap selle maa eest kos temmal maja pääl om 3 päiwa, jalgsi, ja naise trotsmise eest trahwmist, nägija selle man Kristi Rummask et hobene om ristikhaina pääl olnud, ja ollew ka kuulnud kuis Liina Lepland tedda om trotsnud.
Tulli ette Kristi Rummask nink tunnist et Karli Lepland hobene om kül rükki olle pääl olnud, trotsmist ei ole kuulnud seda om nännud et Liina Lepland om rätti pääst ärrä wõtnud ja om tannoga Widrik Urgardi kummardanud ja om üttel terre herr.
Tulli ette Karli Lepland nink wastat et temmal Widrik Urgardiga midagi lepitu ei olle, maja al temmal enam om kül maad enge muud temmal maad ei ole ilma massuta, enge temma om kül päiwi tennud Widrik Urgardil, aga sel aastal om temma kül tgemata jätnud.
naine ei olew tema kuulden joht Widrik Urgardi trotsnud.
Otsus
Möistis kohhus et Karli Lepland piab ega aasta Widrik Urgardil kats päiwa ja Jakob Parmannil 2 päiwa hoonete all ollewa maa est tegema, tol körral kui tarbis om.
Kuulut Widrik Urgard et temma otsusega rahul ei ole.
Kuulut Karli Lepland et temma rahhul om.
