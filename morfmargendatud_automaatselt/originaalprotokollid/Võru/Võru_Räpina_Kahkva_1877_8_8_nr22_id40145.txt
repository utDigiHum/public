Kaibus.
Ette aste Jaan Lepmann nink kaibas, Gottlieb Andersoni karri ellajad ollewa temma röa wiljale kurja tennuwa, nink karjusse wälja ütlemise perra ollew üks Zigga Gottlieb Andersoni ja üks töine Zigga Daniel Jusari karjast olluwa, nink kohtomees Paap Sabbal ollew sedda kurja üllekaeno.
wäljanöudminne:
Gottlieb Anderson, Daniel Jusar ja Wido Using saiwa ette kutsutu, nink neile se Jaan Lepmanni kaibus ette kulutedu, selle päle wastutiwa nemma et selle tarbis ommawa karjuse kes ellajad peawa hoitma.
Otsus:
Gottlieb Anderson, Daniel Jusar ja Wido Using maksawa egga üks, 1 mööt rükki Jaan Lepmannile kahjotassomist, nink wöiwa sedda jälle ommu karjuste palga mant maha wötta.
Päkohtomees Gustav Lepson
Kohtomees Paap Sabbal
Kohtomees Josep Jöks
