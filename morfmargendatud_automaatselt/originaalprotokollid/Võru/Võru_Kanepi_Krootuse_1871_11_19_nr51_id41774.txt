Sel samal päiwal
Kaiwas Maamõõtja, et tema rentnik olewat ööse wäega kangeste rehe ahjo kütnu, nink ei olewat esi mitte man olnu.
Jaan Mark kutsuti ette; Kusiti: mesperast olet sa ni hooletu olnu, ent Mark ütles, et tema om man olnu, nink wõi seda tunnistada Rein Herne, kes selgeste nägi seda.
Rein Hernelt küsiti: Kas olli se ni et rentnik reheajo ütsinda jätse, se tunnist selgeste rentnik olli tõtteste man olnu.
Et nüüd sääl midage kahjo es ole saanu, nink tunnistus selge olli, mõist kohus rentnigo selle man ilma suuta.
Sai ka kohtun kaiale § 773-774 kuulutedus.
                                                 Päkohtomees Jaan Tullus  XXX
                                                Kohtomees  Jaan Wärs  XXX
                                                   tõine        Peter Mandli  XXX
