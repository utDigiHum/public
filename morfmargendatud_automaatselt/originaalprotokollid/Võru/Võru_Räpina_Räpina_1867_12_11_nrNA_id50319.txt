Adam Kaarhäid tulli kohtu ette ja kaibas et Lisu Sabbolisk om 3 kuhja haino temmäl ja 2 kuhja haino sullasel ärrä kiskno ja kano ja neist warrastedo hainust ütte wäikest kuhja ülles tenno, ja om lubbano Lisu Sabbolisk ütte wäikest kuhja jättä Adam Kaarhäidil selle assemele et temmä Adama kuhjast haino om warrastano. Se päle om ärrä lepnowa. Agga et Lisu Sabbolisk leppingut ei olle piddäno ja om omma lubbaga seddä kuhja haino Adam Kaarhäidi nido päält ärrä koddo winod.
Lisu Sabbolisk sai ette kutsutu ja küssitu üttel temmä et ma kaindlago wisin ütten kaindlan ollid tored hainad teisen kaindlan olliwad temmä hainad se perräst et kuiwi haino olli torila sissi tarwis et nemmäd pollama ei lähhä seddä ollen ka handis pallenu ja selle eest ka päiwi tenno.
Jakob Märtman tunnistas et kakkoto olli kül wiiest kuhjast 1 olli wäega ärrä narritu. Ja nideto haino mis unnikun olli ka ärrä kaono Adam Karotsingi jaost.
Marri Karotsing tunnistas et Lisu om länno näppo täist tõmbama ja om paljo tulnu ja pallel et kullokessed andke andis.
Adam Kaarhäidi nõudminne olli kohtu küssimisse päle eggä ärrä kakkotu kuhja eest 1 Rubla hõbbe trahwi.
Kohhus mõistis Lisu Sabbolisk peab 3 rubla hõbbe trahwi maksma neide 5 ärrä kakkoto kuhja eest Adam Kaarhäidi ja temmä sullase kätte. Ja tullewal Eespäiwil peab masseto ollema.
Pä kohtu mees Rein Pabusk
Kohto mees Jaan Kliman
Kristian Kress
