N 1. Waimela kogokonna kohus 21 April 1883. 
Man olliwa: Pääkohtomees Jaan Kuk
Kohtomees Johann Runthal
Kohtomees Jaan Rikkand.
Kohto een andse Hindrik Rikkand, Michel Rikkandile üts sada kolm kümmendviis /135/ rubl lainuses 6% pääle kattes aastas, selle tingimisega, et lainaja massap aasta kevast 67 rubl. 50 kop. ja 2 aasta perast 67 rubl 50 kop. Seda lubawa mõlemba oma käe ala kirjotamise läbi ja kinnitawa:
Lainaja: Hindrik Rikkand [allkiri]
lainuses wõtja Michel Rikkand +++
Pääkohtomees: J.Kuk [allkiri]
Kohtomees Johann Runthal xxx
Kohtomees Jaan Rikkand xxx
Kirjotaja JHäusler [allkiri]
[Ääremärkus: Michel Rikkandi peresade wöörmünder Peter Masto jun Jaan Rikkand oma maski Hendrik Rikkandile 135 rbl  Hindrik RIkkand. [allkiri]]
