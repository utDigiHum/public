Neljas kohhus sel samal päiwal.
Ette tulli Martin Undritz kaiwas et 3 karjust Märri Wagga Mihkli Hublik ja An Mandli tem(m)a zia ollew ni ärra pesnu et ennamb ei olle koddo saanu ja olli üsjaga tooni nink sis lõppetanu.
Ne kolm karjust es ütle hend middagi teedwat muud kui sedda es woi sallata et zigga terwe mõtsa läts.
Et Päkohtumees zia ülle olli kaenu sis olli ta löidnu et kohhalt pestu ja werriallane mõllemba pole külle olnu.
Kohtu otsus: Et karjuse kohhalt sallasiwa nink ärra es ütle kes nimmelt to kurjateggija olli, peawa ülle kolme sedda kahjo kandma ja
2 1/2 Rbl. Martinale ärra masma.
                                                  Pä kohtumees Jaan Tullus
                                                   Kirjutaja H. Undritz
