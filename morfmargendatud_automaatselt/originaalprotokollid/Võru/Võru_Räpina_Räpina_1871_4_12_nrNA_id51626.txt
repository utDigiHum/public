Irriska Polchlin Steppanowi Wõbsost kaibas, et temmä om länno Ramani pole hälli manno, kus Wiskä hälli pääl om olno, ja sääl on Irriska üttelni Wiskäle et minne mahha minna ei lasse sul höro omma kaplo, siis Wiska om löno temmäle 3 körd,
Wiskä käest sai küssitu, üttel temmä, et ei olle minna joht lönod, et Irriskä tulli hälli manno ja üttel minne ärrä mahha halli päält sinna wanna Lits sinna pitsitäd latse wäljä; Ja Irriska om temmä käisist kindi wõtno, ja üttelno minne mahha wanna hoor Lits.
Irriskä andis tunnistajad ülles Jaan Norrusk Marie Timofei.
Irriskä andis ülles, et Jaan Norrusk om säält mödä länno ja nänno kui Wissä om teddä löno.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Gustaw Tolmusk
Kohtomees Paap Kress
Kohtomees Rein Lambing
Kohtomees Kristian Rusand
