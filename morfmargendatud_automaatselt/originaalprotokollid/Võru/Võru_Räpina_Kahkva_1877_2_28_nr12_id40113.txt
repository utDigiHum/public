Kaibus.
kaibas Wallawannemb Wido Ritzing, Walla magasi aitast ollu Jama mona tarbis 15 30/64 tsehtw. karo Werro liina wija, nink selle tarbis ollu neile, kes piddanu karu päle wötma, käsk antu säetul päiwal magasi mant 3 wakka karo päle wötta; ent Jama karo käestandmise man ollew arw mitte wälja tullu nink 3 wakka pudus tullu, nink temma piddanu selle puduwa karo eest 4 Rb. 50 Cop. wäljamaksma; Selle päle tullu wälja, et Daniel Glasmann ei ollew mitte tullu magasi mannu neid 3 wakka karo päle wõtma; Temma wallawannemb nöuwap, et se raha taggasi mastus saas.
Otsus:
Daniel Glasmann saap Kohto ette kutsutama.
Päkohtomees Gustav Lepson
Kohtomees Paap Sabbal
Kohtomees Josep Jöks
