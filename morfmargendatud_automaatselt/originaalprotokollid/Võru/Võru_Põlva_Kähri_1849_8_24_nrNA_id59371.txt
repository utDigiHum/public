Perremees Kachro Jaan nink Jacob kaibawa et Perremees Risso Peter 6 aastad nende karjamaast mis temma Hainamaa weeren 2/3 wakka maa ärra niitnu nink Haina hendale wõtnud.
Perremees Risso Peter sai ette kutsutus nink wastas kussimissen, et temma 6 aastad sedda tükki niitnu, sest et Maamõtja temmale sest ossas lubbanu nink temma sedda puhhastanud.
Selle peale sai Kardist kaetud, nink Kart tunnist et se tük mis 63. nink 64 numer all Kachro Jani nink Jacobi jaos on.
Kochtomees Peter Kirber, kes minnewa aasta neile se tükki ülle kaenu, nink sanu moistetus et Risso Peter, neile katte Perremehhele 1 Rugga Hainu piddanu masma.
Moistetud:
Risso Peter peab Kachre Junil nink Jacobil 2 Rugga hainu masma, nink sai kinnitud et temma eddes päidi omma wahhe piri ülle ei tohhi niita. Need 2 Rugga Hainad massab Risto Peter 1 Rubla hõbbedaga.
