Tänasel ülemal nimetatud päewl andis wana wallawanem Johan Leib uue wallawanemale Johan Pähnale järgmised raamatud, kogokonna kohtu juures olewaid ära.
1. Kogukonna maksu raamat selle wälja minek 1187/88 a. 127 Rbl 76 kop ja sisse wõtmine 149 Rbl 44 kopik.
2. Passi raamat kelle sisse wõtmine 1887/88 eest 3 Rbl on.3. Taadre raha raamat. uus.					4. Wõeraste raamat.			need raamatud on weel läbi rewideerimata		5. Rulli raamat.			    "		6. Wolikohtu protokolliraamat.			       "		7. Keriko ja jaama maksu raamat			             "		8. Soldane raamat			                  "		9. Rewisioni raamat. 			    "		10. Wamiliraamat.			           "		11. Wallapolitsei protokolli raamat.			               "		12. Wakko raamat			        "		13. Rewisioni ehk ümber kirjutus lehet			       "		14. Walla walitsuse pitser.			            "		15. Ameti raha.					
16. Wäärtpaberite ja Magasi raamatud jääwad wasto wõtmata; sellepärast et wana wallawanemal kodu jäänud ja jääb kuni 5. Webruarini s.a. 
Peakohtunik: J Narusk. [allkiri]
Kohtunik: T. Weits [allkiri]
   "   T. Päkko [allkiri]
Uus wanem. Joh Pähn [allkiri]
wana wallawanem J. Leib [allkiri]
