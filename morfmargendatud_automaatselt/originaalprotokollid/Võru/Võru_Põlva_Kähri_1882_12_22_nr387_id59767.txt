Ette tulli Jakob Laar ja kaibas Juri Wokk pääle kes teda olla wargas sõimanu nink lubanu tema pääd ära lahku niida et tema ei wõiwad enamb peremees olla küll olla kogokonna kohus teda keelnu, ni sama ka mõisa politsei ent ei olla ka neid suguki kuulnu weel päälegi ässitama ta muid tõisi ka weel (hendad) teda wasta üles.
Sai Jüri Wokk sen kaibusen küsitu ütles: Kui kohtumees ja mõisa politsei seda tunistawa et ma olen nõnda teinud. Wargas ütli mina teda tema tegude perra, sest minul om wakk kaubeldu poole ma ole saanu, ja sest ole ma weel weski matti weel ära masnu.
Tunistaja Mihkel Käis küsitu ütles: Wokk ütles Sa oled mino warastanu wakaga, ahwardamist es ütle tema kuulwad.
Tunistaja Jakob Reino ütles niisama kui ennegi Laar käsknu Wokil henda luwwa Wokk olla ütelnu, mes eest ma sinno löön.
Otsus Jääb seeni poolele kunni Jaan Wals ja Mihkel Käis naine Mari ette saawa tallitud.
