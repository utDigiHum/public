Ette tulli läsk Marri Karpsepp, ning pallel Koggokonna kohut, temma tahab omma wäimehe Kusta Parsoni mant, töise wäimehe Hans Liwako pole ärra minna, ent Kusta Parson ei tahtwad temma jaggo krami wälja anda, ning nimmelt ollew Kustawi käen:1 Lehm		2 waka rüki		1 waka keswi		10 rubla raha ja		1 Kirst rõiwaitega		
Gusta Parson tulli ette ning üttel temma ei aja mitte omma naise emma henda mant, ent kui temma ihaltap essi minna siis on se temma poolt kelmata; ent temma nöuwap emma lehma sötmise eest 5 Rbl. höb.
Hans Liiwak, tulli ette ning tunnist temma tahab omma naise emma henda pol piddata wõtta seni kui temma ellab, ning temma eest körra perrast hoolt piddata.
Otsus: et Hans Liwak omma naise emma Marri Karpseppa henda pole tahab wõtta, siis jääb keik se kram ja warrandus mes Marri Karpseppal muide innemeste käest weel sata on ütsinta Hans Liwako ja temma naise perralt, ent peas ollema et Marri Karpsepp Hans Liwako mant ilmasjanda peas mant minnema, siis ei tohi temma üttegi häät Hans Liwako käest tagga nöusa; Kui peas Marri Karpson ärra kolma, siis on Hans Wirakul ? oigus ?? ning se nõudja wolgnikude käest.
Gusta Parson saap selle aja eest, et temma omma naise lehma üllespiddanu 4 Rbl. ning keik mu kram peab Gusta Parson Marri Karpseppale taggasi andma.
Pääkohtomees G. Anderson
Kohtomees P. Sabbal
Kohtomees J. Parmak
Sel 28 Fb. 1877 assta sai wallawannemb poolt Hans Liwako kätte kaddunu Marri Karpsoni hä 50 Rb. ärra massetu.
