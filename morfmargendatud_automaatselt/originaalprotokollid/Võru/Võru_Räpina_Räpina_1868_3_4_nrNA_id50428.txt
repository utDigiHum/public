Hels Holsting tulli kohto ette ja kaibas et Alexander Tchernow om teddä lubbano ärrä wõtta ja om temmägä elläno ja temmäl om laps sanod poig Juhhan ja et temmäl ei olle minuagi last toita ja ülles piddädä se perräst pallub koggokonna kohhot seddä asja selletädä.
Alexander Tchernow sai ette kutsutu ja küssitu üttel temmä et minna teddä ütsinda ei olle tenno ja ni paljo körd kui temmä minno manno tulli ni paljo körd minna temmäga elläsin ja keik se assi sündi Räppinä Weski koa mann sel körral kui nemmäd kiwwi Emmändä mann tenistussen olliwad 1/2 aastaiga aigo.
Kohhus moistis et Alexander Tschernow peab maksma eggä aastaja 5 Rbla lapse üllespiddamisses Hels Holstingi kätte. Ja se rahha peab eggä 5 Märzi kuu päiwäl ärrä masseto ollema senni kui 10 aastaiga täis saap.
Pä kohto mees Rein Pabusk
Kohto mees Wido Oijow
Kohto mees Jaan Klimann
Kohto mees Hindrik Toding
Alexander Tschernow tahhab last omma kätte wõtta ülles kaswatada ja hoita ja ütleb et kui se laps om minnul mõistetu siis ma wõttan temmä ka ärrä mis ka kohto poolt keeldmatta om Agga Hels Holsting ei olle sellegä rahhul.
Pä kohto mees Rein Pabusk
Kohto mees Wido Oijow
Kohto mees Jaan Klimann
Kohto mees Hindrik Toding
