Lisu Häidson Jani naine Wabrikust kaibas kohto een et Jaan Sell Soldat om temmäle 3 körd träsägä lönod külle sissi ja 2 körd om kõrwa päle lönod, ja kukruhe om lönod ilma luggematta ja se tülli om laste perräst tulno.
Jaan Selli käest sai küssitu üttel temmä et temmä ei olle mitte 1 körd lönod, et nemmäd issi peawad hendi wahhel tullitsema omma lastega.
Tunnistusse perräst jääs polele.
