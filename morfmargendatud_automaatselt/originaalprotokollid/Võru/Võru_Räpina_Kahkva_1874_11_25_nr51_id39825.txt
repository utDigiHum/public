Herr Zärens Woitka moisa rendnik, kaibas, et sel minnewa neljapäiwal ollew temma sullane Jaan Mõtsar temma wasta hirmsal wiisil rop ja krop ollu, ning ollew nende sõnnadega üttelnu, temma tahhab enne Jürripäiwa omma perremeest läbbi malkutada.
Sel samal päiwal lännu temma (Zärens) rehhe mannu, ning Jaan Mõtsar ollew nende sõnnadega temma wastu üttelnu "tämba saap sinnoga üks kässisödda sama, miks sa mulle mitte olgi eo anna", ning ähwardanu tedda kige kõlbmata sönnadega; selle päle ollew temma (Zärens) Jaan Mõtsari wasta wasta üttelnu, "mis sönno sinna minno wasta aijat, siis ollew Mõtsar jälle wastutanu, mes sinna siis ollet, sest sinna ei massa utte sullase sittaunnikut." Selle päle üttelnu Zärens, temma ei pea mitte unnetama, et temma, temma perremees on; siis Jaan Mõtsar weel üttelnu, temma tahhab tedda weel tämba päiw läbbi malkutada.
wäljanöudminne:
Jaan Mõtsar tulli ette ja wastutas, temma ei salga sedda mitte, et ollew kül nende sõnnadega wasta Zärensi üttelnu selle et Zärens ei ollew temma jaggo olgi andnu.
Hans Jürgenson tunnist, perremees ollew üks koorma olgi kätte annu, ni et siis nemma sullase sedda henda wahel ärra jaggawa, ent Jaan Mõtsar ei olle omma jaggo mitte wõtnu, ning ollew perremeest waega trotsnu.
Peter Oinberg tunnist nisama.
Moisteto:
Jaan Mõtsar saap 15 witsa löki, ent kui temma weel töistkorda peas omma leiwawannemba wastu krop ehk sönnakulmata ollema, siis wõib perremees tedda jallapält tenistusest lahti tehha.
Selle päle tunnist Jaan Mötsar temma ollew ilda aigo lauwa kerrikun ollu, ning pallel sedda trahwi eddesi pole jätta.
Pääkohtomees G. Anderson
Kohtomees P. Sabbal
Kohtomees J. Parmak
