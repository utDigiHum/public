Copie
Meeksis sel 20 Märzil 1872
Wõbso Mölder Maddisson tulli koggokonna kohto ette, ja tõstist kaibust, et mõisa kõrtsi mann, om Michel Griggul Könnost, temmä käest 5 rubla 8 kop ärrä riisno, ja teddä pesno, ent siis omma weel se rahha Wallawannemb Tilga ja körtsimees Jakow käest wõtno nink om hommiku körtsimehhe käest kätte sanod, nink nemmäd ollewat kattekeste ärrä leppinu, ja Michel om 5 rubla lubbano, selle eest massa end ei olle, sano, ja temmä ütlep et rahha om walla wannemb ärrä wõtno, ent temmä mitte,
Walla wannemb Tilga tunnistas, et temmä om Michel Grigguli käest se rahha ärrä wõtno ja kõrtsimehhe hoida andno, nink temmä om se rahha kätte sano, nink muud lärmi ei olle olno, ja ei tea.
Körtsimees Jakow tunnistas, nida samma kui walla wannemb, et om rahha temmä käen olno
Koggokonna kohhus om, selle kaibusse päle, Maddissonil ette andno, et temmä wõib omma kaibust Räppinä koggokonna kohto ette andma kui Michel Grigguli käest nõudmist om.
Pä kohtomees Peter Tensing
Kirjutaja Silberk &lt;allkiri&gt;
Kohhus mõistis, et Michel Griggul peab 1 rubla waeste ladikus ja 1 rubla Maddissonil masma.
§773 om ette kulutedo
Maddisson pallel prottokoli ja ka Michel
Ütten om ka Meeksi Copie ehk tunniskirri Kihhelkonnan ümbre möisteto.
