Ette tuli Ferdinand Ollmann ja kaibas, et temal oli suwwel Juuni kuul püs kambrist ärawarastedu nink seda püssi oli tema Mäe kõrtsimehe Peter Dahrsneek käest kätte saanu. Ollmann palleb kogukonna kohut seda asja üle kullelda.
Peter Dahrsneek tuli kohtu kutsmise pääle ette nink ütel, et tema püssi Peter Klais ja Johan Leoski käest oli saanu. Peter Dahrsneek oli nännu, et Peter Michelson oli keriko poolt teed pidi Wõrro poole lännu nink oli kaugelt nätta olnu, et temal midagi särgi siilu all oli olnu.
Peter Klais ja Johan Leosk oliwa Wõrrult Mäe kõrtsi ette tulnu nink Dahrsneek oli neile ütelnu: Peter Michelson läts kerikomõisa orgu nink temal oli wist midagi särgi siilu al. Klais ja Leosk lätsiwa Michelsonile perra nink tõiwa selle püssa kõrtsi selleperast, et püssa omanik teda säält kätte wõis saada. Seda püssa näitsi mina (Dahrsneek) mitmele nink kui Ollmann oli kuulnu, et üts püs meie pool om, tuli tema senna, nink wei oma tää ära.
Ferd. Ollmann nõud oma püssa warguse eest 50 rbl. nink ega wiidetu päiwa eest 3 rbl. se om 6. kõrra eest 18 rbl. hõbb.
Peter Michelson tuli kohtu kutsmise pääle ette nink ütel, et tema seda püssa olewat Ferdinand Ollmann kambrist wõtnu, temal olewat püssa waja olnu nink selleperast olewat tema purju pääga selle püssa säält äratoonu.
Otsus:
Ferdinand Ollmann ja Peter Michelson lepsiwa henda kesken ära niida: Peter Michelson massab Ollmannile 15 rbl. (wiisteistkümme rbl) püssawarguse eest. Seda raha massab Michelson kate nädala pärast täämbat, sest päiwast 10 rbl. nink nelja nädala pärast täämb. p. 5 rbl. ära. Ent et Rõuge walla piire sehen wargus om ette tulnu, warga trahwi waeste ladikohe 2 rbl. hõbb nink 5 kõrra wabandamata wäljajäämise eest 150 kop. siin 14 päiwa seen wälja.
Se kohtu mõistus saije kohtun käijile kuulutedus nink T. S. r. prg 1860 a. prg 773 j. 774 äraseletedus. n.n.e.
Peter Michelson [allkiri]
Ferd. Ollmann [allkiri]
Pääkohtumees: J. Kroon [allkiri]
kohtumees: J. Pruwli [allkiri]
       "             J. Meister [allkiri]
