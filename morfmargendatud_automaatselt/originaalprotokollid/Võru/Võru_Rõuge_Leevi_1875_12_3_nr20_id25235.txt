Lewil sel 3 `` Decembril 1875
Man olliwa: Pääkohtomees Jaan Thalfeldt
abbikohtomees Joha Kausaar
" Jaan Meos.
Protokoll N 18 kaibuse perra mõtsawallitseja asjan wasta Hindrik Kikkas lattide warguse perrast tulli ette I tunnistaja Peter Kolga ja ütles wälja et Hindrik Kikkas tõije kül mõtsast 2 kõrd säljaga torid latte ent mitto tükki neid olli, sedda ei woi minna üttelda II Tunnistaja Jakob Rebbane ütles wälja et meije kattime wanno lattega kattus liggi walmis, sis tõije Hindrik Kikkas mõtsast paar kõrda seljaga lattide tarbis kattust lõppetada ent kui palju neid olli ei woi minna üttelda.
Moistet:
Hindrik Kikas peap, ni kui temma essi wälja om üttelnu et 10 tükki om tonu, egga tükki päält pää kohtomehhe arwamiseperra 1 rubl tük se om ülle kigge 10 rub ent tõiste kohto meeste arwase perra 25 kop tüki päält se om 250 kop masma mõtsa wallitsusele.
Sem moistminne om om § 773 ja 774 perra mõllembile kuluted.
abbikohtomees Joh Kausaar XXX
" Jaan Meos XXX
Kirjut. [allkiri] Jõggewa.
N 20
