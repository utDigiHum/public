Kaibas Kiidjerwelt koolmeister P. Weiken, et nüüd siin eläja Juhan Lepwaltzi kats poiga, kes minewa aastal sääl tema man koolin käinud, talle omma wõlga 260 kop. ärä ei tahtwat massa, mis na sääl talwel paberi, sulgi krihwli ja raamatude laenamisega oliwad teinud.
Juhan Lepwaltz ütles, et ta andnud vanema poja Peetri kätte 2 rubla koolmeistri kätte maksta, sest et neid raamatit poisi üteluse järele sugugi nii palju ei olla, kui koolmeister ütelda.
Pois Peter Lepwaltz ütles: Tema olla koolmeistri käest 3 raamatut saanud a' 25 kop. ja olla seda ärämaksnud. Ka muu koolimateriali eest olla järelt ärämaksetud. Umbes 3 wõi 40 kop. jäänud maksmata.
Mõistmine:
Koolmeister, kelle käest kõik lapsed koolimateriali wõtawad ja kes weel kunagi selle eest ilmaasjata raha ei ole wõtnud, ei saa ka wistist mitte palja 3. wõi 40ne kop. pärast Kiidjerwelt 12 wersta siia sõitnud olema. Ka on see pois siin juba ennemalt kui suur salgaja tuttaw (kae protokoll № 58 s. 23. Septbr.c.) Päälegi peab ju ommeti koolmeistrit enam uskuma, kui koolipoissi. Sellepärast on Juhan Lepwaltz siitpoolt sunnitud, 260 kop. oma poige eest 2. nädalani a dato Peter Weiken heaks siia ärämaksta.
See mõistmine kuulutedi nende kohtokäiatele Liiwl. Talor. Säd № 773 järele ette. Juhan Lepwaltz, kui poja eestkostja ei olnud sellega rahule, waid andis üles, et ta suuremat kohut tahta käia.
Kopia antud 26.Octbr.c.
Pääkohtomees: J. Nilbe (allkiri)
kohtomees: A. Luk XXX
d.:P. Abel XXX
Kirjutaja as. Zuping (allkiri)
