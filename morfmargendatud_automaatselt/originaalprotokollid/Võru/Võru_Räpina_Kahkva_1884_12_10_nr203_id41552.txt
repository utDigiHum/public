Jaan Kirhäiding tulli ette nink kaebas Skooli kohtu pääl ollew Miina Parmann üttelnu et temma Jaan Kirhäiding ollew Miina Parmanni käsknu wölsi et temma ütte tunni päiwas om koolioppusel olnu.
Tulli ette Miina Parmann ja wastat et Juhan Käis ollew käsknu tedda Jaan Kirhäidinge pääle wölsi see est om Käis Miina Paarmanni lubbanu suurte laste sisse panda ja perast pääoppusele tallitata ehk panda.
Otsus: Johann Käis ses 17 Decembris ettetallitata
Pääkohtunik: Peter Urtson
Kohtunik: Kusta Rämman
Kohtunik: Joosep Oinberg
Kirjotaja F Wreimann &lt;allkiri&gt;
