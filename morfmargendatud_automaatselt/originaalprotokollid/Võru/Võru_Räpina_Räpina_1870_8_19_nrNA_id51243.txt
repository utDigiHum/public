Johhan Mällo Meeksi Mölder kaibas kohto een, et Räppinä mees Jakob Hindow om temmäle kewwädi 1 wakkama linna maad andno Räppinä moisa nurmest ni kui moisa sääl linna maad müis ja temmä linna mahha tenno, ja Jakob Hindow om nüüd temmö linna ärrä münod 20 Rubla eest omma wellele ja 15 R olli maa hind Herrä poolt.
Jakob Hindow üttel et selle perräst temmä om selle linna ärrä münod Juhhan ei olle rahha ärrä masnod mis temmäl herräle massa om.
Juhhan Mallo üttel, et Jakob ei olle temmä käest rahha mitte küssino enge om leppito siis rahha massa kui linna kakma lähhäb.
Kohhos mõistis, et Juhhan peab rahha 15 R, Jakob Hindowil ärrä maksma, ja omma linna ärrä kakma.
Pä kohtomees Jaan Holsting
Kohtomees Paap Kress
Kohtomees Hindrik Toding
