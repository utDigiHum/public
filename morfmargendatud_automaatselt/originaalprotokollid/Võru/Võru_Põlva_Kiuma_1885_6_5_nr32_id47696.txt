Kaibas siit wallast naene Liis Tubli ja kaibas, et siit pois August Aabel olla tema reikese äraraiunud, mis eest tema kahjutasumist nõuda. Kusta Lubi wõida selle raiumise üle tunnistust annda.
August Aabel wabandas, et tema seda kaebatud süüd ei olla teinud ja sellest ka midagi pitkemalt ei teadwat ütelda.
Kusta Lubi ütles, et tema ei olla sellest midagi näinud. Ükskord öösel näinud ja siis peale selle olla ta juutide käes olnud, kes siis kui naene nende sindlilaastusi tahtnud wõtta, teda kinni wõtnuwad ja peale selle olla reikene katki leitud.
Otsus:
Liis Tubli kaebdus on tühjaks arwatud.
See sai kohtukäiatele seaduslikult kuulutatud.
Eesistnik: J. Nilbe [allkiri]
Manistnik J. Aripmann [allkiri]
d,: J. Aints [allkiri]
Kirjutaja Zuping [allkiri]
