1904 a. Aprilli kuu 21 päewal ilmusid Krootuse Wallakohtu ette talupojad Kioma wallast Peeter Joosepi pg. Sõrg ja Loosi wallast Jaan ja Peeter Jakobi pg. Luksepp ja palusid järgmist lepingut ustawaks tunnistada.
§ 1. Peeter Sõrg rendib oma talu kohast Nilbe № 8 majast poole põllumaad ja heinamaad Jaan ja Peeter Lukksepp-le 1 aastaks (üheks aastaks) s.o. 23 Aprillist 1904 a. kuni 23 aprillini 1905 aastani järgmiste tingimiste all.
§ 2. Rentnikud Jaan ja Peeter Lukksepp maksawad nimetud poole talu koha eest 120 rbl. (sada kaskümmend rubla) renti aastaks. Ja säeb poole talu posti-tee, ning orjab kõik poole talu kaela orjused maha arwatud tulekassa ehituse materjali wedamine ka rentnikud maksawad selle rendi 21sel Aprillil s.o. ette ära.
§ 3. Rentnikud saawad nimetud talu koha maast, mis posti wahe wiisi saab jaotud igast nurmest ühte wiisi läbisegi ja Petseri piirist, mis wana ussaiamaad on, saawad weikestes tükkides jaotud ja pooles wõetud.
§ 4. Rentnikud wõiwad kartohwlid ja lina ühte kokku kumbagi 5 wakkamaad teha kahekesti kokku.
§ 5. Rentnikud teewad rükkit wiis wakkamaad üle kahe. Poole seemend annab koha omanik ja poole seemend panewad rentnikud Jaan ja Peeter Luksepp, 3 wakka üle kahe ja omanik nõnda sama palju. Sõnnik peab rukki maa  /kesa/ kõige hiljem Jaanipäewani wälja weetud saama ja rukkit saab maha külwatud kõige hiljem 25 Augustil.
§ 6. Heina teewad rentnikud ühes koos teise poole talu rentnikuga ja pärast kui kuiwatud on jautawad pooles ja hein saab mõlemilt poolt (saab) parajal ajal tehtud.
§ 7.  Hooned saawad rentnikud: 1. otsmane ait, wankri kuurialutsest pool pruukimise õigust, pool hobuse talli ja selle otsas küüni, otsmane laut küüni wastu, pool  rehe õigust üle kahe rentniku pruukimiseks, elutarest lõuna pooltsest otsast üks kammer ja köök, ja saun ka pruukimiseks pooles rentnikudele.
§ 8. Elutare seinad saawad, mis rentnikudel pruukida on omaniku poolt seest poolt ära stukaturitud. Ja teised hooned ja kattused peab omanik korralikult parandama.
§ 9. Rentnikud wõiwad üle kahe neli lehma ehk rohkem pidada.
§ 10. Hagu saawad rentnikud põletamiseks 10 raudsülda, mis ise maha raiutud hagudest peawad kokku panem, aga jala päält ei tohi raiuda.
§ 11. Rentnikud ei tohi põhku ega lina luid ja heinu muiale andma ja ära müia.
§ 12. Kõik heinad mis rentnikud maija toowad saab omanik kaaluga  wastu wõtma rentnikudele oma pruukimiseks; nõnda palju laseb ka omanik rentnikudel wälja wiia kui neid ära mineku korral üle jääb, kui üle ei jää ei ole nõudmise õigust.
§ 13.  Rentnikud annawad üleüldse 3 wakkamaad tõunurmedest, 1 wakkamaa Petseri piiridest wana ussaia mant ja kaks wakkamaad rehe tagand omanikule pruukida.
§ 14.  Aia maad jääb rehealutse nukkast elu tare wasta lauda nukkani ja teisest rehealutse nukkast sauna nukkani omanikule pruukida.
§ 15. Heina maad jääb omanikule Kosolaane niidust sellest kraawist saadik, mis niidust läbi lahab postiteed wastu.
§ 16. Koha omaniku lehm ja kaks lammast wõiwad rentnikude karjas käija.
§ 17.  Petseri piiris olewa raendiku peale ei wõi rentnikud oma karja laskma ega tohi heinu poima.
§ 18. Rentnikud peawad wiis koormad aija tegemise puid ja kaheksa koormad kaewu salwepuid neli jalga pikkad, walmis lõigatud, wälja wedama Nilbe talu metsast.
§ 19. Ploomi aed jääb omanikule; metsa aluse maa jääb rentnikudel pruukimata, aga kari wõib metsa all kaia, muud põllumaad on kõik rentnikude käes pruukida.
§20. Rentnikud peawad ilma üles ütlemata selle poole talu koha päält wälja minema 25 märtsil 1905 ehk kui tahab siis wõib 23 aprillini 1905 a. elada.
§ 21. Koha omanik peab kaewu selle koha peale laskma kaewada, mis peab walmis olema kuni 1seni Juulini 1904 a.
Peter Sõrg  [allkiri]   Peter Luksep  [allkiri]  Jaan Luksep [allkiri]
Üks tuhat üheksa sada neljandamal aastal aprilli kuu 21. päewal on Krootuse wallakohus Krootuse wallamajas eesseiswat lepingut, mis talupojad Peter Sõrg, Peer Luksep ja Jaan Luksep oma wahel teinud, kinnitanud, mis juures wallakohus tunnistab, et lepingu osalised kohtule isiklikult tuttawad, et nendel seaduslik õigus aktisi teha ja et Peter Sõrg, Peter Luksep ja Jaan Luksep peale selle lepingu ettelugemise lepingule tõesti oma käega allakirjutanud on.
Kohtueesistuja J Kuhi [allkiri]
Kirjut Kuddo [allkiri]
