Räppinä kohtomees Jakob Türkson kaibas kohto een wasto Wastse kojola meest Michel Punmanni 6 1/2 Rubla wõlla perräst. Jakob Türkson om se rahha jubba 4 aastaiga aigo taggasi Michel Punmanil andno haino eest wäljä massa, siis kui Michel Tolamal om elläno. Ja 80 kop om weel perrä anto olno.
Michel Punmani käest sai küssitu üttel, et temmä, et se iks om õige kül et ma wõlgo ollen agga ma iks teggin to wõrd häd, et ma arwasin seddä wõlgo tassa sada.
1.) om Michel Punman Jakobil andno 30 kubbo ölgi hinda ei olle Michel pandno
2.) om Michel 1/3 alla haggo mõtsa Michel andno Jakobil raggodsa ka ei olle hinda
3.) om Michel andno Jakobil 1/3 allal linna maad ka ei olle hinda panto
Jakob üttel et 1 rubla om maa eest leppito olno ja se peab ka masseto ollema.
Se 30 kubbo õlgi üttel Jakob ärrä masseto ollewa Ja mõtsa ei pea Jakob ka ollema Michkel käest tono, enge se om Jakobi omma nido pääl olno.
Kohhus möistis: et Michel Punman peab Jakob Türksonil 5 Rubla taggasi masma 29 Mail 1872 peab masseto ollema.
Jakob Türkson es olle mõistmisse mann
§773 om ette kulutedo
