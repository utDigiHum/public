Peter Kerow kaibas kohto een et siis kui temmä Tarton Nekrute kaeman käino om Hindrik Kiudow walla polest temmä kiudo mehhes anto, ja Tartust koddo tullen kui nemmäd Rasina körtsis om olno, om Peter Kerow jobnos nänod ja ei olle teedno kui kiudomees Hindrik teddä Essäkessi körtsimanno on tonod, Ja siis om selle wahhe pääl Peter Kerowil piht särgi taskost 33 rubla raha ärrä kaddono mis Hindrik enne Petre teedmist jo taggan om otsno.
Peter Kerow arwap et Hindrik om seddä rahha ärrä wõtnod.
Hindrik Kiudowi käest sai küssitu üttel temmä et eddimält sai ülle hulga 1/2 toopi wina Rassinal ostetu ja perräst ninno päält 1 korter winä, ja söidime Essäkessele ja Essäkessel läksin minna sissi siis olli ni kauwa wäljän Kristian Tolstberg Petre wö wallale wõtno ja siis om Hindrik kui ta seddä om leidno et Petre wö walle olli tetto, om körtsi mehhel üttelno et tulge kaema et Petre wö om wallale, kas temmä waesus allale om
Siis om Hindrik, ja körtsimees, nink Peep Narrosk otsno ei olle ennämb Petre taskon rahha olno - ni sellet Hindrik
Kristian Tolstbergi käest sai küssitu, üttel temmä et minna ei olle wöd wallale wõtno ei tea ka rahhast middägi
Tunnistusse perrast jääs polele
Pä kohtomees Paap Kiudorw
Kohtomees Jaan Holsting
Kohtomees Paap Kress
Kristian Wossoberg
Hindrik Toding
