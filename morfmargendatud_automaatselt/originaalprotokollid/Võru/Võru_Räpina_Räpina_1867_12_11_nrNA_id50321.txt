Grapi prouwa tulli kohto ette ja pallus et temmä rentnik August Schmaltz temmäle 25 rublat renti ärrä massas mis temmäl wölgo ollewad.
August Schmaltz sai ette kutsutu keä küssimisse päle üttel et temmä ollewad rohkemb elläjäd piddänod kui temmäl kontrahti sissen Grapi prouwal olles piddädä olnud om nimmelt piddädä olnod 1 lehm ja 1 lammas August Schmaltz ollewad suwwel kolm elläjäd ennämb piddänod ja talwel 1 lammas rohkemb kui lepping om olno 1/3 wakkamad maad om Grapi prouwa August Schmaltzi käest ärrä wõtnod kelle päle om piddänod maja ehhitud sama se maa om olnud ajamaast wõetud.
Jäeb polele et kontrahtid es olle kohtu käijatel liggi wõetud.
Pä kohtu mees Rein Pabusk
Kohtu mees Jaan Kliman
Kohtu mees Karli Jagoson
Kohtu mees Kristian Kress
