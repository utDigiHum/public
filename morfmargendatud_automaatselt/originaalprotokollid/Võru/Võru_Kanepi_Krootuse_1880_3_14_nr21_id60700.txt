Tulli ette Jaan Planken ja kaibas et ollewa Jakob Troon käest 120 kop wõlgo saada pallel kohhot tedda ärra massmissele sundi. -
Sai ette kutsotus Jakob Troon nink üllewanne kaibus ette loetus kes jälle koggoniste ärra sallas nink es ütle koppikatki wõlgo ollewat enge ollewat temmäle ärra massno. -
Otsus  Kohhos mõistis et tunnistust ei olle teine nõwwap teine salgap sai kohto poolt se rahha pooles mõistetus et Jaan Planken kaotap ommast nõudmissest 60 kop ja Jakob Troon peäp Jaan Plankenil 60 kop wälja massma mes kattessa päiwa aja seen peäp ärra teüdeto ollema. -
Sai kohto moistminne ja §§ 772-773 ja 774 ette loetus ja lätsiwa rahhon wälja. -
                                                                                       Pääkohtomees Jaan Tullus  XXX
                                                                                           Kohtomees Jaan Leppäson  XXX
                                                                                           abbi     do   Johan(n) Raig  XXX
