Ilmusid Kohtu ette Krootuse wallamajasse talupoja seisusest Kioma wallast Eduard Katre poeg Sõrason ja Krootuse wallast Anna Danieli tütar Kantsik ja paluwad nende wahel tehtud suusõnalist waranduse  müügi lepingus maha kirjutada ja kinnitada.
§ 1
Eduard Sõrason müib kõiki õigustega omapäralt olewa liikuwa waranduse Ann Kantsik'le sula raha eest ära, warandus on järgmine:1, üks uus seina kell wäärtus			2 rbl. 50 kop.		2, üks söögi laud wastne kapiga			3  "      -		3, 25 wakka weike punasid kartohflid			12  "    50  "		4, üks siga			10   "    .		5, 25 paari kingseppa liste 'a 50 k. paar			12  "     50   "		6, kolm tiisleri tooli uued  'a 1 rbl.			  3  "    -		7, üks pada suur			  1   "    -		8, üks uus mustjas-hall katetud kasukas 			 15   "    -		9, üks uus mustjas-hall puumwilla palit						
 10  "   -					10, üks must ülikond riideid			  8  "    -		11, üks kuhwri /kast/   wastne			  4  "    -		12,. 200 küünard hamme riiet 'a 10 kop.			20  "   -		13, kolm tekki willatsed 5 rbl. tükk			15   "      -		14, üheksa ülikond naiste riideid			54   "    -		15, 100 küünart püksiriiet linane 			10   "      -		16 ,üks katetud naiste kasuk			12   "     -		17 ,kaks uued suurt rätti			  7   "      -		18, üks naiste rahwa üle-jakk 			  5   "     -		19, neli siidi rätti uued			10   "     -		20, üks lehm			30   "     -		21, üks lammas			  3   "      -		22, üks tosin /12 tükki/  kätterätte			  6   "      -		23, üks wokk			  2   "      -		24, üks punane uus kast			  2   "      -		25, kingseppa laua riistad			10    "      -		                                                   Ülekõige			267 rbl. 50kop.		
Selle ülewel nimetud waranduse wäärtuse raha on Ann Kantsik käest kätte saanud kokku kaks sada kuuskümmend seitse rubla ja 50 kop. Eduard Sõrason
Ann Kantsik annab üles, et on see waranduse wastu wõtnud ja raha äramaksnud Eduard Sõrasonile.
E. Sõrason   [allkiri]
Ann Kantsik  [allkiri]
Üks tuhat üheksa sada wiiendamal aastal Nowembri kuu 30 päewal on Krootuse Wallakohus ülewel seiswad lepingud, mis talupoja seisusest Eduard Katre poeg Sõrason ja Ann Danieli tütar Kantsik oma wahel teinud Krootuse Wallamajas kinnitanud, mis-juures Wallakohus tunnistab, et lepingu osalised kohtule isiklikult tutawad, ja et nendel seaduslik õigus aktisi teha ja et Ann Kantsik ja Eduard Sõrason lepingule alla on kirjutanud peale selle ette lugemise.
Eesistuja J Kuhi [allkiri]
Koht. M. Puna [allkiri]
   "     J Traat [allkiri]
1905 a 21 det ärakiri Sorason'le wälja antud ja 15 kop tembel maksu sisse nõutud.
Eesistuja J Kuhi [allkiri]
