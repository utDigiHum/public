Jaan Mikson kaibas, et tema ollew Laswa mehe Anz Rõemiga minewa kewaja ütte tükki hainamaa üle kauba tennu, ni et Anz Roem selle eest 20 rubla massap ja esi haina ära niidap ja ollew sesama 15 rubla ära andnu ja 5 rubla masmata jätnu, ent Anz Roem ei ollew seda haina ära tennu ja tema - Mikson - ollew haina pääle Jakobi päiwa ära tennu ja nüüd ollew Anz Roem selle temast tettu haina kuhja ilma temale ütlemata ära weinu, tema nõudwat kahjutasumist 20 rubla, hainu saanu 3 koormat.
Wastut Anz Rõem, et Miksoniga ollew täl hainamaaga 15 rubla lepitu olnu ja Mikson pidanu esi ärategema. Jaan Mikson and tunnistajas üles Wöörmünder Peter Pedras.
Tunnistaja Wöörmünder Peter Pedras 41 aastat wana Lutteri usku, tunnist, et Anz Rõemi naine Ann kõnelnu, et Jaan Mikson ollew 20 rubla küsinu ja nema ollew 15 Rbl. paknu, ja Mikson pidanu ütten Anz Roemiga maha niitma. Kui haina ärawedamise man teda manu kutsutu, lubanu Anz Roem 1 rublat pääle massa. Ka ollew Roem enne wõõra kuhja ära wedanu, arwata et seda kuhja tema ei ollew tundnu, mes Mikson tennu. Kindmat lepingut tema ei teedwat.
Nema es lepi.
Otsus:
Anz Rõem peap masma Jaan Miksonile haina tegemise kahju 10 rubla ja selle et tema Jaan Miksonile haina wedamist üles es anna massap waeste kassa 2 rubla trahwi 14. päiwa sisen.
See kohtu otsus sai kohtun käüjile kuulutedus prg.773 ja 774 S.r. 1860 a. ära selletedus. Anz Roem nõwwap Appellationi.
Pääkohtumees J. Kabist [allkiri]
Kohtumees P. Sillaots [allkiri]
Abiline J. Mikheim [allkiri]
Appellation Anz Roemile 6 Webr. 1887 wälja antu.
