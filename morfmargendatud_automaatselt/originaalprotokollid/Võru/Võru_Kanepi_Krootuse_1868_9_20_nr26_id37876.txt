Jusal, sel 20. Septembril 1868 olli Karraskij koggokonna kohhus koon, nimmelt: 
                                                                      Pä kohtomehhe assemel Jaan Leppasoon
                                                                      Kohtomehhe assemel Johan Mandli
                                                                                   do                    Adam Lukkatz
Mihkel Raig, Taniel Wärs, Jaan Mandel, Jaan Läwwi, Jaan Wõsso, Jaan Ehhin ja Juhhan Kuhhi tulliwa ette, kaiwasiwa: Et Hans Kirristaja, Jaan Lodus, Jaan Sõrmus, Juhhan Wähk, Adam Raig, Mihkli Warres ja Jaan Punna omma neide haina kuhja ja äddala nido päält ärra söötnu, ja ne ennenimmitet perremehhe es woi ka koggoni sallata, et na ei olles neide nido päle putnu. Et nüüd süggisenne aig, ja tinnawo ka ni suur äddal ei olle, mõist koggokonna kohhus: kelle karri neide kige eennimmitet perremeeste nido päle om putnu, massap eggaüts üts Rubla trahwi, se teep ülle summa 7 Rubla neile kahjotassomisses.
Sai ka ütteldus, kes rahho ei tahha olla, wõiwa prottokolli sada. Prottokoll om wälja wõetu 30. Septembril 1868.
                                                                                  Wallawannemb J. Tullus
                                                                                               Kirjotaija H. Undritz
