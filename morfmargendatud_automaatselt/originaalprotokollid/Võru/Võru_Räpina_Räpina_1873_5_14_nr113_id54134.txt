Jaan Norrusk Wõbsost kaibas kohto een et Michel Griggul om teddä riisno Räppinä kõrtsin edde kõrtsin ussemann toika läwweman ärrä om riisno Michel Jani käest1 Ur wäärt			10 rubla		1 kübbär			1 rubla		6 Rubla 90 kop selget rahha			6 rubla 90 kop		Summa			17 Rubla 90 kop		
Ur ja kübbär ollewa kätte sanud, agga rahha 6 Rubla 90 kop ei ollewa mitte kätte sanod
3 körd ollewa Michel ka Janil käegä lönod, ja ollewa malgaga ka ähwärtano pessä, ja seddä om kahjatseno et Uri jalla al katki ei olle lönod.
Uri ja Kübbäräd om Janil kätte otsman olno kohtumees Gustaw Tolmusk, nink Kristjan Rusand Atstawka Soldat Jaak Lepmanson tunnistas, et temmä om tee weren kulu, kui Michel Griggul ja Jakob Pedoson om teed mödä sõitnowa, ja om kulu kui Jakob Pedoson Michel Griggulil om üttelno, sinna kellä warras, ja päle selle, om Jaan Norrusk tulno, seddä samma teed mödä perrä, ilma kübbärättä ja Jaan Norruskil om su werregä koon olno, ni paljo et mitte ärrä es olle wõino tunda, kes se inniminne om. Ja kui Jaan temmä manno om sanod, om temmä küssino kust sa werrega kokko said, om Jaan üttelno, et kütti Michel om pesno.
Peep Zupsmann tunnistas kulu, kui Jaan Norrusk om MIchel Griggulil üttelno et anna Kello mo omma ärrä.
Michel Griggul om üttelno ütte malga päle et kui ma segä tõmba üts 3 jutti kae sis, 
Jaan üttel Eddimält om MIchel läbbi lubbano otsi perräst, ei olle laskno, ja ommawa Rautseppa Jakobiga Räppinä körtsimant ärrä söitnowa.
Ja Michel ollewa üttelno kui ma so läbbi lassen otsi massat sa 5 Rubla agga perräst ei olle lasno otsi.
Michel Grigguli käest sai küssito üttel temmä, et temmä ei tea middägi ja temmä ei ollewa Uri kübbäräd ja ei ka rahha mitte Jaan Norruski käest ärrä riisno.
Kohtumees Gustaw Tolmusk, ja Kristjan Rusand ütliwä et nemmäd om Uri Jakob Pedoski käest kätte sanod. Ja Jakob üttelno et Michel om temmäle Räppinä kõrtsiman andno.
Kübbärä on Jaan Norrusk Rautseppä Jakobi käest Räppinä kõrtsi 10 kop eest panti panto kätte sanod.
Rautseppä Jakob perräst jääs polele 21 Maini.
