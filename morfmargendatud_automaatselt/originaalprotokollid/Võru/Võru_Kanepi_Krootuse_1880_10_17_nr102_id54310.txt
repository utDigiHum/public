Kohto een kaiwas Jakob Parmen /Saarjärwelt/ et temäl sawikiwitegija wenelase Michael Kondratjew käest 54 Rbl. 10 kop. palka saada ollew. Palles kog. kohut M. Kondratjewi sundi sedä raha temäle ärä massa.
Et M. Kondratjew haige ollew, sis ei wõiwat temä mitte kohto ette tulla.
Joh. Soldan tunnistas, et M. Kondratjew ollew ütelnu et temäl Jakob Parmenile 53 Rbl. palka masmata ollew ja lubanu seda raha ärä massa kui ta saawat.
Otsus: Kohto mõistmine jääs tõises kohtopäiwas, kui M. Kondratjew ka üle om kulleldu.
