Räppinä mõisa wallitsusse polest kaibats et Wõbso mõtsa om 8 köiwo tohto ärrä korito walget koort pakso koort ei pea ollema hoito.
Mõtsawaht Writz Melberg tunnist seddä õige ollewad, ja üttel et Wõbso poisikessed om koorno.
Tõllasseppä poig Rodi Parnitze käest sai küssitu üttel temmä, et temmä ei olle mitte kõiwo koorno, Wõbso karjus Hans Moddi poig Jaani käest sai küssitu üttel temmä et temmä ei olle mitte kõiwo koorno.
Julius Jaanson tunnistas, et eggä nemmä es kori nemmäd wõttiwa lipnid kõiwo küllest Androsse Edi om nänno.
Selgembä tunnistusse perräst jääs polele.
