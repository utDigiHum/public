Karraskij Kogokonna kohhos sel 18 augustil 78
                                               Mann olliwa  Päämees Jaan Tullos
                                                  do       do    Kohtomees  Jakop Uibo
                                                   do       do teine    do      Jaan Leppäson
Tulli ette mõtsawaht Hendrik Tuhhand ja kaibas et Adam Wähk ja Michel Kärner, omma ilma teedmada Asso lande lännowa ja sääl hakko kokko pandnowa, nink hao kändmisse tarbis pikki kõiwo witso raggono ja noore raondo pääl ommi hobbesit söötnowa, ja tee mes kin(n)i olli panto wallale tennowa nõudis tolle eest kolm rubla kahjotassomist. -
Sai ette kutsotus Adam Wahk nink Michel Kärner ja üllewanne kaibus neile ette loetus, kes küll eddimält püüsewa sallata agga wimata ärra tunnistassiwa. -
Otsus  Kohhos mõistis et süüd ja kahjo mitte suur ei olle masswa kumbgi 25 kop mõtsawahhile wälja mes kattessa paiwa aja seen peäp masseto ollema. -
Sai kohto mõistminne ja § 772-773 kohton käiadelle ette loetus ja olliwa sellega rahho. -
                                                                        (Allkirri)
