Joh. Moistus tulli ette ja kaibas: tema wars olla Peter Moistusega mõtsast tullen ütten joksnu ja tulnu kuni Hurmi mõisade, kos siis tema warsale kas pessmisse ehk muu wia läbbi haiget om tettu ni et tost ajast nüüd wars põdeb. Gusta Org ja Jaan Nuuma oma warsa kiniwõtmisse eest 1 Rubla wõtnu.
Kusta Org ette kutsuti ja selle ülle küssiti wastut: wars olla mittu kõrd neide willan sõkno ja wähherdanu ja selle oma nema kiniwõtnu.
Nisama ütlep ka Jaan Nuuma.
Leppisiwa henda wahhel ärrä.
Gusta Weits XXX
Adam Org XXX
Kusta Kolk XXX
Kirjotaia assem: G Daniel [allkiri]
