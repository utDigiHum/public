Kolmandalt tulli ette Jaan Just ette ja kaibas et Krotuse mõisa rentniko Tõnnis Neissar ja Jakop Jannson obbese temma 6 akki rükka ni ärra lahkno et üttest akkist ennamb middägi saija ei olle sedda wõiwat tunnistada Jaan Riit ja Hinn Trath keä omma nänno et neide obbese minno akke pääl ommawa sööno. -
Kutsoti ette Tõnnis Neissar ja Jakop Jannson ja saije Jaan Just kaibus ette loetus tolle pääle utliwa mõllemba mes perrast sis Jaan Just meie obbesit henda pööle kinni es wi sääl om ka wast muid obbesit ennamb olno kui meie obbese. -
Kutsoti ette tunnistaja Jaan Riit kes ütles minna nägin kui et Nonnis Neissar ja Jakop Jonnson obbese Jaan Justi akke pääl seiwa.
teine tunnistaja Hinn Trath kes ütles minna ennegi kulin kui Jaan Riit ütles Jakop Jannson tütrele teie obbene om Jaan Justi röa akke pääl minge ajage ärra. -
Otsus  Karraskij kohtomees Michel Wõsso ülle kaemisse pääle et 6 tublit akki rükki kos kige wähhamb 3 wakka rükki olles saano ja nüüd ennegi pool saaki woise arwata moistis kohhos et Nõnnis Neissar ja Jakop Jannson maswa ülle katte 1 1/2 üts ja pool wakka rükki Jaan Justile 8 päiwa sissen wälja. -
Sai kohto Otsus ja § 773 ja 774 kohton käiadelle ette loetus ja lätsiwa ilm sõnna lausomada wälja. -
                                                             (Alla kirri nago üllewal)
