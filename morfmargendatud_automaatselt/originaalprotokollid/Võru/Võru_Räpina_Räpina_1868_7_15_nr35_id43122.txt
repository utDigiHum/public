Jakob Kuns tulli kohto ette ja kaibas et poig Prits ja poig Gottli temmäl middägi toidusses üllespiddimisse tarbis ei anna.
Kohhus mõistis et poig Prits peab 12 leisikat leiwä jahho eggä aastas wanna essa Jakob Kunsil andma, se om kuu päle 1 pund ja se moon jääs anda ni kauwa kui Essä elläb.
Poig Gotli om mona andmissest ni kauwa pri kui Essä temmä pool elläb agga siiski peab temmä ka iks leiwä korwalissega andma ja awwiatama.
Pä kohto mees Paap Kiudorw
Kohto mees Jakob Türkson
Kohto mees Hindrik Toding
Kohto mees Kristian Wössoberg
