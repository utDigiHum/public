Man ollid  Peakohtomees   J. Roht
     "           Kohtomees    M. Wõso
     "                  "                J. Mandel
Ette kutsuti Jaan Seeme laste wöörmünder Joosult, nink sai tema käest noutud, kas keik se kraam ja warandus, mis Jaan Seeme laste hääs sai üles kirjutud, nink tema laste ja ülewaatamise alla sai antud, liikumata ja keik alal om. mis peale Jaan Seeme ütel. "Keik se kraam ja warandus om terwe ja liikumata, puudus ei ole midagi, enemb om majan weel paljo kraami muuks muretsetud, selle eest wastutan mina.
Otsus. Seda Jaan Seeme ütelust tähele pandus ja peab Jaan Seeme kutsumise Raha 30 kop. ära masma.
Allkirjad
Kirjotaja A. Muhler
