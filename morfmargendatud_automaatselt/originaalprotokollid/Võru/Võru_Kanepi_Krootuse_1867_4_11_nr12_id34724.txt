Karraskij mõisan, sel 11. April 1867. olli Koggokonna Kohhus koon, nimmelt:
                                                      Walla wannemb Jaan Tullus
                                                      Pä Kohtomees Michel Raig
                                                      Abbi      do        Karel Grünthal
                                                                    do        Johan Mandli
 Mihkli Tullus ja Jaan Leppasoon tulliwa ette, sai Kihhelkonna Kohto mõistmise perra, ja kässo päle mõllembile täht antus selle päle, kuis Mihkli Tullus Jani kõrwal elladen peap ollema nink mes ta peap sama:
                                                  Ütte wakka ma keskmist rükka
                                                  Ütte wakka ma      -          sitta
                                                  Ütte wakka ma Tõu maad,
nink hendale ja ellajalle korter seni kui ta ellas andma; ent Mihkli Tullus peap koddapoliko wisil allandlikkult ellama, ja eddespidi ma eest middage tassoma.
