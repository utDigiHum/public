Lepping. Nr 37.
Täämbatsel päiwäl sel 10 Juli 1869 om Peter Leppa ja Jaak Kulla wahhel sesinnane al nimmetu lepping sündinu:
1. Peter Lep annap üts tük maad Huusmanni werest, ja ütte wakka alla haina maad 12nes aastas rendi päle Jaak Kullale.
2  Eddimänne aasta ei massa Jaak mitte rendi, ent üts teiskümme aastat, ja igga aasta ütte wakka ma päält kats rubla renti, olgu põllus tettu, ehk ka weel teggemata. 
3. Sedda maad prugip Jaak nidä: küttusse keswä päält wõttap kolm wilja, ja röa päält wõttap temma kats wilja, ent perrun neid wiljo pannep temma sitta.
4. ütte tükki /üts wakka alla haina maad/ haina maad saap Jaak Kulla ka 12 aastas ilma rentida, ja selle haina maad weren om üts tük paijo, mes Jaak peap puhhastama, ja se jääp ka selle niduga ütte ajani temma kätte. 
5. Kik terwe Huusmanni Niit piap Jaak kolme aasta sissen paijust puhtas teggema, nida et wikkat wõip käwwü, ja selle nidu perrä keerminne jääp Jagu päle. 
6. Käe rahhas om Jaak Kulla, Peter Leppile andnu, Wiis Rubla. 
Rendile andja Peter Lepp +++
Rendile wõtja Jaak Kulla +++
