Et Wallawanem Kogukonna Kohtule etteandis endise Kirjutaja Jaan Zupping warandus Kinnipanda selle pääraha wastutamise ette mis tema inimeste käest wastu wõtnud ja mite Wallawalitsusele ei ole äraannud. Et tema ka Kooliõpetaja oli ja maasaagise palga sai siis ka talwese lastekoolitamise tarwis tema warandus kinnipanda, siis sai tema isa Jacob Zupping ettekutsutud kes siin wallas kui Kohaomanik ja temale ettepantud, kas ta laseb oma poja warandus Kinnipanda ehk lubab tema eest nende eespool ülesantud Wallawanema nõudmiste eest oma warandusega wastutada.
Jacob Zupping lubab oma poea Jaani läbi wastuwõetud pääraha eest, eest, mis wallawanemale wäljaandmata jäänud niisama ka eestulewa talwe laste koolitamise eet oma warandusega wastutada ja palub oma poea Jaani Kraami seepärast kinnipanemata jätta.
Seda lubamist ilmutab Jacob Zupping oma käe allkirjutamisega
Jakob Zupping
Otsus
Et Jakob Zupping oma poja eest wastutada oma warandusega on lubanud, siis tema poja kraam kinni panemata jätta.
PääKohtumees Johann Nilbe [allkiri]
Kohtumees: Johann Aintsi [allkiri]
d.: Adam Lukk [allkiri]
Kirjutaja asem. M Bergmann
