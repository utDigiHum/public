Widrik Parmson tulli kohto ette ja kaibas et temmäl Perrosnitsa Tura perrä nido päält 1 kuhhi haino ärrä warrastedo 1 kormalinne kuhhi et Ado Karopun Moisikatzi mees enne seddä om Kidriko käest seddä haino kuhja noudno siis kui nemmäd ütten Ado naise Essä henä kuhja om wiedano selle arwamisse perrä om Widrik länno Mosikatzi Ado pole haina promi perrä taggan otsma ja tahte wõi tunnistust om Widrik leidno Ado ree jälge mõõtmissest mis mödoga ütte om sündino ja haino promi om Widrik ka leidno Ado majast ja ka kuhja pää malgad om Widrik leidno Mosikatzi Kohtomehhe Seppa Märdiga Ado majast. Köiwo puust Mosikatzi kohtomees Seppä Märt tunnistas et ree jälgi temmä ei olle mõõtmisse perrä õiges arwano. Kuhja malgad tunnistas Märt tähhe perrä Widriko selletämisse kõne perrä õige leidno ollewad.
Ja kohtomees Märt om ka haino promi öige ja ütte sündlikko leidno ollewad. Ja Adol om ka haino olno mis promiga ütte ei olle sündino.
Ja Ado Karopun om ka ähwärdäno kohtomees Märdil logega lüwwä.
Ado Karopunni käest sai küssitu üttel temmä et minna ei olle middägi haino kuhja warrastani ja neo hainad olliwad minno omma Wisli so hainad mis promis kaeti.
Ja neo kuhja pä malgad om minno omma koplist raggoto ja olliwad mul omma kuhja pääl köiwo puust 
Widrik nöudis omma haina korma eest 6 Rubla.
Kohhus möistis et Ado Karopunn peab 6 Rubla ärrä maksma haino eest Widrik Parmsonil ja peab weel 3 Rubla maksma kohtomehhe trotsmisse eest Mosikatzi waeste ladikus ja 3 Rubla Räppinä ladikus peab maksma wargusse trahwi.
Pä kohto mees Paap Kiudorw
Kohto mees Jaan Holsting
Kohto mees Kristina Wössoberg
Kohto mees Paap Kress
Kohto mees Hindrik Toding
