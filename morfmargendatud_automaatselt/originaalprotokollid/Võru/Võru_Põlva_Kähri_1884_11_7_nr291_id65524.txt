Jaan Raig tulli ette ja nõuab Kusta Nikopensiuse kadunud welle Johann Raig wõlga 4 rubla.
Kusta Nikopensius wastab: 4 rubla olin mina kord kül temale wõägu aga kadunud Raig käis minu hobusega 2 korda Tartus ühes minuga see eest nõuan mina			1 rubla; 		ühe sae laua andsin temale			-     "     80 kop.		2 mõõtu keswi			2     "       -		1 kord hobusega Wiiral käinud			2     "		                                   Summa			5 rub. 80 kop.		
Selle järele on minule 1 rub. 80 kop kadunud Raig käest saada ja et see õige on seepääle palun mina enast wannutada saada lasta.
Otsus.     Keiserliku Kihelkonna kohut paluda Kusta Nikopensius see pääle ärawannutada.
