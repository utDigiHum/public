Ette tulli Michkli Assi nink kaibas: tema olla ene Jõulo pühhi 5 wakka keswi surmis tetta wiinu Tille weskile ja om kats kõrd allan käunu ja ei olle kõrda saanu nink kui kolmat kõrda om alla weskile länu sis olla 3 wakaline keswa kot ärra kaonu ja om enegi katte wakkaline sääl ollu, nink ne kats wakka olla tema ärrajahwatanu ja Möldre olla temale üttelnu: ooda edespidi kui toisi kotte ärräwijas waest jääb siis sino jaggu mahha, nink kui tema nüüd olla seeni ajani ootnu, agga kotti ei olle perra jäänu nink ei tahtwad Möldre ka sest asjast enamb midagi teeda, siis pallep tema Kogokona Kohut sedda asja selleta ja lubbab Möldrega leppi, kui Möldre temale 2 wakka keswi annab ja ütte mõõtu suurmit, mes temal Möldre käest ene lainatu olliwa.
Möldre Peter Kowit ette kutsuti kaibus ette loeti kes wastut: Michkli Assi tõije kül omi keswi surmis tettanink olli ka tõisen kottin arwata waest 3 wakka, ja kui Michkli Assi kats kõrda olli allan käunu ja kolmat kõrda tulli siis ütli mina: ooda, homen saat weski kõrra, agga Michkli es ütle endal aiga ollewat ja küsse mino käest ütte mõtu suurmit seeni kui jahwata saap nink läts ärrä ja nüüd ma ei tiija sest kottist midagi ei massa ka wälla.
Kui neil midagi ütlemist es olle selle asja polest siis
Mõistse Kogokona Kohus:
Möldre Peter Kowit massap Michkli Assile 2 waka keswi ja selle mõtu suurmit arwab tassa mes Assi tema käest om lainanu, selleperrast et Möldre peab egga weskilisse kotti teedma.
Moistmine ja Sädusse §§  §§ 773. ja 772 saiwa kohtunkäüjile etteloetus kon Peter Kowit kuulut et tema selle otsussega rahhule ei olle ja palles Protokolli wälja, mes temale es keelda, nink sai temma Opusse lehhe.
Protokoll es sa ärräsadetus selle et Päkohtomees tulli ja üttel et Peter Kowit ja Michkli Asi ärrä omma lepnuwa.
G. Weits xxx
Ad. Org xxx
Kust. Kolk xxx
Kirjotaia : G .Daniel [allkiri]
