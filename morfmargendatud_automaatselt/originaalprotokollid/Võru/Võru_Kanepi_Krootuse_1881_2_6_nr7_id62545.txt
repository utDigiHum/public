Tulli ette wallawannemb Michel Wõsso nink kaibas, et temmäl kadonu minnewa süggise ütte obbese kapla pandmisse ahhila ärra, ja om nüüd kulda saanu et Jaan Kebja ollewat Walge moisan Jaan Raigil ütte ahhilit müwwa paknu kos ka minno sulla-
sel Kusta Prüüsile Walgemõisa rentnikko poig Jaan Raig ollewat üttelnu et Jaan Kebja pakse mulle ütsi ahhilit müwwa, ent minna es tahha neid mitte wõtta se ollo Jaan Raig üttelus Kusta Prüüs wasta, pallel kohhot et Kusta Prüüs ja Jaan Raig saas ülle kulleldus. -
Sai ette kutsutus Kusta Prüüs nink kohto poolt tem(m)ä käest küssitus mes tem(m)a tijap sest ahhila kaomissest tunnistada kes nida samma ütles kui temä perremees Michel Wõsso ülles andis. -
Ent Jaan Raig ja Jaan Kebja es ollewa kohto ette tulnu jäije kaibus tõise kohto päiwa pääle selletada
                                                Karraskij Kohtomajan sel 6 webruaril 1881 
                                                                         Pääkohtomees  J. Tullus
                                                                         Kohtomees  J. Leppäson
                                                                          toine  do  S. Raig 
