Johann Raig tuli ette ja andis üles, et temale on 10 rubla wõlgo Karl Nikopensiuse käest saada ja palub seda wälja nõuda. 2 rubla olla tema juba ära maksnud.
Karl Nikopensius ütleb selle pääle 10 rubla olen mina kül laenanud 2 rubla olen ma juba äramaksnud ja 3 rubla eest on tema minu käest maad rentinud.
Johann Raig ütleb et tema maad mite rentinud ei ole waid Karl Nikopensius olla temale see maa muidu annud.
Mõistus. Kogu konna kohus mõistab, et Karl Nikopensius peab 6 rub. 50 kop. Johann Raigle 2 nädali ees wälja maksma 1 1/2 rubla nõudmine saab maarendi ette arwatud, mis Raig Nikopensiuse käest 1882 aastal pidada on wõtnud. 1 1/2 rubla rendi nõudmine Karl Nikopensiuse poolt saab tühjaks mõistetud.
Otsus sai T.r.s.r. §§ 773 ja 774 järele etteloetud. Raig ei olnud mite otsusega rahul. ära täidetud. 
