Kaiwas Jaan Rummask et Josep Bessakonow ollewat temma pä lahki lönu selle et temma Jaan ollew temma Josepi salli ärra lahknu ja tedda rinnust kinni wötnu Selle näggija omma Hindrik Parson ja Kustaw Kübbar
Ette kutsuti Josep Bessakonow nink tunnist kül löwat Jaan Rummaskile, ent nemma omma tedda enne omma essa Mihkli Rummaskiga kattekesse pesnu selle näggija olnu Josep Urgard
Ette kutsuti Josep Urgard nink es tija middaki tunnista õigete nink tunnist töisilde wölsi kui nemma essi kakleja
Omma wastastiko keklemisse omma ärra lepnu henda wahhel; Ent kaklemisse eest päleki et pühhapäiwal Moistis kohhu et kumbki saap witsa trahwi alla, nida et Josep Bessakonow 10 witsa löki saap, nink Jaan Rummask saap 15 witsa löki ent ette tulnu wöls tunnistaja Josep Urgard et ilma näggemata om tunnistajad lönu 5 witsa löki et temma ei olle middaki töttet tunnistama ja päleki essi ilma kutsmata ette tulli.
Päkohtomees Gustaw Lepson
Kohtomees Paap Sabbal
Kohtomees Josep Jöks
