Tulli ette Juusa Jakob Uibo nink kaibas et temma karja pois Jaan Rattus ollewa jälle ärra paggeno, sest kui tedda eespäiwa pääle kohto mõistmisse taggasi toodi sis olli Jaan Rattus wäega haige ni et suure abbi läbbi talli pääle ülles sai, ent kui meie mõtsa ärra lätsime sis olli terwe ja paggeno jälleki ilma näggematta ärra ja se om jobba kolmas kõrd et ärra pakkep, se paggeminne ollo pääle lõunat arwata kella wiie aigo tolsammal päiwal. -
Nõudis et ni mitto kõrda om taggasi sunnito teenistuste ent jalleki ärra om länno egga temmast iks ennamb teenijat ei sa sis 
nowwän temmä kaest leppitut ajastaja palka. -
Palgas olli leppito 15 wiistoistkumme rubla selget rahha üts paar saapit mes kats rubla piddiwa masma üts wak kartohwlit mahha nink üts rubla käerahha. -
Sai ette kutsotus Jakob Rattas poisi essa nink üllewanne kaibus ja nõudminne ette loetus kes mitte sõnnakest es lauso nink läts kohto tarrest wälja kos sis se assi tõise kõrra pääle jäije. -
                                                                     Pääkohtomees Jaan Tullus  XXX
                                                                      Kohtomees Jaan Leppäson  XXX
                                                                    teine      do   Samuel Raig  XXX
