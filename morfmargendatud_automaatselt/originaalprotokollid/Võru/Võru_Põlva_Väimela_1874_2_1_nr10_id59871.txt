N 10 Waimala Kog. Kohton sel 1 Februar 1874
Pakohtomees Peter Pichl
Abbi Michel Traks
dito Jaan Waiso
Moisawallitsus om kaiwano Waiso moldri Alexander Krisa päle et temma om saatno süggise wilja weskiwwile jahwata, ja om taggasi tuwwen üts tawwelik wak röa jahhu kalo perra patus olno. ja temma nowwap nüüd Moldi käest omma kahjo. 
Sepäle om Moldri Alexander Krisa ette tulno ja om wastust andno: et monamees Johan Silm om tulno ka toll kõrral weskiwwile, olli jahwata 1 wak rukki ja 3 wakka keswi, ja sepäle om temma länno kaema ja om laidno et Johann Silm jahwat 2 wakka rükki ja sis om ta ka warrastano.
Juhan Silm om sepäle kostno, se willi ei olle mitte olno tol ajal kaloto kui weskiwwile sadeti ja ka warrastedi, ja temma ei olle mitte neid rükki warrastanno. Sis küssiti temma käest, kas temma om üttelno Moldrile 1 wak rükki ollew olno ja temma om üttelno kül Möldri sedda ütlew, ja neide rükki seen ei olle mitte Waimela moisa rükki olno, enge se ütte wakka rükki om temma enne weskiwile weno ja toise perra komba temma Moisawallitses jahwatasse ütten weije, ent sedda temma mitte Möldrile ei olle ölno;
Sepäle om weel küssito kas Johan Silmal olli ütta wakkalisi kotte ja om kostno: temma om Samul Jurgensoni käest wõtno sola pandu, ja toise ka ütten.
Sepäle om weel Alexander Krisa tunnistano ne olli tõttelikult Moisa massinaga pestu röa mes kolon olli kui temma jahwat.
Ja misperrast es naida temma Möldrile et temma ka tõise wakka rükki perra tõije. Ja temma om sedda kolm kõrd Möldrile üttelno et tal iks uts wak rükki om jahwata.
Selleperrast et Johan Silm om Möldrile üttelno et tõl iks uts wak rükki jahwata olli ja matti temmal massa es olle, ent Kats wakka rukki om jahwatano sis om Moistus:
Johan Silm massap 2 Rbl 50 Cop Moisawallitsussele kahjo tassomist ja 50 Cop waeste Ladiko trahwi.
§773, § 774 Kal.
Päkohtomes Peter Pihl [allkiri]
Abbi Mihel Traks [allkiri]
dito Jaan Waiso [allkiri]
Kirjotaja JKangro [allkiri]
