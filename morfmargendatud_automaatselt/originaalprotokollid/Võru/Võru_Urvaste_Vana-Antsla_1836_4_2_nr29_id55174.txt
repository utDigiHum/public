Sell 2sell Aprillil 1836 Kaibas Willago Samo et Kerner Meister Kintan om egga Aasta Ea Keldre päle ütte Ankro miss 5 wai 6 Panki suur om ollnu, weno.
Siis tunnistass Samo et Meister wõttab igga kõrd kui Hähsti Wina tulleb sealt mant ja oijab Ankrun, ja kui temma ütte Wadi Wina aab siis panneb taa sest korjatust Winast manno, ja nõstab selle läbbi enda Wina Saki.
Temmal om tännawo 16 wai 17 Pangi sedda korjato Wina ollnu.
Kui Pruli pallatawa Wina, siis kui hä om ütleb Meister et see om temma kedeto, kui om Halw, siis ütleb Meister et om Prulide kedeto.
Eea Keldre pält om Meister andno Niluse Tannilale, Matto Karlile, Iwaski Samule, ja Willago Samule püttika Wina.
Läbbi Ea Keldre Kattuse, seal om üts mulk olno tetto, Meistr Mehise Aida, ja Sealt Meister om ülle Aia, kuss minnewal Aastal Herne olliwa, - neile selle Wina anno, ja om neile Kelano, et na nii peawa minnema et Schumacher (Wallitseja) neid ei peas näggema. 
