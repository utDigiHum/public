N 3. Waimela kogukonna kohus sel 24 Aprillil 1886
Man oliwat Peakohtumees Johan Sulg kohtumees Jaan Pihl kohtumees Michel Runthal
Ette tulliwa Adam Rikkand ja Michel Runthal mõlemba Waimelast ning palsiwa esi hinda wahel tehtu lepingut kohtu poold kinnitada, nin kui siin al seissap.
1. Se maajago mees Jaan Kukke käen rentil oli annap Michel Runthal, Adam Rikkantil ühe aasta pääle rentile 23 Apprill 1886 kooni 23 Apprill 1887.
2. Adam Rikkand massap selle maa eest aastast renti 140 rbl (sada nellikümend rubla) ettimäts poole renti massap rentnik 1 Aprillil, 70 rbl. ja tõine pool renti 1 Octobril 70 rbl.
3. Se hainamaa mis Jõrresoon om ei sa mitte rentikule antus. 
4. Rentnik ei pea mitte ommi elajit säll rentitul majan ei suwel ei ka talwel, kui rentik tööd teep woiwa hobese sääl peetas sada kas wõllan ehk tallin sööda. 
5 rentnik saab kats tükki hainamaad hain saab ütten tettus ja pooles woetus, pooles woetud heina woib rentnik majast wälja wija nii paljo kormad kui rentnik jaina wälja wiib majas anab temma ega koorma päält ütte puuda luujahu perremehele koosta. 
6 Nii paljo kui ristik haina om külwatud saab tukeki wiisi pooles mõõtetut, ja eralde tettus.
7 Rentnik peab nurme piddama kolme wakkala lenna tegema. Karthowlit wakkamaa siddaga ja tõine wakkama ilma sittata
8 Walla orjuse [mahakriipsutatud: kõig täitab Rentnik] tea sädmine pooles kogukonna ja kroonu magasi maksut massap rentnik poole
9. Põhku ei tohi rentnik wälja wija nii kui Tallorahwa sätusen seissap 1860 astast
10. Linna woib rentnik põllu päält ärä wija ja Linna luud hendale wõtta selle eest annab rentnik ütte puuda luujahu wasta.
Peakohtumees J Sulg [allkiri]
kohtu mees J. Pihl. [allkiri]
kohtumese 
Kirjotaja ASaremõtz [allkiri]
