Ette tulli Serreste Herrä oman kaibusen Jaan Awwald wasta, aida ja pinnu ja aida lahkmise peräst nink ka usse henge ja konksi olewad ärä kakkutu. Ja ka tunistaia, Jaan Leib, Jakob Järw Peeter Raud. Tasomist nõwab Herrä pino puije eest 2 rubla, raudo eest 2 rubla.
Jaan Leib tunistab et Praksi maia usse olnuwa kõik tõiste kui enne, pääle tuud olewad üts wallimäne aid, kos ene olnu, puudus, muud ei olewad kui saiba. Tuu kõrd kui us-aida tettu, andnu temä Herrä käsü pääle nii paljo aija puid kui kullunu.
Jakob Järw tunistab ka et aida om kül puudus.
Peter Raud tunistab et aida om kül puudus, pääle sedä olewad usse käe-rawa ka ärä kaonuwa.
Jaan Awwald oli ene, sel päiwal een, enegi oli jo ärä lännu, jääb see kõrd poolele.
