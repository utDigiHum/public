Ette tulli Jaan Siidra ja kaibas, et Jaan Hagiwang, Petri poig tema niidu, mes tema Peter Tamme käest henele rentnu, üle piiri ära niitnu, mink eest tema 6 Rbl. kahjo tasumist nõudwat.
Wastut kaibuse pääle Jaan Hagiwang, et tema Peter Tamme lubaga seda nimitedu haina maad ära tennu, kelle käest tema seda henele rentnu.
Kahjo ollew üle kaenu kohtumees Peter Sillaots.
Kohtumees Peter Sillaots, kes kahjo üle kaenu, üttel, et kahjo olnu arwata ütte roa osa hainu hinna perra 1 Rbl.
Otsus:
Peter Tamm ette kutsu.
Pääkohtumees J. Kabist [allkiri]
Kohtumees P. Sillaots [allkiri]
    dito          P. Klais [allkiri]
