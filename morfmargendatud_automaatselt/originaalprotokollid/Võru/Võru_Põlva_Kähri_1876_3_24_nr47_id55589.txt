Ette tuli Johann Lepwalz ja kaibas et Johan Waks ei laskwad temäl enämb temäl oma aija maad ei ka usaija kottust pruuki, ja tahab oma aija maad weel üts aojustaig pruuki.
Johann Waks ette kutsutu ja see kaibus kuulutetu ütlep: et temä tahab esi oma kontrahti perrä oma maad oma piire sisen pruuki niisama ka Johan Lepwalz wana aiamaad.
Otsus: 
Kogokonna kohus mõistab walla lepingo perrä et Johann Lepwalz wõib oma wanna aiamaad ja usaija kottust pruuki, enegi temä peäb sitta pandma. Enegi selle wasta peäb Johann Waksile maad wasta andma.
Otsus kuuluteti §  773 ja 774 perrä T.S. Johanna Waks and üles et temä otsusega rahul ei ole.
Pääkohtomees Andres Nikopensius
Kohtomees Peter Nemwalz
Kohtumees Joh. Nikopensius
do: Jaan Leib
