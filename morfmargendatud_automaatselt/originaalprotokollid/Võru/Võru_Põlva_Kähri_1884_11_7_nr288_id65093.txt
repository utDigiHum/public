Tõdu wallast Juri Hinzer tulli ette ja andis üles et temale on Kährist Johann Hämalase käest 3 1/2 rubla raha saada ja palub seda wälja nõuda.
Johann Hämalane Kährist wastab: Mina ei tää mis raha ehk millas olin mina temale wõlga jäänud.
Hinzer andis ette, et 2 1/2 rubla olla tema Hämalasele põllu päält ostetud kartohwli eest maksnud 1 rubla olla tema laenanud. Pärast olla aga see ostetud kartohwel Hämalasele jäänud, sest et tema osast oli säält ise pääle müümise ärapruukinud ja tema neid enam selle raha eest ei ole wõinud wastu wõtta.
Johann Hämalane ütleb seda kül nõnda olnud olewad. Ei luba aga Hinzerile mite seda raha maksta, sest et Hinzer olla temale Kükka kõrtsis selle raha annud, kus Hinzeri seltsimehed pärast jälle selle raha tema käest ära on wõtnud.
Hinzer wastab selle pääle: see raha sai tema käest kül kõrtsis ärawõetud, sest Hämalane oli kõigile rehnungi teinud ja kui kõrtsimees temalt raha küsis siis ütles tema et raha olla ära warastud saanud. Seepärast otsiti Hämalane läbi ja leiti raha ühes kottiga saapa seest, kus siis kõrtsimees oma wõlla, mis häste wastse kui wana tasa tegi. Kas säält üles jäi wõi mite seda maa ei tää.
Hinzer andis tunistajateks üles: Johann Punak Kährist Jaan Paabo Wana Pranglist.
Johann Hämalane üleb et tema ei ole mite Hinzeri käest 3 1/2 rubla saanud waid üksnes 2 rub. 50 kop.
Hämalane palun äraleppi ja lubab poole sellest summast äramaksa (lubab 1rub. 50kop. äramaksa).
Johann Hämalane ei wõtta neid Hinzerist üles antud tunistajaid mite wastu sest et need tema raha käest ärawõtjad on olnud.
MÕISTUS.   Kogukonna kohus mõistab et Johann Hämalane peab Jüri Hinzerile 2 rub. 50 kop. 2 nädali sees wäljamaksma 1 rubla nõudmine saab tühjaks arwatud sest et selle üle mingisugust tunistust ei ole.
Otsus   sai T.r.s.r. §§ 773 ja 774 järele kohtuskäijatele etteloetud. Olid mõlemad otsusega rahul. 9mal Jaanuar 1885 Hinzerile G. Nikopensiuse läbi 2 rub. 50 k. ärasaadetud.
 
