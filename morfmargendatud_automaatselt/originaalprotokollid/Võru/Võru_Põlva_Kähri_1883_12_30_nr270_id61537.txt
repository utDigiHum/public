Jaan Hindt tuli ette ja andis üles et temale on 2 rubla 60 kop. Kersna Piiri käest saada ja palub seda kohut wälja nõuda 1 rubla 50 kop eest on mulle kwitung wastu.
Kersna Piir wastab minule ei ole  temale kopikutki maksa, mis ma wõtnud olen olen ka ära maksnud.
Jaan Hindt andis kwitungi ette, mis tõeks tunistab et temale 1 rub. 50 kop. Kersna käest saada on. Kwitungi tunistab Kersna Piir õigeks ja ütleb et ta olla teda äraunetanud.
Mõistus. Kogu konna kohus mõistab et Kersna Piir peab 1 rub. 50 kop. 2 nädali sees Jaan Hindtile wälja maksma.
Leppisiwad oma wahel ära nõnda et Kesna Piir maksab 2 rub 60 kop 1 Martsil t.a. Hindile wälja.
