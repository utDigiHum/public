Peep Zernand Jama külläst kaibas kohto een, et Paap Lepman om Räppinä kõrtsin kõnnelno et Peep Zernand ollewa ilma asjanda seddä rahha kübbärä eest masno ja et se kübbär peab parilla rikka mehhe nagla otsan ollema.
Se kübbär kelle eest Peep Zerna rahha 8 Rubla om masno ollewa Werriorralt Johan Praggi kübbär olno.
Johann Karjus Soldat tunnistas et keskmisse põhhi om Paap Lepman kõnnelno et temmä om selgest nänno kes selle kübbärä om ärrä wõtno, ja üts mees om tulno ja om wõtno, ja sis üts kolmas om tulnu ja om kübbärä ärrä wõtno ja silu alla pandno, ja ärrä winud ja peab parilla nagla otsan ollema ja rikka mehhe käen.
Ni samma tunnist Peter Wõikow kulu kui Paap om kõnnelno
Paap Lepmann üttel et temmä om jobno olno selle perräst om temmä ni kõnnelno ja ei tea kuis temmä om kõnnelno.
Tunnistusse perräst jääs polele
