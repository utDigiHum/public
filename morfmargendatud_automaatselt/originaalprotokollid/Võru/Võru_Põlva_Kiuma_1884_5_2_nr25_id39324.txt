Ette tuli siit mõisa pois Joh. Wank ja kaebas, et tõine mõisa pois Jüri Renn olla teda mööda-sõites hobusenarrijaks sõimanud, mida tarwilisel korral ka K. Lentsius wõida tunnistada. Ta nõudis süüdlase seaduslist trahwimist.
Jüri Renn wabandas ja palus, et tema seda mitte teutawal, waid naljakombel olla ütelnud ja ei ole selle juures koguni mitte nimelt J. Wanke peale tähendanud.
Niisama tunnistas ka Karl Lentsius, nagu J. Renn ütles ja et see ütelus peale seda küll nagu Wanke kohta käind.
Otsus:
Jüri Renn noomitada, et edaspidi mitte enam nii ropuste ei ütle, mis ka kohe täidetud sai, kuna kaebduse alune ennast sellepoolest ka tõesti parandusi lubas.
Seaduslikul wiisil kuulutatud.
Kopia 7. Mail antud
Pääkohtunik: J. Nilbe
Kohtunik: H. Lukats
d.: J. Aripmann
Kirjut.as Zuping [allkiri]
