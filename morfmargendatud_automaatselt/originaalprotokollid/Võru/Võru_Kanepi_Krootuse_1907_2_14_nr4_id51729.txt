Ilmusid ette Krootuse wallast talurahwa seisusest Mari Rattus Jakobi tr ja tema poeg Johan Rattus ja palosid Leppingu raa matusse kirjutada, et Mari Rattus annab tema peralt olewa lina lakk ruun hobuse pääle 12 a wana ja kaks kümmend kaks rbl wäärt (22) poeg Johan le määra mata aja pääle tarwitada, sellega et wiimane seda hobust igal ajal Mari Rattus le tema nõudmis pääle kätte andma peab, nimetud hobune on 4 weebr. 1907 a Wõru linnast küündla päewa laadast ostetud
Johan Rattus [allkiri]
Mari Rattus ei oska kirja tema palwe pääle kirjutas alla
Johan Toom [allkiri]
Üks tuhat üheksa sada seitsmenda mal aastal weebr kuu neljal toistkumn edamal päewal on Krootuse wallakohus - Krootuse walla majas eesseiswat suu sõnalist leppingut mis talupojad Mari Rattus ja Johan Rattus on oma wahel teinud, mis juures walla kohus tunnistab, et leppingu osalistel seaduslik õigus on aktisi teha, et nemad, kohtule isiklikult tuttawad, ja et peale selle leppingu ette lugemise temale Mari Rattus ja Johan Rattus oma käeg alla kirjutanud on.
Kohtueesistuja J Kuhi [allkiri]
Koht. J Traat. [allkiri]
koht. D Paloson [allkiri]
