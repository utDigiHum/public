Friedenberg kaibas wasto Grünfeldi et Grünfeld om neide puid tono ilma lubbata Moisekatzi moisast ilma arwato ja omma mötsast om ka raggo ja tonod 1 korma ja om ka Grünfeld söimano Friedenbergi wargas ja Tarto hulkjas Elläjä söötmisse perräst ja ärrä kaono asja otsmisse perräst mis Friedberg illusti om könnelno ja Grünfeld om ka Friedenbergil perset näidäno.
Karl Grünfeldi käest sai küssitu üttel temmä et tehko Friedenberg seddä öiges et temmä om minno kül söimano.
Tunnistuste perräst jääs polele.
