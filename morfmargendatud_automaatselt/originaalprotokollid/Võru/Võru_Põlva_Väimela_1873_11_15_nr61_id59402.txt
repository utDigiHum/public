N 61 Waimela koggokonna Koh. sel 15 Novb 1873
Päkohtomees Jurr Waask Abbi Peter Pallo dito Johan Traks
Täämbätsel päiwal kaiwas selle koggokonna kohto een Noli Johan Runthal et temmale om Jaaska Jaan Jaaska lubbano 1 püt heringit 1 pund raswa ja ütte aasta kahjo mis ilm pernaiselta woib sündita Summa 200 Rbl katsadda rubla
Kui temma hendale mitte muid naises ei wõtta, kui ennegi temma sosare Katri Jaaska. se lubbaus om olno Juni kuul 1872 aastal, nink käise ka temma sõssarega tolsammal aastal Septembri kuul luggeman ärrä ja päle to es tulle se tütrik ennamb mitte temale naises, ja selleperrast nõwwap temma sedda een nimmitedu kahjo? temal omma selle lubbausse man tunnistaja: Hindrig Zuchna temma täddipoig, Jaan Zuchna ka täddi poig Peter Pichl.
Sepäle om Koolmeister Jaan Jaaska kostno: temma ei olle lubbano mitte kopikatki ei ka kigge wähhämbalki, ja sedda temma es tija mitte milles tütrik es lä, ja temma jäije ennegi perran luggemankäimist wäga haiges, ja es tahha ennamb Johan Runthali silmaga nätta ja (Peter Pichl ei woi temma tunnistaja olla sest ta oma temma wainomees.)
Hindrig Zuchna om kostno: temma ei olle mitte kulno neid sõnno mitte Jaan Jaaska käest enge Johan Runthal om temale sedda asja kõnnelno et temma heringit ja raswa om lubbano muido temma ei tija middagi.
Jaan Zuchna om tunnistanno temma ei olle mitte kulno sest raswast ja heringest middagi ja muido om temmale Jaan Jaaska ja temma emma lubbano et me awwitame nisama kui Hollo Michlit ja wanna emma om weel ülno meil om ka jo sääl üts Summa rahha [mahakriipsutatud: kumaga woib temma awwitedu saija] sissen.
Peter Pichl om kostno: temma ei tija sest asjast middagi.
Et Katri Jaaska siin ei olle jääb se assi tõise Kohtopaiwa päle polele ja tunnistis om sano ette loetus.
Päkohtomees Jürri Waask [allkiri]Abbi Peter Pallo [allkiri]
dito Juhhan Traks [allkiri]
Kirjotaja JohKangru [allkiri]
