Kolmas kohhus sellsammal päiwal, nesama kohtomehhe
Jälle tulli ette, Serristo moisa mõtsawaht ja kaibas, et Jaan Kowik poig, ja möldre poig ollewa seitse puud ärra koorinuwa, ja nõudis egga puu korimisse eest herra kässu pääle 50. kopikat. - Teep ütte kokko 3 Rubla 50. koppik. (Kolm rubla ja wieskümmend koppik). -
Kutsuti ette, Jaan Kowik, poig Johan, kes wälja ütles et temma ei ollewa koornu.
Kutsuti ette, Moldre poig, Michel, kes ütles, et temma ka ei ollewa koornu. -
Kutsuti mõllemba üttelisse ette, sis üttliwa teine teise pääle. -
Otsus  Keiserliko Majestäti, Keige wenerigi essi wallitseja kässu pääle. Moistis kohhus, sest et awwalikult nätta olli, et omma süüdlisse.- ja rummalusi tennuwa, egga puu päält 20 kop. trahwi, ütte kokko  teep 7. puu päält, 1 Rubla 40 kop, ütte kokko, kumbalgil 70 koppik, mes peap ütte näddala perrast ärra massetu ollema. -  (Serristo moisa wallitsusele. - Allkiri)
