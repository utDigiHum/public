54
Tämbitsel Paiwäl sel 26 Mail 1871 om Juhhan Reppi ja Johhan Pahli wahhel se sinnane lepping sündinu et Johhan Repp annap Johhan Pahlile all nimmetetu ellaijä ja maija krami alnimmetettu wiisil kätte selle hinna eest kattes astaijas se om Jürripäiwast 1871 koni Jürripäiwäni 1873 wõlgu
1 hobbene 60 Rub. hobbe
2 hobbene 30 Rub
3 lehmä 20 Rub tük
3 Lammast 3 Rub tük 9 Rub
2 Zikka üts suur tõine põrs 10 Rub
2 ratta wankri 30 Rub
2 Atra 2 Rub
2 kirwest 1 Rubla
Summa 202 Rubla
Seddä Rahha luppap Johhan Pahl sedda üllewan nimmetitu katte aijastaija sissen arrä massa: ja essi erralde om weel lepinu annap Johhan Pahl Johhan Reppile 12 wakka Rukki 8 keswi 10 wakka kartohwlit 1/3 wakkama Aijamaad ja kui Johhan Rep hobbest tarwitap omma sõidu päle sis peap temmä üte päiw ette tedä andma sis saap temma egga kord henälle hobbest Ello korter om Juhhan Reppile tarrekamber ja temmä Elläjä panda kolmas jaggo laudast Juhhan Rep saase omma katte lehma ja katte lamba tarwis annap Johhan Pahl pohku ja haino ja sitt jääp Johhan Pahlile ja kui temma terwis kunnap luppap Juhan Rep Johhan Pahli tööga awwita
sedda leppingut tunnistawwa tõttes
Wolmender Mihel Samoson Johhan Peahl
Wallawannemb A. Sulg
