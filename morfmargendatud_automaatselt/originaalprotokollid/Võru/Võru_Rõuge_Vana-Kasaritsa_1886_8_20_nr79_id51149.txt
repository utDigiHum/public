Jakob Puustusma kaibas, et Jaan Andruse lehm ollew tema lehma mõtsa sisen ära pusknu ni et tema selle ära pidanu tapma, tema nõudwat kahu tasumist 40 rubla. 8 rubla ollew enegi lihunik liha ja naha eest paknu. Jaan Andruse ei ole ette tulnu. Jaan Andruse karjus Johan Tutz, 12 aastat wana Lutteri usku tunnist, et Jakob Puustusma poig Johan ollew karja ütte ajanu, mes tema keelnu, tema ei ollew kullelnu ja küsinu tema käest tubakat, elaja lännuwa kokku ja lehma pusklema. Andruse lehm ollew pusknu Puustusmaa lehma. Johan Puustusma lännu mant pakku. Peter Puustusma, Kusta Jakobsoniga lahutanu lehmi. Kusta Jakobson, Jaan Jakobsoni karjus, 11 aastat wana Lutteri usku, tunnist, et Johan Puustusma Jakob Puustusma poig karja kokku wirutano, neide käest tubakat küsida ja kui lehma pusklema lännu, esi paku joosknu, Peter Puustusma lahutanu lehma koost ära.
Otsus:
ette kutsu Johan Puustusma.
Pääkohtumees J. Kabist [allkiri]
Kohtumees P. Sillaots [allkiri]
     "              P. Klais [allkiri]
