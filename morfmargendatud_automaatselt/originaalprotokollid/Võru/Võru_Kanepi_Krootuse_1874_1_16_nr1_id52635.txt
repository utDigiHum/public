Kohtu een kaibas Warbusest Juhhan Nilbe, et minna olle Hurmi Saija tallol, nink Jaak Armulik wõt minno hobbese lihha mäele wija, ja olli selle hobbese poisikeste läbbi alla saatnu, nink poisikese olli tedda wallale lasknu nink hobbene olli koddo poole nakkanu minnema, nink Peter Herne nink Tawit Mõttus olli sedda hobbest tee pääl löidno, ja ohjat olli ree al olnu nink hobbene olli saisnu, ja nemma olli tedda wallale pästnu nink minnema lasknu, ent seddelkat es olle ennamb pääl olnu ja se seddelka mas minnol 2 Rbl nink rihm 70 kop. nink palles kohhut sedda asja perra kullelda nink säduse perra mõista.
Jaak Armulik ette kutsuti kes selle kaibuse ette luggemise pääle wastutas: Et minna wõtti kül temma hobbese lihha mäele tuwwa nink sadi poisikeisilt alla ent poisikese olliwa tedda wallale lasknu ja hobbene olli ärra lännu, ent sedda ei woi minna mitte üttelda Kas sis seddelkat olli pääl wai es olle.
Tunnistaja Peter Herne ette kutsuti nink selle asja perrast küssiti kes wastut: Et meije löisime kül Tawit Mõttusega ütte hobbese tee pääl nink olli kinni jäänu, ja lassime wallale, ent sedda ei woi minna mitte üttelda kas seddelkat olli pääl wai es olle.
Krotusest Jürri Klink tunnistas: Et minna olli Juhhan Nilbe pool nink panni hobbese ette, ja seddelka nink rihm sai ka pääle pantus:
Kui neil kellekil selle asja perrast ennamb middage ütlemist es olle moistis kohhus:
Et Jaak Armulik massap Juhhan Nilbele temma kahjo tassomises 1 Rbl. selle perrast, et temma selle hobbese poisikeste läbbi wallale om lasknu, nink pool jääp temma ommas kahjos, tuu perrast et selget tunnistust ei olle las seddelkat oli saisu tallol pääl wai es olle.
Mõistmine nink § 773 sai Kohtun Käüjile ette loetus konn nnemma ka sellega rahhule olliwa.
Jürri Kramp xxx
MIhkel Soldan xxx
Gusta Weitz xxx
