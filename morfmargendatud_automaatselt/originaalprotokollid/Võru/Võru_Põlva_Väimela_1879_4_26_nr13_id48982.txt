N 13. Waimela kogokonna kohus sel 26 Aprillil 1879. Koon olliwa: 
Pääkohtomees: Jaan Wäiso 
Kohtomees Peter Hauk
d Michel Jaaska
Tulliwa ette Jaan Lilloson, Dannil Lilloson ja Michel Lilloson nink Juhakami maja peremees Peter Pallu, ja lepsewa siin niida, et nee 3 nimitet Lillosoni lääwa siist ärä Samaritu elawa, sis luppas Peter Pallo neide eest kiiki edes päiditsade kogokonna masse eest wastutada, mis neil siin massa om, selle wasta luppawa nee Lillosoni iga kõrd õigel ajal oma kogokonna massu ärä saada ja luppa weel Daniel Lilloson omma jao rahast mis kord Liina ja Michel Samosoni käest saada oma pandis Peter Pallole; et na niida lepnu tunnistawa oma käe äla kirjotamisega
Peter Pallo [allkiri]
Jaan Lilloson [allkiri]
Pääkohtomees Jaan Wäiso [allkiri]
Kohtomees Peter Hauk x++
d-o Mihel Jaaska [allkiri]
