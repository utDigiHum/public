№ 269   Jaan Meinson tuli ette kapitali raamatuga ja kwittungega ning antse ülles et meie lahkusimi 1872 wõi 1873 aastal oma waraga wenast Peterist ja peran seda ka olime kooni kongrussini eraldi ja mina olen kiik kapital 1882 aastani ära masnu 514 rbl. 5 kop. kuna Peter poole maa peremees olli ja mitte kopikatki es massa kooni 1884 a. üttelisi kongrussi peal 425 rbl. see pera olen maa siis weel rohkemb sisse masnu kui Peter Meinson.
Peter Meinson antse wastust mina es olegi suggugi peremees enne kui 1882 aastal pealt soltatist wälja tullen jaggasimi waranduse ja wõtsin pool maja oma kätte mes eest ma ka siis kapital piddi masma ja mitte rohkemb see pera ollen ma siis 263 rbl 50 kop rohkemb masma sest 1882 aastast kooni 1884 kokko 325 rbl. massa oli mida ma poole pidi masma, et nii moodu eletu wõiwa tunnistada Mihkel Kiisk, Writs Lepmann, Widrik Wija ja Mari Meinson Werrioralt.
Otsus:
Ses 12 Septembris ette kutsu.
 
