Et Tille kõrtsimees Jüri Ketz /kes nüüd Pindi mõisas elap/ oma siist kogokonna kohto poolt kohto keelo ala kinni pantu warandust warguse wiisi ärä om häotanu nii kui:
2 lehmä, 1 puutelgiga wanger, 2 lauda, 1 peni ja 1 saina kel mida temä esi oma käekirjaga tunnistap, et temä selle eest wastutap et see kõik alale peap olema, sis pallep see kog. kohus Keiserliko Wõro Maakohut Keiserliko V Tarto kihelkonna kirjaliko  käso pääle d.d. 16 April a c. sub. № 1988 et Jüri Ketz üle Keiserl Wõro Maakohus mõistas.
Otsus:
See protokoll Keiserl. Wõro Maakohtole ärä saata.
Protok. äräkiri K. W. Maakohtule 17. Mail s.a. ära saadetu.
Pääkohtomees Joh. Pähn [allkiri]
Kohtomees Joh. Purasson xxx
do: Mihkel Laine [allkiri]
Kirjotaja W. Luik [allkiri]
