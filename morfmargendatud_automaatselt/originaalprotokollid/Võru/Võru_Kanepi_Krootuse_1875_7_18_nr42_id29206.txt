Kolmandalt tulli ette , Johan Must, Leesmitti Küllast ja Kaibas et Johan Seeme, 5. Kanna, ja Jakob Astel, 3. Kanna, teewat temma Keswale Kahjo, ja pallel Kohhut et Kohhus sedda asja henda poolt keelas, sest et nemma temma Keeldmist Kuulda ei wõtta, muud ei tahha temma middagi.
Saiwa ettekutsutus, Johan Seeme, ja Jakob Astel, nink se üllewal nimmetedu kaibus ette loetus, Kes selle pääle middagi es woi Kosta, ent arwasiwa oige ollewat.
Otsus. Kohto poolt sai Kõwwa Käsk antas, et eggaüts peap omma ellajat Kahjo est hoitma, muido saawa suure trahwi alla, ja Kahjo täwwete ärra tassuma, nink ni rahhuga ärra lastus.
(Allkirri)
