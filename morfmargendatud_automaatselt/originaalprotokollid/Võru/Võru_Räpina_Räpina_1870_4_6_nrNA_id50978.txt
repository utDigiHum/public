Jürri Suik Kõnno külläst kaibas omma wanna Essä Jürri Suiko wasto, et temmä om 19 Märzil Kiwwilt koddo länno ja om kiwwil söimäta olno, ja temmä koddo om länno om 2 körd söwwa küssino ja möda minnen om jalg wokki külge lanno ja wökki kässipuu om katki länno siis Emmäk Essi sisse hõigano ja kui Essä om tulno om warsti katte kappoga karwo kindi pandno ja pesno ni et suust ninnäst werri wällä om tulno ja Willem om ka appi tulnod.
Essä Jprri Suigo käest sai küssitu, üttel temmä et poig Jürri om issi temmä rindo kindi hakkano ja om wokki ärrä lahkno ja om henda ärrä ülles lubbano puwwa ja kui poig Jürri jälle Essä rindo kindi om hakkano siis om Essä teist poiga appi kutsno ja siis om lönod 4 körd käegä põsse päle ja siis om ka Werri wällä tulnud.
Et poig Jürri jodik om, ja enne seddä jo körtsin jomisse perräst Werrise pägä kohto mann om käino kaibaman se om õige seddä tääp kohhus. Et temmä allati tüllitseb. Essä pm poia päle enne sedda jo mittu körd kaibano et temmä rijökäs ja ilm kölbmatta om ja maja piddäjäs ei kõlba.
Essä pallub poiast wallale sada ja pallub et poig majast pri jääs. Essä tahhab poiaga ärrä leppi, kui temmä winä jomist mahha jättäb.
Agga poig Jürri ei lubba Essäga ärrä leppi enge tahhab ärrä minna Essä mant. Ja tahhab omma Warrandust kätte sada mis Essä om lubbano Essä lubbab kül anda seddä lubbato warrandust poia Jürril agga mitte enne kui Essä ärrä koleb saap poig Jürri lastele selle perräst et Jürri kätte ei wõi anda et temmä om jodik nink ärrä raiskab.
Kohhos mõistis, et kui poig Jürri peas wiggases jämä siis peab temmäle antamä Essäst lubbato warrast abbi tarbis. Ja kui poig Jürri nüüd samma herrälde tahhab minda ehk Essast ärrä lahkob peab Essä andma nüüd 1 lehm 1 siggä 3 wakka rükki 2 wakka keswi 1/3 wakkama kartowle maad. Ja süggisi annap Essä ka weel 5 wakka rükki leiwäs Jürrile Jakob päiwa aig Ses sammas aastajas.
Pä kohtomehhe abbi Kristian Wössoberg
Kohtomees Jakob Türkson
Kohtomees Hindrik Toding
Kohtomees Paap Kress
