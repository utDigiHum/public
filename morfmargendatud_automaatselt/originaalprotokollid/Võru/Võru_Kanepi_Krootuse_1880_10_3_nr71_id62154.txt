Karraskij Koggokonna Kohhos sel 3 oktobril 80.
                                                   Mann olliwa Päämees Jaan Tullus
                                                        do    do    Kohtomees Jaan Leppäson
                                                        do     do  teine    do   Samoel Raig
Tulli ette Karraskij  mõisast sepp Jürri Rabba, ja andis ülles et temmal sel 26 septembril s.a üts walge lammas ärra kadono et wõtjat ehk warrast ei woi ülles anda, ennegi Leena Pallo ollewa wälja üttelnu et temma teedwat küll kos seppä lammas ollewat nink pallel kohhot omma asja Leena Pallo läbbi tõttes tettä andis tunnistajas Liiso Siska Krõõta Moritz rentniko naine Ees Koth Sohwi Raig kellele temma Leena Pallo om kõnnelno. -
Sai ette kutsotus Leena Pallo ja sai üllewanne kaibus ja nõudminne ette loetus kes koggoniste ärra salgas et ei ollewat konnelno ei ka teedwat sellest middägi. -
Sai ette kutsotus ülles anto tunnistaja Liiso Siiska woormunder Tannil Kärner sai kohto poolt küssitus mes temma tiap sest seppa lambast tunnistada mes Leena Pallo om kõnnelno kes ärra sallas et middägi ei teedwat ja woi ka middägi tunnistada. 
Sai ette kutsotus teine tunnistaja Krõõta Moritz kes ka nida samma ärra sallas kui Liiso Siiska. -
Sai ette kutsotus Ees Koth ja tema wöörmünder nida sam(m)a Tannil Kärner nink kohto poolt kussitus kes ütles minna ei tija muud kui et Liiso Siiska ollewat temmäle kõnnelno et Leena Pallo ja Sohwe Leib ollewa üttelnu, meije tiame küll kos seppa lammas om, agga ei tohhi wälja üttelda
Sai ette kutsotus tunnistaja Sohwi Raig tem(m)a wöörmünder Tannil nink kohto poolt küssitus mes tem(m)a tiab tunnistada, kes ütles et Leena Pallo ollewa kartohwli wotmisse aigo üttelno, minna tija küll kos seppa lammas om, kas temma nalja perrast üttel wai olli tõtte sääl kuuliwa paljo muid rahwast ka sedda jutto. -
Otsus  Kohhos mõistis tunnistusse perrä, et om üttelnu henda teedwat. -
Ja nüüd salgas ärra massap Leena Pallo Jürri Rabba lamba est 4 nelli rubla henda käest wälja ehk andko ülles kelle käen lammas om sis pässep prii agga peap se rahha nüüd 8 päiwa aja seen Jürri Rabbale ärra massma.-
Toiselt et Liiso Siiska ja Krõõta Moritz ka sest Leena Pallo jutto teedsewa ja essi rentnikko naisele omma kõnnelno massawa kumbgi 1 rubla walla waeste trahwi se om Liiso Siiska 1 rubla ja Kroota Moritz 1 rubla ülle kige kats rubla waeste laati trahwi mes ka 8 päiwa aja seen peap ärra masseto ollema
Sai Kohto moistminne ja §§ 772-773 ja 774 kohton kaiadelle ete loetus ja lätsiwa sõnna lausomatta kohto tarrest wälja. -
                                               Pääkohtomees Jaan Tullus XXX
                                               Kohtomees Jaan Leppäson  XXX
                                           teine      do     Samoel Raig  XXX
