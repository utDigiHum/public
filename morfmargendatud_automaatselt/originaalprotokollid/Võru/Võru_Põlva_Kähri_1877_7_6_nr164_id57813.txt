Ette tulli Jaan Kelt ja kaibas Jaan Sokk pääle, kes temäle oma lepingo perrä tarre raha ärä ei maswad, mes 10 rubla om.
Jaan Sokk selle kaibuse pääle ette kutsutu kes ütlep, et temä ei keeldwat kül tarre rahha, enegi et Jaan Kelt olewad üle oma lepingo katte wakka mõtsa puhtas ärä ragonu, kelle man, Andres Nikopensius ja Peter Ussin tunistaia oma.
 Otsus:
Kogokonna kohus mõistab et Jaan Sok peäb 10 rubla ärä masma, mõtsaperäst wõib Sokk Jaan Kelt pääle kaibata.
25. Julil - masse Jaan Sokk 10 rubl. Jaan Keltal wälja.
Pääkohtumees Jaan Luik
Kohtumees Peter Tallomees
   " Johan Punnak
