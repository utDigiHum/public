Tu Ahja walla perremees Jaek Matson kaibab, temma om tolle Grape prowwel tallu könno küllast Rentnu nink pool aasta renti erra masnu ja nuid talle todda kottus ei olle Grape prowwa ennep andnu ja temma om talle labbe pallo kahju sanu tu kes seel piddi tulle om moisal enne? 150 Rubla eest kontracte ?? masnu et asse saije, nuid pallep et temma kahju massetu saasi, kuid sie kaup saije tehtu olle Jacob Schmalts Jaan Kanna
Grape prowwe omma tunnismehhega Mik Heidkowiga tulle Kohtu ette nink wasta Jaek Mätson om küll temma man keinu ja temma tallu rentma, Schmalts om sedda tallitanu ja renti 58 Rubla pandnu nink talle rahha kette andnu, temma om weggisi tagase paknu Jagule ja Schmaltsille todda kaerahha ning tedda wehga Schmalts irmutanu ka ehvardanu.
Schmalts tulle kohtu een nink tunnist tu Graape Prowwa om tedda pallelnu ütte haed rentnigu tallitanu, temma nuid om sedda Jaek Mätson kutsnu ja temma een om Graape Prowwa Jaek Mätson omma tallo rendi peel andnu ja 28 Rbl 50 Kp pool rendi Grabe prowwall kette andnu
Tu tunnistaja Jaan Kanna ei olle tulnu.
Grape Prowwa ei wutta sedda tunnistaga wasta et ta Jaek Matsoni suggulane om.
Moistetu: Et talle Grape prowwal enda poolt keddage meiste rahwas is olle man, ja Jaek Maltson, Jaan Kanna nink Schmalts om ütte poolt temma wastu, peap sie guht ju könno töhjas gehma ja Jaek Matson omma kaerahha taggase wutnu.
Jaek Matson pallel protokoll welle suremba kohtu mannu
Rein Pabusk &lt;allkiri&gt;
