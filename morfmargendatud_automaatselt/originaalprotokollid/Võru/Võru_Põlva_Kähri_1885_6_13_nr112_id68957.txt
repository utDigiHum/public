Mart Olesk Kährist tulli ette ja kaebas et Ado Miita on tema adra ilma tema lubata künda wõtnud ja ei ole enam tagasi toonud ja nõuab et Miita saaks sunnitud ader tagasi tuua ehk adra eest temale 3 rubla maksa.
Jaan Miita wastab see pääle: Mina ei ole Oleski atra ilmaski künda wõtnud ja ei tää sellest midagi.
Märt Olesk andis tunistajaks üles:
Jacob Laar; Johann Mägi ja Porila Jaan Lepason kes tunistada wõiwad, et Miita ise on ütelnud et tema Oleski atra künda wõtnud.
Otsus.   Tulewaks kohtupääwaks ülesantud tunistajad ette talitada.
Äralepnud sellega et Miita maksab Oleskil 1 rub. adra eest.
