Wijes kohhos sel sam(m)al päiwal  Kohtomehhe ne sam(m)a
Krotuse mees Michel Trath tulli ette ja kaibas läsk Jürri naise Ann Tamm pääle, et Ann Tamm ollewat kuulno kui temma mees Jürri weel om ellano om Michel Trath küssino Jürri Tamm käest omma wõlga 13 rubla kos ka Jürri naine Ann Tamm om sedda kuulno et wistist om Michel Trathil saada olno. -
Kutsoti ette läsk Ann Tamm ja sai Michel Trath kaibus ette loetus tolle pääle ütles Ann Tamm minna ei olle mitte kuulno kui Michel Trath minno kadono mehhe Jürri käest om rahha nõudno, ei tija ka minna mitte kos Michel Tradil kopkat om saada wai mitte olno. -
Otsus  Kohto arwamisse perra ei olle Michel Tradil küll mitte ennamb 3 ehk 4 aastat perrast, üttegi nõudmist waese läsa naise wasta, ei woi ka mitte Ann Tamm omma kadono mehhe tunnistajas tattas sada kui Michel Trath peas jõudma paar tunnistajat ülles anda, sis õlles tõine assi, nink arwas kohhos temma kaibust tühjas  Michel Trath tahtminne om, et kui läsk naine Ann Tamm suremban kohton tahhap tood wandega tõttes tetta et temma mitte ei olle kulno kui tem(m)a kadono mehhe Jürri Tamm käest om Michel Trath rahha küssino sis tahhap häämeelega tood rahha kaotada. -
Sai kohto Otsus nink § 773-774 kohton kaiadelle ette loetus ent Michel Trath es olle selle perrast rahho et Ann Tamm om wistist kuulno kui tem(m)a om Jürri Tamm käest omma wõlga nõudno ja nuud salgawat ärra, ja pallel protokolli mes kohhos ka lubbas anda.- 
Protokoll sel 19 Aprilil wälja anto 1874
L.S.                                                                         Pääkohtomees Jaan Tullos  XXX
                                                                                    Mannistja       Michel Wõsso  XXX
                                                                              abbi kohtomees Jaan Loodus  XXX
(Allakirri)
