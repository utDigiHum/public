14. Ette tulli Kamja walla tüdruk Marri Tepp ja kaibas et temmal ollewat Kähri karja Rentniki Jürri Nõmmega, kelle man temma tuna man olnu, lats ja temma pallup kohhut, et kohhus sedda asja ülle kulas ja temmale temma latse ülles piddamisses moistas, mes sädus kinnitap, sest rentnik ei tahtwat temmaga selle asja perrast hääl wisil märastki selletust tetta.
Rentnik Jürri Nõmm sai ette kutsutut ja tunnistas selle päle: tema ei teedwat selle tüdrukoga middagi se suggust teggemist olno ollewat, ja kui temma julgup temma päle tunnistada, sis temma, Jürri Nõmm, woip tunnistajide läbbi tõttes tetta, et Marri Tepp egga ütte ja kikiga hennast om segganu, ni kui se kortsi paigon, kos nemma ellanu, wäega hõlpsa om. Ni tunnistaja omma Malhin Ulst ja JaanThoddnask. 
ja woip ildam ka tõisi tunnistaijid, kedda agga sel silma pilgul kätte ei sa, weel ette tua. Ette kutsuti tunnistaja 1, se üttel et temma om nännu kui Marri Tepp Jani Westbergi man, kes nüüd soldatis om ant,  om magganu.
Tunnistaija 2, ette kutsutu, tunnistas, et tema woip omma näggemisse perra tõttest tunnistada, et se tüdruk mitme man, kedda temma nimme perra ei tundwat ja kes soldatis antu ja muido lajale lännu ollewat, om magganu.
Kui nüüd se asja man tunnistus pudulinne om ja pälegi terwe aasta satse sündimissest ja kats aastat sest ajast, kui Marri Rentnikko tenistussest lahti sano, om moda länno, ilma et Marri selle asja polest olles päle kippono, kellest awwalik om  - mes ka kaibaja essi tunnistap - et temma tol kõrral, kui Rentniko mant wallale sai, temast weel pohhas om olnuo, sis om se assi moistmatta ja selle man kül arwata, et se tüdruk ennegi selle perrast ja üsna umbe Rentniko päle kippup, et neide õige sü allotside käest, kes soldatis ja kes muido lajale lännu, kül  middagi wõtta ei ole.
Mõistetu: Marri Tepp omma kaibussega kel pohja ei olle, ärra sata.
Päkohtomees Ado Sok XXX
kohtomees Johan Weiko XXX
     do.:   Johan Nemwals  XXX
walla wöörmünder Johan Leppason XXX
   d..        D.. Jakob Liblik XXX
