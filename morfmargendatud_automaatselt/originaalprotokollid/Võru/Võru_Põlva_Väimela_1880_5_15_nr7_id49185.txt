N 7. Waimela kogokonna kohus sel 15. Mail 1880 
Koon olliwa Pääkohtomees Adam Sulg
Kohtomees Peter Rikkand
d-o Karl Liin
Ette tulli Kütti talu peris pidaja Jaan Wäiso, nink andse üles, et kuna tema abikaasa Charlotte koolnu om nink tema taht tõist kõrd abi elu sisse astu, sis saise kadunu abikaasa perandus selletedus, nink luppas Jaan sellest perra jäänu latsile nii kui: poig August ja tütar Julie, kumalegi 40 rubla /nelikümmend rubl./ - nink saije siin selle poolt selletus tettus, et tema wõip ilm takkistamata tõist kõrd abi elu sisse astu. 
Pääkohtomees: Adam Sulg. [allkiri]
Kohtomees: Peter Rikkand. [allkiri]
Kohtomees: Karl Liin [allkiri]
