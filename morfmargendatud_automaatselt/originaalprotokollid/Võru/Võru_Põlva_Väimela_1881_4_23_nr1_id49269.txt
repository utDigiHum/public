N 1. Waimela kogonna kohus sel 23 Aprillil 1881.
Koon olliwa: Pääkohtomees Adam Sulg
Kohtomees Peter Rikkand
d-o Karl Liin
Tulliwa ette Michel Samoson nink Adam Rikkand nink teije oma enditse leppingo pera mis leppingo raamatun N 9, sel 26 März 1876 ärä om kirjotet et Adam Rikkand om selle maja eest hoolt seeni ajani kandnu, nink luppas ka edespäide kanda, nink jääp ka wiimate kiik warandus mis täl om sinna maija, nimelt tema risti poja Adamile, keda tema seeni ajani kui omas kasu pojas ollen pidanu, et selleperast sis ka tema kui jõwwetus ehk kõhnas jääp, toitmist, hoolekandmist ja üles pidamist selle käest löüd.
Lepja Michel Samoson xx+
Adam Rikkand xxx
Pääkohtomees A. Sulg [allkiri]
Kohtomees: P Rikkand. [allkiri]
Kohtomees Karl Liin [allkiri]
