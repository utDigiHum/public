Gustaw Pik kaibas kohto een, et Jahim Girasim om temmä naist Anne pesno.
Ann Pik issi üttel et Jahhim Gerasim ollewa temmäle suppi koppaga arwato 10 korda lönud ja Jahimi naine ollewa 1 körd lonod koppaga, mees Pik om nänno kui Jahhim temmä naise rindu pääl põlwiga om olno kui temmä nurmest koddo om länno kui Gustaw Pik om Jahhmil üttelno et mis sa teed se assi ei lähä mitte körda, sis Jahim om kirwest wõtno ja om ähwärdäno Pikkil löwwa ja issi om üttelno et eggä sinnul kohhut ei olle, ja sis om rinnust kindi wõtno ja pekki wäljä hakkano widdämä.
Jahim Girasim üttel küssimisse päle et temmä ei ollewa mitte Pikki naist Anne pesno, enge ollewa enegi pu algo näitno selle perräst et Gustaw Pikki naine Ann ollewa eddimält Jahhimi naisel suust werre wäljä om lönod. Ja ei ollewa ka mitte Gustawi naise rindu päle sanud. Agga Gustaw Pik ollewa temmä selgä karrano ja kurgust kindi wõtno ja Gustawi naine ollewa hjusist kindi wõtno Jahimel.
Tunnistust es olle kumbagi poolt
Kohhus mõistis et mehhed peawad mõllemba 48 tundi törmän ollema ja naised 24 tundi kaklemisse eest trahwis
§773 om ette kulutedo
Gustaw pik pallel prottokoli
