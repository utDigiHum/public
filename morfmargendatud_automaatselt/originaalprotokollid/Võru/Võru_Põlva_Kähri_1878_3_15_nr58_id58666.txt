Ette tulli Lillo Richm Kiomast ja kaibas et Henn Ewert oli tedä pesnu ja köütnu ja wallawanemba poole saatnu, selle eest et temä oma poja palka nink poja peräst kõnelnu. Ja nõwab 100 rubl.
Tunnistaia Jaan Jorro tunistab, et temä om nännu kui Ewert oli edimäst kõrda Lillo taga kambrest tõuganu, sis ei ole Lillo wälja lännu, sis oli tõist-kõrda wälja wööruste wiinu, säält lännu Lillo tagasi sis oli Ewert tõuganu nii et Lillo sällilde maha kukkunu ja pää ärä löönu, pääle tuud oli Ewert Lillo wõtnu ja kinni köütnu ja wallawanemba manu saatnu.
Tunnistaia Peter Kimmask, et Lillo oli taga kõrtsin kõwaste kõnelnu, sis oli Ewert säält wälja ajanu, sis nakkanu Lillo usse pääle pesma ja tulnu kõrtsi tagasi, taga kõrtsin tennu ka niisama sugust lärmi tennü, säält oli Ewert wälja ajanu ja kukkunu ülle läwe wälja. Sis wõtnu Ewert ja köütnu Lillo kinni.
Jaan Häelmu tunistas niisama.
Otsus:
Jääb niikauges kui Henn Ewert ette saab.
