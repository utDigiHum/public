No 303
Keiserlikko kihhelkonna kohto kässo päle 9 Märzil No 696, al ette wõeto andis Jakob Mälton kohto een ülles et temmä osteto majal Jaan Raudberg om lauda lagge 40 palki päle pandmatta jätno ja 1/2 lauda kattust om katmatta jänod. Ja enne Jürri päiwä o 2 näddälid majast ärrä länno.
Jaan Raudbergi käest sai küssitu üttel temmä, et Selle perräst temmä ei olle lauda lagge widdäno et maja ärrä müdo om, ja kattus om selle perräst katmatta jäno et temmä warju allust sennä om tahtno tettä; Enne Jürri päiwä om temmä selle perräst ärrä löno et Jakob maja wasto oma wõtno.
Jaan Raudberg om sedda maja koggoni wastse paiga päle ehhitäno ni et sääl kiwwi ka enne paigal ei olle olno ja 1 ainoke aastaig om Jaan sääl wastsen majan weel sano ellädä.
Jaan Raudberg andis ülles et temmäl om lauda lae walmis teggemisse ja kattuse päle pandmise eest Jakob Maltoni kätte jänod,
1 koa kattus mis 6 Rubla wäärt
2 Sauna kattus ja põrmand
3 1 wakka alla ristkhaino maad kus seme päle om külweto 1 wakkama hainamaad puhhastedo
Kohtomehhed tunnistiwa et Jakob Mältoni kaibus ja Jaan Raudbergi nõudminne 1870 süggisi wastamissi om ärrä leppito olno, su sannaga, ni et teine teise käest middägi nõudmist ei olno.
Kohhus mõistis, et kumb kumbagi käest middägi noudmist ei olle, et selle süggisisse lepmissegä rahho peawad jämä.
§773 om ette kulutedo
