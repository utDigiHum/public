Sai 15. Juunil s.a. sub № 26 algatud Peep Rihmi päranduse seletamine edasiaetud. Tänaseks oliwad ülesantud wõlglased wõlgasi seletama ja weel pärast ülesantud weel Jaak Trakmann Kährist 220 kop ja Joh. Nääs Tartust 20. rubl. wõlgu, kõik ettekutsutud.
Neist oliwad üksnes järgmised tulnud:
Haaslawalt Joh. Klaos tunnistas seda ülesantud wõlga tõeks.
Kiidjerwelt Daniel Suits:wõlatähe peal seisis sada rbl. mis ta ka nõnda ütles olewat ja ka ülesandja seda järeleandis.
 Wastse-Piigastest Joh. Paabu tunnistas ülesandmise õigeks.
Koorastest J. Milter ütles, et mitte niipalju ei olla, sest Peep Rihm olla tälle selle raha ilma protsenditeda laenanud ja olla sellest nüüd ka juba sada rubl. tagasimaksetud. Selle wastu ei teadnud nõuudjad midagi ütelda. Tarwis kirjalikuid tunnistusi nõuda.
Kährist Jaan Trakmann tunnistas ülesandmist õigeks.
Otsustati:
Kõige ülesantud wõlglastel on wõlg selle otsuse kuulutamise päewast kolme kuuni äramaksta. Nendele, kes täna siin oliwad, sai see otsus ettekuulutatud.
Kes täna puudusiwad, on 17. August c. ettekäsutada, nõnda ka Jaan Milter oma tunnistuskirjadega.
Pääkohtunik J. Nilbe /allkiri/
kohtonik: H. Lukats /allkiri/
abiline: J. Kibena
