Miina Tobrask tunistas: Tema ei olla löömist näinud, aga küll kuulnud, kui Mai Kimask on Leena Aadleriga oma poea pärast tülisse tükinud ja ütelnud, "Sinu kuradi litsi peräst on minu poeg kohtu alla saadetud."
Nõndasamuti tunistas ka Peeter Pai.
Liiso Raag oli ilmawabandamata tagasijäänud, sellepärast jääb asi seekõrd weel pooleli ja on ta 7. Apriliks uuesti seädusliku trahwiga ettkäsütada lasta.
Pääkohtomees:J. Nilbe [allkiri]
kohtomees: A. Luk xxx
d.:                 P. Aabel xxx
Kirjutaja as. Zuping [allkiri]
