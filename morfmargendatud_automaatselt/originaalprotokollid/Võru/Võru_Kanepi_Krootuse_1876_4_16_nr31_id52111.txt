Teiselt tulli ette Leesmitti Johan Seeme ja kaibas omma pooleterramehe Adam Kroon pääle, et ollewat lasknu linna nurme pääl ärra kuijuda, se om seeni kui saanu kolmewakkamaa ärra kakkotus, sis wiinu likku, ent ei olle ennamb pehmes saanu, nink ni jätnu talwes lumme alla, et temma küll mitto kõrda süggise kui olles woinu linna ülles wõtta, Adam Kroonil käsku olli andnu, temmast mitte ei olle täüdetu, nink ni jäänuwa linna talwes lum(m)e alla, ja nõudis selle est kahjotassomist 70. rubla hõbb. nink ni eddesi. -
teiselt kaibas, et om jätnu 3. sülda puid raijumatta nink weddamatta mes kontrakt kässep. -
Kolmandalt kaibas, et om jätnu maad weel kündmatta, nink 
neljandalt, et om jätnu, Postitee, ni kui Kerrikotee pääle, kruusa weddamatta ehk ollewat weidi weddanu. -
Kutsuti ette Adam Kroon ja sai kaibus ette loetus, kes eddimatse punkti pääle ütles Johan Seeme ollewat wõeral rahwal ülle leppingo, lubba andnu linno omman leotussen leotada, ne ollewat wee ärra zurknuwa ehk happus ajanu, selle perrast jäiwa omma linna kalles ja es anna mitte ennembi weest wälja wõtta nink weddada, warrajanne talw tulli nink ni jäiwa lumme alla, nink ni eddesi. -
teise punkti pääle ütles kuis temma minno waiwas omma puuandmissiga mes minnol olli leppingo perra saada, selle perrast es raiju nink es wea minna ka mitte temmale n. ni e. -
Kolmanda punkti pääle ütles weidikenne jäije too ei olle suur assi.- nink
Neljandama punkti pääle ütles minna olle nisamma palju wedanu, kui minnewa aasta weetu olli. -
Otsus   Kuhhus moistis Johan Seeme eddimanne kaibusse punkt tühjas selle perrast, ni kui Protokoll No 30. täämbatsest paiwast näitap et om wõeril lubbanu ülle leppingo omman leotussen linno leotada, teise punkti pääle 
moistis kohus, et Adam Kroon peap Johan Seeme´l 2. rubla 25. kop massma
Kolmanda punkti pääle, et Adam Kroon peap se maa ärra kündma, mes 8. 1/2 päiwa sissen peap massetu nink künnetu olleam, ja neljas punkt jääp, ülle ehk perra kulamisse pääle teises kohto päiwas.
Sai kohtomoistminne ja § 772. n. 773. kohtonkäujattelle ette loetus, ent Johan Seeme es olle sellega rahhul, et arwas hendalliiga tettu ollewat nink pallel hendal Protokolli mes kohhus ka lubbas anda. -
                                    (Allakirri)                                                        (teine Protokoll wälja antu kässu pääle sel 2el Märtsil 1876. (L.S.)
       (LS) Protokoll wälja antu sel 20al April 1876.
