Peter Zernask kaibas wasto Peter Pabuski et temmä sullane Peter Pabusk ollewa temmäl ärrä warrastano rehhe alt rukki ja om Emmägä koddo saatno ja ollewa ärrä warrastano 3 1/2 w kartowlidja ollewa ütte raudlappi wõtno			hind 60 kop		ja üts wenne tabba			20 kop		1 padda mis katki om lödo			50 kop		
ja päle selle ei pea mitte Peter teeni tahtma, ja peab tenistussest ärrä ollema ja ollewa näddäl aigo temmä rõiwid piddäno ja ollewa kassoka ärrä lahknud ja ollewa Aida Kattust kakno ja Aida tabba ärrä kaksano Sinkligä.
Peter Pabusk üttel et temmä ei tea middägi warrastano ja ei ollewa ka Aida lukku kaksano eest egga kattust kakno ja ei ollewa lappid wõtno eggä kattelt lahkno.
Jula Häide 15 aastaiga wanna üttel, et temmä om nänno kui Peter Pabusk om rehhe alt rükkid wõtno ja Emmägä koddo saatno temmä om Aidan kaeno ja ollewa nänno ka kui Peter Kartowlid om warrastano 2 1/2 wakka
Peter Songe tunnist et Peter Zernask om temmäl üttelnu, et temmä ennämb Peter Pabuski hendäl ei jättä ja om lubbano et Peter Songe wõib sullast Peter Pabuski hendäl ärrä palgata.
Karl Menowi tunnistusse perräst jääs polele.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Josep Ritsmann
Kohtomees Writz Solnask
