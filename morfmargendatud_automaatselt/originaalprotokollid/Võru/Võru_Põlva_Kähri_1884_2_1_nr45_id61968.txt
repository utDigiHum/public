Karl Pillberg tuli ette ja andis üles et temale on 2 rub. 10 kop. wõlgo Johann Wäiko käest saada ja palub seda wälja nõuda.
Johann Wäiko wastab et tema olla see 2 rub. 10 kop. Karl Pillbergile kortsi pletti ees äramaksnud.
Sai Johann Wäiko käest küsitud, millas on tema see raha maksnud ütles minewa sügisel maksnud olewad.
Mõistus.   Kogukonna kohus mõistab 
et Johann Wäiko peab see 2 rub. 10 kop. 2 nädali sees Pillbergile wäljamaksma; sest et tema ütles et esiteks et ta on see raha Pillbergile kortsi pletti pääle wäljamaksnud, maksmise aja ütles aga alles minewa sügisel olema olnud kus Pillberg aga enam kortsimees ei olnud ja seepärast see maksmine sellel ajal kõrtsi pletti pääl ei wõinud olla.
Otsus sai T.r.s.r §§ 773 ja 774 järele etteloetud. Johann Wäiko palus 8 pääwa mõtlemise aega.
 
