Ette tulli Erastwerre walla liige, Krootuse walla Purgi talu rentnik Kusta Johani p. Wago ja Kooraste walla liige Kusta Petre p. Taba ja palusid oma wahel hästi läbi räägitud järgmist lepingut maha kirjutada ja kinnitada. 1905 a 23 Aprillil andsin mina Kusta Wago Kusta Tabale pruukita järgmised minu peralt olewad loomad ja asjad Kuni need tagasi saan nõudma. Ja nimelt:1). 1 ruun hobune wäärt			35 rbl.   -		2). 1 lehm                "			35   "     -		3). 1 kahe ratta            "			   5   "    -		4). 1 raud tele praht wankri    "			25  rbl,   -		5). 1 lammas                             "			   3  "     -		                                                        Kokku			 103 rbl.		
wäärtuses ja ei tohi Kusta Taba eenpool olewa waranduse peale ühtegi wõlga teha, mis wastasel korral tühiseks saab arwatud.
Kusta Tabluk [allkiri]   K Wago  [allkiri]
Üks tuhat üheksasada kuuendamal aastal nowembri kuu esimesel päewal on Krootuse wallakohus Krootuse wallamajas eesseiswat suusõnalist lepingut, mis oma wahel teinud Kusta Johani poeg Wago Krootuse wallast Purgi talust ja Kooraste walla liige Kusta Petri poeg Taba, mis juures wallakohus tunnistab, et lepingu osalised on Kohtule isiklikult tuttawad, et nendel seaduslik õigus aktisi teha ja et pärast selle lepingu kuuldawalt ettelugemise täiesti rahul olles tõesti oma käega lepingule allakirjutanud on lepingu osalised Kusta Johani p. Wago ja Kusta Petre poeg Taba
Eesistuja J Kuhi [allkiri]
Kohtunik J Traat [allkiri]
   "           D. Paloson
1909 a. 23 Mail on sellest leppingust ära kiri wälja antud ja 15 kop tempel maksu sisse nõutud
