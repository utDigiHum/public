Kristjan Karask kaibab et Rautsep Jani poig talle kats kord kiwwiga rindu wissanu ja siis om temma ülles lennu ning poisille jusis kinni wutnu paelke sie om ta alla leinu siis om Rautsep Jaan talle körwumede rinde loenu et ta jaen om tahtnu satada tunnistaja oma Gottlieb Kuns ja Alexander Tichanow
Rautsep Jaan omma poigaga tulle ette ning tunnist remma om küll üts kord Kristjanille loinu selle berrast et temma tedda poiga om loinu temma poig ei olle mitte Kristjanille kiwwiga wissanu.
Gottlieb Kuns tunnistap et Rautsep Jani poig om üllewant Kristjanille koiwa sawi tükkiga rindu wissanu, selle peel läts Kristjan ülles ja wutt poisill jusist kinne paele sie tulle Kristjan all ning loije Rautsep Jaan tall waege köwwasta kura kaega körwu pae ning et pae kiwwi saina wasta läts, paele sie pillal pois wiel mahga.
Alexander Tichanow tunnistap neid sammu sönnu mes Gottlieb, mud kui paele tülle ta ei olle nennu pillumist.
Moistetu:
Nemma tulliwa kohtu ette ning tunnistawa et erra lepnuwa om.
