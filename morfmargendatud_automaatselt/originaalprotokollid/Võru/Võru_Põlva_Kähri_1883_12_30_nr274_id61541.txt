Mõistus. Protocolli № 206 kohta teeb Kogu konna kohus otsuseks ja mõistab Peeter Wasseri kaebtust wastu Peter ja Johan Punakut ja Kusta Nikopensiust tühjaks sest et tunistust ei ole.
Otsus sai T.r.s.r §§ 773 ja 774 järele ette loetud. Wasser ei olnud mite otsusega rahul ja sai opuse leht wälja.
Eesistuja Johann Waks XXX
Kohtumees Jaan Salomon XXX
do: Juhann Wasser [allkiri]
Kirjutaja M. Bergmann [allkiri]
 
