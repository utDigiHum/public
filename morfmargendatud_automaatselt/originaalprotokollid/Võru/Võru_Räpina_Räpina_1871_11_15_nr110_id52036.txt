Fr Dannenberg, kaibas kohto een, et krundi perris ommanik Krabiprouwa om teddä wargas tenno om üttelno et temmä laudo om warrastano, ja om läbbi laskno otsi, agga ei olle mitte laudo leidno.
Krabi prouwa käest sai küssitu, üttel temmä, et temmä ei olle sel körral mitte üttelno enge et temmä om Aitta laskno käija läbbi jooskmisse perrast, agga ei olle mitte jallaga Aita länno.
Kohtomees Kristian Rusand tunnistas et Krabi prouwa teddä selle perräst om sena kutsno, et Dannenberg olles temmä laudo warrastano ja laudo wargusse perräst om otsitu agga ei olle leidno mitte laudo.
Kohtomees Kristian Rusand om kaiwo tulpe perräst sena länno.
Kristian Rusand üttel et Krabi prouwa om temmäle üttelno et Dannenberg om temmä laudo warrastano.
Kohhus mõistis, et Krabi prouwa peab 50 kop waeste ladikus trahwi selle masma et temmä Dannenberg wargas om üttelno.
