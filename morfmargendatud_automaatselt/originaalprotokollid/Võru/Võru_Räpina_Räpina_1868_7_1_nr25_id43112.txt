Kristian Pedmanson ja Jakob Zernask saiwad kohto ette kutsutu kui neil kihhelkunna kohto käsk ja wannad prottokolid saiwad ette loeto siis üttel Jakob Zernask et honist ollen minna koggoni pri neid minna ei wötta ja sel körral minnu es sunnita.
Kohhus om täembi 2 näddali aja päle tärmin pandno et 15 Juli kuu päiwal peäb Jakob Zernask se 82 Rubla 44 kop. kohto lauwa päle ärrä maksma mis Kristian Pedmansonil weel sada om selle 22 Aprilli möistetu prottokoli perrä.
Pä kohtumehhe abbi Kristian Wössoberg
Kohtu mees Hindrik Toding
Kohtu mees Jaan Holsting
Kohtu mees Jakob Türkson
