Minna tunnistan ja lubban selle Mihkle Kirberi ette keige omma warrandusega wastotada  ja igga ühte sädussest kästut maksa ilma ühhe wastapannemisseta selle Michel Kirberi ette maksa ja annan omma warrandusse siis koggokonna kohto ees ülles:
kats hobbest mes 60 rubla,  kolm lehma mis 20 r. õbbe,  kaks sigga, mes 2 rub., katte ratta kaks atra.
Peter Kirber XXX
Meie kohtumehhe tunnistame, et sel Perremehhel se warrandus on ja jouab igga ühte kahjo selle  x eest tassoda. 
xxx Jaan Seba  koggokonna kohto pea
xxx Peter Nikopensius Kohtomees
xxx Johan Nemwals Kohtomees
