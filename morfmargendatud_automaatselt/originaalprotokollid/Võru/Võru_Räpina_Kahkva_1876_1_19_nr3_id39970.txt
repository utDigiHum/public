Moisteto:
Kusta Kübbar peab Josep Üllimsonile ühe linna suggemise weitse eest 40 Kop. ja Karl Woigand massap 30 Kop. höb. selle et, nende holetumal wiisil se linna suggimise weits on nita kaotsin jäänu, et ommanik sedda asja ennamb mitte taggasi ei olle sanu.
Pääkohtomees Gottlieb Anderson
Kohtomees Paap Sabbal
Kohtomees Josep Parmak
Kirjotaja J. Maddisson &lt;allkiri&gt;
