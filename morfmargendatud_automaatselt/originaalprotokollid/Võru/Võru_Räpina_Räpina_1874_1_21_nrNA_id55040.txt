No 12 al 17 Januaril
21 Januaril päle ettelugggemisse tunnistiwa ülle kaejad, üllekaemann om käinowa
Kohtomees Jakob Türkson
Kohtomees Josep Ritsmann ja wöölmünder Hindrik Punman
keil 3 ülle kaejad om leidnowa Writz Kirrutari sullasil 4 köiwo ja 2 peddäjäd olno raioto, 3 köiwo om 5 tolli 1 köiw 4 tolli 1 peddäjäs 5 tolli 1 peddäjäs 4 tolli paksod päle selle mes sullased om ülles andnowa et muidel weel rohkemb om raggoto, om ülle kaejad leidnowa sääl raijestikko pääl muidel raioto 5, 6, ja 10 tolli köiwod paksod olno mis ärrä raioto om, olno, 3 tükki om mõdeto olno, agga 1 rohkemb peab raiotot ollema olno, muil rahwal nink ülle mõtsawaht ei olle kaibano.
Kohhus mõistis 1/2 trahwi 4 kop tolli päält 28 tolli 1 Rubla 12 kop peawad neo sullased Wido Norrusberg, Gustaw Hansmanil, Moisale massa, selle perräst om pool trahwi mõisteto, et oigel ajal ei olle kaibato, ja muil rahwal ka om kando raioto mötsan raggoto
§773 om ette kulutedo
