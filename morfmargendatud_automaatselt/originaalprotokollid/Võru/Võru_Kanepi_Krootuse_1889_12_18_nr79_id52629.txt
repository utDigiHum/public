1. Hurmi mõisawalitsuse asemel S, Sibul tuli siia kogukonna kohtu ette ja kaebas, et Adam Zeieril on 94 Rbl wõlgnewat mõisale renti maksmata. Sellepärast palun mina kogukonna kohut, Adam Zeier sundi seda mõisa heaks ära maksa laske.
2. Adam Zeier Luteri usku wastutas selle kaebduse peale, et minul on 90 Rbl wõlgnewat renti mõisale maksmata. 4 Rbl on ära selletedu mõisa walitsusega. Seda 90 Rbl luban mina kuu aea sees ära maksa.
Mõisawalitsus asemel S. Sibul ütles, et 4 Rbl on tasa teenitud ja ära selletedu.
"Kog.kohtu mõistmine:"
Adam Zeier peab 90 Rbl wõlgnewat renti kuu aea sees, mõisawalitsuse heaks wälja maksma.
Mõistmine kuulutadi Seaduse järele; siis olid mõleat sellega rahul.
