Kuwwendalt  Tulli ette Johan Seeme ja kaibas, et Jakob Astel ei masswat temma kassa rahha ärra, se om 8. rubla. -
Kutsuti ette Jakob Astel, sai kaibus ette loetus, kes ka es salga enge üttel õige ollewat, pallel kannatada ja lubbas massa. -
Otsus  Kohhus moistis, et Jakob Astel, peap se rahha Orriko lades, se om 15.al oktobri se astani wistist ärra massma, kui ei massa saap trahwitus, sai moistminne ette loetus, nink rahhun ärra lastus. -
                                                                                             (Allkirri)
