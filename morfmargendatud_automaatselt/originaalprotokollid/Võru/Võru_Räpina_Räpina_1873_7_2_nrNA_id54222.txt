No 104, 30 Aprillil
2 Julil kui köster Undritze kaibus puide perräst ette olli loeto tunnistas Johann Pock Parzi moisast et Jaan Kerthal om puid wasto wõtno köstre käest 6 riitä, 4 riitä om olno 4 süld pikkäd, 2 riitä om olno 3 süld pikkäd, ja eggä riit om olno 1 süld kõrge pallutusse puid,
Rehhe puid om olno Rehheman 2 riita, 1 riit om olno 6 süldä pik ja 1 riit om olno 5 1/2 süld pikk ja 1 süld kõrge.Ülle kige pallutusse puid			22 süldä		Rehhe puid			11 1/2 süldä		Kokko			33 1/2 süldä		
puid mis Rentnik Kerthal sissi minneki aig wasto om wotno köstre käest.
Ja assemale om Rentnik Jaan Kerthal laskno puid weddädäd just ni samma paljo wanna assemide päle Tarremanno pallutusse puid 4 riita, 4 süld pikkäd 1 süld körged, Rehke puid rehhe manno 2 riitä, 1 riit 6 süld pik, 1 riit 5 1/2 süld pik ja ka 1 süld kõrge, ülle kige jälle kokko 33 1/2 süldä mis Jaan Kerthal köster Undritzel käest ärrä andmisse tarbis murru päle om jäno.
Senka Samailow es ütle middägi teedwä tunnistada.
Tolamalt Adam Hindow tunnistas, et Knohensthirni puid om Jürri päiwäl 1872 olno, Knohensthirni majan usseajan 22 süldä prouwa kässo päle om temmä neid puid mõõtno päle selle om temmä Rehhe puid mõõtno nink neid om olno 11 1/2 süldä.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Kristjan Rusand
Kohtomees ​​​​Gustaw Tolmusk
Kohtomees Rein Lambing
