Lewil sel 22 September 1876.
Man olliwa: Pääkohto.
abbikohtomees
"
Protokolli N 36 perra sel 9 September s. a tulli ette tunnistaja Peep Parrison ja ütles wälja et minna olli ka sääl talgo pääl kõwwaste purjon nink ei mälleta sedda Johan Thalfeldti löömist mitte nännu ollewat nink hõikas enne kubbijas et tulge abbis ja köudame kinni. Tunnistaja Peter Paal Penti wallast om Johan Thalfeldti wihhamees nink Johan Thalfeldt ei tahha tedda wasta wõtta tunnistajas. II Tunnistaja An Mattus ütles wälja et Zimm Kärsna ja kubbijas tõine tõist rinnost kinni peijewa sis lät Juhan Thalfeldt manno ja tõmmas Zimm Kärsna jallost mahha ja leije üts kõrd kõrwa päle.
Moistet:
Juhan Thalfeldt peap tunnistaja An Mattuse wälja ütlemise perra masma 2 rubla Zimm Kärsna hääs 2 näddali sissen wälja.
Se moistminne om § 773 ja 774 perra kulutedu.
Pääkohtomehheabbi Jaan Taal XXX
kohtomees Johan Kausar XXX
" Joh. Suits XXX
Kirjotaja [allkiri] Jõggewa
N 39.
