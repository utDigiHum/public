sel samal päiwal.
Kaibas Fritz Sobbal, temmal ollew Hindrik Parsoni käest 9 Rb. raha ja 1/2 wakka rükki nõuda.
Hindrik Parson tulli ette nink es salga sedda mitte, ent üttel, et temma welli Jaan peab sedda maksma.
Jaan Parson tulli ette nink tunnist sedda maksta.
Moisteto.
Jaan Parson peab sedda 9 R. raha ja 1/2 waka rüki Fritz Sobbalile sel 5 Septb. s.a. ärra maksma.
Pääkohtomees G. Anderson
Kohtomees J. Parmak
Kohtomees P. Sabbal
