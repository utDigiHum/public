Tuli ette siit walla mees Jaan Kontz ja andis üles, et tema nüüd omma warandust, mis senini lahutamatult tema poea Jacobi käes talupidada oli olnud, järgmisel wiisil laste peale ärra soowida jagada:
1. Poig Kaarelil on üks hobune  kõiki riistadega ja 1 siga
2. Tütar Annele 1 lehm.
3. Poig Jakobil on 1 lehm ja 1 õhwakene ja 2 lammast ja 1 siga 2 reke ja 2 atra.
4. Poig Peeter jääb weel wanamehe külge ja on tema jäuks üks aastane pullike.
5. Tütar Leenale 1 lehm.
Mis pääle selle weel muu kraam, on wanamehe oma.
Kohus arwas seda sündsaks ja kinnitas:
Pääkohtomees: Joh. Nilbe [allkiri]
kohtomees: A. Luk xxx
        "            P. Abel xxx
Kirjutaja as Zuping [allkiri]
