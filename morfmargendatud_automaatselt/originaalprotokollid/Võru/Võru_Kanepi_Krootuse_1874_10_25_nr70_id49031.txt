Tulli ette, Musti mõtsawaht Jaan Palloson, ja kaibas et Üllejoema karjusse ollewat Moisa keeletu raendu pääl karjaga ollnuwa, ja olnu tulli mõtsan liggi hainu kuhja üllev tettu olnu, mes, Perris Herra poolt koggoniste ärra keletu, nink kange perra kaemisse all om. -
Saiwa karjusse ette kutsutus, nink se üllewal nimmetedu kaibus ette loetus, kos nemma ka es woi salgata. -
Otsus  Kohhus moistis, selle et karjusse, Moisa keletu raendu pääle om(m)a lännu, nink weel tuld julgussiwa ülles tetta, saap eggaüts 
5. witsa lööki õige kibbodaste, ehk massawa eggaüts 1 rubla waeste ladiko trahwi. -
Jani poig Karel Raig, massis henda est, 1. rubla rahha, ent Peter poig Jaan Likker, ja Hanso poig Jurrij Pokketz, saiwa witsat, nink selle kässuga ärra lastus et mitte ei tohhi moisa mõtsa minna egga tuld ulles tetta. -
                Jusal sel 25mal Oktobril 1874.
                                                           Eenistja Jaan Tullus  XXX
                                                          Mannistja Jakob Uibo  XXX
                                                                    do    Michel Wõsso  XXX
