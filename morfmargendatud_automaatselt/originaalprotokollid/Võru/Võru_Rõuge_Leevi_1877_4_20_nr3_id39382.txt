Lewil sel 20 April 1877.
Man olliwa: Pääkohtomees J. Thalfeldt
abbi kohtomees Jacob Kriwa
" Jaan Taal.
Protokoll N 1 perra sel 6 April s. a tulli ette An Tils wöörmünder Peter Härma Perrist ütles et An Tils ei olle käerahha wasta wõtnu enge om Johan Kolga käerahha lawwa päle pandnu nink olli kül arwata 10 rubla palka leppitu nink es lä selleperrast tenima et ollewat wäega raske tenida nink minna olle temma kaswo esse nink ei olle ka Johan Kolga minno käest lubba küssinu. Tunnistaja Jacob Nilbe ütles wälja et Johan Kolga pakkus kül käerahha en An Tils es wõtta enge jättis lawwa päle ja perrast wõttis kül ärra ent palgast minna ei tija.
Moistet:
Et Johan Kolga mitte An Tils kaswoessa käestlubba ei olle küssinu nink ka käerahha lawwa päle om pandnu sis om Johan Kolga kaibus tühjas arwatu. Se moistminne om § 773 ja 774 perra kuluted.
Pääkohtomees [allkiri] Jaan Thalfeldt
kohtumees Jacob Kriwa XXX
N 3.
