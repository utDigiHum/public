Heinrich Sirach kaibas kohtule, et temä ollew preestriherra W. Troitzkile (Kährin) oma hobese ja wankre 3 päiwas lubanu, kes aga üle tema lubamise weel 2 päiwa tema hobest ja wankert henda käen ollew pidanu ja tema wankre ärä lahknu. Nõuab 6 Rbl. hobese päiwi eest ja 20 Rbl wankre lahkmise eest, Jaan Pannel ollew selle man tunnistaja.
Preestri herra W. Troytzki wastutas selle kaibduse pääle, et H. Sirach ollew temäle 3 päiwa ilma hinnata hobest lubanu ja katte päiwa eest, mis ta ilma lubata pidanud, masnu tema Sirachile 5 Rbl.; wankret ei olle temä mitte ärä lahknu, muud kui wankre siiwale ollew kül weidikese wiga saanu.
Otsus: Kohto mõistmine jääp tõises kohtopäiwas kui Jaan Pannel kui tunnistaja ka kohto ette saab kutsutu ja ülle kulleldu.
J. Tagel XXX
A. Org XXX
G. Asi XXX
Kirjotaja Luik [allkiri]
