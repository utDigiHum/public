Jusal, sel 16. Webruar 1868, olli koggokonna kohhus koon, nimmelt:
                                                 Pä kohtomees Mihkel Raig
                                                 Kohtomehhe assemel Johan Mandli
                                                         do                        Johhan Warri
Jaan Ehhin tulli ette, kaiwas Juhhan Wähja päle, et temmal ollew Werrolt küündlepäiwa ladelt tullen üts kaust zuwwa nahka ärrawarrastet:  Kui na tedda Jaan Läwwega kes Ehhina seltsi mees olli, waja löüdnu, sis om Jaan Läwwi Ehhamarrust koddo lännu; ehit Jaan Ehhin Mättistu maja, kos Juhhan Wähk perremees om, sedda nahha kausta tagga otsma lännu. Jaan Ehhin ei tija sis essigi üttelda, mil wisil ta om joobnus jänu, ja sis om hobbene kige rega, kas warrastet ehk kaddonu, et se nahk perrast Mättistu majast kätte sadi, sis arwas Jaan Ehhin ja temma seltsimees Jaan Läwwi, et Juhhan Wähk, kelle majast nahk kätte sai, ka temma hobbesest süüdlane om. Koggokonna kohhus pandse Juhhanit süüdlasses, eesmalt: Et Juhhan Wähk katte Aaslawwa mehhe, Juhhan Uibo ja temma sullase Anzuga (Anzu prinimme ma ei tija) kes teedwa warga, om henda maija wõtnu, nink päle ka neid omma hobbesega late wenu, ja säält omma hobbesega omma maija taggasi tonu.
Tõiselt: Et nahk Mättistu majast kätte sai.
Kolmandalt: Et hobbene sis hukka ei olles sanu kui na nahka ärra ei olles warrastan.
Koggokonna kohhus mõist: Juhhan Wähk peap 10 Rubla, 7 Jaan Ehhinale kahjotassumisses ja 3 Rubla sandi lati katte näddaliga arra masma.
                                                     
                      
