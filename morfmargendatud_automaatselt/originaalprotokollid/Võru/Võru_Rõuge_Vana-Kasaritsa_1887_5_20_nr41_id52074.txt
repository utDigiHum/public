Ette tulli Jaan Weski Jaani poig ja kaibas, et tema kui sääduslik Hinni talo N 82 perija, ent nüüd ollew see talo Johan Silla käen pruuki ja tema nõudwat seda oma esast peritu Hinni talo N 82 Johan Silla käest kätte saada.
Wastut Johan Sild, kui tälle kaibus ett sai pantus, et tema seda Hinni talo N 82 mitte Jaan Weski esa Jaani käest henda kätte ei ollew perinu, enge ütte wõõra käest, nimelt kadonu Jaan Kõiwo käest, ka ollew tema seda talo jo 27 aastat pidanu ja ollew jo se asi ülemba kohtist läbi käütu ja temale mõistetus saanu.
Johan Sild ei ole Regulerimise Akti selle Hinni talo N 82 pääle ette toonu.
Nema es lepi.
Otsus:
Kuna selle kogukonna kohtu kontraktiraamatust wõtta om, et Jaan Weski Hinni talo N 82 peremees om, sis sai selle kogukonna kohtu poolt mõistetus, et Johann Sild peap see nimitedu Hinni talo N 82 Jüri päiwal 23. Aprilil 1888 Jaan Weski pooja Jaan Weskile kätte andma.
See kohtu otsus sai kohtun käüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus. Johan Sild nõwwap Appellationi.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
     "                P. Klais [allkiri]
Appellation Johan Sillale 21. Mail 1887 wälja antu.
