Hurmi mõisawalitsuse asem. S. Sibul tuli siia kogukonna kohtu ette ja kaebas, et Jaan Krebs'il on wana renti 259 Rbl 49 kop maksmata, mida seesama ära maksa ei taha. Sellepärast palun mina kogukonna kohut, Jaan Krebs sundi, seda nimetatud wõlga Hurmi mõisawalitsuse heaks ära maksa laske.
2. Jaan Krebs wastutas selle kaebduse peale, et see rendi wõlg on tõe, aga ei jõua praegast maksa.
"Kog. kohtu mõistmine"
Jaan Krebs peab 259 Rbl 49 kop. wana renti mõisawalitsuse heaks 14 päewa sees wälja maksma.
Mõistmine kuulutadi Seaduse järele; siis olid mõlemat sellega rahul.
