Ette tulli Peter Johanson ja kaibas, et Ado Pai tälle lainatu raha 6 Rbl. ära ei maswat.
Wastut kaibuse pääle Ado Pai, et tema enne seda 6 Rbl. ära ei wõiwat massa kui kaara põimu aiga.
Otsus:
Ado Pai massap Peter Johansonile 15. Nowembrini s.a. 6 Rbl. wälja.
See kohtu otsus sai kohtun käüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
         "            P. Klais [allkiri]
