Kroonu mõtsa herra Lenaskpeldi Akti perra 8. Märzist s.a. N 274, om mõtsawaht Jaan Suss üles andmise perra Soe Kroonu mõtsast warastanu ööse kirwega
1 kõiw 4 tolli jämme ja 15 jalga pik
1 kõiw 3   "         "           15    "      "
Trahwi nõudmine Taksi perra om 2 Rbl. 28 kop.
Wastut kaibuse pääle Jaan Johanson, et see küll õige ollew, ent mitte ööse enge päiwa aigo ollew se asi sündinu.
Wastut mõtsawaht Jaan Suss, et see warguse asi mitte öö aiga, enge päiwa aigo ollew olnu.
Otsus:
Jaan Johanson peap mõtsatrahwi raha 2 Rbl. 28 kop. sija kogukonna kohtu kätte sisse masma 4 nädali aja sisen a dato.
Otsus om ette loetu.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Sillaots [allkiri]
         "            P. Klais [allkiri]
ära masnu.
