Schmalz tulli kohto ette ja kaibas et temmäl om 2see Septembril 1 Raud äggel ärrä warrastedo nurme päält 1868 süggisi ja sel sammal süggisil 17 Oktobril Orriko lado aig om Seppikoddä ärrä lahhoto ja mürist läbbi sissi löhhutu. Seppikoast om ärrä warrastedo nimmelt1,			4 pihted		2,			11 Amert		3,			1 Hobbese kabja raspli		4			3 tükki annowa witsa rauda		5			1 suur tük rauda arwata 16 wõi 26 naela		6			1 paar wastsid usse hengi		7			1 tük mürki		
Schmalt üttel et mõtsa waht Timmoti naine om üttelno et minge Kassi Josepi pole ja säält otsko lauda päält Schmaltz om kätte sanod Josepi lauda päält 24 ägli pulka ja 1 suur Amer mis Seppikoast ärrä om warrastedo.
Josep Jagomani käest sai küssitu üttel temmä et mitte ei tea kes neid senna om wino, neo pulgad ja wasar Ja ei wõi ka arwata kellegi pääle.
Hindrik Timmati naine Ann tunnistas et temmä lapsed om näino, pühha päiw näddäl aigo taggasi Josep Jagomann lauda pääl suurt wassara kotti. Ja selle perräst om temmä teedno Schmalzil ütteldä. Agga se wasara kott mis selle wahhe aia sissen om ärrä kaddono kui Schmaltz om otsma länno ei tea temmä ütteldä, kes seddä Wassara kotti om Josep Jagomanni lauda päält ärrä wõtnod winud 
Josep Jagomannil sai käsk anto perrö otsi kes temmä majan seddä om tenno 17ne Märzini.
17 Märzil sai Josep Jagomani käest küssitu üttel temmä et Timmat om ösi minno Mänsissen käino
Andres Maidow tunnistas et temmä om küssino Timmäti käest ja Timmat om issi omma su sannaga üttelno temäle et temmäl om 2 wiiha aska Josepiga 1 et Josep 25 rubla rahha om ärrä sallano ja perräst ütte peddäjä raggomisse eest mis Josep kirwe ärrä om wõtnod; selle eest om Hindrik Timmat üttelno seddä tenno ollewad ja Hindrik om ka üttelno weel 7 kangi rauda Josepi lauda lae pääl ollewad. 
4tä Märzil 1869 om Schmalz otsman käino ütten om olno wöölmündri Josep Soeson ja Sell Jakobson
Josep ülles anto tunnistaja perräst jääs polele.
31 Märzil tunnistas Jaan Häidson et ta middägi ei olle nänno ni samma ka teised tunnistajas es tea middägi.
Hindrik Timmat käest sai küssitu kui Andres Maidowi tunnistus sai ette loeto üttel temmä et lapsed om seddä kõnnelno Josepi lauda pääl nänno riisto ja 7 Rauwa kangi agga kui sures seddä ei tea.
Pühhapäiw om lapsed mängmisegä leidno ja teisi päiw om Schmalz wöölmündriga otsma länno.
Schmalz om weel ülles andno mis ärrä warrastedo Seppi koast 1 torn ja 1 nagla toos ja 1 suur wiil ja 12 walmis hobbese rauda
Schmalz üttel omma warrasteto asjad ülle kige wäärt ollewad 40 Rubla Schmalz üttel et Jakob Jagoman Josepi poig, om ösi sel körral kui Scmalzi Seppikodda ärrä om lahhoto sääl kaino Schmalzi laste man. Jakob Jagoman üttel et temmä ei olle käino Schmalzi Talli pääl laste man.
Tunnistusse perräst jääs polele 7 Aprillini
Hindrik Timmandi naine Ann tunnistas et temma lapsed om nänno pühhapäiw näddal taggasi Josep Jagomanni lauda pääl suurt wassara kotti ja selle perräst om temmä teedno Schmalzil ütteldä Agga se wassara kott mis selle wahhe aja sissen om ärrä kaddono kui Schmalz otsma om länno ei tea temma üttelda kes seddä wassara kotti om Josep Jagomann lauda päält ärrä wõtnod.
