N 23 Waimela Kog. Kohton sel 22 Marzil 1874
Päkohtomees Peter Pichl
Abbi Michel Traks
dito Jaan Wäiso
Täämbätsel paiwal kaiwas selle koggokonna kohto een Torrobi Johan Samoson et tema om kewwade 1873 Karli Michelsoni sullases leppino olli palk leppito 28 Rbl 6 tobi linna sement ja 1 wak kartohwlit mahha panda, ja parahha massap ka Samoson essi, ja nüüd om tema Johan Samosoni too mant selle aasta seen 21 paiw puutono a palk 20 Cop. ja om weel üts pikask kaotsin hinna perra arwata 3 Rbl.
So een kaiwato asjaga omma nemma nida ärrä lepno et 20 paiw ja 3 Rbl kask temma palgast mahha se om 7 Rbl 20 Cop tollep temma palgast mahha ja nemma omma paega ka ärrä selletanno, nink omma kiggest puhta. 
PaKohtomees: Peter Pihl [allkiri]
Abbi: Mihel Traks [allkiri]
dito: Jaan Wäiso [allkiri]
Kirjotaja JohKanrgu [allkiri]
