Kiomast Carl Kõrwe tulli ette ja andis üles et temale on 4 rub 75 kop kattusse laastu lõikamise raha ja 3 rub. ühe palgi eest summa 7rub. 75kop. Peter Nemwalzi käest saada ja palub seda wälja nõuda.
Peter Nemwalz wastab selle pääle: Minule ei ole Karl Kõrwele midagi maksa. Palki mina tema käest ei ole wõtnud. Laastud on tema minule 3 sülda löönud ja pidi iga sülla päält 1 rub 25 kop. saama ja seda on tema ka ärasaanud.
Kõrwe wastab selle pääle: Palgid oli Nemwalz minu palgide juurest warastanud ja leiti Nemwalzi juurest kätte kus Nemwalz siis kohtumehe Joh. Porila juures selle palgi eest minule 3 rub. lubas maksa millega mina ka rahul olin.
Laastud lõigasin mina temale 3 1/2 sülda ja iga sülla päält oli 2 1/2 rub. lepitud 4 rubla laastu lõikamise raha olen kätte saanud ja 4 rub. 75 kop. on weel saamata.
Kiomast Johann Nilbe tunistab: See masin kellega Korwe oli laastuid löömas oli minu ja meil oli Korwega nii lepitud, et iga süllapäält saab Kõrwe omale 1 rub 25 ja 1 rub. 25 saab minule.
Otsus.   Tulewaks kohtupääwaks Johann Porila ettetalitada tunistuse pärast.
