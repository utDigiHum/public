Jaan Uibo Pikkajärwest kaibas. "1880 aastal, kui mina Michel Wõso mant Karaskis teenistusest lahkusin, jäi minol tema man 40 Rubla palka sisse, kellest tema pidi mino Pearaha ära masma, kuna tema ise sel ajal wallawanemb olli, ja pidi tema sest Rahast, mis üle jäi, mulle wälja masma. Sellest Rahast on tema nüüd mulle Nimelt ära masnud, kahe aasta Pääraha 16 Rubla 21 kop. ja on mul nüüd tema käest weel saada 23 R. 79 kop.  mina annan tunnistuses üles, Joh. Uibo Kammerist, Hans Jännes Pühajärwest ja Johan Ratnik Karaskist."
Kaebatawa Michel Wõso´le Karaskist loeti se kaibus ette kes selle peale ütel."Se noudmine ja üles andmine ei ole sugogi õige. Päele selle 16 R 21. kop. mis tema tunnistab kätte saanud olla, olen mina tema käsu päele, tema welle Johan Uibo´le Kammeris 10 R. andnud. Ka es ole tema mitte 50, waid 40 Rubla, aastas. Ka olen mina jo ammo tema palka temale ära masnud, mis mul massa olli, ja ei ole praego temale enamb ühte kop. wõlgo."
Jääb tunnistajate perast poolele.
Peakohtomees   J. Rocht [allkiri]
Kohtomees   M. Wõso   [allkiri]
    abi     "    J. Uibo 
Kirjotaja   A. Muhle [Allkiri]
