Saie ette wõetus polele jänu asi № 142 Peter Hindriksoni kaibuses Jaan Wainot wasta.
Tunistaia Jaan Hindrikson ütlep et minol ei olle ka enamb melen kui paljo raha mina Petr Hinriksonile olle Jaan Waino kaest wiinu seda om jo 9 aastat tagasi.
Otsus:
Kogokona kohus mõistap Peter Hindriksoni kaibust Jaan Wainot wasta tühjas selle et selget tunis-tunistust ei olle.
Otsus kuluteti kohtukäijile § 773 j. 774 T.S. Peter Hindrikson and üles et tema ei olle otsusega rahul ja sai oppus kirri sels. päiwal antus.
Jaan Waino lubab 25 R. nelja nätali sisen äramassa mes masmata om.
Päkohtumees Jaan Luig xxx
Kohtumees Peter Tallomees xxx
   "  Johan Punnak xxx
Kirjotaija asemel G.Nemwalz [allkiri]
6. Julil päält kiri antu.
