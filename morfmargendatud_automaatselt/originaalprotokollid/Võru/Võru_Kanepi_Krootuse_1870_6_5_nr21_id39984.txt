Neljas kohhus sel päiwal:
Jakob Uibu tulli ette kaiwas, et temma ei ollew wije aasta eest Jagopeetre polest majast wälja minnen pole maja ostja Taniel Wõssu käest ütteki tassomist sanu, nink nõwwap nüüd Taniel Wõssu käest 60 Rbl. hõbbe.
Taniel Wõssu kutsuti ette küssiti: Melles sinna ei olle Jakob Uibule temma pole maja eest wälja minneki rahha masnu? Taniel kosste: Minna anni Jakob Uibule Jaanba maja wasta, - kelle eest minna essi läsja Matli Mandlile temma wälja minneki eest ütte wakalla rükkä, ütte tõugo, 2 mõtu rükki mahha, 2 wakka kartohwlid ja 2 rukka haino massi, - mes temma weel nõud, minnul ei olle tälle krossi
massa. - 
Selle päle mõist koggokonna kohhus: Eesmalt: Et Taniel Wõssu Jakob Uibule Jaanba maja wasta and, kelle eest ta essi piddi tõisele masma, tõiselt: Et tol ajal weel sedda sädust es olle, et ostja kahju tassumist wanna perremehhele peap wälja masma ja kolmandalt: Kui Jakob arwas hendal õigusse ollewat kahjo tassumist saija, mes temma õkwa sis es nõwwa ja nüüd weel wije aasta perrast tullep nõudma, - sedda kaibust tühjas ja ilmasjanda kiusus. Sai kohto otsus ja § 773 kohtun käüjile kulutedus; ent Jakob Uibo es olle rahhul nink tälle om Prottokoll sel 11. Junil 1870 wälja antu. -
                                                                                                Pä kohtumees Mihkli Raig
                                                                                                 kohtumees Jaan Leppasoon
                                                                                                        do        Johan Mandli
                                                                              H. Undritz
                                                                                                 kirjutaja
