Jakob Hindow kaibas kohto een, et täembä 2 naddälid taggasi ollewa nemmäd pimmegä hainama sissest wäljä tulnu ja koddo länno ja teweren om pops kes Karli Kiudorwi ärrä kaddono hobbese kindi om wõtno, ja sis möda minnen te weren om nemmäd Karl Kiudorwil üttelno et temmä hobbene sääl koddapolitse Widrik Simmulmani pool om, Ja kui Jaan Hindow Karli tütrega om kõnnelnu sis om Jaan nänno kui wijekeste om usseaia warrätest sissi tulnu ja saibat om sellän olno sis om Josep Karotsing warsti länno Jaan Hindowi Saapad kübbärä ja kalo rätti ärrä wõtno ja om issi üttelnu et kui sa häga ei anna sis sa lassed omma nahha narri sis Jaan om ärrä andno ja kala rätti ka om ärrä wõtno.
Wido Zangow tunnist nisamma ja üttel et temmäl ka Saapad ärrä om wõeto.
Josep Karotsing üttel küssimisse päle et temmä om selle õigussega ärrä riisnu neid saapid Widul et nemad küllä mödä hulkwa, ja aido lahkwa ja tüdruku mann sangist ollewa temmä wõtno neid Saapid.
Peter Kirratusk üttel et Gustaw Kerow om temmäle perrä länno et tulle nüüd ütte ommawa sääl, sis temmä om senna länno, sis om temmä Wido Saapad käest ärrä wõtno.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Writz Konsap
Kohtomees Josep Ritsmann
