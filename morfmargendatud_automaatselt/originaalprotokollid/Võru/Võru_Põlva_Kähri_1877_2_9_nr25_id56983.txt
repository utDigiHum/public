Ette tulli soldat Peter Westberg ja kaibas Peter Holtz ja Peep Punnak pääle, kes olewad temä käest 30 rubla lainanu, ja lubanu sis wastses aastas 15 rubla ärämassa ja 15 rubla aasta peräst, ja lubanu suwe põllu tööd tettä ja wakka ma kartoflid wõtta, nüüd üttelda neesama et nemä sedä tettä ei jõudwad.
Peep Punnak ja Peter Holz ütlewa mõlemba et nemä olewad kül mõlemba ütten sedä 30 rubla lainanu enegi sedä tööd ei jõudwad nemä mitte tettä, sest na olewad hädalise, ja sedä tööd olewad ka paljo tõeste 12ne rubla eest.
Otsus:
Kogokonna kohus mõistab, et Peep Punnak peäb Peter Westbergi käest lainand 30 rubla kuwe Protzentiga kuni 19 Aprili ku päiwan ärä masma: ent selle tähega tettu lepping saab § 1077 perrä tühjas mõistetus; sest see om liig iintres arwatu
Otsus kuuluteti § 773 ja 774 perrä.
Oliwa rahul.
