Keisrilikko komisjoni kohdole.
Karraski kohdomajan olli koggokonna kohhos koon, nimmelt: Pä kohdomees Mihkli Raig
                                                                                                           man istja Jaan Leppason
                                                                                                           tõine  Juhhan Mandli
Karraski walla tõise klassi nekrudi, nimmelt. Jaak Petterson (Jagob Kärner assemel. Jagob Kärner es sa sest et ta Dartun ellap) ja Mihkli Kaunis kutsoti ette, sai neidega kikin asjon, essi errale neide wõlgo ärra sellededos, nida neil muile, ei ka muil meile middage massa es jä mes na essi ülle anniwa. et se nida om tunnistawa nemma omma käega risti allatõmbamissega.
                                                                          Jaak Pettersoon  XXX
                                                                           Jakob Kärner   XXX
                                                                   ja    Mihkli Kaunis
     Karraskin, sel 14. Weebruaril 1869.
                                                                  Wallawannemb J. Tullus
                                                                           Kirjotaja H. Undritz
                                                                                                             
