Ette tulli Jaan Pang nink and ülles et temma ait om pallama pantu sel 4tal Weebruaril s.a. kulle 10ne aiga oose. Jaan Pangi käest sai kussitus kas temma tijap kes om pallama pandnu, nink tolle päle om temma ülles andnu, et tõise tallo Tütrik Ann Kikkas om temma tütrikole Ann Hollepile kõnnelnu et Saluse Poisi Jaan Päss nink Peter Weisberg omma tool ööl sääl ööd olnu, nink omma küssinu kas siin om kohheki tütrega mannu minna kumma päle Anna Kikkas om üttelnu, et Pangi Jaani pool maggawa tütrigu aidan. Perrast sedda omma poisi wälja lännu nink tükki aja perrast jälle taggasi tulnu, nink nakkanu omme jalgu pästma, ent jalgu pasten omma nemma üttelnu ei tija mes wäljan om kas ku warjatus nink kutsi Ann Kikkast kaema.
Ann Kikkas sai ette kutsutus nink Jaan Pangi kaibus ette loetus, kumma päle temma wastust and, et temma sedda ei olle mitte kõnnelnu.
Sepäle sai Ann Hollep ette kutsutut nink küsitus, kas Ann Kikkas om sedda temmale kõnnelnu, kumba Ann Hollep om tõttes tunnistanu.
Nink Peter Pang tõise tallo perremees om ka kuulnu kui Ann Kikkas om Ann Hollepile kõnnelnu nink tunnistap sedda asja tõttes.
Wiimate and Jaan Pang'i Pops Hindrik Kikkas ülles et enne kui temma maggama läts näggi temma kats poissi aida kõrwal saiswat. Neide tunnistuste päle saije Saluse poisi Jaan Päss nink Peter Weisberg ette kutsutus nink Jaan Päss om küssitus sanu kas temma sel öösel koski wäljan om käünu mes päle temma om wastust andnu, et muijal kongi kui hobbest söötman.
Peter Weisberg sai ette kutsutus nink küssitus kas temma om sel õddangul kongi wäljan käünu kumma päle temma ka om wastust andnu et temma ei tija middagi sest temma maggasi nink es kuule enne kui ülles aeti.
Se Protocoll saap Keiserlikko Werro Ma kohto kätte ülle kullemisses nink mõistmisses sisse sadetus.
Päkohtomees J. Hallop XXX
Abbi M. Jõggewa [allkiri]
  "      Peter Kall XXX
