Sel samal Pääwal
Need samad Kohtomehed
Hans Raba Karaskist kaibas. "Mina saatse oma poja Koppi poodi Suhkrut tooma, nink  Kaupmees Taawid Mandel olli 2 wana Raha poisile tagasi pakkunu, keda aga mino poig mitte ei ole tahtnud wasta wõtta, nink tema käest puhast Raha noudnud. Kaupmees aga wõtnud letti pealt need Rahad ja wisanud neid Poodi nurka raua sekka, nink löönud mino pojale 2 kõrd ümber kõrwa ja ütelnud, "Kasi nüüd kodo." nink Michel Puna, kes ka poodin olnu, ütelnud mino pojale "kae pois, mis sa said, siba nüüd kodo poole."
Taawid Mandlile loeti se kaibus ette kes selle päele ütel."Karl Raba poja tullid poodi nink hakkasid molemad poodin ümber nuhkima tõstsid klaas kasti kane üles, nink noudsid, mis se ja asi massab, nink mina nimetasin iga asjale hinna, ja seekord hakkasid mind soimama, sina Siga, sina Lorob, sina ei tea isege, mis need asjad massawa. Kui ma temal 20 kopikast 2 kop. wälja, nink pois ütel selle päele, sina Lorob, kas sa Raha ei tunne, mina seda Raha wasta ei wõtta, mina andsin teise Raha, aga pois es ole sellega ka rahul nink sõimas mind edasi, mina wissasin selle päele raha nurka nink ütli, lase isa sia tulla, kül ma ise talle Raha kätte anna, kui sa Raha ei tunne, nink saatsin poisi üle ukse minema, ja Raha jäi põrmandule maha. löönud mina teda ei ole.
                                                                                      jääb poolele.
Pääkohtomees  T. Wõso
Kohtomees     J. Rocht
    "                   J. Ratnik
Kirjotaja   A. Mahler
