Gotli Karotsing Linte külläst tulli kohto ette ja andis ülles mis temmäl nõudmist om sada omma wanna majast teggemiste eest tassoda wastse ostja perremees Jaan Soesoni kaest.
Allan nimmitedo nõudmissed om1.,			2 Linna tiigi teggemisse eest			2 R 50 kop leppitu		2.,			175 süld krawi kaiwato			2 R 12 kop leppitu		3.,			25 sulda krawi			75 kop leppitu		4.,			3 leisikat Ristk haino semend			7 R 80 kop leppitu		5			2 pari usse hengi 8 naela			80 kop leppitu		6			1 agganikko uks ja henged			40 kop leppitu		7.,			1 raud kaiwo tulba pulk			50 kop leppitu		8.,			1 koa kattus			3 R leppitu		9.,			2 Aitta 1 Zea laut ja keik ussed henge labbad kokko leppitu			26 R leppitu					43 Rubla 87 kop		
Jaan Soeson lubbano Salwe puud Gottil anda assemale ommast mõtsast
Üllemilnimmitedo Gotli Karotsingi nink Jaan Soesoni leppingo mann om olno päkohtomees Jaan Holsting, Wallawannemb Paap Kiudorw Wöölmünder Writz Solnask, Moisa wallitsus Neisar Walla kubjas Jaan Zengow
Gotli Karotsingil om weel ütte agganikko eest nõudmist 4 Rubla
Gotli Karotsing om Salwe puid ärrä winud 6 Salwe jaggo Jaan Soesoni nõudminne om selle eest rahha taggasi
Kohhus moistis et Jaan Soeson peab Gotli Karotsingil maksma ni kui neil leppitu omrahha perrä			43 Rubla 87 kop		Agganikku eest			2 Rubla		Suma			45 Rubla 87 kop		Säält lähhäb mahha Salwe puide ärrä wimisse eest			3 Rubla		Jaap perrä			42 R 87 kop		Wimäse aasta rent manno			108 Rubla		Kontrahti tagganemisse rahha			54 Rubla		Kokko			204 Rubla 87 kop		
§773 om ette kulutedo
Rahha om täemba ärrä masseto 204 R 87 kop
Pä kohtomees Jaan Holsting
Kohtomees Kristian Wössoberg
Kohtomees Jakob Türkson
Kohtomees Paap Kress
