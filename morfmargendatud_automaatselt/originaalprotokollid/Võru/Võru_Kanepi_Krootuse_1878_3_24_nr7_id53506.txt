Samuel Kuus tulli kohto ette ja kaibas, et tema 1876 aasta kewade om 2 wakka nisse Tille weski pääle wiinu ja jätnu neid seni kui sügüseni sinna möldre hoole al. Sügüse poole talwe ei ole saanu minna neid jahwatama, selle et esi haiges jäänu ja kui pääle Küünla päiwa 1877 neid jahwatama lännü ei ollew neid enamb olnu.
Mölder P. Koowit, tunnistap temä kaibust tõttes, ütlep aga et aig wägä pikka lännu ja temä seda kotti hulga muide kotte sisest ärä ei ollew tundnu, sis saatnu Saamole teedmist, et temä omma kotti kaema tules, kes aga mitte ei ollew tulnu ja nii ollew ee kot si kaotsile lännü.
Otsus: Lepsewa sellega ärä, et Koowit Saamole 3 Rbl. massap.
G. Weitz XXX
A. Org XXX
G. Kolk XXX
