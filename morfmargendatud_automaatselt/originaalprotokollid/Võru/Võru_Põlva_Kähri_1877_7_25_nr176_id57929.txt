Ette tulli Jaan Sokk ja kaibas Jaan Kelt pääle, kellega temä om mõtsa ragomise peräst lepinu, ni mes ostetu om ostetu, ent mes ragoda tullep, saab ni ragodus, et mes rehä warre jämeda jääwa kaswama nüüd om Jaan Kelt kõik puhtas ragonu üle lepingo ja temäle sellega kahjo tennu, ja nõwab oma kahjo katte wakkama ragomise eest 15 rubla.
Andres Nikopensius ja Peter Ussin oma man onu.
Jaan Kelt selle kaibuse pääle ette kutsutu, wastutab, et temäl ei olewad märästki lepingot olnu ragomise mõtsa peräst, mes ragoda om olnu om ka ragotu.
Andres Nikopensius tunistab et ostetu mõtsa peräst om leping olnu, enegi ragoda mõtsa peräst ei ole lepingot olnu, peräst om kül kõrtsi man ragomise peräst kõnne olnu.
Peter Ussin tunistab miisama kui Andres Nikopensius.
Otsus:
Kogokonna kohus mõistab, et tunistaia tunistawa et lepingot ei ole mõtsa ragomise peräst olnu, Jaan Sokk kaibuse Jaan Kelt wasta tühjas. Jaan Kelt wõise oma mõtsa ragoda kuis taht.
Otsus kuuluteti § 773 ja 774 per.T.
Jaan Sokk es ole rahul, selle pääle et tunistaia ei ole õigust tunistanu saie opus kiri antus selsamal päiwal.
Päkohtumees Jaan Luik
8 August wälja antu,
