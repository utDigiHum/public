Teiselt tulli ette Jakob Uibo ja kaibas et temma sullane Johan Raig, ollewa teenistussest jo sel 24al Julil s.a. ärra lännu, nink ei olle seeni ajani taggasi tulnu ja pallel kohhut sedda asja ülle kullelda, nink nõüdis kahjo tassomist aasta palka, ehk tulgo teenistuste taggasi ja masko ne ärra wiidetu päiwa perra ärra. -
Kutsuti ette Johan Raig, sai kaibus ette loetus, kes selle pääle ütles, minna lätsi söögi perrast ärra, et söök olli wäega kõhn, ni kui wessi, ja leiba es olle ka ni paljo kui olles süwwa tahtnu. -
Otsus  Kohhus moistis, et olgo täämba päiw teenistusse taggasi min(n)tu, nink illusaste om aasta ärra teenitu, ja ne ärra wiidetu päiwa perra tetta ehk rahhaga massetu, mes leppitu aasta palk, päiwast wälja teep. - Sai kohtomoistminne ja § 772. n: 773. kohton käüja-
telle ette loetus, nink lätsiwa rahhun walja. -
                                                               (Allkirri)
