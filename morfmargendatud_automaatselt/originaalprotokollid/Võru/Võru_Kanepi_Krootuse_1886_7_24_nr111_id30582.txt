Jakob Uibo andis oma Kokkeri Talo rentnikel Jaan Läwwi nink Johann Punal üles, et termin päiwal mitte renti ei ole ära massnu.
Sai ette kutsotus Jaan Läwwi nink Johan Pun(n)a ja üles ütlemine ette loetus, kes muido raho oliwa, ent kahjo tasomist nouda lubasiwa oma ehituse eest, sis lahwa kewaja 1887 oma rendi majast rahuga ära.
                                                                                                                              Pääkohtomees   J. Rocht
                                                                                                                               Kohtomees   J. Mandli
                                                                                                                               Kohtomees   J. Uibo
                                                                                                                            Kirjotaja as. R. Grünberg
