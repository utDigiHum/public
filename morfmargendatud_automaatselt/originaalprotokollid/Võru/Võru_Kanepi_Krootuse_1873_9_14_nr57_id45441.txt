Teine kohhus selsammal päiwal, nesamma kohtomehhe
Tulli ette Peter Plado, ja kaibas et Ado Pludo naine ollewa tedda sõimanu. -
Sai kohtopoolt küssitus tunnistaja perrast. -
Selle pääle ütles Peter Plado, minnol om Hindrik Märdimäggi tunnistaja, Josust. - Ja ütles weel, temma tullep minno pole tööle, küll minna sis wastsest kaiba kohto, kui Hendrik Märdimäggi siin om. - Jäii nii (Allkirri)
