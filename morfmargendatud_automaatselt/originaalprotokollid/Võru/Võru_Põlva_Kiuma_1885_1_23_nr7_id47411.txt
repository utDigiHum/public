Kaibas siis wallast Jacob Aripmann, et siit Peeter Lukk olla temal Kapist, mis tolkõrral lahti olema juhtunud, kaks wana hõbe rublatükki, ärawarastanud ja nagu seda siit Jacob Kibena olla näinud, nad Kähri mõisa Lepa kõrtsmiku asemiku Hindrek Turile äramüünud. Enne seda oli ta neid ühe siit poisi  Joh. Mägile äramüüa tahtnud. Kaebaja nõudis, kui hõbe tükke kättesaada wõimalik ei peaks olema, 3 rubl. kahjutasumist ja süüdlase seaduslist trahwimist.
Peeter Lukk ei ütelnud sellest rahast midagi teadwat, waid salgas kõik maha; tema ei ollagi Mägile müüa pakunud.
Otsustadi:
Ligimeseks istumiseks wastanikud ja tunnistajad Hindrek Turi, Jacob KIbena ja Joh. Mägi ettekäskida lasta.
Pääkohtunik: J. Nilbe [allkiri]
Kohtunik: J. Aripmann [allkiri]
d.: Joh. Aints [allkiri]
Kirjut as: Zuping [allkiri]
