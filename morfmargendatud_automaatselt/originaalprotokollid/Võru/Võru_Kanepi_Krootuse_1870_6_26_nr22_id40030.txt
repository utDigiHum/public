Jusal, sel 26. Junil 1870 olli Karraskij koggokonna kohhus koon, nimmelt:
                                                                                                    Pä kohtomehhe assemel Jaan Leppasoon
                                                                                                   kohtumehhe assemel Adam Lukkatz ja Johan Mandli
Kammeri mõisa rentnik Mango Undritz tulli omma poja Jani assemel ette kaiwas: Temma om Karraskij Herra käest Tiggase tallo ärra ostnu ja kui kewwajalt se tallo kätte om sanu, om ta ka muid kündjid omma ma pääl löidnu, nimmelt kelle ülle ta kaibap om: Nõrristu perremees Jakob Wõssu, kelle temma poig mannu om lännu kui ta maad kündnu nink üttelnu: Kelle lubbaga sinna minno maad künnat? Sääl üttelnu Jagob wasta: Omma lubbaga minna kül ei künna, enge wallawannemba lubbaga. Se päle om temma poig Jaan üttelnu: Noh kui sul lubba om, sis künna päle; ent minna kül ei lubba tõisil perremehhil omma maad pruki. Ent Jakob ei olle ka sest kelust kuulnu nink om arwata pole tõise wakka ma kara mahha tennu. 
Jakob kutsuti ette küssiti: Melles sinna tõise maad kündnu ollet, ja kaaragi päle tennu? Jakob üttel lühhidalt: Wanna perremees om minnule to ma andnu nink minna weddasi omma sitta sinna päle ja tei karas, ni kui kiik teggewa.
Selle päle mõist koggokonna kohhus: Et se meije walla pruuk om olnu, et egga üts koddapolik peap sis perremehhele päiwi teggema, nisamma peap ka Jakob Wõssu Mangu Undritzele wakka ma päält 6 päiwa teggema, ent perremees peap ommast käest päiwa teggijile söök andma.
Sai kohto otsus ja § 773 kohtun käüjalle kulutedus ent Mango es olle rahhul ja om Prottokoll sel 29. Junil s.a. wälja antu.
                                                                Pä kohtomehhe assemel Jaan Leppasoon  XXX
                                                                       kohtomehhe   do     Adam Lukkatz  XXX
                                                                       kohtumees Johan Mandli  XXX
                                                                                                                           H. Undritz
