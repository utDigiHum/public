N 3 Waimela koggokonna Koh. sel 10 Januar 1874
Päkohtomees Jürri Waask 
Abbi Peter Pallo
dito Jaan Zuchna
Täämbätsel paiwal kandse Tannel Pallosoon koggokonna kohto ette et temmal om Johan Pahli käest saija 6 Rbl 72 Cop. Komb om jobba 1874 aastal sano Koggokonna Kohto een moistetus ent om jäno protokolli ülles wotmada, ja tema om kinkno Pahlile 2 R 72 Cop.
Pahlil ei olle selle wastu middagi olno utlemist. 
Moistus
Johan Pahli palk saap Peter Pallo kätte sisse pantus, ja Peter Pallo massap katte näddali sissen 5 Rbl sell Koggokonna Kotho katte. 
Pakohtomees: Jurri Waask [allkiri]
Abbi: Peter Pallo [allkiri]
dito: Jaan Zuhna [allkiri]
Kirjotaja JohKangru [allkiri]
