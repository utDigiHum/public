Politsei wannemb Unterwald
Kui ülle kaeminne olli ette wõeto Kolbergi kaibusse nink kutsmis perrä siis leiti tonase ülle kaemissest sadik pudus ollewad 1 wäike Uibo tuul katki tenno wõi murdno ja 1 wäike Uibo muido katki tetto enne seddä suurt tuult muido olli keik ajan allale.
Ristik hainast ei moista midägi ni kui ta om ni om kahjo ei moista middägi mönni wannast olno jälg om ei tea kas se wõi wanna aastaja jälled we perräst ei olle ka kassono
Kolberg kaiwas et kaiw hakka om lasto minda kui ülle kaeti leiti wanna wõi  walge wassi sisses ollewad mis mitte pruuki ei kõlba muido olli kuiw omma saisusse sissen ni kui ta tallo olli Kolbergi kaibusse perrä sai Leisiko käest küssitu et kas sa olled wett ka sest kaiwost pruukno kostis Leisik ei mitte ma ei olle tilka sest kaiwost wõtno eggä wõino pruuki ja üttel et minna ollen ommale wessi tond Jaan Kangro käest kewwäjäst sadik mis Jaan Kangro ka sääl samman kaiwo man seddä õige üttel ollewad.
Küssitus sai Kolbergi käest et kas weel middägi kaibamist wõi nõudmist om siis ütles Kolberg et kaege seddä Seppikodda et se peab sama jälle tarres tetto ahjo plidi ja müriga ni kui ta enne om olno seddä asja om kohto mõistusse holde jäeto.
Küssitus sai Kolbergi käest et kas weel middägi pudust wõi kahjo om Kolberg es ütle middägi.
Päkohtomees Rein Pabusk &lt;allkiri&gt;
Kohtomees Gustaw Narrusk &lt;allkiri&gt;
Kohtomees Karle Jaguson &lt;allkiri&gt;
Kirjutaja J Jaanson &lt;allkiri&gt;
Wõbso Polizei wannemb D. Unterwaldt
