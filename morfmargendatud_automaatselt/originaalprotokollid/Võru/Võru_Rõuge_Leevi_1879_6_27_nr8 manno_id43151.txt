Protokolli Nr 8 manno.
Lewi kogokonna kohus, sel 27 Junil 1879.
Man olliwa´ päkohtumees Karl Lauk
kohtumees Jaan Thal
" Jakob Kriwa
Kutsmise perra olliwä ette tulnu Jaan Thalfeldtnink temma welli Johan Thalfeldt, ja wimane and eensaiswa kaibuse päle wastust, et temma om kül ilma perre mehe lubbata se aida wõtme, kon temma warrandus sissen saisap. ärra wõtnu. Selle päle es ütle kohtun käüja ennamb middagi ütlemist ollewat, sis
Mõisteti
et Johan Thalfeldt peap selle ilma lubbata aida wõtme ärra wõtmise perrast 1 rubl. trahwi waeste ladikohe 14 päiwa sissen a dato sellest kohtu otsuse kindmas jämisest ärra masma. Se kohtu otsus saije kohtun käüjile kulutedus nink §§ 773, 774 perra ärra selletedus nink § 881 perra opus leht wälja antus.
Päkohtumees [allkiri] Karl Lauk
kohtumees [allkiri] Jaan Taal
" [allkiri] Jakob Kriwa
kirjotj. [
