Sai kohto ette kutsutus teine tunnistaja Marri Wark nink kohto poolt küssitus, mes temma tiab sest nahha wõlgo wotmissest tunnistada, kes ütles et kui Kusta Ehte nahka käino wõlgo otsman, kos sis näil kats leppingot ollo, kas Kusta Ehte toop nisamma hää nink kõlblik nahk taggasi ehk kui sedda ei olle sis massap selgen rahhan 3 kolm rubla Pärnat Wahheril tagasi todda kulin minna selgeste, nink ärra minnen üttelnu muidogi et rahhaga kige selgemb eddimatse kauba mann es olle minna
                                                     Karraski Kohtomajan sel 21 märz 1881
                                                                     Päämees Jaan Tullus
                                                                         Kohtomees Jaan Leppäson
                                                                                    do    Samuel Raig
