Saije see poolele jäänu № 4 ette wõetus ja selletetus.
Tunnistaia Kioma Pääkohtumees Peter Kimask tunistab et temä kaubast midagi ei tiia, enämb kui Liis Klaan oli tähte näütanu ja käsknu kaeda. tema kaenu kül, ent ei ole tundnu, mes olnu kirjotetu, muud ei mõista midagi kõnelda. 
Märt Häelmu tunistab, et temä ei tija sellest kaubast ei ka otsman käümisest midagi. Sest et temä ei olewad sääl man olnu.
Henn Ewert tunistab, et temä ei teedwat kaubast midagi, tähte oli kül nännu, ent ei ole tundnu mes olnu kirjotetu.
Kibbena tulnu kül perrä temä mõõtnu linna kätte ent Kibbena ei ole wõtnu. Ja tolle ette jätnu perrenaine 5 rubla käe raha sisse; tuud ei olewad oigest meelen kas olnu perän käümine ene ehk pääle laade.
Jakob Kibena ütlep et sedä teedwa mino wõras perre, et mina tõisel päiwal ene Orriku laate otsman käwe.
Otsus: 
Kogokonna kohus mõistab, et Liis Klaan anab 5 rubla wõetud käe raha Jakob Kibbenale tagasi.
Otsus kuuluteti §773 ja 774 perrä T.S.
Olliwa rahul.
