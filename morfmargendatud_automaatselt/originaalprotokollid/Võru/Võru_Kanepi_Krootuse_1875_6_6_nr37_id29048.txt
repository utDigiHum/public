Teiselt Tulli ette Karrasky  Walla wöörmünder, Daniel Wõsso, ja pallel koggokonna Kohtult ütte tunnistus kirja et temma wois selle  waese latse Jaan Teener'i protsenti rahhast mes Kannepi Kerriko Oppetaja herra käen temma kullu hääs wälja wõtta (15.) wiesteistkümmend rubla, nink nimmelt selle tarwis, et ei jõwwa weel ni paljo tenida.Päärahhas 8 rubla, sis jääp temmal omma kullu pääle, Kübbara, Saabaste, n: N. e: pääle 7. rubla, mes ka Koggokonna Kohto poolt sai lubbatus, et woip Oppetaja Herra käest se üllewal nimmetedu raha wälja pallelda.
Nink sai se üllewal kirjotedu ärra (LS) kirri Daniel Wõsso'l walja antus.
Eenistja, Jaan Tullus +++
Manistja, Jakob Uibo xxx
Manistja, Michel Wõsso +++
