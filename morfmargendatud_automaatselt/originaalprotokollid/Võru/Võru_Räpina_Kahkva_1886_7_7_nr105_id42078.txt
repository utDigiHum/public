(Tulli ette Märt Sepp nink kaebas et temma om Sohwi Lepsoni hendale essa lubaga tütrikus palganud ja om 1 rbl. käerahha andnud enge nüüd ei olewat Sohwi Lepson temma manno teenima tulnud.)
Hindrik Lepson jäie ette tullemata.
Tulli ette Peep Brandt nink tunnist et Hindrik Lepson olli lubanud ja ema et tüttar Sohwi Lepson wöib Märt Sepp manno palgata.
Tulli ette Sohwi Lepson mansaisjaga ja Märt Sepp.
Otsus
Möistis pääkohtumees Kusta Rämman et Sohwi Lepson piab omma leppitu aig Märt Sepp ,am ärrä teenima.
(Möistis pääkohtumees Kusta Rämman et Sohwi Lepson piab selle eest)
Möistis kohhus et Sohwi Lepson piab kolmanda jao aasta palka nimmelt 10 rbl (10 naela willo ja) Märt Seppal wälja massma, selle eest et temmal teenma ei ole lännü Märt Seppa manno tenima.
Kuulut Sohwi Lepson et temma selle otsusega rahhole ei ole.
Kuulut Märt Sepp et temma selle otsusega rahhole ei ole.
