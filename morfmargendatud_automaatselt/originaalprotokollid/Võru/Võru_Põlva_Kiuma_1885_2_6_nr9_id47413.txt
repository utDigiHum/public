Kaibas siit wallast talunik Karl Olesk, et Hurmi mõisa Tille sepp Michel Kärsna ja Serrestest pois Peter Huul olla, nagu tema kasutütar Leena Molok tälle ülesandnud, minewa aasta Decembri kuu keskel wägiwaldselt tema aita selle tüdruku juure tunginud, kes seal öösel maganud. Sepp M. Kärsna olla rauaga luku lahti wõtnud ja siis P. Huulil sisse waatama minna käskinud. See olla sisse läinud, tüdrukut eest leidnud ja sellele ütelnud: "Mis tarwis sa kurat ust mitte lahti ei teinud?" Peale selle olla nad tüki aea pärast jälle äraläinud.
Kaebaja nõudis seaduslist trahwimist öösise hulkumise ja wägiwaldse sissetungimise eest. Wargust ei olnud.
Mihkel Kärsna ja Peeter Huul wabandasiwad, et nemad olla tol aeal ainult seal K. Oleski talu lähedal tõisel talul P. Wesi juures asjatalituse pärast käinud ja sealt otseteed kodu ümberpöörnud ja kusagil kõrwal mitte käinud - üleüldse  ei teadwat nemad sellest sissetungimisest midagi ütelda.
Tüdruk Leena Molok seletas nõudmise peale, et need mehed olla küll sel selkombel tema juure aita sissetunginud. Tema ei olla mitte healt teinud.
Kohtu poolest otsustatud:
"See asi luku oma woliga lahti wõtmise eest Kohalise Maakohtu kätte seletamiseks saata."
Peale selle oliwad kaebduse alused walmis, Kaebaja K. Oleskiga kergemal kombel äralepima, kui see seda järele oleks andnud. Lepimise põhjuseks ütlesiwad nad edaspidise Kohtukäimise kulu ja aeawiite olewat, aga ei mitte süüd oleku pärast.
Otsus seaduslikult ettekuulutatud.
Pääkohtunik: Joh. Nilbe [allkiri]
Kohtunik: Jaan. Aripmann [allkiri]
Kirjutas Zuping [allkiri]
d.: Joh. Aints [allkiri]
