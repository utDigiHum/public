Jaan Kissand Lokkotaja perremees kaibas kohto een, et temmä welli Gustaw Kissand 9 aastast sadik taggasi kui Jaan Soldatis om tahheto anda, ei olle Gustaw Kissand sel körral seddä ärrä selletedo Lehmä rahha 7 Rubla weel mitte täembä päiwäni ärrä andno Jaan Kissandi kätte; Ja ka weel paar wakka eggäst wiljäst mis Jaan Kissandil ommast jaost sada om janod Gustawi käest.
Gustaw Kissandi käest sai küssitu üttel temmä et rahha om ammugi ärrä olno masseto. Ja wiljä ei pea ollema suggugi anda jänos ärrä lahkomisse aiga.
Gustaw Kissand üttel ollewa 7 Rubla Janil massa.
Kohhus mõistis et Gustaw Kissand peab 6l Dezembril 1871 se 7 Rubla rahha Jaan Kissandil ärrä masma. Willi jääp polele seni kui tunnistsust saap.
