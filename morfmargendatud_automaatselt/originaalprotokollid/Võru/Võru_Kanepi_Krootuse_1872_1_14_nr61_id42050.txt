Jusal sel 14 Jaanuaril 1872 olli koggonna kohhos koon Pä kohdomees Jaan Tullos
                                                                                               Kohdomees Jaan Wärs
                                                                                                T                   Petre Mandli
Ette tulli Eisri Mõtsa wahd Jaan Waher. (Mis 14nel Januaril poolele jäi) sai 11 Webr. mõisdetus ja kaiwas: et Karaski walla peremees Adam Mäel ollew Eisri mõtsast 7 jalga walmid puid ära warastanu. Hendrik Märdimägi wõiwet seda tunnistada 
Adam Mäe kutsuti ette, küsiti, kuis sa säält olet nii warastanu? Adam Mäe üttel; mina ei ole üttegi puid Eisri mõtsast warastanu, enge Ehamaru kõrdsi mant ütte Pälloste mehe käest ostnu, arwata 5 jalga. 
Tunnistaja H. Märdimagi kutsuti ette, küsiti: Kuis wisi wõit sa seda tõttes tetta, et Adam Mäe ne puu Eisri mõtsast om toonu, ent Hendrik Märdimägi üttel, mina ei tija muud kui, ma tulli hommungu maast üles, nink näi kui Adam puid tarre kandse.
Et ne puu om Adam Mäe parsilt löitu: Mõist kohus Adam peap Eisride 150 kop ära masma, et ta edespäidi ei tohi ööse puid osta. 
                                                              Pakohtomees Jaan Tullus  XXX
                                                              Kohtomees Jaan Wärs  XXX
                                                              Kohtomees Peter Mandli  XXX
