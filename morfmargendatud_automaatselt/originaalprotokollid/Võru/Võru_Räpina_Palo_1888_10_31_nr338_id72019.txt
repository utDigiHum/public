№ 330  Ida Kiwitar Writz Kiwitar man olekin tuli ette ning wastud mina ei ole Jaan Wija aijast mitte kapstid warastanu ega toonu aga latse oliwa nee kapsta nüüd ara söönuwa nii et Wijale enamb taggasi es saa anda, ja ma käsksin ka Jaan Wijal toll koral neid kapstid ärä wija kui nema walla politsei ametnikuga sääl oliwa.
Mõistetud:
Et Jaan Wijal 100 pead kapstid warastetu ja Ida Kiwitar pool loitu politsei ametniku tunnistuse pera.
Otsus:
Peab Ida Kiwitar 2 rbl. kapsta hinda Jaan Wijale wälja masma ning warguse eest 48 tunni oma leiwa pealt kogukonna kohtu wangin olema 2 nädali sees.
See otsus §§ 773 ja 774 järele kuulutetu. 
