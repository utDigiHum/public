No 276 protocolli edesiminnek.
Tulli ette Jaan Oinling nink tunnist et nemma olliwa lännüd körtsi õdangu arwata kella 7aiga ja sis olliwa wiina wõtnud ja olli joonud ja olli Joosep Tepanow laulma nanud ja sis olli Joosep Wöikand manno lännüd ja olli üttelnud et körtsin ei tohi laulda ja sis olli Johann Urgard neide küssitelema naanud ja olli 2 körd jallaga rindu löönud, ja 1 körd käega körwa pääle ja olli ka süllitanud.
Tulli ette Joosep Tepanow nink tunnist niidasammuti kui Jaan Oinling ja perast tunnist et kats körd om jallaga löönud üts körd om süllitanud ja kats körd om russikaga kõrwa pääle löönud.
Peter Oinberg jäije ettetullemata.
Tulli ette Johan Urgard nink and ommalt poolt tunnistajas Hindrik Lepund, Daniel Glaasmann
Otsus
Peter Oinberg, Hindrik Lepund, Daniel Glaasmann ses 12 J
Pääkohtumees eest: H. Leppisk
kohtumees: Kristian Kübar
kohtumees: Joosep Sults XXX
kirjotaja: F. Wreimbaum &lt;allkiri&gt;
