Sai see kihelkonna kohtu käsu pääle seda kirjatäütmises perra nõutus kelle kätte see raha om jäänu Nemwalz ütles et ta om raha küll saanu ent siski Kiil herra kätte ära andnu 1 Aprillil 1880.
Herra von Kiel ütles: Et ta seda raha mitte saanu ei ole, küll om tema seda käsknu päärahade ette arwata mis mitte taidetu ei ole.
Tunistaja Jaan Seeba ütles et tema tõtteste mitte ei mäletawad kas see raha om Nemwalz masnu wai mitte.
Tõise kohtu mehe Jaaan Porila ja Peeter Wals ei ütle ka mitte mäletawad.
Mõistus 
Kogokonna kohus mõistab Peeter Nemwalzil see raha katte nädala sisen ära massa.
Otsus kuulutedi kohtun käüjile § 773 ja 774 perra ette millega kohtun käüja rahul olliwa.
äratäidetud.
