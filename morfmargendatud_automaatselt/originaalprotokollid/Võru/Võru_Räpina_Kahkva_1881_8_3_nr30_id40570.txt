Kaiwas Mõtsa-Esand Jakob Rosneck, et Writs Runhansoni tütar Marja, Paap Sabali tütar Marja, Jakob Parmani tütar Marja, Semen Samoilowi tütar Liina ja poig, Kusta Söukandi tütar Ann, Jakob Parmsoni naine Liina, Peter Kübari tütar Mari omawa mötsast raotu päält hainu poimnuwa. Mötsawaht Jaan Lepland on näid kinni wötnu ja ega üte käest zirbi panti wötnu ja sija kohtu manu toonu. Ann Karpson om ka warastetu hainuga kätte saadu aga mitte mötsast, enge söödu pääl om tema kinni wöetu. Tema, Rosneck nöwwab neide wargise käest ega üte päält 5 Rbl. trahwi.
Ette kutsutiwa need 5 nimitedu tütarlast ja 3 wiimselt nimitedu täiskaswanud inemist ja tunistiwa köik oma süüdi üles, et nema oma küll raotu päält haino pöimnuwa. Pallewa andis.
Otsus.
Kohus möistis, et Writs Runhanson, Paap Sabal, Jakob Parman, Kusta Söukand, ega üts oma nimitedu latse eest 50 kop., Semen Samoilow 2 latse eest kokko 1 Rbl. ja Peter Kübari tütar, Mari, Jakob Parmaksoni naine Liina ja Ann Karpson nee wiimatse 3 ega üts 1 Rbl. trahwi Jakob Rosnickule masma selleperast om neile latsile wähembas trahw jättetu et nema suurte inemiste kihotamise wai juhatamise perra seda kurja tennu.
Pääkohtum Johan Lepland
Kohtum J. Kirhäiding
Kohtum J. Oinberg
See otsus sai neile kohtukäijatele sellsamal päiwal Talorahwa Sääduse 1860 a. §772 ja §773 pöhjuse pääl kuulutedus, mis pääle nema kõik rahule jäiwa.
Liisa Kütsmann ära masnu 1 Rbl.
