Tulli ette Wiido Söugand nink tunnist et Joosep Wöikson om temmal nink ka Kusta Parsonil oma maa nink ka lehma eest ärrä massetu.
Tulli ette Paap Kiiskmann nink tunnist et Joosep Wöikson om temmale selle asta est rükki körre eest ärrä masnud enge lehma est om saamata, ja ollew Wöikson Kusta Parsoniga paiwa tennud ja Kusta Parson om üttelnud et temma selle rükki körre eest midagi ei tahha.
Pääkohtunik: Peter Urtson
Kohtunik: Kusta Rämman
Kohtunik: Joosep Oinberg
Kirjotaja F Wreimann &lt;allkiri&gt;
