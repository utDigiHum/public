Selle Suure Wöörkso wastse Kolimaja ehhituse perrast teggi nimmitud Koggokonna wallitsus omma otsust nita:
se rahha summa mes selle lutterusso Kolimaja ehhituse päle otsast otsani üllesläheb saas Koggokonna Kassa sulla rahast waljamaksetu, ning ni, et se rahha Summa mitte päraha masjede hingete päle ei lange, ent jääb se raha, kassast hobis wälja antu.
Se Protokoll saap allandlikult Keiserliko Kihhelkonna kohtule ette panto.
Nöumees Wido Using
Nöumees Josep Lepson
Nöumees Otto Lemming
Nöumees Sare Josep Lepson
Nöumees Josep Jusar
Nöumees Jakob Parmak
Wöörmünder Jaak Leppind
Wöörmünder Kustaw Kesselmann
Wallawannemb R. Parmatzow
