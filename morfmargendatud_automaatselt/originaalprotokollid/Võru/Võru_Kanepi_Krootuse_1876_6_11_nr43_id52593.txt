Teiselt, tulli ette emmand C. Dulder ja kaibas et Liso Ilda olnu hainamaa sissen nink poimnu hainu, ja nõüdis kahjo tassomist 3. rubla. 
Kutsuti ette Liso Ilda, sai kaibus ette loetus, kes selle pääle ütles, et latse lätsiwa tsuklema, ja minna weidikenne peoga kakse sääl es olle kuige wõrra. -
Otsus  Kohhus moistis, selle et om keeletu aijal hainamaa sisse lännu hainu otsma, massap Liso Ilda, emmand C Dulder´il 1. rubla, mes 
8. 1/2 päiwa sissen ärra massetu peap ollema. - Sai kohtomoistminne ja § 772.n 773 kohtunkäujatelle ette loetus, nink lätsiwa rahhun wälja. -
                                                               (Allkirri)
