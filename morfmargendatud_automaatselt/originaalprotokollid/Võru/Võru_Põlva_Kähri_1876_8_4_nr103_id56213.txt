Ette tulli Serrestest Peter Peitli naine Liis ja kaibas Jaani naise Mai Haak pääle kes tedä oli pesnü ütte suure puuga ja näütas oma puusa, mes kül pessetu oli. Ja ähwartawad weel lüwä. Ja nõwwab et tu trahwitus saasi, tunistaia om Jaan Rõiwas.
Saije Mai Haak ette kutsutus ja see kaibus kuulutetus, ütlep et temä löönu selle eest et Liiso Peitli oli tedä sõimanu.
Tunistaia Jaan Rõiwas tunistab, et temä om nännu kül kui Mai Haak oli Liiso Peitlile nuijaga löönu, aga tülli ei ka sõimamist ei ole kuulnu.
Otsus:
Kogokonna kohus mõistab, et Mai Haak peäb kolm päiwa türmin istma selle eest et temä Liiso Peitlid om löönu.
Otsus kuuluteti § 773 ja 774 T.S.
Pä Kohtomees Andres Nikopensius
Kohtomees Peter Nemwalz
  " Jaan Leib
  " Andres Kuklas
