Protocolli № 142 seletamiseks wastab Triino Pillberg. Riim tahtis neid paberid ise osta ja selle tingimisega et C. Pillberg oleks pidanud selle eest wastutama, kui nende üle midagi ütlemist saab tulema, selle juures wiskas C. Pillberg paberid kasti ja ütles kui ma kõik kahju olen jõudnud kanda siis jõuan selle ka. Patenti ei ole keegi lubanud tagasi wõtta kui mite ei käi.
Luik wastab: Sellepärast on see patent minu nime pääl et Pillberg sellajal ise kohtu all oli ja tema nime pääle ei antud Maakohtust mite patenti wälja ja nii wõtsin mina siis oma nime pääle.
Mõistus. Kogukonna kohus mõistab, et
Carl Pillberg oma naesega peawad see patendi eest makstud raha 9 rubla 60 kop. 2 nädalisse T.r.s.r. § 1074 järele Kusta Riimile wälja maksma.
Äratäidetud.
