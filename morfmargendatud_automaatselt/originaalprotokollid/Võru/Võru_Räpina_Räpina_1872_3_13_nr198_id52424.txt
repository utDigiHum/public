Writz Soekarusk Tositest Kristian Punnissoni rentnik kaibas, et Kristian Punnisson perris perremees om aastaja rendi puud 2 süldä ärrä wõtno mis Writz Soekarasakil om piddäno sama, ja 4. palki om widdäno Writz senna kolimaja rehhemanno mis temmäl suggugi tarwis es olle widdädä kässo päle om temmä widdäno kontrahti neil ei olle.
Kristian Punnisson üttel et temmä puid ei olle lubbano selle perräst temmä ei olle andno.
palgid om selle perräst piddäno widdama et sääl elläs Hindrik Kiudow kauba käemees tunnistas et temmä ei olle middägi kulno puide eggä palgi widdämisse kõnne perräst Kristian om üttelno et ni kui olled ene elläno ni elläd nüüd ka.
Jakob Liiskman tunnistas et temmä ei olle middägi kulu puide perräst kõnnet muud kui 7 rubla tader ja sinna elläd ni samma nüüd kui enne, om Kristian üttelno enne om 6 rubla tader olno et walla palgi widdämist ei olle selle perräst om Writz 7 rubla nüüd tadre eest piddäno masma.
Paap Heddosk tunnistas et temmä ei olle kulu middägi.
Writz Soekarusk nõuwap palgi widdämisse eest 75 kop palgi päält, 2 rubla puu sülle eest.
Kohhus mõistis et Kristian Punnisson peab Writzil taggasi masma palgi päält 75 kop tük ja puu süld 1 R 50 kop kokko 6 Rubla.
§773 om ette kulutedo.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Kristian Rusand
Kohtomees Gustaw Tolmusk
