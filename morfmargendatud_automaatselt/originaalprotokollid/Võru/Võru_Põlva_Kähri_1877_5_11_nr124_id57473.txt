Ette tulli Jakob Troon ja kaibas Peter Kirber pääle, kellega temä mööda lännu aastal oli lepinu ütte tükki maad wasto sel aastal weel maad saada.
Peter Kirber ütlep temä kül lepinu, enegi sellega et weel tuud aia maad ka sel aastal temä tetta woib, kohe temä mööda lännu aastal sitta oli pandnu, kui Jakob Troon tuud lask sis andwad temä ma
Otsus:
Kogokonna kohus mõistab et Jakob Troon saab oma lepingo perrä 1/2 waka maad Peter Kirberi käest hendäle selles tud tõwus tettä. Peter Kirber ei sa oma aija maad enämb pruuki Trooni käest.
Otsus kuuluteti § 773 ja 774 perrä T. S.
Peter Kirber es ole rahul ja saije opus kiri antus, selsamal päiwal.
25 Mail päält kiri antu
