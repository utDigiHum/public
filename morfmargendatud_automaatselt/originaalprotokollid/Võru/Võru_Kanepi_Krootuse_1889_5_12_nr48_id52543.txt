1. Mari Aras Luteri usku /j.s. Jüri Kramp tuli siia  kogukonna kohtu ette ja kaebas, et minul on Adam Leis käest 35 Rbl teenitud palka saada. Sellepärast palun mina kogukonna kohut Adam Leis sundi, seda mino heaks ära maksa laske.
2. Adam Leis Luteri usku wastutas selle kaebduse peale, et minul on küll Mari Aras heaks 35 Rbl maksa, aga ei jõua praegust maksa. Palun edaspidise kokko tomise peale.
" Kog. kohtu mõistmine;" 
Adam Leis peab 35 Rbl Mari Aras heaks ühe kuu aea sees wälja maksma.
Mõistmine kuulutadi Seaduse järele; siis /olid mõlemat sellega/  es ole Mari Aras sellega rahul waid palus opuslehte wälja, mis ka lubati.
/:Ära kiri 31 Mail s.a. K. V. T. k.k. är saadetud:/
