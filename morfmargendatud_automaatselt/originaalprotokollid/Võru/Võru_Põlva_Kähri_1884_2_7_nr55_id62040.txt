Pääkohtumees Johann Waks kaebas et Jacob Järw oma tüttartega olla temale  abikohtum. Wasserile  kolgale ja Woolmöldre Aadam Kroonile kui nemad Jacob Järwe juure on läinud pääraha wõlga sisse nõudma häbematal kombel wastu hakkanud, Järwe tütred Sohwie ja Leena Mari on tema jalgu pidi ukse wahele pannud, kui tema on sisse tahnud minna ja on wiimaks ukse suutumaks kinni pannud ja ei ole enam sisse lasknud. Kui nemad ära on tulnud, siis olla kiwidega wööruste järgi wisanud.
Jacob Järw wastab et k.m. Johann Waks olla ise jala wägisi ukse waheletoppinud sest et uks olnud kinni pantud.
Sohwie ja Leena wastawad, et nemad ei olla mite kiwidega wisanud ukse olla kül kinnipannud.
Aadam Kroon tunistab: et Jacob Järwe tütred olla Pääkohtum. Johann Waksi jalgupidi ukse wahele pannud ja on ka kiwidega järele wisanud.
Kohtumees Johann Wasser tunistab niisama.
Kohtumees Peter Kolk tunistab niisama.
Mõistus  Kogu konnakohus mõistab Jacob Järwe ja tema tütrid Sohwi, Leena ja Mari igaüks 3meks ööks ja pääwaks kogu konna turmis kinni istuma niisuguse häbemata kohtumeestele wastuhakamise eest kes ametitalituse pärast sinna majasse pidid minema ehk igaüks 6 rub. waestelaadikuse maksma.
Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtuskäijatele etteloetud. Olid otsusega rahul. 
