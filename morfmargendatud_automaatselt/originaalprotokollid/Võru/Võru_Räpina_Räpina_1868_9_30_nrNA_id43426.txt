Freitenfeldi herr tulli kohto ette ja kaibas et sullased om wasta neide tahtmist hobbesed ösis mötsa sööma winod, ja ei olle mitte jahho, Freitenfeldi herrä kässo päle hobbesile wälja wõtnod rokkas tettä ja ösi ei olle issi kedägi hobbesil perrä kaeno ni et Sussi ösi hobbest mahha om tõmmano ja ärrä sönod Rehhemanno nurme päle hobbene om 40 Rubla wäärt olno. Fretienfeldi herrä üttel 2 ööd om hobbesed jubba koddon olni tallin söman enne seddä sullased ütliwäd et mitte 1 öö ei olle hobbesed mitte koddon olnod.
Sullased ütliwad ka et Freitenfeld ja Schlogi Emmäd ei olle mitte tahtno jahho anda.
Gustaw Karos om seddä hobbest mõtsa wino sööma keddä sussu om ärrä sönod ilma et perrä om kaeto.
Tunnistusse perrast jääs polele
Pä kohto mees Paap Kiudorw
Kristian Wössoberg
​Hindrik Toding
