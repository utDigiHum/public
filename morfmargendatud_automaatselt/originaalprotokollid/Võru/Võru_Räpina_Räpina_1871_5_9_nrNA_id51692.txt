Wöbson sel 9 Mail 1871
Werro Lamburgi Herrä Krünbergist ette todo
Errastferest Haan Hark andis ülles, et temmäl 8 ja 9 Mai wahhel ösi om Errastferest temmä majast koddost rehhe alt 2 Märrä hobbest ärrä warrastedo 1 om 8 aastaiga wanna olno must hobbene 70 R wäärt 2 om 5 aastaiga wanna olno must hobbene 50 R masseto. Ja nüüd täembä om Jaan Wõbson neo hobbesed möllemad kätte sanod wargadega.
1 Hobbese warras Adam Tisler ka Errastferest üttel et temmä om selle hobbese ütte wennelesse käest ostno 43 R eest ja om Wöbson ütte mehhel 46 1/2 Rubla eest ärrä münod se om 8 aastaiga wanna olno.
Se kes selle hobbese om ostno om Wennemaalt Salna külläst Jaan Märtson, Audowa Kreisi Remjä kuntori alt. üttel ka 46 1/2 R masno.
Ja selle wasto kes hobbese om ostno om Adam Tisler üttelno hendä Ottepäält ollewad. Wankre om temmä Polgastest ja taossed, warrastano ja look Fritsi majast Michel Sorril
Taossed om Räppinäst Tanni Adam 3 R 75  kop eest ärrä ostno, Wanker ja look om muido kätte sanod.
Tanni Adam tunnist ka issi et temmä neo taossed om ostno 3 R 75 kop eest. 5 kop om päle wõtni napsi rahhas.
