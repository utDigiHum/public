Kaibas Wiido Narustrank: Temal ja Widrik Zirnaskil olnu Wenemaal haina ütte tettu üle kiige 3 kuhja, Üte kuhja olevat nema poolest ära toonu ja jäänu weel tuwwa 1 kolme koormaline kuhi ja 1 koormaline. Widrik Zirnask olevat nüüd see 3me koormalise kühja sinna jätnu. Selle perast nöwwab tema W. Zirnaski käest oma poole jao hainu eest kahju tasumist 20 rbl.
Wastutas selle kaibuse pääle Widrik Zirnask: Tema toonu küll see 3 koormalise kuhja ütsinda hendale ära, aga selle perast, et  Wiido Narustrank edimatse kuhja toomise maan hendale poolkoormat hainu rohkem wötnu ja weel pahemba perwe haina. Selle manu nõudnu tema, et mölembide haina saassiwa ära kaalutus, aga Narustrank ei ole sest midagi hoolinu.
Kostis selle pääle Wiido Narustrank, et tema paknu selle edimatse kuhja toomise manu oma koormat tema koorma wastu wahetada, kui tema rahul ei taha olla, aga Wid. Zirnask ei ole mitte wahetanu. Widrik Zirnaski nöudmist, hainu kaalu läbi jagada, ei ole tema mitte olnu. Seda tunnistab Kusta Sultson Räppinalt.
Kostis selle pääle Widrik Zirnask: et wahetust ei ole tema Narustranki pakkumise pääle mitte wasti wötnu, enge nöudnu kaalumist.
Räppinalt sai ettekutsutus Kusta Sultson, kes tunnistas: Edimatse kuhja toomise manu kui koorma olliwa pääle pantu, nakas Widrik Zirnask kiusama, et Narustranki koorma olewat suuremb. Narustrank ütelnu temale, et wahetame koorma ära kui rahul ei ole. Zirnask aga ei ole sedda kuulda tahtnu. Kodo tullen paknu Narustrank weel kors wahetust Zirnask aga jätnu köik rahule ja lännu ära kodo poole. Kaalumisest ei ole juttu olnu. Tema arwust ei ole Narustranki koorma sugugi suuremb olnu.
Need ütelused kohtukäijatele ette loetud, mispääle nema oma äraköneldu kördama naksiwa.
Otsus.
Kohus möistis: Widrik Zirnask peab 3 rubla Wiido Narustrankile masma ja jääb neil see ütine kuhi, mis weel sääl Wenemaal toomata om, Wiido Narustrankile kahju tasumises neide hainu eest, mis Widrik Zirnask ütsinda enamb ära toonu, kui ta pidi tooma.
Pääkohtu mõistja Peter Urtson
Kohtumõistja K. Rämman
Kohtumõistja J. Oinberg
See otsus  sai sellsamal paiwal mölembile kohtukäijatele Talurahwa sääduse raamatu 1860 aastast §772 ja §773 pöhjuse pääl kuulutatud, mispääle Wiido Narustrank 7 Weebruaril s.a. kohtule üles andis, et tema selle otsusega rahul ei ole.
