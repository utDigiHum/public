Karraski koggonna kohhos olli sel 2 Julil 1871 koon, Karraski ja Wija maija perremehhe tulliwa ette omme tannomide ja ma piri perrast wasta harna teed, Wija Jaan kaiwas et Karraski perremehhe omma tema ma wasta harna teed ärra kündnowa. Et koggonna kohhos säratse kardi ja Ma mõtmisse asjo ei tunne, kästi neil henda wahhel ärra leppi ent kumbki es olle rahho, sis jäi selle päle: Nemma tahdwa Kreis ma mõtjat tuwa, ja ma mõtja tominne, ja palk jäi koggonna kohdo otsosse perra selle perremehhe kanda kes selle asja sissen süüdlasses jääp. 
Karraski perremehhe olliwa sellega rahho, ja lubbasiwa sedda kahjo kanda egga üts hä melega kes selle asja sissen süüdlasses jääp, et se kingmas jääp tunnistawa perremehhe omma käega.
   Wijaan Mandli  XXX                                                                       Pä kohdomees Jaan Tullus
   Peter Plado  XXX                                                                                             Peter Mandli
  Ado Plado  XXX                                                                                                  Jaan Wärs
                                                     Kirjotaja H. Undritz
