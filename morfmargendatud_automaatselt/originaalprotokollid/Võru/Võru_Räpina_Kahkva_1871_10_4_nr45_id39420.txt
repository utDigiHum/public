Kaiwas se Mõisa rendi Herra et Mõisa moonamees Andres Kindsiraud om sel 3tal Oktobril temma tarre tullu ja tahtnu ommi päiwi selletata, Herra om temmale ütelnu et temmal mitte wõimalik Selletata ei olle, ja käsknu Andres Kindsiraua tarrest wälja minna, ja Andres Kindsiraud ei olle mitte wälja lännu, sis om temma A. Kindsirauda wälja tõuganu, ja to pääle om A. Kindsiraud Herrale omma moona ramatuga wasta wissanu, ja se ramatu Herra tarre mahha jätnu. Ja tämba ka A. Kindsiraud tööse lännu ja Herra Hobbesed se läbbi tallin saiswad.
Tunistaja Hans Tensmann üttel et A Kindsiraud om Herra een Ramatu mahha wissanu ja ütelnu sööge henda sisse.
Kohhus mõist et Andres Kindsiraud om Herra een Krob ollu ja käsknu Herral ramatut henda sisse süwa saab Andres Kindsiraud 15 witsu. Andres Kindsiraud pessa es lasse sis mõisteti 3 Rubla rahha trahwi.
Sel 18 Octobril om Keiserliko IV Tarto Kihhelkonna Kohtu herra se 3 Rbl trahwi A. Kindsirauwal mahha mõistnu ja mõistis se assemel A Kindsirauawale 50 witsu.
Kristjan Rämman
Kusta Hanson
Fritz Runhanson
