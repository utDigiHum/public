Ueber die Einweisung der Grenzen des dem Bauer Peter Pedras zufolge des am 15ten Nowbr 1877 abgeschlossenen Kaufcontracts, von der Hohen Krone verkauften Sibbula gesinder sub Nr. 42 des in Gouvernement Livland in Werroschen Kreise belegenen Krongutes Neu Casseritz.
Actum den 13. März 1878.
Nachdem der Gemeindeälteste Jaan Andrusse der Seitens des örtlichen Gemeindegerichts abdelegirte Gemeindegerichts Vorsitzer J. Hallop, sowie die benachbarten Gesindeswirthe Jaan Kassak sub Nr 40, Jakob Mihelson sub Nr 116, Hindrik Hallop sub Nr 42 Jaan Palloots sub Nr 46, Peter Kallasse sub Nr 38, Wiido Hanni sub Nr 44, Peter Orraw sub Nr 45, Peter Kesler sub nr 49 Jakob Hallop Nr 55, Peter Mäggi sub Nr 118, Jaan Kassak sub nr 119, erscienen waren, wurde zur Umgehung der grenzen des Sibbula Gesindes sub Nr 41 geschritten.
Parzelle I als Hauptgrund. Angefangen von der Eckkupitze am Zaunwege in gerader Linie bis zur Kupitze Nr 1 dann links bis zur Kupitze Nr 2 dann in einer Richtung bis zur Kupitze 4 u 4 von der Kupitze Nr 4 stark rechts in gerader Richtung bis zur Kupitze Nr 5 dann stark rechts bis zur Kupitze Nr 6, dann etwas links in gerader Linie bis Kupitze Nr 7, dann rechts bis zur Kupitze Nr 8, dann links bis zur Kupitze Nr 9, dann etwas rechts bis zur Kupitze Nr 10, ebenso bis zur Kupitze Nr 11, ebendso bis zur Kupitze Nr 12, dann etwas links bis zur Kupitze Nr 13, dann stark rechts bis zur Ausgangskuputze am Zaunwege.
Parzelle II. Angefangeen von der Kupitze an der grossen Poststrasse dann längst der Strasse bis zur Kupitze Nr 1, dann links bis zur Kupitze Nr 2, ann Rechts bis zur Kupitze Nr 3 dann stark links bis zur Kupitze Nr 4 dann stark rechst bis zur Kupitze Nr 5, von der Kupitze Nr 5 in gerader Richtung über die Poststrasse bis zur Kupitze Nr 6 an der Strasse, dann stark rechts in einer Linie bis zur Kupitze Nr 7, dann stark rechts über die Poststrasse bis zur Ausgangskupitze der Poststrasse.
Parzelle III. Angefangen von der Kupitze an der Lobensteinschen grenze Nr 1 dann in gerader Richtung über die gross Poststrasse bis zur Kupitze Nr 2, dann von der Kupitze Nr 2 stark nach rechts bis zur Kupitze Nr 3 dann von der Kupitze Nr 3 stark nach rechts in gerader Richtung über die Poststrasse bis zur Kupitze Nr 4 an der Lobensteinschengrenze, unbd dann längst der Lobensteinschengrenze bis zur Ausgangskupitze Nr 1.
Parzelle IV. Angefangen von der Kupitze under Salishofschen grenze Nr 1, dann in gerader Linie bis zur Kupitze Nr 2, sodann an der Kupitze Nr 2 nach rechts bis zur Kupitze nr 3, von der Kupitze Nr 3 stark nach rechts in einer Richtung bis zur Kupitze Nr 4 und dann von der Kupitze Nr 4 längst der Salishofschen grenze bis zur Ausgangskupitze Nr 1.
Parzelle V. Angefangen von der Eckkupitze Nr 1 in gerader Linie bis zur Kupitze Nr 2 von der Kupitze Nr 2 nach rechts bis zur Kupitze Nr 3 von der Kupitze Nr 3 nach rechts bis zur Kupitze Nr 4 und dann von der Kupitze Nr 4 bis zur Ausgangskupitze Nr 1.
Somit hatte die Grenzeinweisung und Grenzumbgehung vollständig stattgefunden ohne dass grenzsttreitigkeiten oder Differenzen in der Angabe der grenzen zu Tage getreten waren.
Gemeindegerichts Vorsitzer J. Hallop
" Alteste J. Andrusse
Kaufer Peter Pedrus
Nachbaren Peeter Kallesse
Jakob Michelson xxx
Wiido Hanni xxx
Peter Jakobson xxx
Peter Orraw xxx
Peter Kesler xxx
Jakob Hallop xxx
