Gottlieb Rööpson kaibas kohto een, et temmä om nänno kui Karl Konswik Räppinä körtsin jobno om olno ja Jakob Tolstberg om kätt puhhu ajano ja kui Gotli om üttelno et mis perräst sinna kätt puhhu ajad, siis om Jakob Tolstberg warsti lönod Gotlil kaega körwo mödä 2 körd ja 3 körd om Gottlil jälle taggasi wasto lönod, päle selle om jälle Jakob Gotlil Salliga kala ärä pesno
Jakob Tolstbergi käest sai küssitu üttel temmä et Gotli om teddä Jusist kiskno mahha wissano ja wargas sõimano, ja et temmä ei olle lönod mitte Gotlil.
Kohtomees Gustaw Tolmuks tunnistas et Gotli om eddimält lönod Jakobit.
Gustaw Kissand tunnistas, et temmä om nänno kui Gotli om pesno Jakobil kollakoga pähhä.
Gustaw Wikmanson tunnistas et temmä om nänno kui Gotli om Jakobi maad wasto lönod. Gotli kael om werrine olno.
Kohhus mõistis et Gotli ja Jakob Tolstberg kumbagi peawad 3 Rubla trahwi waeste ladikus maksma ehk wõtko kumbagi 15 witsa lööki selle körtsi kakkelusse eest.
§773 om ette kulutedo.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Gustaw Tolmusk
Kohtomees Paap Kress
