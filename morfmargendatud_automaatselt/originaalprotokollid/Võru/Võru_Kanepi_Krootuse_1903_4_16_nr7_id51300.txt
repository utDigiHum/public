1903 aasta Aprilli kuu 16 päewal ilmusid Krootuse wallaskohtu ette Kioma wallamajasse Krootuse wallast talupoeg Jaan Johani p. Seeme ja Kiuma wallast Johan Mihkli p. Rammul ja panid järgmist suusõnalist lepingud maha kirjutada ja kinnitada.
§ 1
Perisomanik Jaan Seeme annab oma Wõru kreisis Kanepi kihelkonnas Krootus wallas olewa Leesmiti talu № 1 rentnik Johan Mihkli p Rammulile ühe aasta peale se on 23 aprillist 1903 a kuni 23 aprillini 1904 rendile.
§ 2
Rentnik maksab selle talu eest raha renti ükssada üheksakümmend /:190:/ rublad ette ära 1 aprillil üks sada rublad ja 1 oktobril üheksakümmend /:90:/ rublad ja peale selle rendi ostab rentnik oma raha eest wiiskümmend 50 puuta sibi mulda /portreti/, mis tema ise peab wedama ja külwab kartohwli põllu peale, sellepärast et sõnnikut ei lubata ja rüa põllule ostab rentnik wiis kuue puudalist kotti luujahu, mille eest  perisomaniku käest tasumist ei sa
§ 3
Rentnik saab oma kätte kõik need nurmed, mis temale perisomaniku poolt ette naidates ja nimelt: Suured nurmed mes kuwwen nurmen omawad ja neli mis Liismetti talu №2 piiri wastu seiswa. Heinamaad saab rentnik oma kätte siin pool jõge Rotti poolne niit Laane niit ja nurme aluse soo ja kaks tükki aiamaad mõlemil pool tanumid, muido jääb kõik perisomanikule.
§ 4
Rentnik saab oma kätte kaks nurme rukkid ja kaks nurme ristikheina üks wana teine uus. Ristikheina nurme peab rentnik iga aasta hästi arima ja täwwe seemnega külwama. Rentnik peab kesä nurme pealt kiwi ära korjama ja koplisse unikusse wedama ja peab röa nurm kõik korratud olema ja rukki koige hiljem 15-25 August ära tegema.
§ 5
Rentnik wõib seitse wakamaad lina teha ja wiis wakamaad kartohwlid panda, kui rentnik rohkem lina teeb maksab iga rohkem tehtud wakkamaa linade eest, kui ka iga puuduwa wakamaa ruki ehk ristikheina eest kakskümmend wiis rubla perisomanikule ära wabandamist et maad mitte säetud ja mõõta ei mõista ei sa kuulda wõetud.
§ 6
Lina leotusest on pool õigust perisomanikul ja nurme tee mes selle talu nurmis labi läheb ei tohi rentnik üles künda, waid peab parandama
§. 7
Perisomanikul käiwad suwel kaks lehma ja lambad nii palju kui on, rentniku karjuse all karjas, ka lubab rentnik kõige kahju eest wastutada, mis tema karjuse läbi wõib sündida ja ei sa selle eest maksu.
§ 8
Õlgi, heinu, aganid ei ka põhku ei tohi rentnik laenata, kinki ei ka müia ei koha pealt ära saata, niisamuti peab rentnik ka linad kodus ajama ja rabama.
§ 9
Ilma perisomaniku teadmata nink tahtmiseta ei tohi rentnik tema kätte antud talu kohta ei terwelt ei ka jao kaupa ära anda.
§ 10
Walla, kroonu, kiriku magasi ja taadre maks ja tee seadmine ja muud sundimised, mes selle talu peal on ja mes tulewad peab rentnik ilm wastu panemata täitma.
§ 11
Tulekassa ja dessätini raha maksab perisomanik, aga kui peaks ette tulema tule õnnetus, weab rentnik materjali niisamuti ka iga walla talule manu kus õnnetus juhtub.
§ 12
Kõik õnnetused, mis pikse tule rahe sao ehk weiste katku läbi tulewad peab rentnik omas kahjus kandma
§ 13
Rentnik saab oma kätte järgmised hooned Rehetare, laut, küin, hobusetall ja kaks aita, teised hooned mes siin ei ole nimetud, ei puutu rentnikule ei ole ka nende üle mingi sugust õigust, ommawa perisomaniku walitsuse all. Perisomanikul om luba oma rehesid igal ajal peksä ja rentnik ei wõi keelda. 14
§ 14
Rentniku kohus on neid tema kätte antud talu hooneid ja nende katuseid hästi parandada ja heas korras hoida, niisamuti ja kõik aiad korra pärast parandada.
Perisomanik annab iga aasta üks koorma aia puid ja rentnik peab raguma, wedama ja ei sa selle eest maksu.
§ 15
Rentnik peab rukki korre ja ristikheina söödi sügise aigsaste ja hästi üles kundma suure leht adraga.
§ 16
Perisomanikul on se õigus et ta iga ajal ja nii mittu korda kui se tema meele järele on, seda talu koha talitamist wõib kas esi läbi waadata ehk teistest lasta katsuda.
§ 17
Rentnik paneb kautsjoniks kõige nende lepingu järele enese peale wõetud sundimiste pärast, üle kõige oma liikuw ja liikumata warandus, selle sees olgu kõige wähem: kaks hobust ja kaheksa karjaelajad, mis hea korra peal on üles peetud.
J. Seeme [allkiri]   J. Rammul [allkiri]
Üks tuhat üheksasada kolmandal aastal Aprilli kuu kuuetõistkümnemal päewal on Krootuse wallakohus Kiuma wallamajas eespool seiswad lepingut mis Krootuse talupoeg Jaan Johani p. Seeme ja Johan MIhkli p. Rammul Kioma wallast oma wahel teinud kinnitanud, mis juures Wallakohus tunnistab et lepingu osalised wallakohtule isiklikult tutawad on et neil seaduslik oigus on aktisi teha ja et Jaan Seeme ja Johan Rammul oma käega selle lepingule alla on kirjutanud.
Kohtueesistuja asemel D Paloson [allkiri]
Kirjutaja: KKUddo [allkiri]
№§
