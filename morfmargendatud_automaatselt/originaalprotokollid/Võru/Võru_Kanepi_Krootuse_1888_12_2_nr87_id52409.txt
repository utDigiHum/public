1. Mõisawalitsuse asemel S. Sibul tuli siia kogukonna kohtu ette ja kaebas, et Jaan Woolil on mõisale wana renti maksmata 194 Rbl 80 kop, mida seesama ära maksa ei taha. Sellepärast palun mina kogukonna kohut, Jaan Wooli sundi, seda mino heaks ära maksa laske.
2. Jaan Wool Luteri usku wastutas selle kaebduse peale, et minul on küll 194 Rbl 80 kop wana renti mõisale maksmata, aga ei jõua praegast maksa.
"Kog.kohtu mõistmine:"
Jaan Wool peab 194 Rbl 80 kop. mõisa renti, mõisawalitsusele 14 päewa sees ära maksma.
Mõistmine kuulutadi Seaduse järele, siis olid mõlemad sellega rahul.
