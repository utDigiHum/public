Teine kohhus selsammal päiwal nesamma kohtomehhe.
Tulli ette Sõrristo moisa mõtsawacht Jaak Hääl, moisawallitsuse assemel, ja Moisa Herra tähhega, nink kaibas Leesmetti tallo rentniko Jaan Kowit, pääle, nimelt, 
1, malt hainama wälja andmisse perrast. - 10     { ja nõudis tähhe perra mes Herra {
2,selt Sitta ärra wijmisse perrast. -              3      { kaest olli ette naudata   45 Rubla{
3,dalt Linnamaa wälja andmisse perrast,   20.
4,dalt Niidu teggematta jätmisse perrast, ja 12. Rbl
5,dalt Tahtis et kohhus piddi Jaan Kowit´ul, ülles ütlema, et Jürrij päiwal 1874 peap majast wälja minnema. - se olli suu sõnnaga ent ütles Herra käsu pääle
Kutsuti ette, Jaan Kowit, kes küsimisse pääle kostust andis. -
1,se punkti pääle, ütles minna ei olle märästgi hainamaad wälja andnu ehk numanu, enge wanna räbbastikko olle minna ommale koddapolitselle andnu kost temma - zirbiga hendal woip korjata sedda woip minno sullane tun(n)istada. -
Selle pääle kutsuti ette, sullane Rein Anton, kes walja ütles et niit ei olle, enge ikkes räbbastik, ja üts weikenne tee kottus olli kost wikkatiga woise lüwwa, nink haino wois ulle kige - toorelt kümme punda olla. -
2,selt  teise punkti pääle andis kostust ja ütles, et koddapolinne, eddimätse põhho om kolgastest sija tonu sedda om egga üts nännu, sis es woi minna ka koddapolitse sitta mitte keelda. -
3,ma punkti pääle ütles, se om kodda politse sittamaa, ja minna keeldsi tedda küll, enge temma es kuule minno keeldu, ja teggi ülle kelu, sedda nõwwetud trahwi woip temma essi kanda. -
4,ma punkti pääle ütles, egga minna teggematta ei jätta, röä leikus tulli pääle, nüüd om ka muil rahwal weel haina tetta. -
5,ma punkti pääle ütles, minno kontrakti aig ei olle weel mitte täüs, ja minna ei tija märrastki süüd tennu ollewat mes kontrakti murrap. -
Otsus  Keiserlikko Majestäti keige wenrigi essi Walitseja kässu pääle, moist kohhus, 
1,se punkti pääle, tunnistuse perra et räbbastik om olnu, mes ka haina saagist arwata om, massap Jaan Kowit, (1.) üts rubla Karraskij walla ladi trahwi, nink peap ne (10) kümme punda haino, kas koddapolitse kaest, ehk muijalt maija taggasi ostma, selle et, ei tohhi märrastki haina wälja lubbata tetta. -
2,se punkti pääle sitta perrast, kui koddapoline eddimätse aasta om Polgastest - põhho sisse tonu, sis om Karraskij koggokonna pruk, et koddapoline woip omma sitta sinna wija kos maad saap omma walla sissen. Sääl ei woi middagi saada. -
3, ma punkti pääle linna perrast, jääp tõise kohtopäiwa pääle, sest et koddapoline ehk linna teggija ei olle mitte siin. -
4,ma punkti pääle hainama teggemisse perrast, Jaan Kowit, peap hainama warsti ärra teggema, ei woi kohhus süüdi arwata, sest et mitman paigan om weel haina tettä. -
5,ma punkti pääle ei woi sesinnane kohhus üttegi süüd leida, mes ülles ütlemisse wart, ehk kontrakti murrap, päälegi kümme päiwa pääle Jakobi päiwa. -
Sai kohto moistminne ja § 773.n. 774. kohton käüjatele ette loetus, ent Serristo moisa wallitsusse assemik es olle sellega rahhul ja pallel Protukolli, mes kohhus lubbas anda. - 
                                                           Eenistja  Jaan Tullus.  XXX
                                                           Mannistja Peter Mandli. XXX
                                                            Mannistja Jakob Uibo.  XXX
Protukoll wälja antu sel 10.mal Augustil 1873.
