Ette tuli Jaan Kopli ja kaibas, et tema olewat 20 N zigari tubakut oma raha eest Tanil Rästale ostnu, se tubak massis 2 rbl. pund, aga nüüd ei nõwwa mina mitte enamb 2 rbl. enge 20 N tubakut. Ka anni mina Rästale 1 mõõt suwi rukki maha külwata, nink 10 N ratta witsa rauda, seda arwamine 60 Kop. Pääle selle tegi tema üte aasta mino seppikoan mino riistoga tööd nink palut ka weel mino seppikoa saina ära. Rugani külla anni mina Rästale, alasi, kruustükki ja wasara, - wassar om weel Rästa käen. Katte hobesega künni mino sulase temale maad nink äesti esi üte päiwa: Minul om sis Tannil Rästa käest  nõuda:
20. nakla tubago eest                              3 rbl    - kop.
1 mõõdo rükki eest                                   1 rbl  50 kop.
10 N rawwa eest                                                 60 kop.
seppariistade kulu                                   6 rbl    - kop.
kündmise ja äestamise eest                   3 rbl    - kop.
                                      Summa               14 rbl.  10 kop.
oma wassart, mis Tannil Rästa käen om, nõwwa ma tagasi.
Kohtokutsmise pääle tuli Tannil Rästas ette ja andse kaibusele wastusse, et tema ei olewat mitte 20 N enge 10 N tubakut saanu. Rükki ei olewat tema mitte Kopli käest saanu. 10 N rauda ole ma saanu, seppa riistust es pruugi ma mitte muud kui ütsinda lõõtsa. kündnu ja äästanu ei ole mulle Kopli mitte. Jaan Kopli wassar om weel mino käen. Rugani küllan olli kül kopli allas ja kruustük mino käen, aga tööd es te mina mitte neidega.
Mõistus:
Tannil Rästas peab siin Jaan Koplile 14. päiwa seen wäljamasma:
10 N tubaka eest                           1 rbl     - cop
10 " rawwa eest                                      60 cop
seppa riistade kulu                      2 rbl    - cop
                                                     --------------
                                                        3 rbl. 60 kop.
Ent rükki ja maakündmise man ei ole tunnistust nink om see kaibus tühjas mõistetu. Kopli wassara peab Rästas tagasi andma.
Pääkohtomees: J. Kroon [allkiri]
Kohtomees: J. Pruwli [allkiri]
        "             J. Meister [allkiri]
Kirjutaja: J. Treu
