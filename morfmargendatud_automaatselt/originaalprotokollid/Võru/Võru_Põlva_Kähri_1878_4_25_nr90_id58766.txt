Ette tulli Jaan Holz ja kaibab ja nõwab Peep Wals pääle, kes temä tarren 19 aastat oli elanu ja nüüd olewad tuud tarre ärä münu ja nõwab nüüd selle tasomises 25 rubla nink weel oma wanembide perändust mes ka Peep Wals kätte jäänu.
Peep Wals ütlep et temä oli sääl tarren eläden Jaan Holz tätti ka pidanu, ja arwab sellega selle tarre hindäle jäänu olewad; nüüd olewad temä sedä 6.. rubla eest ärä münu, siski olnu tu tarre jo parandetu ja olnuwa temä kramist ümbre tettu.
Otsus:
Kogokonna kohus mõistab et Peep Wals peäb pool müüdu hinda mes om 3 rubl. Jaan Holzil ärä masma.
Otsus kuuluteti § 773 ja 774 perrä T.S.
