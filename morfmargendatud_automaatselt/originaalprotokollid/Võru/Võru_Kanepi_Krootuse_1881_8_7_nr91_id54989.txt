Jaak Põder kaibdusen Ello Wiga ja tõiste wasto haina põimmise perast /№ 78/ wastut
Sohwe Ilm /m.s. Joh. Mõtus/ selle kaibduse pääle, et temä ei ollew mitte Jaak Põder maa päält haina põimnu, waid mõisa maa päält. Perast ütel, et temä ütsinda ollew üle piiri põimnu, kos Jaak Põder temä rüpüst haina maha raputanu.
Otsus:
Ello Wiga, Juuli Ilm ja Sohwe Ilm peawa ega üts 50 kop. Jaak Põdrale kahjotasomises masma, selle et nemä Jaak Põder maa päält haino põimnu tunnistaja üteluse perrä.
Kui kog. kohto otsus ja Sääduse §§ 773 ja 774 kohtun käüjile ette saiwa loetu, sis es ole keegi selle otsusega rahul ja palssiwa protokolli wälja, mis neile lubati.
 
