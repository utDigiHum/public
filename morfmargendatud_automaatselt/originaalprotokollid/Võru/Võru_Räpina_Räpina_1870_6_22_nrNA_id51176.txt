Iwan Zengowi naine Gusti kaibas kohto een et Saksamamees Hörtman Räppinä monak teddä om pesno 2 näddäl aigo taggasi ütte hainä käppätäie perräst mis Gusti om põimno omma mona tarre läwwe mat, Saksama mees Hertmann om teddä pesno kollakoga 1 körd pähhä ja teist körd om selgä lönöd, ja kät piddi om tarrest wäljä widdanud.
Saksamees Juhann Hortman käest sai küssitu üttel temmä, et temmä ei olle mitte lönod, agga Gusti om Jama Moisa nurme päält Ristk haino põimisse perräst sannadega ette wõtno selle perräst et nemmäd ütten majan om elläno ja kui Herrä näeb et haino om põimeto ja ka neide süis ei arwato.
Tunnistusse perräst jääs polele 1 Julini 1870
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Hindrik Toding
Kohtomees Paap Kress
