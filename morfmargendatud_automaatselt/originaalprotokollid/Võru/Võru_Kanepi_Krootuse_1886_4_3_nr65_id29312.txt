Wallawanemb Samuel Raig kaibas. "Mina nouan seletust selle Raha üle, mis weel praego Walge Johan Mandlil aastast 1881 eest wallale wõlgneb, nink mis endine Wallawanemb Jaan Undritz kui sisse noutawa wõla ühes aasta Rehnungiga mino kätte on andnud, mina tahan seda Raha kätte sada.
Jaan Undritz, wallawanemb aastast 1885 ütel "Mina sain oma ameti wasta wõtmise ajal mino eel käija Michel Wõso käest seda Raha 6 R 79 1/2 kop kui sisse noutawa wõla oma kätte, nink Joh. Mandel es massa seda raha mulle mitte wälja, sest ta ütel, et ta seda Raha M. Wõso kätte olli äramasnud."
M. Wõso, Wallawanemb aastast 1881 ütel. "Johan Mandel ütel mulle, et tema om seda Raha Jaan Undritz kätte wälja masnud."
Wallawanemb S. Raig ütel weel. Kui mina kohto mees olli, siis olli kõrd se asi siin kohto ees seletuses ees, nink J. Mandel näitas oma massu Raamatu ette, nink Ramatun olli wõlg tasa tehtu. Kes seda Ramatun kirjotanud olli, ei mäleta mina enamb mitte.
J. Roht, selle aegne Kohtomees ütel seda sama wälja, mis esimene.       
Jääb poolele.
All kirjad
Kirjotaja A. Muhle.
