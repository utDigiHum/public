Rein Janikessing kaibas kohto een, et Gustaw Norrusk om temmäl andno Nutte tallo maa 1871 aastal kuwwes aatajas rendi päle ja eddimisse aastajas 1/2 maad ja perräst 5 aastaiga 2/3 Reinol ja 1/3 Gustawil, ja rent om pole maa eest 47 1/2 Rubla leppito. Agga nüüd peab Gustaw tahtma, et Rein majast ärrä lähhäb, ja Rein pallub ommal ja naise lastel ülles piddämist.
Gustaw Norrusk üttel, et temmä tahhab omma poiga sissi panda ja Rein woib poia manno sullases minda.
Gustawil peab ollema 6 aastaja päle Reinol maja piddämisse kontraht anto ja tetto.
Kohhus mõistis, et 1/2 maas jääs weel üttes aastajas Reino kätte ja teise aasta lasko Gustaw kohto läbbi Reinol ülles ütteldä.
§773 om ette kulutedo.
