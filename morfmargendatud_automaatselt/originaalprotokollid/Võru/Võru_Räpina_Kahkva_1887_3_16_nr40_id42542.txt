Tulli ette Lisa Tobrow nink kaebas et 21 Februar s.a. om temma Kachkwa möisa lännud ja om Jaan Kroon kotsile saanud ja om selle pinni suure tee pääle tulnud ja om temma palitu ärrä lahknud ja nöwap 11 rbl. selle palit eest,
Tulli ette Jaan Kroon nink wastat et see wõib kül olla te temma pinni seda kurja om tennud aga Lisa Tobrow om palitu sinna jätnud ja om üttelnud et kui Kroon see palitu ärrä lasseb parandata niida et ei tunne sis wöttab temma see palitu wasta.
Otsus
Möistis kohus et Jaan Kroon aiab 1 rbl. kahjotassomist massma Lisa Tobrowi et temma pinnist om Lisa Tobrow palitu ärrä kaksanud, ja palitu niida ärrä om parandada lasknud et kanda kõlblik om.
Talr. Seadr. §773 ja §774 suuremba kohtu käimisest kohtukäijatele kulutud.
Kulut Liisa Tobrow et temma otsusega rahul ei ole. /mansaisja Fritz Parmson
oppusleht sel 16 Märsil wälja antu
protocoll sel 13nel Aprillil saadetu
Pääkohtumees: K. Rämman
kohtumees: Kristian Kübar
kohtumees: Joosep Sults XXX
kirjotaja: F. Wreimbaum &lt;allkiri&gt;
