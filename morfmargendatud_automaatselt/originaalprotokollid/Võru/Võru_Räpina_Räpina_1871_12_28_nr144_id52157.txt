Fr Dannenberg Madam E Krappi rentnik kaibas kohto een et Madam Krapp om temmäle ülles üttelno, et temmä maad ärrä müib ja selle perräst temmä maja piddamissest ilma ja kontraht tühjäs jääp, ja et selle läbbi Dannenberg om kahjo sanod ja selle perräst 87 1/2 Rubla rendi rahha kohto kätte sissi masma seni kui se se assi ärrä saap selletedo koggokonna kohto läbbi.
Madam Krappi käest sai küssitu üttel temmä eest kostja Peter Jagomani läbbi et temmä maad ei olle sanod mitte ärrä müwwä ja tahhab et Dannenberg maad eddesi peab ja ehk kui Dannenberg ei tahha siis temmä wäele ei sunni teddä maad piddämä
Madam Krappi üllesütleminne Dannenbergi wasto om sündinon 19 Julil tähhe läbbi prottkoli ramato sissi kirjutamatta.
Et Dannenberg maad eddesi wõib piddädä se ütleminne om 7 Dezembril Ramato sissi kirjutedo.
Kohhus mõistis, et kui Dannenberg tahhab wõib mindä siis ellägo omma leppingo perrä eddesi ni kui neil essi hendä wahhel leppitu om. Kahjo tassomist Dannenbergil ei olle middägi nõudmist.
§773 om ette kulutedo.
Dannenbergil om Appelatsioni wäljä anto.
