Moisa wallitsus ja Peter Kirjutar tulliwad kohto ette omma asjo selletämä selle ajamäe tallo krudi ülle mis Peter Kirjutar kewwädi herrä käest ostis ja nüüd ennämb osta ei tahha ja 600 Rubla om Herrä kätte sissi maksnod.
Moisa wallitsusse nõudmissed om Petre wasto1.,			23 tadre 27 krossi maa eest renti			142 Rbla 50 kop		2.,			kahjo tassomist Märt Solnaskile			56 Rbla 15 kop		3.,			100 Rbla eddiminne käerahha mis moisa wallitsus wälja ei massa			100 Rbla		Summa			298 Rubla 65 kop		4 wakka röä Seemne eest mis Peter rohkemb om wäljä külwno massap moisa			12 Rubla					286 Rubla 65 kop		Siis moisa herrä lubbab wäljä maksa			313 Rubla 35 kop		Summa			600 Rubla		
Peter Kirjutari nõudminne om moisa wallitsusse käest1.,			90 palgi widdämisse eest 40 K tükki päält			36 Rbla		2.,			honed raggomisse eest			10 Rbla		3.,			200 süldä krawi lõigato 6 jalga lai 4 süggäw			20 Rbla		4.,			8 wakkama rükki mis pudus olli 1 w eest 20 R			60 Rbla		5.,			4 wakka rükki rohkemb mahha tetto			20 Rbla		6.,			6 2/3 wakkamad heinämaa eest			50 Rbla		7.,			200 leisikat õlgi ostetu 5 K leisik			10 Rbla		8.,			67 korma sitta eest 20 kop korma			13 Rbla 40 kop		Summa			319 R 40 kop		
Kohhus möistis Et Peter Kirjuta peab moisale ärrä maksma mis moisa nõudminne om ja moisa peab jälle Peter Kirjutaril maksma mis Peter Kirjutari nõudminne om.
Pä kohto mees Rein Pabusk
Kohto mees Gustaw Narrusk
Kohto mees Karli Jagoson
Kohto mees Jaan Kliman
Wido Oijow
Kristian Kress
Wido Narrosberg
