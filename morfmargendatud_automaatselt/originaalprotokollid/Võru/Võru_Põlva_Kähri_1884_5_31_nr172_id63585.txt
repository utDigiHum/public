Ette tuli Külmsoo talu peremees Widrik Ilwes ja palus oma soowimist protocolli üleswõtta.
Widrik Ilwes soowib Kähri walla Kõrge Kroonu Külmsoo talu № 23 mis tema käes alaliseks pruukimises on, ära osta ja tahab seda ostmise hinda 49 aastaga protsendi wiisil kroonu kassa äramaksa ehk kui jõud kaswab ka ennemalt seda teha ja lubab kõik need tingimised täita mis Keiserliku Baltimaa Domeni Walitsuse ringkirjas 31. Januarist 1879 sub № 129 ette on kirjutud.
Ühtlasi palub ka Widrik Ilwes, et ostmise kontraktis saaks nimetatud, et temale üttitsite maade ja teede pruukimine teiste külaperemeestega ühte wiisi oles.
Eesistuja Johan Waks XXX
Kohtumees Jaan Salomon
do Juhann Wasser [allkiri]
Kirjutaja M. Bergmann [allkiri]
