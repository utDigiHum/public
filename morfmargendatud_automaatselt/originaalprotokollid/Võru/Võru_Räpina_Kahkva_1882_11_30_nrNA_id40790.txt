Kaibas Kristjan Karpson: Temal olewad öösel 19/20 Oktobril sepikoast pistol ära warastatud. Sepikoa seina alt olnu auk kapatatud ja selle läbi waras sees käinud. Selle augu juures liiwa pääl olnu üts jälg, mis Jaan Kärtmannist näitab olewat. Tunistajad jäle möötmise juures olliwad Daniel Glassmann, Karl Lepland, Writs Woronow.
Nöwwab Jaan Kärtmanni käest 5 rbl. kahjutasumist pistoli eest.
Wastutas selle kaibuse pääle Jaan Kärtmann: Tema ei teedwad sest pistoli wargusest midagi ja ei olewad ka see jälg augu juures mitte tema jälg olnud.
Tunnistas Fritz Woronow: Sepikoa seina all augu juures olnud ütsa palja jala jälg ja sai Jaan Kärtmanni jalaga seda jälge möödetu, see jälg oli just tema jala järele, see on, tema jalg passis selle jäle sisse nii kui oma jäle sisse.
Tunistas Karl Lepland, niisama.
Tunistas Daniel Glasmann, niisama.
Tunistused kohtukäijatele ette loetud, mispääle neil asjaliku wastust ei olnud.
Kohus möistis, et Jaan Kärtmann peab Kristjan Karpsonile 5 rbl. kahjutasumist wälja masma pistoli warguse eest, selleperast et tema jälg on augu juures löitud, ja selle järele tema ka selle augu tegija ja pistoli warastaja on.
Pääkohtu möistja Joh. Lepland
Kohtumöistja J. Kirhäiding
Kohtumöistja J. Oinberg
See otsus sai sellsamal päiwal mölembile kohtukäijatele Talurahwa Sääduse 1860 aastast §772 ja §773 pöhjuse pääl kuulutatud, mispääle Jaan Kärtmann sellsamal päiwal kuulutas, et tema selle otsusega rahul ei ole.
