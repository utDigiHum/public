Ette tulli Liso Kirhäiding Gustawi läsk ja kunna temma jobba 7 ajastat ilm eestkostjada (ehk Wöörmündrida) om kiki lastega olnu ja neide warrandus hukka lät sis pallep hendale ja lastele esstkostjid säda, kes selle warrandusse ülle wallitsewa mes mehhest om perra jänu tälle ja lastele.
Kunna tälle ka seni ütteki eestkostjat ei olle säetu ja nisamma ka lastele; sis saije koggokonna kohto poolt tälle kats Wöörmündrit säetus kiki lasteka kokko.
1, selle walla Perrismapiddaja ja temma koolnu mehhe ja laste Essa Täddipoig; ja tütre Lotta ja poja Peetre Ristessa; Paap Parman.
2 Töine temma Wäimees Johan Käis selle walla koolmeister, kelleka temma Liso Kirhäiding rahhul om olnu.
Pääkohtomees G. Lepson
Kohtomees P. Sabal
Kohtomees J. Jöks
