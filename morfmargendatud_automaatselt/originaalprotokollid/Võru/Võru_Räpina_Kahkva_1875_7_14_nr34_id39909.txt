Ette aste Gustaw Zernask nink kaibas Sülga külla hobbose waht Jaan Kriggoland, ollew temma rükki (kos 4 möto külweto ollu) hobbostest labbi söta ka tallata lasknu, nink nouwap selle tükki eest 30 Rub. höb.
wäljanöudminne:
Jaan Kriggoland sai ette kutsutut ja wastutas, hobbose lännuwa mängoga rükki sisse nink temma ei ollew neist jaggo sanu pea säält wälja ajatu.
Kohtomees Josep Parmak tunnist, sötut ei pea ollema, ent röa ollno roassilo ja läbbi tallato.
Moisteto:
Koggokonna kohus on sedda kurja üllekaenu (sel 15 Julil s.a.) nink massap Jaan Kriggoland selle perra 4 möto rükki Gustaw Zernaskile kahjotassomises wälja.
Pääkohtomees G. Anderson
Kohtomees P. Sabbal
Kohtomees J. Parmak
