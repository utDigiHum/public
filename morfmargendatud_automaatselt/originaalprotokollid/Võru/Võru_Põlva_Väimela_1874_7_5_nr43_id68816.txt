N 43 Waimela kog. kohton sel 5 Julil 1874.
Pakohtomees P. Pichl
Abbi: M. Traks
dito: J. Waiso
40 Nomri kaibusse päle om Matli Waiso tunnist et Korzi emmand om tahtno kät wälja kunita ni om wotno pois Peter Ots temma kuhwlist kinni ja om ärrä lahhano, ja sedda temma ka om nanno et se pois sanna kuulmata om.
Mari Müürsepp om tunnistanno et Peter Ots om iks wasta pandja ei tulle mitme ajamisse päle ülles ja ei kule sukkugi sanna ja kaip ka ööse hulkin.
Peter Ots om tono omma koggokona lubba tähhe Wastse Lina koggokonna wallitsusse poolt komb wälja antu 18 Marzil s.a N 78 ja es olle mitte Peter waid Otto Ots nink temma parahha weel om wõlgo 14 R 90 Cop.
Anna Grunwaldti palwe om et temma seest teendrist walla saas ta ei woi tedda mitte ennam pittä. 
Tallorahwa sadusse ramato §386 pohjose päle om selle koggokonna kohto poolt
Moistus:
Peter Ots om Ann Grünwaldi teenistust seest paiwast wallale ja peap sist wallast omma walda taggasi minnema.
Pakohtomees Peter Pihl [allkiri]
Abbi Mihel Traks [allkiri]
dito Jaan Wäiso [allkiri]
Kirjotaja JohKangru [allkiri]
