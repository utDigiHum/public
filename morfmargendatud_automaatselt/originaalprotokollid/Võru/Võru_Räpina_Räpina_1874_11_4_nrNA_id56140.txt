N 226, 21 Oktobril
4 Nowembril Jakob Kirjutus Willemi karjus tunnistas, et temmä Konsa Jakobi nido päält om kindi wõeto, Meelwalt, mõtsa weren Mäggiotsa nido weren, ja nido waht ei ollewa läbbi mõtsa koddo laskno mindä.
Kohhus mõistis et eggä üts perremees peab omma elläjä päält kahjo tassoma 13 kop eggä ütte elläjä eest.1			Willem Jürri Suik 20 elläjäd			2 R 60 kop		2			Johan Kirjutus 18 elläjäd			2 R 34 kop		3			Jakob Jagomann 9 elläjäd			1 R 17 kop		Kokko			6 R 11 kop		
päle selle peawa ülle kige 1 Rubla pantmisse rahha masma eggä üts 33 kop.
Kahjo om mõisteto ülle kaemisse perrä
11 Nowembril peab ärrä masseto ollema.
Johann Kirjutus ja Kristjan Rusand ommawa selle päle lubbano et nemmäd eddespiddi Meelwä niite päle omma elläjäd ei lasse ja kui nemmäd laskwa massawad 1 Rubla elläjä päält.
Ja ni samma maswa Meelwa mehhed.
