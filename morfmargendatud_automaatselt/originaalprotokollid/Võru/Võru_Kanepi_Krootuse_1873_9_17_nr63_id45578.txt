Karraskij koggokonna kohhus, Jusal, sel 17mal Septembril 1873.
                                               Mann olliwa, Eenistja Jaan Tullus.
                                                    "        "       Mannistja Peter Mandli.
                                                   "        "                do      Jakob Uibo. -
Karraskij moisa perris Herra A. von Moller, kirja pääle sest 14mast Septembrist s.a. No 15. Sai koggokonna kohto poolt, selle Suure Jakopi tallo, rentnikule, Adam Raig´le ülles ütteldus; tallo müümisse perrast, ja sai temmal se, Suure Jakopi tallo ostja - Walgjärwe perremehhe, Adam Pallosoni, een kontrakt ette loetus, nink kik punkti temmal kahjo eest hoitmisses ärra selletedus. -
Jusal sel 17mal Septembril 1873.
                                                               Eenistja Jaan Tullus  XXX
                                                               Mannistja Peter Mandli  XXX
                                                                Mannistja  Jakob Uibo  XXX
