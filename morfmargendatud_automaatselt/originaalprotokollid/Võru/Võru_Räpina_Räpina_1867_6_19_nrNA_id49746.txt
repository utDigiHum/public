Räppina pallu küllä kubjas kaibab et  Nore Herrä monamees Peep Tilgand kes Nore Herrä manno omma tüttärt om hendägä ütten lepno tööd teggemä agga nüüd om Peep Tilgand omma tütre ärrä andno Leppikessile Kahkwa walda kärjan käima nink moisa wallitsusse nõudmisse perrä säält mitte ei olle taggasi tonod.
Se päle sai Peep Tilgand ette kutsuto ja küssito et mis perräst sa olled omma tütre Mina Leppikessile karja andno ja mitte Herrä kässo päle ärrä ei olle tonod taggasi kostis Peep Tilgand et temmä naine ei wõi tüttart nättä ja rõiwid ei anna ja om üttelno et minno silmägi ärrä tulgu temmä.
Moistetus sai et Peep Tilgand peab omma tütre Minä warsti taggasi toma ja omma kontrahhe perrä ellama nink weel päle selle 1 R hõbbe waeste ladiko trahwi maksma 26 Juni kuu päiwäni peab ärrä masseto ollema.
Kohto käijal om §773 ette loeto.
Kirjotaja J. Jaanson &lt;allkiri&gt;
