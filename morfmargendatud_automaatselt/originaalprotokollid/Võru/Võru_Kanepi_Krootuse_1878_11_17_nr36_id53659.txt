An Wiga kaibduse asjan Joh. Timpka wasto /vide № 31/ annab Joh. Timpka naine Martha seda tunnistust, et temä ei ollew midagi leppingut Annega tennu, muudkui seda üttelnu kui, kui A. Wiga tedä pallelnu:" Tule nopääle meie mano kui sa tahat." Töö man ei ollew An konagi terwet päiwa olnu ja ei ollew ka temä Anne konagi kutsnu oma töö manu. An ollew ka wõlskeeli kandnu temä ja tõise peremehe wahel, misperast temä sis tedä ärä ajanu.
Otsus: Joh. Timpka peap An Wigale selle töö eest mis ta tennu 1/2 wakka keswi ja 1 wak kartohwlid andma.
Kui kohto otsus ja Sääduse §§ 773 ja 774 mõlemba kohtun käüjile ette sai loetu, sis olliwa mõlemba selle otsusega rahu.
G. Weitz XXX
A. Org XXX
G. Asi XXX
Kirjotaja Luik [allkiri]
