Täämbätsel ülewan nimitedu päiwäl olli herra von Roth selle kogokonna kohto palwe pääle nee wõlg kirja mink pääle tema H. Sirack liikwa warandust om lasknu kinni panna selle kogokonna kohto ette saatnu oma pääwalitseja R. Hörmani läbi, kos sis kate wõlg kirja pääl, kellele H. Sirack oma nime ala om kirjotanu, kuus sada katskõmmend wiis Rbl. herra von Rothil H. Sirack käest saada om. Ka tunnistap H. Sirack üte tähe läbi kos ta jälle oma nime ala om kirjotanu, et kui temä mitte oma wõlga 20 Augustis 1879 ärä ei massa, et sis temä terwe warandus selle eest wastutama saap. Et temä aga oma Termini täitnu ei ole, sis tegi kog. kohus 
Otsuses:
Et Heinrich Sirack warandus saap awaliku wäljapakumise teel ärä müüdus.
Joh. Pähn XXX
Joh. Purasson XXX
Mihkli Laine XXX
Kirjotaja Luik [allkiri]
