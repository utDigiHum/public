204, 1 Oktobril
Päle ette luggemisse Mina Sabbalson 13 aastaiga wanna tunnistas, et sis kui Hobbese waht ärrä om länno sis om 2 Hobbest röä päle länno 1 Hobbene om Maddise Janil ja teine om Andres Loritze Hobbene olnod.
Wöölmünder Hindrik Punmann ülle kaeja üttel, et temmä om kahjo arwanod 1 1/3 wak röäd.
Kohhus mõistis, et Hobbese waht peab 1/3 röäd masma, ja Hobbese wahhi poig Andres Loritze karjus Jakob Munsk peab 1/3 röäd masma ja Andres Loritze sullane Gustaw Lambing peab 1/3 röäd masma, ja Jaan Norruski poig Wido peab 1/3 wak röäd Peter Kirrutsonil masma.
