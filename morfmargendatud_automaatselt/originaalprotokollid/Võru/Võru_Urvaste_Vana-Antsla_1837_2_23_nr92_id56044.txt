Sell 23mal Februaril 1837. Wõttiwa Püssa Marguss nink Toffre, selle Püssa Jucko ja Reino maja enda kätta, nii et Marguss Lukas lätt Reino maja, ja Toffre Kask lätt Jucko maja.
Ja lubbawa nii kui Ausa Perremehe Säduss kannab, Wacko Ramato perra orjata, ja kiik Krono ja muu moisa massu kanda ja täita, nink enda maja est holt kanda.
Se Leppink om tetto Kuue Aasta päle,
sedda tunnistawa siin allan tähendeto enda Risti alla pannamisseka
Marguss Lukas XXX
Toffre Kask XXX
