Kährist Tamme Johann Nikopensius kaebab oma poea Peetri pääle, kes ilma tema lubata olla omale laenatud raha eest tema maa pääle Jaan Tennasilmal 1 mõõt linaseemnit protsendi eest maha teha lasknud. Seda lina mina J. Tennasilmal ei luba waid nõuan teda omale tagasi, sellega et mahakülwetud seemni tagasi maksan.
Peeter Tennasilm wastab, et tema ei ole mite ilma isa lubata lina teinud waid tema lubaga.
Jaan Tennasilm ütleb: Minu perast wõib Joh. Nikopensius lina tagasi wõtta aga maksku enne minu raha 14 rubla ühes protsendiga ja 1 rubla töö raha wälja, raha on 1 1/2 aastad tema käes olnud.
Mõistus: Kohus mõistab, et Johann Nikopensius
peab 1 rubla linamaha tegemise eest ja maha külwetud 1 mõõt linaseemnid 126 kop. 1 1/2 aastalist protsenti ja 14 rubla 26 kop. 2 nädali sees Jaan Tennasilmal wäljamaksma ja pärib see Tennasilmal mahatehud lina.
Pääle selle mõistab kohus Johann Nikopensiusel ja tema poeal Peetril kumgil 1 rubla trahwi waeste laadiku maks seepärast et isa ei ole mite õigel ajal protsendi eest lina mahategemist ärakeelnud ja poeale et tema seda sääduse wasta on julgenud teha.
Otsus sai T.r.s.r. §§ 773 ja 774 järele etteloetud. Kohtuskäiad olid otsusega rahul. Äratäidetud.
 
