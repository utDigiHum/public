Peeter Leer kaiwas, et Peeter Naaber kari olnu temä röa pääl, mida temä kinni wõtnu. Nõudse kahjotasomist 5 Rbl. ja pandistamise raha 1 Rbl.
Peeter Naaber wastut selle kaibduse pääle, et temä kari olnu kül Peeter Leer röa pääl aga kahjo ei ollew temä eläja midagi tennu.
Kohtun käüja lepsewa mõlemba niimoodu ärä, et Peeter Leer ei nõua Peeter Naaber käest midagi.
Otsus: See kohus om lõpetadu.
Pääkohtomees Joh. Pähn, [allkiri]
KOhtomees Joh. Purasson xxx
do: Mihkli Laine [allkiri]
Kirjotaja W.Luik [allkiri]
