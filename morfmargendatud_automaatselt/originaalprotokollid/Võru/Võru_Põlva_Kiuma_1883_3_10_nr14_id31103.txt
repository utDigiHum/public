Ollid ettetulnud selle walla hiljuti ärasurnud taluomaniku Peep Rihmi pärijad: Poead Jaan ja Kusta, tütres oma eestkostjatega: Leena, Wiio ja Miina ja tema II abikaas Mai ja sai nendele kadund Peep Rihmi testament etteloetud, mis selle kohtu nõudmise peale Kusta Rihm 9. Webr. c. siia ette  oli toonud, kus ta juba lahti oli murtud. Seesama on 30.Octobr. 1868.a. kadund Põlwa õpetaja J.G. Schwartzi kirjutatud, Peep Rihmist ja V Tartu Kihelkonna Kohtust 11. Novbr. 1868 sub №  2072 kinnitatud.
Pääkohtunik: J Nilbe
kohtinik: H. Lukats
d: J. Aripmann
Kirjut.as.Zuping [allkiri]
