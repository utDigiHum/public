Saije poolele jäänu № 250 ette wõetus.
Tunistaia Jüri Kroon Tartost es ole tulnu.
Ado Ein ütlep et Adam Kroon om jo mino wihamees see ei ole mino tunistaia nisama ka Jüri Kroon neid ei wõtta mina wasta. Kaegu Kuslapu weel kodost järele ehk leiab ahjo lõhki ehk mõne roho katski sis kaiwaku weel mino pääle.
Kuslapu ütlep mina ole seeni ajani ootnu nüüd palle mina kätte; temä lubas minole muretseda.
Otsus:
Kogokonna kohus mõistab kaibuse tühjas.  Ado Ein peäb 5 rubla Michel Kuslapule puu põlletamise eest masma.
Otsus kuuluteti § 773 ja 774 perrä T.S.
Ein es ole otsusega rahul saije opus kiri selsamal päiwal opus kiri antus. Nisama es ole ka Michel Kuslapu rahul ja saije opus kiri selsamal päiwal rahul.
Pääkohtomees Jaan Luik
Kohtomees
18. Decbr Copia
29. Januar 1878 toine Copia
