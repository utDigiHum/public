N 39 Waimela koggokonna kohton sel 9 Augustil 1873 
Päkohtomees Jürri Waask Abbi Peter Pallo dito Jaan Zuchna 
Karl Tobre om tunnistanno, Jaan Waisweer om kats aastat temma man teenistussen olno, ja om masno aastast palka 20 Rubla mas koggokonna massu ja pool mõto linna semnit olli ka weel temma poolt mahha tettu, ja saije ka tema aost orritus. 
Kaebaja ei olli kotton.
Pakohtomees Jurri Waask
Abbi Peter Pallo
dito Jaan Zuhna
