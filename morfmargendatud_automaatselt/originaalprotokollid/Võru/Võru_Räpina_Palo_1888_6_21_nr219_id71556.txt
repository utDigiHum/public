Kaewas Simon Wadeikin Kusta Rööbson peale kea mino ja mino wenda ja õdede wöölmündre oli ning kõig mino ja ka perijade peranduse 84 rbl. 60 kop oma hoiju alla wõtnu om ning nüüd ära raisanu on mida ma nüüd tema käest kätte nõwano, keiserliko Wõru maa kohtu akti sunni järele sest 7 Maist s.a. № 6538. kus kästakse enne nõudmist selgeks teha ja mõista laske, siis palun tunnistajad Andres Wija ja Peter Kiwak ette kutsu ja küsida, nõwan ka sellele 84 rbl. 60 kop. protsendi. ja tänast päewa palka 1 rbl.
Peter Kiwak Kreega usku antse tunnistust Ado Raudsaar kõneli minole, et Kusta Rööbson om 1878 aastal om Mai Wadekin waranduse wöölmündris heitnu minoga ütten, aga kui paljo perandust ehk warra oli om minol tedmada, sest et Raudsaar sest midagi es kõnele.
Otsus:
Ses 4 Julis ette kutsu.
