Wõbsost Jürri Kaswand tulli kohto ette ja kaibas et: Et Maddisson ostis laudo Tuddoska käest Wöbson 100 tükki ja Tuddoska se lauwa müiä wennelanne om 5 tükki pudus jätno. Ja kui Maddisson koddost ärrä olli länno ja naine laudo widdämän olli lanno om temma turru kubja Jürri Kaswando luggema kutsno ja om neid 5 lauda mis pudus olli ärrä nõudno päle 3 körd nõudmisse Agga Tudduska ei siiski weel neid 5 pudusse lauda ärrä andno enne kui Polizei wannemb Unterwaldt om senna länno ja ärrä laskno anda.
Ja siis kui Jürri Kaswand neil 5 lauwal perrä om länno om Tudduska Jürri Kaswandat löno 2 körd eddimält om rindo lono ja mahha perräst kui Jürri maast ülles tulno om pähhä löno
Tudduska sai ette kutsuto ja küssito et mis perräst sa leid 2 körd Jürri Kaswandole? Kostis Tuddoska et temma om se perräst et Jürri lauda om kiskno temmäle üts körd löno käegä pahhä.
Tunnistaja lömisseman olno Lena Maddisson sai ette kutsuto nink küssito: Üttel temmä et eddimält lõi katte käega rindo ja löi perseli mahha ja perräst teist körd lõi käege wasto tõrwa kübbar läks mahha.
Jürri Kaswand küssis kõrwa lögi eest 10 rubla ja rindo lögi eest 5 rubla.
Kohhos mõistis et Tuddoska peab trahwi maksma essiti Jürri Kaswanol 2 rubla lögi eest ja kohto ladikus 3 rubla ilma asjanda tulli eest ja et kaubeldo laudo warsti ärrä es anna
§773 om ette loeto.
Pä kohtomees Rein Pabosk
Kohto mees Kristjan Kress
Kohto mees Karli Jagoson
Kohto mees Gustaw Narrosk
2 R om ärrä masseto
3 R saap Jakob Wikman käest wennemaalt
 
