Ette tulli Jaan Orraw nink kaiwas, et temma omme palke omma welle Kaarli Orrawa käest tahhap kätte sada mes temma essi om Jakob Kõiwo käest ostnu nink neide eest masnu 5 Rbl nink nõwap nüüd 7 Rbl.
Kaarli Orraw koste selle kaibusse päle et se om õige et temma om ostnu nink wiis rubla masnu, ent nüüd ei massa ka minna ennambat kui wiis Rbl. taggasi, tolle perrast et wanna essa olli ka neid palke raggoman.
Kunna nüüd Kaarli Orraw essi om 5 Rbl. omma wellele lubbanu sis om koggokonna kohtu poolt
Mõistus
Kaarli Orraw peap Jaan Orrawale 5 Rbl mes temma om palke eest wälja masnu nidawisi ärra masma et sel 23. April 1878 2 Rbl 50 Cop. nink tõine pool sel 10. Nowembri s.a selle koggokonna kohtu kätte ärra masma.
Mõistus sai ette loetus T.S. r. wälja antu nink prg. 773 n. 774 ärrä selletedus nink wimate prg. 881 oppusse leht wälja antus.
Päkohtumees J. Hallop XXX
Abbi Jõggewa [allkiri]
  "     Peter Kall XXX
Kirjotaja J. Raudsepp
Jaan Orraw ei jä rahhule mes perrast temma palke man tööd om tennu.
