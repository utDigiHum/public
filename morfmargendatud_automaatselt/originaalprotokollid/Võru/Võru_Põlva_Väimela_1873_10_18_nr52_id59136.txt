N 52 Waimela koggokonna kohton sel 18 Octob 1873
Man olli Päkohtomees Jürri Waask Abbi Peter Pallo dito Jaan Zuchna
Täämbätsel päiwal om Johan Meos kaiwano selle koggokonna kohto een et temmal om Jurri Musto käess saija 21 Rubl. mes neil om sano kewwade Moisakortsi kambren leppitus, kon temma tunnistaja omma olno, kortsimees Johan Kalling ja Ants Samoson.
Sepäle om Jürri Must ette tulno nink om wastust andno et temmal om weel Johan Meosile ülle kigge massa 5 Rbl 82 Kop temma rehknung agga tunnistap nida: et 92 Rbl 70 Kop om temma Johan Meosele masno komast olli 65 Rbl 52 Kop wõlga mahha masto, nink jäije een tullewa paega päle 27 Rbl 18 Kop, sis tullep weel paega ette taoste eest 4 Rbl ja rattaste parrantaminne 5 R. 
Sepale ei olle woino Johan Meos middagi woino kosta.
Tunnistaja Johan Kalling om tunnistano neide lepping olli tolkorral kewwade kortsinnan temma ella kambren, et neide lepping olli ennige 40 Rbl puhhas palk muido es olle tolle leppingo sissen middagi. 
Selleperrast et tunnistaja Ants Samoson siin ei olle jääb se kohhus tõise kõrra päle. 
Pakohtomees Jürri Waask
Abbi Peter Pallo
dito Jaan Zuchna
Kirjotaja JohKangru 
