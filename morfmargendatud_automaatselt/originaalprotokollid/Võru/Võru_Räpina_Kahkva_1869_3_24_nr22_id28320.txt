Kahkwa kohtu majan sel 24 März 1869
Kaiwas mõtsasaks J. Rosneek, et Willem Jürgenson om mõisa winakoa mant puid Wõbsu winu mes temale Peep Simmulman om kõnnelnu.
Tunistaja Peep Simmulman ütel, et wallitseja H. Zärens olles temmale ütelnu: Kui sinna mitte ei ütle, kes om puid winu, sis massat 10 Rbl trahwi. Sis om tema ütelnu, et temma nännu om, et Willem Jürgenson om winakoa mand puid pääle pandnu ja Hendrik Laugason om tarre mant pääle pandnu. Se om õdago pimmega ollu, temma om palja jalgoga ja om külm nakanu, sis om tarre lännu ja ei tija kos poole nema puijega lätsiwa.
Ette tulli Willem Jürgenson ja ütel, et temma ei olle ütte halgo puid kohhegi winu.
Hendrik Laugason ütel, et temma om paari üssa täuwe ossa pääle heidnu omma tarre läwwelt ree pääle
jäi saisma mõtsasaks kohtu es tule
