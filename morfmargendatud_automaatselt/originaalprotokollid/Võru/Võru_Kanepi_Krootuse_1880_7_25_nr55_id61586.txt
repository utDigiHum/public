Tulli ette Leesmitti tallode No 2 ja 3 perrispiddaja A. von Kiel nink andis omma rentnikudel kui nimmelt Adam Kogger Karel Kumm nink Jaan Wahter ülles anda sest et neide kontrakti aig teüs om, sis palle minna sääduslikkult neile kuulotada et nemma een tullewal Jürripäiwal 1881 rendi majast peäwa wälja minnema ehk kui kaupa sünnitawa sis jättan ka paika. -
Saiwa ette kutsotus ne Leesmitte tallo No 2 ja No 3 rentnikko Adam Kogger Karel Kumm ja Jaan Wahter nink kohto poolt ülles ütleminne kulotedos, kos neil kellegil selle wasta middägi ütlemest es olle ja lätsiwa rahhon wälja
                                                                                  Päämees J Tullus  XXX
                                                                                   Kohtomees S. Raig  XXX
                                                                                  abbi    do     J. Mandli  XXX
                                                      
