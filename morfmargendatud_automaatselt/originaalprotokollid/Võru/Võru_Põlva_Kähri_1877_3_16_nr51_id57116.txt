Ette tulli Kahru Jaan Kirber ja kaibas oma wanemb poia Johanni pääle, selle nõuga et ta tahab tedä hendä maiast ärä ajada. Ja lubab tälle 3 wakka maad ja ka sanna eläda; ehk täüt palka ärä massa. Johann ei olewad ussin tööle, olewad ka hooletu ja tahab tõist poiga  kodo wõtta.
Saije Johann Kirber ette kutsutus ja see esä kaibus ette pantus, ütlep kuis wõiwad temä sis esä mant enämb ärä minnä, sest esä wõtnu tedä jo teenistusest hendä manu tagasi ja lubanu ka maja maja õigusega  tälle anda. Selle pääle tulnu temä tagasi ja orjanu essä sõna kuulmisega. Ja tahtwad ka orjata. Ei mitte kohegi ärä minna, enegi esä ei saawad temäga sugugi rahol.
Otsus:
Kogokonna kohus mõistab, et Jaan Kirber jo oma wanemba poja Johann Kirberi teenistusest hendä manu tagasi om võtno ja maja kõige raud warandusega ärä lubanu, peäwa nemä ütten elämä armastusen, rahun ja ütteusen, esä Jaan Kirber om peremees nikawa kui temä eläb, ja Johann kui temä wanemb poig temä abiline ja töö tegija ja peränd temä surma maia periä. Enegi kui nemä tüllin eläwa saab tülli alostaia (süüdläne) häste trahwitus.
Nink saije ka neid nomitus.
Otsus kuuluteti § 773 ja 774 perrä tallorahwa S.
Jaan Kirber es ole otsusega rahul ja saie selsamal päiwal opus kiri antus.
Pääkohtumees Jaan Luig [allkiri]
Kohtumees P. Tallomees [allkiri]
J. Punnak [allkiri]
Päält kiri 23 M. antu.
