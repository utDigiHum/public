N 58
Leewi kogokona kohus, sel 25 Juli 1884
Man olliwa Pääkohtum  Jaan Thalfeld
kohtumees Johan Suits
" Kusta Moistus
Ette tulli Hindrik Kikkas ja palles kogokona kohtu een Wiro rentnikule Johan Pappeli üles anda, et tema sest 23 Aprillist 1885 aastal sest kottusest prii om.
Johan Pappel tulli ette ja olli selle ütlemisega rahul.
Otsus.
Se üles ütlemine Johan Pappelil kuulutedu ja wasta wõtnu.
Pääkohtumees [allkiri] Jaan Thalfeld
kohtumees [allkiri] Juhan Suits
" [allkiri] Kusta Moistus
Kirjotaja [allkiri] JohUibo
