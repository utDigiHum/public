Keiserliku Kihelkonna kohtu käsu pääle saije Liis Waldson kaibus Jaan Nemwalz wasta ette wõetus ja selletetus.
Kaibaja Liis Waldson es ole tulnu.
Kaiwataw Jaan Nemwalz oli kül ette tulnu.
Sellepääle arwab Kogokonna kohus, et Liis Waldson katte kõratse ette kutsmise pääle ette ei ole tulnu, ja et temä ütskõrd jo oma kaibust Kogokonna kohtust tagasi om pallelnu, temä kaibuse Jaan Nemwalz wasta tühja olewad.
Pääkohtumees Andres Nikopensius
Kohtumees Peter Nemwalz
Kohtumees Johann Nikopensius
Kohtumees Jaan Leib
