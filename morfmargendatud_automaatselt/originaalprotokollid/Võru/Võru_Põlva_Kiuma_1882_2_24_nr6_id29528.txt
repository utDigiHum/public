Tulli ette siit mõisast M. Kiwak ja kaibas et Hindr. Hamme olewat teda aidamehe ees laimanud, nagu oleks ta Tartus käies moisahobuste kaeru äramüünud, mida aidamees herrale edasi olla rääkinud. Mihkel Rundal wõida sellest ligemalt tunistada.
Hindr. Hame wastutas, et tema ei olla müümisest rääkinud, waid et see kot kadund olla.
Aidamees Jaan Sulby tunistas, et tema olla herrale nõndasama edasirääkinud, et kadunud olla.
Michl. Rundal ütles,et kaera kot jäänud Tartus, õhtul lihuniku tõllahoonesse, kust teda hommikul enam kätte ei olla saadud.
KOhto poolt soowiti lepingut, mis ka korda läks.
Pääkohtomees: J. Nilbe [allkiri]
kohtomees: A. Luk xxx
abiline: K. Kõrwe
Kirjut. as Zuping
