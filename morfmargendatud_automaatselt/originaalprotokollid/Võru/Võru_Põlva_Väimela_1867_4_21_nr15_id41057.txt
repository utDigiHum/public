N 15
Mihkli ja Jürri Sosaare lepping.
Täämba sel 21 Aprillil 1867. leppip essa Mihkli Sosaar omma poja Jürriga, nida et: essa annap Jürrile perremehhe ammet kätte, kige tawwalikku kuudwarraga, mes sädusse perrä Kauka majan piap ollema. 
2 hobbest
7 Lehma
5 Lammast
3 Zikka
2 ratta
2 rekke 
18 wakka Suwwe wilia semeni ja muido tarwilikku majan prugitawa tö riista, kelle eest Jürrin üttegi wõlga essal taggassi massa ei olle.
Essa Mihkli Soosaar xxx poig Jürri Soosaar xxx
Mann olliwa
Walla wannemb Peter Pihl xxx
Wöölmöldre Mihkli Runthal xxx
Koggokonna kirjotaja Joh. Kalling [allkiri]
