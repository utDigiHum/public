Ette tulli koolimaia ehitaia Meister Jaan Paulus ja kaibas Jaan Tamberg pääle kes Prangli töö eest massetu raha, liinan sisse-kirjotaten wõlsi sisse oli kirjotanu ja mõnd jagu raamatust wälja kratznu.
Jaan Tamberg selle kaibuse pääle küsitu ütlep et see ei ole tõisi, mes temä lask minol sisse kirjotada, sedä kirjotasi mina temä silma al sisse ja temä pand oma raamatu tasku ja hoidis ka alati omas taskus, mina ei ole rohkemb ei ka wähemb kellegi üles kirjotanu, ei ka temä ramatust mes wõimalik ei ole, midagi wälja kratznu.
Otsus:
Kogokonna kohus mõistab kaibuse tühjas selleperäst et kaibus n. süüd Tarto liinan om olnu.
Pääkohtumees Jan Luik
Kohtumees Peter Tallomees [allkiri]
Johhan Porrila [allkiri]
