Mõisa walitseja kaibas kohto een, et 17 Hurmi mõisa wilja kotti Jaan Kirst käen ollew ja nõuab neid kätte saada,
Jaan Kirst wastutas selle kaibduse pääle, et nee kotti ollew Warbus jaama wiidu, kost temä neid enamb kätte ei saawat.
Otsus: Kohus käsk Jaan Kirstul nee kotti jaamast ärä otsi ja mõisa walitsejale kätte anda.
G. Weitz XXX
A. Org XXX
G. Kolk XXX
Kirjotaja Luik [allkiri]
