Ette tuli Ilis Juul ja kaibas, et tema oli Aukskolniga leppnu koormat tagasi Riiast tuwwa 850 Kop. kolme kaalu eest, aga sulane Jaan Tuunas es too mitte seda minu Augskalniga leppitu kraami, enge oli kraamitoomise kaupa ära tennu nink nüüd ole mina oma woorirahast ilma jäänu. Juul palleb, et kogukonna kohus seda raha 850 Kop. Jaan Tuunas käest tagan otsis.
Kohtukutsmise pääle tuli Jaan Tuunas ette nink ütel, et tema olewat ütsinda selleperast peremehe leppingut Augskalniga murdnu, nink tõise kaupmehe kraami tagasi toonu, et kraami maha pandmine lähemb nink kalimb olewat olnu. Mina lepsi Jakob Aid käest Riast kaupa Rõuge tuwwa 30 Kop. puuda päält, tõi 3 kaalu Rõuge nink pidi selle eest 9 rbl. saama, aga Rõugen es lubata mitte enamb anda kui 26 Kop puuda päält. Jakob Aid olewat kraami pääle andja olnu.
Jakob Aid ütleb, et tema ei olewat mitte Tuunaga kaupa puuda päält tennu, enge see koorem olewat Friedrich Toomile ära antu, nink se olewat seda Tuunale andnu.
Friedrich Toom ütleb, et temal ei olewat määnestki koormat Riast tuwwa Jakob Aid'ga leppitu olnu. Toom ütleb kuulnu olewat, et Jakob Aid olewat Tuunast kutsnu koormat pääle pandma.
Aid olewat Rõuge mõisast 35 Kop. Dahrsneeki käest 30 Kop nink Silsoni käest 26 Kop. puuda päält saanu.
Mõisteti:
Kuna Jakob Aid om Jaan Tuunale koorma Rian pääle andnu, se om 30 puuda, Rõuge wia nink ei olewat, henda üteluse perra, määnestki kindmat kaupa tennu, sis peab tema seda wõrt woori raha, 8 rbl 50 kop., mis Tuunal Augskalniga kaupeldu oli siin 14. päiwa seen Illis Juul sulasele, Jaan Tuunas wäljamasma.
Pääkohtumees: J. Kron [allkiri]
Kohtumees: J. Pruli [allkiri]
       "              J. Meister [allkiri]
Henda kesken äraõiendanu.
