№ 19 - 4- Aprilist järeldatud:
Et Jaan Häelm senna pidule ilma kutsumata on läinud, nõnda ka oma lubaga pilli seal on wõtnud ja mänginud ja ei tea, kuhu peale selle pandnud, sest et seda pärast muude käes ei ole nähtud. Tõisipidi aga ka täielist tunnistust ei ole, et ta seda just warastanud on, mis ka juba sellest näib, et tõisel hommikul Jaan Häelmut läbiotsides seda mitte tema elutusest ei ole leitud.
Et kohtu soowimise peale leping nende wahel ei sündinud, siis järgmine
Otsus:
Jaan Häelmil on selle omawolilise talituse pärast 5 rubl. kahjutasumiseks Jac. Nilbele 2 näd. a dato ära maksta, sest tema hooletuse läbi oli wõimalik, et see pill kuhugille kautsi läks.
See otsus sai nendele Liiwl. Taal. Säd. § 773 järele kuulutatud.
Pääkohunik: H. Lukats
kohtunik: J.Aripmann
abiline: W.Wisse
Kirjut.as Zuping [allkiri}
