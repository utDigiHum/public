Teiselt sai Karrasky Moisa perri Herra Kirja pääle sest 16mast Julist se asi neile Walge Moisa rentnikudel, nink nimmelt, Johan Mandli nink Johan Raig'le, Koggokonna Kohto een ülles ütteldus, sest et neide wanna Kontrakti aig täis olli, ehk kui tahhawa wastset leppinkut Herraga tetta - massawa eentullewa ajal 600. rubla egga aasta Walge Moisa est renti.
Saiwa ette kutsutus Walge Moisa  Wanna rentniku, Johan Mandli, nink Johan Raig, ja se üllewal nimmetedu ülles andminne ehk jääminne ette loetus, nink neil es olle middagi selle wasta ütlemist, nink lätsiwa rahhun ärra.
(Allkirri)
