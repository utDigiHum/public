Kohus sel samal pääwal.
Need samad kohtomehed.
Läsk Katre Loodus, Hanso naine, nink tema täis ealine poig Michel Loodus, said kogokonna kohto poolt ette kutsutud, nink nende käest küsitud, mis, ja kui paljo warandust esast järele jäänud? Mis peale Katre nink Michel Loodus ütlesid. "Mingi sugust warandust ei ole järel jäänud.
Kogokonna kohto poolt saije Katre Loodus I. Wöörmünder jaetud tema poeg Michel Loodus, nink tõises Wöörmünder härra Rudolph Grünberg Karraskist. kellega ka Katre Loodus rahul olli.
                                                                                                    Pääkohtomes  J Roht
                                                                                                    Kohtomees  Ma Wõso
                                                                                                             "            J. Mandli
                                                                                                   Kirjotaja A. Muhle
