Kuwwendalt tulli ette Hilba mölder maa. poleterra peremees Jaan Wõsso, ja kaibas, et temma sullane Jaani poig Johan Raig ei kuul-
dawat sukkugi sõnna, ei tullewat maggamast ülles, ei läwat ka mitte tööle, ent ellawat omma tahtmist mööda, kui arwap sis lähhap tööle, ent kui ei tahha sis ei lähha ka mitte, ni kui minna joo mitto kõrda kohhut olle pallelnu, minna saa selle läbbi wäega suure kahju sisse, kui kohhus minno hätta ei parranda ja sedda asja ülle ei kulle, 
Kutsuti ette sullane Johan Raig, sai kaibus ette loetus, kes küssimisse päle mitte ütte sõnnagi wastust es anna. - muidu kui omma suurust olli täüs. -
Otsus  Kohhus moistis, selle perrast, et teedse, et Johan Raig´al paljo koerustikko sissen om, nink paljo päiwi, ja aigo perremehhel ärra wiitnu temmal 30. õige kibbedat witsa löögi, mes ka warsti sai ärra täüdetus, ja selle kässuga perremehhel kätte antus et peap illusaste sõnna kuulma, ja lätsiwa rahhun walja. -
                            Karraskij Kohtomajan sel 17al junil 1877
                                                                 Eenistja, Jaan Tullus  XXX
                                                                 Mannistja, Jakob Uibo  XXX
                                                                  Mannistja, Jaan Leppson  XXX
