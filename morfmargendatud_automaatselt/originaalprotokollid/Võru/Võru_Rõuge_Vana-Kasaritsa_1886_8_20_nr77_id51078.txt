Tannil Saks kaibas, et Peter Hannus ollew 1/2 wakkaala maad tema käest ära wõtnu mes täl jo 7 aastat tagasi Andri Tamme käest rendi pääle wõetu olnu, ja nisu pääle külinu, tema nõudwat kahjutasumist 10 mõõtu keswi, sest se olnu keswa maa, kohe tema keswa pääle oles tennu.
Wastut Peter Hannus, et tema ollew jo minewa aasta selle maa eest Tannil Saksale maa wasta andnu ja 4 mõõtu kaaru.
Nema es lepi.
Otsus:
Peter Hannus peap Tannil Saksale masma 10das Novembris s.a. 4 mõõtu keswi, et oma woliga maa ära wõtnu.
See kohtu otsus sai kohtunkäüjile kuulutedus prg. 773 ja 774 Sääduse raamatust 1860 aastast ära selletedu.
Pääkohtumees J. Kabist [allkiri]
Kohtumees P. Sillaots [allkiri]
       "            P. Klais [allkiri]
