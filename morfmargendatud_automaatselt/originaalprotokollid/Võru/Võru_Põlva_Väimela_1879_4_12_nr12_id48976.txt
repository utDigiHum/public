N 12. Waimela kogokonna kohus sel 12. April 1879 Koon olliwa:
Pääkohtomees Jaan Wäiso
Kohtomees Peter Hauk
d. Michel Jaaska
Wallawanemba M. Traksi een istmisel olli kiik walla perisplatsi mehe koon, nink kinnitiwa seda leppingut N 1, 1878, et kiik peremehe leppuse ja luppawa henda wehel siin kohto een niida, nii kui neide lubamises sääl nimitet prottokollin omma ärä kirjotet, et iga peremees massap abiraha, kui tule kahju johus, nink rehe tare ärä palas; sellesama päält wiis rubla, nink kui sest rehetarest wäljale päsnu tulest kellegile tõisele talu pidajale kes siin selle täütja luppawa olla, kas ait, kõlgus ja laut äräpallos; andas sest wije rubla wiisi sisse tulnu rahast 50 rubla; üts kiik kas üts neist nimitet hoonist ehk ka kiik nesama hoone ärä pallasse, saap iks seesama nimitet 50 rubla. 
Pääkohtomees Jaan Wäiso [allkiri]
Kohtomees Peter Hauk xxx
d. Mihel Jaaska [allkiri]
