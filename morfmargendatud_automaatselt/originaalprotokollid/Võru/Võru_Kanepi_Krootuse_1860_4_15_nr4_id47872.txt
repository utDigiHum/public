Kaibaja, Juhan Lukkats tulli ette kaiwas et temma omma tütrik An Läwi ka om wastsest tenistusse kaupa tenno, se päle ka 40 kop käerahhas andno, ent nüüd ei tahha An temma manno jäda, nink ütles et se rahha tälle palk rahhas om antu, ei ollew ka temma kaupa wastsest tenno.
An Lotus kutsoti tunistajas ete, ja tunist et iks Ann Läwwi om käerahha wõtno ja kaupa Juhhan Lukkatsika teno ehk temma naiseka tenno.
Koggokonna Kohhos moist tütrikot tunistusse perra Juhhan Lukkatsilekätte.
