Peep Meinberg tulli Kohtu ette ja andis ülles ettemma Jaan Laskar oma omma Krundi päle lasknu tarre ülles tetta Jaan Laskar oma kuluga ja se maja jääb ka Jaani ommas ja se maja saab eddimalt kümne aasta päle senna lubbatus, ja kui Jaan Laskor middagi sudi ei teja omma massu terminis ärra massap jääb se tarre eddesi sesamma platsi päle ülles ja selle ma eest mes temmal sinna selle maja ümbre piddatu saab andus peab 2 rubla egga wakkama eest rendi egga aasta masma ja Jaan Lasker lehm wõip ka Peep Meinbergi ma pääl karjan kaijä ilm hinnata ja wõip ka weel muid honit sinna ülles ehhita omma kulluga, rendi massap katte tärmini päle nimelt sel 1. Oktobril pool ja sel 1 April tõine pool.
Saije Jaan Laskar se lepping ette loetus ja lubbas sedda selle ma eest sedda rendi massa.
Man olliwa Pakohtumees Peter Niklaus
   "      "        Kohtumees Petr Jõks
   "      "         abbi    "       Writs Lepman
Kirjutaja L. Kann [allkiri]
