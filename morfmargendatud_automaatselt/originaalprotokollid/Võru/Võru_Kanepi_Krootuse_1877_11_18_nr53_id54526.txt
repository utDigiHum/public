Tõiselt tulli ette sel sel sammal päiwal soldati naine Ann Hirschoon temma wöölmünder Tannil Kerner ja kaibas et tem(m)al ollewa Johann Peitli käest saada, nink nimmelt ni kui allan om ülles anto, pallel kohhot et ärra massmisele sunnis, sest temmal ollewa ka omma waja, nink ollewa joba mitto aijastaiga ootno ent ärra tassomissest hää kombe pääl ei olle loota.
1malt, 1 1/6 wak rükki jahho.-
2selt,  2/3- wak heid ernit.-
3dalt,  1 -    wak heid keswi 
4dalt, 1/3 - wak ilma jahwatamada rükki
5dalt, 5 1/2 - wakka kartohwlit
6dalt, 1  -    lammas katte poojaga 
7dalt,  10  rubla selget rahha
8dalt, - 2 rubla utte paari wastside saabaste eest.
9dalt,   - lammaste willo eest ?
Kutsoti ette Johann Peitli sai kaibus ette loetus, kes selle pääle ütles, muido kiik saano ollewat kui ennege 1/3 wak rükki ei ollewat mitte saano, nink ütles ka hendal Ann Hirschooni käest saada ollewat kui nimmelt
1malt  - 4 kõrda olle minna Anne perrast Resto moisan käino egga woor kats rubla teep wälja  -       8 rubla
2selt -  1 kõrd Werrol kaino tolle eest                -                           --                                                           -        60 -
3dalt -  1 kõrd pulman käino minno hobbesega 1 päew                             -                     -                   -             60 -
4dalt -  1 paar kängi mes tütter ärra warrastanno   -                         -                                  -                      1   -
5dalt -  tatriko suurmide eest mes tütter    do          -                                      -                                -         1  -
6dalt  - tütter 2 rättikot ärra warrastanno   -                                   -                          -                             -  1 -  
7dalt -  Ann Hirschoon      1 undrik  saano                  -                                 -                                  -             1.  - 50
8.dalt-  Ann Hirschoonil rättik anto   -                               -                                       -                               -         30.
9.dalt - minno naise sõimamisse eest nõwwan minna                             -                              -                    3-  -
                                                     Johann Peitli wastane .                                            Summa                         17 rbl 50 
Kutsoti weel ette Anne Hirschoon ni sam(m)a ka temma woolmünder Tannil Kärner sai Johann Peitli mõudminne ette loetus kes 1se 2se nink 3da punkti pääle ütles, se olli too kõrd kiik sõprusen ni ärra tetto, nink olli kõnneldo minna olles temmal massno 2 rubla      90 kop nink suwwel töö mann awitanno, mes ka tettos sai, 4da 5da nink 6da punkti pääle ütles kae nüüd latse asja mes rummalussest om tenno 1/2 toopi surmit mes temma ommi lastega om ärra kiitno, nink üts wanna kakkeno rättinirmas nukko tarwis ärra pruukkino todda lugge temma wargosses mes üts tõine mees es olles tihhano kohto een ni suggost wäikest asja mitte nimmitada
7da kaibuse punkti pääle ütles, too wanna undrik ei massa konnagi ni paljo mes minno kätte sai, 8 punkti pääle ütles minnol om too rätti tük allale todda annan minna tem(m)ale warsti taggasi 9 punkti pääle ütles saal es olle märrastgi sõimamist, mes perrenaine minnole ütles noid sõnno ütli minna taggasi ja ei muud mitte middägi. -
Otsus  Kohhos mõistis et Johann Peitli peap Ann Hirschoonil se üllewanne ülles anto wilja kraam ni samma ka willa kiik lamba 8 päiwa aja seen ärra massma muud kui se 1/3 wak rükki mes Johann Peitli ärra salgas jääp säält mahha kos Ann Hirschoon lubbas tolle pääle tunnistust ülles anda, ent tolle selge rahha arwas kohhos täwweste wälja massa -  10 rubl
ja saabaste eest et mitte ei ollewa wastse ollno J Peitli utlemisse  perra tunnistust ka es olle moistis kohhos 
et Johann Peitli peap A Hirschonil wälja massma                         -                  -            1  -  25 kop
nink  katte nagla willo eest   -                                    -                                    -     -           1 - 
                                                                                                                                      --------------------------------
                                                                                                                         Sum(m)a        12 - 25 -
Nink Ann Hirschoon peap Johann Peitlil wälja massma 4 kõrd Resto moisan sõido eest a 1 rubla kõrra päält   -   4 rubla
Werro soido eest et essi om henda tallitusse perrast soitno nink Anne om ütten wõttno   -          -                               30 kop 
Pulma sõido eest ni kui nõudminne om                                    -                                           -                                     -            60 -
Kängi eest mes mitte ülle hinna nõudminne ei olle                    -                                     -           -                           -      1  - 
Undriko eest et om wanna ollno pool hinda nõudmissest           -                         -                                                -              75 kop
                                                                                                                                                                                     ---------------------------                                                                                                                                                                                            Summa  6 rbl  65  -
Nink jääp sis Johann Peitlil weel Ann Hirschoonil selget rahha manno massa 5 rubla 60 kop mes ka ni samma 8 päiwa aja seen peap masseto ollema, ent J Peitli muu nõudminne arwas kohhos tühjas
Sai kohtomoistminne ja § 772-773 kohton käiadelle ette loetus nink lätsiwa rahhon wälja
                                                 (Alla kirri nago üllewal)
