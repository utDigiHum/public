sel samal päiwal.
Kaibas Wido Parmson, temma üks wassik ollew karja man ärra lõpnu, nink pallub neid karjusse selle perrast ülle kulelda.
Karjuse tulliwa ette, ning awaldas üks neist, et töine ollew ütte lehma kes rukki tahtnu minna wastu löönu, ni karranu se lehm äkki wassika sälga ja wassik satanu maha, lännu weel karjale perra, ent perrast ödango poole ollew wassik maha satanu ja ärra löpnu.
Moisteto:
Kumbgi karjus saap 5 witsa lögi, selle et nemma karja häste es hoija.
Pääkohtomees G. Anderson
Kohtomees P. Sabbal
Kohtomees J. Parmak
