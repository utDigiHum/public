Seriste Herra von Kiel tuli ette ja andis üles et Kusta Kasuk on minewa aasta 76 waka kartohwlid widamata jätnud ja puid ragumata jätnud 28 sülda. Seepärast nõuan kontrakti järele makstud saada: 7 rubla 60 kop.kartohwlide eest ja 16 rub. 80 kop. ragumata puie eest.
Kusta Kasuk tunistab seda õigeks ja tõutab kewadini tasa teeni.
Herra von Kiel lubas 1 kuu aega weel oodata kus Kasuk need 76 wakka kartohwlid äraweab ja 28 sülda puid äraraguma peab ei täida aga Kasuk seda 1 kuu aja sees mite ära siis saab see nõutud suma kohtu poolt sisseriisutud.
Kasuk oli sellega rahul.
Kogukonna kohus kinnitab seda lepingut.
