Ette tulli Jaan Aija ja kaibas, et Jaan Andruse, Jaan Wassa ja Jaan Jakobsoni kari tema rüa orase sõknu, tema nõudwat kahjo tasomist 10 mõõtu rüki ehk 10 Rbl. raha. Kohtumees Peep Klais ollew ülekaenu.
Wastutiwa kaibuse pääle Jaan Andruse ja Jaan Jakobson et nema sellest röa söötmisest midagi ei teedwat üttelda.
Kusta Jakobson, et alaelaline man saisja esa Jaan Jakobson, üttel, et neide kari kunagi Jaan Aija rüa pääle ei ollew saanu, ent Peter Aija ja Jakob Puustusmaa karja ollew tema kül rüa pääl nännu.
Otsus:
Ette kutsu Jaan Andruse ja Jaan Wassa, Kusta Jakobson, Jakob Puustusma, Peter Aija, Jaan Wassa kõiki karjustega ja Jaan Jakobson.
Kohtumees J. Kabist [allkiri]
    "                P. Sillaots [allkiri]
    "                Peep Klais [allkiri]
