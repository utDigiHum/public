Moisa mona naised Kesselmanni Ewä Peter Wikkingi naine Karolina tulliwad kohto ette Ewa Kesselmann kaibas et neil om ütte wõrd ja ütte modo pohko olno moisa poolt anto. Ewa Kesselmanni pohk om taggast poolt lau maja pöningil errä södeto ja teisel om weel allale.
Kohto mees Wido Narrosk om ülle kaeno ja üttel et ütten koon ilma wahheta om põhk paika panto ja ütte wörd elläjäd om neil et teisel om weel 1/3 jaggo ja teisel 2/3 jaggo põhko allale.
Kohhus möistis Kunna neil ütte wisi põhk ja ütte paljo paika olli panto ja ilma selge wahheta ütten sööta ja ütte wõrd ellajed siis peab se ülle jäno põhk ka weel ärrä sama jäetus ni et ütte wõrd kumbagil saab.
Pä kohto mees Rein Pabusk
Gustaw Narrosk
Wido Narrosk
Kristian Kress
