Kui sel 1 Marzil Wanna kojolast Michel Sörg kaibas wasto Dawid Todingi Wöbsost ette olli loeto tunnistas Ossip Feodori Rainowast, et temmä om nänno, kui Tawid Toding Räppinä körtsin Michel Sörg kalbano ja kindad om ärrä wõtnod. Ja om ka nänno kui Tawiti kässi Mihkli puhhun om olno.
TawidToding üttel et temmä Räppinä lõrtsin öd om olno jobno ja teisi päiw om teddä läbbi otsitu Wöbso körtsin, ja ei olle middägi leidno ja kolma päiw hommingo om Michel Sörg ja Jaan Sok kinda temmä naise pole winud, ja sääl om Tawita naine omma kinda Mihkli kinda alt ärrä tundno.
Kohhus mõistis, et Dawid Toding peab Michel Sõrg kätte maksma se Mihkli nõudmisse perrä 6 R 20 kop. Selle perräst jääs Tawid ilma trahwita, et temmä ütte jodiko mõllemba olliwad.
22 peab masseto ollema.
§773 om ette kullutedo.
Pä koh
Dawid Toding om selle eest 15 witsa lögigä trahwito, et temmäl 2 aasta pärahha wõlgo om ja pälegi körtsin allati winä jodik om.
