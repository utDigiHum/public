Jusal sel 9. Aprillil 1871 olli koggokonna kohhus koon. 
                                                                 Man olliwa: Päkohtumees Jaan Tullus
                                                                                           kohtumees Jaan Wärs     
                                                                                                    do      Peter Mandli
Sai Wõrru Makohtu otsuse perra Juhhan Troonile ütteldus et täl neide warrastedu linnu eest 5 Rbl. 56 2/3 kp. Jakob Uibule massa om. Ent Juhhan Troon pallel hendaga Märdipäiwani 1871 kannatada. Sedda lubbas ka Jakob Uibo tetta ja tun(n)istawa sedda omma käekirjaga.
                                                     Jakob Uibo XXX                                Juhhan Troon XXX
                                           Kirjutaja ass. P. Undritz
