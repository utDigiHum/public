Kaiwas Wõbust Schlogi Prouwa welli Dreitenfelt, et Kahkwa Mõisa Aidamees Straus om neide Podist 15 Rubla 18 Kop. eest omma tütre mattuse ajas Ciggarid Rummi ja Weini wõtnus Treitenfelt om mitto körda Straussi man käinu wõlga otsman, ja Straus om iko massa lubbanum ent ei olle konnagi masnu.
Ette tulli Straus ja ütel et temma Treitenfelti ei tunne ja temma om Schlogi Proua käest omma Schwagari Rehnungi pääle kauba toonu.
Kohhus mõist et Straus om se kauba Schlogi Podist ilma omma Schwagerita wõtnu ja ei olle ka üttegi tähte omma Schwageri käest Podin ette näidanu. Ja se kaub temmale hendale ussutu om, sis peab ka Straus se wolla essi masma.
Peter Urgard
Jakob Parmson
Gusta Söugand
