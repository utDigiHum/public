Wöölmünder Josep Soeson tulli kohto ette ja andis ülles et Karli Heddosk om Maggasi Aitta tulnu ja üttelnud et temmä kott om ärrä kaddono mis 4 wakka keswi eest karadega täis olli, ja perräst es olle temmä ennämb kotti taggan otsnu.
Jaan Warres tunnistas et Karli Heddosk om issi omma kotti koddo wino karoga ja et temmä selgeste om kaeno.
Karli Heddosk üttel et temmä ei olle mitte omma kotti koddo wino.
Wöölmündri Josep Soeson üttel et kott om Maggasi aidast ärrä kaddono.
Jaan Warres tunnistas kindmaste et Karli Heddosk om issi omma kotti koddo wino ja karad mis kottis olli mahha küllinu.
Kohhus mõistis Jaan Warrese tunnistusse perrä et Karli Heddosk selle kotti wargsil wisil koddo om wino ilma ramatus kirjutamatta et Karli Heddosk peab 4 wakkal 4 wakka manno maksma se om kokko 8 wakka Maggasis taggasi maksma ja weel 3 Rubla rahha waeste ladikus trahwi maksma.
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Jaan Holsting
Kohto mees Jakob Türkson
Kohto mees Hindrik Toding
Kohto mees Paap Kress
