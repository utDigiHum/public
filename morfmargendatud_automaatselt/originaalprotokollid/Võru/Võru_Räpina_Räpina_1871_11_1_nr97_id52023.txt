Kui se 27 Septembri prottokol Jaan Zengowi kaibus Wõbsost Uibo lahkmisse ja Ubbina wargusse perräst ette ärrä olli loeto üttel Fritz Raudkütt et temmä peljoga om üttelno et temmä ajan om käino. Teine pois om teddä opaano kust aida saap.
Kohhus mõistis, e tWritz Raudkütt Jaani poig peab 2 Rubla kahjo tassomist masma ja Wargusse eest 24 tundi törmän ollema.
Wastastikko om ärrä leppitu.
