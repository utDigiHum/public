Hindrik Kiudorw Räppinä wallast rendi perremees kaibas, et temmä om Rassina wallast kes 1872 aastal Räppinä wallas sullases om olno, om temmä seddä samma sullast Hindrik Paap hendäle sullases palgano 1873 aastal palk om olno leppito 30 rubla, käerahha om anto 1 rubla ja 1 rubla om jälle käerahhas anto kokko 2 rub la kõnno küllän Räppinäl om 25 kop andno, Tammistul om 2 rubla andno weel ollewa kõnnon andno 40 kop kokko 4 Rubla 65 kop.
Hindrik Paap üttel et temmä om henda sullases palgano, agga selle perräst ei olle sullases tulno et Rasinalt ei olle lasto, ja ülle kige ollewa temmä Hindrik Kiudorwi käest sanod 1 rubla 45 kop
Hindrik Kiudorw nõuwap, et sullane Hindrik Paap temmä manno sullases peab tullema 
Hindrik Paap üttel, et teddä ei olle lasto selle perräst tullema muido kui wigo omma wanna Emmä Essä ja 2 wäikud sossart ütten.
Kohhus mõistis, et Hindrik Paap peab se 4 rubla 65 kop Hindrik Kiudorwil taggasi masma, ja weel 1 rubla trahwi waeste ladikus masma.
18 Junil 1873 peab masseto ollema.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Gustaw Tolmusk
Kohtomees Rein Lambing
Kohtomees Kristjan Rusand
