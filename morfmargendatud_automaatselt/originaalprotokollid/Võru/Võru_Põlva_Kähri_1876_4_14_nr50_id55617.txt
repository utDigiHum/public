Ette tulli Peter Raud oma kaibusse pääle August Hektor, Josep Haugas wasta mehitse wargose peräst ja selletas et nee nimiteto poisi olewad mööda lännü sügüsel temä mehitse ärä kisknuwa ja oma sis esi Jakob Järwele tunistanuwa, et Josep oli nuijaga tsusknuwa ja August oli met käega wõtnuwa, nink olewad tood mehist ärä riknu ja to olewad nüüd otsasaanu ja nõwab oma mehitse eest neide käest 20 rubla.
Josep Haugas oli ette tallitetu ja saije küsitus ütlep et nemä oma Augostega ütskõrd õdango kül sinna mehitse manu joosknuwa ja kaenuwa, sis oliwa ka nuijaga tarro külge zusano, ni et taru laud maha oli kukunu, enegi met ei ole wõtnu joht, mehitse tulnuwa kül ka wälja.
August Hektor es ole tulnu.
Jääb pooleli. Saawa ka tunistaja Jakob Järet ja Jaan Awwald ette käsutada.
Pääkohtomees Andres Nikopensius
Kohtomees Peter Nemwalz
do: Jaan Leib
 " Johann Nikopensius
