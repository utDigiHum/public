Kaibas Jakob Säksing, et Writs Hanson olewat temale 12 möötu rükki wölgu, mida Hanson terwe aastaig aiga laseb oota ja massa mitte ärra.
Wastutas Writs Hanson selle kaibuse pääle, et Jaan Kisar, kes nüüd ära koolu om, andnu temale nee röad wölgu ilma möötmata ja ütelnu, mööda kottun ära, ja siis nii palju kui saab, saat sina minule tagaso masma. Kottun olewat tema seda wilja äramöötnu ja olnu 4 waka ja 3 kar. Seda wölga on tema walmis nüüdsama ära masma, ehk ka sügisel seda massa.
Jakob Säksing kostis selle pääle, et terwe aastaig on Writs Hanson selle wöla pääle oota lasknu, selle perast ei ole sellega rahul.
Otsus.
Kohus möistis, et Writs Hanson peab see wölg 4 waka 3 kar. rükki Jakob Säksingile 15 Augustil s.a. wälja masma.
Pääkohtumees Joh. Lepland
Kohtumees J. Kirhäiding
Kohtumees ​​​​​​​J. Oinberg
