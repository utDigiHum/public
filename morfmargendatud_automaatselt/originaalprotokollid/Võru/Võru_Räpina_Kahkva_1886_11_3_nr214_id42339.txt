Räppina wöölmöldre herra A. v. Siwers kaebas sel 13 Octobril su, No 57 kirjalikult et Sohwi Hörbergile kes Kachkwa möisan teenib, sääduse peralist  trahwi panda, et temma kooli wanembat Jakob Leppindit, kes tulli Sohwi Hörbergi laste kooli trahwi rahad nöudma, kurja sõnaga om ähwardanud nink trotsnud.
Tulli ette Sohwi Hörberg nink wastat et temma Jakob Lepindit trotsnud ei ole ja abnnab tunnistajas ülles, Marja Otsing, Ann Käsnar, Marja Adosk.
Tulli ette Marja Otsing nink tunnist et Sohwi Hörberg temma kuulmise perra Jacob Lepindit mitte trotsnud ei ka ähwardanud ei ole kõnne om neil kül henda wahel olnud.
Tulli ette Ann Käsnar Thoma tüttar nink tunnist et temma sel korral sääl ei ole olnud ja ei tija ka sest asjast midagi.
Tulli ette Marja Adosk nink tunnist et temma om enna sedda wälja tulnud kui Jakob Leppind Hörberg tarre läts.
Otsus
Jakob Lepind ja Sohwi Hörberg 17dal Now. ette.
