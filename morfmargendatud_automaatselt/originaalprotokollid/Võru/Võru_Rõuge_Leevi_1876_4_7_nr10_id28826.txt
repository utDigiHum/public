Lewil sel 7 April 1876
Man olliwa Pääkohtom. J. Thalfeld
abbi kohtomees Joh. Sults
"  Joh. Kausaar.
Ette tulli Jakobson ja kaibas et Hindrik Rautsa temmale ütte wiggalise lehhma münu 15 1/2 rubla nink 3 päiwal perrast ostmist temma emmand Hindrik Rautsare pole lännu ja üttelnu et wõtke nüüd lehm taggasi sest temma om wiggaline ent Hindrik Rautsar enne naarnu nink nowap nüüd omma rahha ja pakkup lehma taggasi ent Hindrik Rautsar ei wõtwat ja ollewat tunnistaja sen asjan Jakob Nilbe ja Liso Himma ja Jakob Kinsiraud. Hindrik Rautsaar ütles et 3me näddali perrast olli tulnu Jakobsoni emmand sinna ja üttelnu et lehm om haige nink minno naine üttelnu et wahhest om asse halw ent minnu teedmise perra olli se lehm mümise man terwe nink es olle meil märast ke kaupa tettu ei kui lehm haige sis taggasi. I Tunnistaja Jakob Nilbe ütles wälja et tõisel hommikul perrast ostmist olli se lehm jo haige nink meije nõstime tedda ülles. II Tunnistaja Jakob Kinsiraud üttles wälja et muido se lehm mitte ülles es sa kui lõia otsast wallale lasti. III tunnistaja Peter Himma ütles et kui se lehm todi sis olli temma jo werrine nink meije tõstime tedda jo tõise hommikul ülles ent nüüd om se lehm, kui rohto om sanu ja terwemb nink üttel Hindrik Rautsaar et küündle kuul saap se lehm kandma nink se lehm olli tomise man mahha saddanu ea päle. Jakobson ütles ka et kaupa taggasi anda es olle meil joht tettu.
Moistet: Hindrik Rautsaar peap, sest et tunnistus om et lehm häddalinne om olnu, Jakobsonile 1 rubl. 50 kop. rohho rahha masma, wai wõttap Hindrik Rautsar se lehm taggasi ja wõttap ka Jakobson omma rahha.
Se moistminne om § 773 ja 774 perra kulutedu ja oppus leht antu.
Pääkohtomees [allkiri] Jaan Thalfeldt
abbikohtomees Joh. Suits XXX
" Joh. Kausaar XXX
N 10.
