Peeter Naaber Luteri usku tuli siia kogokonna kohtu ette ja kaebas, et Leena Sibul on minu teenistusest ära läinud, ilma, et temal mingit põhjust olleks olnud. Aasta palk oli lepitud minul Leena Sibula'ga 7 Rbl raha ja 1 leisik linnu. Sellepärast palus  kogukonna kohut Leena Sibulat sundi aasta palk tagasi maksa laske. Sellepärast, et ära läks.
2. Leena Sibul (m.s. Hindrek Sibul) Luteri usku wastutas selle kaebduse peale, et mina tulin sellepõhja peal ära, et Peeter Naaber mino lapsele süia ei anna, kuna meil see lepingus oli.
"Kog. kohtu mõistmine"
Peeter Naaber kaebdus tühjaks arwatud, sellepärast, et Peeter Naaber Leena Sibul lapsele süia ei ole andnud, kuna neide lepingus olnud ollew.
Mõistmine kuulutadi Seaduse järele; siis es ole Peeter Naaber sellega rahul; waid palus opuslehte wälja, edasi kaebamiseks.
/:Ära kiri 5. Septembril s.a. K. II T.k.k. ära saadetud:/
