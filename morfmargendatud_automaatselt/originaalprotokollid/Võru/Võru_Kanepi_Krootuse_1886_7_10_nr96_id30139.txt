Tulli ette Jaan Uibo nink kaibas, et om Wilga M. Punna käest 150 Rbl üts sada wiiskümmend Rubla wõlga saada, kos sel 1. April 1884 joba pidi masseto olema, nink om ka tolle ajast saadik protzent massmatta. Nink palun teda sundi et oma raha kätte saan, sest pikkembalt ei woi mina ennamb kannatada. -
Sai ette kutsotus Michel Punna, kaibus ja nõudmine temäl ette loetus, kes kiik ütles õige olewat, ennegi pallel kannatamise aiga, sest praega ei olewat mitte wõimalik ära massa. -
Leppisiwa henda wahel niida wiisi ära, Jaan Uibo lubas konni 2 Weebruarini 1887 om wõla nõudmisega kannatada, ent sis peäb kindmalt ära massetu olema, Michel Punna lubas seda ka teüta 
                                                                                                                      Pääkohtomees   J. Rocht
                                                                                                                      Kohtomees    M Wõso
     Jaan Uibole ärakiri 6 Septembril 1891 a.                                            Kohtomees   J Mandli
     wäljaantud.                                                                                       Kirjotaja as. R. Grünberg
      Eenistuja:   Kirjotaja: M. Tegelmant (ei loe nime välja/                                                                                             
