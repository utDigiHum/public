Saiwad järelseiswad mitmepoolsed talu rendikontrahtide ülesütlemised, mida nende pärisomanikud oliwad palunud, ettewõetud ja täüdesaadetud:
1. Selle walla Hattika talu rentniku Peeter Sawisaar'ile omaniku riiginõuniku proua C.v.Beyeri nimel wolitatud hra. E. v. Cossarti poolest tema siia saadetud kirja pääle 25. Juulist c.
2. Selle walla Matsi №5. talu rentniku Peeter Mustmann'ile hra. D. Margi poolest kirja läbi 19. s.k.pw.
3. Selle walla Troffi II. talu rentniku Hindrek Nagelile omaniku Jacob Kübarsepa poolest. Rentnik aga ei olnud haiguse pärast selkorral siia tulla saanud.
4. Selle walla Koolimaja № 26 poole talu rentniku Joosep Zuping'ile omaniku Jacob Zupinge poolest.
Neile kõigile sai üteldud, et nad kas tulewa Jüripäewani need talud maha peawad jätma ehk kui sünnib, siis omanikudega uued kontrahid kokkulepima.
Pääkohtunik: J. Nilbe
kohtunik: H. Lukats
d.: J. Aripmann
Kirjut.as. Zuping /allkiri/
