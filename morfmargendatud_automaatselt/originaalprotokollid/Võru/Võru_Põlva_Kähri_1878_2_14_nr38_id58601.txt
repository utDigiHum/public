Ette tulli Jaan Pinkson ja palles et Protokolli saas ülles pantus, et temä waestlast Jani p. Michel Mandlid jo kats aastat kui oma last om pidanu ja tahab ka et see temäle kasu pojas saas kinnitetüs, ilma et temä kaswatamise eest midagi nõwab, lubab ka henda man seppäs oppata ja ka perän oma surma oma wana seppä riista peranduses jätte.
Selle latse wöörmündri Jakob Liblik n. Peter Kirber lubasid sedä kül, kui enegi lats häste saas kaswatetus, opamisen manitsemisen ja karristamisen ja ka neil perrä lastas ülle kullelda.
Kogokonna kohtu poolt saab see kasu poja õigus lubatus ja Michel Mandli Jaan Pinkson kasu pojas kinitetus. 
Seda kinnitawa kasu esä Jaan Pinkson XXX
 Wöörmündri Jakob Liblik XXX     Peter Kirber XXX
Pääkohtumees Jaan Luik
Kohtumees Johan Punnak
   " Peter Tallomees
