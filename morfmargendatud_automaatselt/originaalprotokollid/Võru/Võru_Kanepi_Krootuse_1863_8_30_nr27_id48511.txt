Ette tulliwa Jaan Wärs nink Adam Raig ja Petre Läwwi Kakko maja perrast kedda perris Herra poold eddimalt Addam Raig ja Petre Läwwile olli ärra lubbatu.
Et Jaan Wärs sedda Kakko maja jo paljo aiga olli piddano, nink nüüd temma tõiste paika weddammisse perast Herraga nipea kaupa es olle woino kergel melel ärra tetta, olli kül Herra neile enne nimitet Adam ja Petrele  ärra andma,
selle et Herra neist es tija kas na jõudwa ehk ei sedda maja wõtta.
Et Kohdomehhe sedda asja parrem tedwa:  ne poisi nikrudi hirmo perrast sedda maja tahdwa ülle jõwwo wõtta, selle sai se assi perris Herrale teda antus ni kui ta olli. 
Herra polest kästi: Kui Adam ja Petre jõudwa sadda ja wiiskümment ruplat kautsjoni sisse, ehk towa täwwelik perremees endale käemeheks, sis woip kül neile maja kül anda.
Kohdomeestel es olle selle wasta middage ütlemist; anti aiga rahha ehk käemeest tuwwa, ent nemam es tu.
Se päle moist Kohhos maja wanna perremehhele Jaan Wärsile,
sest et selgest teda olli et neil muud lodost es olle kui maggatsini aida pole
