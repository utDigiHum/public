Seitsmendalt  Tulli ette Johhan Seeme ja kaibas, et Adam Kroon, ollewa 1. wak rükkij rohkemb wõtnu, kui temmal waija wõtta olles olnu,
 ja pallel kohhut A. Krooni sundi, et taggasi annas. - ehk peas saama. -
Kutsuti ette Adam Kroon, sai kaibus ette loetus, kes selle pääle ütles, et Johan Seeme, om omma woimuga minno käest 13. wakka röä seemend wotnu, ent 12. wakka olle minna omma peo läbbi mahha külwnu, nurm om taüs, ja täwwe seemnega külwetu, päälegi om J. Seeme, minnol omma suuga üttelnu, et kik nurme omma ütte suuru, kuis temma sis wäggisi tahhap minno waiwa hendale kisku, minna ei olle mitte üts terra temma omma putnu, enge om(m)a wõtnu. -
Otsus   Kohhus moistis, et tunnistajat neide wahhel es olle, Et pooleterramees, peap ni paljo külwama, kui tem(m)a om wasta wõtnu, päälegi omma nurme kik ütte suuru, nink Johan Seeme peap rahhu jääma, et ei olle üllekohtust asja waja nõüda. - Sai kohtomoist-
minne ja § 772 n: 773. kohtunkaujatel ette loetus, nink lätsiwa rahhun wälja. -
                                                   Karraskij Kohtomajan sel 20al Augustil 1876.
                                                                            Eenistja, Jaan Tullus.  XXX     
                                                                             Man(n)istja, Jaan Leppason.  XXX
                                                                                      do  Abbi, Jaan Lodus.  XXX
