Neljandalt  Tulli ette Mäe Musti kõrtsimees ja kaibas et temmäl ollewa Michel Põkker käest 6 rubla saada, et mitte wiina ehk olle eest enge heringe soola ja muu wäikesse kauba eest, ja ütles weel et minnol om jürripäiwal ärra minnek sis om omma ka wäega tarbis, ja olle temmä käest joba mittond kõrda küssino, agga tollest ei panne middägi tähhele. - Et Michel Põkkeril es olle käsko anto, sis jäije kaibus tõise kohto päiwa pääle. -
                                       Karraskij Kohtomajan sel 15 märzil 1879
                                                             Pääkohtomees Jaan Tullus  XXX
                                                                Kohtomees  Jakop Uibo  XXX
                                                               teine    do    Jaan Leppäson  XXX
