I. Schmalz Könnost perresmaa piddäjä tulli kohto ette ja kaibas et 2 Augustil poolpäiw wasto pühhapaiwa ööd om Könno oidselissed temma Uibo ajan ärrä warrastano 3 Uibod om koggoni puhtas wõeto ja 2 1/2 Uibot kahjo arwas Schmalz eggä ütte puu päält 3 Rubla kokko 12 Rubla.
Schmalz andis ülles siis kui temmä om leidno et Uibod om ärrä lahhoto olno, ja kui temmä jälgi mödä taggan om läno otsma, agga et siis jo ni ildä om olno et oidselisse ennämb koon es olle ja Schmalzil tarwis om kirrikus minda olno om selle perräst Eespäiwäs jäno perrän otsi, Öidsel om koon olno keik teised agga Timmati poig Peter Tanno poig Jaan ei olle öidsel olno enge om mõtsan kaugel teiste mant ärrä olno pilli mango,
Kui Schmalz ja walla wannemb otsman om länno siis om nemmäd eddimält Tanno pole otsma länno siis om Tanno üttelno omma poia olno Aidan maggano, ja Tanno poia poolt om Timmati poia pole länno Timmati poig om üttelnu et meije läksime Sippo Jakobi pole ni samma om ka Tanno poig Jaan üttelnu et Sippa Jakob om neid siis kus nemmäd Tartust Landgkwistist om tulnu kutsnu hendä pole et säält Wönno kirrikus läwä.
Siis om Scmalz wallawannembaga Suigo Jürri pole länno ja sääl om Jürri poig Märt üttelnu et Öidsemann es olle öösi minna
1 ​​​​Soe Jaani 2 Pedi Jagomanni 3 Tanno poig Jaani, ja Timmati poig Peter
Küssitus sai Tanno Jani käest, üttel temmä, et meije es käi kuski ajanm meije ollime omma koddon, es käi kuskil, Walla wannemb üttel sinna ütlid siis ja et sinna Sippa Jakobi pool Rassinal olled olno sel ööl.
Timmati Petre es olle tulnu
Suiga Jürri poig Märt üttel, et nemmäd om Kassi Josepi Talli pääl maggano, agga sääl maggamisse mann ei olle olno ja ei olle ka öidsel nänno Tanni Jani poija Jaani ja Timmati poiga Petert nänno
Jaan Soeson käest sai küssitu, üttel temmä: et minna ollin Kassi Josepi pool tarren ösi maggaman, ja hommingo om öidsele taggasi länno, säält.
Pedo Jagomanni käest sai küssitu, üttel temmä, et meije ollime omman koddon nurru pääl karskime ja tarren maggasime.
Tanno poiga Jaani ja Timmati Petert meije es näe.
Tunnistusse poolest jääs polele
20 Augustil kui se Schmalzi kaibus sai neo tunnistussed 13ne Augusti prottokolist ette olli loeto kaibaja Jacob Schmalz üttel et sel ööl ei olle mitte waht ajan maggaman olno kui Ubbina ärrä om warrastedo.
Peter Timmati käest sai küssitu ütte temmä et temmä sel ööl öidsel ei olle olno ja om omma koddon olno ja ei olle ka mõtsan pilli lömän käino. Ja ei olle ka öidse poisikessi nanno ni uttel Peter.
Kaibaja üttel walla wannembaga et Peter om üttelno omma koddon et temmä sel ööls Rassinal Sippa Jakobi mann om käino.
Peter Timmat üttel et temmä Tanno Jani perrä ni üttelno, selle perräst et Tanno Jaan om temmä Welle Hindrik Timmati läbbi pallelda laskno.
Schmalz sullane Rein Griggulson tunnistas et Tanno Jaan om temmä käest küssinu kas teil kalid om ka ajan Rein om üttel eggä ei olle nüüd om weel wäike siis om Jaan kussinu kas Ubbina puud om ka täis Rein om üttelno et nored om weel ja siis Jaan om üttlenu puhhastage ärrä.
Tanno Jani käest sai kussitu, et mis perräst sinna pallusid Peter Timmati wölsi temmä welle Hindriko läbbi, et teije Rassinal Sippa Jakobi pool käisite kus teije suggugi es olle Jaan üttel et temmä om selle perräst üttelnu et Schmalz om taplema tükno ja seddä om temmä walla wannemba een üttelno taggan otsmsse aig et temmä Sippu Jakobi pool Rassinal ööd om olno. Agga Timmati Petre welle Hindrikul ei olle temmä middagi üttelnu. Peter Timmat üttel Jani käskmisse perrä wölsno ja Jaan üttel jälle selle perräst wölsno et temmä Schmalzi om pellano. Agga kohto een om Tanno Jaan ni julge et temmä sanna ei allanda.
Walla wannemb Peter Jagomann tunnistas et temmä om arwanud 1 Rubla eggä ütte uibo päält ubbinide eest kahjo.
Kohhus mõistis et Tanno Jaak nink Peter Timmat peawad kumbagi 2 Rubla ubo ubbinide eest maksma ni et walla wannemba tunnistusse perrä 1 Rubla puu päält tullbe ja päle selle peäwad weel Jaan nink Peter kumbagi 1 Rubla trahwi waeste ladikus maksma.
§773 om ette kulutedo
Pä kohtomees Paap Kiudorw
Kohto Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Hindrik Toding
