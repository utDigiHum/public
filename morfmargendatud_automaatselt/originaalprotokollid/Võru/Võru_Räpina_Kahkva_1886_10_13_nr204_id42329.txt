Ärrä surnud Johann Sepp warandust om perrajäänud:Tarto panka intressi pääle pantu raha			1000 Rbl.		Maja kraami om perra jäänud			480 Rbl.		5 aasta intressi raha a' 50 rbl, 1000 rbl eest			250 Rbl.		Summa			1730 Rbl.		Sellest summast lähap maha marki kullu ja toomine			8 Rbl.		Jääb Summa			1722 Rbl.		
Mehele läinud tütrile om saanu ja töisile perijaleAndre Lillo  naise  Miinale		1 lehm			wäärt 16 rbl.		2 lammast			wäärt 4 rbl.		1 kirst			wäärt 8 rbl.		rahha			25 rbl.		5 punta hainu a' 40 K.			wäärt 2 rbl.		8 möötu rükki a' 1 rbl			wäärt 8 rbl.		Summa			63 rbl.		Widrik Zernask naise Liisole		1 lehm			wäärt 14 rbl.		1 kirst			wäärt 2 rbl.		1 lammas			wäärt 3 rbl.		8 möötu rükki a' 1 rbl			wäärt 8 rbl.		Summa			27 rbl.		Mari Petajal, Jaani naisele		ei midagi		Leena Seppale		1 lehm			wäärt 17 rbl.		2 lammast			wäärt 4 rbl.		1 kirst			wäärt 2 rbl.		puhast raha			10 rbl		Summa			33 rbl.		Emma Wijole Joh läsk			10 rbl.		poja Petrele			20 rbl.		tüttar Annele			10 rbl.		tüttar Maiele			10 rbl.		poja Karlile			--		Summa			50 rbl.		Summa			1895 rbl		
Selle järrele om 9 waranduse perijat, ja saab iga ütsa 1895 rbl ärrä jaetu 210 rbl 55 5/9 kop.
Sellest summast tullep maha arwata mis egal üttel saada sis saab iga üt:Peter Sepp			190 rbl. 55 5/9 kop		Miina Lillo			147 rbl. 55 5/9 kop		Liiso Zernask			183 rbl. 55 5/9 kop		Mari Petäi			210 rbl. 55 5/9 kop		Wijo Sepp			200 rbl. 55 5/9 kop		Mai Sepp			200 rbl. 55 5/9 kop		Ann Sepp			200 rbl. 55 5/9 kop		Karl Sepp			210 rbl. 55 5/9 kop		Leena Sepp			177 rbl. 55 5/9 kop		
Sest 1895 rbl. om Peter Sepp käen:Maja kraam			480 rbl.		5 aasta intress			242 rbl.		Summa			722 rbl		
ja kui Peter Sepp maja kraami henda kätte jättab sis piab temma kui omma jago maha arwatus töisile weel 531 rbl 45 4/9 kop. wälja massma.
Ja on see eesseisaw waranduse jagamine niida kog. kohtu poolt jaetu kui protocoll nimmitap ja sai see jagamine Peter Seppal, Miina Lillol mansaisija Andre Lillo, Liiso Zernask mansaisja Karl Lepland, Karl Seppal mansaisja Jaan Lepikow Wijo Sepp mansaisja Thomas Körtmann, Mai Sepp mansaisja Peter Urtson, Ann Seppal mansaisja Anz Liwak ja Leena Seppal mansaisja Jakob Parmak ette kuulutud.
§773 ja §774 suuremba kohtu käimisest ettekuultud.
Kuulut Peter Sepp et temma otsusega rahhul ei ole.
Miina Lillo om rahhul.
Liiso Zernask niisama
Wijo Sepp niisama
Mai Sepp niisama
Ann Sepp niisama
Karli Sepp niisama
Leena Sepp niisama
opusleht sel 13dal Octobril wälja antu.
protocoll sel 3 Nowembril saadetu
Pääkohtumehe abi Hindrik Leppisk
kohtumees: Kristjan Kübbar
kohtumees: Joosep Sults
Kirjotaja: F. Wreinbaum &lt;allkiri&gt;
