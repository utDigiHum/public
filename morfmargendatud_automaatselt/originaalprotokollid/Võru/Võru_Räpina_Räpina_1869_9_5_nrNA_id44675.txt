Tonnis Mälk Wöbsost kaibas et üts mees om temmä pibo suust ärrä tömmanud Räppinä körtsin kust temmä warsti 2 sammo maa taggast seddä wäggisi käest ärrä wõtjad kindi om sanod wõtta wars om Tõnnisel kätte sanud agga piip om issi wäggisi käest ärrä wõtja kätte jänud.
Pibo wötja Peter Loos Werrolt üttel et nemmäd tullid mulle hulgani manno
Turru kubjas Hindrik Toding tunnistas seddä nänno ja õige ollewad, et se mees nida wäggisi Tõnnis Malki pibo temmä suust ärrä om wõtnod.
Küssitus sai pibo wõtja Peter Losi käest üttel temmä härge ajama tulnu, ja rõiwast ostma Räppinä Sahhari ladul, agga kui Peter Losi käest sai küssitu kas sul rahha om, üttel temmä et rahha ei olle mul rahha löppi eilä wöi täembä otsa, teist körd küssimisse päle üttel 3 kop ollewad.
Kohhus möistis 15 witsa lööki Peter Losil trahwis karristamisse selle wäggisi pibo ärrä wõtmisse kui kurja tembo ja wölsi eest.
Pä kohtomees Paap Kiudorw
Kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Hindrik Toding
