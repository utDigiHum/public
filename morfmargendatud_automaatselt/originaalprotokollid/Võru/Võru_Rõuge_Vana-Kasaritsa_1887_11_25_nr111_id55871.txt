Ette tulli Hindrik Sillaots ja kaibas, et noor Jaan Pang wägiwaltsel wiisil tema aida wõtme ära wõtnu, tema nõudwat oma aida wõttind Jaan Pangi käest kätte saada.
Wastut kaibuse pääle noor Jaan Pang mansaisja Peter Kõiw, et tema selleperast Hindrik Sillaotsa käest aida wõtme ära wõtnu, selle et se wilja nakkanu ära häötama ja ei ollew ka Hindrik Sillaots tälle rõiwast sälga ja kängist jalga andnu.
Tõent selle wasta Hindrik Sillaots et tema mitte Jaan Pangi wilja ei ollew häotanu sest leppingu perra 30. Aprilist 1886 aastast N 43 ollew Jaan Pang oma maja ja maa pidamise eenõiguse kõige warandusega tema (Sillaotsa) kätte andnu igawetses ja jäädawas peranduses ja selle perra ei ollew ka Jaan Pangil enamb Pangi talon N 12 määrastegi peremehe eenõigust. Rõiwa ja kängitse andmise perast Jaan Pangile wastut Hindrik Sillaots, et tema tälle (Pangile) mitte neid ei ollew xxnu.
Nema es lepi.
Otsus:
Kuna Jaan Pang om Pangi talo N 12 Protokolli perra 30. Aprilist 1886 a. N 43 kige warandusega Hindrik Sillaotsa kätte ära om andnu igawes ja jäädawas peranduses ja enamb selle perra määrestegi eenõigust Pangi talon ei ole sis peap Jaan Pang hommen päiwa s.a. 26. Nowb. 1887 aida wõtme Hindrik Sillaotsale kätte andma wastalisel kõrral massap Jaan Pang 3 Rbl. trahwi waeste ladiku hääs. Ent trahwis lähep Jaan Pang, selle perast et tema oma woliga Hindrik Sillaotsa aida wõtme ära om wõtnu 24 tunnis kogukonna türma oma kulu pääle.
See kohtu otsus saije täämbatsel päiwal kohtun käüjile kuulutedus § 773 ja 774 Sääduse raamatust 1860 aastast ära selletedus. Jaan Pang nõwwap Appellationi.
Pääkohtumees: J. Kabist [allkiri]
Kohtumees: P. Klais [allkiri]
Abiline: J. Mikheim [allkiri]
Appellation Jaan Pangile 27. Nowbr. 1887 wälja antu.
