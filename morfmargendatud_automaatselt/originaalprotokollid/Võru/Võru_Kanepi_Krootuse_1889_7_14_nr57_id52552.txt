1,Hurmi mõisa walitsuse asemel S.Sibul tuli siia kohtu ette ja andis Laane-Niidu maja rentnik Johan Sulele üles, et seesama peab 23 Aprillil 1890 aastal majast wälja minema, sellepõhja peal et tema on sala kombel sõnnikut majast wälja wiinud. Sellepärast palun kogukonna kohut Johan Sulge sundi, nimetatud terrminiks Joh, Sulge majast wälja mõista.
2. Johan Sulg wastutas selle kaebduse peale, et Hurmi mõisa walitsus on lubanud minul esi sõnnikut kartohwlile panda teise maa peale.
3. S. Sibul ütles, et mõisa walitsus ei ole mitte lubanud sõnnikut wõera maa peale .
"Kog.kohtu mõistmine"
Johan Sulg peab 23 Aprillil 1890 aastal majast wälja minema sellepärast, et tema sõnnikut majast wälja wiinud. Nendasama ka et tema kohtu käske ei täida nagu Leping  № 63 all 1888 a. näitab.
Mõistmine kuulutadi Seaduse järele; siis olid mõlemat sellega rahul.
