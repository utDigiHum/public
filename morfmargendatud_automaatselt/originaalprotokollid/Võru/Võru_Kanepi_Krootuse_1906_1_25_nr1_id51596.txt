Krootuse Wallakohtu ette ilmus Krootuse wallast talupoja seisusest Tonnis Andruse poeg Kurrikoff ja Wiktor Hendriku poeg Korp ja palusiwad allseiswad oma wahel tehtud ja heaste läbi mõteldud waranduse müümise ja selle ostmise lepingud maha kinnitad ja kirjutada.
§ 1.
Wiktor Korp müüb oma waranduse nagu nimelt:1, üks kõrbjas täkk hobune 3 a. wana wäärtus			20 rbl.		2, üks raudtele wanker pool wana			   7  "		3, üks mustjas pruun lehm 3 p. ema			20  "		4, üks punane walge seljaga lehm 13 p. ema			15  "		5, kaks musta lammast 'a 2 rbl. kokku			  4  "		                                                                  kokku						
 66 rbl.					
kuuekümne kuue rbl. wäärtuses kõigi õigustega Tõnnis Andruse poeg Kurrikoff'le kõiki õigustega ära müünud, mille eest Korp kõik raha kätte on saanud. See ülewel nimetud warandus jääb WIktor Korp kätte pruukita;  (50 rbl.) wiiekümmend rbl. Kurrikoff läbi õkwa ja Bergmann wõla tasumiseks äramaksetud raha eest. Korp kohustab ennast Kurrikoff'le seda raha 50 rbl. Kahes tärminides  ära maksma, nimelt 25 rbl. 16 Webruaril 1906a. ja 25 rbl. 16 Oktobril 1906a. siis sab selle ülewel nimetud waranduse oma omandusena ostu alt wabaks.
T Kurrikoff [allkiri]
W Korp [allkiri]
Üks tuhat üheksa sada kuuendamal aastal Jaanuari kuu 25 päewal on Krootuse Wallakohus ülewal seiswad lepingud, mis talu pojad Tõnnis Andruse poeg Kurrikoff ja Wiktor Hendriku poeg Korp omawahel teinud Krootuse Wallamajas kinnitanud, misjuures Wallakohus tunnistab, et lepingu osalised kohtule isiklikult tuttawad ja et nendel seaduslik õigus aktisi teha ja et Tõnnis Kurrikoff ja Wiktor Korp peale ettelugemise oma käega lepingule alla on kirjutanud.
Eesistuja J Kuhi [allkiri]
Koht.. M. Puna [allkiri]
  "      J Traat [allkiri]
