Kaibas Wiido Jöks, Löötja möisa mötsawaht, et Löötja möisa karjus Jaan Kärtmann olewat tema püssü ära warastanu ja seda oma pääle Wäike Werksu külla ära toonu. Märgid: pikk täwweline üte rawwaga püss, ilma warwata, wask warwa toos, tähendus wasine, laat werrew ja takan wasine pera. Ka olewat seesama Jaan Kärtmann Löötsja Herra hamme ära toonu. Püss wärt 5 Rubl. hame 70 Kop.
Wastutas Jaan Kärtmann, et püssi ei olewat tema mitte warastanu, enge temal olnu oma püss hendaga Löötsja möisan üten ja sedasamma püssi toonu tema ka oma ema poole tagasi ja andnu seda oma welle Johani kätte. Hamme toonu tema selle perast, oma hamme sääl ärespidanu. Jaan Kärtmann näitas oma püssi kohtule ette. Märgid: lühikene katski laadi ja katski lukuga üterawwaga püss; laat wärwimata must; ilma kikata, warwata, zilenirita, warwatoosetu ja waskperats lagunu püss.
Tunistas Josep Kärtmann, et Jaan Kärtmann tulnu Löötjalt kodu ja olnu püss käen umbes 4 jalga ehk ka pikemb; püss olnu muido kõik joonen ja täwweline üterawwaga, muud kui ilma warwata, wasine tähendus ja wasine perra. Püss, mis Jaan Kärtmann siin kohtu een ette näitas, ei ole mitte seesama, mis toll körral Jaan Kärtmanni käen oli, kui tema Löötjalt kodo tuli.
Tunistas Aleksander Lillmann, niisama.
Johan Kärtmann ütles, et tema wötnu seda püssi hendale, mis tema weli Jaan Kärtmann Löötjalt kodo om toonu.
Otsus.
Kohus möistis, 1) et Johan Kärtmann Löötjalt warastanu ja tema kätte andnu, 5 Rubl. kahjotasomist Wiido Jöksile wälja masma.
2) Jaan Kärtmann saab 10 witsa lööki trahwis warguse eest ja selle eest, et kohtu ette mitto körd om kutsutu ja selle mann wastupanemist üles näütnu.
Pääkohtumees Joh. Lepland
Kohtum. J. Kirhäiding
as. kohtum. J. Leppind
