Kohtu een kaibas Jaan Hallop: Et minna lätsi Mihkel Kauni tarre, nink kõnneli temmale hää wisiga, et meije tahhame karjust siija sinno tarre ellama panda, sis wõt temma puuhallo, nink ähward sellega mulle lüwwa. Kunna nüüd Jaan Hallop sellega rahhule ei olle, et Mihkel Kaunis om tedda puhalloga ähwartannu, tuuperrast palles temma kohhut sedda asja perra kullelda, nink säduse perra moista.
Mihkel Kaunis ette kutsuti nink kaibus ette loeti, kes wastut: Et Jaan Hallop tulli minno tarre ja üttel meije murrame, sis wõtti minna puu hallo ja ütli kui sinna middagi puttut, sis minna lüün, nink Jürri Herne kuuli ka sedda.
Tunnistaja Jürri Herne ette kutsuti nink selle asja perrast küssiti kes wastut: Et minna es näe muud middagi, kui et Kaunis wõt  puuhallo nink lubbasi lüwwa kui keaki middagi peas putma.
Kui neil kellelgil selle asja perrast ennamb middagi ütlemist es olle 
Moistis Kohhus:
Et Mihkel Kaunis saap selle puuhalloga ähwartamise eest 5 witsa lööki ehk massap 1 Rbl trahwi waeste hääs.
Mostmine nink  § 773 sai kohtun käüjile ette loetus kon nemma ka sellega rahhule olliwa.
