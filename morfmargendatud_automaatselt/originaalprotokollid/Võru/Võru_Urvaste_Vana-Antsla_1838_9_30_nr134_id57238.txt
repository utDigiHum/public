Marja Treumuth, Toffre Jani Tüttar leppi selle päle erra, et temma ess taha ennamb Kohot käwwa enda Latse Essa perrast, kedda temma om Kihelkonna Kochto nimmetano, selle No. 321 all ja ei taha päle nende kolme kümne Rubla, miss temma om sano Kohut käwwa, ja selle Latse Essa käst middaki ennamb küssita, egga kenneki wasto sest asjast kõnnelda.
Wanna Ans Mois sell 30mal Septbril 1838.
Märd Sillper 
Päh Kochtomees
M Boustedt
Tunnistaja
