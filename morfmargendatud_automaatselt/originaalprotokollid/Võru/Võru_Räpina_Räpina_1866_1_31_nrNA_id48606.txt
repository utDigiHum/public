Wido Punberg ja kostnapühkja Kolberg tulsewa kohto ette endawahhel omma naudminne selletama
Oppipois Wido Punberg ütles Kolberg om temmal 9 Rubla kuu paele palk lubbano, Kolberg om temmal 9 Rubla kuu paele pak lubbano, Kolberg aga ütlis sedda om mitte oige ta om enne 5 Rubla temmale pakkono, neil ütleb es ollis kontrakt kuida na ommawa leppino, ja ka mitte tunnistajad.
Kolberg andis omma rechnung sissa, ta om selle järrel Wido Punbergile massno ja andni 30 Rub 60 kopik
Wido Punberg sai se rechnung ette leoetud, ja tunnistas tedda oige ollewat, enne se sögi rahha om ülle liga pantud. Kohhus wottis sögi rahhast 1 Rub 60 kopik mahha, siis jäi se rechnung 29 rubla paele saisma.
Wido Punberg andis omma rechnung ülles, ta tahtis 4 kuu est 36 Ruvla ja 2 kuu est 10 rubla kokko arwato 46 Rubla.
Kohhus arwas kige kue kuu est temmale 30 Rubla, selle perrast et ta om selle kinda est ka tenino, ja oppipoise palk ei woi mitte ni suur olles. Selle perrast sai moistetud et Kolberg peab Wido Punbergile weel 1 Rubla erramassa.
