Protokolli Nr 19 manno.
Lewi kogokonna kohus, sel 10 Octobril 1879
Man olliwa päkohtumees Karl Lauk
kohtumees Jaan Thal
" Jakob Kriwa
Kutsmise perra olliwa ette tulnu Johann Thalfeldt, Jaan Thalfeldt, ent Mihkel Thalfeldt ja Jürri Thalfeldt oliwa wabandamata wälja jänu.
Jaan Thalfeldt and eensaiswa kaibuse päle wastust, et temma kätte om Jakob Thalfeldti warrandust jänu nimelt: 2 lehma, 2 lammast, 2 zikka, ütte kanga kuddamise pele, ütte pu wankri, 40 rubl. raha ja ka suggu wilja, ent se raha kui ka willi om neide Jaak Thalfeldti laste üles kaswatamise kolitamise päle ärra kullunu, ent Johann Thalfeldti es wõtta mitte tunnistajas wasta et se tema wiha mees om.
Otsus: Mihkel Tahalfeldt ja Jürri Thalfeldt Pindi wallast sel 24 Octobril tõistkõrda ette kutsu. 
Päkohtumees
kohtumees
"
kirjotj: [allkiri] J. Kirrop
