   No 4
Jusal sel 12 Februaril 1871 olli kohhos koon: Pä kohdomehhe ass. Peter Mandli
                                                                       ja  Johan Raig ja Hindrik Tillmann
Ette tulli Jaan Uibo kaiwas: Et tem(m)a ei olle Jagobetre maijast, 1864 tol ajastal, kui Jaan Wärs sedda Jagobetre maija om ärra ostno ei olle temmale wälja mineki kahjo tassomist masno, nink nõwwap nüüd 60 Ruplat hobbetat.
Jaan Wärs aste ette üttel: Minna olle Jaan Uibole nellikümment kats ruplat, esmald meije omma leppimisse nink perrast Kreiskohdo mõistmisse perra wälja masnu, ja weel selle kewwajald ette tettu to Est 5 ruplat, se om ülle kige 47 Ruplat hõbbetat masno, ja ei olle ennamb middage asja.
Kohdo otsus olli: 
Et Jaan Wärs omma pritahdmisse perra, ja kui perrast lepimist tülli tulli, Kreiskohdo läbbi assi olli selletet, ja otsus antu olli 47 ruplat olli Jan Uibole masno, sis moist koggokonna kohhos sedda kaibust tühjas, ja ei moisteta middage. Sai kohdo otsos ja § 773 kohdo käijile kulodedos, ent Jaan Uibo es ole rahhul, ja palles prottokolli mes ka sai antus.
