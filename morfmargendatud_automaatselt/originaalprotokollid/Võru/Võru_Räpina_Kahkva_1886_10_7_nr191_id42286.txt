Otsus
[Möistis kohus et Joosep Mitt, Jaani poig]
Tulli ette Fritz Solnask nink tunnist et Jaan Mitt poig olla kolm körd hainu kottiga toonud Möisa nurmest, kos olli 3 wakaline minkaga temma töi, ja olli ega körd täis
Otsus
Möistis kohhus et Joosep Mitt, Jaani poig saab 6 witsa lööki selle Möisa haina warrguse eest.
§773 ja §774 suuremba kohtu käimisest ette kuulutud kohtukäijatele.
Kuulutiwa mõllemba otsusega rahhul.
Joosep Mitt, Jaani poig om 6 witsa lööki ärräsaanud.
