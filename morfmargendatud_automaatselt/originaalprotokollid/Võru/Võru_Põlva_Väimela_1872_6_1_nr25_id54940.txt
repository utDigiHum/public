N 25. Waimella kog. kohton sel 1 Junil 1872
Kui üttegi selgembaid tunnistuisi ei sa ennamb kui kohtomehhe essi nemma kulda sano, et nemma omma kik wäga jobono olno.
Moistus: Moistap se kog. kohhus kae prot N 23 d.d. 18 Mai s.a. Peter Hütt ja Jürri Wäisweer sawa kombege 13 witsa löki, ent Hindrig Lauga jaap ommast rahhast ilma, kes selle moistussega rahhele ei olle se woiss § 773 ja § 774 nink wimale § 881 perra tallita.
Jürri Waask [allkiri]
Peter Pallo [allkiri
Jaan Zuhna +++
Kirjotaja JKangro [allkiri]
