Kohus 21. Junil 1886
Koos ollid.   Peakohtomees   J. Roht
      "                   Kohtomees    M. Wõso
      "                                 "          J. Mandel
Tulli ette Mõtsawaht Hindrik Tuhand ja kaibas. "Rentnik Peter Koot Zea karjus on moisa metsast 4 noort Kõiwo ära koorinud. Seda om näinud Weber Zolgist.  palun, et poisi trahwitud saakse, ja nouan kahjo tasumist.
    jääb poolele, teises kohto pääwas.
                                       Kohtomeeste  all kirjad
                                   Kirjotaja   A. Muhle
