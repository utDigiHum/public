Karraskij Kohtomajan Jusal sel 13al Mai´l 1877.
Keiserlikko Kihhelkonna Johto tsirkuläiri pääle sest 2est mai s.a. No 1910. saiwa wallawallitsusse kässu pääle, kokko tallitedus kreeka õige ussu kerriko saadiko perrast, Kähri õige ussu kerriko tarwis, kui nimmelt, wallawannemb, Michel Wõsso. - Lutteri usku, Jaan Mäggi, kreeka õiget ussku, nink Michel Järg kreeka õiget ussku. - Sest, et siin koggokonnan ennamb - kreeka õige ussulisi ei olle, sai wallawannemba poolt kohto tahtminne ette kuulutedus, nink et waidlemist es tulles, loosi tettus, ja loosimisse perra Jaak Mäggi, Kähri kreeka õige ussu kirriku saadikus säetus, et se nida tõtte om, tunnistawa omma nimme taade risti tõmbamissega. -
     Karraskij Kohtomajan sel 13al Mai´l 1877.
                          Lutteri ussku wallawannemb, :M. Wõsso.
                          Kreeka õiget ussku, saadikus saanu, Jaan Mäggi.  XXX
                               do     do        do  mannolleja , Michel Järg  XXX
