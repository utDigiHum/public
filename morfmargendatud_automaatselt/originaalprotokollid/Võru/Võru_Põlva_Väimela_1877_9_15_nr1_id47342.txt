N 1. Waimela Kogokonna kohtun sel 15 Septembril 1877
Man olliwa Päkohtumees Jaan Tjuchna Kohtumees Jaan Kuck.
Ette tulli Jürri Sulg ja Jaan Jaaska, ja palsiwa et Kogokonna kohhus neide leppingut kinnitasi mes neil 1867 aastal om leppitu: Et Jürri Sulg om Kaarli poja Jaan Jaaska 1867 aastal kes tookõrd 9 aastat wanna olli hendale kassu latses wõtnu, nink minna olle tedda seniajani kaswatanu ja hoolt kandnu, ja selleperrast et minna nüid terwusest kõhn olle ja ennamb tööle kolblik ei olle sis om [mahakriipsutatud: Jaan] minno kassu poig Jaan Jaaska minno eest lubbanu hoolt kanda ni kawwa kui minna ello. 
Jaan Tsuchno. +++
Jaan Kuk. [allkiri]
Leppingu teggija: Jurri Sulg [allkiri] Jaan Jaaska [allkiri]
Kirjotaja A Hallap. [allkiri]
Kopia sel 21 Sept. 78 aastal rötewan niwitet lepijäle wälja antu JHänsle [allkiri]
