1904a. 14. Januaril ilmusid Krootuse walla kohtu ette Kioma wallast talupojad, Tintso taluomanikud, Jaan ja Peter Märdi p. Häelm ja nenda samute Tintso talu omanikude maa peal asuw Kioma walla talupoeg, Kaupmees, Hendrik Johani p Nilbe ja palusid järgmisd suusõnalist lepingud maha kirjutada ja kinitada.
§ 1.
Tintso talu omanikud Jaan ja Peeter Häelm annawad oma talu peralt olewa maa tüki mis ennemalt kaupmees Hendrik Johani p. Nilbe käes, Johann Jaani p. Nemwaltsi käest ostetud maja all ja seal ümbruses olewa maa tüki 1/3 (üts kolmandik) wakamaad, Hendrik Nilbele rendile kolmekümne aasta peale, s.o.23 Aprillist 1904 a kuni 23ma Aprillini 1934 a., ja maksab Nilbe tähentud maa tõki eest /10/ kümme rubla renti iga aasta algusel.
§ 2.
Kaupmees Nilbe wõib selle maa tükiga teha, mes tema arwab, kas põllus pruuki hoonid peale ehitada, ehk wilja puid istutada, mida aga kaupmees ise oma arwamise järgi heaks arwab, ei ole omanikul õigus keelda.
§ 3
Rendi aja lõpul ei ole omanikudel õigust nõuda, et hooned, ehk tõine asutus, mis selle maa tüki peale kaupmees Nilbe poolt on ehitatud - ära peab lõhkuma, waid maa omanikud ehk nende perijad peawad rendi termini kas koku lepimise ehk asja tundjatelt määratud rendi eest edasi rentima, nõuda et hoonet, ehk asutus ei pea mitte ära saama lõhutud.
§ 4
Kui peaks ette tulema, et kaupmees Nilbe mõne suguse põhjuse perast seda asutust ühes maaga mõne trahwimata isikule nende samaste tingimistega ära müüb ehk rendib, ei ole omanikudel õigust seda keelda.
§ 5
Kui peaks ette tulema, et keski lepingu tegijatest enne termini lõpu ära sureb, ehk kui Häelm oma talu mõnele wõerale edasi müüb, siis peawad nemad selle tingimise toimendama, et leping perijate kui ka wastse ostja kohta muutmata jääb.
§ 6
Kõik enne tänast päewa tehtud lepingud selle maa tüki kohta saawad selle lepinguga tühjaks tunnistatud ja  kohtulikud  protsessid selle sama maa perast lõpetud.
§ 7
Kui peaks ette tulema, et nimetud leping peab tempel poogna peale kirjutud saama ja Notariuse juures kinitud, siis peawad seda rendile andjad wastu panemata tegema, kuna aga kulud kannab rendile wõtja.
§ 8
Kaupmees Nilbe wõib selle maatükile ka aid ümber ehitada, mida omanikud ei saa keelda.
