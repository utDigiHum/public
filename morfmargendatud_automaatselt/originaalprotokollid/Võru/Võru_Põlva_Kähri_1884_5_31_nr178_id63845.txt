Josep Wasser Kährist tuli ette ja kaebas et temale on Peter Laas käest 1 rub. 57 kop. saada ja palub seda wälja nõuda. Mida Kohtum. J. Waks wõib heaks tunistada.
Peter Laas Joosust wastab: Mina ei mäleta enam kui palju mulle Wasserile maksa on. Raha ma wõtnud ei ole aga niisama kopiku kaupa on kõrtsi wõlga jäänud.
Pääkohtumees Joh. Waks tunistab: Ükskord ütles kül Peter Laas, et temale pääle rubla Wasserile maksa on.
Mõistus.  Tunistuse põhja pääl mõistab kogukonna kohus, et Peter Laas peab 1 rubla 57 kop. Joosep Wasserile 2 nädali sees wäljamaksma.
Otsus sai T. r. s. a. §§ 773 ja 774 järele kohtuskäijatele etteloetud. Peter Laas ei olnud mite otsusega rahul.
