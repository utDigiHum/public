Kaiwas Pihkwa Kubbermangust Pognitsa Küllast Soldat Midra Karpow et temma om sel 20 s.k. Wirksu Körtsin 10 Rbl rahha wahhetanu ja om Körtsimees temmale 9 Rubla pabberi rahha taggasi andnu ja ütte rubla wasses wahhetanu kost Körtsimees wina eest om mant ärra wõtnu, ja Daniel Lepland om lubbanu Midrale 1 Put röa jahhu müwa 80 kop. eest, ja omma to pääle häste wina jonuwa ni et Midra ka om häste purju jäänu, sis om Daniel Lepland ja Midra Koddu jahhu perra lännuwa kui nurme pääle om saanuwa om D. Lepland Midrale ütelnu istume weidike mahha kui Midra mahha om istnu om D. Lepland Midra mahha litsnu ja se 9 Rubla rahha ärra wõtnu wäggisi karmanist ja joskma nakkanu.
Tunnistaja Kusta Söugand kes nurme pääl kündman ollu om sedda asja selgetse nänu kui D. Lepland om Midraga nurme pääl kisselnuwa ja Daniel Lepland om käe Midra karmani ajanu ja punga wälja tömanu ja sis ärra lännu, Midra om rögnu et ärra wöt rahha punga, ja Kusta Söugand om ka nännu et walge pung om ollu ent ei tija kui paljo rahha om sees ollu.
Krissa Samoilow om ka kuku kui Midra om rögnu et ärra wöt rahha.
Kaiwatawa D. Lepland ütel et Midra om kül jahhukotti temma kätte andnu ja Wenne Külla Hansu Paapu poole lubbanu minna ja 10 Rubla rahha wahhetanu ja wina häste jonuwa ja kui Midra Wenne Küllast taggasi tullu sis om weel wina jonuwa, ja perrast temma poole lännuwa jahhu perra om Midra mahha istnu ja Üttelnu panneme pibud pallama sis om temma Midrat silust tõmmanu ja ütelnu lämme minnema ent Midra ei olle tullu sis om temma ärra lännu.
Tunistaja Mihkel Rummask üttel et temma om paar sadda sammu kaugel ollu ja om selgeste kulu kui Midra om rögnu ärra wiib Danila rahha.
Tunistaja Peter Oinling ütel et temma om ülle oja ollu sääl liggidal kui Midra om rögnu Danila oda järgi ärra wii rahha ärra.
Et tunnistajade ütlemise perra Daniel selgeste süidlane om ent D. Lepland kangeste sallas ja ka se assi ülle Kog. Kohtu mõistmise om saadab se Kog. Kohhus D. Leplandi Keiserliko Maakohtu ette.
K. Rämman
J. Parmson
P. Urgard
perrast om D. Lepland Midrale se rahha ärra andnu ja Kohtumehhed om Danila​​​​​​​ walla lasknu.
