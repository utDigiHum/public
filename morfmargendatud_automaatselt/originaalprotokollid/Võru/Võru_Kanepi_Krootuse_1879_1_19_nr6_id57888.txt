Tulli jälle ette Rein Kruuse ja kaibas et Johann Naris lehm om suwwel üts kõrd kinni aeto, ja om Rein Kruuse üts rubla henda käest sinnä kinni wõtmisse trahwi Serreste moisahe massno, ja pallel kohhot et sunnis Johann Narist, et tem(m)a omma masseto rubla kätte saas
Sai ette kutsotus Johann Naris ja üllewanne kaibus ette loetus kes selle pääle ütles min(n)ol olli Rein Kruuse hoijo alla omma lehm anto ja massin temmäle tolle eest /3/ kolm rubla, et nüüd temma poig karja mann maggama heikano, ja minno lehma Serreste moisa walitsussel om laskno kinni ajada ei putto se mulle middägi, täwwelisse hinna olle minna wälja massno n. ni eddesi .-
Sai Rein Kruuse weel ette kutsotus ja Johann Naris kõnne ette loetus, kes selle pääle middagi es wõi ennamb üttelda. -
Otsus  Kohhos mõistis et Rein Kruusel ei olle üttegi nõudmist Johann Naris käest, om Rein Kruuse om(m)a hoole alla lehm hoita wõtno sis peäp ka essi tolle eest wastotamma, kui kurja lassep tettä. -
Sai kohto mõistminne ja § 772-773 kohton kaiadelle ette loetus ja lätsiwa rahhon wälja
                                                               Pääkohtomees  Jaan Tullos  XXX
                                                              Kohtomees     Jakop Uibo  XXX
                                                           teine     do    Jaan Leppason  XXX
