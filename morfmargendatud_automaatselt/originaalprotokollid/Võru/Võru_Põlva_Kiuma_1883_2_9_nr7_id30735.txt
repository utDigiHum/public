Kaibas Wana-Koiolast Joh. Aripmann, et siit wallast Peeter Sawisaarel olla paari aasta eest tälle ühe söögilaua ja 6. hamme õmblemise eest 250 + 120= 370 kop. maksmata jäänud, mis ta nüüd ära püüdwat salata.
Peeter Sawisaar ütles sellewastu, et ta olla see raha ühe waka rükiga tasa teinud.
Aripmann ütles, et see olla ristikheina seemnete eest maksetud ja Sawisaar ei saanud ka salata, et ta ristikheina seemnid Aripmanni käest ei olnud saanud. Aga kui Sawisaar tõendas, et hamete (särkide) õmblemise eest igakord ära olla maksetud, siis ütles Aripmann, et ta ka seda wäga julgesti ise ei teadwat, waid tema naesterahwad olla ütelnud, et niipalju maksmata olla.
Otsus:
Selle laua eest on Sawisaarel J.Aripmanni heaks 250 kop. 2 nädalani äramaksta.
Seaduslikult nendele ettekuulutatud.
Pääkohtunik J.Nilbe [allkiri]
kohtunik: H. Lukats
d: J. Aripmann
Kirjut.as, Zuping [allkiri]
