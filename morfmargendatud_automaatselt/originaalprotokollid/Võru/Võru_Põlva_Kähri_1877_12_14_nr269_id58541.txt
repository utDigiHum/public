Ette tulli Jaan Kõiw oman mitmen kaibusen Jaan Sawisaar wasta, ja nõwab et ütskõrd see asi saas selletetus ja nõwab et jo saas selletetus.
Jaan Sawisaar ettekutsutu nee kaibuse ette loetu, kes ütlep kõiki neide kaibuste pääle, wõtmid ei olewad temäl wõlsid olnu, kes tunistab ehk mes eest mina wäitsega ümbre käwe, mina ei ole minkisugust kurja Jaan Kõiwul püüdnu tettä ei tee ka, nee oma kõik wõlsi ja tühja üllesandmise.  Ja kaibuse.
Otsus:
Kogokonna kohus mõistab, sedä ülles wõetud ähwardus ja wägisi kiskmise asja olla kohto trahwi säädus perrä Keisr. Werro Ma kohto selletus kätte anda.
Jaan Kõiw es ole rahul ja lubab esi kaibata.
Pääkohtumees Jaan Luik
Kohtumees Peter Tallomees [allkiri]
  " Johhan Punnak [allkiri]
  " Jaan Hingo [allkiri]
