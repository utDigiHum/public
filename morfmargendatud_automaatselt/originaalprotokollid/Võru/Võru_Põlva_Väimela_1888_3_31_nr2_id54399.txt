N 2. Waimela kogukonna kohus sel 31 Märzil 1888. 
Man oliwat Pääkohtumees Johan Sulg kohtumees Jaan Pihl kohtumees Michel Samoson.
Michel Mäos ja Peter Traks tulliwa kohtu ette, ning palsiwa kogukonna kohtu nende leppingot kinnitada 
Michel Mäos annab Peter Traksile Küllamõtsa maa tükki külles ütte tükki maad ümber ühe? wakkamaa künnimaad. [mahakriipsutatud: katte] wiie aasta pääle renti pääle rendi massap Peter Traks wakkamaa eest 3 rbl. wakka maa eest. 
Michel Mäos annab ütte wakkala hainamaad sääld samast ilm rentitada, kui peas ette tullema, selle maa sisse krawi kaiwa honet ehita, ehk üle ültse wastsest ehita, ehk teen, selle eest massab Michel Mäos, Peter Traksile wälja. 
Jürri Mäos tulli ette lubasi, ja ütles kui mina wijetal aastal jooksul Peter Traksile üles ütle siis peab Peter Traks ärrä minnema sellest kohast, kui Jürri Mäos wijetal aastal mitte Peter Traksile üles ei anna, siis ellap Peter Traks sääl 25 aastad, edasi. 
Ne leppija tunnistiwa seda leppingot õiges oma alkäe kirjotega. 
Michel Mäos xxx
Jüri Meos x++
Peter Traks [allkiri]
Pääkohtumees,
kohtumees,
kohtumees.
Kirjotaia A Saremõts [allkiri]
