N 266 al 2 Dez
16 Dez et tambäl päle ette luggemisse Hels Ritsberg üttel et temmä middägi ei tea häd ei halwa üttelda temmä ollewa omma kõhho täis sönud.
Jaan Puksow nõuwap sullast ja päiwäd massetus sada.
Kohhus mõistis sullase kaibust sel körral tühjäs, ja sullane Jaan peab taggasi teenmä minnemä ja kui eddespiddi mant ärrä lähhäb massap päiwäd.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Josep Ritsmann
Kohtomees Writz Konsap
Kirjutaja J Jaanson &lt;allkiri&gt;
