Paap Zerning Tolamalt kaibas kohto een et temmä om olno 1872 aasta Jürri päiwä aig, 4 kuud Peter Raubmann teenmän aasta päle ollewa kaup olno leppito tetto 34 Rubla ollewa aasta päle palk olno leppito ja kätte ollewa sanud 3 Rubla 10 kop, ja selle perräst ollewa Paap Zerning ärrä länno et terwis ei olle kandno orjata. Ja Zerning ollewa ilma perremehhel ütlematta ärrä länno.
Peter Ruba üttel küssimisse päle, et temmä om Paap Zerningi hendämanno aasta päle sullases palgano 1872 Jürri päiwäst 1873 Jürri päiwäni, agga Zerning om ärrä länno kui linno om hakkato kahkma ja om keik tööd teggematta jätno, ja om ka perremees Peter Rauba ütte körraga ilma sullaseta jätno,
Zerning nõuwap 4 Rubla kuu eest 4 kuud 16 Rubla.
Kohhus mõistis kaibust tühjäs et leppingo perrä ei tenito.
§773 om ette kulutedo.
Soldat Paap Zerning pallel prottokoli
Päkohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Josep Ritsmann
