Juhann Mandli wottab Mikko Jaane maija 6 astajas enda kaette, ja ehhitab noide 6 astaja sissen 1 wastse Laud, 1 küin, 1 aid ja kattab noid ka, tolle eest leppitas temmale 3 astaja pael kümnes mahha antas temmale 10 süld haggo küttisses, ja 3 astaja eiga astaja 2 süld 3 hallone puu, ja moisa pold 130 leisikat õlgi.
Kui peas Juhann Mandli enne noid 6 astajas wallal saama tahtma, sis massab temma puhta rahhaka 3 astaja kümnes, 130 leisikut õlgi  6 süld puu, ja 10 süld haggo indo perra taggasse.
Temma lubbab 2 neddal paele Jürri päiwan moisale 3 surre ellaja, ja hobbene üllesnoitama.
Sedda kontrakt tombab Jurri peiwas 1857 ni kawwa kui 1863 Jurri peiwan.
Karrasky 30 März 1857
Mõisaherra W. Röcker  [allkiri]
Tunnistaja H. Tensmann
Maja rentija orjus pääl Johhan Mandli xxx
Tunnistap Koggokonna Kohhus tõtte Peter Urbärg xxx
