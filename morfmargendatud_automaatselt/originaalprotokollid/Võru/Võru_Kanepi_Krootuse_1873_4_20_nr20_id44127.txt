Neljas kohus selsamal päiwal, manistja nesama kohtumehe.
Wija Hendriki perrisom(m)anik Otto Grünbergi herra tulli omma nurme kaartiga ette ja tahtse sedda koggokonna kohtu man kinnitada; ent kohtumees Peter Mandli üttel:"Ma ei kinnita Teije kaartit enne, kui Teije sedda täüdäte, mes Teije kontracti teggemise man, Jaan Markale, suusõnnaga lubbasite, et Jaan wõip kessa nurme ka osast tõwwo nurmen pitta. - Selle päle üttel O. Grünbergi herra, pahhandusega kohtumees Peter Mandlile: Tulge wälja lawwa takkast, ma tahha Teije päle kaibata; Teije kinnitade kondracti, ja kaartit ei kinnita. - Peter Mandli aste kohtu lawa takast wälja ja üttel:"Ma ei kinnita selle kaartit, enne kui Otto Grünbergi herra om(m)a lubamist, Jaan Mark man ärra täüdap. - 
Kuhhus mõist: Selle et Otto Grünbergi herra kohtumeest ilmasjanda ja süüta, kohtu lawwa takkast wälja kutsup, - massap
O. Grünberg üts rubla hõbb. trahwi kattesa päiwa sisen kogokonna waeste laati ärra.
Kohtu otsus, ja §.§ 773n 774 sai kohtu käüjile ette loetus, mõllemba olliwa sellega rahhu.
                                                       Kohtumees Jakob Uibo  XXX
                                                                 abbi    Mihkli Wõsso  XXX
                                                ja       do           Johann Raig  XXX
