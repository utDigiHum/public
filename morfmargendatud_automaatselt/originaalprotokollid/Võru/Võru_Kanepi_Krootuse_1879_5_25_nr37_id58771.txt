Kolmandalt  Tulli ette wallawannemb Michel Wõsso ja kaibas, et ollewat Jaan Kebja kaest /6/ kuus rubla saada pallel kohhot sedda wõlga Jaan Kebja käest manno ajada kui ei salga kan(n)ata lubbas. -
Sai ette kutsotus Jaan Kebja nink ullewanne kaibus ette loetus, kos tem(m)a mitte es salga enge pallel kannatada konni süggiseni. -
Temma perremees Jaan Mark sai kohto ette kutsotus nink sullase Jaan Kebja palka keelo alla pantus
Kui nimmelt wallawannemba hääs       -                          -   6 rubla
nink Jaan Wälk hääs        -                  -                   -         -   5. -
Ütte kokko ütstõistkümme rubla mes märdi päiwas s.a. Jaan Mark wälja peäp massma, omma sullase Jaan Kebja teenistusest, mes teuta lubbas. -
                                                  Pääkohtomees J. Tullus  XXX
                                                 Kohtomees    J. Uibo  XXX
                                               teine      do     J. Leppason  XXX
