Köstre koli pois Jaan Keertman kaibas kohto een et Jama külläst Jahhim Girasim om teddä pesno 2 näddälid aigo taggasi pühhä päiw päle kirriko koddo minneko päält Jama Küllä kotsil tee pääl, selle perräst et mõllembide hobbesed tee pääl wastamissi kokko om länno, ja päle selle om Jahhim wõtno weel Jaan Keertmanni wö rätti ärrä wõtno.
Jahhim Gerasimi käest sai küssitu üttel temmä et temmä ei olle mitte pesno agga wö rätti om temmä kül ärrä wõtnod selle perräst et Jaan olles temmä hobbesel pähhä löno.
Jahhimi welli Alexander Gerasim üttel, et koli poisid om neidegä hobbesed kokko aijano ja neil om Wehmer katki länno ja kui temmä wehmre ärrä om parrandano om ärrä länno welli Jahhim om koli poiste kätte jänod agga seddä pesmist temmä ei olle nänno.
Koli pois Jakob Kiudos Daniel Otsing Josepi poig Daniel Otsing Pedo poig tunnistiwa et Jaan Keertman om hobbest lömas ja su werest kändno ja siis om Wehmer katki länno ja tunnistiwa ka, et Jahhim om Jaan Keertmanni rinnust kindi wõtno, ja om löuwa alla lönod wasto rinda.
Köster Murritz tunnistas et Jahhim om temmäle koddon könnelno et koli pois Jaan Keertman om mis eest nemma om tahtno herrä hobbesele pähhä löwwä köster om uttelno no siis olled sinna ommetigi lönod Jahhim om üttelnu kes poisikeste jatta oskwas wöttab
Kohhus mõistis: et Jahhim peab selle eest temmä tee pääl Jaan Keertmanni pühhä päiw om riismä hakkano, ja temmä wörätti ärrä wõtno peab temmä 10 witsa lögigä selle eest trahwitus sama.
Pä kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Hindrik Toding
