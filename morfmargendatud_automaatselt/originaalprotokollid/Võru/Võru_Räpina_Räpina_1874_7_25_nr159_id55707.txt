Adam Häidson kaibas kohto een, et Michel Grigguli hobbesed om temmä keswä pääl olno, ja ollewa 2 wõhherust ka keswä päle pandno, ja karja elläjädegä ollewa 1 1/2 wakka alla röä külw rükki ollewa läbbi ärrä sökkoto. Adam Häidson nõuwapröä kahjo eest			10 Rubla		keswä eest			2 Rubla		kindi wõtminne			1 Rubla		Kokko			13 Rubla		
Michel Griggul üttel küssimisse päle, et temmä karri mitte Adami röän ei ollewa käinud, Ja lohhust ollewa Jakob üttelnu hobbese kindi wõtno.
Ülle kaejad Kohtomees Writz Konsap ja Wöölmunder Widrik Melberg üttliwä et röäst ollewa mõnned jäljed läbbi käino ja keswä ollewa södo, ja 2 wõhherust ollewa ka pääl, agga kes neo käijäd elläjäd om olno seddä ülle kajad ei tea, Ülle kige om kahjo arwato 3 Rubla ülle kaejade poolt.
Tunnistusse perräst jääs polele.
