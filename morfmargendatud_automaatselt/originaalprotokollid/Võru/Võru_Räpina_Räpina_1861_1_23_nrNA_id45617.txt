Kernere pois Johan tulle kohtu ja wastut temma mitte ei olle neid tütrigu seel kokku kutsnu, nemma olle eige üts 1/2 top saksa wiena lasnu tuwwa, talle Karle om toja olnu.
Kernere pois Karl Kirjatusk tulle ette ning wastut, ne tütrigul om esse endas kokku tulnu, saksa wiena na om küll lasnu enda tarwis tuwwa ja ollut om na ka körtsist tonu, ja teise poise talle Juhan om ütsinda wiena ja ollut gutnu, ja Alexander Parrison om Tosikats Seppa pool olnu paele sie tunnist temma nemma om Weikine Iwanill lasnu ollut tuwwa.
Kernere pois Iwa tunnist nemma om küll ollut ja wiena tonu ja Ahrand om olle gaggega.
Talle Juhan tulle ette ning wastutiwa temma om küll kernere tarre lännu ja nennuwa kui tantzinu ja kakliwa
Wabriku tütrik Halla Miena tunnist neid ei olle keddage kutsnu temma om esse pulma kaima lennu.
Wabriku tütrik Kattarina Mikalai tunnist nemma om esse endas sinna pulma kaina lennu.
Wabriku tütrik Kükke Dora tunnist nemma om esse lennu ilma kutsmata ja Ahrand om neil ollut gagganu.
Wabriku tütrik Kihwa Lotta tunnist nemma om esse sinna lennu ja kutsnu neid ei olle keddago, ollut om Ahrand neil gaganu.
Oppetaja tütrik Mina tunnist nemma om esse endas sinna Kernere poise tarre man lennu, Ahrand om ollut gutnu.
Kiwwi tütrik Miena tunnist teine om tedda sinna kutsnu ja temma om ilma gehnu.
Wikki Gustaw wastut, kernere poig Johan om talle kirja andnu Kükke Dorall wija seel om kirjutetu olnu tütriku tulki nie palli kui om poise enne tulku kats tüki.
Kernere pois wastut temma ei melleta mitte sedda et ta kirju om satnu.
Kükke Dora wastut, Wikke Gustaw om talle kirja küll tonu temma ei moista luggeda.
Lotta Pinding wastut Wikke Gustaw om küll kirja tonu ning ette luggenu sell om tütriku nimme paele kedda tedda kutsute, ja nemma tu iddago ei olle lennu.
Moistetu: neil 2 Kernere poistell Karl Kirjutas ja Johan Olde sawa 15 loge ja Iwan 10 loege witsu selle eest et temma om omma tarre neid tütriku kokku wutnu ja tandsi piddanu ning tütrigudelle kes Kernere poiste man tantsma lennu nimmel Karo Jula, Halla Mina, Katta Nikulai, Kukke Dora Kihma Lotta Oppetaja tütrik Miena sawa 10 loege witsu Wikke Gustaw kes kirja om umbre wienu 10 loege witsu.
