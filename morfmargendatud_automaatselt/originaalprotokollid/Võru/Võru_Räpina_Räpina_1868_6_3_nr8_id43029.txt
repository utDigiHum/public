Pedo Lätting tulli kohto ette ja andis ülles mis temmä arwap hendäl Josep Kristowaski käest sada ollewad mis Aida pallamisse aig ärrä om pallano kui Aitt Josep Kristowoski pibust om pallama länno 2 aastaigo aigo taggasi Pedo Lättingi kahjo tassomisse ülles anto nõudminne Josep Kristowaski käest oma			4 leisikat linno			10 Rubla		b			Selget rahha mis ärrä om pallano			1 Rubla		c			2 seinä särgi röiwast			10 Rubla		d			6 wakka keswi			9 Rubla		e			6 wakka üssatsed			9 Rubla		f			1 wak hernit			2 Rubla		g			1/2 wak seme hernit			1 Rubla		h			1 wak Uppe			2 Rubla		j			4 Lamba nahka			3 Rubla		k			2 kirsto			2 Rubla		l			1 naiste rahwa undrik wastne			1 Rubla 50 kop		m			Aida ülles teggemisse eest			25 Rubla		Summa			75 Rubla 50 kop		
Josep Kristowask sai ette kutsutu ja küssitu üttel temmäa			4 leisikat linno om wäljä sani ja perräst Pedo issi Wöbsohe ärrä münod		b			Selget rahha om Pedo üttelno sissi jäno sse 1 R		c			Särgi röiwas om sisse jäno agga ei tea kui paljo		d			6 wakka keswi ei olle nänno ja Pedo issi üttel sel körral et mul es ja muud kui weidi agganid jäi sissi		e			6 wakka kartoflit ei tea Josep middägi		f			1 wak hernist ei ütte Josep ka middagi teedwä		g			1/2 wakka seme hernist ei üte Josep middägi teedwä		h			1 wakka Uppest ei ütle ka Joosep middägi teedä		j			4 Lamba nahka om Pedo üttelno sissi jäno		k			1 ilma kaseto kirst ja 1 wanna kast olli		l			1 wastne naisterahwa ündrikko ei ütle Josep olno		m			Aida ülles teggemisse eest ütleb Josep 5 Rubla Pedol kätte andno ollewad agga mis muido sest ei ütle midägi teedwä tunnistajad kumbagil es olle		
Josep Kristowask üttel omma haida enne Pedo Lattingi aitta kige täijegä ka ärrä pallano ja kus ka 65 Rubla rahha om sissi jänod.
Kohhus mõistis et Josep Kristowask peab Pedo Lattingil wälja maksPedoma Aida teggemisse eest se 20 Rubla mis weel maksmatta om Agga muu ülles anto Pedo Lättingi nõudmisse perrä ei wõi kohhos middägi moista se perräst et Josep Kristowaski Aitt om ka üttelissi ja weel enne ärrä pallano ja et Pedo Lätting om osso omma tullew rikkutu wiljä kätte sano ja ärrä pruukno.
Pä kohtomees Paap Kiudrow
Kohto mees Kristian Wössoberg
Kohto mees Hindrik Toding
Kohto mees Paap Kress
Kohto mees Jaan Holsting
möistuse ei jäe täijelikkus enge allan nimmitedo lepping jäeb täijelikkus
Pedo Lätting nink Josep Kristowask on teine teisega ärrä lepno 25 Rubla päle ülle kige kahjo mis Josep Kristowask Pedo Lättingil peab wälja maksma
5 Rubla om Pedo Lätting kätte sani ja 10 Junil saap 20 Rubla siis om otsan
Se lepping om kohto polest kinnitedo
