Kaibas Semen Samoilow, et Karl Otsing ei masse temale ära
1, 30 rbl., mille eest Otsing temale lubanu wastast maad pitta anda
2, 10 rubla aida eest, mis tema, Samoilow, oma kulu pääle ehitanud, katuse ja usse, ja hinged pandnu;
3, 3 rbl. 2 korjuse katuse eest.
Wastutas selle kaibuse pääle Karl Otsing:
1, see raha olewat korteri eest antud, 20 r. om kätte saanu 10 r. weel saada
2, Samoilow olewat tema kambre laotanu ja sest aida tennu osalt warastetu palgist. Katuse olewat tema olgi ka; ei massa
3, korjuste katuse tarwis olewat Samoilow tema tütre Mari Waheri olgi ka wötnu; ei massa
Kostis Samoilow pkt. 1. manu tunnistaja Karl Lepland ja Karl Wöikland. pkt. tun. Dawit Tikso
Tunn. Karl Lepland ütles, et Semen Samoilow lubanu 30 rbl. Karl Otsingile poole maa õiguse eest.
Tunn. Karl Wöikland ütles, et lepitu olnu nii, Karl Otsing mütab Samoilowi omas poolemaamehes ja see massab selle eest 30 rbl. 20 rbl. saanu siis ära massetu, 10 r. jäänu siis weel masmata.
Tunn. Dawit Tikso, et tema müünu Samoilowile 45 punda olgi, mida see aida katuse tarwis ära pruuuknu.
Otsus.
Lepsiwa ära niida: et Karl Otsing massab Semen Samoilowile 25 rbl. üle köige kahju tasumist ja Samoilow ei nöwwa enamb midagi pääle selle Otsingi käest ja lähab majast wälja.
Paakohtumees Peter Urtson
Kohtum. K. Rämman
Kohtum. J. Oinberg
Karl Otsing XXX
