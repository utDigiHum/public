Karraskij Koggokonna Kohhus, Jusal, sel 3mal Augustil 1873.
                                                  Mann olliwa Eenistja Jaan Tullus.
                                                            do       Mannistja Peter Mandli.
                                                             do      Mannistja Jakob Uibo
Tulli ette, Wija Hindriko (perrisommaniko maamõtja herra O. Grünberg´i) rentnik Jaan Mark, ja kaibas. - 
1, et temma ei ollewa ni paljo rükki wälja külwi saanu kuda kontrakt  ütlep. 
2, Kaibas et temmal om saanu sel 25mal Julil s.a. enne aigo ülles ütteldus, ja nöüdis kontrakti perra, kahjo tassomist. - Ja ütles weel, et temmal olla wastne asse walmis ja sinna ollewa rahha waja sisse massa. - 
Selle pääle küssis kohhus wastast kontrakti ette näüdata, ent sedda es olle.
Kutsuti ette, perrisommanik maamõtja Grünberg, kes henda kontrakti pääle toetus ja ütles, 
1,se punkti pääle, rentnik Jaan Mark, peap ni kui kontrakt ütlep rahhu ollema, ent siski leppisiwa ni kaugelle kokko, et mamõtja Grünberg massap J. Mark´i Orriku laade aigo (5.) wiis wakka rükki, kõige karjasse palgaga kokko, ja sellega olli mõllembit piddi rahhu.-
2,se punkti pääle, ütles mamõtja O. Grünberg, minna olle rahho, ja kontrakti rikkumisse perrast kohhut pallelnu Jaan Mark´il ülles üttelda, ja ei massa tem(m)al mitte üts koppik. -
Otsus   Keiserliku Majestäti keige wene rigi essi Wallitseja käsu pääle. - Moistis kohhus ja kinnitas eddimäst punkti, et olgu Orriku laade aigo s.a. ärra massetu, mamõtjal (5.) wiis wakka rükki, Jaan Mark´il. -  Ent teise punkti pääle ülles ütlemisse perrast sai kohtu poolt tühjas arwatus, (ja Jaan Mark jääp paika, ehk kui tahhap, lähhap (ehk woip) kewade 1874 ilma kahjo tassomattawälja minna,) 
+ eesmalt selle perrast et Jaan Mark, es woi omma wastast kontrakti ette näüdata, ja teiselt, et mamõtja O. Grünberg, ehk kiuste perrast Jaan Mark´il ülles ütles.- Sai kohtu moistminne ja § 773.n: 774. kohtun käüjatelle ette loetus, ent Jaan Mark es olle sellega rahhul ja pallel Protokolli, mes kohhus ka lubbas anda. -
                                                                                    All kirri
LS                         Protokoll wälja antu sel 10mal Augustil 1873.
