II.
Perri walla perremees Mihkel Wõsso kaibas siin Koggokonna Kohtu ees et Körzimees Jaan Ehhi temma kaest 1 Sia 3 Rubla eest ostnu om, nink tõiselt weel Jaan Ehhi temmale  3 Rubla rahha wõlgu.
Jaan Ehhi sai küssitus wastata: wastutas et om Perri walla perremees Mihkel Wõssu käest 1 Sia 1 Rubla 50 kop. eest, ehk jälle nisamma  suur sigga taggasi anda - kaubelnu. Se rahha wõlg ollewat temma ärra masnu.
Mõistetus:
Et neil kummaltki poolt oiget tunistust ei olle, peab Jaan Ehhi Mihkel Wõssule selle SIA eest 1 rubla 25 kop. masma.
Agga selle rahha wõlla eest ei woi siin Koggokonna Kohto een ilma tunistajadeta middagi mostetus sada - enga jääb tühjas mõistetus. 
Perri walla perremees ei olle selle Kohtu otsusega mitte rahhu, enga palleb Protokolli suremba kohto wija wälla-
Allkiri
