Michel Rammask Kahkwalt kaibas wasto Jaan Puksandi Lokkotajalt omma poig Suwwe karjan käimisse palga perräst saija peab ollema poia palk Jani käest 2 wakka wilja 1 w röd 1 wak keswi ja ütte Soldati Lehmä eest 1/3 mõllembit wiljä.
Jaan Puksandi käest sai küssitu üttel temmä et karjus Jaan Rammask om enne aigo 4 näddälid karjast ärrä länno ja Jaan Puksand om teist karjust 4 näddäld piddano.
Kohhus mõistis, et Jaan Puksand peab masma karjusse palka 2/3 röäd 2/3 wakka keswa ja 1/3 wak röäd 1/3 wak keswi peab Jaan Puksand peab sel karjusel masma kes Jaan Rummaski assemel karjan om käinud.
