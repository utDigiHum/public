Ette tuli Peter Saarnit ja kaibas, et Mari Freiberg olla kõnnelnu, et tema olla Silsoni raha 150 rbl. ärawarastanu, seda olla Mari Freiberg Wastse Kaseritsa mõisan kõnelnu.
Kohtokutsmise pääle tuli Mari Freiberg ette ja üttel, et tema ei olewat mitte nimmelt ütelnu, et Saarnit raha wõtnu om, enge tema olewat kõnelnu, et Saarnit ja Reichardti poisi oliwa sääl, nink latse oliwa wõtme suhwli ette unetanu.
Otsus:
Waste-Kaseritsa Utra kõrtsinaine ja karjanaine ülekullelda tulewal kohto päiwal.
Pääkohtumees: J. Kroon [allkiri]
Kohtomees: J. Pruwli [allkiri]
Abi: P. Kuus [allkiri]
Kirjutaja: J. Treu
