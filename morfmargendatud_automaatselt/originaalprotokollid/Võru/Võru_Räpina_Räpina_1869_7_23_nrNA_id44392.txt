Moisa wallitsusse polest anti kohto een ülles, et neo allan nimmitedo tallo rentnikkud, ehk perremehhed ei olle kontrahti perrä omma nurme piddäno, enge om wasto kontrahti wilja küllino monnel kessä päle mönnel neljändä nurme päle rohkemb.
Moisa wallitusse polest üllest anto:1			Tosikatz Wido Raudask			1/2 w linna kessä nurme ja 1 wakkama kartowlid		2			Peter Zupsberg			1/2 wakkamaa kartowlid 2 läuta nurme päle rohkemb küllitu		3			Paap Heddosing			1/3 keswi 1/3 kara 1/6 Linna 1/2 wakkama kartowlid kessä nurmen		4			Peter Läsk			2/3 maa kartowlid kessä pääl		5			Hindrik Kiudorw			2/3 maa kartowlid kessä pääl		6			Hans Ritsland			1 wakkamaa linna kessä pääl		7			Tammisto Paap Pedmanson			2 wakkama linna ja kartowlid ülle pole nelländä nurme päle		8			Kristian Tolstberg			1/2 wakkamaa keswä kessä pääl		9			Widrik ja Paap Liiskman			linna ja kartowlid 2 wakkamaad		10			Jaan Hindow			4 wakkamaa linna 1 wakkamaa kartowlid nelländä nurme pääl		11			Radmalt Wido Ojow			1/2 wakkama linna kessä pääl		12			Linte Kristian Kress			1 1/2 wakkamaa linna tettu nelländä nurme päle		13			Lintelt Daniel Raudask			1/3 maa kartowlid nelländä nurme päle		14			Meelwalt Mik Pärkson			1/6 wakkamaa linna kessä päle tetto		15			Lokkotoja Karli Konswik			1/6 wakkamaa linnä kessä päle		16			Mikk Ojoland			1/6 wakkamaa linna kessä päle		17			Jama külläst Rein Lambing			2 1/2 wakkama kartowlid kessä päle tetto		18			Lintelt Kristian Pedosk			om omma nurmed seggäminne peeto ja karjama ka ülles künnetu		
Se selletaminne jääs polele seni kui herrä Riiast koddo tulleb.
Pä kohtomees Paap Kiudorw
Kohtomees Kristian Wössoberg
Kohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Hindrik Toding
