Ette tulli Johann Huul ja kaibas Peter Wasser pääle kes tedä oli pesnü, seda oli Ado Wasser ja temä peremehe sõsar Liis Paidrow nännu, ja nõwab oma pesmise eest 50 rubla.
Saije Peter Wasser ette kutsutus ja see kaibus kuulutetus, wastutab, et temä ei olewad mitte löönu, tülli om kül een olnu, selle et pois om penniga temä tsikku purretanu ja esi tsikku pesnuwa selle et Johann Huul om tedä hirmsast sõimanu. Sis oli temä poisi lompi tõuganu.
Tsiku pesmise peräst ütles Johann Huul et wanamees om temäl käsknu minnä kaema mes peni haugub, temä lännu kaema, sis olnuwa tsea pennil säljan ja tahtnuwa penni ära kakko muud lärmi ei ole olnu.
Tunistaia Ado Wasser tunistab et temä ei ole midagi nännu, ei tapelust ei ka nuije kellegi käen.
Jaan Paidrow tunistaijas kutsuti, ütlep et temä tappelust ei ole nännu, aga lärm om temä usaijan suur olnu.
Wallawöörmünder Andres Kuklas tunistas et Johann Huul om kül pessetu, sest külle pääl oma selge pesmise märgid.
Jääb poole kuni Liiso Paidrow tunistaias saab.
Neesama kohtumehe.
 
