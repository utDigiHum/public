Sai ettekutsutud siit walla äräsurnud Kusta Tiidemanni naene Katre ja tälle ja tema alaeälistele lastele seätawad wöölmündrid Mihkel ja Iwan Tiidemann ametisse kinnitamise pärast ja waranduse üleswõtmiseks. Üksikud waranduse tükid on järgmised:
 2 lehma, 1 lammas, 1 siga, 1 puutelgedega wanker ja üks regi; ülepea 50 rubl. wäärt. Nimitatud mehed saiwad ametisse juhatatud, esimene laste ja wiimane naese jäuks; ja nendele selle ameti kohused selgeks tehtud. Warandus aga kulub laste kaswatamiseks pruukida.
Pääkohtomees J. Nilbe [allkiri]
kohtomees: A. Luk xxx
        "           P. Aabel xxx
Kirjutaja as. Zuping
