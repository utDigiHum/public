N 44 Waimela kog kohton sel 19 Julil 1874
Pakohtomees P. Pichl
Abbi M. Traks
dito J. Waiso
Koggokonna pakohtomees om Michel Wassiljewile käsku andno et temma piddi omma pärahha wõlg Joso koggokonda Keiserliko Kihhelkonna kohto kässu päle masma; sis Michel Wassiljew üttelno päkohtomeesi wasta: päkohto mehhel ei olle temma wallaga middagi teggemist, sis om üttelno päkohtomees, et temma lehm om oksjoni päle ärrä kirjodet, sis om temma sepäle kostno: sinna lakku minno lehma perst, ka minna sinno ja wallawannemba omma rahha lakku ei anna ja kui päkohtomees omma jalla temma us aida weep, sis temma tahhap näita et päkohto mehhel temaga middagi teggemist ei olle. 
Michli sullane Jaan Lilloson kes selle üllewan selletedu juttu man om olno, om koggokonna kohto een se asja kigge ärrä sallano et temma es kule middagi
konna temma man olli ja selgete kuulse kui Michel Wassiljew ni krap päkohto meest wasta om olno nink Jaan Lilloson sallano om sis om koggokonna kohto poolt
Moistus
Michel Wassiljew masap kats Rubl waeste Ladikotte trahwi nink Jaan Lillosoon saap kümme witsa loki wai massap 2 Rbl waeste Ladiko
Päkohtomees: Peter Pihl [allkiri]
Abbi: Mihel Traks [allkiri]
dito: Jaan Wäis [allkiri]
Kirjotaja JohKangru [allkiri]
