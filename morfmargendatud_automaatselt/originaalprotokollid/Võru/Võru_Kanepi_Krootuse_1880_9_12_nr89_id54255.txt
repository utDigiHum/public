Kohto een kaiwas Jakob Astel, et Kusta Jahu ollew temä käest maad ärä riisnu, mis mõisa walitsus ja kohtomees temäle kewäde kätte ollew mõõtnu. Nõuab nimelt pool wakamaad nisso, mida Kusta Jahu hendäle ollew omandanu kätte.
Kusta Jahu wastutas selle kaibduse pääle, et temä ei ollew mitte Jakob Asteli maad wõtnu, enge see maa olnu temäl hendäl söödust künnetu ja selleperäst ollew temä sinna nisu pääle külwnu.
Mõisawalitseja Samuel Rõigas tunnistas, et Jakob Astelil ja Kusta Jahul ollew mõisa poolt maa pooles mõõdetu nii et kumbgil ütte wõrra maad peap olema, ja et Kusta Jahul ei ole midagi rohkemb nõuda kui Jakob Astelil.
Kohtomees Joh. Purasson tunnistas, et temä om mõisawalitsejaga maa pooles mõõtnu ja keelnu ärä, et Kusta Jahu ei tohi tõist poolt, see om Jakob Astli maad pruuki.
Otsus: Kusta Jaho ja Jakob Astel peawa see riio al olew nisu üten ärä pesma ja ütte wiisipooles tegema. Et aga Kusta Jahu wasto säädust ja üle keelo niso om külwnu J. Asteli maa pääle peap tema 3 öödpäiwa kogokonna türmi trahwiga trahwitus saama.
Kui kogokonna kohto otsus ja Sääduse §§ 773 ja 774 kohtun käüjile ette saiwa loetu, sis es ole Kusta Jahu selle otsusega rahul ja pallel opuslehte wälja mis temale ka lubati. Kusta Jahu om kohut andis pallelnu, mis perast temäle ka trahw jättetu.
 
