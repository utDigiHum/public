Ette tuli Josepi talu peremees Johann Porila ja palus oma soovimist protocolli üles wõtta.
Johann Porila soovib Kähri walla Kõrge Kroonu Joosepi talu № 31, mis tema käes alalises pidamises on äraosta ja tahab seda ostmise hinda 49 aastaga protsendi wiisil kroonu kassa ära maksa ehk kui jõud kaswab ka ennemalt seda teha ja lubab kõik need tingimised täita mis Keiserliku Baltimaa Domeni Walitsuse ringkirjas 31. jaanuarist 1979 sub № 129 ette kirjutud.
Ühtlasi palub ka Johann Porila, et ostmise kontraktis saaks nimetatud, et temale üttitsite maade ja teede pruukimine teistes külaperemeestega ühte wiisi oles.
Johann Porrila   [allkiri]
Kirjutaja M. Bergmann [allkiri]
