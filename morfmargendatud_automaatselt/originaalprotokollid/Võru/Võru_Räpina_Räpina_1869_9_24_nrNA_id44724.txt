Moisa wallitsusse polest anti ülles et Jaan Hindow ei olle kontrahti perrä omma nurme piddäno enge om wasto kontrahi omma neljäs nurm keik linnoga ärrä küllinu mis ülles anto moisa wallitsusse polest et Jaan Hindow om omma nelländä nurme päle 4 wakkamaa linno nink 1 wakkama kartowlid küllino.
Jaan Hindowi käest sai küssitu? üttel temmä et siis kui temmä senna tallo paika ellämä om länno om keik maad södin olnu, ja nüüd seni mani ei olle ka keik weel jõudno ärrä arrida, ja moisa wallitsusse polest ei olle ka su sannaga middägi könneldo, ja temmä issi ei olle ka teedno kuis piddädä keddägi nurme, ja ei olle ka nurmi piiri teedno.
Kohhus möistis, et Jaan Hindow peab eggä ütte tadre päält mis selle tallo perrä om 50 kop peab moisale maksma selle perräst et temmä ilma körrata nurme piddäno.
§773 om ette kulutedo
Pä kohtomees Paap Kiudorw
Kohtomees Kristian Wössoberg
Kohtomees Jaan Holsting
Kohtomees Paap Kress
Kohtomees Jakob Türkson
