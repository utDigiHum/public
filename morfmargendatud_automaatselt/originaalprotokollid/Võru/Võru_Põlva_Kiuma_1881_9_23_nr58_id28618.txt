Kiidjärwe koguk. kohto nõudmise pääle 22. Septbr. c. № 161 Pritz Klaasmanni kaibuses wastu Peter Lepwalzi warguse pärast siin Jacob Tiidemann ja Mari Kantzo ülekuulatud ja sellest kopia 26dani Septbr. 123da numbri all Kiidjerwele saadetud. Nende tunistused selles asjas ollid aga järgmised:
Jacob Tiidemann tunistas: Tema ei olla 15.Septbr. P. Lepwalzi näinudgi, sest et ta mitte kodu ei olnud, waid Kioma mõisas tööl. Pärastpoole olnud ta küll kord P. Lepwalziga koos, aga see ei olla tälle mitte kõige wähemat sest warguse asjast rääkinud, mispärast sis ka tema sellest midagi ei teadwat.
Mari Kantzo tunistanud: P. Lepwaltz ei olla 15. Septbr. mitte enne kodu tulnud, kui Kioma mõisas ettepaneki laud p.l. ärä olnud löödud (pääle kella kahe). Kodus olla ta warsi hobuse ettepannud ja Himmaste külla lubanud sõita.
Siinjuures on weel tähendada, et P. Lepwalz oma kodust äräolekut esite salata püüdis, kui ta selle kohta eesistniku nõudmise peale ütles, et ta juba warsi pääle kella 11. e.l. kodus olla olnud.
Eesistnik J. Nilbe (allkiri)
Manistnik: A. Luk XXX
d.: P.Abel XXX
Kirjuta as. Zuping
