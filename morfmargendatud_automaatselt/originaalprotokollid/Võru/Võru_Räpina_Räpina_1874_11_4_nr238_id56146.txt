Mango Raudsepping Wabrikust kaibas kohto een, et Hindrik Rikkandson ollewa teddä Räppinä moisa kõrtsin pesno, ja ollewa teddä lauwomant ärrä tõuganu, ja perräst rinnust kindi wõtno ja sis om pesmä hakkano ja särgi katki kakno, ja perräst kui Mango om koddo tullema länno sis ollewa körtsi wõrussen jälle Hindrik Mango kindi wõtno ja mahha lönud ja jalguga rindo päle länno ja pesno.
Hindrik Rikkandson üttel et Mango ollewa kiwwi wõtno ja tahtno teddä löwwa sis temmä ollewa kiwwi käest ärrä wõtno, ja perräst kui temmä nuia om wõtno Mango käest sis Mango om mahha saddano
Paap Parmason Wabrikust tunnistas, et perrä otsa pääl om temmä kulu kui Hans om üttelnu Hindrikul poig wõtta kiwwi ja lö pa lahki ja ollewa nänno kui Hindrik Mango oma mahha lönod.
Pawel Tihhanow tunnistas nänno kui Hindrik Mango särgi om kõrtsin katki kakno
Kohtomees Writz Konsap tunnistas kulu kui Hans kõrtsi om länno ja om üttelnu et temmä sai
Mango nõuwap om pesmisse ja särgi kakmisse eest 3 Rubla.
Kohhus mõistis, et Essä Hans Rikkandson peab waeste ladikus 2 Rubla masma poia kihhutamisse eest ja poig Hindrik Rikkandson peab 2 Rubla Mangul wallo rahha masma, ja päle selle poia Hindrikul weel 10 witsa lööki oppuses.
Essä Hans om poia Hindriko eest Kostja.
§773 om ette kulutedo
Hans pallel prottokoli
