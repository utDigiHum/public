Tulli ette Kusta Musting nink tunnist et temma om kül Wöbsu körtsin käinud ja sis om Peter Säkna Joosep Listakindil pool nakla wiina ostnud aga Joosep Listakind ei ole mitte joonud ja sis om üts mees Peter Säkna kinni wötnud ja tahtnud tedda wälja wisa enge Peter Säkna om käed wasta tugenud ja ei ole körtsist wälja lännud ja om Säkna üttelnud et raha kot om ärrä wöetu ja tema om ütelnud et peasi omad seltsi mehed läbi otsma, enge tema Säkna ei ole sedda teinud.
Tulli ette Joosep Listakind nink tunnist et temma om kül Wöbsu körtsin käinud ja tunnistap et Kusta Musnkij on nännud et Peter Küttmann om Peter Säkna wälja wiinud ja Peter Säknal om rahha kot käen olnu ja sis om Säkna ütelnud et temmal om raha ärrä wöetu.
Otsus
Peter Säkna ja Peter Küttmann ses 8 Aprillis ettetal.
Pääkohtumees: Peter Urtson
Kohtumees: Kusta Rämman
Kohtumees: Joosep Oinberg
Kirjotaja F Wreimann &lt;allkiri&gt;
