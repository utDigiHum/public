Lewil sel 22 April 1876.
Ette tulli  mõtsa wallitseja herra Kruse ja kaibas et ollewat hendale sullase leppinu Daniel Ilwes ja ollewat palk olnu 25 rubla rahha ja roiwas, ja käerahhas andnu 1 rubla, ja piddi kapsta Marja päiwa tenistuste tullema ent sis käerahha taggasi tonu ja üttelnu et haige om nink tenistuste ei woiwat tulla nink ollewat Kruse üttelnu et kui sinna haige ollet sis to minnole tohtre mant üts tunnistus ent nüüd mitte tunnistust ei tonu ja ka tenistuste tulnu nink Wõro linna tenima ärra lännu, - Nüüd pallep Kruse tedda § 383 perra trahwida, selle eest et tenima ei olle tulnu
Otsus:
Danel Ilwes tõises kohtopäiwas talloitada ja 30 kop trahwi et täämbatsel päiwal tulnu ei olle kutsumise perra
Pääkohtomees Jaan Thalfeldt [allkiri]
abbikohtomees Joh. Suits XXX
" Joh. Kausaar XXX
Kirjot. [allkiri] Jõggewa
N 14.
