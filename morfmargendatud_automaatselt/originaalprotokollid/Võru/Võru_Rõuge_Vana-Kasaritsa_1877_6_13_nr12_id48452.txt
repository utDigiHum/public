Ette tulli Jakob Lauk nink kaiwas et Johann Pettai poig om minno poja pä kiwwiga lahki lönu nink pallep koggokonna kohhut sedda asja ülle kullelda, kaibaja nõud 10 Rbl. omma poja lömisse eest, se päle om kaiwato ette kutsutu nink wastust andnu, et Jakob Laugi poig om minno enne sõimanu nink ka weel lönu selle perrast olle minna ka temmale kül kiwwiga lönu nida et pääst werri wälja tulli, nida om se minnol juhtunu
nink Walla wannemb x xxx om sedda poisi ülle kaenu nink tunistap et poisi pä olli werritses lödu, sis om koggokonna kohtu poolt.
Mõistus.
Johann Pettai peap Jakob Laugile 3 Rbl 2 näddali seen selle koggokonna kohtu een ärra masma
Moistminne sai kohtun käüjile ette loetus T. S. ramat 1860 a. wälja antu prg. 773 nink prg. 774 ärra selletedus,
Lepping tettu Johan Pettai mks 1 Rbl
Päkohtumees J. Hallop XXX
Abbi M. Jõggewa XXX
   "     P. Kall XXX
Kirjotaja Johann Raudsepp
