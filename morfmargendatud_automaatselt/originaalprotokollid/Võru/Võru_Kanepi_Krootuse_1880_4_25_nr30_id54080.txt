Hindrik Heik tulli kohtu ette ja kaibas et temä perenaine Mari Heik temä aasta palka 25 Rbl. mitte ärä ei tahtwat massa ja käskwat selle käest nõuda, kellega temä palka lepnu ollew. Mari Heik mees Ernst Heik kellega temä palka ollew lepnu, ollew suwel ärä koolu ja temä ollew oma aasta edesi teeninu. Pallep kogukonna kohut seda asja selletada, niisama ka seda raha Mari Heik käest sunduse teel wälja nõuda, mis temä Ernst Heikile om lainanu ja kelle man An Herne tunnistaja om olnu. Lainatud raha ollew 17 Rbl. ja palka 25 Rbl. kokko 42 Rbl.
Mari Heik (Man saisja Jaan Narusk) kutsuti kohto ette ja kui temäle kaibdus ette loeti wastutas seesama, et Hindrik Heik ei ollew temäga midagi palka lepnu ja ei teedwat tema muust palgast midagi kui sellest, et Hindrikul suwel lina maan om olnu, keda ka Hindrik ärä ollew wõtnu. Raha ei ollew mitte 17 Rbl. olnu waid 16 Rbl. kedä Hindriki käest lainatu ja kelle eest lehm om saanu ostetu. Sellesama lehmä ollew Hindrik esi laadele ärä müünü 17 1/2 rubla eest ja ei teedwat temä mitte kas Hindrik seda raha temä mihele ärä om andnu ehk mitte.
An Herne tunnistus ( № 9 sellest 25 Januarist 1880) et temä om nännu kui Hindrik Heik om andnu Ernst Heikile 17 Rbl. lainusses.
Otsus: 
Mari Heik peap Hindrik Heikile 25 Rbl. palka ja 17 Rbl. wõlg raha ärä masma, kokko 42 Rbl. niimoodu, et see palk raha 25 Rbl.peap kate nädali sisen ärämasma ja lainato raha 17 Rbl. 1 Nowembrini s.a.
Kui kogokonna kohto otsus ja Sääduse §§ 773 ja 774 kohtun käüjatele ette saiwa loetu, sis tunnitas Mari Heik, et temä selle otsusega rahul ei ole ja palles opuse lehte wälja, mis temäle ka lubati. Hindrik Heik olli kogokonna kohto otsusega rahul.
Pääkohtomees Joh. Pähn XXX
Joh. Purasson XXX
Mihkli Laine XXX
 
