Anne Kaarmann tulli kohto ette ja kaibas et temmä om wõtno köstre käest rabbamisse wõi rehke paklid kottode langas tettä 3 1/2 kop om nagla ketrus olno leppitu ja kui palji paklid om olno seddä temmä ei olle teedno ja temmä om kottides ka ärrä kuddano ülle langa ketrusse ja kottide kuddomisse om temmä 3 Rrubla nõudno ja köster ei olle wälja maksnod.
Köster Undritze käest sai küssitu? üttel temma et mul olli ni wisi leppitu et temmä wõttis paklas ja piddi walmis kottid taggasi toma ja siis olli 5 kop kündrä hind leppitu ja temmä issi korjas ja rabbas paklad ja saiwad illusad paklad ja temmä töi mulle taggaasi arwad ja halwad kottid rõiwa ja pälegi rohkem kui mul olli antu. Ja slleperräst et halw ja arw rõiwas om olno om köster rõiwa taggasi paknod kotti rõiwa ilma hinnata ja om üttelno wõtta weel rohkem paklid kui tahhad.
Pä kohto mees Paap Kiudorw tunnistas et temmä om sel körwal sääl olno köstre pool kui Anne om selle kotti rõiwa ärrä tonod siis om köstre kotti röiwa taggasi pakno kige taijega ilma hinnata. Ja neo kottid om ni halwad olno ja arwad et willi sääl sissen ei olle wõino püssidä. Et eddimält kartohwled olled wõino sissi panda.
Köster üttel 12 naela linno sissest wälja suitsussi paklid olno ja 52 naela rabbatussi paklid.
Kohhus mõistis et kui Anne Kaarman sel körral köstrest pakkoto rõiwast taggasi es wõtta siis olgo omma töpalgast ilma.
§773 ette loeto
Kohto mees Jaan Holsting
Kohto mees Jakob Türkson
Kohto mees Paap Kress
Kohto mees Hindrik Toding
Kirjutaja J. Jaanson &lt;allkiri&gt;
