teiselt tulli ette Leesmitti tallu perris ommanik Johan Seeme, ja kaibas et temma pooleterremees Adam Kroon, ollewa omma wõimoga temma mõtsa lännu ja toonu 1. koorma puid hendal wälja ilma lubbata.-
Kutsuti ette Adam Kroon, ja sai üllewal nimmetedu kaibus ette loetus, kes selle pääle ütles henda toonu ollewat, ja ütles weel, minna panni rehhe ülles, ja lätsi Johan Seeme poole küssima, et temma maggasi minna hõikasi paar kõrd temma es kuule, sis lätsi omma wõimoga mõtsa ja tõije ütte koorma kuiwi pikkalt, koddo. -
Otsus  Kohhus moistis Johhan Seeme kaibus tühjas, selle et kontrakt lubbap 4 sülda puid, ent ei olle seni ajani sukkugi weel antu, ja weel selle kõwwa kässuga, et olgo warsti mõts näüdatu kost Adam Kroon woip raggoda ja sülda panna. -
Sai kohtomoistminne ette loetus nink lätsiwa rahhun wälja. -
                                          Allkirri
