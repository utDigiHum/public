Ette tulli Hindrik Lukkatz Kiomast ja kaibas Peter Kärbe pääle, kes ütte pedaja mõtsa ragomise man üle piri oli ragonu, ja nõwab selle eest 3 rubla.
Peter Kärbe küsitu ütlep mina massi selle puu eest jo H. Lukkatzi wellele mõtsan üts rubla, ja jäijeme rahule, ent see puu oli siski mino ostetu piiri sisen.
Otsus: Et asi Kioma ma pääl sündinu, selletab Kioma Kogokonna kohus.
