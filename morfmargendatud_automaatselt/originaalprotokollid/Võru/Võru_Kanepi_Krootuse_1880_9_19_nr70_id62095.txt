Tulli ette Heindrik Tuhhand ja kaibas et poisi käinowa minnewa poolpäiwa ööse temma majan kui nimmelt 1 Tannil Troon 
2 Jakob Mandli 3 Heindrik Wõsso 4 Hanso poig Michel Loodus 5 Michel Ilda 6 Johann Kebja ja wõtnowa temma pistoli särgi taskost ärra ent kes noide 6 seäst se wõtja olli ei tija minna mitte üttelda, palle kohhot et minna omma katte saas Pistol massap 3 1/2 rubla. -
Kutsoti ette Tannil Troon sai kaibus ja nõudminne ette loetus kes ütles minna käisin küll Lauril ent pistolit ei olle putno ei ka nänno. -
Kutsoti ette Jakob Mandli kes nida samma ütles
Kutsoti ette Heindrik Wõsso kes nida samma ütles
Kutsoti ette Michel Loodus    do    do     do      do .-
Kutsoti ette Michel Ilda          do     do     do      do  .
Kutsoti ette Johann Kebja kes ka nida samma utles et ollewa küll sääl olno agga mitte middagi hendaga ütten wotno ei olle. 
Otsus.  Kohhos mõistis selle põhjuste pääle et öösenne hulkominne om ärra keeleto nink ei tohhi sedda mitte olla ja päälegi nakkawa warrastamma, saap öösetse hulkumisse eest eggamees kümme kibbedat witsa lööki ehk kats rubla rahha trahwi nink motsawahhi Heindrik Tuhhand pistoli egga üts 60 kop se teep et Heindrik Tuhhand saap omma pistoli kolm rubla kiik muu massiwa rahha kui ennegi Michel Ilda ja Johann Kebja saiwa warsti pääle Otsusse kulotamist ihho trahwiga ärra trahwitus
Sai kohtomoistminne ja § 772 773 ja 774 ette loetus
                                                   Karraski sel 19 septembril 1880
                                                          Päämees Jaan Tullus  XXX
                                                        Kohtomees Jaan Leppason  XXX
                                                         teine    do   Samoel Raig  XXX
