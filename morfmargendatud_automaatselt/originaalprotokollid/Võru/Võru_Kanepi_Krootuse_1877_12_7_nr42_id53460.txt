Tannil Laine tulli kohto ette ja kaibas, et Jaan Tamm ollew ütte wakkaala maad tema maast oma wilja al pidanu ja ei ollew selle eest mõisa herrale mitte renti ärämasnu, mida tema nüüd pidanu masma. Pallep  Kog. Kohut, et seesama sunnis J. Tamme temale seda renti tagasi massa, mis tema mõisaherrale ollew masnu.
Jaan Tamm wastutas selle kaibduse pääle, et see asi, mis üle Tannil tema pääle kaibab juba kohto läbi, niisama ka mõisaherra man selletedu om / wide № 17. 6 Julil/ ja et Tannil tema maast joba enne weel rohkemp om pidanu, kelle eest tema renti olla masnu, siis ei ollew temal selleperast Tannilale midagi massa.
Otsus:
Et selle asjaga joba ütskõrd kog. kohtu poolt otsus om tettu, niisama ka mõisaherra poolt ja et T. Laine nüüd jälle wastsest sedasamma wanna kiusu ette wõttap, sis mõistap kog. kohus, et Tannil Laine 12 tunni wangin olemise trahwiga trahwitus peap saama ilma asjata kiusu ajamise eest ja ei ole T. Lainel J. Tamme käest midagi nõudmist. 
Peran kohto otsuse kuulutamist tunnistas T. Laine, et tema selle otsusega rahu ei ole ja lubas edesi kaiwata.
G. Weitz XXX
A. Org XXX
G. Kolk XXX
Tannil Laine om oma 12 tundi wangis ärä olnu.
