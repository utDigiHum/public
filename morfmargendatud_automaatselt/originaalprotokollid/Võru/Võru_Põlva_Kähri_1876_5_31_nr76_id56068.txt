Ette tulli Johanna Tennäsilm ja kaibas Jaan Waino pääle kes oma krundi sisse ei laskwad kütteta maa pääle temäl enämb walla leppingo perrä kolmandad wilja tettä, oli jo lina temä põllu pääle tennu.
Jaan Waino wastutab, et kedägi oma krundi sisse ei lase, lepping ei puttuwad temäle midagi.
Otsus:
kogokonna kohus mõistab, et lepingo perrä ei wõi Jaan Waino Johana Tennäsilm põldu tettä, Joh. Tennäsilm massab Jaan Wainole seemne tagasi, ja saab linna hendäle.
Otsus kuulutati kohtu käüjile § 773 ja 774 perrä.
