Jüri Wäiko tuli ette ja andis üles et tema on 8 pääwa Peter Kirberi juures töös olnud ja nõuab igapääwa eest 60 kop. s.o. 480 kop. mis ta palub Peter Kirberi käest wälja nõuda.
Peeter Kirber wastab: Mina päiwiti Jüri Wäikot töösse ei ole tellinud. Meie: Johann Wisk Karl Wäiko ja Jüri Wäiko ja mina wõtsime neljakesi Peter Sokk tare 100 rubla eest üles teha. Jüri Wäiko teenis aga kõigest 8 pääwa ära ja jättis töö poolele ja läks siis ära. Kui tare walmis saab siis teeme rehnungi ja Jüri Wäiko saab siis niisama oma pääwa palk kätte nii kui meiegi nii kuidas siis rehnungi järele pääwa pääle tuleb.
Otsus
Jüri Wäiko peab seni ootama kui P. Sokk tare walmis saab kus temale siis rehnungi järele pääwa palk makstud saab.
