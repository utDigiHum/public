Ütte ette tulnu tüli perast, mis Jaan Siirak ja temä poja Heinrich wahel om ette tulnu, tulliwa mõlemba kogokonna kohto ette oma tüli asja õiendama, lepsewa aga herra von Roth lepitamise läbi järgmisel wiisil kohto een ärä:
Heinrich Siirak jääp Tille weski pääle ja saap kõik wara ja kraam omale muud kui lubab esa'le Jaan Siirak, kes ärä peap poja mant minema, selle et se ette tulnu tüliperast nema enamb kokko ei sünni elama.
Jaan Siirak om 600 rbl. hõb. joba kätte saanu ja lubap poig Heinrich temale weel: 1 hobene, 2 wankret, kõik puumeistre tööriista, mis Tille weski man Siiraki peralt omma, niisama ka tema oma rõiwa, 1 sohwa, kõik mehitse, 1 saksamaa ader ja kõik maja söögiasja 1 wiiol, 1 harmonikum ja 1 säng kõige sängi rõiwastega. 
Ka lubas weel Jaan Siirak, et tema kattessa päiwa sisen Tille weski päält ärä tahap minna ja selle 8 päiwä sisen minkisugust tüli ega riido tõsta, peaks seda aga tema poolt ettetulema, sis lubap et   kohus teda jala päält ilma kohto mõistmata kogokonna türmi wõip panna.
G. Weitz XXX
A.Org XXX
G. Asi XXX
Kirjotaja Luik [allkiri]
