Puskaro kõrtsimehe Kuslapuu kaibuse asjun Jaan Kolk wasto kõrdsi wõla nõudmise perast /wide № 4 c 17 Märtsil/ wastutap
Jaan Kolk, et tema ollew kül Puskaro kõrdsin söönu ja joonu, aga ollew seda ka õkwa ära masnu ja ei ollew temäl Kuslapuule midagi massa. Pääleki ollew temä esä Peeter Kolk kõrdsimehel kõwaste ära keelnu, et kõrdsimees ei tohtwat temäle /Jaanile/ midägi wõlas anda.
Otsus: Et kõrdsimees Kuslapuu mitte täämbä ei ole tulnu, sis jääp mõistmine tõises kõrras, kui Kuslapuu wastsest seda nõuab.
G. Weitz XXX
A. Org XXX
G. Kolk XXX
Kirjotaja Luik [allkiri]
