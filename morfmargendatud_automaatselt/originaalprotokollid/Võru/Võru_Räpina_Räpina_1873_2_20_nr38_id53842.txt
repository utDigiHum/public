Wido Konsap kaibas kohto een et Gustaw Mäus om temmä kuhjast haino warrastano 1 rugga ossa haino, Kalsa nido päält.
Gustaw Mäose käest sai küssitu üttel temmä et, temmä ei tea sest middägi, ja temmä ei ollewa ka mitte warrastano, Wido Konsapi haino kuhjast.
Wido Konsap üttel et temmä om Warrastedo Haina teed mödä perrä lanno Gustaw Mause pole neide ärrä Warrastedo Hainul;
Hindrik Rämmänn tunnistas et temmä om ka nänno Gustawi pool neid Kalsa so haino agga kelle neo hainad om olno seddä temmä ei tea
Wido Soeson tunnistas et temmä om nänno tee pääl neid haino maan, ja om ka nänno Kalsa so haino Gustaw Mäüse pool maan mis wärskelt olli tudo olno, ösi ja häste maast ärrä ei olle kassitu olno.
Kohtomees Kristjan Rusand üttel, et temmä Josep Kirjutusse ärrä kakkoto kuhja olli kaeno ja säält om Wido kuhja manno länno, ja Gustaw Mäusel om hainad mitmesuggumistega seggi ärrä olno seggäto ja haina te ollewa ka Mause pole länno
Gustaw Mäus üttel et temmä om Rautseppä Hanso pole länno ja sis ollewad temmäl Ree päält hainad olno ja tulega ollewaa ree päält mahha aiano. Gustaw Mäus üttel et temmäl peab ollema 5je nido päält hainad Rainowa soost kokko olno tudo.
Wido Soeson tunnistas et se haina tee mis Mäuse majamanno om länno, om selgeste Mäuse warrastedo haina tee olno.
Ni samma tunnistas ka Hindrik Rämäsk et se hainate om Mause warrastedi haina tee olno mis temmä usse aida om länno.
Wido Konsap arwas haina kahjo 5 Rubla
Kohtomees Kristjan Rusand arwas 3 Pudta neid ärrä warrastedo haina kahjo olno.
Kohhus arwas 40 kop Pudt wäärt olla
Kohhus mõistis et Gustaw Maus peab Wido Konsapil haino eest masma 2 Rubla 40 kop Ja päle selle peab Gustaw weel wargusse trahwis 24 tundi törman ollema.
§773 om ette kulutedo.
Gustaw Mäus pallel prottokoli.
