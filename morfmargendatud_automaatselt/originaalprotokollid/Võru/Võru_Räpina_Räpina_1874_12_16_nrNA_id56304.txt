N 245, 11 Nowembril
16 Dezembril päle ette luggemisse om ette anto et ülle kaemist talwe lumme perräst ei olle sanud.
Et Johann Kirjutus järgi mödä selle sama ütte asja sissen kutsmisse perrä kohto manno ei olle tulno, tunnistap ka kässo andja Kohtomees Writz Konsap õige ollewa et temmä järgi mödä 3 körd Kohto manno kutsmisse käsko Johanil kätte om andno. Kohhus mõistap ilma Johanita ärrä.
Kohhus mõistis Kristjan Rusandi nõudmisse perrä, et Johann Kirjutus peab Kristjan Rusandil wäljä masma se Summa ülles piddämisse tarbis 236 Rubla.
Teggemissed jäwä päle ülle kaemisse päle selle saap nüüd selle samma asja perräst Johan Kirjutus 3 järgmist Kohto päiwä järgi mödä Kohto otsust kulma kutsutu.
7 Januaril päle otsusse kulmist pallel Johann prottokoli.
