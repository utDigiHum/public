Kui se 10 Dezembri 1869 aasta ja 7me Januaril prottokoll 1870 aastal Schmalzi kaibus wasto sullast Jaan Zengowi ette olli loeto. Juhhan Olde tunnistas et Schmalzil om hering käen olno, ja om üttelnu et Jaan om selle heringe ärrä warrastano. Agga Juhhan Olde ei olle mitte nänno seddä, et sullane Jaan om Warrastano heringet.
Kohhus möistis et Schmalz peab sullase Jaan Zengowi täembässe päiwäst sadik taggasi wõtma töd palka leibä ja totio andma ja seni piddämä kui aastaig täis saap ni kui leppitu om.
§773 om ette kulutedo.
Pä kohtomees Jaan Holsting
Kohtomees Kristian Wössoberg
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Hindrik Toding
Schmalzil protokol wälja kirjuteda 28 Januar No 38 al.
