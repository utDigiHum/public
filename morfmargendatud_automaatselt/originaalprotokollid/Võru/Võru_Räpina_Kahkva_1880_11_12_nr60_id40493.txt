Kaibas Jakob Rosneck, et Jakob Karpson olewat teda teotanu ja ütelnu:"Mis mötsa Esanda tüdruk Mari Prantz sest sai, et temale lats sündis ja tema see latse ära hukas, kas temal ilman ruumi elada es ole!" Seda teotust kuulnuwa Katti Musting, Sepo Samoilow ja Hindrik Urgard.
Wastutas Jakob Karpson, et tema ei tija sest midagi ja ei ole ka määrastki teotuse sönna Jakob Rosneku pääle könelnu.
Tunistas Katti Musting, et tema ei olewat määrastki teotuse sönna Jakob Rosneku pääle kuulnu ja ei tija sest midagi.
Tunistas Seppo Samoilow, nii sama.
Tunistas Hindrik Urgard, nii sama.
Otsus.
Kohus möistis seda asja tühjas, selle perast et määrastki tötes tegemise tunistust ei ole ette toodu.
Pääkohtum. Joh. Lepland
Kohtum. J. Kirhäiding
Kohtum. J. Oinberg
See otsus sai sellsamal päiwal mölembile kohtukäijatele Talorahwa Sääduse Raamatu 1860 aastast §772 ja §773 pöhjuse pääle kuulutetus, mis pääle Jakob Rosneck 19. Now. s.a. kohtule teeda andis, et tema selle otsusega rahul es ole.
