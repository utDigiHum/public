Wijes kohhus selsammal päiwal. Nesam(m)a kohtumehhe. 
Mõtsawaht Jaan Uibo kaibas Mustimõisa rentnik H. Undritzi päle, et se ollew tem(m)a  Kudduse lane hainama katte wakkama, omma karjaga lasknu ärra sööta, nink nõwwap nüüd kahjo tassomist 40 Punda hainu. - 
Mustimõisa rentnik kutsuti ette ja küssiti, melles Teije om(m)ile karjusile sädust ei panne, nink nüüd om mõtsawahhi Jaan Uibo niit ärra södetu? Se wabband henda sest ja es ütle middagi teedwat. Karjoside käest küssiti, melles nem(m)a sedda kurja karjaga teiwa? Ne ütliwa: Meije ei olle sedda niitu söötnu, sääl olliwa hobbese sissen ja Tulemäe karri käwe sinna. - Mõtsawaht Jaan Uibo tunnistaja, mõtsawaht Jaan Palloson üttel: Et karjuse holetusse läbbi selle nidu omma ärra söötnu ja et muud karja sin(n)ä ei käü. - 
Kohtu otsus: Et mõtsawahhi Jaan Pallosoni karri ka Musti mõisa karjaga kokko olli käünu, kui niit södetus sai, mõist kohhus se kahjo ülle hulga massa: Musti mõisa kolm karjost peawa eggauts poolrublat seom 1 1/2 rublat, ja mõtsawahhi J. Pallosoni karjos 1/2 rublat, kokko 2 rublat, kige ildamb kattesa päiwa sissen Jaan Uibule ärra masma.
Kohtumõistmine, §§ 773 ja 774 sai kohtun käüjile etteloetus; nink kiik olliwa sellega rahhu.
                                                             Päkohtumees J. Tullus  XXX
                                                                 Kohtumees, J. Wärs  XXX
                                                                  do      "      ,  Peter Mandli  XXX
