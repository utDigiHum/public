Ette tulli Miina Pusep ja kaibas Liso Mitteri päle et Liso Mitter ollewat teda wargas sõimanu.
Liso Mitter ütlep selle päle et temä ei ollewat Miina Pusepa mitte teda wargas sõimanu, enegi ütlep seda et Kähri mõisa härbanist ollewat äräkadonu üts raha protmani (kos Herr v. Deni raha kwitungid sisen olnu ja om katte sanud lilli lawa suhwlist ja 5 R. raha). omada komotist mes mul kadonu om, ja Miso Mitter ütlep et Miina Pusep om ene wargust mõisa herbanid möda ümbre käünu ja kiki asju mannu putunu.
Tunistaia Mari Erinits ütlep kül et Liso Mitter om kül ütskõrd Miina Pusepa wargas üttelnu.
Walitseja Krenz ütlep et Herrä käsk om et Mina oma jalg mõisahe tuwa ei tohhi ja ometi ki kule mina seda. Selle pääle oli  M ütlenu wäga krop.
Otsus:
Kogokona kohus mõistap et Miina Pusep peab 3 ööd päiwa kogokona türmin olema sele et tema wäga krop om olnu kogokona kohtu een mõisawallitsejat wasta.
ja kogokona kohus kelap ka mõisa herra käsu päle et Miina Pusep ei tohi ilma mõisa wallitseja lubbata oma jalga Kähri mõisa herbani wiija.
Otsus kulutedi kohtukäijile § 773 ja 774 T.r.S. ja
Miina Pusep es olle sellega rahul. Sai opus kiri selsamal päiwal antus.
Pääkohtumees
Kohtumees
6 Julil päält kiri antu.
