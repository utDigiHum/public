Ette tulli Josep Paide ja kaibab Johann Wasser pääle, kes temä lepingo perrä poolt pääraha jätnu temäle masmata arwata 2rubl. 40 kop.
Johann Wasser ütlep selle kaibus pääle, et temä ei ole Josep Paidega midaga wõlga, pääraha masnu temä tol aastal nii paljo kui om nõwetu.
Otsus:
Kogokonnakohus mõistab et tunistust ei ole Josep Paide kaibuse tühjas.
Otsus kuuluteti § 773 ja 774 perrä T.S.
