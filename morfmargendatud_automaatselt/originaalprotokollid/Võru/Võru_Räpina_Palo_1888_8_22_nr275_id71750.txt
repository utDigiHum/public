№ 265   Jaan Meinson ja Peter Sõukand ette tulnu ja palewa otsust.
Mõistetud:
Et Peter Sõukand Peter Meinsoni käest om ostnu, ja ka selle jago hoonid saab nagu Peter Meinsonil oli.
Otsus:
Peab Peter Sõukand kõik 
1. nee Peter Meinsoni hoone kätte saama.
2. laud tarre ja tõise üttitse hoone jääwa seenini ühes pidada naagu enegi oli, aga kui kumbgi hoonit wälja tahab ehidada siis sawa ütitse hoone kohtu poolt takseritus ja wõtab na kas Meinson ehk Sõukand oma ühenduse ehk kohtu otsuste järele ühes tukis omandada.
3. Maa mõtmise peawa nema 2 kuu aja sees ärä teha laskma.
See otsus §§ 773 ja 774 järele kuulutetu.
Jaan Meinson paleb suuremad kohut.
Opusleht 31 August.
Era kiri 21 Sept.
 
