Ette tulliwa Johann Mita ja Adam Ilwes ja teggiwa leppingot.
Johann Mita wõtap Adam Ilwesse tütre Lisa Ilwes hendale kaswo latses, kes jo nüüd kattesa aastat wanna om, ja lubbab sedda last kui omma last piddada ja kaswatada, ja nida kolitada kui kristlik kerriko sädus nõuwap, ja kui temma ealisses saap, ni kui eggale ütele tüttar latsele, 1 lehm, 1 lammas ja rõiwa kirst kige rõiwaga, ehk kui Johann Mita ärra kolep, ja temmal hendast perrijät ei jää, sis jääp kik temma warrandus Lisa Ilwesele kui temma kaswo latsele.
Adam Ilwes ei perri sedda last Lisat, kui temma ka suures saap, ennamb hendale, ennegi jääp Johann Mitale kui lats.
Seda leppingot kinnitawa nemma omma käe alla kirjotamisega.
Johann Mita XXX
Adam Ilwes XXX
Wallawannemb Peter Paidra [allkiri]
Wöölmöldre Jakob Leppason.
 P.Nemwalz [allkiri] kirjotaja
