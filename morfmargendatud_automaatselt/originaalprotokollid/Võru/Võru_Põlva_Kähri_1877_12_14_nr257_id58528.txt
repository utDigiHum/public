Ette tulli Jaan Kirber ja kaibas Johan Weiko pääle kelle tseä temä niitu 5e ehk 6we wakka ma olewad ärä tsungnu, ja ütlep et temä ajanu esi Weiko tsea Weikol kätte; ja nõwab oma kahjo kost 50 rukka haino oles saanu, mes 100 rubla wäärt olnu. Tunistaia oma.
Johan Weiko selle kaibuse pääle küsitu ütlep et temä tsea wistist ei olewad tsungnu ei olewad ka kunagi olnu, kunas Kirber oma ütlemise pääle oles niidust wälja ajano; sest minol oli karjus allati tsikko takkan.
Jääb poolele kuni tunistaia saawa.
