Olli wallawallitsus ja koggokonna kohhus koon.
Ette tulliwa Serreste Wallawallitsus ja koggokonna kohhus ja tahtwa henda ühhendada Kähri koggokonnaga.
Kähri koggokond wõttap neid wasta sedda wisi:
1. Maggasi willi kokko ja arw errale ni paljo kui meil om, et Serraste wald omma henge willi massap.
2. Neide kroonu wõlg ei puttu Kähri wallale middagi sedda mas Serreste wald essi.
3. Rewisjoon jääp errale ja Nekrudi andmine errale.
4. Maggasi aida ja kohtu tarre üri maswa nema 10 Rublat egga ajasta.
5. Wallawannemba, kirjotaja, ja kassaka palk ja kantselei kullu maswa nemma hengi päält ni kui Kähri wald ütte wisi.
6. Kiodu kõrra ja te säädmisse jäwa ka errale.
7. Jama mas, kerriko mas, ja kerriko tö jääp erräle.
8. Koggokonna kohhus saap ütte kokko ja neil peap 1 wöölmölder ja üts kohtumees ollema.
Wallawannemb Peter Paidra [allkiri]
Wöölmöldre Jakob Leppason XXX
   "  Jürri Jõggewa XXX
Kohtomees Peter Nemwalz XXX
   "    Jakob Kahre XXX
Serreste Wallawannemb Jaan Herne XXX
wöölmöldre Adam Jallas XXX
Kohtomees Jürri Illo XXX
   " Kusta Reggi XXX
Nõumees Johann Kurg XXX
   " Hans Herne XXX
