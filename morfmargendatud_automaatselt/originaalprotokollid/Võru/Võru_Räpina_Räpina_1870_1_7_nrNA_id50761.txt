Kui se 5ta Nowembri prottokoll 1869 aastal Ewa Kriggulson kaibus wasto Gustaw Parmsoni lapse samisse perräst ette olli loeto.
Gustaw Parmson käest sai küssitu, üttel temmä et temmä ei olle mitte seddä tunnistust sanod et Ewa Kriggulson muidega om elläno. Ewa nõudminne om 10 Rubla eggäl aastal lapse ülles piddämisses senni kui kümme aastani.
Kohhus möistis et Gustaw Parmson peab Ewa Griggulsonil eggä üttel lapse ello aastal 5 Rubla maksma seni kui 10me aastaiga täis saap. 1 Febroaril om eggä aasta tärmin künna eggä aasta jaggo rahha peab masseto ollema 1870 aastal 1 Febroaril allustab massu wanna jaggo
§773 om ette kulutedo
Pä kohtomees Jaan Holsting
Kohtomees Kristian Wössoberg
Kohtomees Paap Kress
Kohtomees Hindrik Toding
