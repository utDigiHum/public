Gustaw Puksand kaibas wasto Essä Jaan Puksandi et temmäl om sada Essä Jani käest 28 Rubla 80 kop ja nüüd nõuwap Gustaw Puksand seddä rahha wäljä massetus sada.
Jaan Puksand üttel et temmä ei olle mitte koppikud rahha poial massa.
Gustaw üttel kuis temmä Essäl om rahha andno
8 Rubla ratta rauwa päle
9 Rubla eest linna semend
1 Rubla 25 kop Kresla rauwad
77 kop linno kaskmisse päle anto
35 kop tõrwa eest masseto
76 kop merresi Sällüste eest masseto
20 Rubla 13
1 Rubla 50 kop pärahha
Essä Jaan Puksand üttel, et se rahha peab keik ärrä ollema masseto.
Tunnistust ei ollewa kumbagil
Kohhus mõistis kaibust tühjäs et tunnistust ei olle.
§773 om ette kulutedo
Gustaw pallel prottokoli
Päkohtomees Jaan Holsting
Kohtomees Jakob Türkson
Kohtomees Paap Kress
Kohtomees Writz Solnask
Kohtomees Josep Ritsmann
Kirjutaja J. Jaanson &lt;allkiri&gt;
