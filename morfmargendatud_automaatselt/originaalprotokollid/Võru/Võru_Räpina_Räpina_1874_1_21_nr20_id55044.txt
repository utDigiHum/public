Peter Kirrutson kaibas kohto een, et temmä naine om andno Hindrik Dobrowile 2 Rubla 4 aastaiga aigo taggasi selle eest et Hindrik om lubbano rögga õlle päle Linno tettä 7 Rubla päle ollewa kaup tetto 5 Rubla ollewa wõlgo jänud. Nüüd nõuwap Peter se 2 Rubla taggasi selle perräst et maad ei olle sanud.
Hindrik Dobrow üttel, et temmä om 2 Rubla Petre käest sanud, 1 Rubla om selle eest jänud et röggä om tetto olno ja teise Rubla lubbab Hindrik nüüd taggasi massa.
Jaak Mälton tunnistas et Petre naine om temmä käest 2 Rubla pallelno Hindrikul anda röggä õlle eest anda kus tõugo tettä. Peter om sis wõi Petre naine seddä rahha wõtno kui temmä röggä om tenno.
Kohhus mõistis: et 1 Rubla jääs Hindrikul selle eest et Petre röggä temmä ma pääl olli, ja teise Rubla peab Hindrik taggasi Petrel masma.
