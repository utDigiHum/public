Teine kohhos sel sam(m)al päiwal  Kohtomehe ne sam(m)a
Mõtsawaht Jaan Polloson tulli ja kaibas et temma sullane Johann Raig ollewa joba sel 7dal Januaril 1874 temma käest 1 Rubla käerahha wõtno, kos ka temma essa om mann ollno, ja palgas ollewat lepno 10 rubla, wastse palga pääle olle ka 1 paar sapit pätno 
                                                    a -  50kop kooli trahwi
       massin temma eest -        -     24. do selget rahha 
       andsin temmale           -           10  do - 1 wäits om temma  mull 
                        ärra wõtno                15  do  -   ülle kige teep 99 kop wälja, nüüd om Ronni Mihkli käest ka wist käerahha wõtno, ja om minnu mant ärra länno. -
Kutsoti ette sullane Johann Raig ja sai Jaan Polloson kaibus ette loetus nink kohto poolt küssitus, kas sinna ollet mõtsawahhi käest wastse aasta pääle käerahha wõtno, tolle pääle üttel olle küll käerahha wõtno, om õige. - Kas ollet wastse aijastaja palga pääle 99 kop saano sis üttel olle küll 99 kop. saano, kas ollet 10 rubla palka lepno, sis üttel Johanna Raig ei mitte 10 rubla enge 8 rubla. - olle temmolga lepno, ja minna ütle nüüd weel, et ennamb taggasi temma manno ei lähha. -
Kutsoti ka Ronni Michel Kirristaja ett kes tedda olli wastsest hendale sullases kaubelno ja sai tem(m)alt küssitus, kuidas sinna wõiset ni sugust sullast hendale kaubelda kell joba sel 7 Januaril om wanna perremehhe käest käe rahha ärra wõetu, päälegi om sinno Walla-walitsus keelno, et ni sugutse poisiga ei woi sinna kaupa tetta, ei ka mitte Ronnile wija ollet ülle keelo  länno. -
Tolle pääle üttel Ronni MIchel Kirristaja temma üttel mulle et temmal üttegi assend weel ei olle, egga minna  es tija, et ni suggone assi olli. -
Otsus   Johann Raig massap mõtsawaht Jaan Pallosonile aijastaja palk too om poisi ütlemisse perra - 8 Rubla mes käerahhas anto
                                                                                                                                                                                    1    do
                                                                                                                                 wastse palga pääle anto          -          99 kop  
ütte kokko 9 rubla 99 kop, mes ka Ronni Mihkli Kirristajale sai ütteldus, et palka ütte kopkat kätte ei anna kui enne Jaan Palosoni omma käen om, kui massat sis wastotat ka essi. -
Ja Ronni Michel Kirristaja massap /1/ üts rubla walla waeste laati selle perrast et wallawannemb om nimmelt keelno et ei tohhi poisi Ronnile wija  Ja om ülle keelo tenno. -
Sai kohto otsus ja §773 nink 774 kohton käiadelle ette loetus ja lätsiwa Jaan Palloson ja temma sullane ilm sõnna lausmada wälja, ent Ronni Michel Kirristaja üttel minna ei massa mitte utte kopka egga minna süüdlanne ei olle. -
           Karraskij Kohtomajan  sel 13dal Aprilil 1874
                                                              Pääkohtomees Jaan Tullos  XXX
                                                                 Kohtomees  Jakop Ui bo  XXX
                                                                         do        Michel Wõsso  XXX
