Ette tulli Kusta Sööde Mustist nink kaibas Rentnik Michelson pidi mulle Kontrahti järel Seppa koa kõrra perast üles tegema, Seda tema teinud ei ole, sest kattus on hoopis lagunud ja Tuul ja wihm puhuwad igalt poolt sisse. Mul on Seppa koas kalli riista nink Lõõts maksab mulle 35 R. kui mul piaks kahjo juhtuma, nouan mina tasumist Michelson käest.
Selle kaibuse peale ütel kaibataw K. Michelson "Kusta Sööde ise olli töö man nink pidi teadma, kuis hea ja tarwilik pidi olema.
Otsus. 
Kusta Michelson piab tarwilist Materjali andma nink seltsis Kusta Söödega Seppa koa kõrra piale seadma. 
Seda lubasid Kohto käijad ka teha.
Pääkohtomes   J Roht 
Kohtomes  M Wõso
     "          J Mandli
Kirjotaja A. Muhle [allkiri]
