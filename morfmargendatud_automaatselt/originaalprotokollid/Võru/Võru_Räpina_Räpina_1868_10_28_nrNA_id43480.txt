Michel Hainson tulli kohto ette ja kaibas et temmäl om 1 Sigga 1 1/2 asstaiga wanna om karjustest ärrä tappeto malga ja kiwwidega. 7 Rubla om Sigga wäärt olno.
Karjos Johan Peters käest sai küssitu üttel et körtsimehhe Sigga purri ja siis tulliwad keik Zead ümber 
Karjus Danieli käest sai küssitu üttel temmä et Reedi kui Lumme satte siis tulli körtsi karjus Gustaw Wikking senna ja soos körtsi Orrik pillus keikeki
Karjus Jakob Kiudioski käest sai küssitu üttel temmä et körtsi suur Orrik tulli senna ja kiskus moisa Siggo ja siis om keiks Ainsoni Sea ümber lännowa ja siis körtsi mehhe Sigga purri.
Tunnistusse perräst jääs polele.
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Jaan Holsting
Kohto mees Hindrik Toding
J. Jaanson &lt;allkiri&gt;
