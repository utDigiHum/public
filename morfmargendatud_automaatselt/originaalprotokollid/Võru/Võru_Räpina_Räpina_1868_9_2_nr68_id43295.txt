Moisa wallitsusse polest kaibati et Rodama perremehhed Wido Häidritz nink Hindrik Silatz ommawa nurme sisse tükku turba maad pallama pandno minke ülle moisa wallitusu eddest piddi kahjo arwab et maa kust turbad olles lõigato wõino ärrä om pallano
2.) Et ülle pä olli tullega pallutamist ärä keleto.
Wido Häidritzi nink Hindrik Silatzi kaest sai küssitu ütliwä nemmad õige ollewa et lapsed om pallama pandno.
Kohtomees Jaan Holsting arwas 1/3 maad ärrä pallano ollewad 1 jalg süggäw lohho kottusmaad.
Kohhus möistis et Wido Häidritz 1 1/2 Rubla ja Hindrik Silatz 1 Rubla moisale trahwi peawad maksma 11 Septembril peäb masseto ollema.
Pä kohto mees Paap Kiudorw
Kohto mees Kristian Wössoberg
Kohto mees Paap Kress
ärrä masseto sel 11 Septembr 2 1/2 R.
