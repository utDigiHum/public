Tembe olle Raegla Sep Jaan Oioson omma poigaga Gustawiga sien kohtu ette ning saije todda modu leppitu Walla maggasijast andas Gustawil eige asta 3 wak Rukki 2 wak Keswe ning 6 Rubla rahha soola ja lihha peel, sie tullep ülle kolme aasta 9 wak rukki 6 wak keswe ja 18 Rubla rahha, kui temma erra kolitetu sap ja gep Räppina moisa eihk koggokonna aal, sus ei olle taggase masmist, kui ta teise moisa all eihk kohege moijall tahhap erra minna peap sie moon ja rahha taggase massetus sama, enne ei peese kohhege minnema. Selle lepmissega olle Sep Jaan Oijoson ning temma poig Gustaw rahhu ja kirjutiwa omma kaega sien all.
Jaan Oijon &lt;allkiri&gt; Kustaw Oijon &lt;allkiri&gt;
abbi kohtomees Peter Zupsberg XXX
Rein Pabusk XXX
Jakob Heideson XXX
Jaan Höräts XXX
Peter Jagomann XXX
