N 36. Waimella koggokonna kohto sel 20 Julil 1872
Man olli Pakohtomees Jurri Waaski Abbi Peter Pallo dito Jaan Zuhna
N 33 kohto Protokolli pale and Liso Räbbowõitra tunnistust neide mõllembedde karri omiks to hommongo polde ütten olno, ja omma õkwa karja Peter Leppa pole ajano sest temmale om to käsk kottost anto kui ennegi karri Leppa ma pääl om sis aja kinni.
Mihkli Tiggane om tunnistust andno et Kikka Peter karjas om ajano ne ellaja utten omma karja koddo minnema sis om weel kortsimehhe karjus ülno: mes temma karjaga halbip, temma peap teggema ni kui talle kottust om kästu. Peter Räbboson om nisamma tunnistanno.
Nikui tunnistaja tunnistawa et se kik essi Leppa Petre nõu om olno ja et se karja pois kotton om wõls et temma jäije maggama sis läts karri kara ja et Jaan Liin ommas kahjos middagi ei olle nänno moistap koggokonna seattus:
Moistus: Peter Lepp massap 3 Rbl walla waeste Ladikotte ja se pois Jaan Repp saap 5 witsa löki. 
