Polele jäänud kaibuse asjas sest 7. Maist s.a. sub. № 88, tulli ette Jaan Loodus, kes selle kaibuse peale ütel, "Mina ole Jüri Rabale kül Odra maad andnud, ja on tema sealt jo kaks seemed sealt pealt jo ära wõtnud, enne olid Kartohwlid ja perast Nisu. Kaub oli 2 wilja peale tehtud, ja selle järel oli mul õigus maad tema käest jälle ära wõtta.
Johan Troon tunnistas. "Mina wedasin üle minewa sügise Roni maa peale sitta Rabale, ja Raba tahtis Rukkit sinna sisse teha, aga et hildas oli jäänud, jäid rukkit tegemata, ja tegi Raba tõisel kewadel Nisu sina maa peale.
Otsus. 
Tunnistuse perra piab Jaan Loodus Jüri Rabale se waev, mis tema selle maa peale on teinud, kätte andma, ja annab Jüri Raba Jaan Loodusele sinna peale külitud seme kätte, ja töö eest massab 1 Rubla. nink maa rent.
Otsus ette kuulutud § 773 ja 774 järel. Jaan Loodus es ole rahul, palus luba edasi kaebata mis kohus temale ka lubas.
Eesistja     J. Roht
Manistja    M. Wõso
  "              J. Mandli
Kirjotaja A. Muhle [allkiri]
