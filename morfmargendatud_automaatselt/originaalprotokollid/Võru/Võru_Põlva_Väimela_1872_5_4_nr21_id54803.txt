N 21. Waimellan sel 4tal Mail 1872
Man olli Päkohtomees Jurri Waaski Abbi Peter Pallo dito Jaan Zuhna
Nimmitedu päiwal kaiwas selle koggokonna kohto een Carel Mihkelson et pühhapaiwa sel 23 Aprilel om Lappi Kõrtsi man temma mahha tõmmano Jaan Lillosonn ja om kostno et temma om kaiwato pibo ärrä wõtno, nink om temmale silma päle pesno, nida et silma paal märk olli nätta. 
Sedda luggo om ka wallawannemb Adam Sulg ja kohtomees Peter Pallo nänno. 
Sepäle kostse Jaan Lillosonn et Carel om wõtno temma käest pibo nink om nano koddo ärrä minnema, sis om kaibaja temale lõua isjanu temma kaibaja maha tõmmano, nink rindo päle länno. Nemma omma henda wahhel koggokonna kohton nida ärrä lepno et Lillosonn massap Mihkelsonile üts Rubla, ent koggokonna kohhus om arwano et pühhäpäiwa ni hirmsaste om pesno mõistap koggokonna kohhus kaiwatolle 10 witsa löki.
