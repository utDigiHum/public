Ette tuli Wido Kübbard ja kaebas et temmal Räppinan oppetaja herra juures ülles höikamine Juula Oina läbbi om kinni pantu ja pallep et perra nöutasi mis perrast se om sündinu.
Juula Oinas, masnsaisja temma essa kutsuti ette ja küssiti mis perrast temma sedda om tennu, ja temma tunnist et om Wiido Kübbardiga 3 aastat ütten ellanu ja nowwap et Wiido Kübbar temaga abielluse peap heitma, ja üttel et temma om terwe.
Otsus
Moistis kohhus et Wiido Kübbard peab ütte (1) Rubla öösetse ümberhulkumise eest waeste ladikohe masma Jaan Oinas, Juula Oina essa peab ütte (1) rubla waeste ladikohe masma selle et temma om ütte ööhulkujat omma maja wasto wötnu.
Päkohtumees: Peter Urston
kohtumees: Kusta Rämman
kohtumees: Josep Oinberg
