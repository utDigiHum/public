Marri Sepp käemees Jürri Sepp kaebas: tema on 13 aastad wenda Kristjan Seppa umbes 15 aasta eest teeninud ja on muist palka alles weel saamata ja nimelt riide palka 8 kuube ja 10 alus kuube nõudis oma palka kätte.
Kristjan Sepp ja ühes tema naene Rõõt ütles et (Marri Sepp) nende isa on alles sellel ajal elanud ja peremees olnud ja on naad ühes koos isa juures elanud, pealegi on Marri ühes emaga ise paika omale tööd teinud ja oma kraami ise äranis endale korjanud, tema ei tea mingisugust palka maksa olema ja ei maksa ka tema midagi. 
Marri Sepp nõudis 8 kue eest á 3 Rubla se on 24 Rubla
ja 10 aluskue eest á 2 Rubla se on 20 
ühte kokko 44 Rubla
Moisteti:
Sest et Marri Sepp oma palga saamise ja nõudmise üle mingisugust selgust ega tunnistust ette tua ei saa, kuna se nõudmise ja teenistuse aeg juba 15 aasta eest olnud, nüid Kristjan Sepp enam peremees ei olegi, siis saab sell põhjusel Marri Seppa palga nõudmise asi tühjaks arwatud. Mõistus sai edasi kaebamise tärminidega kohe kuulutud.
Peakohtumees: Hans Ant [allkiri]
Kohtumees: M. Länesar [allkiri]
-"- J. Rätsep [allkiri]
