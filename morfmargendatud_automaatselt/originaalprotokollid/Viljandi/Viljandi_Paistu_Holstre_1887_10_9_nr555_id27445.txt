Andres Tukk nõuab kad. Frommhold Balzari periatelt 8 Rubla mis temal surnud Balzari käest saada jäänud.
Kadunud Balzari periad: Johann Balzar ja Dora Janusson ühes nõumehega Andres Janusson wastawad et nad sellest wõlast ühtegi ei teadwat ega seda maksta ei wõiwat.
Kui nüüd kaebaja oma nõudmist mingil wiisil põhjentada ega tõeks teha ei jõua, sellepärast sai
Mõistetud: Et Andres Tukk sellest oma nõudmisest ühtegi saada ei wõi.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja need Appellatsioni Formaliadega tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumehed JAndresson [allkiri]
                        JJõgewest [allkiri]
Kirjut Raudsepp [allkiri]
