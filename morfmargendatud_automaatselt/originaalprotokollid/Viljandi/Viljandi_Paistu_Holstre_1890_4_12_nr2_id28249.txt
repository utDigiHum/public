Ette tuli Paisto kiriku walla Kappo talu peremees Jaan Rookmann ja kaebab järgmiselt: Minewal aastal on Loodi walla liige Karl Nesler kuni tänawu kewadini tema teenistuses olnud. Teenistuse hakkatus olnud umbes 20. Märtsil. - Karl Nesler on aga nüüd kus uus teenistuse aasta juba hakkanud ja tema ka enne seda ennast lahti üttelnud ei ole, 8"Aprillil s.a. teenistusest äraläinud.-
Kaebaja nõuab poissi Karl Nesleri teenistusse tagasi ehk kui see tagasi ei taha tulla siis nõuab ta sellelt aasta palga 37 rubla. J.Rookmann.
Karl Nesler 17,5 aastat wana, luteri usku, leeris käimata ühes oma isaga Jakob Nesler. Loodi wallast talu pops, ette kutsutud, wastab eesseiswa kaebtuse peale, et peremees Jaan Roosmann teda kätpidi kinni wõtnud ja wärawast wälja saatnud ning teenistusest ära ajanud, sellepärast olla ta ennast mujale ära tellinud ning ei wõiwat enam Rookmanni teenistusse minna ega sellele palka tagasi maksta.- Wõerid tunnistajaid ei ole sääl juures olnud.
Jaan Roosmann ütleb et tema ei ole poissi mitte ära ajanud waid on seda enesega ühes kutsunud aga poiss ei ole tulnud.- Karl Nesler annab üles et tema teenistuse palk mööda läinud aasta eest järgmine olnud:
puhast raha                             10 rubla
kasuk wäärt                                3   "
2 paari paklased püksid           1    "
2 hammed                                  1    "
2 paari sukkad, 2 p. kindad      "   60 kp.
1/3 wakkamaa lina                   10   50
                             Summa         26   50
 Jakob Nesler nõuab ka Jaan Rookmanni käest aasta palka 26 rubla 10 kop, et see tema alaealist poega kätpidi raputanud ja teenistusest ära ajanud kus juba uus teenistuse aasta hakkanud olli.-
Jaan Rookmann wastab et tema poissi ära ajanud ega raputanud ei ole, ning ei taha sellele ka palka maksta. Mööda läinud teenistuse aasta palk olnud kül nenda suur nagu Karl Nesler seda ülesse annud, aga eestulewa 1890/91 aastal pidant Karl Nesler 1/2 wakkamaad lina saama.-
Karl Nesleril ega Jaan Rookmannil ei ole tunnistajaid ette anda, - sellepärast ühendasid endid koosolewad kohtuliikmed Keiserliku Majesteeti käsu peale järgmise Otsusele:
Et Karl Nesler 8 päewa sees tänasest Jaan Roosmanni teenistuse tagasi minema saab.- Kui tema aga tärminil tagasi teenistusse ei läha siis peab ta Rookmannile 26 rubla 10kp. palka tagasi maksma.
Eesseiswa otsus sai tänasel päewal kohtukäiatele kuulutatud.   J.Rookmann
Karl ja Jakob Nesler ei oska nimed kirjutada.
