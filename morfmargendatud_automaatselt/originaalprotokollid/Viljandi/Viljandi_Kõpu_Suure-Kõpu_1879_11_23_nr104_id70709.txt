Peet Sarepera tulli ette ja kaebas, et tema peremees Jüri Glück tema teenistuse palka osalt kini pidada ning mitte wäljamaksta, kus neil kaubeltud olnud, 100 R aasta palgaks, ehk kui peaks juhtuma, et tema sügisel nikrudiks peab minema, siis 60 R suwe palgaks, tema 23 Aprilist kuni 6 Novembrini s.a. teeninud ja nüid wäeteenistusese peab minema ja seda kaubeltud 60 R nöuab, kelest 55 R kätte saanud aga Jüri Glück seda puuduwad 5 R mitte wälja maksta ja palub kaebaja, et kohus Jüri Glücki sunniks, temale seda äratasuma.
Jüri Glück sai ette kutsutud kes ütles, et neil kül nende kui ette antud 100 € aasta palgaks, aga seljärgil kui Peet Sarepera nikrudiks läheb siis 55 R suwe palgaks pidanud saama, keda tellimist ka Tönnis Piiskop teada ning sel wiisil ühtegi Peet Sareperale maksta olla.
Jaan Soob (15 aastad wana) sai ette kutsutud kes ütles, et tema kuulnud kui Peet Sarepera ja Jüri Glück oma wahel kaubelnud, ning Peet Sarepera 100 R aastas ehk kui sügisel nikrudiks läheb siis 60 R suwe palgaks pidanud saama.
Peakohtumees Jaan Kuusik andis ette, et tema Tönnis Piiskopi jutu kuulnud, kus see enast sele kaubast ei mingisugust ütelnud teadwad.
möistetud:
Jaan Soobi tunnistuse kui ka, et suwi palk körgem wöib takserida, peab siis Jüri Glück seda waieldawad 5 Rbl. Peet Sareperale wäljamaksma.
Kohtumees: M. Pern XXX
Kohtumees: Alex. Rosenthal (allkiri)
Kohtumees: T. Juhewitz XXX
Kirjutaja: Köhler (allkiri)
