Johann Takel tulli ette ja kaebas et Johan Uint temale järel nimetud wölgasid maksta olla keda mitte ära tasuda ja palub kaebaja et kohus Johan Uinti sunniks neid temale äramaksma kui:laenatud raha			8 R - kop.		1 paar püksid			2 " 75 "		1 paar sapid			2 " 50 "		1 westi			1 " 75 "		1 hermonik (pil)			7 " 50 "		1 pool waka linaseemneid			3 " - "		
Summa 25 R 50 kop.
Johann Uint sai ette kutsutud kes ütles, et temal köiges 7 R laenatud raha Johann Taklele wölgo olla, muud köik äramaksnud ning sele hermoniko oma kätte kodo wia wötnud ning mitte ostnud.
Karl Pernitz sai ette kutsutud kes ütles et tema muud seles asjas ei tea, kui näinud kus Johann Uint seda hermonikud Johann Takle käest 7 R 50 kop eest ostnud ning sel koral see raha wölgo jäänud.
möistetud:
Johann Uint peab seda laenatud wölga 7 R ja hermoniko raha 7 R 50 kop. ühte summa 14 R 50 kop. Johann Taklile wäljamaksma ning Johann Takelid teiste nöudmistega ilma tunistuseta rahule sundida.
Kohtumees: T. Riesenberg XXX
Kohtumees: J. Michelson XXX
Kohtumees: T. Juhewitz XXX
Kirjutaja: Köhler (allkiri)
