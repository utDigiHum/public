Täna 26"Juulil 1890 astus ette Hans Hanso poeg Kondel, talu peremees Loodist, ja andis üle, et tema selle eesseiswa Paisto kiriku wallakohtu otsuse andmisega tänasest 26 Juulist s.a. Nr. 12 rahule jääda ei wõiwad, waid paluma pidavat seda assja Appellationi teel I Ülema talurahwa kohtusse  saata järgmiseste põhjuste pärast:
1. Et kogukonna kohus temale nõutud summa wälja ei ole mõistnud ehk kül Jaan Soosaar tunnistanud et Hans Kirseli karja pois seal heinamaa peal olnud kui üks kari sääl heinamaa sees olnud.- Karjapois olnud muidugi oma peremehe karjaga sääl sest et tal sääl muud otsimist ei olnud.
2. Ei puutuwat kellegi maade talude karjad senna tema heinamaa juure kui ainult Jaak Undritse, Hans Undi ja Hans Kirsli karjad.
Kaebaja palub et Ülem talurahwa kohus kaebatud mehed temale 100 rubla kahju tasumist maksma mõistaks.
                                                                                                                     Hants Kondel
Eesseiswa Appellationi kaebdus sai Jaak Undritsele ja Hans Undile ette loetud ja ei soowinud nad mitte endale sellest iseäraliku ärakirja.                                          Hans Hunt                      Jaak Undritz
Kästi Hans Kirseli 2" Augustiks s.a. otsuse kuulumiseks ette tellida.
