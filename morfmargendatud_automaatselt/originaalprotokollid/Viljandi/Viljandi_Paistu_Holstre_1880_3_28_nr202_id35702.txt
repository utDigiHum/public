Kaebab Hans Lakk et Hendrikson tema palgast 5 Rubla ilma asjata kinni wõtnud on. Kaebaja nõuab nüüd seda raha.
Hendrik Hendrikson tunnistab tõeks et ta Hans Lakka palgast 5 rubla selle eest kinni wõtnud on et see purjo joonud, magama jäänud ja siis ühe kahehobuse wankri kraawi sõitnud ja katki ajanud, kelle eest tma Aido mõisa walitsusele 5 Rubla maksma pidanud.
Hans Lakk tunnistab tõeks et selle kahehobuse wankre kraawi on sõitnud sest et ohjad on katki läinud. Wanger on kraawi ja ka katki läinud.
Et nüüd Liivimaa talurahva sääduse  § 378 põhjusel iga teener kahjude eest mis ta peremehele teeb wastama peab sellepärast 
Mõisteti: Hans Lakk ei wõi seda 5 rubla Hendrik Hendriksoni käest saada
Kui eeseiswa mõistus kohtukäiatele kuulutatud sai palus Hans Lakk Appellationi mis temale aga seepärast lubatud ei saanud et asi üle wie Rubla ei lähe.
