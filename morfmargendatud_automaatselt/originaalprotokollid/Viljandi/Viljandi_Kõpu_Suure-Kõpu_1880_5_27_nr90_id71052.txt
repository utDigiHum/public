Mari Otti (käemees Jüri Kusik) tulli ette ja kaebas, et Hans Liewakiwi teda enesele naeseks wötta lubanud ning temaga lihalikult koko elanud, kus tema lapse saanud kes nüid 2 kuud wana, Hans Liwakiwi aga teda nüid mitte enesele naeseks wötta ja palub kaebaja, et kohus Liwakiwid sunniks temale 70 R h. lapse toitmiseks abi andma.
Hans Liwakiwi sai ette kutsutud kes seda Mari Ottiga koko elamist ei salganud ning selele see eest 2 R maksnud olla aga ka et teised, kus Jaan Aunap ja Jacob Prual, Mari Otti juures käinud seeperast ei wöida tema ühtegi Mari Ottile maksta.
Karl Maksen, Ano Juhkam ja Johan Kusik said ette kutsutud kes ütlesid näinud olewad kui Jaan Aunap öösel Mari Otti juures maganud ja päewal ühes, töö juurest metsast käinud, aga kas need koko elanud ei teada nemad.
Tönis Tamm sai ette kutsutud, kes ütles enast näinud, kui Jacob Prual öues Mari Otti juures maganud, muud ei teada tema.
Jaan Aunap sai ette kutsutud kes ütles, et tema kül Mari Otti kui ka teiste tüdrukutega wallatust teinud aga selega mitte lihalikult koko elanud.
Selepeale ütles Mari Otti, ei keegi teised kui üksnes Hans Liwakiwi temaga koko elanud, tema ka Liwakiwi käest mitte raha saanud.
möistetud:
Et Hans Liwakiwi isi ütleb enast Mari Ottiga koko elanud olewad, aga mitte selgese ei wöi tehja, et ka teised seda teinud, teda siis seles asjas süialuseks arwata, ja peab kuni see laps 10 aastaseks saab ja elab, iga aasta 5 R Mari Ottile lapse toitmiseks abi andma.
Peakohtumees: J. Kusik (allkiri)
Kohtumees: K. Aunap (allkiri)
Kohtumees: Joh. Tamann XXX
Kirjutaja: Köhler (allkiri)
