Kohtu ette ilmusiwad Wana - Wõidu wallast Muhulase talu omanik Tõnis Tõnista poeg Raudsepp ja selle sama walla liige Jaan Jaani poeg   ....    ja palusiwad oma wahel läbiräägitud ja kindlaks tehtud põllurendi kontrahti kirja panna ja ...waks tunnistada, mis järgmine:
§1.
Muhulase   43  taluomanik Tõnis Raudsepp rendib oma Muhulase talust Jaan Wardja kätte kolmkümmend viis / 35./ wakamaad põldu ja heinamaad mis 1897 aastal endise rentniku Tõnis Puhkru käes tarwitada olnud, ühe aasta peale tänasest päewast kuni 23 Aprillini 1899 aastani tarwiliku karjamaaga, mida Wardja 23 Aprillist 1898 a. kätte saanud.
§2.
Rentnik maksab selle põllu, heina ja karjamaa eest aastas iga põlluwakkamaa eest
