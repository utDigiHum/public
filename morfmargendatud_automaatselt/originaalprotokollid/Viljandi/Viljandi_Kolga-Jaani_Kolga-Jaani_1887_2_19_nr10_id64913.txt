Jaan Kull kaebab, tema püss olnud Murraka ? taga ja on sealt ära kadunud, tema hakkanud ühe jälgi mööda järele minema, jälled läinud metsa ja on sealt mütsi leidnud, mis Johann Ossik ja Jaan Jakobson on Jakob Kassi omaks üttelnud ja pärast on ka Jaak Wäljapõhi seda mütsi Jakob Kassi mütsiks üttelnud. Nüid on aga Jakob Kass temaga ära leppi tahtnud 1. Rubla kätte annud ja 3 Rubla pärast lubanud tua, aga nüid ei olema seda raha mitte ära maksnud, tema ei woi ka 4 rublaga ? rahul olla, sest tema püss maksnud 8 Rubla; Tema on aga selle pärast ära leppinud et asi selgeks saab. 
Johann Ossik tunnistas, Jaan Kull toonud ühe mütsi kõrtsi sisse ja küssinud kas keegi tunneb seda mütsi, Jaan Jakobson arwanud et se on Jakob Kassi müts ja tema arwanud et se ehk wõib Kassi müts olla, sest et sell nisugune maha wajunud äär olnud kui Kassi mütsil, pärast on aga Jakob Kass sisse tulnud ja olnud nisama sugune müts peas, tema ei woi siis üttelda et se Jakob Kassi müts on.
Jaak Waljapõhi tunnistas, tema on küll näinud et Jakob Kassil just nisugune müts ikka peas olnud, aga täiesti tõendada ei woi ta mitte et just se Jakob Kassi müts on.
Otsus: Jakob Kass ette telli.
Peakohtumees Hans Ant [allkiri]
Kohtumees M. Länesaar [allkiri]
-"- Jüri Kass [allkiri]
