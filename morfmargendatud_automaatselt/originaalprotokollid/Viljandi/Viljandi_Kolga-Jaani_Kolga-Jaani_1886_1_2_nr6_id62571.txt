Õppetaja härra kaebuse kirja peale 14. Decembril 1885 oli Ann Mikk ühes käemehega Hans Euts ette kutsutud. Ann Mikk seletas kaebuse peale et tema ei ole kellegis asjas sõna kuulmatta ega wasta hakkaja olnud ei ka midagi waletanud. Muud ei ole midagi olnud kui proua käskinud teda ülewal köögis lehmale wett soendada, ta on palunud et se on ülewal köögis raske teha aga ta on ikka seal wee ära teinud. Teda on aga jahu wargaks ööldud ja ära aetud, tema on aga ilma süüta ja on ka kihelkonna kohtuse kaebanud et ta oma aasta palka kätte nõuab.
Kiriko muisa kärner Hans Pung ütles, proua kässinud tüdrukut köögis lehmale wett soendada aga Ann Mikk üttelnud et ta seda ei saa teha. Üks kord üttelnud Ann Mikk et sulbile on wesi peale pantud aga ei ole mitte wett peal olnud. - Jahu warguse asjast ei tea tema midagi, se asi olnud enne teda. 
Ann Mikk seletas, tema on sulbile wee peale pannud nenda kui tarwis ja wee on ta ülewal köögis kohe ära soentanud. 
Otsus: Et Ann Mikk oma palga nõudmist ülewasee kohtusse kaebanud ja siin ei ole ka need kirjalikult üles antud süid mitte selgeks saanud, ei wõi Ann Mikk mitte trahwitud saada. - Otsus sai kohe kuulutud. 
Peakohtumees Hans Ant [allkiri]
Kohtumees Jürri Jürrison [allkiri]
-"- Märt Länesar [allkiri]
