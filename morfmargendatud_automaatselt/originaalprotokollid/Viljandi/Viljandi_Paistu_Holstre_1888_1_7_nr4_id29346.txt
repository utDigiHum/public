Kaebab Peter Sepp et ta oma wenna Johann Seppa, 17 wana, rätseppa Mats Musti juure 2 aastaks õppima tellinud ja maksnud 10 rubla kindluseks sisse et sellest siis meister pidada wõib kui õpipois enne kahte aastat ära peaks minema. Pärast 2 aastat mööda jõudmist pidanud Mats Must selle raha tagasi maksma, aga ei makswat mitte. Kaebaja nõuab nüüd oma raha kätte. Tunnistaja Peter Kass.
Mõisteti: Assja toimetusele wõtta.
Kirjut Raudsepp [allkiri]
