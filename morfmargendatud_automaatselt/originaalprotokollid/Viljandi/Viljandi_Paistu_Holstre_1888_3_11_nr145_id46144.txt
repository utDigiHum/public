Ette tuli tütrik Epp Lillimägi ühes nõumehega Jaak Sowa ja nõuab surnud Fromhold Balzari periatelt oma 19 teenistuse aasta eest mis ta kadund Balzari teeninud, palka  200 rubla.
ja sellele laenuks antud raha                                        12   "
                                                                         Summa 212 rubla.
Surnud Fromhold Balzari periad Johann Balzar ja Dora Janusson ühes nõumehega Johann Markus ette kutsutud, wastawad et nemad sellest midagi ei teadwat et Epp Lillimägi palk wõlgu olewat, ning wõiwad nad sellepärast mitte lubada et see Balzari perandusest maksetud saab.
Epp Lillimägi ütleb et temal tõeste 19 teenistus aastade palk Balzari käest saamata jäänud on, aga temal olewat selle oma nõudmise tõeks tegemiseks kül mitte tunnistajaid üles anda.
Mõisteti: Et Epp Lillimägi seda oma nõudmist tõeks teha ega põhjentada ei ole jõudnud sellepärast ei wõi ta sellest nõudmisest ühtegi saada.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja Appellatsioni Formaliad tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JAndresson [allkiri]
Kirjut Raudsepp  [allkiri]
