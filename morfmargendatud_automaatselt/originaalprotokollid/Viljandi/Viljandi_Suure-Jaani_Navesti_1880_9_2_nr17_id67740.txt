Sel 2 Septembril 1880
juures ollid: Tõnnis Elbrecht, peakohtumees
                       Jaan Taar, kohtumees
                       Jaan Koff,   --
Hans Mill tulli ette ja kaebas: kaebaja pois Ado Madawer olla kaebajad kobuste waljastega hirmsal wiisil pea pihta peksnud, nenda et kaebaja 2 nädalat aega sepeale haige olnud. Pealegi olla Ado Madawer keigis asjus oma teenistuses kasu wastupaneja sel wiisil, et ta ial seda ei tee, mis kaebaja teda käsib: suil keige pakkilisem töö aial olla Madawer palju päewi ära wiiwitanud sest et ta ial enne töösse ei ole läinud, kui alles sööma aial. Ke eila olla Madawere kaebajal ähwartanud "Lämuks teha". Niisugusel järgil näeb kaebaja ennast tunnistut, kohut paluda, Ado Madawere peksmise pärrat kange säädusliku trahwi alla tõmbada ja temale peale panna, 20 rubla kaebajale walu raha maksa ja wiimaks, teda kaebaja teenistusest lahti lasta sest et ta käsuwastupaneja ja kaebajat alles eila ähvarranud "lämmuks teha".
Ado Madawer wastas kaebduse peale, ta ei ole oma peremeest Hans Milli mitte peksnud, ei ka teda ähvartanud, ei ole ka ühtegi teeinustuse pääwa ära wiiwitanud.
Kadri Soobel tunnistas: ta olla selgeste näinud, et Ado Madawer ilmaasjata Hans Milli kaks korda raud waljastega pähe löönud, mis peale Hans Mill kaua aega haige olnud, weel teada tunnistaja, et Ado Madawer sugugi peremehe käsku ei kuulda ja et ta keik sel suil õigel aial töösse ei ole läinud, waid alles siis töösse tulnud, kui teesed pererahwas juba söögi wahe tööd ära teinud. Eila olla Madawe ka peremeest ähvartanud "lämmaks teha". 
Kohtumees Jaan Taar andis tunnstust: Han Milli nõudmise ja palumise peale, se üle, et Ado Madawer sugugi käsku ei kuulda ja töösse ei tahta minna, olla tunnistaja kakskorda Han Milli tallusse läinud ja leidnud, et Madawer enne töösseei läinud, kus alles sööma aial.
Wallawanem Tõnnis Wildfluch tunnistas: ta teada selgeste, et Ado Madawer iga pääw peremehe käsu wastupaneja olnud ja ilmaski õigel aia töösse ei ole läinud.
Kui kohtukäiatel mida enam ette tuua ei olnud siis sai
mõistetud:
Kui tunnistajate läbi tõeks tehtud, et Ado Madawer peremeest Hans Milli peksnud, teda ka weel eile ähvartanud lämmaks teha ja weel, et ta käsu wastupaneja ja laisk on, siis maksab Ado Madawere peksmise pärast 4 rubla trahwi 8 pääwa sees waeste laadikusse ja pean tänasest pääwast Hans Milli teenistusest ära minema. Hans Mill aga peab Madawerele tema teeinutd palga, kui teda weel sees on, wälja maksma. 
Moistus sai kuulutud ja palus Madawer appelatsioni mis temal ka lubatus sai järel anda.
