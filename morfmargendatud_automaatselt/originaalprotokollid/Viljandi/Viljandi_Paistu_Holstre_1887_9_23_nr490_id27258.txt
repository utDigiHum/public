Holstres 23 Septembril 1887.
Ette tuli Märt Kansi ja nõuab surnud Frommhold Balzari periatelt järgmist töö palka ja raha wõlga:1.			Kewadel põldu künnud ja kartuhwlid teinud			6 päewa		2.			Ühes naese ja tütrega nädala aega heinal kainud			18  "		3.			Tütrega ühes ristik heina wedamas olnud			2  "		4.			Enneseda on weel naene ja tütar heinu teinud			4  "		5.			Sõnnikud wedamas tütrega olnud			2  "		6.			Rehte peksmas ühes tütrega olnud			4  "					Summa			36 toopäewa		
Nende hulgas on 34 jalg tööpäewa kelle eest ta 25 kop kokku aga 8 Rbl. 50 kp
nõuab ja 2 hobuse päewa eest                                                              1  " 50
Sula raha räimede eest                                                                           1  " 40
Keik kokku 11 Rubla 40 kp.
Mõisteti: Assja toimetusele wõtta.
Kokirjutaja Raudsepp [allkiri]
