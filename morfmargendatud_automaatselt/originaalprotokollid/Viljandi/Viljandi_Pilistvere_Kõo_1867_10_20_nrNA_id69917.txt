Mitto Arrosare mees kaebasid, et nende külla perremehhed, mis all nimmetud, wägga holetumad on ja mitte ehk kül mitto korda käsko on sanud, talli wilja poldudele aeda ette ei te, mis läbbi paljo kahjo ja pahhandust tulnud.
Need sanna kuulmatta perremehhed on:
Hans Põdra
Hans Riik
Jaan Riik
Tõnnis Luik
Jaak Tamm
Mart Tamm
Hans Sihw
Jürri Säro
Jürri Jürrisonn
Jürri Kukk
Mõistetud: igga perremees maksab kui eddespiddi weel ette tulleb sanna kuulmatta mele ja selle läbbi tulnud kahjo eest 3 Rubl. walla waeste laekasse.
Peakohtomees: Jaak Kukmann
Kohtomees: Jürri Käärdt. XXX
Walla Wannem: Karel Saar XXX
        dito        abbi: Jakob Müller.
