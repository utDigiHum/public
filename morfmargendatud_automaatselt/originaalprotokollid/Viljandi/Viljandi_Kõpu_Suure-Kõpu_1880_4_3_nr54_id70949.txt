Lieso Johanson (käemees Jüri Tedder) tulli ette ja kaebas, et Tönis Tietel temaga lihalikult koko elanud kus tema lapse saanud, ning palub kaebaja, et kohus Tönis Tietelt sunniks temale ühte summa 15 R lapse toitmiseks abi andma.
Tönis Tietel sai ette kutsutud kes seda ei salganud ning maksis seda nöutud 15 R siin samas Lieso Johansonile wälja, kus neil seles asjas peale seda enam mingisugust nöudmist ega seletust üksteisega ei jäänud.
möistetud:
Seda nönda kui sündinud maha kirjutada.
Peakohtumees: J Kusik (allkiri)
Kohtumees: Alex. Rosenthal (allkiri)
Kohtumees: Tonis Riesenberg XXX
Kirjutaja: Köhler (allkiri)
