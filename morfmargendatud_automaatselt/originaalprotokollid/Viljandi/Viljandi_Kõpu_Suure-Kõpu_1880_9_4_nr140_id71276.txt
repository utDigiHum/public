An Mürisep (käemees Jaan Pill) tulli ette ja kaebas, et Karl Michelson teda enesele naeseks lubanud wöta, temaga lihalikult koko elanud ning tema lapse saanud kes nüid 3 kuud wana, nüid aga teda mitte naeseks wötta, ja palub kaebaja, et kohus Karl Michelsoni sunniks temale 90 R lapse toitmiseks abi andma.
Karl Michelson sai ette kutsutud kes seda An Mürisepaga koko elamist ei wöinud salgada ütles aga, et tema An Mürisepa mitte enesele naeseks wötta lubanud ning see alati isi tema juure tulnud, mis läbi ka selega kokoelamine sündinud.
Möistetud:
Karl Michelson, peab Ann Mürisepale iga aasta, kuni see laps 10 aastaseks saab 5 R lapse toitmiseks abi andma, ning seda esimise aasta raha, 29 Septembrini s.a. ära tasuma.
