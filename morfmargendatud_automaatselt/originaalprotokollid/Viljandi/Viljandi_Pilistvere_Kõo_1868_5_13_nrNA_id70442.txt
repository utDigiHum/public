Kõowalla Pikkali perremees Andres Konks, kes 58 aastad wanna, haige seggane ja kelle perrenaene surnud, astus koggokonnakohto ette ja kaebas, et temmal mitte ennam wõimalik ei olle sedda temma krono maia piddada egga selle eest hoolt ja murret kanda, ja temma essiminne poeg Tõnnis kellel muhk jalla peal ja haige seggane ja se läbbi ka mitte maia tallitust omma peal ei tahha egga wõi jätta, - ja pallus Andres Konks, et temma krono rentkohha peale Tallo  N 25 Pikkali koht temma teine poeg Hans Konks kes 21 aastad wana saaks perremehheks pandud ja kinnitud.
Mõistetud: Hans Konks Pikkali Tallo perremehheks N 25 kirjotada ja Tarto Krono mõisa Wallitsusse kinnitusseks sata.
Peakohtomees: Jaak Kukmann
Kohtomees: Tawit Markus. XXX
-  Andrej Krasowskj
Wallawannem:
