Koggokonna kohhus sel 17. Nowembri k. p. 1862.koggokonna peakohtomees Karl JosephKohtomehhed: Jaan ReimannJürri LarentzMihkel Loks, Wannapõltsamalt.
wata Protokolli eilsest päwast pkt. 2.Juhhan Will selletas, et Jacob Turro se hobbose Eriko-kõrtsi jurest lahti lasknud, pärrast kahhekeisi käinud Jakobi hobbosega tagga otsimas polle leidnud, wiimaks ollewa Jakob isse üllesleidnud, siis ühhes Eriko kõrtsi ees peale istnnud ja Annikwerre poole läinud. - Annikwerre küllast ollewa siis Jakob üksi Nõmme läinud.
Jacob Turro salgas,- ütles et üksi Põltsamalt koddo läinud.(Soldat Karl Lehman on näinud, kui kot Reini sauna jurest metsa wiidud.)
Juhhan Will selletas, et temma minnewa näddalilkeik sedda Reino Jaanile üllesräkinud.
Reino Jaan, kes eile keik salgas, selletasnüüd, et se temmal olnud melest ärraläinud.
Kaebaja selletas, et neljakeisi olnud, kui teddatee peal pekstud. - Nõudis 15 Ru hõbb. wallorahha.Kännu Juhhan, kes ka peksa sanud 4 Ru hõbb.
moisteti:on keik ühtlaisi süidlassed, maksawad siis kaebajalle 10 Ru hõbb. wallorahha ja Känni Juhhanile 3 Ru hõbb. mis igga-ühhe peale 325 kop. hõbb. wilja teeb.Jacob Turro ja Jaan Tedder sawadkumbgi 30 keppi lööki, Ants Will 30 hopiwitso ja Juhhan Will 3 päwa wangi.
