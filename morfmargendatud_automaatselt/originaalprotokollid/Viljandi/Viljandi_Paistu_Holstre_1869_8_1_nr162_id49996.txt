Kaebab Johann Assu Janus Pill peäle et see oma karjaga tema rukki ära söötnud olla.
Janus Pill ütleb aga et tema kari mitte Johann Assu rukkise saanud ei olla.
Kui nüüd Johann Assul kedagi tunnistust selle asja juures ei ole et Janus Pilli kari tema rukkis käinud oleks ning tema ka kunagi kord Janus Pilli elajat rukkist kinni võtnud ei ole sellepärast sai
Mõistetud:
Tunnistuse ning selge tõeks tegemise puuduse pärast ei või Johann Assu sellest oma kaebdusest ühtegi saada.
See mõistus sai mõlematele kohtu käiatele kuulutud.
Kogukonna kohtu eesistja J Hansen
Kõrvalistja J Moks
" J Ainson
