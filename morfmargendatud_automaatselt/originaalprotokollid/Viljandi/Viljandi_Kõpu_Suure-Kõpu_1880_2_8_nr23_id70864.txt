Kohaline metsa walitseja, herra Schröder tulli ette ja kaebas, et Tönnis Lacks ja Hans Gustawson möisa metsas walmis raiutud sindli puid sillaks pruukinud, ja palub kaebaja, et kohus niisugust oma wöimusega puu pruukimist ära keelaks.
Tönis Lacks ja Hans Gustawson sai ette kutsutud kes seda oma wöimusega puu pruukimist ei wöinud salgada,
möistetud:
Tönnis Lacks ja Hans Gustawson seles asjas süialuseks arwata ning kumbagid á 50 kop. waeste laekase trahwida.
