Jüri Thomson tulli ette ka jaebas, et Märi Otti teda kiwiga löönud, seeperast et tema seda öhtu ilja maeast wälja aeanud ja palub kaebaja, et kohus tullitegiad trahwiks.
Märi Otti (käemees Jüri Reimann) sai ette kutsutud kes ütles, et tema mitte Jüri Thomsoni kiwiga köönud aga Jüri Thomson teda maeast wälja aeanud ja kiwidega pildunud kus temal kus temal Hans Liwakiwiga könelemist olnud.
Möistetud:
Jüri Thomsoni ilma tunnistuseta oma kaebtusega rahule sundida.
