№ 66 Kõowalla Anni Tallo perremees Tõnnis Annow, kes wanna, agga wägga haige seggane, nenda et igga päew kardab ärra surrema, pallub omma essiminne poeg Mart, kes ontlik mees ja 20½ aastad wanna nüüd Protokolli ramatusse üllesse kirjotada selle temma tallo perremehheks ja ka krono Tarto kohto polest kinnitada lasta.
85. Essi tallo perremees Hans Riisk, kellel teine Naene, pallus koggokonnakohto Protokolli ramatusse mahha kirjotada, et temma ja temma essimisse naese poeg Josep tännasest päwast selle temma tallo perremehheks saaks kirjotud ja ka Tarto kohto polest kinnitud saaks.
Mõistetud: nende mõllematte palwe järrele tehha.
Peakohtomees: Jaak Kukmann
Kohtomees: Andrej Krasowskj
Kohtomees: Tawit Markus. XXX
Wallawannem Rein Kopp
