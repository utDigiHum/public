Ette tuli Holstre kogukonnas passi peäl ennast üles pidav Villandi kogukonna liige Hans Tammist ning kaebas Koigu peremehe Andres Koik peäle et see tema vilja 6 vakka otre ja 6 vakka kaeru, mis ta mineva aasta kottidega magatsinise Andres Koik eest sisse maksnud, nüüd välja toonud ning omale võtnud olla.
Andres Koik ette kutsutud vastab selles puuduva küsimise peäle et ta seda vilja 6 vakka otre ja 6 vakka kaeru mis Hans Tammist mineva aasta sisse maksnud, selle pärast välja võtnud ja omale pidanud olla, et see tema Koigu põllu peält saadud vili on olnud.
Hans Tammist ütleb et ta seda vilja Andres Koigu käemehe Jaab Laane käsu peäle sisse maksnud olla.
Kui Jaan Laane selle üle küsitud sai tunnistas ta Hans Tammiste ütteluse õige olevad.
Astusid keik ära.
Kui nüüd Andres Koik ise tunnistab et tema seda Hans Tammistest tema eest sisse makstud vilja endale võtnud on, sellepärast sai
Mõistetud:
Andres Koik peab Hans Tammistele 6 vakka otre ja 6 vakka kaeru varsi kätte maksma.
Kui seesinane eelseisva mõistus mõlemate kohtu käiatele kuulutud sai, ütles Andres Koik et sellega rahule jääda ei võivad ning Appellatsiooni tähte paluma pidavad; mis temale ka lubatud ja täna välja antud sai. 
Kogukonna kohtu eesistja J Hansen
Kõrvalistja J Moks
" M Tuk
" J Tomson
" J. Ainson
