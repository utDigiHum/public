Lieso Kuusik (Käemees Jüri Kuusik) tulli ette ja kaebas, et tema isa Joh. Kask teda söimanud ühe tönniga lükanud, kui ka sele tönni kanega lüia ähwardanud, kus tema (Lieso) sele juures ära kohkunud ja sele järele haigeks jäänud, ja palub kaebaja, et kohus seda asja läbi kuulaks ka tülitegiad trahwiks.
Johann Kask sai ette kutsutud kes seda söimamist, lükamist kui ka ähwardamist salgas.
möistetud:
Kaebajaid ilma mingisuguse kaebtusega rahule sundida.
