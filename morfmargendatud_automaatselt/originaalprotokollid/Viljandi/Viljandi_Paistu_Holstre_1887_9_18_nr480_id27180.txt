18 Septembril 1887.
Johann Wiidika kaebduses Janus Rekkandi wastu, nõudmise pärast, olid mõlemad kohtukäiad tänasel päewal ette tulnud.
Janus Rekkand wastab sellesinatse nõudmise peale, et temal Johann Wiidika käest hobuse hind 35 rubla, kätte saamata olewat. Johann Wiidik wiinud tema hobuse kewadel 1887 Willandise laadale müünud selle senna ära aga raha ei ole ta sellele ära wiinud. Palka olewat kül 35 rubla ja muud raha 16 rubla saamata. Kui nüüd palga raha hobuse hinnaga tasa läheb siis olla kaebajal weel ainult 16 rubla saada.
Johann Wiidik wastab et tema selle hobuse 29 rubla eest ära müünud ja on see raha kül tema kätte jäänud sest et Janus Rekkand laadal enne hobuse müümist weel enam kui seda wäärt raha laenuks wõtnud. Selle tunnistuseks olewat üks kingsepp Willandist kelle nime ta praegu ei maletawat.
Janus Rekkand wastab et tema laadal Johann Wiidika käest raha laenuks ei ole saanud.
Mõisteti: Kohtukäiad ja tunnistaja, kelle nime Johann Wiidik järele kuulama ja üle andma peab, ettekutsuda.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JJaama [allkiri]
Kirjut Raudsepp [allkiri]
