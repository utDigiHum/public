Holstre Kogukonna kohtus 27 Nowembril 1887.
Koos olid: Peakohtumees Jaak Luik
                   Kohtumees: Johann Jõgewest
                        "                Jaan Jaama
                        "                Jaak Sowa
/:Kohtuistumine algas kel 11 ajal:)
Jaan Tormi kaebduse asjas Jaan Kibe wastu nõudmise pärast, puudus täna kaebataw Jaan Kibe.
Mõisteti: Neid uuesti kutsuda.
Peakohtumees JLuik [allkiri]
Kohtumees JJõgewest [allkiri]
                     JJaama [allkiri]
Kirjut. Raudsepp [allkiri]
