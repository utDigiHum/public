Ette tuli Tõnis Suhkur ja kaebab et külakubjas Jaak Kibe temale käsku annud ei ole, et tema koolilapse kooli saatma oleks pidanud.
Jaak Kibe wastab et tema omalt poolt need käsud järgeste wäljaanda on lasknud Jaak Asu läbi kes tema pois on.
Jaan Asu wastab et tema koolilaste kooliminekuks käsu terwe wakkusele ja ka Tõnis Suhkrule wäljaannud on.
Mõisteti: Tõnis Suhkur ei wõi sellest omast kaebdusest ühtegi saada.
Eesseiswa mõistus sai kuulutadud.
