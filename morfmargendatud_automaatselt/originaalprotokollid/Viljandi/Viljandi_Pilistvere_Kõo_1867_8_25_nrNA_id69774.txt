Kõnno Metsawaht Jürri Mölder kaebas, et Kallani Hans Kallmann ja temma poeg Hans, kes Suurkoddal on, möda Kõnno metsa keppidega ja temma ei teada, mis tarwis nemmad seal käinud.
Selle peale wastas Hans, et temma omma ühte ärra kaddunud ellajast ja sai sepärrast se assi tühjaks mõistetud.
Peakohtomees: Jaak Kukman
Kohtomees: Ado Lagus. XXX
Wallawannem: Rein Kopp
