Ette tuli Ado Karro ning kaebas Jaak Konni peäle et see Raasilla kõrtsi juures tema käest 10 rubla hõbe kaartidega ära mänginud; et see õige et see raha keik Jaak Konni kätte läinud võida tunnistust anda Jaak Purro ja Andres Konni.
Jaak Konni ette kutsutud vastab selles puuduva küsimise peäle et ta paljalt 2 rubl. h. Ado Karru käest kaartidega ära mänginud olla.
Tunnistus Jaak Purro ette kutsutud ütleb küsimise peäle, et tema oma silmaga näinud olla kui 3 rubla Ado Karro raha Jaak Konni kätte mängimise läbi läinud on.
Andres Konni kes ka tunnistuseks üles antud ei ütle ennast sest asjast ühtegi teadvad.
Astusid ära.
Mõisteti:
Talurahva seäduse raamatu 1860 aastast p. 1079 põhja peäl, läheb Ado Karro 24 tunniks vangitorni, Jaak Konni saab aga 10 hoopi vitsu ning peab peäle selle see kaartidega võidetud 3 rubl. h. Ado Karule tagasi andma.
See eesseisva mõistus sai kohtu käiatele kuulutud ning ka täidetud.
Kogukonna kohtu eesistja J Hansen
kõrvalistja J. Ainson
"  Tuk
" abi Andres Uint xxx
