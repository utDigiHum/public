Jaan Ott kaebas, Mall Kiis tema naese õde on oma lehmaga 3 aastad tema korteris ja söögis olnud; Webruari kuu sees läinud Mall küll ise ära aga jätnud lehma senna, ta annud küll siis käsi katte et kohe lehma ära wiib, kui senna jättab maksab 1 rubla öö ja päeva pealt, sest temal põhku ei ole, aga lehm on praegu seal, nõudis 6 Rubla iga kuu pealt Malle ja lehma söögiraha kokku. 
Mall Kiis käemees Hans Eits ütles et tema on järjest Jaan Ott töös olnud, üksi siis ei ole saanud tööd teha kui ta haige olnud; Lehm olnud Jaan Otti käes pruuki, sest piim ja wõi saanud ottele, piim saanud ühes koos ära söötud, aga wõi nimelt 2 leesikat on Ott ära müinud ja raha omale wõtnud. Jaan Ott annud küll käsu kätte lehma ära wia, aga temal ei ole kuskil kohta olnud.
Jaan Ott ütles, se lehm on nüid alles leidnud ja keik se aeg kui Mall ära on, muidu seda söötnud.
Moisteti 
Et Jaan Ott on 3 kuud peale seda kui ta käskis lehma ära wia, seda puudulikku ajaga söötnud, maksab Mall Kiis temale iga kuu 5 Rubla söögi raha nenda et 15 Rubla maksa tuleb. Et naad aga ennemalt leplikult ise on koos elanud ei wõi nende aastate eest mingitsugu söögiraha arwatud saada.
Moistus sai edasi kaebamise tärminidega kohe kuulutud. 
Peakohtumehe abi Jüri Kass [allkiri]
Kohtumees Jürri Jürrison [allkiri]
-"- M. Länesar [allkiri]
