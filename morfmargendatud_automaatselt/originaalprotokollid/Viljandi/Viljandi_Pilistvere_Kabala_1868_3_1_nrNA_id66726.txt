Perremees Tõnnis Kulpuss tulli kohto ette ja kaebas Wanna Willandi walla meeste Ado Palandi ja Tõnno Jerwekulle peale kes temma kuhjas 2 kormad heino on warrastanud mis Tõnnis Kulpuss sure waewa ja otsimissega nende jurest on leidnud kus ka Willandi kohtumees Jürri Kude on jures olnud kui Tõnnis Kulpuss need heinad on leitnud Ado Palandi ja Tonno Jerwekandi jurest
Mõistetud
Tõnno Jerwekant ja Ado Paland maksawad nende warrastud heinde eest, mis 70 leisikat arwatud on 7 R. ja Tõnnis Kulpussi waewa eest 3 R., peale sedda sawad mollemad 20 hopi witso warguse pärrast.
7 R. schuldig bezahlt
Pea kohtumees Jürri Koik
