Ramlaiv Issand tulli kohto ette, ja kaibab et temma õppipois Jürri Köpmand kes kolme aasta eest temma juurde wie aasta päele Tõllaseppaammetet öppimma olli antut, agga Jürri Köpmand temma jurest ilma lubbata ärraläinud.
Kutsuti Jürri Köpmand kohto ette, ja anti temmale se köwwa käsk, warsi Ramlou Issanda jurde taggasi minna, ja need kask aastat nönda truiste ja sannakulelik olla kui se esimenne aasta, ja saab Issanda käst nende kahhe aasta eest kolmkümmend rubla ja kahhi aasta pearahha, kui agga Jürri Köpmond mitte truiste ja sönnakulelikkult ei teni, ja Issandalle käemist ja pahhandust teeb, saab ta warsi kohto ette woetut, ja kohhus moistab temma ülle kohhut kas ihho rapi woi järrele tenimist Issanda jures kui nemmad sedda tahhawad.
Selli kohto kullo maksab Issand.
Peakohtomees Jürri Hühnerson XXX
Kohtomees Jaan Kiis XXX
Kohtomees Märt Illison XXX
