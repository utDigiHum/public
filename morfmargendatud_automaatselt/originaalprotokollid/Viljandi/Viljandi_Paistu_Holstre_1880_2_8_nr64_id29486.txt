Ette tuli Hannus Neumann ja kaebab et Aidu pois Hans Jaakson ühel Pühapäewa hommikul wara tema majasse tulnud, sääl tülitsenud ja teda otsani narriks nimetanud. Kaebaja palub et see sääduslikult trahwitud saaks.-
Hans Jaakson wastab seepeale et tema tütruku käeraha sinna tagasi wiinud ja sääl juures muud midagit üttelnud, et selle tükki oled sa narriste teinud.
Tunnistaja Peeter Märtenson ütleb ennast kuulnud olema kui Hans Jaakson Neumanni "otsani narriks nimetanud".
Mõisteti: Hans Jaakson peab 1 Rbl. Holstre walla waeste laeka hääks trahwi maksma.-
Eessseisuw mõistus sai kuulutatud.
