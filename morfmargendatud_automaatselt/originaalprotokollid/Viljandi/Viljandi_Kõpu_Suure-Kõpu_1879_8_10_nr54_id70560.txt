Hans Meier (käemees Tönnis Meier) tulli ette ja kaebas, et Hans Grimpe tede teenistusest äraaeanud ja riided pantinud ja palub kaebaja, et kohus seda asja läbi kuulaks ja Hans Grimped sunniks tema riided wälja andma kui ka teenistuse eest palka maksma.
Hans Grimpe sai ette kutsutud kes ütles, et Hans Meier temmale wastopanelik olnud, kus tema siis selele joobnud peaga ütelnud, et sel wiisil mitte aastad täis ei teeni ning wöib äraminna, peale seda jäle Hanso enesele tagasi tellinud aga see mitte tulnud.
Jüri Juhkum sai ette kutsutud kes ütles, et Hans Grimpe ja Hans Meier üksteisega tüllitsenud kus Grimpe Meiert käskinud äraminna.
Meier ja Grimpe lepisid omawahel senna, et Hans Meier jäle oma teenistusese tagasi läheb ja kuni 1 1/2 nädalad peale Jüripäewa 1880 teenib ning siis oma tellitud aasta palka täieste wälja maksetud saab ilma et need temast siaaeani ära wiidetud teenistuse päewad tema kahjuks arwatud saawad.
möistetud:
Seda nönda kui sündinud maha kirjotada.
