Kaebab Hans Adamson et Jaak Jänes kes selle aegne külakubjas olnud, mis käsku annud ei ole, et Mari Päts kooli minema pidanud ning too 42 päewa koolist wäljajäänud.-
Jaak Jänes ütleb et terma tääda on Käsk igakord wäljaantud saanud.-
Johann Kuusk kes Jaak Jänessa pois on ütleb et tema selle käsu wäljaannud on Hans Adamson olla ise Külakubja kirja lugenud.-
Mõisteti: Jaak Jänes jääb priiks, et käsu wäljaannud on.-
Eesseiswa mõistus sai kuulutadud.
