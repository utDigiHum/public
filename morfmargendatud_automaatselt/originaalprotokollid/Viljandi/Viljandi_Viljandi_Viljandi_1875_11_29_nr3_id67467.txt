Tulli kohto ette kirrikomoisa rendnik Kusta Herman ja kaibab et moisa poisi Hans Leppiko holetusse läbbi temma hobbune mis temmale 102 Rub maksab teiset ärraraiutut on.
Kutsuti Hans Leppik kohto ette ja temma tunnistab, et temma sedda hobbust sinna on jälle pannud kus ta sedda on söötnud.
Siis kutsuti rendnikko pois Mihkel Wilup ette ja se ütleb et se hobbun teise kohhapäel kinniseotut olnud.
Et nüüd kohhus selgemad säedust ei sanud, sowis kohhus leppidust teha.
Siis lubbas Hans Leppik ning Mihkel Wilup kummagi rendnikkule Jürripawas 10. Rubla maksta, misga renning leppitust teggi.
Peakohtomees Jürri Hühnerson XXX
Kohtomees Jaan Kiis XXX
Kohtomees Märt Illus XXX
