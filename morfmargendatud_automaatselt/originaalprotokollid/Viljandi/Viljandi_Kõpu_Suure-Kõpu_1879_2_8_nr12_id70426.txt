Jüri Rütel tuli ette ja kaebas, et Joh. Riesenberg temalt ühe hoone oma kätte ülese ehitada tellinud, aga seda mitte täitnud kele läbi tema suurt kahjo saanud ja palub kaebaja, et kohus seda asja läbi kuulaks ja seletaks.
Johann Riesenberg sai ette kutsutud kes seda hoone ehitust enese kätte tellimist püüdis salgada, lepisid aga oma wahel nönda, et Joh. Riesenberg 5 Rbl. Jüri Rütlile kahjo tasumiseks kolme päewa aea sees ära maksab, ning on siis köik nende wahelised nöudmised seletud.
möisteud:
Seda nönda kui sündinud maha kirjotada.
Peakohtomees: J. Kusik (allkiri)
Kohtumees: Alex Rosenthal (allkiri)
Kohtomees: J. Tiemann XXX
Kirjotaja: Kohler (allkiri)
