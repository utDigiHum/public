Kai Sein astus koggokonna kohto ette ja kaebas, et Jürri Wain tõotanud tedda abbikasaks wõtta, ja sel wisil tedda ni paljo petnud, et temmal nüüd laps on, ja 2 aastad on nemmad kui abiello koos ellanud.- Jürri Wain ei wöinud sedda mitte walleks ajada, et temma langenud Kai Seiniga on koos olnud. Simanni Kaarle naene Ainna Truwelt tõendab, et Jürri Wain ühhel laupäwa õhtul Simanni Reino maiasse ollid läinud, ja ka puddeliga wina senna winud, selle tõotussega, et pea Kai Seiniga luggema lähhäb, ja pallunud, et Kai mitte weel tanno alla ei panda.-
Mõistetud: et Jürri Wain Kai Malale, selle temma Kaie Seini põetamisse ja temma lapse 3me näddali toitmisse eest 2 rubl.73 Cop. maksab.- Ülle selle peab Jürri Wain igga aasta Kai Seinile 2 wakka rukkid ja 2 wakka odre kunni 10ne aastani omma lapse toitmisseks andma.- Nüüd essite maksab 2 rubla pima ostmisse tarwis, sest lapse emmal ei olle rinnas immemest-
Peakohtomees Jaak Kukmann. 
Kohtomees Mihkel Oll. XXX
Kohtomees Hans Riisk XXX
Kohtomees Hans Sild XXX
Kohtomees Rain Truwelt XXX
Wallawöörmünder Jürri Kalm XXX
Wallawöörmünder Rein Kopp XXX
