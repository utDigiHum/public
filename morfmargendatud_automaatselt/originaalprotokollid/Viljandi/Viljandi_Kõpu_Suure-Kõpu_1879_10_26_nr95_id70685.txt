Hans Leppik tulli ette ja kaebas, et Jüri Martinson kele juures tema enem sulaseks olnud, temast wälja külwatud 1 1/2 waka rukid pöllo pealt ära leikanud ja ärapeksnud, kelest arwata 7 1/2 waka rukid saanud aga seda wilja, temale mitte kätte andnud, ja palub kaebaja, et kohus Jüri Martinsoni sunniks temale seda 7 1/2 waka rukid wäljamaksma.
Jüri Martisnon sai ette kutsutud kes seda ei salganud, ütles aga, et sele leikusest mis köiges 2 1/2 koormad olnud 5 1/2 waka rukid saanud, kus ka 1 wak leikuse töö eest wälja andnud ning seda 4 1/2 waka mitte Hans Leppikule wälja anda wöida sest et see temal wasto panelik ja mitte igal korral tööle tulnud, nönda sama olla täna ka ühe heina kuhja Hans Leppiko käes wastopanemise eest tahtnud ära wötta aga Hans Leppik tema keelo wasto seda ära pruukinud.
Sele peale ütles Hans Leppik, et neil mitte kaubeltud olnud, kui paljo päiwi tema peremehele pidanud teenima, ning aga siis kui tarwis tööle pidanud tulema, ning tema iga kord kui peremees temale käsko andnud tööle läinud ja ei kelekil wiisil wastopanud.
Jüri Rautsik sai ette kutsutud kes ütles et tema sele asjast ühtegi teada ei ka Hans Leppikule heina keelust, muud kui näinud, et Hans Leppik oma heino koko widanud.
Andres Reinberg sai ette kutsutud kes ütles, et tema muud ei teada kui kuulnud kus Jüri Martinson Hans Leppikule ütelnud kui see tööle ei taha tulla, siis oma heinde ja rukidest ilma jäeb.
Madis Piir sai ette kutsutud kes ütles, et tema muud ei teada kui Jüri Martinson Hans Leppikule heino ärakeelnud.
möistetud:
Et Jüri Martinson mitte selgese ei wöi tehja, et Hans Leppik temale wastopanelik, peab tema siis seda 5 1/2 waka rukid, kelest 1 wak leikuse töö eest maha arwata tuleb, Hans Leppikule wäljamaksma.
Kohtomees: Alex Rosenthal (allkiri)
Kohtomees: T. Riesenberg XXX
Kohtomees. J. Tiemann XXX
Kirjotaja: Köhler (allkiri)
