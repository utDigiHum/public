Hans Meijer (käemees Märt Martinson) tulli ette ja kaebas, et Hans Grimpe tema pearaha 7 Rbl. 52 cop. keda see enese peale äramaksta wötnud, mitte äramaksta ja palub kaebaja, et kohus Hans Grimped sunniks seda äramaksma.
Hans Grimpe sai ette kutsutud kes ütles, et tema Hans Meijeri eest pearaha maksu mitte oma peale wötnud ning see seda isi maksma peab.
Willem Saar sai ette kutsutud, kes ütles küsimise peale, et Hans Grimpe oma kauba järele 15 R, 1/3 wakamaa lina, riided ja aastalik pearaha Hans Meijerile aasta palgaks lubanud kui ka loetama pidanud.
möistetud:
Tunistaja wäljaütlemise järele, peab Hans Grimpe oma poisi Hans Meijeri eest pearaha 7 R 52 cop. 8 päewa aea sees wäljamaksma.
