Ette tuli Peter Kansi ja kaebas et Johann Wiidika sead tema 1/2 wakkamaad rükkid suwel ära söönud ja tallanud on. Kohtumees Johann Andresson olla selle kahju üle waatand. Kaebaja nõuab nüüd kaks setwerti rükkid kahju tasumiseks.
Johann Wiidik wastab et mitte tema sellest kahju tegemisest süüdlane olewat waid tema seapois Jüri Pattane kes aga alles 8 aastat wana on.
Kohtumees Johann Andresson ütleb ennast selle kahju üle waatand olema ja takseerib selle ühe setwerti rükkid wäärt olema.
Mõisteti: Et Johann Wiidika sead selle kahju teinud on sellepärast peab tema ka Peter Kansile ühe setwerti rükkid kahju tasumiseks maksma.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja need Appellatsioni Formaliadega tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumehed JAndresson [allkiri]
                        JJõgewest [allkiri]
Kirjut Raudsepp [allkiri]
