Jaak Lorius olli aknast sisse moisa õlle-keldriskäinud Teisipäwa ösel, olli moisawallitsusse kaebtuse pealekohto ette tarwitud, ei selganud ka mitte, nimmetas etõlle-pruwel sedda keik isse pealt näinud.
Jaan Kass, õlle-pruwel selletas, et temma mitte seddapolle näinud kui warras sisse läinud, agga tedda kül keldrisees leidnud ja wälja kutsunud. Ütles ka, et temma keigeinnimestele sedda asja räkinud.
Opmann Kirschberg nõudis moisawallitsusse nimmelet süidlased saaks trahwitud.
moisteti:Jaak Lorius saab karristusseks 30 hopi witsoja Jaan Kass 20 hopi.(Jaan Kass lubbas peksu eest rahha maksta.)
