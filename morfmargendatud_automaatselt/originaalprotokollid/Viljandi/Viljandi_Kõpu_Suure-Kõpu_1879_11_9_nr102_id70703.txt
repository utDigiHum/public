An Almann (käemees Willem Saar) tulli ette ja kaebas, et Hans Gustawson ja Tönnis Lacks teda sagedaste peksta ning nimelt Hans Gustawson teda nüid peksnud ja kaelast pitsitanud kui ka enem seda kaks korda peksnud, nöndasama ka Tönnis Lacks teda suwel 25 korda rusikaga päha löönud, ning tema (Almann) nende peksmiste peale pödelik ja haige olla, ning palub kaebaja, et kohus seda asja läbi kuulaks ja peksjaid trahwiks.
Hans Gustawson sai ette kutsutud kes ütles, et An Almann teda wärdjaks ja hoorapoeaks söimanud kus tema siis sele rinust kinni wötnud ja sele söimamise üle järele küsinud aga mitte peksnud.
Tönnis Lacks sai ette kutsutud kes ütles küsimise peale, et tema möne kora An Almanni löönud aga mitte nende kui ette antud, sest et An Almann teda alati naeste hoorajaks muu sele sarnasteks söimada, ning ei ial rahul seista.
Ano Riesenberg sai ette kutsutud kes ütles, et tema näinud, kui Hans Gustawson An Almanni rinust kinni wötnud sest et An seda ropust söimanud, peksmisest ei teada tema.
Peakohtumees: J. Kusik (allkiri)
Kohtumees: Alex. Rosenthal (allkiri)
Kohtumees: T. Riesenberg XXX
Kirjutaja: Köhler (allkiri)
möistetud:
Et kaebaja kui ka kaebatawad köik üks teisega tüllitsenud ja söimanud neid siis köiki süialuseks arwata, ja iga ühte 2 R waeste laekase trahwida.
