27mal Septembril 1863.
Kohto jures ollid: Peakohtomehhe eest:
kohtomees Hans Siww
Kohtomehhe eest abbi Juhhan Alle 
Jürri Kaerd
Kohto ette astus Enno perrenaine Ann Noor ja kaiwas et temma mees, perremees Jürri Noor temmaga wägga sandiste ümber käib, tedda ilmaaego söimab, ja need wöerad lapsed mitte korrapärrast ei pea, egga neile palka ei anna nemmad leppisid, ja Jürri Noor lubbas laste eest hoolt kanda ja kortta pärrast riided tehha.
