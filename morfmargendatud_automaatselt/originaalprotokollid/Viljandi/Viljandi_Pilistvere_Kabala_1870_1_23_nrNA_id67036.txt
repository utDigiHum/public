Mihkel Must pallus kohto protokoliramato kirjutada sedda rahha wõlga mis temmal omma enna Peeti käest sada jai siis kui temma nekrudiks ärra anti.-
Mihkel Must tunnista Peeti kaest sada ollewad
1. wannemade pärrantussest 23 R. 33Kop
2 Palkarahha                             15 R.   -
Summa                                       48R. 33 Kop ja kui Peet Must sest eespool nimmetud rahhast peaks wenna Mihklelle wälja maksma siis peab temma igga kord sedda rahha wälja maksmist siin Koggokonna-kohto jures ühhe kohtomehhe läbbi protokoli ramato laskma kirjutadada - jättab Peet Must sedda teggematta siis ei sa se rahha wäljamaksminne mette tõeks arwatud.-
Peakohtomees
Lisalehel:
Tähhentus
Kui paljo Peet Must omma wenna Mihklele rahha on jätnud sest rahhast mis Peeti kätte jäi 48 R. 33 Kop.-
sel 24mal Octbr 1870 Mihklele postiga sadetud - 7 R.
sel 17  Dbr 1871             d                d.            d.          4 R.
sel 11 Dbr  1872             d                d.            d.          4 R.
sel 27  April 1873          d                d              d.         8 R.
