Kirriko- ja koggokonna wöörmünder Jürri Larentz kaebas, et Karl Tähnas tedda Eriko Trahteris - kus jalgo puhkanud - karwo piddi kiskunud. Kuigo Juhhan Watter ja Jaan Reimann, mõllembad kohtomehhed, ollewa tunnistajad.
Karl Tähnas selletas, et Jürri Larentz tedda seal ollewa wihhale ärratanud, lubbanud temma allasi ärra wia, polle temma seppise palka ärra maksnud ja mönda muud. - Ka ollewa Jürri tedda lasknud palja ihho peale ühhe rihmaga, kus pannal otsas olnud.Josnikko lüa.
Josnik selletas, et Karl Tähnas enne kohtomeest Jürri karwopiddi kiskunud ja peale se kui meleto ennast paljaks pildunud, keik riided teisele wasto silmi. Se peale ollewa kohtomees Juhhan Watter oelnud, wõtta rihm ja anna se kolmik wallo,- ollewa nelli korda palja ihho peale lönud.
Juhhan Wetter ja Jaan Reimann räkisid nenda sammoti, - Karl on kui meleto mõllanud, on Jürri karwast kinni wõtnud ja wiimaks riided wasto silmi pildunud nenda kaua kunni isse allasti jäänu.- Juhhan Watter on sanud peale se temma käest üks kord lüa ja Josnik Juhhan 3 korda.
Karl Tähnasse tunnistus selletas:Hans Moritz selletas, et Josnik Kaarlid 9 korda lönud.
 
1, - Karl Tähnas saab karristusseks 30 hopi witso ja maksab Jürri Larentsile 6 Ru hõbb. wallo ja teotuse hinda.2, - Kuigo Juhhan Wattred on üks kord lönud ja Josnikko kolm korda, agga need on tedda jälle lönud ja sedda rummalaste teinud, - jääb wastastikko.
