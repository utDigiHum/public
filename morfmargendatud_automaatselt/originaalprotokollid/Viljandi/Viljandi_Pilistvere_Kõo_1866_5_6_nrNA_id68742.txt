Leis Nurk naene Marri olli pühhapääw pärrast lõunat, kui mõisa karri koddo tulnud, omma lehma pari teiste lehmadega mõisa karja seast koddo ärra aiades, mõisa õue alluse keige parrema pehme heinama peale, kus krawide läbbi wet peale saab lastud.- sööma aianud ja issi karjatseks jure jänud,- sedda nähhes olli emmand mõisa kubjale käsko annud, need lehmad sealt wälja aiada.-
Kubjas olli need lehmad Marri käest kül kord wäggise wälja aianud, agga Marri olli kubjast-emmandad teotades, söimades ja neid kiwwiga wiskades, mis kül mitte nende jure egga külgi põlle tulnud, need lehmad ommeti taggasi aianud.-
Et Marri wasta aiab, et temma koerasid, mis kubjas temma lehma peale assetanud, kiwwiga wiskanud, agga mitte innimessi, - siis mõistis kohhus Marrid 3 rubl. hõbb. walla waeste laeka heaks trahwitada,- et temma kubjast ja emmandad sõimanud ja teotanud ja lehmad pärrast wälja aiamist ja keelmest teist korda heinama peale taggasi aianud.- Et temmal, kuida ütleb praego rahha ei olle, maksab tullewa kord s.a. hiljemalt 3 näddali pärrast siinsammas koggokonnakohtus sisse.-
Peakohtomehhe abbi: Karel Riisk
Kohtomees: Jürri Olbrey XXX
        d.           Karel Malkin XXX
Wallawöörmünder: Jürri Kalm.-
