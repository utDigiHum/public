Et kuida teada, on ello üllesse piddamissega walla wabbadmeestel ja waeste innimestel tännawo aasta hopis waene luggo, selle pärrast ollid tännasel päwal walla ammeti mehhed kokko tulnud et arro piddada, kuida nenda sugguste häddaliste innimestele ello üllesse piddamisse polest abbi tehha.
Siin said siis üllesse arwatud, et liggi 20 innimest on, kes walla poolt üllesse piddamist on sanud ja ikka sada tahtwad ja et neile walla maggasini aidast nenda paljo kui tarwis ello üllesse piddamisseks, ei wõi antud sada, sest et perremehhed sedda eddespiddi ei jõuaks sisse tassuda, siis tullid keik wimati selle peale, essiteks neile walla waestele, kes walla poolt ello üllesse piddamist peawad sama, walla waeste laekast 60 Rubla hõbbedad nende ello üllesse piddamisseks wõtta ja selle tarwis lubba keiserlikko V Perno Kihhelkonnakohto käest palluda.
Selle kinnitusseks pannewad omma nimmed seie alla.
Wallawannem Rein Kopp
temma abbi: Jürri Kalm
Peakohtomees: Jaak Kukmann
Kohtomees Tawit Markus. XXX
Nõumehhed: Andres Konks. XXX
dito Hans Luik XXX
dito Mart Annow XXX
