Pendi Hans Laasmann, kes Mangul Riik lesk naene Mall wõttis ja senna kohha peale wanna mehhe assemelle perremehheks ja wõera laste toitjaks, kunni poeg Karel Riik omma surnud issa kohta wõib pärrida, kes praego alles 13 aastane, 9 aasta eest nende laste wöörmündride Hans Kalm ja Juhhan Soon wallitsemisse ja sowimisse järrele läinud ja selle aia sees sedda maia iggapiddi kossutanud, kaebas, et temma naene Mall tedda sõimama ja sealt ärra aiama.-
Sellepeale ütleb Mai, et temma põlle tedda mitte ärra aianud, waid temma tahhab, et Hans wägga hoidja ja temma tütrid mitte keiki asja ei lasse osta, perremehhest poisiks tehha ja omma ühte wäimeest Tõnnis Seil kellel naene ja üks laps, senna wõtta ja issi sedda maia piddada.
Laste wöörmündrid Hans Kalm Juhhan Soone ei lubba sedda mitte, et temma omma wäimehhe Tõnnis Seil senna wõttab, waid tahtwad, et nemmad wanna modi nenda kaua peawad illuste ellama jäma, kui se poeg kes sedda maia pärrib, sureks kaswab, ja sowiwad, et üks neist kolmest surest tüttardest, et koht weike ja neid nende illo tahtmissega ei jõua piddada, - wõera jure tenima lähhäb.
Mõistetud: et nemmad mõllemad senna wanna modi ühte ellama ja sedda maia wallitsema jäwad, kunni se poeg sureks kaswab, kes kohta pärrib.- Kui nemmad mitte ühhes illus seal ei ella, sawad mõllemad wälja aetud ja laste wöörmundridest uus wallitsetud senna wõetud.-
Sepeale leppisid mõllemad käeandmissega kohto ees illuste ärra ja lubbasid kristlikkul wisil ühhes armastusses ellada.
Peakohtomees: Jaak Kukmann
Kohtomees: Tawit Markus.XXX
        d.           Jürri Olbrey. XXX
        d.           Karel Malkin. XXX
        d.           Hans Riisk. XXX
Wallakirjotaja: J. Otto
