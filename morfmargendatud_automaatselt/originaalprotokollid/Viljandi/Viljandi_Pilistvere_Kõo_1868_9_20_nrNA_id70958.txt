Kirriwerre küllast Mäelt Tallo № 102 perremees Jürri Kalm olli sealt omma külla perremeestega mahha teinud, et temma sealt omma poliko tallo Made pealt senna selle metsa külla saare peale nimmelt "Suuralle" ellama tahhab assutada ja omma poliko tallo Maad selle wasto küllale ärra wahhetab, mis wahhetamisse läbbi ni heaste küllale kui ka temmale lahhetam ello tulleb.
Et nüüd külla perremeestele selle Ma wahhetamisse läbbi keddagid kahjo ei tulle, selle pärrast on perremehhed sedda keik lubbanud, ning pannewad selle lubbamisse kinnitusseks perremehhedomma nimmed seie alla
Hans Rey XXX             Hans Essmann XXX              Laar Ewart XXX
Jaan Kask XXX            Mihkel Ewardt XXX               Hans Luik XXX
Jaak Kask XXX            Tõnno Södar XXX                  (Karel Rey XXX)
Juhhan Rey XXX        Mart Saar. XXX
                                    Hans Saar XXX
Pärrast sedda, kui Kirriwerre külla perremehhed sealt külla perremehhele Mäelt Jürri Kalm sedda Ma tükki on wahhetanud, saab sedda tarwilisseks tähhele pannemisseks Protokolli nöri ramatusse mahha kirjotud, et Jürri Kalm tullewa aasta jubba omma kessa põllo nendega wõib wahhetada ja omma rukkid senna sare peale mahha tehha.
Peale selle sai wallawallitsusse poolt mõistetud sedda mahha teggemist krono Tarto mõisade Wallitsussele kinnnitusseks sata.
Wallawannem: Rein Kopp
      dito          abbi: Jürri Sarits. XXX
