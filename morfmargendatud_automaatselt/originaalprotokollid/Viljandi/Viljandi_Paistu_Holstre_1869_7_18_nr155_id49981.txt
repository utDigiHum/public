Ette tulli Holstre vallas Koigu talus ennast üles pidav Villandi kogukonna liige Hans Tammist ning kaebas Koigu talu peremehe Andres Koik peäle, et see tema küttise rukki mis tema mineva aasta kui Koigu talu pois nimetud talu maa peäle teinud ning mis Andres Koik temale palga abiks lubanus nüüd ära võtnud olla; peäle seda ei tahtvad Andres Koik ka 45 rubl h. palga raha mis ta mineva aasta oma käemehe Jaan Laane juures olemises temale palgaks lubanud nüüd mitte välja maksta.
Andres Koik ette kutsutud vastab selles puuduva küsimise peäle et Hans Tammiste perimine mitte õige ei olla, tema ei olevta sellele ei küttise rukkid ega ka 45 rubla raha lubanud.
Jaan Laane ette kutsutud ütleb küsimise peäle, et Andres Koik Hans Tammistele mineva aasta 45 rubla h. aasta palgaks lubanud ning tema kui Andres Koigu käemees olla seda lubamist omalt poolt kinnitanud.
Astusid ära.
Selle peale vaates, et Hans Tammist üks terve aasta Koigu talu teeninud on, kui ka Jaan Laane tunnistuse peäle ennast toetades, ühendsaid koos olevad kohtu liikmed ennast järel seisva Mõistusele:
Andres Koik peab Hans Tammistele 45 rubla palga raha varsi välja masma. Küttise rukkide perimisest ei või Hans Tammist ühtegi saada.
Kui seesinane eespool seisva mõistus kohtu käiatele kuulutud sai, ütles Andres Koik et ta sellega rahule jääda ei võivad ning Appellatsiooni tähte palus mis temale ka lubatud ja täna välja antud sai.
Kogukonna kohtu eesistja J. Hansen
Kõrvalistja J. Ainson
" J. Moks
" J. Tomson
" M. Tuk
