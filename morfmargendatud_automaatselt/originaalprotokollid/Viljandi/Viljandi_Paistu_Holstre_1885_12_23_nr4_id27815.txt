Ette tuli kaupmees Jaan Roosmann Aidust ja nõuab Paisto kiriku mõisa kärneri Hans Mõttuse käest poewõlga 20 Rubla 73 kp.
Hans Mõttus ettekutsutud ütleb et temal kül Jaan Roosmanni poest kaupa rehnungi peale toodud aga kui palju seda olewat seda tema peast ei teadwat.- Temal olewat rehnungiraamat aga ta ei ole seda praegu kätte leidnud ja palub asja teiseks korraks jätta, milleks te raamatu wälja otsima ja rehnungi ära selletama saab.
Mõisteti:  Kohtukäiad uuesti ette kutsuda.
