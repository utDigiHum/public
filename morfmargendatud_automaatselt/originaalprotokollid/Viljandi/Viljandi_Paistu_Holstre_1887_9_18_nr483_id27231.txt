Holstre wallawalitsuse kaebduses posti jaama wõla asjas, protokoll 27 Augustist 1887 No. 445, ei olnud Johann Parsmann ega Ado Järw ettetulnud.
Johann Moks andis tänasel päewal siin kohtu ees tunnistust: et Johann Parsmann Keiserliku VI Pernomaa kihelkonnakohtu ees seda ise tõeks tunnistanud on et tema, Johann Parsmann, siis kolme talu jaama moona, nimelt kahe Kowali ja Mammo talude moona jaama wiimata jätnud, lubanud aga seda moona pärast seda ära wiia. Kas Joh Parsmann selle moona ära wiinud wõi wiimata jätnud seda ei tea tunnistaja üttelda ja pidawat seda jaama kuitas wälja näitama.
Mõisteti: Tunnistaja üttelust protokolli wõtta.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JJaama [allkiri]
Kirjut Raudsepp [allkiri]
