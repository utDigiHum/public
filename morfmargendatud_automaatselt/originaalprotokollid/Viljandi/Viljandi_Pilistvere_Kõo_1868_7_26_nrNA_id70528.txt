Kõomõisa tallopoiad Mats Nahkur ja Tõnno Loik kaebasid, et Arrosare karja mees Jaan Põrk essimissel rukkid ja teisel kaero omma karja ellajattega ärra söötnud. Jaan Põrk aiab wasto, et temmal agga üks ainus kord 2 lehma sisse jooksnud, agga mitte ei olle sötnud.
Tüdruk Liso Wolmer tunnistas, et temma näinud, kui Jaan Põrk tänna kahhe näddali eest omma ellajad wiljast wälja aianud, mis Jaan Põrk mitte ei wõinud wasto aiada. Kõowalla wannema abbi Jürri Kalm ja Arrosare Wallawannema eest Tõnnis Rebbane käinud sedda kahjo ülle watamas ja arwawad sedda kahjo suur ollema 5 küllimitto rukkid ja 4 kullimitto kaerd.
Et Jaan Põrka ellajad, et kül sönud on, põlle mitte kinni aetud, sai mõistetud, et Jaan Põrk maksab pole kahjo se on 3 küllimitto rukkid ja 2 kullimitto kaero tänna 14 päwa sees wälja,
Peakohtomees Jaak Kukmann
Kohtomees: Andrej Krasowskj
dito Tawit Markus. XXX
dito Jürri Kukk. XXX
Sel 21 Märtsil. Marie Kerup tunnistab, et Jaan Põrk sel päwal mitte ei olle ellajatega seal pool küllas käinud, mis peale arwatud sai, et Jaan Põrk ei prugi maksa nenda kuida enne mõistetud on.
Peakohtomees - Jaak Kukmann
Kohtomees: Andrej Krasowskj
dito Tawit Markus
              XXX
