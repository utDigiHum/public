Marri Rätsepp käesmees Jakob Rätsepp nõudis Marri Jürrisoni käest 50 rubla ja selle intres ja 1 Setwerik rukkit mis selle mees on wõtnud. 
Marri Jürrison käemeestega Peet Jürrison ja Jaan Rümmel ütles et 50 rubla wõlga on ja kui too ka seda intressi nõuab siis wõib se intres ka makstud saada. Tema mees on aga 6 pääwa nendega Tartus käinud ja nõuab 6 rubla. Wiimaks sai nenda maha pantud: et Tõnnis Leppik käes on 32 rubla ja isa Tonnis Leppik käes on 16 rubla nenda peab Marri Jürrison 2 rubla juure maksma ja saab need 50. rubla ära makstud.
Keik need muud asjad jääwad neil keik wastastikko kuidas üks teisele on antud sest Tartus käimise pääwadega saawad rukkit parajaste tasu.
Peakohtumees Hans Ant [allkiri]
Kohtumees M. Länesar [allkiri]
-"- J Rätsep [allkiri]
