Kaebas Hindrik Heinthal, Kõo-wallast, et Temma Süggise Mardi aeg Tõnno Trulli käest Odre kaubelnud sadda wakka, 5 Ru hõbb. hind Tschetwerti pealt, ja 25 Ru hõbb. käerahha andnud.- Peale Jõulo ollewa Tõnno 5 Tsetwerti ärra tonud ja üttelnud, et rohkem ei andma. Pallusiwad et kohhus sunniks Kaupa kaubaks saama.
Tõnno Trulli selletas, et temma ehk olleks sedda wilja andnud, agga temma Issa Jaan ollewa ommale kohha ostnud ja prukima isse sedda wilja seemneks. - Ka ollewa ostja tedda wastowõtmissega pärrinud, - essiteks wiljawoori õuest wälja ajanud ja wiimaks suurema wakkaga kui õigus wasto wõtnud.
Jaan Trulli selletas, et temma sedda wilja mis nüüd ommal tarwis, ei olle lasknud anda.
Kaebaja pärris 25 Rubla hõbb. taandust.
moisteti:Tõnno Tull tassub puduwa-wilja wahhe kasso, 75 kop. Tst. pealt nenda kuidas käerahha arrul nõutud, 21 Ru 25 kop. hõbb. - Se wie Tsetwerti pealt mis kätte widud arwati 375 kp. hõbb. mahha.(näddal aega tassuda.)
