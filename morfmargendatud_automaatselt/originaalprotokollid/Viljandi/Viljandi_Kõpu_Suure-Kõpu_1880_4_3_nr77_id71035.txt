Hans Liwakiwi tulli ette ja kaebas, et tema endine peremees Willem Koff tema raha palgast mis 32 R suur 3 R kinni wötnud, ilma, et tema mingisugust süidi teinud, ning palub kaebaja et kohus Koffi sunniks seda temale wäljamaksma.
Willem Koff sai ette kutsutud, kes ütles, et Hans Liwakiwil mitte 32 R waid 30 R palka tellitud olnud ning 1 R järel nimetud asjade eest kinni wötnud, - kui 1 heinamärs = 50 kop. ja 1 hobosedek = 50 kop. = 1 R, mis Hans Liwakiwi ära kaotanud, kus temal sel järgil mingisugust Liwakiwile maksta ei ole.
Sele peale ütles kohtumees Peet Tamm et tema peale 2 aasta eest, kui see tellimine Willem Koffi ja Hans Liwakiwi wahel olnud, ka seal kauba juures olnud ning selgeste mäletada, et Hans Liwakiwile 32 R raha palka lubatud saanud.
möistetud:
Kohtumehe P. Tamme wäljaütlemise järele peab W. Koff seda waieldawad 2 R H. Liwakiwile 14 päewa aea sees wäljamaksma.
