Kohaline metsa walitseja Schröder, tulli ette ja kaebas, et Jaan Kollist möisa metsast 2 kaske, mis á 14 tolli alt jämetad ja 60 jalga pitkad, taksi järele 11 Rbl. 68 Cop. ning kolme wöra 35 Rbl. 4 Cop. wäärt, raiunud ja ärawiinud ning palub kaebaja, et kohus seda asja läbi kuulaks ka süialust seda kahjo äratasuma sunniks.
Jaan Kollist sai ette kutsutud kes ütles, et tema sele wargusest mingisugust ei teada.
Metsawaht Peet Tisler sai ette kutsutud kes ütles, et tema seda metsa raiumist mitte isi näinud, aga Jaan Kollist metsawahe Jüri Ohlepile isi rääkinud et neid puid raiunud on.
Metsawaht Jüri Ohlep sai ette kutsutud kes ütles küsimise peale, et Jaan Kollist isi temale könelenud, et neid puid raiunud on; - nöndasama olla Fritz Riesenberg ka seda tema (Ohlepile) rääkinud seda Jaan Kollistast puuraiumist näinud olewad.
möistetud:
Seda nönda kui sündinud maha kirjotada ja Fritz Riesenbergi kohtu ette tellida.
Peakohtomees: J. Kusik (allkiri)
Kohtomees: M. Pern XXX
Kohtomees: J. Michelson XXX
Kirjotaja: Köhler (allkiri)
