Kohaline metsa walitseja, herra Schröder tulli ette ja kaebas, et Andres Raamann möisa metsast ühe kase, mis 14 tolli jäme ja 70 jalga pitk raiunud kus see puu sele käest ärawöetud saanud ja palub kaebaja, et kohus Andres Raamanni sunniks sele puu eest taksi järele 6 R 32 kop. kahe wöra = 12 R 64 kop. wäljamaksma.
Andres Raamann sai ette kutsutud kes seda puu raiumist ei wöinud salgada ütles aga, et sele puul ladv tuulest maha murtud olnud ning tema seda siis mitte tooreks ega kaliks puuks arwanud ja ära raiunud.
Möistetud:
Et Andres Raamann oma wöimusega seda puud raiunud, teda seles asjas süialuseks arwata ja peab seda taksi järele hinda 12 R 64 kop. kahe kuu aea sees metsa walitsusele wäljamaksma.
