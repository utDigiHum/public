Tönis Peterson tulli ette ja kaebas, et Hans Glück temale heinamaa söötmise eest 5 Rbl. wölgo olla keda mitte äramaksta, kele wiiwituse läbi temale aea wiido ja kohtukäimise kulusid sunnitud ja palub kaebaja et kohus Glücki sunniks temale neid kulusid kahju tasumiseks wäljamaksma.
Hans Glück sai ette kutsutud kes seda ei wöinud salgada.
Kohtumees Andres Holzmann andis seletuseks, et tema seda Hans Glückist Tönis Petersoni heinamaa soomist üle waatnud kus juures need isieneste wahel lepinud et Glück ilma kohtu ja muu wiiwituseta, Petersonile 5 R kahjutasumiseks maksab.
Möistetud:
Hans Glück peab seda heinamaa söötmise kulu 5 R kui ka sele maksu wiiwituse läbi sündinud aea kulu 1 R = 6 R 14 päewa aea sees Tönis Petersonile wäljamaksma.
Kohtumees: K. Aunap (allkiri)
Kohtumees: J. Org XXX
Kohtumees: P. Bergmann (allkiri)
Kirjutaja: Köhler (allkiri)
