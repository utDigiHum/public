Karl Elbrecht tulli ette ja kaebas, et endine tema tüdruk Leno Adamson, tema teenistuse aeal 8 päewa enesele kangast kudunud kele eest 5 Rbl. kahjutasumiseks nöuab.
Leeno Adamson (kaemees Willem Eckbaum) sai ette kutsutud kes seda tööd ei salganud ütles aga et see temal teenistuse kauplemise aeal tellitud saanud. Karl Elbrecht ütles seda mitte tellitud olewad.
Möistetud:
Need 8 töö päewa á 25 Cop. 2 R see läbi et kumbagil pool tunnistust ei ole pooleks arwata ning Leeno Adamson peab 1 R Karl Elbrechtile 8 päewa aea sees wäljamaksma.
