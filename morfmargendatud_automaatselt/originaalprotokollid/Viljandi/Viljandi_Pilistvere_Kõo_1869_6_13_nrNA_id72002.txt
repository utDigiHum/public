Nachdem von Seiten der publ. Wolmarshofschen Gemeindes verwaltung unterm 4 April 1869 mehrere  verabschiedeten Untermilitairs  zur Ansiedelung Eines Kaiserl. Dörptschen Bezirks verwaltung vorgesteltt  werden; die Bezirks verwaltung dieser gemeinde verwaltung unterm 26: April subst. 1147 die Eräffnung gemacht, daß die Vorstellung der eine Ansiedlung  wunschender Untermilitaris  bis june 20 habst zu  mechen ist, so würde und von Seiten der gemeinde verwaltung in der Gemainde die Mittheilung gemeist, daß die jemgen Untermilitairs welche aus Land anzusiedelt zu werden wünschen sich zum Anschreiben im Wolmarshoffcsen Gemeindehause einzusender haben.
Denmach waren erschieman.
verabschietete Untermilitairs.
Karel Noor
Maddis Mäggi
Jaak Kummer
Hindrek Ewardt. Unteroffizier
Hans Seask.  dito
Hans Ewardt.
Jaan Sein.
Karel Rakel
Jaan Liwer
Untermilitairs auf Billet unterssen die eries 20 jahrs grinst hette.
Jürri Riik.
Tõnno Raam
Josep Rakel
Rein Sild.
Hans Pokk.
Hans Södar. Unteroffizier
Jaan Mäggi.
Jürri Wink.
Juhhan Pillmann.
Hans Olbrey
Jaak Ilwes. Unteroffizier
Josep Södar.
Tõnnis Truwelt
Rein Rink.
Willem Meiessaar.
Jaan Sein.
Jaak Kabritz
Tõnnis Allik
Jaan Wachter
Ado Kallmann.
Jaan Kallo
Jürri Kask
Tõnnis Kannep.
Jaan Seask
Alle verbennenten Untermilitairs Mit aus erhare van Hindrek Ewaldt: Hans Seask welche zub detestztette bitten unterthängst, Eine Kaiserl. Dörptsche Bezirks verwaltung wolle gefülligst dahin wirken, daß ihnen von dem Walmarshoffchen Hofeslande Soldaten Landausindelungen, zum Bun der Wohnhäuser die erferd erl Landstellen su usen Balken und Latten und dem  Krauswaldes moylchse abd dem Wolmarshoffchen Liwwassaarschen Walde baldmaglichst ungernisen werden, da sie den der Baurbnitung der Landstellen suteglihes NetrKaneman erhulten. Zugleich lütte sünantliche Untermilitairs, daß ihannauch zugleich die einmalige Unterstützung pr. Menn 40 Rubl. v für jeder unteroffizire 'a 50 Rubl Sr. verabsolyt werde.
Verfügt. Einer Kaiserl. Dorptschen Bezirkverwaltung hieron eine Cosiec zu unterlagen, v Eine Kaiserl. Dorpts verwaltung, wie hierdurf geschinst, geharsenst zu  bütten dast allen verbenennten Untermilitairs dem siebl. Wolmarshoffes en Hofeslande die erfardenluhen Militariansiedelungs Landstellen, sowie die zum Aufbar des Wohin b häuses ersonderlihes Balken ds Latten moglihst  anddem Wolmarshoffchen LIwwassaarschen Walde anweiser, seine denselben die einmalige Unterstützung zelder aerabselgen lassen zu wollen, demit sie sich aufs Land ansendeln Künnen und ihr tägliches Brod ded  aus ersolten.
Subst: Gemeindegerihtes Vorsitzer: Tõnnis Uusna
                              dito        Beisitzer: Andrej Krasowskj
Subst: Gemeinde Aeltzter: Jürri Kalm
            Gemeinde Vorsteeher: Jürri Sarits
