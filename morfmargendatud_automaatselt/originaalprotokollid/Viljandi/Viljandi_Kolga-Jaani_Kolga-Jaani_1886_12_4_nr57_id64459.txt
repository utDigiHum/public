Jaan Rätsepp kaebab Tonnis Kass on tema käest 15 Rubla 6 ehk 7 aasta eest laenanud, siis teinud Kass temale ühe laua 3 Rubla eest, ja on temal weel saada 12 Rubla. Tõnnis Kass ütles tema on 13 rubla laenanud, sepeale teinud ühe laua 4. Rubla eest ja on temal weel maksa 9. Rubla aga mitte rohkem. 
Hans Wint tunnistas, tema ei ole küll mitte laenamise juures olnud, aga Jaan Rätsepp ja Tonnis Kass on mõlemad temale rääkinud et 15 Rubla seda wõlga on, siis on mõlemad ka jälle temale rääkinud, et se Tonnis Kass tehtud laud 3 Rubla maksab, mis sest wõla rehnungist maha on arwatud. 
Moisteti:
Tunnistuse järele on Tonnis Kass wõlga 12 Rubla ja peab oma wõlg ära maksma. Moistus sai edasi kaebamise tärminidega kohe kuulutud.
Peakohtumees Hans Ant [allkiri]
Kohtumees M. Länesaar [allkiri]
-"- Jüri Kass [allkiri]
