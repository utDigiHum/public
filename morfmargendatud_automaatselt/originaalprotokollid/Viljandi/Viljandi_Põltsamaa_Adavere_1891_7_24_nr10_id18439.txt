Tänasel päewal andis Ruttikwere mõisaomanike wolinik J. Grossthal andis Addawere-Ruttikwere ühentud kohtu eesistnikule üles, et Juhan Aunbaum Ruttikwere mõisamaa Perteloja koha päält Jürri päewal 23 Aprilil 1892 a ära peab minema.
J. Grossthal /allkiri/
Ruttikwere mõisamaa Pirtteoja Juhan Aunbaum rentnikule sai se ülesütlemine kulutud.
Juhan Aunpaum /allkiri/
Lahtütleja ja lahtiüteldud on mõlemad kohtule tuntud.
Eesistnik H. Kors /allkiri/
kirjutaja eest
