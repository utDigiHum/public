Maret Henno kaebduses Märt Henno wastu, nõudmise pärast, oliwad tänasel päewal ette tulnud: Kaebaja Maret Henno ühes nõumehega Peter Henno, ja kaebataw Märt Henno.
Kaebduse protokoll 23 Juulist s a N. 398 sai loetud.
Märt Henno wastas sellesinatse tema wastu tõstetud nõudmise kaebduse peale: et tema ommetige ei ole wõinud Maret Hennot kesa põldu pruukida laskta. Nendasama ei ole ta ka mitte wõinud oma talu põllu põhust saadud sõnnikut talust ära wiia laskta, sest et tema seda näinud ei olewat ega sellest ei teadwat et Maret Henno mujalt põhku tema taluse sisse toonud ehk ostnud oleks.
Kui nüüd kaebaja tõeks teha ei jõua et ta mujalt põhku ostnud ja Ritsukse Märt Henno talus elajatele ära söötnud oleks, Märt Henno aga seda kesa põldu mis ta Maret Henno käest ära wõtnud pruukinud ja nimelt senna kartuhwled peale teinud on sellepärast sai
Mõistetud: Et Maret Henno sõnniku eest midagi tasumist ei wõi saada waid peab see senna Ritsukse taluse jääma.
Ühe wakkamaa põllu rendi peab aga Märt Henno 5 rubla kaebajale Maret Hennole maksma, mis ta wõimse käest ära on wõtnud.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja needsamad Appellatsioni formaliadega tutwustatud.
Peakohtumees JLuik
Kohtumehed JAndresson
                        MHansen
Kirjut Raudsepp
