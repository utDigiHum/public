Peter Sepp Wiljandi mõisast wõlgneb Holstre magasi raamatu järele 31 karnitsat rükkid.
Kui tema täna ette kutsutud oli saanud, ütleb ta, et Jaan Sepp, kes nüid surnud, selle wõla tem eest ära maksnud on.
Et Peter Sepp oma ütelust, et Jaan Sepp ära oleks maksnud, tõeks teha ei jõua, sellepärast sai magasi raamatu põhjusel Mõistetud: Et Peter Sepp peab 14 päewa aea sees 31 karnitsat rükkid Holstre magasisse ära maksma.
Eesseiswa mõistus sai kohtu alusele kuulutatud.
Peakohtumees JLuik [allkiri]
Kohtumees MHansen [allkiri]
                     JAndresson [allkiri]
