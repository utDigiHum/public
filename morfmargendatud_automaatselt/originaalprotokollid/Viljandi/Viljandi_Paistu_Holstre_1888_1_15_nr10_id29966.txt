Ette tuli tüdruk Els Kree ühes nõumehega Johann Markus ja nõuab Holstre Metsa talu peremehe Ado Hendriksoni käest 70 rubla raha. Sellest summast olewat 50 rubla tema surnud wenna Peter Kree raha mis juba 6 aasta eest tagasi kaebatawa kätte jäänud ja 20 rubla kaebaja oma raha. Selle raha eest olla ta Ado Hendriksoni käest ühe korra 10 rubla intressideks suaand.
Et Ado Hendrikson ettekutsustud wastab, et tema Els Kree käest ei wenna raha 50 rubla ega kaebaja enda raha 20 rubla saanud ei ole ega sellele mitte kopikatki  ei wõlgnewat.
Kui nüüd Els Kree oma nõudmist milgi wiisil tõeks teha ei jõua sellepärast sai
Mõistetud: Et Els Kree sellest omas nõudmisest ühtegi saada ei wõi.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja need Appellatsiooni Formaliafega tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumees JAndresson [allkiri]
                     MHansen [allkiri]
Kirjut Raudsepp [allkiri]
