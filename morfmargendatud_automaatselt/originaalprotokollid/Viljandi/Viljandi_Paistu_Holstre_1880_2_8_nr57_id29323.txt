Hans Jaaksoni kaebduses Kadri Härg wasto teenistuse kontrahi murdmise pärast andis Hendrik Naber seda tunnistust, et ta sääl tellimise juures olnud ning kuulnud, et Hans Jaakson Kadri Härjale aastas 36 R. palka lubanud ja sellest 5 Rubla käerahaks annud, keda see ühe nädala sees tagasi ..... wõinud kui kaubast taganeda tahab.
Mõisteti: seda tunnistaja üttelust protokolli wõtta.
