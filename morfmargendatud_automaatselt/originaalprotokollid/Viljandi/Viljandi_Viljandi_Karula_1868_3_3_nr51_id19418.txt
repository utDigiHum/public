Karrola walla Koggokonna Kohhus, sel 13. März 1868
juures ollid: Pea Kohtumees Tõnnis Tõllasep
              "         "     Abbi         "          Tõnnis Taar
              "         "         "            "         Hans Köhnberg
Moisa wallitsus andis ülles et Uwwe Köppo mees Peter Jakobson ollewa tunnistanud, et Hans Laan temale üttelnud, et ta Mihkel Karros rahha  warrastanud
 ette kutsuti Peter Jakobson ja tunnistas, et Hans Laan temmale keldres rääknud kui nemmad sõbraks sanud, et temma se Mihkel Karros rahha pandnud 6 Rbl. laulo ramatu wahhele enne sedda otsimist ja muist sest rahhast sönniko sisse rihhe all pandnud.
   ette kutsuti Hans Laan ja ütles, et temma teiste küssimisse peale kül wangis rääknud, et temma wargusse perrast ollewa wangi pantud, ja enne tedda läbbi otsimist laulo ramatus 6 Rbl pandnud ja ka sedda üttelnud, et teisel saap kappis hoita ja minnol ei sa omma tarwis pruuki.
se peal sai moistetud
Sedda asja uhhe Keiserliko Silla Kohtu kätte anda, keik ülle kulata ja weel ussutad
  Peakohtumees Tõnnis Tõllasep  XXX.
