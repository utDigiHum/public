Kaebas Konni talu peremees Enn Konni Kõrvi peremehe Hans Leo peäle et seesama oma hobusid ja karja kumbagit 5 korda rukkist ja teine 5 korda heinamaast kinni võtnud olla, et nemad mineva sügise Hans Leoga seda kontrahti eneste vahel teinud olla et nii pea kui kumbagi elajad või hobused üle oma piiri teise talu maa peäle lähavad, nende loomade peremees sellele iga looma peält 1 rubl. maksma peab; sellepärast nõuab ta Hans Leo käest iga ühe looma peält kes tema viljas käinud 1 rubla tasumist.
Hans Leo ette kutsutud vastab selles puuduva küsimise peäle et see Enn Konni kaebdus õige ei olla; seda ütleb ta kül õige olevad, et nad mineva sügise oma loomade pantimise ja maksmise pärast niisuguse lepingu teinud on.
Tunnistus Jaan Ritson ette kutsutud ütleb ennast näinud olema kui Hans Leo 2 hobust Enn Konni rukkis on olnud.
Hans Ritson tunnistab üks teine kord näinud olema kui Hans Leo 2 hobust Enn Konni rukkis on olnud.
Johan Ritson tunnistab näinud olema kui Hans Leo kari heina maa sees käinud olla, mis Enn Konni päralt on olnud.
Lasti eest ära astuda.
Et Enn Konni kunagi kord seda kahju, mis Hans Leo temale teinud mitte üle vaadata lasknud ei ole selle pärast 
Mõisteti
Hans Leo peab Enn Konnile vie korra lomade kinni võtmise pantimise raha 5 rubla h. Enn Konnile maksma.
Kui see eesseisva mõistus kohtu käiatele kuulutud sai ütles Hans Leo et tal nüüd raha maksta ei olevad, mispärast ka siis temale maksu termin 2 nädali peäle a dato antud sai.
Kogukonna eesistja J Hansen
Kõrvalistja J Ainson
" J Moks
" J Tomson
" M Tuk
