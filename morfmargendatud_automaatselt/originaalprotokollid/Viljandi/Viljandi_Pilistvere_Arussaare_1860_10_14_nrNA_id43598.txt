Anne Jürrison tulli ette ja kaibas et Ans Rum temmal minnewa aasta tehnistuse palka ei maksa ja temma lambas mis Ans Rum on sõge peale wõtnud mitte taggasi ei anna.
Ans Rumme mis ette tulli sai küstut mis pärrast ta temmale Palka ei maksa, ütles et temma issi sest tallo Piddamisest ei olle keggadi sanud ja lambas temma ei anna taggasi sest et se lambas temmal talle ei olle tonud, ja willate ette üksi temma lambast ei woi ülle talwe ei woi toita.
Kohhus moistis et se Ans Rum peab Annel aasta palka ärramaksma se on
3 urgit ja üks must kuup.
Ja se lambas warste Anne Jürrisonil taggasi andma.
Pae kohtomees Jaak Tamme XXX
Kohtomees Ans Siww
Kohtomees Jürri Noor XXX
