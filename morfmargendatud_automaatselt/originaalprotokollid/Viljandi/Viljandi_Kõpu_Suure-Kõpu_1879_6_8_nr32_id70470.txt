Kohaline metsa walitseja herra Schröder tulli ette ja kaebas, et Iia Johann Pernitz oma heinama pealt ilma lubata puud raiunud, mis 30 Rbl. 24 Kop. wäärt ja palub kaebaja et kohus süialust sunniks pool osa sele trahwist möisale kahjotasumiseks maksma.
Johann Pernitz sai ette kutsutud kes seda ei wöinud salgada ütles aga, et temal heinama pealt luba olnud puid raiuda ja heinamaad puhastada; lepisid aga oma wahel senna, et Johann Pernitz 5 R.. möisawalitsuse kasuks maksab ja need raiutud puud siis enesele saab.
möistetud:
Seda nönda kui sündinud maha kirjutada.
