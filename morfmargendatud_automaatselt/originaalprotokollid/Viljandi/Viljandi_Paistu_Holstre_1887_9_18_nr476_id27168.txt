Ette tuli lesk Epp Paulson ühes nõumehega Johann Margus ja nõuab ühe kwitungi põhjusel mille surnud Fromhold Balzar allakirjutanud ja kelle nisuke allkiri kohtumehe Johann Andressoni poolest tõeks tunnistatud saanud, surnud mehe perandusest: 20 rubla raha, 5 wakka rükkid, 75 Lu õlgi. Tema nõuab ühes raha ja rükki ning õlgega kokku 42 R. 50kp. Balzari periatelt.
Fromhold Balzari periad: Dora Janusson ühes nõumehega Andres Janusson ja Johann ning Peter Balzar olid kohtu ees ja lubasid, et see raha 42 rubla 50 koppikat surnud F. Balzari warandusest wälja makstud wõiks saada.
Epp Paulson palus et kohus selleraha J. Raudseppa kätte wälja maksaks.
Mõisteti: Epp Paulsonile resp. selle wolipidajale Raudseppale, pärast F. Balzari kraami müümist seda raha wälja maksta.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JJaama [allkiri]
Kirjut Raudsepp [allkiri]
