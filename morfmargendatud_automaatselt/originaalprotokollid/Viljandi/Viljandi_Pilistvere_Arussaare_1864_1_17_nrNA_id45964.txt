Arrosaare koggokonna kohhus, sel 17mal Januaril 1864
Jures ollid: Pea kohtomees Jaan Noor
Kohtomees Hans Siww
Juhhan Alle
Kohto ette astus Köo walla kaupmees August Pertmann ja kaiwas, er se Arrosaare walla mees Hans Awik temmale 4 rubla 39 koppikud öbbedad wölgo on, mis ta minnewal aastal Peakohtomehhe Jaan Noor läbbi Hans Awiko käest on lasknud pärrida ja mis peale Hans Awik kohtomehhele on üttelnud, et temma se wölg tahhab warsi arramaksa, agga sedda tänapäwani mitte polle teinud. Nüüd pallus August Pertmann siin koggokonna kohhut Hans Awiko käest sedda wölga wälja pärrida, kus jures temma omma walla ramatu ette näitas, et temma töeste Hans Awiko käest 4 rubla 39 koppikud öbbedad pärrida on.
Hans Awik astus ette ja ütles, et temma se wölla tööga tassa on tenenud, ja mitte koppikud ennam wölga ei olle.
August Pertmann wastas se peale, et Hans Awik temmale kül tööd on teinud, agga enne sedda tööd jubba 1 rubla 81 koppiku eest kaua walja on wötnud ja ni pea, kui töö walmis sanud, on se, mis temma ülle 1 rubla 81 koppiko teninud temmale tassutud sanud, mis ka temma ramatust selgest nähha on, ning se 4 rubla 39 koppikud pärrast seddo tööd Hans Awikust wölga woetud on.
Moistetud: Et Hans Awik se 4 rubla 39 koppikud 17maks Küünla ku päwaks 1864 August Pertmannile peab ärra maksma.
moisa wallitsusse nimmel
Pea kohtomees Jaan Noor XXXkohtomees Hans Siww XXX
kohtomees Juhhan Alle XXX
Kui kohhus löppetud ja kohtomehhed wälja ollid läinud, astus Hans Awik minno tuppa, kus ka August Pertmann olli ja pallus andeks, et temma essite salganud, sest temmale polle mitte meele tulnud, temma arwab agga et tae August Pertmanniga nipaljo wölgo on ja pallub kannatada künni süggiseni 1864, kus ta siis maksa lubbab
moisa wallitsusse nimmel
