Kohaline metsa walitseja, herra Schröder tulli ette ja kaebas, et Matz Glück möisa herrale könelenud kus Jüri Piiskop metsawahti Willem Schülerid peksnud olla, mis aga mitte tösi, -ja palub kaebaja, et kohus tühja jutu tegiad trahwiks.
Matz Glück sai ette kutsutud kes seda ei salganud ning ütles, et Jüri Piiskop isi seda temale könelenud, kus metsawahti peksnud nönda et see mitto päewa haige olnud.
Jüri Piiskop sai ette kutsutud kes ütles, et tema sele ette antud jutust ega metsawahi peksmisest mingisugust ei tea.
Willem Schüler sai ette kutsutud kes ütles küsimise peale, et tema kelegist ei ole pekstud saanud, ega selest ühtegi ei teada.
Möistetud:
Matz Glück tühja jutu töstmise eest 15 witsa löögiga trahwida.
