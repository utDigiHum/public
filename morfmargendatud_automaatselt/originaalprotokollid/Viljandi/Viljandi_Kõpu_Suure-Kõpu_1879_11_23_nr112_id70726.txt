Kohaline metsa walitseja, herra Schröder tulli ette ja kaebas, et möisa metsast üks kuusk mis 15 tolli jäme ja 84 jalga pitk ning taksi järele 19 R 80 kop. wäärt, ärawarastud, kus taga otsides puu kooresid Peter Tieto maea juurest, puru unikust leitud, mis sele warastud puu kännuga kokosündinud ning seda wiisi nähja, et Peter Tiet seda puud warastanud ehk sele wargusest teab ja palub kaebaja et kohus Peter Tieto sunniks seda nimetud trahwi wäljamaksma.
Peter Tiet sai ette kutsutud kes ütles, et tema sele nimetud puu wargusest mingisugust ei tea, kül aga oma platsi pealt puid raiunud, kele koored tema maea juurest leitud.
Metsa waht Rolle andis ette, et tema nähes need puu koored, mis Peter Tieto maea juurest kohtumehe Alexander Rosenthali juures olemisega leitud, metsast ärawarastud puu kännuga koko sündinud, ning sedawiisi nähja et Peter Tiet seda puud on warastanud.
Kohtumees Alex. Rosenthal andis ette, et tema seal puu warguse tagaotsimise juures olnud, ning need leitud koored, mis jo osalt kuiwanud, kül mönest kohast sele ette näidatud känno otsa sündinud aga et need saega löigatud seda wiisi ka möne teise puuga koko sündinud ning see perast mitte selged aro saanud, et need, sele ette antud känno otsast löigatud koored on.
Kohtumehed: Jaan Kuusik ja Tönnis Riesenberg andsid ette, et nemad iljam seda ärawarastud puu kändo, kui ka Peter Tieto maea juurest leitud, kooresid ülewaatnud ja kokosünnitanud, kus üks koor, sest et saega löigatud sele ette näidatud kännu kui ka mitme teise kännuga koko sündinud, teine koor aga mitte sele ei ka teise känudega koko passinud ning seeperast, mitte aru wöinud saada, et need leitud koored, sele warastud puu kännust löigatud.
möistetud:
Et üle waatamise järele mitte selged on aru saanud et need Peter Tieto maea juurest leitud koored ärawarastud puu kännuga koko sündiwad, ei ka kaebajal mingisugust muud tunistust seles asjas ei ole, - siis kaebajad oma kaebtusega rahule sundida.
Peakohtumees: J. Kusik (allkiri)
Kohtumees: Alex. Rosenthal (allkiri)
Kohtumees: J. Timann (allkiri)
Kirjutaja: Köhler (allkiri)
