3. Pernu metsa herra kirja järel 21. Juulist sub Nr 321, selles puutudes et Massimõisa Peter Ainson Ennilase kroonu metsa vahi abi Jaan Leo üles andmise järel oma hobused, üks kord 3, kaks kord 4 hobust, kokku aga 11 hobust, kroonu metsas, temast ostetud ja senna külge piiravas rajastikus käia lasknud olla; sai tänasel päeval ette kutsutudPeter Ainson ning vastab seesama selles puuduva küsimise peäle, et tema hobused mitte üks ainus kord kroonumetsa veel vähem rajastikusse saanud olla.
Jaan Leo ütleb, et Peter Ainsoni hobused kül nenda palju korda nagu metsa herra kirjas üleval seisab, kroonu rajastiku peal on olnud, aga ta ei olla neid mitte pantinud ega ei olevat selle juures tunnistust et nad seäl peäl oleks olnud.
Astusid ära.
Et kroonu metsavalitsus kuidagi viisil tunnistuse läbi tõeks teha ei jõua, kui Peter Ainsoni hobused kroou rajastikus käinud oleks ning metsavahi abi ka ühtegi neist hobustest pantinud ei ole, sellepärast ühendasid koos olevad kohtu liikmed endid selle
Mõistusele
Kroonu metsa valitsus ei või sellest omast kaebdusest tunnistuse ning muu tõeks tegemise asjade puuduse pärast ühtegi saada.
Seesinane eesseisva mõistus sai Peter Ainsonile tänasel päeval kuulutud ning
Kästi: sellest protokollist üks ära kiri 3. Pernu metsa Herrale teäda andmiseks sisse saata.
Kogukonna kohtu eesistja J Hansen
Kõrvalistja J Tomson
" M Tuk
