Silla kohto kisso päele mis siu 8. sel kuupäwal N3493 walla liikmette moistusse järrel peab ülles woetut sama. Kas selle walla lige Peter April, kes Pihkva wangihonesse sadetut kui temma aeg täis on, peab taggasi pallutama woi mite, kutsuti kui allamal luggeda kohto ette ja 1) Jaan Kiis ja 2) Jaak Halliksaar, ja 3, Tõnis Kiis ja, 4, Hans Klettenberg, 5, Adam Kiis, 6, Margus Hühnerson, 7, Tõnis Rütel, 8, Jakob Nuttel, 9, Hans Pink, 101, Hans Laan, 11, Jürri Hinuks, 12, Jurri Martensen, 13, Jürri Pilt ning keik kohtomehed kokku 16 hinge keik ja Peter April on 43 aastat wana lutterus usko ja naisimees, temma naise nimme on Marri. 43 ½  aastat wana. Temma poeg Aleksander 7. aastane ja tüttar Maria 3. aastane.
Nüüd hakkati arwama, ja keik ütlesit ühhest suust, meie tahhame sest et ta enne ärravimist üks laitmatta mees olli, et tedda meie koggodusse taggasi lastakse.
Peakohtomees Jürri Hühnerson XXX
Kohtomees Märt Tümm XXX
Wallawanem Hans Pill
