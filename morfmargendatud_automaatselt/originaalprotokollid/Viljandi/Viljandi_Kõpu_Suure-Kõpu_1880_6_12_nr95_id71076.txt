Michel Lohk (käemees Jüri Reimann) tulli ette ja kaebas, et tema endine sulane Tönis Nömm tema sepikoea katust ära wöta tahtnud kus tema seda ärakeelama läinud, aga Tönis Nömm teda seal juures raud tangidega löönud, ja palub kaebaja, et kohus tülitegijad trahwiks.
Tönis Nömm sai ette kutsutud kes ütles, et tema Michel Lohku mitte löönud, kül aga möned naelad, mis sepikoea seina sisse löönud, ärawöta tahtnud, kus juures Michel Lohk teda aia teiwaga löönud, tema seda seal juures rinnust kinni wötnud ja enesest eemale töikanud.
Hans Kalja sai ette kutsutud kes ütles ennast eemalt näinud kui Tönis Nömm Michel Lohko löönud, aga mis selel löömiseks käes olnud, eiteada tema.
Lieso Must sai ette kutsutud kes ütles küsimise peale, et tema näinud kui Tönis Nömm Michel Lohk'o löönud, kus see maha kukunud ja teisi api kutsutnud, aga et Lohk ka Nömme löönud ei ole tema mitte näinud. 
Michel Lohk ütles Nömme mitte löönud olewad.
möistetud:
Tönnis Nömm löömise eest 24 tunni peale wangi panna.
