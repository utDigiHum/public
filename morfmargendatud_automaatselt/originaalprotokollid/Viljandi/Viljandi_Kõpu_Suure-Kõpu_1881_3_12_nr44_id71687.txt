Jüri Kask tulli ette ja kaebas, et Hans Wahher temale 62 R laenatud raha wölgo olla keda mitte äramaksta ja palub kaebaja et kohus Waherit sunniks temale seda wäljamaksma.
Hans Waher sai ette kutsutud kes seda wölga ei salganud, ütles aga, et temal nüid raha ei ole seda ära tasuda.
Möistetud:
Hans Waher peab seda wölga 62 Rbl. 14 päewa aea sees Jüri Kaskele wäljamaksma.
