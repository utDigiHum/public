18. Septembril s.a. sai Kassi Mart Puuseppa loomad 1 siga, 1 wasik ja 1 lehm üles wõetud et need loomad wõlgade pärast 9. Oktobril ära müia, kuulutused said wälja pantud. Täna läksid kohtumehed neid loomi ära tooma, aga ei olnud enam mitte ühte. Märt Puusepp kutsuti ette selle asja üle seletust andma kus ta need loomad on pannud, ütles: siga on ta ära tapnud, wasika on ta täna hommiko peremehe Tonnis Läänesaarele annud ja naene on lehma metsa wõi laande, woi ei tea kuhu ära ajanud, tema käest midagi wõtta ei saa. 
Otsus: Märt Puusepp nisuguse kohtule wastu hakkamise pärast karistuseks Keiserlikko Wiljandi Sillakohtu üle kuulamise alla anda.
Peakohtumees Hans Ant [allkiri]
Kohtumees Jürri Jürrison [allkiri]
-"- M. Länesar [allkiri]
