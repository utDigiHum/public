Metsawalitsuse kaebuse asi metsa raiumise pärast sai ette wõetud
Metsawaht Ado Kass andis üles et peremeestel on luba olnud hago teha ja puu raistikku taieste ära puhastada, hagude seas on aga need kase puu tüikat leitud ja on ära tollitud saanud. Tollitamise juures olnud tema, metsawaht Kristjan Maikalo ja wallawanema abi Hans Rosenberg.
Kristjan Maikalo ütles nendasama et kase tüikad on hagode seas üles otsitud ja ära mõõdetud saanud.
Läne Mait Länesaar seletas, et raiustikus on puuraiumise aeg keik tarwilikut puud metsa wahi üle waatamise all puudeks ära raiutud ja keik puru ja oksad, lubatud neid ära koristada ja on nenda keik ära koristud saanud, aga mingisugust puid ei tea tema raiunud olema. Pealegi ei ole keegi teda senna juure kutsunud, wait tema haod on aga keik laiali laotud ja on tema tehtud töö ilma tema teadmata ära wõetud.
Jürri Kass ütles et tema on luba järele hagu raistikus köitnud ja nenda teinud kuidas kästud, nüid on aga tema haod sootuks ära wõetud, puude raiumist ei tea tema mitte.
Saare Tõnnis Länesaar seletas tema raistikus olnud üks pikk kase kand kraawi kalda peale kelle ladu enne pealt ära raiutud olnud ja on tema selle kannu ära raiunud. 
Läne Gustav Länesaar seletas et tema saanud teatus et haod wõib ära teha, tema poisid teinud haod ära ja ei tea tema sest midagi, et tema haod on laiali pillutud ja ära mädanenud siis  tema jättab need haod senna samade ja ei taha sealt midagi ja ei taha seda trahwi ka mitte maksa.
Kaubi Mikk Kaup ütles et tema on raistikku ära puhastanud ja ka wanad rampet kokko koristanud aga puid ei ole senna sekka saanud teda ei ole keegi juure kutsunud ega temale midagi kurja tööd ära näitanud, pealegi ei ole ka mitte kohtu poolt keegi käinud üle waatamas.
Metsawaht Ado Kass ütles et leppa puid on ka üles wõetud saanud ja ka wana puu tüikat ära tollitud.
Selle mõõtmise juures ei ole aga keegi peremees ise mitte olnud.
Wallawanema abi Hans Rosenberg seletas et teda on juure mõõtma kutsutud, hao hunikut olnud enne juba lahti lõhutud. Tüika puud saanud ise paika mõõtetud ja ladwa tükkit saanud ise paika ära mõõtetud. Need kase puud ei ole mitte hao (hunikute) kubude seas olnud wait ise äranis hao hunikute alla pantud. Leppa puid ei ole mitte olnud. Mõõtmise aeg olnud nende mõõtjatele teada et Soppi Peeti hao hunik saanud waatatud aga kuidas kuulda ei ole se mitte Soppi Peeti hao hunik olnud wait Lääne Gustavi haod.
Moisteti:
Sest et keegi peremees senna puude üle waatamise juure ei ole kutsutud ja sell wiisil sugugi kindel ei woi olla kas iga ühe oma töö on ka tõeste üles woetud saanud ja pealegi sugugi üles antud ei ole et need üles wõetud puud oleks kuhugilt raiutud saanud, pealegi on need inimesed juba isegi sellega ära trahwitud et nende tehtud haod on ära woetud, saab se kaebus tühjaks arwatud ja ei saa need inimesed siit poolt mitte enam trahwitud.
Moistus sai edasi kaebamise tärminidega kohe kuulutud.
Peakohtumees: Hans Ant [allkiri]
Kohtumees: J Rätsep [allkiri]
-"- Jüri Kass [allkiri]
