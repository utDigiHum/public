Jaan Kalm kaebab, et Hans Pertmann pühhade aial Unnakwerre kõrsis joobnud peaga tülli teinud, nöuassare Tõnno pibu käest ärra wõtnud ja mitte ennam kätte annud, sellepeale on Tõnno ähwartanud lüa, ja kui Josep Waldmann tunnistab, on nõuassare Tõnno Hans Pertmanni 2 korda lönud, ja üttelnud, minna teen se 10me rubla tassa, mis temma minnole wõlgo on. Jaan Kalm on Hans Pertmanni wäggise kõrsist ärrawinud, sellepärrast töotanud Hans Pertmann Jaan Kalmo aida ärrapõlletada. Hans Pertmann on ka Andres Ibrust 3 korda lönud, - Hans Rei ja Hans Pertmann Andres Ibrust kõrtsi ümber pu käes tagga ajanud. Andres Ibrus on Hans Rei ni kui Anton Kotsar tunnistab 3 korda lönud. 
Moistetud: Hans Pertmanni 30ne witsa hobiga trahwida.
Nõuassare Tõnno 6 rubla kirriko laekasse .
Andres Ibrus 3 rubla kirriko laekasse.
Hans Rei  15ne witsa hobiga trahwida.
