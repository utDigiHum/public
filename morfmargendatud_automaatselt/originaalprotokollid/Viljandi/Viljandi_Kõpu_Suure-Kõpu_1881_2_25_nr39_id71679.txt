Kohaline metsa walitseja herra Schröder tulli ette ja kaebas, et Hans Grimpe möisa platsi pealt1 kask 11 tolli jäm 24 jalg pitk			1,44 kop		1 lep 10 - " - 24 - pitk			- " 36 "		1 lep 11 - " - 24 - pitk			- " 42 "		Summa			2 R 22 kop		
wäärt warastanud, ja palub kaebaja et kohus Hans Grimped sunniks seda nimetud hinda kolme wöra, möisale kahjutasumiseks maksma.
Hans Grimpe sai ette kutsutud kes seda puu raiumist ei salganud ning ütles, et tema sele asja üle metsa walitseja Straussiga lepitust teinud ning selele 3 Rbl. wäljamaksnud, ja seda wiisi temal ühtegi enam maksta jäänud.
Metsa walitseja Strauss sai ette kutsutud kes ütles, et tema Hans Grimpega metsa warguse üle könelenud ja sele üle seletust nöudnud aga seal juures ningisugust lepitust ega raha maksu olnud, ning see Hans Grimpe etteandmine tühi laimamine on.
Möisetetud:
Hans Grimpe seles asjas süialuseks arwata ning peab seda taksi järele ette antud trahwi 2 R 22 kop. kolme wöra = 6 R 66 kop. 14 päewa aea sees metsawalitsusele wäljamaksma, ning teda oma lepituse 3 R rahamaksu nöudmisega ilma tunnistuseta rahule sundida.
