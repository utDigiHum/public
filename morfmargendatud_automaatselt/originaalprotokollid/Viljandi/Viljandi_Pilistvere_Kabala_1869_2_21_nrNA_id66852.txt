Perremees Jaan Parrikas № 112 ja perremees Jürri Parrikas № 109 tullid kohto ette ja pallusid omma wahhel leppinud kondrahti kohto protokollo ramato ülles kirjutada kui allamal seisab.-
Jaan Parrikas, kes pärra ma ommanik № 109 Tallust on, annab selle Tallo № 109 Jürri Parrika kätte ühheks aastaks  rendi peale ja selle aasta eest maksab Jürri Parrikas omma wenna Jaanile 31 R. Renti ja teeb keik tallo tööd nenda samma kui siis kui se maa Mõisa käest temma käes renti peal olli. Peale sedda ei olle siis Jürri Parrikal mingisuggust nõudmist siis ennam kui sest Tallust  № 109 wälja lähheb. Se kondrahhe aeg kestab 23s April 1869 kunni 23s April 1870. Raud warra jäeb Jürri Parrika ommaks.
Rentija Jaan Parrikas XXX
Rentnik Jürri Parrikas XXX
Pea kohtomees J. Koik
