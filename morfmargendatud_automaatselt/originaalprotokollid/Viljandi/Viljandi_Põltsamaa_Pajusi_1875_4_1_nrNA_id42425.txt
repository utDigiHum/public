Tännasel üllewal nimmetud päewal maksis Karel Jürgen sia Kaarle Saarele, kes temma est on nekrudiks lainud, 30 rubla sisse, ja pallus, et sedda rahha, kui Karel Saar isse ehk ka kirja läbbi peaks perrima, wibimata temma kätte saab antud ehk sadetud. 
Mõisteti: Et sedda asja nõnda toimetada kudda üllewal on räkitud. Peakohtumees: Andres Pusep xxx
Karel Saar on ned 30 rubla sel 22 Aprillil wälja wöttnud. Peakohtumees: Andres Pusep xxx
