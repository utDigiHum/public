Willem Saar tulli ette ja kaebas, et temal 3 naela takust lönga á 4 pooli, ärawarastud kus neid löngo An Grimpe juures nähtud olla ja palub kaebaja, et kohus seda asja labi kuulaks ja süialust temale seda äratasuma sunniks.
An Grimpe (käemees Michel Tiet) sai ette kutsutud kes ütles, et tema sele wargusest ühtegi ei teada.
Lieso Grimpe (käemees Hans Grimpe) sai ette kutsutud kes ütles, et tema 3 takust lönga An Grimpe woodis näidud, keda see iljam ärawarjanud, aga kele omadus need olnud, et teada tema. Ka olla tema (Lieso Grimpe) oma lönga An Grimpe kapist leidnud ja kätte saanud.
Sele peale ütles An Grimpe, et see löng, mis tema kapist leitud, oma lönga arwanud olewad, aga Saare lönga mitte tema woodis olnud.
Möistetud:
Willem Saart puudulise tunnistusega oma kaebtusega rahule sundida, aga An Grimpe Lieso Grimpe löngade üle süialuseks arwata ja 24 tunni peale wangi panna.
