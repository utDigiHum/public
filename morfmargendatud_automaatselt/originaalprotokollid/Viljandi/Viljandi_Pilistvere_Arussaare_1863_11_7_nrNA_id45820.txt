7 November 1863
Koos ollid: Jaan Noor Peakohtomees
Hans Siww kohtomees
Jürri Noor kohtomees
Se nekrut Hans Eward No 1. astus ette ja pallus protokoli üllespanna, et se Waino saun kus temma emma ja wöeras issa Tönno Riik sees ellawad Tönno Jürri Kaerdti käepeale jääb, kes temmale 50 rubla öbbedad maksab, ja kui temma üks kord peaks taggasi tullema siis maksab ta Jürri Kärdile se 50 rubla taggasi ja saab sauna ommale. Ait jääb Marri Ewardile. Kui Hans Eward ennam taggasi ei tulle siis saab se saun Marri Ewardile, kui ta se 50 rubla Jürri Kärdile ärramaksab, kui kegi ei makse jääb se sain Jürri Kerdile. Marri Eward saab 1 mullika ja 1 lammas. Tönno Riik peab ja maja eest hoolt kandma ja nipaljo ehhitama, kui tarwis. Hans Eward saab Tönno Riik käest omma sui tenistusse eest 2 wakka rukkid ja 2 wakka odre. Rukki wakka hind 1 rubla 80 koppikud se teeb 3 rubla 60 koppikud, odra wakk 1 rubla 30 koppikud se teeb 2 rubla 60 koppikud, summa 6 rubla ja 20 koppikud mis Tönno Riik wälja maksab, kui ta wilja kohhe ärra müja ei sa. 1 kuub 3 rubla 20 koppikud paale kassuka ette 3 rubla summa 6 rubla 20 koppikud. 10 rubla, mis ta pahhu rahha on annud. 20 rubla ühhe hobbese eest. Üllepea on Tönno Riikil maksa 42 rubla ja 20 koppikud öbbedad Hans Eardi kätte.
Peakohtomees Jaan Noor XXXKohtomees Hans Siww XXX
Kohtomees Jürri Noor XXX
