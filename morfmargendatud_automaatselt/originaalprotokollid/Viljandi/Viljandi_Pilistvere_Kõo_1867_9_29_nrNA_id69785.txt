Tännasel päwal ollid Kõo ja Arrosare walla wannemad, nende abbid, nõumehhed ja kohtomehhed pearahha arwamas koos, ning tulletas Kõowalla Wannem pearahha arwamisse aeg nõumeestele mele, et Kõo egga Arrosare wallal walla ja kohto maia ei olle ja koggokonnakohhus kassari hones, kus praego on, ei wõi mitte pitkalt jäda, sest et se maia ammu wanna, mäddand, must ja hopis kohto ja ello maiaks sündmatta on, ja et uut walla ja kohto maia warsti peab teggema hakkama, mis jures tarwis olleks praeguse pearahhaga hingede pealt 100 Rubl. hõbb.sisse aiada.
Selle peale wastasid nõumehhed, et wallal uut maia kül tarwis ja peab ehhitud sama, agga mitte wõimalik ei olle pearahhaga hingede pealt walla Maia ehhitamise jäoks rahha sisse aiada, sest et pearahha muidogi suur ja paljo neid, kes maksa ei jöua, ning arwasid nenda, et Kõo ja Arrosare se maja kokkoteggema sawad, siis wõiks Kõowalla laekast arwata 200 Rubl. ja Arrosare walla laekast 50 Rubl. selle Maia ehhitusseks wõetud sada, sest wald se läbbi ka mitte kahjo ei sa.
Ka walla Wannemad ja nende abbid ja kohtomehhed ollid nende nõumeeste ette pannemisse ja nõuga rahhol, mis peale 
mõistetud: sai sedda nõumeeste ette pannemist ja nõu piddamist walla laeka abbiga kohto maia ehhitada, allandlikkult Keiserlikko Pernu Vma Kihhelkonnakohtule kinnitamisseks ette panna.
                                                        Walla Wannem: Rein Kopp
Arrosare Wallawannem                       dito         abbi:  
           Karel Saar. XXX                          dito         abbi: Jürri Olbrey
Nõumees: Maddis Keller XXX      Nõumehhed:  Jaak Saar. XXX
                   Karel Riisk. XXX                                    Andres Konks. XXX
Kohtomees: Jürri Käärdt XXX                               Mart Seil. XXX
Wallaaidamees: Tõnnis Rebbane                         Jürri Saar. XXX
                                     X X X                                     Andrey Krasawsky ja teised
Peakohtomees: Jaak Kukmann
Kohtomees: Ado Lagus. XXX
         dito      Hans Riisk. XXX
