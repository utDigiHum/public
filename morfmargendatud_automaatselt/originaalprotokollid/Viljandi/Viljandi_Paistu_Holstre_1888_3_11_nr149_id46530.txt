III Pärnu kroonu metsawalitsus on kirjaga 17 Augustist 1885 No. 341 all kaebanud, et Hans Laane Willandi mõisast Holstre kroonu metsast warastanud:
3 mända 6 sülda pitkad 7 tolli jämedad
1    "         5     "          "     6    "        "
1    "         4     "          "     5    "        "
Hans Laane ette kutsutud wastab selle peale, et tema neid puid, mis Fromhold Balzar tee peal tema käest kinni wõtnud, mitte kroonu metsast warastanud, waid ta on need puud Sukki talu soo pealt toonud.
Kohtumees Mihkel Hansen andis protokolli: tema on kui kohtumees endise kroonu metsawahi Fromhold Balzari juures enne selle surma kodus käinud kõnesseiswa puuwarguse asjas küsimas ja on siis metsawaht temale tunnistanud, et Hans Laane seda puu wargust isegi salganud ei ole, waid temale raha pakkunud kui ta mitte ülesse ei annaks ning olla Hans Laane weel lubanud kannud ise metsas kinni katta.
Selle sinatse kohtumehe Mihkel Hanseni üteluse peale sai
Mõistetud: Et Hans Laane 8 päewa sees tänasest 17 rubla 84 kop. kroonu kassa heaks trahwi ära peab maksma.
Eesseiswa otsus sai kohtukäiatele kuulutatud ja need Appellatsioni Formaliadega tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumees MHansen [allkiri]
                     JAndresson [allkiri]
