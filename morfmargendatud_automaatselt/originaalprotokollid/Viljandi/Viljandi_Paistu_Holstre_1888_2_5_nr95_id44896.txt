5" Webruaril 1888.
Hans Jako kaebduses Peter Johannsoni wastu, aia lõhkumise eest kahju tasumise nõudmsie pärast, olid mõlemad kohtukäiad ja järgmised tunnistajad ette tulnud:
Jüri Pütsep, Holstrest tunnistab et tema selle aia teinud ja on Hans Jako selle teha lasknud. Aia lõhkumist ei ole ta näinud. Enne kui Jaako senna aeda teha lasknud on sääl ka tükkati aeda olnud. Ka saanud uue aia tegemises mõni lepp Lihapulli talu metsast raiutud. Need roowikud mida senna aia tarwis toonud olnud kuuse roowikud.
Hans Rääbus tunnistab et tema Hans Jakol abiks olnud Heimthali metsast roowikuid wedamas, aga kui palju neid roowikuid säält toodud seda tema ei teadwat.
Jaak Mossa tunnistab et ta aia lõhkumisest ei tea, aga seda teadwat ta kül et Hans Jako selle aia teha lasknud ja on ta ühes roowikuid Heimthali metsast wedanud.
Peter Johannson wastab sellepeale, et sääl enneseda juba aed olnud, ja olla Jako ainult aeda parandanud seda wõiwat tunnistada: Märt Tõnisson Aidust, Hans Silk, Andres Silk wana, ja Toomas Raudsepp Holstrest.
Hans Jako andis Peter Rookmanni selle juurte tunnistajaks et Peter Johannson aia lõhkunud on.
Mõisteti:Kohtukäiad ühes üles antud tunnistajatega ettekutsuda.
Peakohtumees JLuik [allkiri]
Kohtumehed: JJaama [allkiri]
                         JTill[allkiri]
Kirjut Raudsepp [allkiri]
