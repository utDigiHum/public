Et Koorti Mihkel Wiilopi naene Alwine ära surnud sellepärast sai wiimsest järele jäänud warandus tema säädusliku päriate hääks tänasel pääwal üles kirjutatud.
Pääle lesemehe Mihkel Wiilopi on lapsi järele jäänud: Eduard 11 aastat, Hugo 4 aast., Ida 3 aast. ja Leo 1 aasta wana.
Laste vöörmündriteks säeti Johann Äniline ja Jaan Rookmann Paistu kiriku wallast.
                                    Pärandus on järgmine:
               Pärandus              Rbl.Kop.						
        Pärandus    Rbl. Kop.                               			
					                                                                        						
Transport 40. 90    					 1. kummut                            5     "  			1 wana kappi                        2     "		 1 riide kast                           3     "			4 patja                                  8     "		 1 laud                                    2     "			 1 õmbluse padi                   3     "		 2 ümmargust lauda            6     "			 1 willane teki päälmine      2     "		 1 õmbluse kast                    1      "			 1 sohwa padi                        1     "		 2 peegelt                             2     "			 1 takune kirju tekk              "    50.		 1 suur peegel                      2     "			1 naesterahwa kübar            1    "		 4 hegeldatud laud lina      6     "			 1 kasuk riidega                     5     "		 2 linnariidest woodi lina   1     40			 1 Palito                                 10     "		 4 linast woodi lina             2     80			 2 sitsi, 1 willane alune          2     "		 2 takust woodi lina            1      "			 1 sitsi 1 willane jaki                1     "		2 padja pööra                      1      "			 1 lehm                                   20    "		4 linast kätterätikud           1      20			 1 lammas                                 2     "		5 jämedat kätterätikud      1      50			 1 Kohwi masin                        1     60		
3   laudlina                            5       "        
                                               Kokku 40 90
                                                                                     
                                                                               Summa 100 rubla
revidint am 15 November 1889
.......................................
