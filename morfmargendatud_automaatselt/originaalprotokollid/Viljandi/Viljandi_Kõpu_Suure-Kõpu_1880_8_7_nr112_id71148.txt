Kadri Mänd (käemees Jüri Waher) tulli ette ja kaebas, et Tönis Mänd tema tütre Ano enesele karjatseks tellinud aga seda 6 Julil c. lahti lasknud, ja palub kaebaja et kohus Tönis Mända sunniks tema tütrele palka ja tarwilist ülespidamist andma.
Tönis Mänd sai ette kutsutud kes ütles, et Ano Mänd mitte enam karja minna tahtnud ning tema seeperast teda lahti lasknud.
Sele peale ütles kaebaja Kadri Mänd, et tema tütar Ano, teiste karjatsetest, karja juures alati pekstud saanud see perast ei ole see mitte enam karja minna wöinud.
Lepisid oma wahel senna, et Tönis Mänd karjatse Ano Mänd jäle oma teenistusese wötab, ning sele eest hoolt kannab, et see teistest mitte pekstud ei saa.
möistetud:
Seda nönda kui sündinud maha kirjutada.
