III Pärnu kroonu metsawalitsus on kirja läbi 10 Märtsist 1887 No. 149 all kaebanud, et et Märt Kansi kroonu metsast warastanud on: 1 kuusk 4 sülda pitk 7 1/2 tolli jäme
                            4    "      2    "       "     2          "      "
Märt Kansi ette kutsutud  wastab et tema kroonu metsast puid ei ole warastanud ega ei teadwat ta sellest wargusest ühtigi.
Kui nüüd siin tänasel päewal ees olew kroonu metsawaht Johann Balzar tunnistab, et Märt Kansi  tõeste need puud kroonu metsast warastanud on, sellepärast sai
Mõistetud: Et Märt Kansi 8 päewa aea sees kroonu kassasse 5 rubla 16 kop. ära maksma peab selle puu warguse trahwi.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja Appellatsioni Formaliad tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumees MHansen [allkiri]
                      JAndresson [allkiri]
