Koggokonnakohhus Kõomõisas sel 24mal Märtsil 1867. aastal.
Mõisa kubjas Hans Oiassonn astus koggokonnakohto ette ja kaebas, et mõisa pois Jürri Luik metsa tee peal tülli ja rido teinud, hobbusega kõrtsis pole päwa wahtinud ja pealegi weel mõisa poissi Tawit Siworie kolm korda lönud mis temma issi wasto ei aia kelle eest temma 20 hobi witsadega trahwitud sai ja maksab peale selle Tawit Siworile kelle piip temma olli ärra wõtnud, 75 Cp.
Kohto nimmel: Peakohtomehhe abbi: Karel Riisk
