Jaan Peedi kaebduses nõudmise pärast Jaan Johannsoni wastu, olid mõlemad kohtukäiad tänasel päewal ette tulnud.
Kui nüüd kohtukäiatel oma endiste ütteluste täienduseks enam ühtegi ette taus ei olnud, ja et Jaan Peet omad nõudmised Jaan Johannsoni wastu tunnistajate läbi tõeks teinud on, sellepärast ühendasid ennast koos olewad kohtu liikmed selle
Mõistusele: Et Jaan Johannson Jaan Peedile maksma peab:
1. Kwitungi põhjusel 31 Juulist 1877 280 Rbl. 10ne aasta  5% intressid senna juure 140 R.
2.Talu ostmise hinna protsendiks maksetud 8 Webr. 1878 aastal  37.37
   Intressid senna juure, 10ne aasta eest 18.90
                                         Summa 476 Rubla 7 kop.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja need Appellatsioni Formaliadega tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JJaama [allkiri]
                        JAndresson [allkiri]
Kirjut Raudsepp [allkiri]
