Jaak Kalm tulli ette ja kaebas, et Jaan Pillau temale 2 Rbl 15 kop. hobuse söido eest wölgo olla keda mitte äramaksta.
Jaan Pillau sai ette kutsutud kes seda ei salganud ütles aga, et Jaak Kalmul temale 2 kord hobuse rautuse eest 2 R 50 kop. maksta olla. Sele peale ütles Kalm et Pillau temal ükskord 2 hobuse rauda ühes naeldega andnud ja teine kord tema enesel rauad ja naelad ligi olnud, kus Pillau siis need rauad tema hobusele alla löönud ning seda köik ilma maksuta tehja lubanud.
Möistetud:
Jaan Pillau peab seda wölga 2 R 50 kop. Jaak Kalmule wäljamaksma, kust aga hobuse rauad ja rautuse töö eest 40 kop. maha arwata tuleb.
