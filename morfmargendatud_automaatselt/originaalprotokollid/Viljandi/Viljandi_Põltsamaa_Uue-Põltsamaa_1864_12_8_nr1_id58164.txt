Kaupmees Martinson kaebas et temmal ühhe linna-woori saates Perno tee peal mehhed linna warrastanud.Pernus kirja järgi pudus:
Terrasse Karl Mintel - 2 leisikad - naela.
Linnosaare Ans Weinglas - 9 leisikadMihkel Bergmann - 30 naela(Juhhan Sussi) - 4 leisikadJaan Kert.
Summa 16 leisikad 10 naela.
Ka ollewa Mart Pitk Nõmmewerre kõrtsi ees linno ümber wahhetanud.
Kaebaja nõudis, et puduwad linnad 3 Rubla hõbb. leisik sawad makstud ja wargad trahwitud ja Mart Pitk ka kohto ette tarwitud.
Tulliwad siis ette, Ans Weinglas ja selletas, et Mart Pitk temma koorma pealt Nõmmewerre kõrtsi ees 3 leisikad ümber wahhetanud omma linnade wasto, - kus puduwad linnad jänud, ei teadma temma mitte.
Sussi Juhhan Sassi, selletas et se wahhetus 9 1/2 leisikad olnud, Mihkel Bergman teadma sedda - Kaantso kõrtsis ollewa igga woori-mees ühhe kimbo sisse wiinud ja kõrtsi mehhele ärramüinud. - Torri kõrtsis on ärramüidud 3 Leisikad 10 naela.
Mihkel Bergmann, selletas et Mart Pitk Nõmmewerre kõrtsi ees linno wahhetanud. - Ants Weinglas ollewa räkinud, et temma isse 2 1/2 leisikad ja Mart 7 leisikad wahhetanud.
Jaan Kert selletas et temma Kaantso kõrtsis ühhe kimbo müinud, muist ollewa Sussi Juhhan ja Mihkel Bergmann ärrawarrastanud.(teisiks kohto päwaks.)
