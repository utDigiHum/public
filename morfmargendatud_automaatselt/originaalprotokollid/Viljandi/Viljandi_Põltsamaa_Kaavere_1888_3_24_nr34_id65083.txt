Kaavere mõisa perriherra O. von Samson nõudis peremees Hans Kukk käest kirja läbi kohtu teel koha tarbimise raha 1000 Rbl kätte.
Kaebatu Hans Kukk  ütles endal see 1000 Rbl. Kiil wälja olema- nõudis aga temini seda, et tema kreditkassat kaebaja herrale välja maksta tahab.
Hans Kukk  peab see raha 1000 rubla kaebajale kolme nädala sees wälja maksma- see on 14 Aprilini 1888. See sai kaebatule...
Peakohtumees Jaak Oinas xxx
Kohtumees Karl Mihkel xxx M Anni
