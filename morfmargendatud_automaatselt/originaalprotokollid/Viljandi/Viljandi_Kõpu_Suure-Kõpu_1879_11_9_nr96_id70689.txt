Jüri Mänd tulli ette ja kaebas, et Peter Lacks Puna körtsi ees tema hobuse pealt ühe sedulka ärawarastanud, ja palub kaebaja et kohus seda asja läbi kuulaks ja süialust trahwiks.
Peter Lacks sai ette kutsutud kes seda ei wöinud salgada ning lepisid kaebajaga kahju üle nenda, et Peter Lacks Jüri Mänale 3 R kahjutasumiseks maksab ning seda 8 päewa aea sees äratasub.
möistetud:
Peter Lacks warguse töö eest 15 witsa löögiga trahwida.
