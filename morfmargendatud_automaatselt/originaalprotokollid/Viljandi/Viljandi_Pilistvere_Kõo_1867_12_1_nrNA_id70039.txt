Kõomees Mihkel Nurk kaebas, et Jürri Tamm temmal wäggise jallast üks paar sapaid jallast ärra wõtnud ja ärra lõhkunud, sest et temma temma lõõtsa pilli wasto Maad lönud.
Selle peale wastas Jürri Tamm, et Mihkel Nurk Willemi Jürri pilli wasto Maad lönud purruks ja temma Mihkle eest 1 rubla wälja maksnud.
Mõistetud: et Jürri Tamm maksab Mihkel Nurgale sabaste eest 3 Rubla wälja ja saab 10 hopi õppetusseks.
Peakohtomehhe abbi: Karel Riisk
Kohtomees: Tawit Markus. XXX
        d            Karel Malkin. XXX
