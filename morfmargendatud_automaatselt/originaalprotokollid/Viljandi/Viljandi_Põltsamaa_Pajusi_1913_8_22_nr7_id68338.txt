Alguslepingu on 50kop. wäärtuselise tempelmarga peale kirjutatud.
Kopia.
Leping (kõrval: vene keeles, Originaal lepingu olen wastu wõtnud 20 Detsembril J. Jürgen).
Tänasel allnimetatud pääwal on Pajusi walla "Mando" N025 talu omaniku Jaan Joosepi poja Jürgeni ja Uuepõltsamaa walla liikme Mihkel Jaaku poja Mihkelsoni wahel järgmine leping tehtud, mis mitte ainult lepingu tegijatele maksew ei ole, waid ka nende pärijatele ja õiguse wõtjatele makswaks jääb.
Paagraf 1.
Jaan Joosepi poeg Jürgen müib oma talu piirides olewast turwa rabast Mihkel Jaaku poja Mihkelsonile pool (1/2) wakamaae ära, nende piiride sees, nagu seda müia Jürgen ja ostja Mihkelson ühes Jaan Prinksi ja Hans Kungas'e juures olekul wälja mõetnud ja ära märkinud on, nimelt: Taki talu piirist 42 küinart eemalt alates, 1/4 wakamaa pikuselt, Jaagu Priksi ja Hans Kungase turwa platsi wahel, selle tingimisega, et Mihkel Mihkelson, ehk tema pärijad ja õiguse wõtjad, selle raba tüki pialt turwad wõiwad ära lõigata ja ära wedada, ette määramata aja sees, aga maa põhi jääb piale turwaste ära lõikamise müüa omanduseks.
Paragraf 2.
Ostja Mihkel Mihkelson maksab müüa Jaan Jürgenile selle poole wakamaa eest (50) wiiskümmend rubla, mis rajastt müüal Jürgenil (40) nelikümmend rubla kääs on ja puuduolewa kümme rubla selle lepingu kinnitamise juures kohe wälja.
Paragraf 3.
Müüa Jaan Jürgen annab tee, mis turwa rabast algab ja müia talu õuest läbi maantee piale wälja lähäb ostja Mihkelsonile nii kauaks waba pruukimiseks, kuni turw ostetud maast wälja on lõigatud ja ära weetud.
Paragraf 4.
Ostja teeb omale kohuseks, mulla, mis turwa pial on umbes üks jalg sügawuselt wanasse turwa auku lükata ja iga aasta pialt tasaseks lükata.
Paragraf 5.
Müüa annab ostjale, luba wiimasid turwi seal kuiwatada, kust turw ostetud platsi pialt ära on lõigatud.
Paragraf 6.
Müüa ei wõi turwa lõikuse aeg wett paisuga mitte kinni pidada.
Paragraf 7.
Selle lepingu kinnitamise kulud kannab ostja üksi.
Müüja Jaan Jurgen
Ostja M. Mihkelson
1913 aastal Augusti kuu 22 kuupäewal on eeolew leping Pajusi wallakohtus, Wiljandi kreisis ilmunud, lepingu osalistele iskutele Jaan Josepi p. Jürgen'ile ja Mihkel Jaagu p. Mihkelsonile ette loetud, kes lepingus tähendatud tingimistega rahul oliwad ja eesolewa lepingu oma käega alla kirjutasiwad. - Lepinguosalisedd isikud on kohtule palelikult tuntud ja on neil öigus aktisi walmistada, sellepärast on eeolew leping Pajusi wallakohtu akti raamatusse N7 all sisse kirjutatud, mida Pajusi wallakohtus ustawaks tunnistab.
Eesistuja: J.Watter
Kirjutaja: M. Must
Alguskirjaga õige!
Eesistuja: xxx. Kirjutaja: xxx
