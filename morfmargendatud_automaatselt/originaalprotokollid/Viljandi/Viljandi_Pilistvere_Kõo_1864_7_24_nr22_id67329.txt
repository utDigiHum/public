Luhhawaht Jürri Kallmann kaebab, et Kõo Mölder Mart Kraner mõisa kuhjast heino winud, ja temma tedda tagga aianud ja kätte sanud ja arwates ühhe leisika jaggo heino temma käest mõisa talli winud.-
Mart Kraner tunnistab, et temma mitte kuhjast põlle kiskunud, waid kuhja alt neid, mis lahti maas olnud, omma wakresse pannud:
mõistetud: et Mart Kraner neid mõisa heino sealt on tonud, maksab 3 rubla trahwi.-
