Kaebab Jaan Lõo et tema mõtsa maha raijotud on saanud ühel õhtu poolel, kirwega
1 kask    - 12 sülda 12 tolli
2 kaske -   8    "       8    "
3 kaske -   5    "       7    "
Tema on poisi Peter Lõoga mõtsa wahtima läinud ja näinud kui Jaan Kikka tulnud oma hobuse mõtsa ajanud, kõige jämema pakku pääle pannud, kui nemad senna ajanud ratsal hobustega on Jaan Kikka pakka päält mahalükkanud. Nõuab kroonu taksi järele puude eest tasumist, ja 1 rubla pantimise eest.
Jaak Kikka ütleb, et tema säält mõtsast mööda minnes näinud, et puid mõtsa raiatud olnud tema on arwanud et Lõo poisid mõtsas on ja on seepärast mõtsa sisse läinud.- pakku ei ole tema aga mitte peale pannud, ega neid puid raijunud ega warastanud kül aga tema hobune panditud saanud.
Peeter Lõo 16 aastat wana tunnistab ennaast näinud olema kui Jaan Kikka tulnud ja hobusegi mõtsa ajanud ühe pakku peale pannud ja tõist weel panna tahtnud ja kui nemad hobustega juure ajanud on seesama pakkud päält mahalükkanud. 
Et Peter Lõo alles alaealine lugernata poisike on, kelle tunnistus midagi ei maksa seepärast
Mõisteti: Jaan Lõo ei wõi omast kaebdusest kuni paremad tunnistust saanud ei ole, Jaan Kikka wastu, ühtegi saada,- muud kui pantimise raha 1 Rubla peab Jaan Kikka Jaan Lõole äramaksma.
Eesseiswa mõistus sai kohtukäijatele kuulutadud ja olid needsamad sellega rahul ja maksis Jaan Kikka 1 rubla ära.
