An Reimann (käemees Hans Wahher) tulli ette ja kaebas, et Jüri Glück tema poega Andres Reimann kes 11 aastad wana, enesele karjatseks wötnud aga seda 12 nädalise teenistuse järele ära aeanud, ja palub kaebaja et kohus Jüri Glücki sunniks tema poeale tarwilist ülespidamist maksma.
Jüri Glück sai ette kutsutud kes ütles, et Andres Reimann wäga hooleto olnud, karja üksi jätnud ning isi muiale läinud kus kari sagedaste wiljadesse läinud ja kahju teinud seeperast ei ole tema seda mitte enam oma teenistuses pruukida wöinud ning An Reimanni enast karja sundinud, aga An Reimann siis ühes oma poeaga ära läinud seeperast ei wöida tema neile ühtegi maksta.
Möistetud:
Jüri Glück peab 12 nädalise Andres Reimanni teenistuse eest 1 wak rukid, An Reimannile maksma.
Peakohtumees: J. Kusik (allkiri)
Kohtumees: T. Taman (allkiri)
Kohtumees: J. Org XXX
Kirjutaja: Köhler (allkiri)
