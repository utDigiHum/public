Hans Nurga kaebduses Hans Seppa wastu, olid mõlemad kaohtukäiad ette tulnud.
Kaebduse protokoll 5 novembrist 1887 No. 595 sai ette loetud.
Hans Sepp wastab et tema Hans Nurgaga kaarti mängimise juures tülisse läinud on, sellepärast
Mõisteti: Et nad mõlemad 1 rubla trahwi Holstre walla waeste laekasse 8 päewa sees maksma peawad.
Eesseiswa mõistus sai kohtukäiatele kuulutatud. Hans Nurk palus Appellatsioni mis lubatud ei saanud et asi weike on.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JAndresson [allkiri]
Kirjut Raudsepp  [allkiri]
