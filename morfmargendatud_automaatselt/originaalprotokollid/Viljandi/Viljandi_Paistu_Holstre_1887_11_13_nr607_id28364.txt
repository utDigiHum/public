Ette tuli lesknaene Reet Nirk ühes nõumehega Hendrik Mossa ja nõuab Peter Nirgi käest selle eest aastas 25 rubla, et ta kaebatawa pesu pesenud, söögid walmistanud ja et see 11 aastad tema majas korteris olnud, ühte kokku aga 275 rublat.
Peter Nirk ettekutsutud wastab ees seiswa nõudmise peale, et ainult mõne korra sääl ennast üles pidanud ja temal ainult üks weikene kapp sääl kaebaja majas olnud, muu kraam aga sügisest 1886 saadik. Et ta seni puumeister alati wäljas töös olnud siis ei ole ta ka mitte Reet Nirki oma pesu peseda ega söögid keeta laskta saanud. Ka on tema ajuti Lahmuse metsawahil ja Wiisa asutkeses elanud, aga kunagi mitte pitkemalt Reet Nirgi majas, sellepärast ei wõiwat ta ka sellel ühtegi maksta.
Kui nüüd Reet Nirk tõeks teha ei jõua et ta Peter Nirgi pesu pesenud ja selle söögid keetnud oleks ja et korteri üüri ajuti sääl olemise eest ka mitte tellitud ei ole sellepärast sai
Mõistetud: Et kaebaja Reet Nirk sellest omast nõudmisest ühtegi saada ei wõi.
Eesseiswa mõistus sai kohtukäiatele kuulutatud ja neile Appellatsioni Formaliad tutwustatud.
Peakohtumees JLuik [allkiri]
Kohtumehed JAndresson [allkiri]
                        JTill [allkiri]
Kirjut Raudsepp [allkiri]
