Kohaline metsa walitseja herra Schröder tulli ette ka kaebas, et Napsi Tönnis Mäekink oma karjaga möisa metsast läbi käinud kus seda kinni wöetud, ja palub kaebaja et kohus Tönnis Mäekinko sunniks panditud koomade pealt, kui: 
18 kuri elaja eest á 30 kop - 5 R. 40 kop.
35 lamba eest á 20 kop - 7 " - "
ja pandiraha - " - 1 " - "
Summa 13 R. 40 kop.
möisale kahjo tasumiseks maksma.
Tönnis Mäekink sai ette kutsutud kes seda ei wöinud salgada ütles aga et tema kari seal metsale mingisugust kahju teinud.
möistetud:
Tönnis Mäekink peab rahu kohto saaduse § 148 järele 10 R. kahjutasumise ja 1 R. pandi raha, möisale maksma.
