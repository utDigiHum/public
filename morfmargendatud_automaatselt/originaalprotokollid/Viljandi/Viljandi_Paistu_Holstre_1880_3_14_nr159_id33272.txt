Ette tuli Mihkel Pertma kes alaealine ühes oma käemehega Peter Pertma ja kaebab et tema endine peremees Jaan Ritson tema palgast 4 Rubla ilma kellegi süüta kinni pidanud ja teda omal ajal ka kooli ei ole lasknud minna kelle eest ta 1 R. 50 kopk. trahwi maksma pidanud.- Kaebaja nõuab nüüd 4 R. 50 kp.
Jaan Ritson wastab et ta  Mihkel Pertma palgast sellepärast 4 Rubla kinni pidanud et see pulmas käimisega 3 päewa ära wiitnud.- Kooli minemast ei ole tema Mihkel Pertmat keelani, ega ei taha ka selle trahwi maksta.-
Tunnistaja Kadri Loorits ütleb ennast aga kuulnud olema kui Mihkel Pertma üttelnud "leiwa kotti mul ei ole kellega ma kooli lähen". Perenaene on ka sellel päewal linnas olnud.
 Mihkel Pertma ütleb et ta 2 päewa kül pulmas käinud on.-
Mõisteti: Jaan Ritson wõib nende tunnistatud kahe ära wiidetud pooliku päewade eest 1 Rubla kinni pidada peab aga 3 rubla palga ja 1 R. 50 kp. kooli trahwi raha, summa 4 rubla 50 kp. Mihkel Pertmale wälja maksma.-
Eesseiswa mõistus sai kohtukäiatele kuulutatud.
