Köster Schneider andis ette et Johann Piip on ühel õhtul lugeja tüdrukutega wallatust tegemas näinud kus ta kinni wõetud.
Õpetaja ütleb et ta sellel õhtul Johann Piibu oma juure kutsunud aga see on sonakuulmata olnud ja Kostrele wallatust tegema läinud. Johann Piip ei saa seda mitte wabandada.
Mõisteti. Johann Piip saab 20 hoobi witsadega karistadud.
Eesseisew mõistus sai kuulutud ja täidetud.
Eestseisha Peter Rookmann xxx
Kõrvalistjad Johan Reitel xxx
Hans Kirsel xxx
