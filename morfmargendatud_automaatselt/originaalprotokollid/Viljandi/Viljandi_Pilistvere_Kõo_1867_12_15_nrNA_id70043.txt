Wõiseko walla mees Libbeda Karel Alli kaebab, et temma omma poia Mihkel Loopre perremehhel Wiwarre Jürri Karjatseks annud kahhe aasta eest ja seal ei olle mitte lapsele riet tehtud egga ka lastud kolis käia ja nüüd poisi weel poliko aasta pealt ärra aianud, mis perremees mitte ei wõinud wasto aiada. Mõistetud: et se pois saab 1/2 wakka rukkid, 1/2 wakka odre, 1 nael lihha, 1 nael silko näddalis kunni aasta täis saab küünla päwa aeg ja teeb perremees temmale ka selle wõlga jänud kue kätte.
Peakohtomees: Jaak Kukmanm
Kohtomees: Hans Riisk XXX
      dito         Ado Lagus. XXX
      dito         Karel Malkin XXX
      dito         Karel Riisk.
