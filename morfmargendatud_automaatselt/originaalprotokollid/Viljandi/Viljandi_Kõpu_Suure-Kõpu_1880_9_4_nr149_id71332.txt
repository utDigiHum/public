Peter Bergmann tulli ette ja kaebas, et Jüri Kusik teda ropuste söimanud "lits, karmantsik ja siga", ning palub kaebaja, et kohus söimajad trahwiks.
Jüri Kusik sai ette kutsutud kes ütles, et Peter Bergmann teda laemanud et tema lapsed karmanstsikud olla keda koolmeister selele rääkinud olewad, sele peale tema (Kusik) siis ütelnud et koolmeister siis litsmees on kui niisugust walet on könelenud, aga mitte Peter Bergmanni söimanud.
Wöörmünder Jaan Thomson sai ette kutsutud kes ütles, et Jüri Kusik ja Peter Bergmann wastastiko tülitsenud ja waielnud, kus Kusik Bergmannile ütelnud, et see karmanstsik, siga ja häbemata lits olla.
Möistetud:
Jüri Kusik sele söimamise eest 2 R waeste laekase trahwida.
Kohtumees: J. Org XXX
Kohtumees: J. Taman (allkiri)
Kohtumees: A. Holzmann (allkiri)
Kirjutaja: Köhler (allkiri)
