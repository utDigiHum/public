Endine kooli wanem Peter Bergmann tulli ette ja kaebas, et koolmeister Märt Soob kooli puudest 2 aastad ning iga aasta 8 jalga puid Jaan Kollistele äramüinud, ning palub kaebaja, et kohus seda asja läbi kuulaks, sest et koolmeistril kooli puude üle mitte woli olla ega neid ara anda wöida.
Märt Soob sai ette kutsutud kes seda 15 jalga puu ära andmist ei wöinud salgada, ei ka sele üle mingisugust wabandust ette anda.
Sele peale sai walla wanem Johann Jaska ette kutsutud ning temalt sele puu woli üle seletust nöutud, kes ütles, et koolmeistril terwe aasta puud ees on, mis tulewa kooli aeaks seisawad, ning koolmeistril walla puude ära andmise mingisugust wöimust ei ole.
Möistetud:
Märt Soob peab neid 16 jalga äraantud puid, wiibimata kooli juure tagasi muretsema, ning saab niisuguse sääduse wasto puu ära raiskamise eest 3 R waeste laekase trahwida.
Kohtumees: K. Aunap (allkiri)
Kohtumees: J. Taman (allkiri)
Kohtumees: J. Org XXX
Kirjutaja: Köhler (allkiri)
