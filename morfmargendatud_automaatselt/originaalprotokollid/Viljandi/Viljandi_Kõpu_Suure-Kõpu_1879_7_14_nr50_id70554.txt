Romann Mühlberg tulli ette ja kaebas, et tema öel 1 kleid ja 1 paar suki mis 5 Rbl. wäärt, pleek aidast ära warastatud, kus seda kleiti peale seda An Köwerjala seljas nähtud ja palub kaebaja, et kohus seda asja läbi kuulaks ja süialust temale seda kahju ära maksma sunniks.
An Köwerjalg (käemees Tönnis Bachmann) sai ette kutsutud kes esiteks salgas, wiimaks aga tunistas seda kleiti aga mitte neid suki warastanud olewad.
möistetud:
An Köwerjalg peab sele warastud kleidi eest 5 Rbl. ühe kuu aea sees Roman Mühlbergile wäljamaksma ja saab warguse eest 24 tunni peale wangi pantud.
