Peter Takel, kes enem mitmekordse siitse kohto käso wasto pannud ning mitte kohto juure tulnud ei ka kohtu otsusid täitnud ega oma pearaha wölgasid wähendanud, -sai nüid ette kutsutud ning tema käest sele üle seletust nöutud, kes aga mingisugust öiguseliko wabandust ei wöinud ette anda, ning sai
möistetud:
Peter Takel käso, kui ka kohto otsuste täitmata jätmise üle, 10 witsa löögiga trahwida ning peab oma pearaha wölga, mis 24 R 33 kop. suur, sel wiisil äratasuma, et ita järeltulewa kuu sees tänaselt päewalt arwata 5 R äramaksab, kuni see wölg täitsa kustutud saab.
Peakohtomees: J. Kusik (allkiri)
Kohtomees: A. Ekbaum (allkiri)
Kohtomees: J. Org XXX
Kirjutaja: Köhler (allkiri)
