Märt Puusep tulli ette ja kaebas, et tema oma poeg Gusta temalt 25 R raha ärawarastanud ning Otto Bandlowi kätte andnud, kes ütlemise järele seda raha ärakaotanud olla, arwab aga kaebaja, et Otto Bandlow seda raha oma wanemate kätte on andnud ning palub et kohus seda asja läbi kuulaks ja süialust temale seda raha wäljamaksma sunniks.
Gusta Puusep (12 aastad wana käem. Märt Puusep) sai ette kutsutud kes ütles, et tema seda ette antud raha Otto Bandlowi jätte andnud kes sele eest temale ühe raamato ja muid asjo pidanud ostma, kui ka 2 höberubla tüki nöndasama Otto Bandlowi kätte andnud, kes sele wasto oma ema käest paberi raha toonud ning nemad sele eest kompwekisid ostnud.
Jüri Waldow sai ette kutsutud kes ütles, et Otto Bandlow isi temale könelenud, et seda Gusta Puusepa käest saadud 25 Rbl. oma wenna Johan Bandlowi kätte olla andnud.
Rein Saar sai ette kutsutud kes ütles et tema näinud kui Johan Bandlow üht 25 R rublast raha, körtsis wahetada tahtnud aga kele omadus see raha olnud ei teada tema.
Johan Bandlow (16 aastad wana) sai ette kutsutud kes ütles et tema mitto kord 25 rublalesi rahasid körtsis ära wahetanud keda oma isa Johan Bandlowi käes sele tarwis saanud, aga oma wenna Otto käest mingisugust raha saanud.
Ema Ano Bandlow sai ette kutsutud kes ütles, et tema oma poea Otto käest mingisugust raha pole saanud ega selest ühtegi ei teada.
Isa Johan Bandlow sai sele üle küsitud kes nöndasama ütles enast sele ette antud rahast ühtegi teadwad.
Otto Bandlow (10 aastad wanna käem. isa Joh. Bandlow) sai ette kutsutud kes ütles et tema kül Gusta Puusepa käest üks 25 Rbl. raha tük saanud see aga temalt ära kadunud, ning need höbe rubla tukid olla Gusta Puusep isi kaupmehe Tieto juures ärawahetanud, tema ka kelegile sele rahast teistwiisi könelenud.
Kohus arwas senna, et seles asjas mingisugust muud tunnistust ei ole et see ette antud 25 R kusakil ärawarjätud ehk antud siis Gusta Puusep ja Otto Bandlow üksi seles asjas süialuseks arwata ning sai
möistetud:
Gusta Puusep ja Otto Bandlow peawad seda 25 R kumbagi 12 R 50 kop. sel aeal kui teenistusele kölblikud, Märt Puusepale wäljamaksma.
