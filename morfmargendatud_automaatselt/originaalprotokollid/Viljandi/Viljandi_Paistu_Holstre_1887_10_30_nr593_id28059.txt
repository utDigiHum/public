Mihkel Pulleritsu ja Ado Karu wastastiku kaebdustes, olid mõlemad kohtukäiad ette tulnud.
Ado Karu andis ette et ta sellest ühtegi ei teadwat, et tema kaebdus Mihkel Pulleritsu wastu Kriminal kohtu juures lõpetatud on, sest et temal olewat seletatud ei olewat.
Mihkel Pullerits wastab sellepeale et temale kül otsus kuulutud ja tema priiks mõistetud´saanud.
Mõisteti: Mihkel Pullerits peab selle oma ütteluse tõenduseks Keiserliku Perno Maakohtust ära kirja tooma.
Eesseiswa mõistus sai kohtukäiatele kuulutud.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JTill [allkiri]
Kirjut Raudsepp [allkiri]
