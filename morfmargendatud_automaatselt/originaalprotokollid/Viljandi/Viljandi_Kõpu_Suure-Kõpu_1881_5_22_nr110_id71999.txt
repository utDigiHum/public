Jaan Tuisk (käem. Peet Rosenberg) tulli ette ja palus, et kohus tema ära surnud poea warandust, mis siin kohtu juures 6 Aprilil s.a. No 87 344 R 20 Cop. peale üles wöetud, nüid kus tema poea laps ära surnud, lesk Mari Tuisko temale wälja andma sunniks.
Lesk Mari Tuisk (käem. Joh. Rütel) sai ette kutsutud kes ütles, et tema arwates see tema mehest järele jäänud warandus tema omaduseks jäeb ning selest wähe Jaan Tuisole wöib wälja anda.
Möistetud:
Talurahwa seadus § 994 järele jäeb pool osa sele nimetud 344 R 20 Cp. warandusest, lesk Mari ja teine pool ära surnud Jaan isa Jaan Tuisole perida kus siis Jaan Tuisk köiki seda üles takseritud kraami enesele peab ning sele eest 172 R 20 Cop. lese Mari Tuisole wäljamaksab.
