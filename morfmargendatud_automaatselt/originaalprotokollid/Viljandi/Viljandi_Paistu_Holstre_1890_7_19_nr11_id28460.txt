Jakob Jaago poeg Kaarti kaebduses Johan Kristali wastu olewad mõlemad kohtukäiad tänasel päewal ilmunud.-
Kaebduse protokoll sai loetud ja wastas selle peale Johann Kristal: Tema olewat Jüripäewal 1890 ise säält koha pealt äraläinud ja naene ning lapsed olewat weel sääl.- Naene ei minewat wälja sest et see kohast wälja ajamine mille säädusline ei olewat. Aia põletamisest tema ei teadwat ja üüri raha tema ei wõiwat maksta.- Temal ei olewat wõimalik oma naest säält kohast wälja wiia.
Et tunnistajat ega Johann Kristali naest siin ei ole sellepärast
Mõisteti: Kohtukäiad tunnistajadega ning Mall Kristal ettekutsuda
