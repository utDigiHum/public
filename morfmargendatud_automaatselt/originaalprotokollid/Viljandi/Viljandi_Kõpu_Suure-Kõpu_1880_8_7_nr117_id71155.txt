Märt Pöder (käem. Michel Kull) tulli ette ja kaebas, et Michel Glück teda peksnud ja palub kaebaja, et kohus peksjad trahwiks.
Michel Glück sai ette kutsutud, kes ütles, et tema mitte Märt Pödra peksnud kül aga ühte rihma, mis ärakadunud taga otsinud ja Märt Pödra käest küsinud.
Möistetud:
Kaebajad, ilma mingisuguse tunnistuseta rahule sundida.
