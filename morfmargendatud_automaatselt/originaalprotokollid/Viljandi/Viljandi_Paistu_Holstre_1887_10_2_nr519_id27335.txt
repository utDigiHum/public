Jaan Peet kaebduses Jaan Johannsoni wastu, selle naese toitmise kulude nõudmise pärast olid mõlemad kohtukäiad ettetulnud. Tunnistaja Hans Allast oli tulemata jäänud.
Jaan Johannson wastab sellesinatse kaebduse peale et tema koguniste mitte käskinud ei ole oma naist ära minna, waid käskinud seda üht tallitajat tuua. Aga naest ei luba ta nüüd mitte enam tagasi wõtta. Koos wara ei ole naene mehele minnes ühtegit ühes toonud.
Mõisteti: Kohtukäiad ühes tunnistajaga uuesti tellida.
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
                        JJaama [allkiri]
                        JAndresson [allkiri]
Kirjut Raudsepp [allkiri]
