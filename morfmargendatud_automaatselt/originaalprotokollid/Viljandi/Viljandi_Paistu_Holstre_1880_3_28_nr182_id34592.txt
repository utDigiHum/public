Ette tuli wenekiriku maade rentnik Jaak Weiber ja kaebab et kirikumaade mõtsast Johann Koortmann, Willandi mõisast warastanud on ja nimelt saega;
1 kast 8 sülda pitk 13 tolli jäme
1    "    7    "         "   11     "      "
Johann Koortmann ettekutsutud wastab et ta need puud tee äärest maast leidnud ja oma ree peale pannud on aga neid mitte ise maha lõikanud ei ole sest et et temal saagi ega kirwest ühes ei olnud. Et nüüd need warastatud puud Johann Koortmanni käest leitud saanud sellepärast saab tema kui nende waras arwatud ning
Mõisteti: Johann Koortmann peab 6 Rubla trahwi Jaak Weibri kätte ja 90 koppikat mitme korra wälja jäämikse trahwi sia kogkohtu kätte ära maksma.
Eesseiswa mõistus sai kuulutatud ja kohtukäiad Appellationi Formaliadega tutwustatud.
