Holstres 23 Septembril 1887.
Ette tuli Epp Nirk ühes nõumehega Johann Markus ja kaebab et tema surnud Frommhold Balzarile laenuks annud 60 kubu õlgi aga ei ole neid õlgi kätte saanud. Ka olewat tema 17 aastase pojal Johann Nirgil 3 amed/Särki saamata kelle eest ta tükki pealt 75 koppikat nõuab. Õlgede eest 20 koppikut kuo pealt.
Mõisteti: Assja toimetusele wõtta.
Kogkirjut Raudsepp [allkiri] ad mand.
