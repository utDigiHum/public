Siitpoolse protokolli täienduseks 9" Juulist 1887 No 375 olid tänasel päewal siia kogukonna kohtu ette tulnud: kaebaja Ann Koni ühes mehega Enn Koni ja kaebatud Hans ja Reet Ritson. 
Ann Koni kaebduse protokoll sai ette loetud ja ei olnud temal enam ühtegi senna juure lisada. Hans ja Reet Ritson wastawad, et nemad Ann Koni peksnud ei ole ega ka mitte juukseid kisknud ei ole.
Tunnistaja Jaan Sepp ei olnud täna ette tulnud. Ann Koni ütleb aga et Jaan Sepp muud ühtegi tunnistada ei teadwat kui ainult seda et tema süli juukseid täis on olnud. Muid tunnistajaid ei olnud kaebajatel ette anda.
Et nüüd Jaan Sepp sellest tülist ühtegi näinud ei ole, sellepärast arwati teda ettekutsumata jätta ning sai Mõistetud:
Ann Koni kaebdust Hans ja Reet Ritsoni wastu  kui põhjendamata tühjaks arwata.
Eesseiswa mõistes sai kohtu käiatele kuulutatud ja Appellatsoni formaliad tutwustatud.
Peakohtumees JLuik
Kohtumehed: JAndresson
                         MHansen
