Kohtuse sai kutsutud Madli Hunt ja järrel kulatud miks pärrast ta kohhut olli sõimanud ni sugguste häbbemata sannadega - et kohtuwannem tulgo ja köhhigo tema P... kui ta lubba annab agga kui  mitte siis ei woi köhhida. 2 weel et Madli Hunt on walla koli maia jurest haggo warrastanud 7 kubbo öösel.
Se häbbemata sannade ja se warguse eest mõistis kohhus tedda 2 päwa puuri wangi trahwi (48 tundi) mudo olleks ta witsa saanud agga ta wigga pärrast jäi ta ilma.
Teiseks sai weel Madli Janimägi kohto ette kutsutud et ta kohut olli sõimanud - se eest et kohhus tema palga olli se wargusse eest trahwinud wata N6. Kohto ette tulli Madli Janimägi. Kohhus kuulas temmalt et ka sa tohhid kohhut nenda rummalas sõimata et minno poia perse kannikad on kohto mehhe nurga kiwwid kelle naial ta seisab, ja weel et kohto wannem on hullo koera sarnane kes rumalast peast keigi kallale jookseb.
Se eest mõistis kohhus tedda 2 päwa wangi trahwi agga kui ta sedda kulis ja natuke aega weel otata kästi, jooksis temma ärra agga tallitaja läks warsti järrel agga tema ep olle mitte tulnud waid on ütelnud tulgo kohto wannem minno järrel, agga kohto wannem läkkitas oma 2 kõrwa meest siis on ta ütelnud minna ei tulle ehk wõtke mo pea otsast.
Se eest et tema järrel kaks kord olli käidud ja ei olle mitte tulnud mõistis kohhus weel 1 pääw jure et peab 3 pääwa ollema.
täidetud sel 29/3 75
Koggukona kohto wannem: Juhan Korw XXX
kõrwamees Jürri Stein XXX
Jaan Mähkülla XXX
