2.
Rieseperre walla kohhus olli kohto korra pärrast walla kohtomaiase kokko tulnud, kohto ette astus Nömme Jakob Ilau ja ütleb omma kohha ülles, sest et temma waesusse pärrast sedda ennan ei jöua piddada.
3.
Kohto ette astus Rieseperre möisa kubjas, Wallitseja herra Feldingi nimmel, ja ütleb: Nömme Jakob Ilaul on 32 1/2teu pääwa wölgo a 40 kop pä, ja et temma nüüd ennam teule ei tulle, sellepärrast pallun minna kohhut tedda sundida et ta need päwad ärramaksaks, kas tööga ehk rahhaga.
Kohhus möistis Jakob Ilau peab need päwad mis temmal wölgo on, Jürri päwaks ärra maksma.
4.
2, Annab Rieseperre moisa wallitsus ülles, et Tuliko s. Tönno Klarusel 20 teu päwa wölgo on.
Tuliko s.Tönno Klarus astub kohto ette, ja ütleb et temmal 11. päwa wölgo on, ja et temma selle aia on haige olnud ja lubbab need pääwad Jürri päwaks ärra maksta.
Kohhus möistis need päwad möisa wallitsussega selletada.
Kohtowanem: Jaan Sarik xxxKörwamehhed: Jürri Simson [Allkiri]Jaan Lehbert xxx
Kirjutaja R Wiek [Allkiri]
