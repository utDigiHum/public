Astus ette Prits Kress ja kaebas et Hans Pluumberg olla temma poega lönud üks hoop pähha ja pärrast olla Pritsu omma habbemest kinni akkanud, ja olla öölnud Pritsu poeg olla sead kambre lasnud ja temma leiva kannika lasnud ärra söia.
Agga Pritsu poeg ütleb et temma polle sead mitte kambre lasnud ja Hans Bluumbergil polle selle ülle ühtigi tunnistust, kes need sead kambre lasnud waid ilmaaegu Pritsu ja temma poega lönud.
Selle peale moistis kohhus et Hans Bluumberg maksab Prits Kressi ja temma poia lömise ette 1 Rbl 50 Cop walla laeka: agga polnud rahha maksta ja sai 15 hopi kätte antud.
