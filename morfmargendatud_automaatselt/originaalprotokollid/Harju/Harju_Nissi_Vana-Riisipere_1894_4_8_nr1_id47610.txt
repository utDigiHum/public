1894 goda aprelja 8 dnja
Ilmusid Riisepere walla kohtusse Riiseperest Kai Ilau ja Ellamaalt Kustas Mursaka lesk Wiio Mursak ja antsid üles et Kustas Murask ära on surnud kellel Kai Ilaule /200/ kaks sada rubla maksta on ning palusid järgmist suusõnaliku lepingud Riisepere walla kohtu akti raamatusse kirjutada.
See raha mis Kai Ilaul /200/ kahe saea rubla suuruses summas surnud Kustas Muraski käest siit walla kohtu actiraaatu No1 järele 1892 aastast järele saada on jäeb edespidi Kustas Muraski Lesk Wiio Muraski kätte nende sama tingimistega kui enne nimelt:
Wiio Murask maksab selle raha eest Kai Ilaule 5% aastas, ning selle kapitali maksab tema Kai Ilaule nelja aasta jooksul wälja see on 1898 aastal. Kui aga Wiio Murask oma surnud mehe päralt olewa talu koha mis on Soniste wallas Pakita nime all ära tahab müia siis peab selle wõla enne ära maksma
Et Kai Ilau ise kirjutada ei oska siis kirjutas tema palwe peale alla.
J.Tamberg [allkiri]
Wio Mursak [allkiri]
Riisepere walla kohtu poolt saab see läbi tunnistud et see leping Kai Ilaule ja Wio Muraskile ette on loetud kes mõlemad walla kohtule tutawad on ja et neil õigus on omawahel lepingid teha, ning et Kai Ilau palwe peale J.Tamberg ja Wio Murask oma käega nimed alla on kirjutanud
Kohtu Eesistuja J.Sesmiin [allkiri]
Liikmed H Gefria [allkiri]
J.Linroos [allkiri]
J.Simson [allkiri]
Kirjutaja MOrgusar [allkiri]
Po sei sdelki /200/ dvesti rublei ot Vio Murakasa polutsila 
Maja 12 dnja 1895 goda
Kai Ilau XXX
Predsetatel Suda Ja Sesmin [allkiri]
Pisar Orgusar [allkiri]
