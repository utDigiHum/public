Kohtu ette tuli kaebama mõisa kutsar Willem Petoffer ja ütles: Pühapääwal tulin Pridik Andermanniga linnast Mustjõele ja jõime paar pudelit õlut. P. Andermann ja wälja ja tuli kohe tagasi ja ütles "Ma sain peksta!" Wiie Juhan ja Kristjaani Juhan tulid kohe järele ja kippusid tuuas uueste P. Andermanni kallale. Mina lõin Wiie Juhan'ile wastu rindu ja kui tema teistkord kippus, siis lõin talle wastu silmi. Selle peale wiskas neid kõrtsmik neid molemid tuast wälja. Mina kutsusin Pridik Andermanni mõisa juure. Õhtu läksin teda kodu saatma. Mererandas tuliwad wiiekeste meile wasta ja Wiie Juhan tuli kõige ees ja küsis Pridik Andermann'ilt: "Kus sa kurat lähed?" Kristjaani Juhan wõttis kiwi ja tahtis lüa. Mina jooksin wasta ja plaks käis mulle wasta silmi, mis seal pärast sündis, ei tea ma mitte. Silm oli küll tugewa hoobi saanud.
2. Pridik Andermann tunnistas nii: Kui ma Mustjõe kõrtsust wälja läksin seisid Wiie Juhan ja Kristjaani Juhan silla peal. Mina läksin kooja poole ja nii pea kui silla peale sain, lõi Kristjaani Juhan mulle wastu silmi 3 korda ja Wiie Juhan ühe korra; ma jooksin kõrtsu tagasi. Kõrtsu sees kippusid jälle minu kallale; aga kõrtsmik ajas neid tuast wälja. Mererandas, kui õhtul kodu läksin, tulid Kristjaani Juhan ja Wiie Juhan wasta lõid minu maha ja kui ma maast üles tõusin, pistsin jooksma, siis jooksid mitmekeste mo järele ja peksid mind kraawis: Wiie Juhan, Kristjaani Juhan, Peter Kastan ja Kustas Kruusiauk. Pea oli PridikAndermannil mitmest kohast haawatud.
3. Wiie Juhan Saarmann tunnistas: Mina põle Pridik Andermann'i Mustjõel puutunud, tema lükkas mind üle aja. Mererandas lõi P. Andermann mind kiwiga pähe
4. Kristjaani Juhan Jerwan tunnistas, et Pridik Andermann lõi Juhan Saarmanni enne.
5 Peter Kastan, tunnistas: Priidik Andermann lõi mulle paar pattakast, ilma süüta ja kui pärast hunnikus olid siis ma lõin teda ka.
6. Kustas Kruusiauk tunnistas, Mustjõel mina ei tea ühtigi, mis seal oli olnud. Mererandas tuli P. Andermann ja Willem Petoffer wastu ja P. Andermann'il oli kiwi käes ja lõi mulle pähe, kui ma küsisin, mis eest see oli pistis tema jooksma, järele jookstes sain teda kätte ja andsin temale mõned korrad, aga selle rumala tembu eest oleks temale tarwis olnud enam anda.
Kustas Kruusiaugu peas oli haaw näha.
Kohus mõisitis: 1, Peter Kastan ja Kustas Kruusiauk on süüst lahti arwatud sest et nemad mitte põle tüli alustanud.
2, Willem Petoffer ja Pridik Andermann ei ole midagi pärimist, oma haawade ette, sest et nemad on ise tuli otsijad olnud.
3. Juhan Jerwan ja Juhan Saarmann, kui tutwad tüli ja riio tegijad makswad trahwi Juhan Jerwan 2 rubla ja Juhan Saarmann 1 rubla 50 kop.
Kohtowanem Gottleb Torbek &lt;allkiri&gt;
Kõrwamees Willem Andermann &lt;allkiri&gt;
Lisamees     Hindrik Berg  &lt;allkiri&gt;
Kirjutaja Joh Tasson &lt;allkiri&gt;
Trahw makstud.
