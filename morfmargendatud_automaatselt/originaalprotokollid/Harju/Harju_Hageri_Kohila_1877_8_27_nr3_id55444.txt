Astus ette Sallutaguselt Allesti perremees ja kaibas et Pahkla moisa teomees Hans Flus wõtnud temma sälla metsast kinni ja temma leinud temma kojo järrel agga ta ei andnud kätte waid, ütles se on minnu sälg ja hagand tedda weel pealegi sõimama.
Ette astus Hans Plus ja ütles egga ma tundnud, ma mõtlessin omma Sällu ollema mul olli ka ni samma suggune.
Tunnistusse mees hundiratta peremees Jaan Wiil ütles minna tunnen se on tõesti Allesti perremehhe Sälg.
Kohhos mõistis et Hans Plus maksab 1 (üks) rub Allesti perremehhel selle eest et ta sälgu kohhe kätte ei and, kui ta järrel käis.
