Kogukonna kooli maease ollid kokku tulnud Wollimehed Magasinist wilja laenamise pärrast.
Nõuks sai peetud, etigga kue päwa mehhel wõib sada antud			kuus 6 Tsetwriku Rukid			ja sui wilja niisamma paljo;		ja kolmepäwa mehhele			3 Tsetweriki Rukid			ja sui wilja sesamma jäggu;		ja kahhepäwa mehhele			2 Tsetwerik Rukkid			ja sui wilja nendasammuli;		Agga jalla päwa meestel peab sama arwatud			üks pool Tsetwerki rukid			ja üks pool Tsetweriki sui wilja.		
Ja täie perremeestele wõib sada rohkem sui aeal antud, kellel toeste tarwis on, agga kellel wannad wollad ees on, neile ei wõi ülle selle lubbatud arro mitte anda.
Seda kinnitawad koggokonna nimmel
wollimehed Tõnno Sesmin XXX
Mart Roswalt XXXMart Miller XXXJakob Meinwald XXXJürrij Bekman XXXabbimehed Juhan Simson XXXHans Waltsnep XXXTallitaja Mart Meinwalt [allkiri]
