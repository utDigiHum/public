Kohto ette tulli Saida mõisa kubjas Jaan Jaansohn ja kaebab kohto ees, et Söedi Willema naene Marri Petoffer ennesa poea Jürriga kaera tündrima leikuse aeal mind sõimas, lolliks ja rummalaks, et kulluks kolm kulbi täit kubja sitta jure weel, siis wast wõiks kubjas sada ja weel muid sõnno, mis üks mõistlik innimenne ei tahha suhhu wõtta.
Kohtu ette astusid Jürri Petoffer ja Marri Petoffer; Sai kohto poolt küssitud; miks teie Saida kubjast sõimasite? Ei meie põlle keddagi sõimanud. Agga sedda asja olli pealt kulanud Ado Waltsnep ja Mart Lindmann ja ütlewad: et naad on sõimanud. Ja kohto ees on naad omma õigust täis surelised, et neid peaks kartma, sepärrast arwab kohhus, et kubjal on õigus.
Kohto otsus: Kohhus mõistis, et Jürri Petoffer peab maksma 3 rubla ja Marri Petoffer peab maksma 3 rubla trahwi walla laekasse.
Kohto peawannem Jaan Willenthal XXXKõrwa mehhed Jürri Pressmann XXXJuhan Meinwald XXXKirjutaja J.Arnak [allkiri]
Kohus täidetud 8 Nowemb 1880
