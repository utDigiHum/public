Astus kohtu ette Sõrwe metsawaht Juhan Kursmann, Harku mõisa walitsuse käsu peale ja kaebas, et karja poisid, Wanaaugu Otto poeg Juri Annus ja Pimeda Juhani poeg Karel Prunsfeldt molemas 10 aastad wana panid mõisa metsas 4 sülda hagu ja 1 1/2 sülda puid põlema, ja mõisa walitsus nõuab, et poisid peawad karistud saama.
Ette sai kutsutud Wanaaugu Otto Annus oma poea Juriga ja Pimeda Juhani naene Anu Prunsfeldt oma poea Karlega ja sai nendele kaeptus ette loetud; Poisid tunnistasid ennast süüdlaseks, nemad olla tahtnud pärast rumalast peast haud põlema panud. Juri Annus ütles, et Karel Prunsfelt pani põlema, ja Karel prunsfeldt kinnitas, et Jüri Annusel oli piip ja tuletikud kaasas ja käskis teda panna.
Kohtu otsus: Poisid Jüri Annus ja Karel Prunsfeldt saawad kumpki 10 witsa hoopi ihu nuhtlust oma wanemate kää läbi kes neid kohtu otsuse järel kohtumaja tallis peawad karistama, miks naad nii koerad on, ja tulega nii ette waatmata ümber käiwad ja meelega kahju tewad.
Kohtumees: Karel Annus XXX
Korwasmehed R. Korsen XXX
                          Karel Kruusmann​​​​​​​ XXX
Said oma trahwi kätte seal samas.
