Harku Hüüru walla kohus oli täna 25 Nowembril 1889 Kadaka külas, Hans Tusari ja Hindrik Pillarti palwe peale, nende koku ostetud Kopli talu koha peal olewad metsa ja Rehe hoonet takseerimas.
Walla kohus luges Kopli koha peal olewad puud ära neid oli Karjasmaa peal Hans Tusari poole peal 60 jämedad ja 172 peenemad männapuud, ja Hindrik Pillardi poole peal 40 jämedad ja 27 peenemad männa puud. Sellest oli Hans Tusar kohustus Hindrik Pillartile 10 jämedad ja 72 peenemad puud tagasi maksma.
Hans Tusari nõudmise peale sai ka heinama pealt puud üle loetud mis juba 20 aasta eest pooleks oli wõetud. Heinamaa pääl oli Hindrik Pillarti poole peal 22 leppa puud rohkem, sellest pidi Hans Tusar 11 leppa omale saama, kohtumehed antsid nou et peawad ära leppima ja nemad lubasid sellega leppitad Hans Tusar annab oma poole koha pealt 2 jämedad mända Hindrik Pillertile, ja 8 manda jätab nende 11 leppa ette, ja need 72 peenemad jääwad Hans Tusari kahjuks, miks Hindrik Pillart juba enne selle heinamaa pealt muist leppapuid oli maha raiunud.
Rehe hoone sai 90 Rbl. alla takseeritud, sellest maksab Hindrik Pillart Hans Tusarille 40 Rbl. ja annab need kaks mända tagasi, kui nemad sellega kokku leppiwad, siis peawad 12 Decembril Harku walla kohtus seda kinitama.
Kohtumees: Karel Annus XXX
korwasmehed: R Korsen XXX
                           K Kruusmann XXX
Kirj: G. Kollip &lt;allkiri&gt;
