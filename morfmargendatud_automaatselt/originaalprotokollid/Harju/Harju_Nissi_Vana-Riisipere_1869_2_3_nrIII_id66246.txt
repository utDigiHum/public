1) a tulli ette kässu järrel Orro Jürri Eisman nenda kui se 22 Janoaril s.a. olli kästud ja tunnistas et sest rahhast se 25 Rublane babber on ka enne wahhetamest monne näddala Karja Jürri naese Anno juurdes seisnud ja ütles et anno ei olle mitte teadnud kus tu olnud waid temma on isse arra pannud kambre ühhe tendre wahhele
b tulli ette Orro Jakob Wilman selle Rahha näggija ja 25 Rublawahhetaja
d tulli ette Sureba Jakob Suharow sele rahhanägija keska nouannud sedda rahha sallaja ommale biddada
e toodi ette Sureba Jakob Paus ka selle rahha naggija kes ka nou annud seddaommale salla biddada
g toodi ette Jäätma Hans Eisma kes sedda 10 Rubla oidja olli mis ta käest ärra toodi
h toodi ette Matsu Tõnnu Kuutman kellekaes 5 Rubla hoida oli ja tunnistas et se rahha alles kolme päwa eest temma kätte olli antud
i kutsuti ette Karjatse Jürri naene Anno Enoh ja waid  les kankeste wasto et se rahha ei olle mitte temma kätte sanud et Jürri on kül seälkäinud ja ka nende kambres käinud ja räkinud et temma on lada aial rahha maast leidnud 3 Rubla 34 koppike ( sest se Jürri Eisman on Anno Õepoeg) ja temma olnud se kord üsna aige ja kui rahha seält arra winud on Jakob Wilman ka temmaga olnud ja  Jürri on jälle üksi kambres käinud sedda tunnistas ka Jakob Wilman
k kohhus moistis trahwid neile keigile
a Jürri Eisman peab keik sedda leitud rahha kolmkümmend kaks Rubla 75 koppike walja maksma kellest nüüd jubba 17 Rubla 60 koppike käes on
weel maksta 15 Rubla 15 kop wõlga jaTrahwi rahha walla laeka			3 Rubla		b Orro Jakob Wilman trahwi			3 "		d Sureba Jakob Suharow "			3 "		e Sureba Jakob Paus "			3 "		g Jäätma Hans 10 Rubla hoidja			2 "		h Matsu Tonnu Kuutman wiis Rubla hoidja trahwi			1 "		i Karja Jürri naine Anno kelle maias rahha seisnud ja mitte üsna selgeste teada ei sanud kas se kaks kümmend wiis rubla on ta käes olnud wai isse sinna pannud			1 "		Summa			16 Rubla		
Sest kuus teist Rublast saab antud 
a selle essimesse üllese andjale nenda kui kõrts mik on lubbanud 5 Rubla
b Kõrtsmik 2 päwa kohtus käima eest 1 Rubla ja et se rahhapärrija on ikke üttelnud kusserahha maial on kui kõrsmiko kääs moistis kohhus selle kannatamise ja hea järrel kulamiseeest 2 Rubla agga körsmik Juhhanes Dunkel lubbas sedda kaks Rubla kirriko waestele ja päwade palk 1 Rubla walla laeka heaks anda
d Walla laeka heaks jääb 8 Rubla obbedad 
Peawannem Otto Meldorff
korwa mees Hans Einbach
Lissamees Jürri Nei [allkiri]
