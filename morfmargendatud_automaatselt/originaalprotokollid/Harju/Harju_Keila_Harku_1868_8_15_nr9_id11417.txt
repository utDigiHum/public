Rannamõisas sel 15. Augustil 1868.
Koos ollid Kohtum. Karel  Trauberg Karel Luker ja Juhann Lipp
Ette tulli kaebaja (Mutti Trahteri isand) Ratkowitš ja kaebab, et Karel Paemurd ja Karel Kaskirilohw on 28 Julil (1868) pühhapäwa õhtu kella 10ne aeal Kõrtsis tüllitsenud ja selle läbbi kolm tuhja puddelid 2 õlle ja 1 wina klaas katki läinud ja et naad tulle on ära kustutanud.
Tülli on sest tulnud, et K. Paemurd on 1/2 kortert wina wõtnud, ja ütleb et ta on maksnud, agga kõrtsi Issand ütleb et ta põlle maksnud ja on ka weel teineteist sõimanud.
Agga Karel Paemurd tunnistab, et temma on 5 koppikut selle 1/2 kortri wina eest annud ja kõrtsi Issand on veel 1/2 koppikusse timp saia taggasi annud. Ja et nemmad mitte põllle tuld ärra kustutanud egga puddelisi ja klasisi katki teinud, waid kõrtsiemmand. Temma (kõrtsiemmand) on letti ukse kinni tõmbanud ja se läbbi tulli ärra kustunud ja ka puddelid ja klasid mahha põrrunud ning katki läinud. 
Seddasamma tunnistas ka Karel Kaskirilohw.
Sai Mõistetud, et mehhed, omma õppetusseks et teine kord ennam kõrtsi ei lähhe, peawad trahwi maksma Karel Paemurd 1 rubla hõbbedad ja Karel Kaskirilohw 1/2 rbla hõbbedad walla laekasse, mis ka pea makstud sai. Ja kõrtsi Issand piddi issi omma kahju kandma, sellepärrast, et pühhapäwa õhtu kella 10 weel kõrtsi tohtise lahti piddada.
Karel Trauberg XXX K. Luker xxx J. Lipp xxx
Kirjutaja M. Kehrt &lt;allkiri&gt;
