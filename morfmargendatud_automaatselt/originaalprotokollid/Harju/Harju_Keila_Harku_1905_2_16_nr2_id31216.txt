Üks tuhat üheksa sada wiiendamal aastal Webruari kuu kuueteistkümnemal pääwal ilmus Harku walla kohtusse kohtule isiklikult tuntud lesk Mari Anderman 84 a wana, elab Harku wallas, ja palus järgmist suu sõnalist testamenti kohtu akti raamatusse sisse kirjutada:
Minu warandus on praegusel ajal weel üks weike elumaja Lehmja mõisa krundi peal, umbes 50 rub wäärtuses /wiiskümmend rub/ selle maja tõutan ja luban pärast minu surma minu tütri Mari Sudtile (sünd. Anderman), kes minu eest wanaduses on hoolitsenud ja nueret pidanud, kuna minu tütre Leenal, kes sündimisest saadik tumm on, õigus jääb selles majas tema surmani korteri saada. Minu poeg Ado kes Tallinnas elab eo wõi pärast minu surma sellest majast midagi pärida sest et tema minu eest ei ole hoolitsnud.
Mari Aderman ei oska kirja tema palwe peale kirjutas alla Einrich Multer
Eel sisaw testament on pärast akti raamatusse kirjutamist Mari Andermannile ette loetud, kes terwe mõistuse juures oli ja tema palwe peale kirja mitte oskamise pärast Harku walla talupoja Heinrich Multeri poolt allakirjutud.
Kohtu President M. Jerwan &lt;allkiri&gt;
Liikmed J. Wäljamäe  &lt;allkiri&gt;
               O. Reinde &lt;allkiri&gt;
               H. Meri &lt;allkir&gt;
Kirjutaja J. Rammus &lt;allkiri&gt;
