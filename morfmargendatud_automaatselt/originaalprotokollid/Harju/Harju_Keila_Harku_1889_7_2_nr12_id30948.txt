Astus kohtu ette Awiku Kristjan Humberg ja kaebas, tema andis Juhanese Hindrik Sarapuule wiis palki 3 1/2 sülda pikkad ja 7 tolli latwast jäme, Hindrik Sarapuu lubas need palgid aasta pärast kätte maksta ja nüüd ei taha tema lesk neid palkisid ära maksta.
Hindrik Sarapuu lesk Anna sai ette kutsutud, tema tunnistas  seda kaeptust tõeks, ja lubas need palgid ära maksta, kas rahaga ehk toob talwel palgid kätte, ja leppisid sellega kohtu ees, kas wiis palki, ehk maksab wiis rubla Jüripääwaks 1889 a.
Kohtumees: Karel Annus XXX
                      Reinhold Korsen XXX
                      Kustas Kruusmann XXX
