Ette tuli kaebaja Kalajagu wana isa Jakob Wisart ja kaebas, et tema naene on ära surnud ja poeg Alexander ei anna taale mitte tema riided kätte, mis ema kääs olnud:
Taal olnud 1 palito, 1 riidega kasukas, 1 paar saapaid, 2 paari sokisid ja särkisid ei ole taal praegu ühe ainustgi ja 1 püs, peale selle hõbe hasjad: 6 supi lusikat, 5 tee lusikat 3 laste lusikat, 1 sukru toos, 2 soola toosi, 1 tubaka toos, 2 Wenne Jumala Kuiu, kõik wana hõbedast, mis nemad endise Mutti trahteri pidaja Rakowitsi kääst pandiks saanud 80 rubla eest ja 3 kuld tukatid, peale selle weel 1 hobone, 1 lehm, 1 raud wanker, kõik need asjad olla ta naese kääs olnud ja jäänud poea kätte.
Ette sai kutsutud poeg Alexander Wisart ja tunnistas waga suerelise meelega et Isal tema kääst midagi ei ole pärida.
Kohus pakus neile lepimist aga poeg A Wisart kostis et temal lepimist ei ole.
Asi jäi teiseks koraks mitme üle kuulamise pärast.
Kohtuwanem J Kallas XXX
Kõrwasmehed P Reining XXX ja W. Karik XXX
Kirjutaja M Käärt &lt;allkiri&gt;
