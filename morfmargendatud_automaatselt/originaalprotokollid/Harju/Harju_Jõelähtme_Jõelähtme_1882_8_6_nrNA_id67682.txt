Kaebaja tüdruk Ewa Purk ütleb; et tema on oma pere sulase Jaan Roheli käest ilma süüta wiis korda pütsaga lüa sanud.
Kaebataw Jaan Rohhel ütleb: et tema pole mitte ühte hoopi lönud, muud kui tema on lähkre seest joonud, ja Ewa Purk tomband lähkre tema käest ära, ja siis on ta ka teda ühe korra lükand.
Kohus käskis neid se üle tunnistust ette tulewaks kohtopäwaks ette tua.
alla kirjutand
Kohtuwanem Jaan Kütt
Korwamehed Hans Mitt, Jürri Maisa, kirjutaja Ovir
Kaebaja Ewa Purk annab üles eta jättab oma kaebtuse maha
sel 5. Novbr 1882
