Sel tännasel päwal tullid kokko Lehheta walla Tallitajad ja wollimehhed, nou piddama selle Maggasini aitast wilja wälja laenamisse ülle, selle pärrast wõtsid nemmad nouks: Et igga kuu kohta tulleb, kuue päwa perremehhele 5 Tsetwerikud Rukkid 3 Tsetwerikud oddre ja kolmepäwa perremehhele 3 Tsetwerikud Rukkid 2 Tsetwerikud oddre, ja Jalla päwa kohtade piddajatele 2 Tsetwerikud Rukkid ja 2 Tsetwerikud oddre laenuks anda, kui sedda Lehheta moisa pölitsei poolt kinnitakse ja Keiserlikku Nissi kihhelkonna kohtu poolest lubba antakse.
Lehheta walla pölitsei nimmel 
TunnistusseksTallitaja Andres Simson XXX
wollimehhed Jürri Lindros XXX
Andres Ewart XXX
K. T.Lasmann [allkiri]
