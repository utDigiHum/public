Woormunder kaebas et Hans Fluss olla temma wasta akkanud, ja tedda sõimanud.
Astus ette Hans Fluss ja ütles. Et woormunder tulnud sinna ja üttelnud: Mina ollen Pahkla walla käest kuulnud. Et teie olla pühhapääw ni sandiste olnud. Mis peale Hans Fluss küssinud: Kes sedda kaebas: kas emma. Mis peale woormunder wastanud: Emma polle kaebanud, ma ollen sedda küllast kuulnud, Mis peale Hans Fluss öölnud: Se on häbbi assi et teie walla jutto peale tullete minno käest küssima.
Selle peale ei woinud kohhus mitte Hans Fluss trahwida.
