5.
Tuli ette: Lohult Willa Kraasija Mihkel Palu ja ütles temal olla Tõnis Määrasmäe teenistuse kauba tegemise juures tunnistusi nimelt: Jaan Polma ja Anton Laager: Need said kohtu ees ülekuulatud. 
J. Polma ütles: tema olla sell ajal M. Palu juures teenistuses olnud kui T. Määrasmae senne teenima tulnud kus tema ütelnud 50 rubla aastas palka saawat. Pärastpoole rääkinud ka tema wastu et isal olla teda kodu tarwis ja wõtnud enne aasta lõppemist teenistusest lahti.
Anton Laager tunnistas, tema olla juures olnud kui T. Määrasmae emand Sosanne Paluga teenimise kaupa teinud kus emand temale 50 rubla aastas palka lubanud.
Tõnis Määrasmae tunnistas: Tema olla kuu peale kaubelnud 4½ rubl kuus: 4 kuud ja 13 pääwa olla tema teeninud, nelja kuu eest olla 18 rubla kätte saanud, 13 pääwa raha jäänud saamata.
Tunnistustajate tõentuste M. Palu enese tunnistuse ja tema teenijate palga maksmise raamatu järele on selge et T. Määrasmae aasta peale teenistusse on kaubeltud 50 rubla aastas palka ja teenitud palk tärmini peal teenitud aja ette wälja makstud.
See peale ühendasid kohtu liikmed ja tegid see Mõistus: Et Tõnis Määrasmae walet on kohtu ees tunnistanud ja Mihkel Palu käest weel 13 pääwa raha nõuab kus need temale juba teenistuse lõpetusel wälja makstud on nii kui tema ise tunnistab. Seepärast maksab Tõnis Määrasmäe 1 rubla walla laekasse trahwi.
