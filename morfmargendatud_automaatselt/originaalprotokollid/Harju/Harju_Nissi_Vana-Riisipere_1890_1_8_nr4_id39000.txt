Ehhitamisse kaup.
Kärdi sauna Jaan Mikkow wõttab omma peale kewade 1890a. ennesele üks uus ellohoone rehhalusega, omma kulluga üllesehhitada. Palgid saawad mõisa poolt ilma rahata antud.
Temma saab selle ehhitamisse, palgi raiumisse, koorimisse ja wäddamisse ja kattuse õlgede ja kattuse teggemisse eest, aknade ja ustega tükkis - üllepea 24 rubla 60 kop.
(kakskümmend nelli rubla, kuuskümmend kopik)
Jaan Mikkow tunnistab, et selle rahhaga on keik maksetud, ja koha araandmisse aial ei ole temmal ühtegi ennam tassumist selle eest mõisa herra käest pärida
Ärrakiri sel 8mal Januari ku p. 2es Eksemplaris wäljaantud.
