Astus Kohtu ette Hans Steinberg ka kaebas, et Gustaw Rotteri Sead olla 8 tükki tema karjasmaa mis haea sees on, üles tohninud, ja ta olla Gustaw Rotterile sead kätte annud, selle tingimise all, et Rotter temale teeb kaks kartuli pääwa, ehk maksab 80 kop. sigade pealt trahwi (se on 10 kop. sea pealt). Gustaw Rotter olla esite seda trahwi lubanud, aga pärast ei tahto maksta.
Gustaw Rotter sai ette kutsutud ja temale kaeptus ette loetud, miks ta seda leppitud summa maksta ei taha. Tema waitles wastu, et ta selle pärast ei maksa, miks wõerad inimesed kopli wärawa lahti jätwad, ja nenda sead kopli peasesid, ja miks teised küla mehed temale trahwi ei maksa, kui nende loomad tema maapeal käiwad.
Kohtu Otsus: Gustaw Rotter maksab Hans Steinbergile seda 80 kop. trahwi ära, ehk teeb kaks kartuli wõtmise pääwa.
Peakohtum Jur Saalfeldt XXX
Kõrwasmehed Karel Ander XXX
                          Juhan Humberg XXX
