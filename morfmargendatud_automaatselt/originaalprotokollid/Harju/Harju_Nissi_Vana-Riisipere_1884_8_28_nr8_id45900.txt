Kohto ette astus Lepiko koha omanik Jüri Hoidorp, ja kaebas:
Et Leheta mõisa poolt olla temale ööldud:
Et tema neli härga olla mõisa kaerde sees käinud Wanaweske põllu peal; ja olla palu heinamaas ühe kuhja alla heinu maha ajanud; ja tahetud selle eest mõisa: wiis rubla trahwi saada; aga Leppiko Jüri Hoidorp ütles: Et tema seda ei luba maksta sest et kahjo seda wäärt ei olla; sai kohtumehest ja Tallitajast järel waadatud et kahjo ei ole mitte nenda suur, ja heina kuhja alla on wanaste heinu maha aetud mitte wärskeste, ja keski ei ole Leppiko härgi kuhja all näinud.
Seepärast mõistis kohus:
Talurahwa seadusest §1118 mööda trahwi: Iga härja pealt 30 koppik, neli härga kokko 1 rubl 20 koppik raha, ja kahju tasumiseks üks külimit kaero mõisale.
Tunnistuseks:Kohtu Peawanem: Kustas Waldmann XXXKõrwa mees: Jaan Petoffer. XXX" " Karel Linros. XXXKirjutaja: J. Wiil [allkiri]
