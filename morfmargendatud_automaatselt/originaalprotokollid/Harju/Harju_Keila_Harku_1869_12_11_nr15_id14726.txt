Rannamõisas sel 11 Detsemb  1869.
Koos ollid PeaKohtumees K. Trauberg
Kõrwamehhed K. Luker ja J. Lipp
Ette sai kutsutud üllewel nimmetud nr 14 W. Petermanni kaebtus P. Lensmanni wastu. Ja ka kohhus leidis asja tõe ollevad, mis K. Lamann olli tunnistanud, et W. Petermann on P. Lensmanni sõimanud ja sepärrast P. Lensmann tedda lönud.
Sai mõistetud, et W. Petermann ja P. Lensmann peawad teine teise rubla trahwi maksma. W. Petermann sellepärast, et kõrtsmikku ilma asjata on sõimanud: ja P. Lensmann sellepärast, et teist kohhe on lönud ja mitte selle peale mõttelnud, et rusika õigus kaddunud on. Trahwirahha sai ka selsammal korral makstud walla laeka heaks.
Peakohtumees K. Trauberg. XXX
Kõrwamehhed K. Luker XXX ja J Lipp XXX
Kirjutaja M. Kehrt.
