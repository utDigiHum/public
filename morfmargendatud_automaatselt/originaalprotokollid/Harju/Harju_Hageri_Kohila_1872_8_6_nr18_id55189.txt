Pahkla moisa poolt kaebati et Hans Kress olla moisa tamme põllma pistnud.
Astus ette Hans Kress ja ütles et errilasse pessa olnud selle õnes tamme sees ja temma pistnud sedda errilasse pessa põllema ja tamm akkanud sest õhkuma ja et teine külg jubba enne keik mäddanenud olnud, on terwem külg weel nattuke põllenud ja tam mahha kukkunud.
Et Hans Kress agga ühheksa aastane on, moistis kohhus et temma 5 päwa kardole wõtmisse aega moisa teeb.
