18. (wat. Nr 15. ja 17.) Mari Klapper 29a.w. Luter.usku tunnistab: 1888a. sui kaks nädalit pääle Jaanipäewa nägin mina arwata poole wersta kauguselt, kui Kassi lambad Tõnu Leinkoppi suiwilja põllul olid; mina ütlesin seda Tõnu Leinkoppi naese Marile; lambaid mina ei lugenud.
See protokoll on nendele ette loetud, nad ütlewad: See on tõsi ja õiete üles kirjutud, mis nad on rääkinud, ja et nendel ei ole enam midagi seia juurde ütelda.
Tõnu Leinkopp ei wõi mitte tunnistuse läbi tõeks teha, et Jüri Tominga lambad on kaks korda tema wilja põllul käinud.
Lepitus sai pakutud, ei wõetud wastu.
Mõistetud: prg 1118 Nr 1 järele 1856 Eestimaa talurahva Seaduses: Jüri Toomingas maksab Rabiwere walla laeka trahwi: 13 lammast 'a 15 kop = 1 rbl 95 kop, 4 weist 'a 30 kop. = 1 rbl 20 kop, Summa 3 rbl. 15 kop.
(Moistetud Rapplaringi Ülematalo rahwakohtu saadetud)
