Üks tuhat üheksa sajandal aastal Juuli kuu 26 pääwal ilmusid Harku walla kohtusse kohtule isiklikult tuntud selle walla talupojad Kristjan Timm ja Juhan Timm ja palusiwad eneste wahel tehtud suusõnalist lepingut kohtu akti raamatusse üles wõtta:
Juhan Timmil on oma wenna Kritjan Timmi käest surnud isa pärandust 300 rubla saada, selle eest Kristjan Timm, kui Torni koha omanik, mis Harku mõisa järel on, annab Torni koha järel olewa maa tüki nimega Põtkä umbes 2 tessatiini suur ja ise äralise ristikiwide wahel, Juhan Timmile jäädawaks omanduseks ja ei wõi Kristjan Timm ega tema üärijad tähendud maatükki Juhan Timmi käest edespidi milgi wiisil ilma maksuta tagasi nõuda. Kõik seaduslikud kohused ja rahalised maksud kannab terwelt Torni koha eest praegune omanik Kristjan Timm, aga maandi sillad teeb Juhan Timm.
Juhan Timm &lt;allkiri&gt;
Kristjan Timm &lt;allkiri&gt;
See leping sai pärast akti raamatusse kirjutamist pooltele ette loetud ja nende poolt alla kirjutud.
Kohtu Eesistuja M Jerwan &lt;allkiri&gt;
Liikmed J Wäljamäe &lt;allkiri&gt;
               K. Tui &lt;allkiri&gt;
               H Meri &lt;allkiri&gt;
Kirjutaja J. Rammus &lt;allkiri&gt;
копю ??
J. Timm
ärakirja wälja wõtnud 16/II 1927
K. Timm
