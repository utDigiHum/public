III)
Gustaw Weismann kaebas et Angerja wäe peremehe, see on Kadarpiku, Rätsepa, Rõngelepa, Laagre, Männiku ja Aawiku loomad on minu heina maa pealt hädalise ära söönud ja heinama ära tallanud. Mina nõuan selle ette 2 Rubla kui see asi siin seletud saab, kui see asi suuremase kohtuse läheb siis nõuan ma rohkem. Kohus küsis: Kas on keegi loomad kinni ajanud sinu heina maa pealt. Wabriku Jaan Roodi on need loomad seal näinud.
Kohtumõistmine: Et Jaan Rodi mitte kohtu ette põle tulnud, siis jäi see asi tulewa kohtu päewaks seletada.
