Astus kohtu ette Juhan Kurell ja kaebas, et Karel Tirelli aned olla tema odrad ära söönud ja tema aias aned kinni, ja Karel Tirell olla lubanud üks rubla maksta, aga nüüd ei taha oma lubamist täita, Karel Tirell sai ete kutsutud tema tunnistas, et Juhan Kurelli hobune olla mullu kord tema Odras käinud ja tema ola ilma maksuta kätte annud.
Juhan Kurell waitleb selle wastu ja kinnitab, et se koguni tõsi ei ole.
Kohtu otsus: Karel Tirell peab Juhan Kurellile üks rubla 10 hane pealt trahwi maksma kahju tasumiseks.
Peakohtumees: Juhan Humberg XXX
korwas mehed Gustav Reindes XXX
ja                       Hind Sarapuu XXX
makstud.
