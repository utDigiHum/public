1902. aastal Martsikuu 19 p. ilmusid Keila Walla Kohtusse Ohtust Ewa Ikmelt 65 aastane Jaan Ikmelti lesk ja Mari Rank 46 aastane Jaan Ranku lesk ja palusid akti raamatusse järgmine leping kinnitada
Mina Ewa Ikmelt müüsin oma elu maja Ohtu moisa /Raja/ sauna krundi peal on Mari Rankule 51 rub. ette missugune summa on kõik wälja makstud.
Kohtusse ilmunud Ohtu kogukonna talupoeg Peter Ikmelt 55 aastane tunnistas et se müüdud elu maja mis on Ohtu moisa Raja sauna krundi peal on tõesti Ewa Ikmelti omandus ja et seda nüüd on ära ostnud Mari Rank 51 rubla ette
Se leping peale akti raamatusse kirjutamist sai ette loetud ja kohtu poolt ustawaks ja õigeks tunnistud
Ülewal nimetud isikud keegi kirjutada ei oska ja panid oma käega kolm risti alla
Ewa ikmelt xxx
Mari Rank xxx
Peter Ikmelt xxx
Kohtu president T. Wäljamäe [allkiri]
Liikmed: J. Zimmer [allkiri]
                J. Wederik [allkiri]
Kirjutaja A. Almberg [allkiri]
