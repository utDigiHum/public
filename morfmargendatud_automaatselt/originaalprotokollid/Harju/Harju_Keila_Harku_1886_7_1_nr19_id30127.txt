Astus ette kaebaja Tiiri Karel Tirell ja kaebas, Saueaugu koha omanik Mart Treumann ei luba teda enam oma karjasmaast üle käia, kust Tiiri koha tee juba peale 100 aastad on käinud, ta tegi walli ette ja pani tee kinni ja tema Kaarel Tirell ei pease enam oma põllu peale.
Mart Treumann sai ette kutsutud, tema tunnistas, ta olla koha ostnud ja ei wõi lubada, et tema karjasmaa kõige küla teede all peab olema, Tiiri Karel Tirell wõib oma krundist wäga heast suure tee peale peaseda, olla paljalt 200 sülda kõwera teha, tema tahab oma karjasmaahaea sisse teha.
Tallitaja käis nende tüli üle waatmas ja tunnistas, et se tee kust Tiiri oma krundi pealt wõiks wälja peaseda, on wäga tüma pori, ja tarwitab enne suuri kulusid enne kui teed wõib läbi teha.
Kohtu Otsus:
Kohus mõistis, Saueaugu peab oma karjasmaast 10 jalga Tiiri Karlele teed andma, Tiiri peab oma kuluga wärawa, ette tegema, ja selle ette walwama, et Saueaugu, tema läbi käimise pärast kahju ei saa.
Peakohtumees: Juhan Humberg XXX
kõrwasmehed Gustav Reindes XXX
                          Reinhold Korsen XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Mart Treuman nõudis Kihelkona kohtu seleta olla  walla kohtu mõistmise anda.
