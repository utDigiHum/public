Ükstuhat üheksa sada üheksamal aastal aprilli kuu 15 paewal ilmusiwad Harku walla kohtusse selle sama walla talo pojad Hindrek Karli poeg Pendt ja Mart Mardi p Teiman ja palusiwad eneste wahel tehtud suusonalist rendi lepingut kohtu akti raamatusse ülesse wõtta. Molematel pooltel on seaduslik woli wabatahtlisi aktisid teha. Leping on järgmine
Hindrek Pendt annab oma talukohast Pendi Sorwe külast põllu mis Liiwaku nime alla on nimetud ja heinama Ristiniidi koppel Mart Teimanile kolme aasta peale rendi peale. Mart Teiman maksab iga aasta pealt wiiskümmend (50) rubla Hindrek Pendile renti. Tanasel paewal maksab Mart Teiman Hindrek Pendile kolme aasta rendi ükssada wiiskümmend rubl. kohe wälja.
See leping wõib üks puha kumba poole läbi tanasest paewast kahe nädali jooksul ümber lükkatud saada, siis maksab Hindrek Pent 150 rubla renti terwelt Mart Teimanile tagasi. Peale seda jääb tänane leping täie seadisliku jou sisse.
M Teiman &lt;allkiri&gt;
H Pent &lt;allkiri&gt;
See leping on pärast akti raamatusse sisse kirjutamist pooltele ette loetud ja nende poolt alla kirjutudmida kohus oma allkirjaga oigeks tunnistab
Kohtuesimees O Reinde  &lt;allkiri&gt;
Liikmed  H Meri  &lt;allkiri&gt;
                A Otsman  &lt;allkiri&gt;
                R Korsen  &lt;allkiri&gt;
Kirjutaja Kask &lt;allkiri&gt;
