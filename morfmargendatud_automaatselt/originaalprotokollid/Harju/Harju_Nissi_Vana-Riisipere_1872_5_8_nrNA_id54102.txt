Sel tännasel päwal tullid kokko Lehheta walla Tallitajad ja wollimehhed ja keik perre kohha rentnikud, nou piddama selle kortle kambri rahha pärrast mis jubba ammust aiast on wölgo jänud ja nüüd wiist sedda wanna wölga kätte tahhetakse. Ja nemmad wötsid sedda nouks et nemmad mitte ei tahha sedda rahha enne maksta kui härra omma wanna wölla ärra maksab mis on temma aegas wölgo jänud mis pärrast nemmad peawad härra wölga maksma sedda nemmad ei lubbanud mitte koggoni.
Lehheta walla pölitsei nimmel
Tunnistusseks Tallitaja Jürri Hoidorff XXX
Kirjotaja TLasmann [allkiri]
