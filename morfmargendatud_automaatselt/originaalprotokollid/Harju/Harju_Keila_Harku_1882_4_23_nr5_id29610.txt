Ette tuli kaebaja Hindrik Wahtel ja kaebas, et tema sulane Hindrik Sarapu kellega ta aasta kaupa oli teinud, on enne aastad ära läinud.
Hindrik Sarapuu sai sisse kutsutud ja temale kaebtus ette loetud. Tema tunsitab, et ta ei wõinud Hindriku Wahteli juures teenida, peremees peab wäga purjutama ja teda siis hirmsa moodi sõimama, kui ta joobnud peaga kodu tuleb, ega anna oma teenijattele korralikku söömist ja peab neid nälgas. Kord tulnud ta jälle segase peaga kodu, peksnud kõik naese ja lapsed toast wälja ja sõimanud teda häbematolt, et ta tema naesego häbi elu elada ja nenda tema wara ära raiskada. seda tunnistab Tallitaja Hindrik Paramell tõeks, kes seda sõimu pealt on kuulanud.
Kohtu otsus.
Kohtu otsus: Hindrik Wahtel maksab sulase Hindrik Sarapule se 10 rupla palka mis taal sees oli täielikult ära, ja wõib omale uud sulast kaubeltud. leppitud.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Leppitud
