Rieseperre walla koggukonna kohhus koos isseärralisse asjadepärrast
19  Ette sai woetud Jürri Jööts hobbuse pettmisse pärrast otsust tehja mis II Protokolis No15 all polele jähhi.
Kohto ette sai kutsutud Ans Sönnajalg tunnistusse mees Tallinnna worimees Karel Wilbaum ja ütles: et Jürri Jööts on Rieseperre Rabbakörtso ette tulnud ja küssinud: Kes tahhab hobbusid wahhetada? ja on wiis rubla peale küssinud, agga Ans Sönnajalg on 3 rubla pakunud ja kui hobbused on prowitud olnud on Ans Sönnajalg ka 5 rbla. peale annud. Ja pärrast on Jürri Jööts küssinud kes nüüd tagganeda tahhab maksab 20 rubla tagganemist.
Kohhus arwab: et Karel Wilbaum ja Ans Sönnajalg ühhed seltsimehhed on ja ka ühhes nöus woiwad olla ja ei te mitte kohto otsust waid üks ehk kaks Ruila meest peawad tullewal kohto päwal kohto ette tullema ja otsust andma Sest Ruila mehhed on linnnas olnud ja ei woinud tänna kohtu ees olla.
Tullewal kohto päwal saab otsus tehtud.
