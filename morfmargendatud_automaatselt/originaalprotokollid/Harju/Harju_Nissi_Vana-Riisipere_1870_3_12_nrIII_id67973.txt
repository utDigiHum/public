1 a tulli ette Augo Tõnno Umberg ja kaewas et temma on wanna Tallitaja Juhhan Wiesemanni kätte maksnud 1867 aasta teise poole eest 163 koppik mehhepealt, kolme mehhe eest 4 Rub 89 kop ja 1868 aasta Neekruti rahha kahhemehhe pealt 50 kop ja ütleb sedda rahha 1868 aasta Aprilli kuus üks Pühhawa, Kirriko kõrsos wälja maksnud 5 Rubla 40 kop, ja ei olle mitte lasknud üllese panna, ja nüüd pärri tade jälle temma käest,
b Sellepärrast sai ette kutsutud, Juhhan Wieseman ja tunnistas, et temma kätte ei olle mitte maksnud, ja ei lubbanud ka mitte maksta,
d käe mehhi ei olnud neil kumba gil
kohhus moistis et neid saab kihhelkonna kohtusse saadetud kui mitte muddu siis kas wande läbbi omma asja selletaksid
e ja nemmad ollid ka mollemad selle nous et nenda saaks selletud
Pea wannem Jürri Wiil [allkiri]
korwa mees Hans Wiik XXX
kõrwa mees Jakob Kraut XXX
