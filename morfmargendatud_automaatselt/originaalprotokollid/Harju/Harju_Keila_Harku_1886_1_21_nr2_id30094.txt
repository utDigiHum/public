Astus kohtu ette kaebaja Juhan Emar ja kaebas, Willem Kulu olla tema taskust linna tee peal, Rakatka kõrtsu ees, 8 Rbl. raha wägise ära wõtnu, ja tunnistas, teda hulga rahwa ees oma raha wargaks.
Willem Kulu sai ette kutsutud, tema tunnistas, Juhan Emar olla tema juures ööd maganud, ja wõtnud tema kuue öösiks oma peaalla, selle taskus oli 8 Rbl. raha ja se raha olla ära kadunud, ja tema olla raha kadumise üle, kaks pääwa perast seda kadumist teadust saanud, ja arwab, et Juhan Emar se waras on, ja selle põhjusel wõtnud tema Rakatka kõrtsu ees, oma raha Juhan Emari taskust ära.
Juhan Emar ütles selle raha oma peremehe kääst teenistuse palgaks saanud.
 Et Willem Kulul mingi kindlad tunnistus ei ole, kus näidata wõis, et Juhan Emar tema raha oleks warastanud, siis mõistis kohus, et W. Kulu peab need 8 Rbl. raha Juhan Emarille tagasi maksma (maksis kohtu ees tagasi.
Peakohtumees: Juhan Humberg XXX
kõrwasmehed Gustav Reindes XXX
                          Reinhold Korsen XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
makstud.
