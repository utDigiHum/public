Meie alnimmetud tunnistame selle läbbi, et se meile teada on, et meie seadus se järrele meie koggodusse kohhus on, köige omma waestelle maggaskist wilja laenuks anda, ja tootame selle pärrast hoolt kanda, et köikidel, kellel tarwis lähheb,- olgo perremeestele ehk wabbatikkudele,- wilja laenuks peab antama; kui agga maggaskis wilja ennam ep olle, siis tootame wistist weel selle Wina ku sees rahhaga, mis walla laekas on, wilja osta. Kui meie selle jures keik, mis meie täita on, täitmatta jättame, siis wastame meie selle eest ja anname ennast selle trahwi alla, mis seadus selle ülle kinnitab.
Paunküllast Peatallitaja: Jürri Künnap XXX
abbimehhed: Mihkel Lauk XXX
Thomas Peutel XXX
wollimehhed: Kustaw Plus XXX
Jaans Widdens XXX
Karel Tulp XXX
Mart Pihlak XXX
Juhhan Waks XXX
Josep Luup XXX
Jaan Luup XXX
Magnus Tammert XXX
Paunkülla Koolimajas 
Sel 22mal Oktobril
1868
