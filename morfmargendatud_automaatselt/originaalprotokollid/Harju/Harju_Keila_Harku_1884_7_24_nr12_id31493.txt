Täna, sel 24 Heina k.p. 1884 siin Habersti walla kohtu ees Tiihi koharentnikule Priidik Otsmannile ja Umboja koharentniku Juhan Luurmannile linna moisade walitsuse kiri, Baggo herra kirjutud sel 19. Heina k.p. s.a. ette loetud ja teada antud, et nende soowimise ja palwe peale on linna mõisa walitsus nüüd nende rendi maad on takserida lasknud, hinda määranud ja nüüd 10% sissemaksmise hinnaga müüdud saawad. Seega on siis selle teada andmisega nende praegused nuumakontrahtid murtud ja linna moisa walitsuse kask kohtu poolt täidetud saanud.
Kohtuwanem XXX G. Suurmann
Kõrwamees   Gottleb Torbek &lt;allkiri&gt;
teine              XXX Andres Jaakmann
Kirjutaja Joh Tasson &lt;allkiri&gt;
Lõpetud.
