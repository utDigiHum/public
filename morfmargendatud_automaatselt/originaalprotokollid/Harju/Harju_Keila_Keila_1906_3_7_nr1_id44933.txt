1906 aastal Martsi kuu 7 p. ilmusid Keila Walla Kohtusse kohtule tuntud Keila walla talupojad Juula Andrese lesk Piiron , tema poeg Jakob ja Lena ja tütre Tiina Piironi mees Hindrek Ikmelt ja palusiwad järgmise suu sõnal tehtud leping akti raamatusse üles tähendada.
Meitel koigil kui surnud Andres Piironi pärijatel on Keila kogukonnas "Trombi"koha peale elu hoone ühes 2 aida ja 1 laudaga mille wärtus on 280 rub /kakssada kaheksakümmend rubla/
Need hooned meie Juuli, Jakob  ja Leena Piironid lubame Tiina Piironi mehele Hindrek Ikmeltile  täieks ommanduseks, mille ette Ikmelt on kohustatud ema Juuli Piironi pidama ja toitma kuni surmani ja peale seda maksab Jakob Piironile sada rubla /100 r/
Leena Piiron  on saanud juba oma wenna Jakobi käest kakskümmend rubla ja jäeb weel saada ema Juula käest kakskümmend rubla
Se leping peale akti raamatusse kirjutamist on pooltele ette loetud, mille tõenduseks kirjutawad omad nimed alla
Juula Piiron  kirjutada ei oska tema palwe peale kirjutas alla J.Maaberg [allkiri]
Jakob Piiron {allkiri]
Leena Piiron [allkiri]
H. Ikmelt [allkiri]
President J. Witsman [allkiri]
Liikmed: J. Peärnberg [allkiri]
                J. Lind [allkiri]
kirjutaja: A. Almberg [allkiri]
