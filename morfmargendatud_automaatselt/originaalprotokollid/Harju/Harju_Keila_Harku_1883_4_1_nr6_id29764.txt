Hüüro mõisa metsawaht kaebas, et Otto Pless olla moisa metsast üks koorm hagu warastanud, Otto Pless ei salganud seda ja tunnistas, et tema ola 5 kubu kogematta mõisa maapealt  raiunud ja palus omale selle ette kerget trahwi, ta olla waene ja ei joua raha trahwi kanda. Moisa leppimise peale moistis kohus temale 20 (kakskumne) witsa hoopi ihunuhtlust.
Peakohtumees Jurri Salfeldt XXX
Kõrwasmehed Karel Anderman XXX
                          Juhan Humberg XXX
lepitud
