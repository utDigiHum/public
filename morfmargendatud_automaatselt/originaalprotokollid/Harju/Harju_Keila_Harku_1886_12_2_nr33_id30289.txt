Astus kohtu ette Mulgi Otto Kõrge ja kaebas, Tema andis linnas, heina turul Willem Kulu kätte 6 naela wõid, ja käskis, et Willem seda wõid wõib siis ära müüa, kui 25 kop. nael saab, ehk oodata nii kaua, kui tema heinad on ära müünud, Willem Kulu pole aega saanud teda niikaua oodata, ja annud wõi püttid turu peal Juhan Emari kätte ja läinud isi ära. Nüüd olla Juhan Emar wõi kõige püttitega, kas isi ära müünud, woi ärra kautanud ja ei taha tema kahju tasuda.
Juhan Emar ja Willem Kulu sai ette kutsutud, Juhan Emar tunnistas tema pole mitte käskinud Willem Kulud wõi pütta oma wankri peale panna, tema olnud turu kõrtsu ees ja näinud küll, kuda W. Kulu wõi tema wankri pannud, siis läinud ta jälle kõrtsu sisse, kes teab kes seda sealt ära warastas (kõige puttitega.)
Kohtu Otsus:
Kohus rehkenda Ottu Kõrge kahju 1 Rbl 80 Kop. suuruseks ja mõistis, et peawad kolmekesti ühtlase seda kahju kandma, Willem Kulu ja Juhan Emar kokku maksawad Ottu Körgele 1 Rbl 20 kp. selle wõi ette, ja Otto kahjuks jääb 60 kop.
Leppitud.
Peakohtumees: Juhan Humberg XXX
Kõrwas mehed Gusta Reindes XXX
                           Reinhold Korsen XXX
Kirjut G. Kollip &lt;allkiri&gt;
Makstud.
