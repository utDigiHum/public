Kohto korra pärrast ollid Ohto walla  kohtus walla hoones kokko tulnud 
Kohto wannem Otto Tulp
Käemehhed Jurri Krous
                       Hans Luuk
Ette tulli kaebaja Hans Luuk  (kohto kõrwamehhe poeg) ja kaebas Kustas Sauli peale, et Kustas Saul temma löötsapilli mis 1 Rub 50 cop wäärt olnud löhki kärristanud ilma et neil middagi tülli ehk ridu on olnud. Nemmad läinud Jöuluwiimse pühha öhtu Röukülla perrese, joond seal öllut, ja Hans Luuk pannud pilli lauua alla. Kustas Saul wötnud selat pilli ja kärristanud löhki, ja Röukülla perremes Jurri Rang tunnistab ka et se nenda olnud. Kustas Saul ei aia taggasi et se nenda ei olnud.
Kohhus möistis, et Kustas Saul piddi Hans Luukile 1 Rub 50 cop pilli eest maksma ja peale sedda 50 cop trahwi walla laekase ja Hans Luuk piddi 10 cop trahwi wallalaekase maksma
Kohtowannem Otto Tulp xxx
Korwamees Jurri Krous xxx
Lissamees Jaan Tasso xxx
Kirjotaja Jacob Rosenwaldt
Kohto käiad lepptud ja trahw maksetud
