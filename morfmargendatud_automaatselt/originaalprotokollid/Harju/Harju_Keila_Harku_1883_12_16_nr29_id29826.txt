Harku Hüüro moisa walitsus kaebas et Juri Kiibits olla Temale üles annud, et Sõrwe mesa waht olla Kristjan Klaserile üks koorm hagu müünud ja olla isi siis ühes koos Kallasti kõrtsu läinud seda raha ära jooma, ja Kiibits olla moisas tunistanud, et Kaarel Krumm ola seda ka näinud, siis nouab moisa walitsus et Juri Kiibits selle asja toe pohja peale saadaks.
Kohus kutsus metsa wahi ja Kristjan Klaaseri ette, ja päris järel mill aeal nemad saarnas kaupa olid teinud ja kus Kristjan Klaaser need haud ola panud sest walitseja laskis küla läbi otsida aga ei leitud miski märku. Nemad tunistasid seda täitsa waleks, Kristjan Klaaser ola kül metsa wahiga Kallasti kõrtsus kokku saanud, ja sealt ühes koos jalgsi ilma hobuseta kodu tulnud, seda ola ka Karel Krumm näinud ja siis juttu ära teinud, et nemad Kõrtsus purjutamas käinud, seda saanud wana metsa waht Juri Kiibits kuulda ja lainud walega moisa kaebama, et uus metsa waht olla metsa müünud ja siis seda raha Kallastil ära joonus, ja andis üles, et Lauri Karel Krumm olla ka näinud ja woida tõeks tunnistada.
K. Krum, tunnistas, et tema ei ole miski näinud ola aga kuulnud, et metsa waht Kortsust tulnud, wargust ei woi ta tõeks mitte tunistada.
Jarel kuulamise läbi tuli walja, et Juri Kiibits ola kadeduse pärast tahtnud uue metsa wahi peale walet kaebada, et siis jälle isi aseme tagasi saaks mis ta kelmuse pärast kautas.
Kohtu otsus
Jüri Kiibits istub 48 tundi selle trahwiks kinni, ehk maksab 4 Rbl trahwi, miks ta seesugust wale kaeptust üles wõttis, ja seläbi mõisa walitsust ja walla kohut tülitab ja uut metsa wahti püüdis laimada ja wargaks teha.
Juri Kiibits palus omale raha trahwi.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
leppitud
