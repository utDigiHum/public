II.) Piddasid selle sandi rukkiste ülle nõu Mis Keiserliko kihhelkonna kohto härra on käskinud wälja anda, nende kätte kes on sisse toonud: (Ja nenda sai ka pakkutud) nemmad lubbasid kül wötta; agga uut ei olle mitte assemelle tua: Sepärrast piddasid Wollimehhed ja Tallitaja sedda nõu:
Et need sammad mehhed ostwad selle ärra mis naad on sisse toonud selle samma hinna eest mis üllemal nimmetud parrama rukkiste eest pärritakse. Ja nemmad ollid kõik sellega wägga rahhul: Kui sedda Keiserlikko Kihhelkkonna kohto härra lubbab ja rahhul on.
Tunnistusseks: tallitaja
Jürri Lindros XXX
Jaan Simson XXX
Wollimehhed: Jakob Gutmann XXX
Andres Simson XXX
Kirjutaja JWiil [allkiri]
