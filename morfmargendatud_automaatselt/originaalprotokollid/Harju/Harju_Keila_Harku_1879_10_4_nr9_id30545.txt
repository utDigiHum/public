Ette tuli kaebaja Mari Merikül Soäre Juhani naene ja kaebas, et Kristjan Paemuro poeg on tema lapsi peksnud (Meriküla lapsed 5. ja 7 aastased, Paemuro poeg 12. aastane) ja kui tema seda K Paemurole rääkinud, siis on tema oma poeale weel nõu taka anud.
Ette sai kutsutud K. Paemurd ja tunistas, et tema poeg pöle mitte M. Meriküla lapsi peksnud, waid lapsed on isikeskes natuke negelenud ja M Merikül tulnud kohe kaebama.
Kohus mõistis asja mitmet pidi läbi kuulates ja leidis et nemad isi mõlemad wastamise on nurisenud ja purelenud ja sepärast peale K. Paemurd 1 rubla trahwimaks ja M Merikül 4 tundi wangis olema sellega olid Kohto Käiad lepitud.
Kohtowanem K Lensmann &lt;allkiri&gt;
Kõrwasmehed O Mänd XXX ja K Sõrmus XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
Mõistus täidetud nimetud pääwal.
