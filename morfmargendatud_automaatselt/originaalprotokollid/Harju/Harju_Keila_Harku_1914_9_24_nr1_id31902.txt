1914 a. Septembri kuu 24 päewal tulid Harku walla kohtusse Wääna Naage külast Klambri koha omanik Juula Kustase t. Reits ja tema poeg Hans Kristjani p. Reits, kes kohtule isiklikult tuntud ja palusid järgmist lepingut akti raamatusse üles wõtta
§1
Juula Reits annab oma päris koha Klambri Naage külas oma pojale Hansule kõige inwentariga piiramata õigusega tarwitada, aga selle tingimisega, et tema Klambri kohta oma wendadega Otto ja Eduardiga seltsis peab ja kõik koha maksud ära õiendab.
Koha maksude suurus ületab praegu kuni 200 rublani aastas.
§2.
Hans Reits tõotab §1 nimetatud koha maksud korralikult ära õiendada ja kohta oma wendadega Otto ja Eduardiga seltsis wennalikult ja rahus pidada ja kohta heas korras hoida.
§3.
See leping kestab ühe aasta, aga kuni lepingu osalised teda üksteisele üles ei ütle, kestab muutmata edasi.
§4.
Ülesütlemise põhjusteks on korratu koha pidamine, halb läbisaamine wanemate ja wendadega.
Enne alla kirjutamist on see leping meile ette loetud.
Jula Reits &lt;allkiri&gt;
H Reits &lt;allkiri&gt;
Kohtu esimees: A Tobon &lt;allkiri&gt;
Kohtunikud J Lenzman &lt;allkiri&gt; J Wekmann &lt;allkiri&gt;
Kirjutaja J Plukk​​​​​​​ &lt;allkiri&gt;
