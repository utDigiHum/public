Hansen'i herra kojamees Gustas Liibmann, tõi Tallinna Kwartali herra kirja seia walla kohtu, kus kirjutud oli, et temal kolm nädalid enne Jõulut m.a. üks siga, mis nuuma peal olnud, ühel ööl ära tappetud ja warastud saanud. See wargus sündind nenda: Talli uks oli lahti tehtud (mitte lukkust) siga äratapetud ja seina august kust lauas olid wälja rõhutud, siga wälja wiidud. Werd olid kõik seinad täis pritsind. Poolteist nädali eest rääkind Habersti mees Peeter Jaakmann temale et tema wend Juhan ja Kingsep Joosep Liiw olla tema sea ära wiinud. Rääkides lisanud P. Jaakmann juure: Jõulu tuli Juhan meile ja ma ütlesin temale. Mis sa seia tuled, eksu söö nüüd om warastud sea liha." Kaks sinki sest lihast toonud Juhan Jaakmann tema juure. Selle juttu peale tulnud Juhan ise ka nende ja tema küsind kohe ja öölnud sina oled mo sea ärawarastanud, seda tunnistab praegu so oma wend. Juhan öölnud P. Jaakmannile: "Mis sa augud." Seda kuulnud pealt Ottu Wilklin ja Jaan Kurtmann.
2. Peter Jaakmann tunnistas Juhan olnud tema juures ja kiitnud omal liha kül olewat. Tema naene käinud Raba saunas, kus Juhan korteris on, seal rääkind sauna naene tema naesele: Eks Juhan kurasita Hansna Gustase sea peale, mis ta Joosepiga ära tõiwad. Seda olla Gustas Liibmanile rääkind.
3. Juhan Jaakmann waidles wasta, et tema sea wargusest midagi ei teada. Tema ostnud sea lihuniku poisi käest, keda mitte pole tunnud. Pärast ütles et ta singid üksi ostnud ja 1 ruba tük.
4. Otto Wilklin tunnistas, et kui Peter Jaakmann G. Liibmannile rääkind et tema wend Juhan sea ärawastanud ja selle peale Juhan warsti senna tulnud ja Gustas Liibmann Juhan Jaakmannilt sea wargust küsinud siis Peter Jaakmannile üttelnud: Mis jutt see on, mis sa oled rääkinud, siis P. Jaakmann wastanud: Ja muidugi, hakkad sa seda weel tagasi ajama wargad?
5. Jaan Kurtmann tunnistas niisama.
Asi jäi poolele. Tulewal kohtu päewal saab Sauna naene Leeno ja Joosep Liiw sea kohta kutsutud, tunnistust andma. Et mitme kordse kutsumise peale Sauna naene ja Joosep Liiw tulemata jäänud siis
Kohus mõistis:
a, et see asi Krimminal asjaga segatud on saab see kohtu protokoll Hakenrihtri mõistmise ja kuulamise alla saadetud.
b, saab kohtu poolt arwatud et Juhan Jaakmann sest wargusest mitte süütu ei ole sest tema on jo enne seesugusid tükka teinud.
d, et see asi linna maa peal on sündinud ja Kustas Liibmann soowib, siis saab temale see protokoll suurema kohtu tarwis wälja antud.
(Gustas Liibmann palus linna.)
Kohtuwanem XXX G. Suurmann
Kõrwamees   XXX G. Torbek
teine              XXX A. Jaakmann
Kirjutaja Joh Tasson &lt;allkiri&gt;
Wälja antud.
