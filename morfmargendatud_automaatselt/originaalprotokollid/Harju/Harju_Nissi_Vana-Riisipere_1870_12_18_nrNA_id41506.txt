Keiserlikko Nissi kihhelkonna kohtu polest rewidiritud ja leitud et mitte igga asja sees keik tarwilikkud tunnistused põlle kuuldud, nikui nimmelt sel 20mal Aprillis Tõnno Pillerpau wälja ütlemisse peale on sündinud, kus mõisa herra kaebduse peale - wasto wõetud õlle rahha polest ilma pitkema tunnistusteta Tõnno sõnna on rotud saand.
Nurme moisas sel 18mal Decembril 1870
kihhelkonna kohtu wannem: B von Mohrenschildt [allkiri]
