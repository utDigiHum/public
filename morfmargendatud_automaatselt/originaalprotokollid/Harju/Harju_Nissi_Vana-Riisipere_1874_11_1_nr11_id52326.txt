Kohto ette sai kutsutud kusiko Jakub Kutmann, Keiserliko Nissi kihhelkonna kässo peale, et Walltto härra Paron Majdel on kihhelkonna kohtole kaebanud, et Kusiko Jakob kutmann on omma kuhha heinamad poleks annud tehha, ja omma pöllo maad sandiste arrinud, ja suiwilja all piddanud. Ja nüüd peawad walla kohto mehhed sedda asja selgesdi selletama ja järrelkuulama, kui paljo on heinamad wälja numanud, ja kas temma on igga aasta omma pöllud sönniku all piddanud.
Siis sai Jakob Kutmann kohto ette kutsutud ja temma käest küssitud kes sealt ollid heino pooleks, ehk töö peale teinud: Jaan Sassi 
Ja temma ütles et Russalo kuolmeister on teinud heina poleks 12ne sao ma ja ühhe kuhjama on tal palga eest teinud, ja Terjato Jaan Lindros on teinud 4 saoma sullase palga eest, sest et ta  temmal on sullase eest olnud, ja Pollist Sirko Jaan Warbola on teinud, 2 saoma Kusiko maapealt ja Wäljatagguse Andres Wohlmann naene Tina on teinud ühhe sao ossa töö peale, ja Pajo Juhhani Marri on teinud ühhe sao ossa töö päwade peale, ja Kusiko omma wabbatik Trutto Ewart on teinud kahhe sao ossa töö päwade peale, ja Tallitaja ütles siis weel sinna jure, et üks nurme walla mees on ka teinud ühhe saoma heinamad, Kusiko maa pealt ärra isse ommale.
II
Siis sai kohto ette kutsutud, Russalo mees Jaan Sassi, ja temma käest küssitud kui paljo ta kusiko maapealt olli heina teinud; Ja ta ütles, et ta olli teinud 12 saoma heinamad poleks, ja ühhe 4ja saoma olli ta palga eest teinud, sest se hein saab kusiko kohha perremehhele.
III
Sai Terjato Jaan Lindros ette kutsutud ja ta käest küssitud; Ja temma ütles et ta olli 4 saoma heina teinud, mitte poleks, waid ta olli tal sullase eest olnud, ja sai sedda palka
VII
Sai Pöllist Sirko Jaan Warbula ette kutsutud ja temma käest küssitud ja temma ütles, et ta küll aidanud heina tehha, agga ta ei olle ennam heina sanud kui 2 saado, ja kusiko perremees on needgi omma heinde hulka pannud, ja lubbanud nenda anda kui piddanud jätkama.
A
Sai Andres Wohlmann naene Tina ette kutsutud, ja temma käest küssitud; Ja ta ütles et ta olli teinud ühhe saoma heina kusiko maa pealt töö peale
VI
Sai Juhhan Lindmann ette kutsutud, ja temma käest küssitud; Ja ta ütles, et ta olli teinud ühhe saoma heinamad töö päwade peale, ja on lubbanud sönniko taggasi anda.
VII
Sai Wabbatik Trutto Ewart ette kutsutud ja temma käest küssitud; Ja ta ütles et ta olli teinud kahhe saoma heinamad tööpäwade peale; Jase Nurme mees ei olnud küll mitte seal, agga Tallitaja ütles et ta olli teinud ühhesaoma heinamad töö päwade peale, ja kakob ütles et ta ei olle mitte talle heino lubbanud.
Ja Jakobi käest sai küssitud kuidas ta omma pollo maad olli arrinud ja ta ütles et us olli ta arrase ärra sönud se pärrast olli ta suiwilja rohkem mahha teinud kui rukid, ja minnewa aasta on temmal olnud 2 tündremad, ja tännawo aasta jälle 2 tündremad rukkid maas, ja kohhos ei wöinud talle middagid se ülle möista
Kohto peawannem Kustas Waldmann
(Mart Wakker
(Jaan Arro
T. Lasmann [allkiri]
