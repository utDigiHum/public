15
Rieseperre walla kohhus olli peale kordlikult koos Rieseperre moisa metsa üllewataja herra Sterni kaeptusse pärrast mis Protokol No7 punkt 13 polele jäi
Kaebtusse kandja endine Lattisilla metsawaht Hans Karjus astus kohto ette ja sai temmale se kaeptus ette loetud, temma tunnistas sedda õige ollewad.
Agga minna ei tea sest wargusest mitte suggugi kes need puud Kaldama mäelt on warrastanud sest minna ollin sel aial Metsaherra tenistusses:wäddasin puid 2 päwa raiusin koddopeneks kolm päwa, istutasin Ue Rieseperres metsa 9 päwa, ja roikaid raiumas ollin 3 päwa Märtsi ku sees.
Agga need 2 mändi ja se suur Kask mis Kloostre Nõmme metsa wahhi poeg Kustas Trewink neid on warrastanud. Se suur kask mis Kloostre Lattisilla õues maas olli, ollen ma käskinud omma poia omma koio tua, agga et sant te olli ei wõinud koio tuua, siis sai se Kloostre Lattisilla õue mahha pandud.
Kohhusmõistis sedda metsa warguse asja selletamist surema kohto ette sest et se surem on kui walla kohhus selletada wõib.
Kohtowannem: Jaan Sarik XXX" kõrwa mehhedJürri Simson XXXJaan Lehbert XXX
Protokoll Surema kohto ette wäljaantud 19mal Junil 1886 aas.
