Astus kohtu ette Hüeromõisa karja üle waataja härra Eggert, ja kaebas alamal nimetud meeste peale, et need on mõisa akkende al hirmsast sõimanud, wandunud, ja karjunud söda öö ajal; ja mõisa wallitsuse nimel annab tema neid kohtu alla.
Kutsuti kohtu ette need süüalused nimelt:1.			Kabeli Karel		2.			Kustas p. Karel Tahwer		3.			Karel Jäger		4.			Karel Kraasmann		5.			Jaacop Pijrkel		6.			Mart Toom		7.			Hindrek Pless		8.			Hindrek Krumm		
Kõik need mehed tunnistasid oma süüd tõeks ja palusid et nende süü saaks andeks antud sest nemad olla seda wiinase peaga teinud.
Kohtu otsuseks tehti: Et need mehed seda öö ajal on teiste rahu rikkunud hirmsa wandumise teutamise ja sõimuga saawad iga üks 20 witsa hoopi
Kohtuwanem: Jürij Saalfeldt XXX
Kõrwasmehes Heinrich Hein XXX Karel Pijrkel XXX
Kirjutaja: G Steinberg &lt;allkiri&gt;
Sai sell pääwal trahwitud.
