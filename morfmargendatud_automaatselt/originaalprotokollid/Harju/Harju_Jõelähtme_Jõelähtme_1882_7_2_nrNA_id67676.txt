Kaebaja Peter Peetremägi ütleb: et kümne aasta eest kui tema on Kermo koha peal peremees olnud siis on Matsu Andres Kuusmani lomad tema ohras olnud, tema aianud kinni ja se Andres Kuusman pidand siis temale wiis pütti ohre kahju tasumist maksma aga ei ole siitsadik weel mitte ära maksnud.
Kaebataw Andres Kuusman ütleb et üks kord on kül tema lomad Peter Peetremägi juures kinni olnud ja tema on siis wiis rubla trahwi maksnud aga neist wie pütti ohratest ei tea tema sugugi ja sepärast on se Peeter Peetremägi nüüd tema käest hakand neid ohre pärima et tema on pearaha wõlga hakand küsima mis mulle aasta on wõlga jänud.
Peter Peetremägi ütleb weel, et tema mitte kopikud lomade trahwiraha Andres Kuusmani käest ei ole sanud.
Tunnistus Jaan Rohhel ütleb: et tema on se aeg Peter Peetremägi jures teenumas olnud, kui Andres Kuusmani lomd on ohras käinud ja tema peremees saatnud teda selle trahwi järele, aga se Andres Kuusman ei ole mitte tema kätte and.
Kohus moistis se kaebtuse tühjaks sest te se asi juba kümne aasta eest on olnud ja kegi seda kahjo ei ole se aeg ülewatamas käinud.
Peetremägi nouab suremat kohut, saab.
Protokoll antud sel 3. Juli 1882.
