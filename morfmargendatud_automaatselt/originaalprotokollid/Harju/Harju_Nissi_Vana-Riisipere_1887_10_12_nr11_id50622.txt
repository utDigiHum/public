22
Teiseks kaewab Metsa herra Stern: Selle otsimisse juures leidsin minna Kusiko saunast 15 nored tammed, ´a 2 tolli jämme, ja minna küssisin kusiko s. wanna mehhe käest kust sinna need tammed wõtsid, temma ütles: tõin Sosalust omma tütre jurest.
Kaebtusse kandja assemel astus Kusiko s. Liso Jegerson kohto ette, ja ütleb wälja: meie issa kes praego haige on tõi need tammed Sosalust omma tütre poia Leppiko Karel Melis käest.
Kohhus mõistis: et Sosalu Leppiko Karel Melis peab teiseks eestullewaks kohto päwaks Rieseperre walla kohto ette tullema tunnistust andma, et senenda tõssi on kuida Liso Jegerson rägib.
