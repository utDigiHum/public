Sel 2sel Nowembril olli Nurme walla politsey kohhos koos Nurme koolimaeas
a) ette sai kutsutud Waino Matso Liso Rosewon, linnast tulnud kirja küsimise peale, selle Juhan Wirroga peksmise pärrast mis enne jubba 10mal Septembril protokoli sai kirjotud ja Hakenrehi jure läkitud.
Liso Rosewon astub ette, ja kaewab omma hädda, et temma ei wõi kedagi tehha pea on haige - ja (3) kolm kord ollen kuppu lasknud, agga kõik ei aita ühtigi, ei sa parremaks - nüüd on silmade sees ka wallo, ja pea käib ümber, - tahan ennast kül püsti höida, agga wahest kukkun ikka maha, kui kummardama löön. Ja külle sees on wallo, ei wõi hingata, kui hingan, siis just kui noaga lõikab allati külle sees.
Tallinnast läkitud kirja wastuseks on se saanud Tallinna sadetud.
abbimees Juhan Simson
abbimees Hans Waldsnep
Tallitaja Mart Meinwalt [allkiri]
