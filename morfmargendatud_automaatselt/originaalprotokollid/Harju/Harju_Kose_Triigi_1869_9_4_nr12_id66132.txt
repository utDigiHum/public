Ette tulli
Nõmme jama piddaja Härmann Weser, ja kaebab, et Wanna moisa metsawaht Aado Pudru Aprili Kuus No 1868 aastal Nõmme kartuhwlid rehhe jurest warrastanud.
Kohhus küssib, Kas Teil on tunnistusse mehhi? Nüüd astus tunnistusse mees, Mihkel Linder ette, ja tunnistab, et Aado Pudru on 6 toopi sedda moodi kartuhwlid Marri Lindrelle annud, kui Nömmes on.
2, Jula Sikkeme tunnistab, et temma mees Jürri Sikkeme on temmale räkinud, et nemmad Aado Pudruga rehhe jurest Nömme kartuhwlid on warrastanud. Agga et temma mees Jürri Sikkeme surnud on, kelle peale naene tunnistab.
Hans Tas rägib et ta Aado Sikkeme wasto üttelnud et Metsa waht Aado Pudru Nomme kartuhwlid warrastanud. Aado Sikkeme üttelnud, eks ta siis olle kimpus, kui ta on warrastanud. Hans Tas ütles, küllab nad temma käes on, agga kes neid ennam temma käest wälja wöttab. Aado Sikkeme üttelnud, egga Metsawaht poisike ei olle, et ta sedda üllese ütleb.
Agga kohto laua jures ajas Aado Sikkeme taggasi et temma neid sanno Hans Tassi wasto ei olle üttelnud, egga Metsawaht poisike ei olle. 
Nüüd kutsus kohhus Aado Pudru ette, ja küssis. Kas sa olled Nõmme kartuhwlid warrastanud et need tunnistusse mehhed so peale tunnistawad? Aado Pudru ütles, minnul olli ommal ühheksa tündru kartuhwlid.
Jürri Paimets tunnistab, minna aitasin Aado Pudru kartuhwlid august wälja wötta, ja ei olnud ennam kui kaks tündred.
Aado Pudru ütles, minna ei olle sind mitte näinud kui ma omma kartuhwlid wälja wötsin.
Mihkle Lindre naene Marri tunnistas, et ta olli Aado Pudru keldres käinud ja ühhe wakka ossa kartuhwlid keldres näinud.
Aado Pudru wastab, minna ollin seemne kartuhwlid wälja tassinud, ja need olli söma jäggo.
Nüüd küssiskohhus, Härman Weseri käest Kui paljo teil kartuhwlid on warrastud? Härman Weser ütles kümme tündred, ja tünder maksab 2 rubla 50 koppika se teeb Summa 25 Rubla.
Paunkülla Koggokonna kohhus ei wöinud sedda asja selletada, et se assi surem on, kui meie seadus kannab.
