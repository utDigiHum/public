Kohhus wottis sedda Rieseperre moisa wallitsuse kaebduse asja (wata Protk. sest 29 Detsembril No13) heina warguse pärrast, mis poleli jäi, ette, weel selletada ja otsust tehha.
Sai ette kutsutud Nõmmiko Hans Umberg ja sai küssitud? kas ta ennast teab ehk woib wabbandada ommast süist; agga tema aias taggasi; et tema polle need heinad warrastand
kutsuti kohto ette Kirsti Umberg Hanso ödde. küssuti? kas ta woib oma ja oma wenna wabbandamiseks middagi kosta; agga ei middagi; muud kui ollid nüüd oma wennaga kokko räkinud, mis naad enne teine  teise wasto ollid tunnistanud; sellepärrast
Moistis kohhus: et Hans Umberg ja tema ödde Kirsti makswad heinde eest mis nemad warrastand ühte kokko 5 rubla höbbe moisale ja Hans saab trahwiks ihho nuhtlust - 25 witsa hoopi; aga tema Ödde Kirsti jäi sellepärrast ilma trahwita, et ta on polel lerimata.
Hans Umberg ei olle selle kohto moistmisega rahul ja tahhab kohtust protokolli ehk kirja surema kohto minnna; siis sai temale kohto läbbi ette pandud: et kui tema iljem 8 päwa surema kohto ette tulleb, ehk kirja surema kohto minnna, wälja tahhab wotta, ei sa sedda ennam kuulda woetud ja on tema se wiwituse läbbi kohto möistmist omma peale öigeks tunnistand ja peab siis sedda trahwi kandma, mis siin kohtus tema peale on möistetud.
Kohto peawannem: Jakub Winkel [allkiri]
körwamehhed: Hans Surow XXX
Jurri Simson XXX
Kirjotaja: B Gildeman [allkiri]
