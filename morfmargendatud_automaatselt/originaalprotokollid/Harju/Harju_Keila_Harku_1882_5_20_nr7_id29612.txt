Astus kohtu ette Hüero mõisa karja tütruk Anna Watsow ja kaebas, et Hindrik Humberg kelle juures ta kui kaswantik 16 aastad oli üles kaswanud ja kolm wiimast aastad tüdruku asemel teenis, ei anna tema riide palka kätte, mis tüttruku kord ja kohus saada on ja peab tema oma muretsetud riided kinni.
Hindrik Humberg sai sisse kutsutud ja temale kaebtus ette loetud. Tema tunnistab, ta ei oleks tütruku riided mitte kinnipidanud, kui tütruk temale õigel ajal oleks üles ütelnud, et ta tema juurest tahab wälja minna. (keda ta 16d aastad kui oma last oli kaswatanud.) Perenaene ola külast teada saanud, et Anna tahab ära minna ja olla tema kääst küsinud, miks ta ära läheb. Ta ola seda küla rahwa tühjaks juttuks pidanud ja ei ole lubanud mitte ära mina. Seda tunnista sulane H. Humberg kes kuulnud oli, kui Anna Watsuw perenaese wasta oli töutanud mitte ära minna, Ometigi läks ta üks nädal hiljem ära ja kaubles ennast Hüero mõisa karja tütrukuks. Tema kääst sai küsitud miks ta seda oli teinud ja ilma ütlemata ära läks, ta tunnistab, et ta ola perenaese wastu juba nelja kuu eest lubanud ära minna, miks perenaene temale niipalju riiet ei anud kui teiste tütrukutele, ja ola teda näljas pidanud.
Kohus sundis neid leppima ja H. Humberg andis tale (tütrukule) need riided kätte mis, ta oli kinni pidanud, ja andis tale luba Hüero mõisa karjatütrukuks jääda. Leppitud.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Leppitud.
