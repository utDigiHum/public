Astus kohtu ette Harku mõisa walitseja Ditmann ja kaebas moisa sulase Juhan Jägri peale, et Jäger olla moisa põllul heina alwaste niitnud, ja kui kupjas temale ola ütelnud, et peab paremast niitma siis ola tema ütelnud, ega mul pole koeri tarwis järe aukujaks, nii sama on ta ka aida mehele ütelnud, Kupka kaeptuse peale läinud walitseja tema halba tööd ülewaatama, ja leidis, et kaeptus tõsi oli, ta käskis Jäägrid paremine niito, Jäger ola wastu ütelnud, kui walitseja tema kääst paremad tööd nõuab siis tema läheb ära, ega ta pole tulnud moisa põllule kiwa niitma, siis hakkanud walitseja tema wikkatist kinni ja ütelnud kui sa pareamst oma tööd ei tee ja wastu augus, siis wõid minna. Sele peale ähwartanud Jäger walitsejad wikkatiga läbi lüüa.
Seda tunnistab kupjas ja aidamees tõeks, et Jääger on nenda ähwartanud.
Jäger sai ette kutsutud ja temale kaebtus ette loetud, tema ütles et tema ola siis lubanud walitsejale wikkatiga wastu lüja, kui walitseja teda oleks esite löönud. Kupjas olla teda juba kewadest saadik kiusanud ja tema üle ilma aego kaebtust tõstnud, siis olla ta ka kupjast koeraks ütlenud, ega luba nüüd ka enam mõisa teenistuse jääda, ehk ta kül aasta peale oli kauba maha teinud.
Kohus mõistis, et Jäger kui süüalune, oma kaupa on rikkunud ja mõisa walitsejad wikkatiga lubas läbi rajuda, saab 30 witso hoopi ihu nuhtlust, ja wõib joosta kus ta tahab.
Jääger ei olnud selle kohtu otsusega mitte rahul, ja ei lasknud enast kohtu majas trahwida, peastis enast wägiwaltselt taskunoa abiga meeste kääst lahti ja läks jooksu. Walla kohus andis tema wastu hakkamist perast, tema kohtu otsuse hakenrichtle kohto hooleks.
Peakohtumees Jurri Salfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Hakenrichteri kohtuse ära antud.
