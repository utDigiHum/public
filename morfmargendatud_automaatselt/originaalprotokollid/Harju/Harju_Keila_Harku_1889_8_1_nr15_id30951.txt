Ette tuli kaebaja Harku moisa kora tütruk Nurme wallast päritud Kai Sesmin, ja kaebas, Harku moisa Aidamehe naene Madli Prumm peksis teda ilma süüta moisa jääkeldri ees joonud peaga, ja loi temale kolm korda kääga wastu pead ja kiskus juukstest, selle peale jäi tema öösi haigeks, ja tuli kange südame walu, ja Baroni hära saatis teda hospitali, selle labi on tema 15 Rbl. kahju saanud oma teenistuse sees.
Madli Prunn sai ette kutsutud tema tunnistas, mina olin piima rentniku juures surnud last kirstu panemas, siis tuli koratütruk Kai Sesmiin tema kääst aida wottid saama, ja kui tema toast ära laks teutas ta mind ja soimas joonuks wana moorik, sellest üles arritud, laksin mina tema järele ja wõtsin taalt karwust kinni ja loin kaks korda palja kaga kergelt wastu palesid, oma teutamise pärast.
Kai Sessmiin läks sealt karja aeda lehmi lüpsma, ja pühapäwa homiku, tegi Kai Sessmiin ennast haigeks, tema käis tütrukud waatamas ja andis temale arwates et on südame will natuke äädika happu weega sisse mis ta enne isi mekis ja ka karjatse naesele proowida andis, tütruk tegi enast kawalusega rohkem aigeks, teiste nouandmise peale, Tohter tuli teda waatama ja andis tunnistust, et tütruk wist saada huluks minema, sest et hull koer teda 1888 a. Decembri kuu sees oli hamustanud, Saarnane haigus woi hullustus oli ka Kai Sessmiinil üks aasta enne, kui tema Hüürus kora tütrukuks oli.
Tütruk Kai Sessmin tunnistas, et ega tema küll sellest peksust aigeks ei ole jäänud, tema oli ennast wihastanud ja sellest tuli wist haigus.
Kohtu otsus:
Walla kohus moistis otsuseks, tütruku oma tunnistuse järele, et need kaks hoopi pöle teda mitte haigeks teinud, siis oli temal üks teine aigus, ja Aidamehe naene Madli Prunn maksab 2 Rbl. trahwi walla laeka, miks tema oma woliga tütrukud trahwimaläks, ja teda kaks kord lõi.
Peakohtumees: Karel Annus XXX
korwasmehed: Reinhold Korsen XXX
                           Kustas Kruusmann XXX
Kirjutaja: G. Kollip &lt;allkiri&gt;
Maksis trahwi ära, kohtu ees 1 Augustil.
