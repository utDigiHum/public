Ükstuhat kaheksa sada üheksakümmend kuuendamal aastal Märtsi kuu 19 päewal ilmus Harku Walla kohtusse Otto Otto poeg Kulu ja palus järgmist testamentlikku seadmist akti raamatusse üles wõtta:
Mina Otto Otto poeg Kulu kinnitan oma poja Willem Otto poeg Kulu oma waranduse ainumaks pärijaks pärast surma, koha annan kohe poja kätte ja tema peab minu ja minu naese eest surmani hoolt kandma. Pärast surma saab Willem kõik hooned ja wara ning koha omale ja peab ema surmani ülewel. Teistele midagit ei luba.
Otto Otto poeg Kulu kirjutada ei oska, tegi kolm risti XXX
See seadus on pärast akti raamatusse kirjutamist pooltele ette loetud ja nende poolt allakirjutud
Eesistuja: K Tahwel &lt;allkiri&gt;
Kirjutaja as. J Awing &lt;allkiri&gt;
