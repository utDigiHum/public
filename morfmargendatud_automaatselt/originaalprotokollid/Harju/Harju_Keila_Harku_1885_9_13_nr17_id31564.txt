Tallinnast Jüri Rosenberg kaebas siin walla kohtus Haberstis elustawa linna mehe Juhan Pellmuse peale nenda: Enne Jõulut 1883 müüsin mina hobuse kõige kraamiga 69 rubla ette Juhan Pellmus'ele 30 rubla maksis tema minule kohe ära ja 39 rublaga palus ta mind kannatada ja andis mulle selle wõla raha peale wiitungi. (Näitas wiitungi ja see on õige); aga nüüd olen ma mitto kord Haberstis Juhan Pellmus'e käest oma raha kätte nõudmas käinud, aga ta warjab ennast minu eest ära. Mina wana inimene olen oma eluga puuduses., ei tea kust igapäewast ülespidamist saada, seepärast palun kohut, mulle abiks olla et oma kätte saan.
2,Juhan Pellmus tunnistas Wiitungi, mis Jüri Rosenbergi käes oli küll omaks, aga ütles: Mina arwan, et mino naene on selle raha enne surma ära maksnud, ja nüüd mull ei ole enam midagi maksta, ja ma ei tahagi enam maksta, ega mina seda wiitungi ei kirjutanud, seda kirjutas naene ja tema on nüüd surnud.
Kohus mõistis: a' et Juhan Pellmus mitte ise oma wõlga põle äramaksnud ja ka mitte näinud ei ole et naene ära maks on, siis on see raha 39 Rbl. maksmata Jüri Rosenbergile.
b, peab Juhan Pellmus Jüri Rosenbergile Jõuluks sest wõlast 20 rubla äramaks ja 19 rubla Jaanipääwaks 1886.
Kohtuwanem Gottleb Torbek &lt;allkiri&gt;
Kõrwamees XXX W. Andermann
teine                    Carl Ambos &lt;allkiri&gt;
Kirjutaja Joh Tasson &lt;allkiri&gt;
Lõpetud.
