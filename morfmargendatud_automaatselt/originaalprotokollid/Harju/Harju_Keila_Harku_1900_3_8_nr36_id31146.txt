Üks tuhat üheksaja aastal Märtsi kuu kaheksamal pääwal ilmusid Harku walla kohtusse, kohtule isiklikult tuntud Johannes Kustase p. Loek ja Hans Juhani poeg Polna ja palusiwad eneste wahel tehtud suusõnalist lepingut kohtu akti raamatuse üles wõtta:
Johannes Loek Känu No 23, 27 koha omanik müüb oma koha küljest maatüki, mis Känu koha kaarti peal No 27 alla iseäranis tähendud on Hans Polnale jäädawaks omaduseks kaheksakümne (80) rubla eest, selle tingimisega et kõik seaduslikud maa ja koha maksud jääwad Känu koha peale, ega saa nendest midagi müüdud maatüki peale arwatud, mille eest aga ostja Hans Polna iga aasta Känu koha omaniku Johannes Loekile wiiskümmend kop. (50 kop.) maksab.
Ostja Hans Polna on kõik ostu hinna kaheksakümmend rubla müjale Johannes Loek'ile wälja maksnud.
Juhanes Loeg &lt;allkiri&gt;
Hans Polna &lt;allkiri&gt;
See leping on pärast akti raamatusse kirjutust pooltele ette loetud ja nende poolt alla kirjutud.
Kohtu Eesistuja M Jerwan &lt;allkiri&gt;
Liikmed K. Tui &lt;allkiri&gt;
               H Meri &lt;allkiri&gt;
Kirjutaja J. Rammus &lt;allkiri&gt;
