Harku moisa walitseja hära Dittmann kaebas, Alliku külast karjapoisid Juhanes Kuusk ja Karel Elu, raiusid moisa metsast noored manna latwad maha ja tegid omale mangu ämri, ja raiskasid mõisa metsa.
Poisid Juhanes Kuusk ja Karel Elu sai ette kutsutud nemad tunnistasid endid süüdlaseks ja kohus moistis nendele kummakile 10 hoopi witsu ihu nuhtlust mis nende oma wanemate läbi sai kohtumajas kätte antud.
Peakohtumees: Karel Annus XXX
korwasmehed: Reinhold Korsen XXX
                           Kustas Kruusmann XXX
Kirjutaja: G. Kollip &lt;allkiri&gt;
Said oma witsad kätte.
