Tallinnast, sep Iwan Bogdanow kaebas siin walla kohtu ees, et tema isa Stepan Iwanow, kes suil 1879. siin Habersti Mäekõrtsus kilu purkidesse soolas; Olla siis kui ta hospidali läinud, oma abimehe Toomas N. (liiga nime ei tea) kätee jätnud: 1, Teemasin, 1, Tek, 1, kasukas, 100 purki, 1 padi, 2 lina, ühed tuhwlid, ja muid asju. Surma woodil käskind need nimetud asjad Toomalt wälja pärida. Palus kohtu abi!
Siit walla liige Ottu Kramann, tunnistas, et tema olla minewa sügise Tooma, nende nimetud asjadega Tallinna Moskowski trahteri ükse ette wiinud ja asjad senna treppi piale maha pannud.
Kohus mõistis: 1, et Tooma liig nimi siin tundmata seepärast on kohtul wõimata teda kätte saada, ja 2, wõib Iwan Bogdanow Tallinnas, kus Tooma kätte saab, tema peale kaebtust tosta!
Kohtuwanem XXX Juhan Kandberg 
Kohtuwomees: XXX Kristian Jerwan
                       XXX Hindrik Grossfeldt
Kirjutaja: Joh. Tasson &lt;allkiri&gt;
