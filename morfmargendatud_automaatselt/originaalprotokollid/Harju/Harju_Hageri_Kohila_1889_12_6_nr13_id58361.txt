I)
Tallitaja kaebas: Jula Karu ei maksa multse aasta magaski wõlgu 1 püt 2 garn ohri ära.
Jula Karu sai ette kutsutud, küsiti:miks pärast ei maksa sa oma wõlga. Wastus: ega mina üksi neid ära pruukind ohrad said seale söödetud ja mehega seltsis sõime tali otsa sea liha. Mehe Tõnu Karu käst sai küsitud: kas sa käskisid naest ohri magaskis tuua? ei käskind oli wastus. Kohus küsis: kas sa said seda sea liha? Wastus: ei mina ole saand mina tõin liha oma isa käest, ja mina seda wälja ei maksa!
Kohtu mõistus: Tunistust kumbalgi ei olnud ja kewadest saadik olewad nad teine teisal et Jula on toonud siis peab ta ka maksma. Lepisid sellega et Jula maksab 150 kop ja Tõnu maksab kahe aasta kasud 20 kop.
