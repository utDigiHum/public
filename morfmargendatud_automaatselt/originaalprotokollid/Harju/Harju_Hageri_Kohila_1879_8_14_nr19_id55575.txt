Ette astus kaibaja Ewardi Hans Üllenom ja Kaebas et Pahkla mõisa poisid on sel 5mal Augusti ku päwa ösel tema karja lada peal käinud ja tikust tuld tõmmanud, nimelt Kustas Nesselt, Kustas Niglassoon, Juhhan Karro, Tõnnu Wolman, Jaan Teeler ja Pahkla walla poisid Hans Pessor, Jaan Waltri, Jaan Eisen.
Ja temma tütruk Mina Üllenom ja Kalli Prits Kulderknup on öösel sedda teada annud.
Ette astus kalli Prits Kulderknup ja ütles et temma on näinud kui karja lada pealne olnud tulle walgus agga põlle tunnud kes on olnud.
Ette astus Kustas Nesselt ja Kustas Niglasson ja ütlessid et nemad on kül Ewardi õues käinud agga põlle mitte tikkust tuld tõmanud.
Ette astus Juhhan Karro ja tunista et nemad põlle mitte üksi olnud seal olnud weel kalmu Jaan, Sõeru Mart ja Mart Rebbane, Hans Paddolerin.
Ette astus Tõnnu Wolman ja ütles ma käisin Ewardi tannawas ja kuulsin kalli Prits ja tütrukud rääksid karja lada peal ja meie läksime watama ja Prits olli tütrekude jures.
Ette astus Hans Tesson ja tema läinud sealt mudu möda ja terretanud weel Ewardi perremeest ja perremees tunnistas ka et ta olli hanso näinud.
Nõnda samma ei sallgand Jaan Teler, Jaan Waltri, Jaan Eisen et nemad ei olle Ewardil käinud waid tunistasid issi et nemad ollid seal käinud tütrekid watamas.
Kohhos mõistis et Kustas Nesselt, Kustas Niglassoon, Juhhan Karru, Tõnnu Wolman sawad igga üks 30 hopi witsu. Jaan Teler, Jaan Waltri, Jaan Eisen sawad igga üks 15 hopi lapse witsu, Hans Pessor maksab 1 rubl walla laeka.
