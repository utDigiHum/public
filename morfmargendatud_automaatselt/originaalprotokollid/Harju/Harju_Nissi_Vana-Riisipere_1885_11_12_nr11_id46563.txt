3) Kohtu ette astub Saida mõisa metsawaht Tõnno Janilind ja kaewab, et Leppiko Juhan Simsoni sullane Juhan Ammert on ühed sahkwarred raiunud ja seepörast, et põle neid kätte saanud teiste puude wahelt on ta kaks kuuske maha raiunud, ja nõnda on ta kolm noort kuuske raiunud, mis kõik 4½ tolli paksud olnud.
Ja seda on Jakob Mürissep näinud, kui Juhan Ammert seal puid on raiunud, Ja Jakob Mürissep öölnud Juhan Ammertile, kas kümme rubla taskus kiheleb, et siin puid raiud?
Kohtu ette astub Tomminga Juhan Simson oma sulase Juhan Ammertiga
Kohto poolt sai küssitud; kas oled enesa sullast kaskinud Saida metsast sahkwarssa raiuda? Ei mina põle käskinud ta pidi sealt minu kässu peale samlaid kiskuma ja siis ta oli raiunud, need olla wäga paksu metsa sees olnud, ta põle neid kätte saanud, Aga Koilo sauna jäu pealt on ta ühed toonud.
Kohto küssimine Juhan Ammerti käest kust saa need sahkwarred raiusid, mis sa koeo wiisid? Ma raiusin Koilo sauna jäu pealt.
Aga kus sa need pannid, mis sa Saida metsast raiusid? kui Jakob Mürissep sind nägi raiuwad? Need jäid senna. Mis Jakob sooga rääkis? Ta ütles: Kas mul kümme rubla taskus kihelede et ma puud raiute.
Kohto otsus: Kohus mõistis, et Jakob Mürissep seda tõeste näinud on ja weel temaga rääkinud, seda asja tõeks, et Juhan Ammert need kolm kuuske ikka raiunud on ja seepärast peab Juhan Simson oma sulase Juhan Ammertiga 2 rubla Nurme walla laekase trahwi maksma
Kohtopeawanem Mart Meinwald XXXKõrbamehed Tõnno Leismann XXX Jaan Arru XXXKirjutaja J. Harnak [allkiri]
