Astus kohtu ette kaebaja Tuiso Karel Annus ja kaebas, et t ema olla Juhan Klaaserile 5 Rb raha laenanud Tallinna waekoja taga kortsus, juba kolme aasta eest, ja Juhan Klaaser ei maksa tema wõlga ära.
Juhan Klaaser sai ette kutsutud ja temale kaeptus ette loetud, tema tunistab seda waleks, ei ütle Karel Annuselt raha laenanud olewad, aga ta ola kül juures olnud, kui Tuiso Karel Annus söldati Karel Annusele 5 rupla raha laenas, ja olla seal juues ütles, et tema soldatile raha muidu ei anna, kui Juhan Klaaseri kääpeale.
Karel Annus ütleb seda tõsi olewad, aga tunistab oma käämeestega Hindrik Norbek ja Willem Andermanniga, et Juhan Klaaser on raha wastu wõtnud ja siis soldatile annud, ja on lubanud sele raha ette wastoda.
Kohtu otsus:
Juhan Klaaser maksab selle 5 rupla (wiis rubla) Tuisu Karel Annusele ilma wiibimata ära.
Kohtumoistjad
Jurri Saalfeldt XXX
Karel Andermann XXX
Juhan Humberg XXX
Seletud.
