I, Sai Leheta mõisa walitsuse käsu järel ette kutsutud: mõisa sepp Tõnu Hamermann; Tema käest küsitud: Mis on temal rääkimist ja kaebamist? 
Sep Tõnu Hamermann kaebas, ja tunnistas: Et karjase naine Mari Wohlmann on Baroni Härra ette tema peale tühja waletanud ja kaebanud et tema olla pääwa kutsari toas maas maganud. Aga tema ei olla mitte kordagi töö aegus pääwa maganud, et kül wahest õigus oleks olnud magada olnud; sest et tema ööd otsa miili on kütnud; ja et temal on need tööd weel  teada, mis el pääwal on teinud kui Mari öölnud teda magawad.
Aga Mari Wohlmann on mõisa kölkaid oma lehmale söötnud, ja Kustase Mari Grosstein ka Põumani Leeno on seda selgeste näinud
II, Sai ette kutsutud Karjase Mari Wohlmann, ja tema käest küsitud: Kas tema on näinud seppa Tõnud magawad? millas, ja kus kohas?
Mari Wohlman kostis ja tunnistas: Et sepp on kutsari woodis maganud, Kolubere laada järel see Teisipääw, teine kord siis kui sepp on linnast tulnud, ja siis ei ole kül mitte maganud waid pitkali olnud; ja kolmas kord sügise karduhwli wõtmise aegus üks homiko, oma taas woodis, aidamees on käinud sealt ülesse hüidmas hobusid rautama, Kustas on juba hobused wälja toonud seppa paja juure, ja seppa tulnud ülesse ajama, pääw olnud pooles keskhomikus olnud.
III, Sai ette kutsutud Kutsar Jaan Kõrgemägi, ja tema käest küsitud: Kas tema on näinud seppa Tõnu'd oma woodis magawad?
Kutsar Jaan Kõrgemägi tunnistas: Et tema ei ole mitte kordagi näinud teda töö aegus magawad, ei oma woodis, ega ka majal, wõib olla siis kui tema kottu ära on, aga neil nimetud päiwel on tema just kodu olnud, kui Mari ütles seppa magawad.
III, Sai ette kutsutud Mari Grosstein ja tema käest küsitud: Kas tema on näinud seppa oma toas woodis magawad Karduhwli wõtmise aegus, - sest et nende toad ühes on, ja kas tema on näinud Mari Wohlmanni lehmal mõisa põhku ees olewad, nenda kui sepp tunnistas?
Mari Grosstein tunnistas: Seppa tema kül ei le näinud pääwal magawad, aga seda on tema selgeste näinud: et Mari Wohlmanil lehmal selged masina odra kõlkad ees on olnud; Leenu Põumann on seda Karjase Marile ette öölnud, ja Mari on teda weel sõimanud rumala sõnadega.
V, Sai ette kutsutud Leenu Põumann, ja tema käest küsitud: Kas tema on ka näinud Karjase Mari Lehmal mõisa kõlkaid ees olewad?
Tema tunnistas: tema on seda selgeste ja töeste näinud, mõisa masina odra kõlkad on lehmal ees olnud.
VI, Sai ette kutsutud Hollendes Jüri Willenthal,
Tema tunnistas: et seda kül ei ole näinud kui lehmal kõlkad ees on olnud, aga neid kõlkaid on tema karja laudapeal näinud kus nende heinad on, ja et need pealt ära kadunud.
Seda tema ka ei ole näinud kui sepp on pääwal maganud, et tema kül tihti ka seal toas on käinud. Jüri Willenthal kaebas weel: et Mari Wohlman on tema karduhwlid ühe wau ülesse wõttnud, ja karduhwlid omale pärinud; sest nende karduhwilid on kõrwu maas olnud ühe maa peal; Mari on enne omal hakanud wotma, ja nenda temal ühe wau ära wõtnud, arwata 5 külimito karduhwlid
Ja tema on pärast Mari käest küsinud: Sa oled mul ühe wau karduhwlid üles wõtnud? Mari kostnud: "jah ma wõtsin",  Aga miks sa wõtsid? kas kogemata woi meelega? Mari kostnud:"Miks pärast kogemata mul kaks silma peas." mis ma tegin kui oli üles aetud, külm oleks naad ära wõtnud. Muud midagi.
VII, Said ette kutsutud kõik ülemal nimetud tunnistajad seltsis uueste.
Nemad tunnistasid kõik seda samma mis ülemal on juba kirjutud.
Aga Karjase naene Mari Wohlmann ütles et need ei ole mitte kõlkad olnud mis tema lehma ees on olnud, waid: ruki sasid - Ja neid karduhwlid on tema käskinud Hollendert sealt ära wõtta, mis tema üles wõtnud, aga tema ei ole toonud, siis onn Mari pärast ise ära toonud. Kolm ja üks pool, wõtmise korwi täit.
Teised naesed: Mari Grosstein ja Leeno Põuman tunnistasid weel tõeste need on selged odra kõlkad olnud, ja weel: et tema nendele öölnud: et sepp on ees toas töö pingi peal maganud, ja kohtu ees ütles Mari teda Kutsari taas woodis maganud.
Seepärast näitas kõigest tunnistustest, et Mari Wohlmann tühja walet on kaebanud; ja ka mõisa kõlkaid on wõtnud,
Kohus mõistis siis: Et Mari Wohlmann peab trahwi maksma walla laeka:Esiteks seepärast et tema on tühja waletanud ja kaebanud seppa Tõnu Hamermann'i peale.			1 rubla.		Teiseks seepärast et on mõisa kõlkaid oma lehmale söötnud:			2 rubla		Summa:			3 rubla		
Ja Hollandrele peab Mari tema karduhwlid wälja maksma: 1  wak karduhwlid, ehk 40 koppik raha.
Kohtu mõistmise juures olid:
Pea kohto wanem: Kustas Waldmann XXX
Kõrwa Mehhed:
Juhan Ulmenthal XXX
Jaan Kuuskberg XXX
Kirjutaja: J.Wiil [allkiri]
