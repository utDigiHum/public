II) Sai see asi uuesti ette wõetud: Waata Protokoll Nr 1.
Tuli üles, et peremees Kaarel Rosenberg ei ole Jaan Mürkelt kambris löönud puuga, selle läbi et ei ole kohtumehele näidand kas märgid näha. Pois ütleb wanad riided olnud aitas see pärast tahtnud aita minna, aga riided on kambris olnud; ja peale selle oma wana peremeest teiste ees teotanud.
See peale tegid kohtu liikmed see Mõistus: Jaan Mürkel maksab 2 rubla trahwi et on kahe walega kohtu ette astunud. 1 rubla lubas K. Rosenberg kiriku waestele 1 rubla saab walla laeka.
Need püksid ja kindad kellest eespool Nr 1 räägitud, on üle kauba olnud, sulase pääwad, mis aastast puudu jäid muist peremehe kahjuks.
