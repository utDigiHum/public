Rawwila kolimaias 27mal Webruaril 1867.
Kous ollid:Peakohtomees Juhhan Suursild,temma abbimehhed:Jaan Sein,Hans Kross ja Hans Siimson walla kirjotaja.
Ette tulli lesk Kai Jäks,kaebab:et temma on Peter Unternupile 29 Kop.rahha laenanud,ja temma naesele 10 Kop.(1865)nüüd kui ta kätte küssinud,on Peter Unternup tedda kämblaga wasto silmi lönud.
Wastane Peter Unternup tunnistab:etta on 29 Kop.wõtnud;agga naene ei olla wõtnud.-Agga et Kai Jäks tühja häbbematta jutto temma peale teinud,sepärrast on temma tedda lönud.
Et Peetri naene 10 Kop.wõtnud ei olle õiget tunnistust;agga mõista on et ta wõtnud on.
Kohhus mõistis:Peter Unternup peab se 40 Kop.Kai Jäksile ärra maksma,ja et ta tedda lönud 90 Kop.trahwi.Agga et Kai Jäks häbbematta jutto teinud saab temma trahwi rahhast 50 Kop.trahwi wallalaeka.
