Ette astus Rabiwere walla mees Juhan Ringel ja kaebas et tema peremees Gustaw Steinberg olla teda kese aasta ilma mingi süüta lahti lasknud ja just wastu talwet kus mingi teenistust ei ole kus ta sui kõik too weike palgaga olla ära teinud, tema olla kewade tahtnud kohe ära minna kui nainud kuda nende peres teenijatega lugu olnud ja siis nõudnud peremees tena kääst aasta palka tagasi ja nuud lasknud ta teda isi ilma süüta lahti siis nouda ka tema aasta palka.
Gustaw Steinberg astus ette, ja ütles et sulane olla isi lubanud ära mina. Nimelt: Tema olla linnas kuulda saanud, et sulased wana rahwaga se on tema isa ja emaga kodu juures riidus olnud, siis läinud tema maale ja panud tingimises ette kas sulase tahta edaspidi rahu olla kui mitte siis saab iga mehe palk käte makstud ja woida teist peremeest otsida. Juhan Ringel olla ütelnud, et seda olen ma juba ammu ootnud ja ola siis lubanud ära minna kui peremees tema aasta palga ära maksab. Peremees Steinberg kaebas, et sulane olla temale peale 15 rubla kahju teinud oma laisa tööga ola ta 5 rubla kahju teinud ja omblus massina jalad teinud katki mis tema Kätte oli ustud Tallinna wiia, sulane waitleb selle wastu ja ütles, et wana isa ola peale panud ja ta ei ole enne masinaid mitte wedanud ja se ola ilma teadmata katki läinud.
Kohti Otsus:
Kohus mõistis, et Steinberg peab sulaselle senni maani kus ta teeninud oli täielikult tema palga ära maksma ja wõttab koik muud kahjus mis tale sulase läbi on juhtunud oma kanda.
Peakohtumees Jurri Saalfeldt XXX asemel Ottu Wattel
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Protokol sai Keiserlikku Kihelkonna Kohtule ära kirjutud.
