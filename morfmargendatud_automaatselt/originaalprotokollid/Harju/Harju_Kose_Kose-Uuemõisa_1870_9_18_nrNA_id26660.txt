Kohtu korra pärrast olli Uemoisa koggukonna kohhus kokkotulnud koggukonna maias:
Kohtuwannem Hindrek Anton
Körwasmehhed Niggulas Rosenbaum, Hans Teppan
Kohtu kirjutaja Friedrich W. Schotter
Kohtukäiaid ei olnud, ei ka mingi suggust asja selletada.
Kohtuwannem XXX Hindrek Anton
Körwasmehhed XXX Niggulas Rosenbaum
Hans Teppan [allkiri]
Kohtukirjutaja Friedrich W. Schotter
