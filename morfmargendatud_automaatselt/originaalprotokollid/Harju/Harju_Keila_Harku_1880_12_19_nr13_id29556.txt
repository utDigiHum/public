I Mõisa wallitsuse poolt sai kohto ette pandud et Christjan Tahwer on mõisa metsast 3. Kuuse tüükast 2 tolli jämetad raiunud.
II, Kutsuti Kohto ette Christjan Tahwer ja kaebtus sai temale ette pandud; Tema tunnistas süüd tõeks ta tahtnud sugulastele Jõulo puudeks wiia.
III Kohto otsuseks anti, et Christjan Tahwer on need 3 Kuuske warrastanud, saab tema 15 hoopi witsu.
Kohtowanem: Jürij Saalfeldt XXX
Kõrwasmehes Heinrich Hein XXX Karel Pijrkel XXX
Kirjutaja: G Steinberg &lt;allkiri&gt;
Sai sel pääwal 15 h. w. kätte.
??? d. 3 Januar 1887
?????
G. baron Meijenderff
