1909 aastal Webruari kuu 25 päewal ilmusiwad Harku walla kohtusse kohtule isiklikult tuntud Baldiski linna kodanik Karl August Roiser ja Harku walla liige Hindrek Juhani poeg Luurman kelledel seaduslik woli on wabatahtlisi aktisid teha ja palusiwat kohut eneste wahel tehtud kirjaliku ostu lepingut akti raamatusse sisse kirjutada.
Leping: Tanasel 15 Webruaril 1909 a on Karl August Roiseri ja Hindrek Luurmani wahel järgmine talukoha ostu leping maha tehtud. Karl August Roiser müüb oma Wäena mõisa järel olewa Pikkasedi talukoha, kelle suurus 16 tiinu 510 ruutsülda on ja mõisa mima hind ukstuhat seitsesada rubla on, Hindrek Luurmanile, kes wiimane, kui talukoha ostja Karel August Roiserile peale mõisa muima hinna /1700 r) weel wiissada rubla koha parandamise ja eesoigustest lahti ütlemise eest maksab, koliraha maksab Hindrek Luurman Karl August Roiserile kolm kümmend rubla Moisa omanikule sisse makstud ostukapitali ükssada wiiskümmend rubl. maksab Hindrek Luurman uhes 5% sisse maksu ajast Septembri kuust 1908 a peale Karl August Roiserile tagasi.
Koha äraandmine müja poolt ja wastuwotmine ostja poolt sünnib 25 Martsil 1909 a. Käsiraha kauba kinnituseks maksab Hindrek Luurman Karl Roiserile tänasel 15 Webr. 1909 kakssada wiiskümmend rubla sisse, mis Karl August Roiser Hindrek Luurmanile kahekordselt tagasi lubab maksta selle lepingu murdmise korral, tema, müüja poolt Wäänas 15 Webr. 1909 a. koha müüja K. A. Roiser &lt;allkiri&gt; Koha ostja; Hindrek Luurman &lt;allkiri&gt;
Tunnistaja Chr Audum &lt;allkiri&gt;
See leping on pärast akti raamatusse sisse kirjutamist pooltele ette loetud ja nende poolt allakirjutud mida oma allkirjadega kinnitame
Kohtuesimees O Reinde  &lt;allkiri&gt;
Liikmed  H Meri  &lt;allkiri&gt;
                A Otsman  &lt;allkiri&gt;
                R Korsen  &lt;allkiri&gt;
Kirjutaja Kask &lt;allkiri&gt;
&lt;venekeelne tekst&gt;
Hindrek Luurman &lt;allkiri&gt;
1909 aastal Martsi kuu 4 paewal tunnistan oma allkirjaga, et Pikkasoodi koha peal pere oues puust ait, kus agerik ostas on, mitte minu poolt ostetud koha ostu lepingusse ei käi, sest see pidada endise koha omaniku Karl Roiseri ema Mari omadus olema
alguskirjast waljakirjutud ja õige
Kohtuesimees O. Reinde
Kirjutaja Kask
1909 aastal Martsi kuu 11 paewal annan oma allkirja selle üle et olen Hindrek Luurmani käest kakssada wiiskümmend /250/ rubla ja koliraha kolmkümmend /30/ rubla katte saanud ja ütlen ennast Pikkasodi koha eesõigustest lahti ja ei ole enam mull ühtegi noudmist sellest kohast.
Karl Roiser &lt;allkiri&gt;
Kohtuesimees O Reinde  &lt;allkiri&gt;
Liikmed  H Meri  &lt;allkiri&gt;
            R Korsen &lt;allkiri&gt;   A Otsman  &lt;allkiri&gt;
Kirjutaja Kask &lt;allkiri&gt;
