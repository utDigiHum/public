Nissi kihhelkonna kohto herrapolest Russalo Moisas sel 19mal Febral 1868 No49
Keige Nissi kihhelkonna walla wallitsustele
Ümberkäia kirri.
Järrel seisawad Eestima kubberneri herra kiria sest 19mast künla ku päwast s.a. No15 mis Saksa kelest on ümberpandud, ollen minna seläbbi igga walla wallitsussele, truwist läbbiluggemisseks; tähhelepannemisseks ja täitmisseks, kus see tarwis peaks ollema, - tahtnud ette panna, - ja käskida et igga walla wallitsus; sedda omma wollimeste protocolli ramatusse peab ärra kiriutama, - selle järrel omma wollimeste koggo kokko wõtma, sedda kiria neile ette luggema ja täiest arraselletama, ja selle ülle noupiddama mis seal sees seisab, - ja keige hiliem essimesseks Paasto ku päwaks minnule Tallitaiatte läbbi ommad Protokollid selle nou piddamisse ja ärraarwamisse ülle ette naitama, et minna õigel aial kubberneri herrale selle ülle otsust woin anda.
Igga walla wallitsusse murre on, et se kirri allamal seisawad korda möda, ilma wibimatta saab eddasi läkkitud ja iggaühhe nimme järrel tähhendadud milla see kirri on läbbi käinud.
Lehhetas sel 25mal künala kuu päwal s.a. sadetud Riseperre.
