Soldat Mart Pappa kaebab:Kopleaugo perremees Hans Kant on temma heinad mis temma omma õdde metsast pri kohha pealt tehha lasknud,ärra winud,ilma teada andmatta.2 sado heina,öölnud omma heinama ollema.
Hans Kant Kopleaugo perremees tunnistab:Temma on need heinad ilma teada andmatta ärra winud sepärrast et ilma temma lubbata need heinad temma heinama pealt tehtud.
Mai Pappa tunnistab:Perremehhe hein on jo tehtud olnud kui temma sedda metsa allust on teinud ja siit ja sealt se 2 sado heino mõisa metsa alt perremehhe heinama kõrwast korjand.
Tomas Wiiso tunnistab:Raia egga sihti ei pea seal wahhel ollema waid mets,mõisa mets,ja sealt perremehhe heinama äärt möda on mai need heinad korjand.
Kohto arwamist möda need 2 sado heino 1 koorm arwata 3 Rubla wäärt,sedda kahjo poleks wõtta,kohhus mõistis:Hans Kant peab Mart Pappale nende heinde eest 1 Rbl.50 Kop.maksma et need heinad ärra tonud,ja 1 Rbl.50 Kop.jäeb Mart Pappa kahjo et ilma lubbata teinud.
