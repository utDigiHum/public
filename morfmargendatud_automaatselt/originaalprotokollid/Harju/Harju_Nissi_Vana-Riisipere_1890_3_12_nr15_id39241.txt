Töökontrakt.
Riisepere mõisa wallitsusse ja Kulli peremehe wahhel (Juhhan Rosswald) on tänasel pääwal all nimmetud töö ülle see kindel kaup tehtud, et Juhhan Rosswald peab mõisa kässo järele:1 tundrema rukkid leikama ja kokkopannema			4 rbl.		15 kartuhwliwotmise pääwa tegema			5 rbl. 25 kop.		Summa			4 rbl. 25 kop.		
Juhhan Roswald lubab ülewal nimetud tööd truuwiste ja heaste ilma mingi suguse wastupannemisega nii pea ärra teha, kui mõisa poolt saab käskantud
Kui ta jattab mönned tööd tegemata siis ta ei sa selle tehtud tööde eest.
ühtegi maksetud. Se kontrakt maksab Jürri pääwast 1890 nii kaua kui üks kumbagi kaubateggijad Jakobi pääwal aasta ette üllesutleb.
Mihkli pääwal saawad tööd selletud ja maksetud.
Ärakiri 2es eksemplaris sellsammal pääwal wäljaantud.
