1896 aastal Juuli kuu 26 päewal Riisipere wallast Riisipere kogukonna talupojad Jüri Hansu poeg Root ja Reino lesk Wiio Põldmann, kes kohtule isiklikult tuttawad on palusiwad Riisepere Walla kohtu lepingu raamatusse oma suu sõnalist järgmist lepingud kirjutada:
Leidi sauna Wiio Põldmann ja Tuuliko sauna Jüri Root wahetawad Riisiperest omad rendi kohad tänasest päewast saadik ära, nõnda, et edaspidi Wiio Põldmann jäeb Tuuliko sauna rentnikuks ja Jüri Põldmann Leidi sauna rentnikuks ja täidawad nende kohtade rendi tingimised nii kudas nemad seatud on, ehk seatud saawad. Siin juures paniwad nemad Riisipere moisa omaniku Karlotto Baron Stackelbergi lubakirja selle üle kohtule ette.
See leping on pärast lepingu raamatusse kirjutamist pooltele ette loetud.
XXX Jüri Root
XXX Wiio Põldmann
Pooled kirjutada ei mõistnud ja paniwad nime asemel ristid, mis kohtu poolest õigeks saab tunnistud.President J.Sesmiin [allkiri]Liikmed H Gefria [allkiri]J.Linroos [allkiri]J. Simson [allkiri]Kirjutaja Saan [allkiri]
