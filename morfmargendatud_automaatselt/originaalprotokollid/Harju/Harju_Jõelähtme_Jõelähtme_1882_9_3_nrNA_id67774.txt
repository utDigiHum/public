Kaebaja Kostiwerre talitaja Mart Kuusman ütleb:  et pühapääw sel 1.sel august on tema Nida kõrtsu ees maandi peal Kostiwerre ja Jaggala meeste wahel olnud ja on neid keelnud wastastiku riido minemast siis tulnud Jaggalast Mihkel Remmel ja lönud teda neli woi wiis korda.
Kaebataw Mihkel Remmel Jaggalast ütleb: et tema ei ole kedagi lönud, waid tema on ise lüa sanud nenda et ninnast on weri wälja tulnud aga tema ei ole ise mitte seda löjad tund, waid parast on ta teiste käest kuulnud et se löja pidand Kostiwere talitaja olema.
Tunnistus Jegelehtme talitaja abimees Gustas Kiwwimäe ütleb: et tema on seda näinud kui Mihkel Remmel on Kostiwerre talitajad lönud, aga seda ei ole ta mitte näinud, et se talitaja Mihkel Remmeli on lönud.
Tunnistus Hans Liaks Kostiwerrest ütleb: et tema on näinud kui Mihkel Remmel on nende talitaja rinnust kinni wõtnud ja siis mone korra lönud.
Kohus arwas seda kaebtust Hakenrichti herra juure saata.
Protokoll Hakenrichti herra juure sel 20. Septbr 1882.
