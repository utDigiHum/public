I.) Wallitsesid ja mõistsid: Ue Tallitaja ja ka Abbimehhe perre kohha rentnikude seast; sest et wannade aeg, kolm aastat jubba ümber olli ja sepärrast seadust möda lahti mõistetud.
Wallitsemas ollid:
1.) Perrekohha rentnikudJuhhani perremees			Jürri Reinberg.		Jago " "			Kustas Waldmann.		Pajo " "			Jaan Wohlmann.		Leppiko " "			Jürri Hoidorff.		Waino " "			Jaan Simson.		Kusiko " "			Juhhan Guutmann.		Rähnima " "			Jürri Lindros.		
2.) Pääwa kohha piddajad:Wanna Karjama			Jaan Hoidorf.		Kesk Karjama			Jaan Petoferr.		Tatrama " "			Willeme Hoffmann.		Matliko " "			Jürri Trump.		Sep.			Mart Wakker.		Pajo S.			Hindrik Sepp.		
3.) Ilma maadeta liikmed kes omma käe peal
Juhhan Lindmann.
Andres Wohlmann.
Andres Simson.
Andres Ewart.
Jürri Hoidorff.
Jaan Hoidorff.
Need üllemal nimmetud mehhed wallsesid ja mõistsid: Jago kohha rentnik Kustas Waldmani ueks Tallitajaks, Rähnima Jürri Lindrosi assemele, ja Juhhani perremehe Jürri Reinbergi abbimehheks, Waino Jaan Simsoni assemele.
Ja üks uus wollimees sai mõistetud, päwa kohha piddajade seast: Paio Sauna Hindrik Sepp. Soniko Jürri Trumpi assemele.​​​​​​​
Lehheta walla politsei ülle watamisse ja järrel kuulamise all.
Jürri Lindros XXX
