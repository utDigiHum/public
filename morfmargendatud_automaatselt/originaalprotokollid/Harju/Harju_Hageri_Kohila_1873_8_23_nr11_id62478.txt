Astus ette:
Jaan Nimmerfeldt tunnistab et Pendi Jaan Kärrakas on Moisa heinama ärra niitnud sedda heinamad on olnud poolteist sammo wähhem kui pole wakkama ja neid heino mis seält peält on logus olnud kuus leisikast olnud sedda tunnistab Tallitaja abbimees Jürri Nimmerfeldt temma on neid järrele watanud.
Jaan Kärrakas tunnistab et temma ei olle mitte sedda heinamad muido nitnud kui Moisa Aidamees on tedda käskinud sedda ärra nita ja on pannud tundrema posti sinna kohta siis on Jaan sedda ärra nitnud.
Kohhus on moistnud et Pendi Jaan Kärrakas wiib need heinad mis temma on Moisa ma peält teinud 12 leisikat Moisa. On selletud.
