Astus kohtu ette teumehe naine Leena Kanep ja kaebas, et teised naised Ella Wiitus ja Mari Pikwer ola tema sahwri aknast tuhka tema sööma kraami peale wiskanud, ja temo tüttart, kes wõera poisiga sahwris puhkasid äbematult sõimanud.
Marri Pikwer ja Ellu Wiitus ei ütle seda mitte tõsi olewad ja tunistawad, et Leeno Kanep on neid enne hakkanud sõimama ja siis olla ta Ellu Wiituse silmad sisniseks löönud.
Kohus leidis, et kõik süüdlased olid, ja mõistis Leeno Kanepi ja Marri Pikweri 24 tunni ja Ellu Wiituse 12l tunni wangi trahwi olla.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Sai trahwitud.
