1905 A. mai kuu 10 pääwal Harku walla talupoeg Karl Karle p. Krumm ja Paide Kreisi Orisaare walla talupoeg, kes kohtule isiklikult tuntud, ja kellel õigus on wabatahtlisi aktisi teha, palusiwad nendest sisse antud järgmist kirjaliku lepingut, kohtu lepingute raamatusse sisse kirjutada: 
"Leping. Harku walla talupoja Karl Karle p. Krummi ja Paide Kreisi Orrisaare walla talup. Mart Karle p. Kanne wahel on järgmine leping tehtud.
1) Karl Karle poeg Krumm, kui praegune Undiaugu koha omanik, Kadaka külas, annab oma Undiaugu koha krundist Harku mõisa piiri ääres, wastu kadaka küla teed, Mart Karle p. Kannele üks sada ruut sülda /100 □ sülda/ põllu maad, temale maja ehituse krundiks, ühe kordse maksu seitsme kümne /70 rubl/ eest, üheksa kümne üheksa aasta peale selle lepingu kinnitusest arwatu, pruukida, kõigi nende õiguste ja kitsendustega, mis Undiaugu talu kohta maksewad on.
2) Rendile wõtja Mart Kanne on ära maksnud tähendud summast kaks kümmend wiis rubl /25 rubl) puuduwa summa nelikümmend wiis rub (45 rubl) maksab ühe aasta jooksul Krummile
3, Rentija Mart Kanne wõib oma õigust selle lepingu põhjal ilma Undiaugu koha omaniku nõuusolemiseta edasi anda ehk müüa
4= Rendi tähtaja lõpemisel ei wõi Undiaugu koha omanik neid hooneid ehl ehitusi, mis Kannele renditud maa tüki peal on, omale pärida
See leping on pärast akti raamatusse kirjutamist pooltele ette loetud ja nende poolt alla kirjutud, mis meie sellega õigeks tunnistame.
Stempel maks 40 kop. makstud.
Kohtu Eesistuja M. Jerwan
Liikmed J. Вальямя &lt;allkiri&gt;
H Meri &lt;allkiri&gt;
Kirjutaja J. Rammus &lt;allkiri&gt;
Algus kirja lepingust olen kätte saanud 10 Mail 1905
M Kanne &lt;allkiri&gt;
