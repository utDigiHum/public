Ette tuli Kaebaja Anna Sunder ja kaebas et tema peremees Eriko Willem Sinep on tahtnud teda ära naerda ja on kolmel koral tema kallale kipunud, et ta aga mitte pöle tema nõusse heitnud siis aetud teda Jakop pääwa aeg ära, aga nende kaup olnud aasta peale Jüripääwast Jüri pääwa ja peremees ei taha ka temale mitte aasta palka maksta.
Ette sai kutsutud nemetud peremees W. Sinep ja tunistas, et ta olla üks kord nalja pärast tüdruku kallal haukunud ja pärast ei olla tüdruk teda enam rahule jätnud waid teda alati turkinud siis ei ole perenaene enam seda mitte sallinud ja aeanud tüdruku ära sepärast et selle asja pärast kartnud.
Kohus mõistis asja mitmet pidi järele kuulades et nemad ei wõi mitte enam ilu puhtuse pärast koos elada ja peremees maksab temale poole aasta palga se teeb 13 rub 50 kop. ja peale selle 3 rubla trahwi et ei mõista mitte kui peremees elada.
Sellega oli kohus löpetud.
Kohtowanem K Gutmann XXX
Kõrwasmehed J Kallas XXX ja W Runsmann XXX
Kirjutaja M Käärt &lt;allkiri&gt;
Mõistus täidetud sel samal pääwal
