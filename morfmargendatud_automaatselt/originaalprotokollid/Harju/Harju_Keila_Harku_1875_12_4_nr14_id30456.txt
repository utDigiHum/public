Ete tuli kaebaja Ana Kaskirilow ja kaebas, et möda läinud puhapäwal Sõrwe lugemisele minnes on Sõrwest Mart Pronsfeldi koer tema palito lõhki kiskunud tulnud wärawa taha tema kalale.
M. Pronsfeld sai ete kutsutud ja küsitud kas ta koer seda ene ka on teinud, aga tema wastas, et se olnud esimine kord ja ta ola warsti sele peale koera maha lönud.
Kohus watas kahjo järele ja leidis, et palito selja tagune lõhki oli kistud kaksipidi.
Kohus mõistis asja läbi kulates et M. Pronsfeld peab A Kaskirilowile maksma 1 rub. 60 kop. se on 3 jagu kahjo tasumist üks jägu jääb A. K. kahjuks kanda.
Kohtualused olid selega lepitud
Kohtowanem K Gutmann XXX
Kõrwasmehed O. Mänd XXX ja W. Mänd XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
Mõistus täidetud selsamal päwal.
?? 5 Januar 1876 ?? &lt;allkiri&gt;
