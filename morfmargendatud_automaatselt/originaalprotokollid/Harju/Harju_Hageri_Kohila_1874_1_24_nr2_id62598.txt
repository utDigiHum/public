Ja Juhhan Jürgens tulli ette, ja ütles, et Juhhan Prenner on Kõrtso letti tagga olnud, ja Mart Karp on tulnud, ja lönud tedda üks kord käega möda pead, ja lönud Juhhan Prenneri pitkale mahha, ja Juhhani oma naene on siis appi tulnud, ja lükkanud ta Kõrtsi tahha Kambre, ja Juhhan Jürgens põlle mitte sedda teist korda lömist näinud.
Ja Luige Jaan Nimmerfeld tulli ette ja ütles et ta põlle mitte nende rido egga lömist näinud, muud kui et Juhan Prenner ja Mart Karp on teine teisega waidlenud, miks agga Mart Karp on Juhhan Prenneri wankret watanud.
Ja neil ei olnud mitte ühhesuggused egga tõssised tunnistussed, ja Kohhus ei wõinud selle tunnistussega mitte selletada.
Ja nimmetud Juhhan Prenner ja Mart Karp leppisid teine teisega ärra, siis olli nende riid otsas.
