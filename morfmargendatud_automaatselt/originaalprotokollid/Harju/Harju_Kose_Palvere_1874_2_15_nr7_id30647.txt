Kohtu korra pärrast ollid Rawwila ja Palwerre walla kohtusse walla hones kokko tulnud:Kohto wannem Hinrek Nokkur
                                                                                                                                                        Kohto käemehhed:Mihkel Krusement
                                                                                                                                                                                          Jakob Käit
                                                                                                                                                                   Kirjotaja Hans Siimson
Trigi wallast Hans Wirla kaebab:Temma on ühhe kue ostnud Kosse kaupmehhe käest 4 Rubla eest,ja sedda on nüüd Kossel Kiwwilo mees Jaan Sarw temma käest ärrapärrinud,et se temma jaggo olnud ja temmalt ärrawarrastanud;ja temma nüüd ommast ilma jänud,ja nõuab sedda kahjo tassutud sada.
Jaan Sarw,Kiwwilo mees,tunnistab:Temma on Trigi mehhe selgas omma kue tunnud Kosse kirriko al ja on sedda ärra pärrinud,sepärrast et se temma omma olnud.
Kaupmees Schmiedehelm tunnistab:Temma on minnewa talwe selle kue omma padis päwa aial ühhe nore mehhe käest ostnud,kes öölnud ennast negruti ollema ja tahta omma kube ärramüa;agga se mees on temmale tundmatta olnud,kes ta olnud,egga wõiks tedda praego mitte tunda.
Et neil iggaühhel õigus on omma nõuda;agga siiski se teadmatta kes sedda kube warrastanud,sai neile kohto poolt nõu antud iggaühte omma poolt kahjo kanda wõtta ja nenda ühhes kous leppida.
Kaupmees Schmiedehelm lubbab omma poolt pool selle kue hinda 2 rubla mahha jätta.
Teised agga ei tahha essite omma poolt kumbgi middagi kautada,tene nõuab omma kube ja teine omma 4 Rubla.Wimaks Kiwwilo mees Jaan Sarw lubbab kue Trigi mehhele taggasi anda,kui temma 1 Rubla temmale maksab kue prukimist,et aasta otsa kube prukinud,ja kui se kaupmehhest lubbatud 2 rubla ka temmale saab,siis temma wõttab kaubeldud kue hinnast 1 Rubla kahjo.
Ja et se kuub weel 5 Rubla wäärt arwata;sai kohto poolt Trigi mehhe Hans Wirlale nõu antud se rubla maksta ja sel wisil leppitust tehha.
Hans Wirla maksis se 1 Rubla ja kaupmees 2 Rubla Jaan Sarwele wälja ja kuub sai Hans Wirla kätte,ja leppisid ärra.
Kohto wannem Hinrek Nokkur XXX
Kohto käemehhed:Mihkel Krusement XXX
                                  Jakob Käit XXX
Kirjotaja Hans Siimson
