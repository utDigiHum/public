Astus kohtu ette Kiwinoka perenaese tütruk Katharine Ormann ja kaebas tema perenaene Liisu Annus tahab teda kese aastad, wastu talwe lahti lasta ja ei juhata temale mingi tood kätte ja kiusab teda ega maksa wana aasta palka ära.
Liisu Annus sai ette kutsutud, tema ütles, et tütruk olla sõna kuulmata ja wastane, ei taha seda teha, mis leiwawanem temale tööks annab ja haugub igas asjas wastu.
Kattarine Orm. tunnistas tema polle millalgi wastu rääkinud, ainult ühe körra sügise Mardipääwa aeg, kui perenaene teda aeas palja kättega pöllu pealt kartulid korjama, ega annud kindaid kätte.
Kohtu Otsus:
Tütruk Kattarine Ormann ja perenaene Liisa Annus peawad ära leppima ja perenaene ei tohi tutrukud ilma süüta kese aastad ära aeada ega tema palka kinni pidada. Naad leppisid kohtu ees wastastiku kokku.
Peakohtumees: Juhan Humberg XXX
kõrwasmehed Gusta Reindes XXX
                          Reinhold Korsen XXX
Leppitud
