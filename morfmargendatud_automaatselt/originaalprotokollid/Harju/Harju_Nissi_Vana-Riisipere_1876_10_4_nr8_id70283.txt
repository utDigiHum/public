Kohto ette tulli Riesepere moisa wallitsuse kaebdus sest 26. Septembrest: et ollid teomehed Leidi s. Hans, Mäe Jakob ja Simani Jurri tahtnud raud kiwwi, kaerte sisse mässitud, auro peksu massina alla panna, et massinad katki tehha; sedda kiwi olla leidnud Wannaõue teomees Juhann kes kaero massina peale angus ja et Jaan Gutmann ja Pärna Jurri Tiege sedda woida tunnistada
1) Kohto ette astus Jaan Gutman kes moisa peksu masina jures järrel waatmise ammetis olli ja ütles kohtu ette: et tema kül sedda kiwi polle näinud agga, nimelt Pärna Jurri Tiege ja Wannaõue Juhani käest kuulnud, kes rehhe massinas kaero alla pannid ja sedda kiwi olla leidnud kaerte sisse mässitud ja 3me siddemega seutud ollewad
2) Kohto ette astus kaebduse allune Mäe Jakob Nokas, kes künes kaero kätte annud ja arwatud et tema sedda kiwi kaerte sisse mässinud ja ütles et tema sedda kiwi polle mitte kaerte sisse pannud
3) Simani Jurri, teine kaebduse allune, astus kohto ette ja ütles: et tema sedda kiwi polle kaerte sisse pannud ja et temmal künes seks aega polle olnud ja koggoni sedda kiwi polle nainudgi
4) Kolmas kaebduse allune, Leidi s Hans, olli enne kui ta käsko sanud kohto ette tulla; Kollowerre ladale läinud
5) Ka olli Wannaõue Juhhan enne kohto käsko ladale läinud
6) Pärna Jurri Tiege polle kohto ette tulnud
Kohhus moistis: 1) sedda asja tullewa kohto päwal kui keik tunnistuse? ette tullewad weel selletada ja otsust anda. 2) Et Pärna Jurri Tiege, kui selget tunnistust wabbanduseks ei olle, peab selle eest et ta kässo järrel polle kohto ette tulnud 50 kop. trahwi maksma.
Kohtopeawannem Jakob Winkel XXX
Korwamees Hans Surow XXX
kirjotaja BGildemann [allkiri]
