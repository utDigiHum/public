16
Kohto ette Kaewas Riseperre mõisa wallitseja herra Welding et Hie perremees Hans Kruus omma tö kontrakti mitte ei pea, temma kaup olli et piddi mõisa tööd teggema:10 Karduhwli wõtmisse päwa  rahha			3rbl 50 kop		1 tündrema heina tehja			2 " 40 "		1 " rukki lõikust			4 "		2 " suiwilja niita			3 "		Summa			12 rbl. 90 kop.		
sedda tööd on Hans Kruus kõik teggematta jätnud.
Kaebtusse kandja Hie perremees Hans Kruus astus kohto ette, ja kohto poolt sai temma käest küssitud mikspärrast sa kaubeldud tööd mitte ärra ei tee?  temma ütleb selle wasto: minno poeg olli talwe wiina wabrikus tööl, ja wallitseja herra wõttis palgast 5 rubla ärra, sellepärrast ei julgenud ma tööd tehha et ka wallitseja herra sedda rahha ärra wõttab.
Wallitseja herra Welding ütleb selle wasto et 5 rubla on poisi palgast sellepärrast ärrawõetud et temma wabrikus on koerust teinud.
Wallitseja herra Welding, ja Hie perremees Hans Kruus leppiwad kohto ees nõnda, Hans Kruus lubbab omma tääd ärra tehja, ja Wallitseja herra maksab 3 rubla sest rahhast mistalwe temma poia palgast trahwiks wõetud Hans Kruusile taggasi kätte siin kohto ees.
Kohhus mõistis: et se nõnda jääb kuddas nemmad isse on leppinud.
