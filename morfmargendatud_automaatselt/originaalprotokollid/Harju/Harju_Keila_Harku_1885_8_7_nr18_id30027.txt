Astus kohtu ette Harku moisa walitseja ja kaebas et Mäe kõrtsus Juhan Humberg olla ilma lubata mõisa metsas Jahi peal käinud ja Baruni härra nõuab, et teda peab walla kohus karistama.
Juhan Humberg sai ette kutsutud, tema waitles selle kaebtuse wastu ja ütles et ta olla püssiga oma heinamaa pealt läbi mõisa metsa tahtnud kodu minna ja seal olla teumehe naene Ingel Kattu teda näinud pussiga tulewad, Ingel Kattu tunnistas, et Juhan Humberg olla pussi lasknud moisa metsas.
Kohtu otsus: Juhan Humberg peab 24 tundi puuris istuma, selle trahwiks, miks tema ilma lubata üle keelu mõisa metsas käib püssi laskmas.
Peakohtumees: Juhan Humberg XXX
korwas mehed Gustav Reindes XXX
ja                       Hindrik Sarapuu XXX
täidetud.
