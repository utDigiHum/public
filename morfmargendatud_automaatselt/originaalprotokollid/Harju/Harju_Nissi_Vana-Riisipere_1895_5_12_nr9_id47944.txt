1895 aastal Mai kuu 12 päewal Nurmest Riisipere walla talupojad, kes kohtule isiklikult tuttawad on palusid selle kohtu lepingute raamatusse oma suusõnalist järgmist lepingut kirjutada: Kustas Meinwalt ostis Juhan Sepateusse käest üks tessatin maad /50/ wiiekumne rubla eest ja tunnistame sellega, et mõlemad selle mõõdu ja raeatega rahul oleme mis maamõõtja Aleksander Kalf selle kohto on teinud
Kustas Meinwald [allkiri]
Et Juhan Sepateus ise kirjutada ei oska siis kirjutas tema palwe peale alla
Juhha Jaan Meinwald [allkiri]
See leping on pärast lepingute raamatusse kirjutamist pooldele ette loetud.
Kohtu Eesistuja J.Sesmiin [allkiri]Liikmed H Gefria [allkiri]J.Linroos [allkiri]J. Simson [allkiri]Kirjutaja Orgusaar [allkiri]Kopiju palutšilK Meinwald [allkiri]
