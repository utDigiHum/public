I)
Kirikherra kaebduse peal et neid Saunikud Söero, Milima, Urge ja Tammiko mitte omma kiriko päwad polle teinud.
Tamiko Jula wastab et sel päwal, kui päwa teggemist tellitud olli, et siis wihma saddanud, ja pärrast polle mitte enam woetud. (on rahha 55 siin kohto laua jures maks)
Urge Hans Mühlberg üttleb sed sama, et wihma saddanud sel tellitud päwal ja lubba rahhaga maksma, 55 kopik ja warsti 2 Nowembril makstud.
Milima Jürri Wilpert, ja Söero Trino Wilokas polle mitte tänna kohto ette tulnud.
Kohus moistab et need wimased peale need 55 kopik weel trahwi peab maksma 25 kop, kui mitte 2 nadala sees polle makst.
