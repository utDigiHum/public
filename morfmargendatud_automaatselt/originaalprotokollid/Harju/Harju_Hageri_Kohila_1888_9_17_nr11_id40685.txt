2.
Tuli ette: Angerja wallast Joosep Taruutin 25 aastat wana, Luteruse usku, Märtsis s.a. laual käinud, kaebab: Lohult Ottu Paemurd ütles et meie Jüri Kärmes ja Mart Aro oleme tema lõõtsapilli ära warastanud ja et meie oleme pilliga Röa kõrtsus käinud, meie palume et ta need inimesed kaasa toob kes meie käes on pilli näinud.
Kabalast Hans Reitmann 28 aast w. Lutteruse usku, kewadi s.a. wiimist korda laual käinud, tunnistab: Pill kadus ära, mina ei tea kes ta warastas; seda mina kuulsin kui Ottu Paemurd ütles: Joosep Taruutin, sa oled mu pilli ära warastanud! Kärmasele ja Arole ei ole tema minu kuuldes mitte ütelnud et nad on pilli warastanud.
Hans Kollits Kuusiku mees 21a. Luteruse usku kewadi s.a. laual olnud tunnistab seda sama mis Reitmann.
See protukoll on nendele ette loetud ja nad ütlewad, et see tõsi on mis nad on rääkinud ja et see õieti on üles kirjutud.
Mõistus: Rahu kohtu seadus § 130. Ottu Paemurd sai noomitud et ta Joosep Taruutinile ütelnud pilliwaras, aga temal selle kohta kõige wähemat tunnistust ei ole. Ta wõib warast otsida, aga mitte kellegile ütelda, waras!
