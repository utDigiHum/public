Kohtu korra pärrast ollid Rawwila ja Palwerre walla kohtusse walla hones kokko tulnud:Kohto wannem Hinrek Nokkur
                                                                                                                                                        Kohto käemehhed:Mihkel Krusement
                                                                                                                                                                                          Jakob Käit
                                                                                                                                                                   Kirjotaja Hans Siimson
Ette astus Lenderma külla karjane Hinrek Bera,kaebab:Nõmme perremees Hans on tedda karwust kiskunud ja lönud et temma weiksed nattoke ollid Nõmme perre wäljale läinud.
Nõmme perremees Hans Nokkur tunnistab:Mittokord on karjane weiksed temma wäljale lasknud tulla,on temma heinama wäljal ärra söötnud ja ial põlle karjast ennast näinud,sel päwal on 2 kord sanud wäljaaetud ja sekord 36 loiust korraga wäljal olnud ja et ta tedda maggamisse pealt siiski leidnud,on temma tedda karwust sassinud.
Hinrek Bera ei wõi nende tunnistuste wasto seista.
Kohto poolt sai Bera kaebdus tühjaks kaebduseks arwatud ja sai kästud neid Nõmme perremehhega ärra leppida,ja Bera sai omma holetusse pärrast nomitud,Leppisid ärra.
