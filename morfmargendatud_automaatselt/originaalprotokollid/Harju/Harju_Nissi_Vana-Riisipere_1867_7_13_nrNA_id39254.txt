Nurme moisa Polletsei annab selle kohto läbbi teata et tullewa Jurripä wast sawad ned konrattid tehtudja sellepär rast ja sellepärrast sai nende all nimme tud perremeestele kohto ees ööltud et nem mad peawad kahhe näddala aial moisa tullema huut kaupa ja konratti teggema
Perremeeste kohhad
1. Pärdi
2 Üllewaino
3 Maddise
4 Kahhoma
5 Nõmme
6 Rüütle
7 Matso
8 Wannajürri
9 Uetoa
10 Mikko
11 Jani
12 Sookrana
13 Mäe
14 Leppiko
15 Hanso
16 Mikko
17 Södi
18 Kiwwimatso
19 Keldrema
20 Kiwari
21 Rätsepa
22 Suretoa
23 Koilo
24 Losso
25 Kesknomme
26 Arro
27 Joera
Wabbatikkode kohhad
1 Wanna jürri saun
2 Mikko s
3 Matso s
4 Mäe s
5 Sookrana 2 sauna
6 Södi saun
7 Rätsepa s
8 Waino s
9 Janijürri s
10 Koilo 2 sauna
11 Losso 2 s
12 Kople 1 s
13 Ue s
14 Wäljataguse s
15 Pihho s
16 Saidaperre s
17 Saidakõrsi Kai
ja nüüd on ka nende keigile sedda kohto ees ööltud ja kahhe näddale par rast peawad jälle keik mõisas ollema
Kohto pea wannem Juhhan  Presman XXX 
kõrwamees Tõnno Pahkjalg XXX 
kõrwamees Jaan PetohwerXXX
