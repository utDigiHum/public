Sel tännasel päwal astus Lehheta walla koggokonna kohto ette, Lehheta walla pölitsei nimmel Juhani perre sullane Jaan Wohlmann ja tunnistas et temma olle omma perremest 9 aastat teninud, ja temma põllemitte omma palka keik kätte sanud, ja tunnistab peab ollema üks kuub ja püksid saada, ja perrenaene ei tahte mitte sedda kuube ja püksa ennam temmale tehha. Ja Juhani perrenaene Liso Reinberg sai sellepeale kohto ette kutsutud ja Liso Reinberg tunnistas et temmale ei olle mitte Jaan Wohlmannile ennam palka tehha et temma olle nimmetud sullase palka igga aasta järjest kätte teinud ja temmal ei olle mitte riide puudust ollnud, temma olle enneminne ommad lapsed ilma jätnud ja olle ikka selle nimmetud sullase Jaan Wohlmann palga igga aasta järjest kätte teinud.
muud kuid selle aasta ette mis temmal weel polel on se kub ja püksid on temmal weel saada ja need kuue ja püksid lubbab temma kätte tehha kui aasta aeg täis saab. Seest et nendel kummagil ei olnud tunnistussemeest, ei wõinud kohhos selle peale ühtige mõista.
Kohto mõistmisse jures ollid
koggokonna kohto peawannem: Jaan Wohlmann XXX
kohto kõrwamehhed:
Jaan Simson XXX
Jaan Hoidorf XXX
