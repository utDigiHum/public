1894goda Oktjabrja 14 dnja krestjanin Rizenbergskoi volosti i obšestva Jugan Kraut i poverennõi vladeltsa im. Rizenberg Nikalai fon Frei. litsnoizvestnõje volostnomu Sudu prosili o vnesenij v aktovuju knigu etogo Suda predjavlennõi imi pismennõi dogovor sledujusjego sodersanija.
Ehituse kaup.
Sütsi Juhhann Kraut wõtab oma peale kewadil 1894al enesel üks uus hello hoone oma kuluga üles ehitada. palgid ja material saawat mõisa poolt ilma rahata antud. Tema saab selle ehituse eest, palgi raiumise koorimise, wedamise, katuse õlgede tegemise, akendega, ustega tükkis üle pea /53 rub. 75 kop./ wiiskummend kolm rubla seitse kümmend wiis kopikud. Sütsi Juhan Kraut tunnistab et selle rahaga on kõik makstud ja koha äraandmise aeal ei ole temal enam ühtegi selle eest pärida
N Gafia [allkiri]
Juhhan  ???????? [allkiri]
Dogovor etot po vnesanij ego v aktovuju knigu bõl protšitan storonam podpisan imi, sto  isvidetelstvujem.
Predsetatel Sudo J.Semin [allkiri]
Tšlenõ J.Linroos [allkiri]
J.Simson [allkiri]
Pisar Ovgusar [allkiri]
