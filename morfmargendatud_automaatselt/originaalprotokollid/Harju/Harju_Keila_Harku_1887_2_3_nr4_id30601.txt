Ette tuli kaebaja Reinuld Korsen ja kaebas, Tema oma wend Karel Korsen olla tema kambri otsast aeast ühe linnupuu ära warastanud ja magus seest ära wõtnud ja linna wiinud.
Karel Korsen sai ette kutsutud tema tunnistas oma kurja tegu üles, ja ütles et olla seda teiste nouandmise peale joonud peaga teinud ja palus et wend temale pidi andeke andma.
Reinuld Korsen noudis oma kahju tasumist 12 Rbl.
Kohtu otsus: Karel Korsen maksab oma wennale linnupuu ette 10 Rbl. kahju tasumist, ja saab 20 witsa hoopi ihunuhtlust, miks tema oma wenna linupuud ära rikkus joonud peaga.
Kohtumees Juhan Humberg XXX
Korwasmehed Gustaw Reindes XXX Reinhold Korsen XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Leppitud ja sai witsa kätte. Reinhold Korsen on selle trahwi raha oma wenna Karelile kinkinud ja ei nõua enamb seda raha.
