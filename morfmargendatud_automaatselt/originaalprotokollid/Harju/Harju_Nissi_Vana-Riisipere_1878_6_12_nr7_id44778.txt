Kohto ette tulli Tännawaotsa Tõnno Miller ja kaewab kohto ees, et kirriko herra karjatsed minnewa ja selle aastane tehha sedda juttu temma peale, et ta Rieseperre innimeste käest rahha wõtnud se eest, et nende loomad wõida Nurme mõisa nõmmes pri pärrast käija.
Kohto ette astus minnewa aastane karjane Ann Janilind ja kohto poolt sai küssitud: kas sa olled räkinud, et Tännawaotsa Tõnno Rieseperre innimeste käest rahha wõttab?
Ei ma sedda öölnud - agga sedda ütlesin ma kirriko herra prouale, et olleks Tõnnole wiina anda egga ta siis kinni aeaks. Olled sa näinud mõnne Rieseperre innimese Tõnnole wiina andwad?
Ei olle näinud.
Kohto ette astus Marri Simberg ja sai kohto poolt küssitud: Olled sa räkinud, et Tõnno Rieseperre innimeste käest rahha wõttab? Ei ma sedda põlle räkinud; Ma kuulsin, kui Ann ütles prouale et olleks wiina anda, siis ma tõendasin takka no jah olleks wiina anda egga ta siis kinni aeaks.
Kohto otsus: kohus mõistis, et Ann Janilind peab maksma 50 kop trahwi ja Marri Limberg peab maksma 50 kop. trahwi Nurme walla laekase.
Kohto peawannem Hans Waltsnep XXXKõrwamehhed Jakob Lindmann XXXJuhan Simson XXXKirjutaja JArnak [allkiri]
Kohus mõistetud ja täidetud 12 Junil 1878.
