7.
II) Tuli ette: Maidlast Ojari peremees Jaan Kärrakas ja kaewab Madis Priimets on teda teutanud ja kartuwli augu wargaks teinud.
Madis Priimets tunnistab temale on, nüüd surnud Jüri Prousen mõne aasta eest rääkind, et Jaan Kerrakas olla tema heinamaal kartuwli koormaga tema wastu tulnud.
Kohus pakkus neile lepitust: Jaan Kerrakas ja Madis Priimets leppisid ja M. Priimets maksab J. Kerrakale 3 rubla teutamise eest.
Leppimist tunnistavad:Jaan Kerrakas XXXMadis Priimets XXX
