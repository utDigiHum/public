Sel 29mal Neäri k.p. 1871.
Kohtu korra pärrast ollid kokko tulnud Uemoisa koggukonna kohtus koggukonna maias. Kohtuwannem Hindrek Anton, korwasmehhed Niggulas Rosenbaum, Hans Teppan, kohtukirjotaja Friedrich W. Schotter.
Jaan Arjasep kaebab Jürri Mikkita peale, et kaebataw on temmale 3 Rub. 40 kop. höb wölgo, mis temma ei tahha maksta. Jürri Mikkita wastab: temma on sest wöllast 1 Rub. 77 kop. höb ärramaksnud ja mitte ennam 3 Rub. 40, waid 1 Rub. 63 kop. höb. mis temma ka ei olle keelnud maksta.
Et Jaan Arjasep ei wöi sallata, ennast kaebaja käest 1 Rub 63 kop. höb. wälja ja leppisid nemmad ärra. Leppisid issekeskis.
Kohtuwannem XXX Hindrek Anton, korwasmehhed XXX Niggulas Rosenbaum, Hans Teppan, kohtu kirjotaja Friedrich W. Schotter.
