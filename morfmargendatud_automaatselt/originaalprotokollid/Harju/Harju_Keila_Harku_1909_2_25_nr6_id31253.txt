Ukstuhat uheksasada uheksandamal aastal webruari kuu kahekumne wiiendamal päewal ilmusiwad Harku walla kohtusse kohtule isiklikult tuntud Harku walla liikmed Anna Prunsweldt Otto Lesk ja Aleksander Reinholdi poeg Korsen kelledel seaduslik woli on wabatahtlisi aktisid teha ja palusiwad et nende wahel tehtud suusonaline rendi kontrakt saaks kohtu akti raamatusse sisse kirjutud.
Aleksander Korsen wõttab Anna Prunsweldi käest Harku wallas Kodapää külas seiswa Jaagu koha maad oma katte rendi peale kuue aasta peale 23 apr. 1909 a hakkates kuni 23 apr 1915 aastani
Rent on üks sada rubla aastas mida rentnik kaks korda aastas peab maksma nimelt, iga Jüri paewaks 50 rubla ja iga Mihkli paewaks 50 rubla. Rent peab ette makstud saama.
Rentnik Aleksander Korsen wõttab enese peale selle koha jarel olewad maantee sillutamised; kõik muud kohustused jääwad koha omaniku Anna Prunsweldi kanda. Anna Prunswelt saab Jaagu koha maadest omale uhe wakkamaa põllumaad pruukida kodust toa juurest, ulewalt poolt teed ilma tasuta ja esimesel rendi aastal wiiskümmend puuda katuse õlgi maja parandamise tarwis kõik kodused hooned jääwad kohaomaniku Anna Prunsweldi pruukida, ainult metsa küünid jääwad rentniku kätte.
Rentnik wõib oma äranagemise ja soowi järele karjamaast uut polda juurde teha aga tall ei ole luba selle eest uhtegi tasu nõuda. Rentnik saab maad ilma sõnnikutta oma kätte ja annab ka ara rendi aja lopemisel ilma sõnnikuta. Rentnik peab, kui koha maad kääst ära annab põllud umber kündma. Kui kumbki pool peaks enne rendi aja lõpemist kontrakti murdma, peab teisele terwe aasta rendi juure maksma.
XXX tähendab Anna Prunswelt
Anna Prunsweldi eestkostja J Pals &lt;allkiri&gt;
A Korsen &lt;allkiri&gt;
See rendi kontrakt on peale akti raamatusse sisse kirjutamist pooltele ette loetud ja nende poolt allakirjutud niisama ka Anna Prunswelti eestkostja Juhan Palsu poolt allakirjutud mida oma allkirjadega kinnitame
Kohtuesimees O Reinde  &lt;allkiri&gt;
Liikmed  H Meri  &lt;allkiri&gt;
                A Otsman  &lt;allkiri&gt;
Kirjutaja Kask &lt;allkiri&gt;
1909 aast. 25 webr maksis A Korsen Anna Prunsweldile wiiskummend (50) rubla renti ara
kohtu esimees O. Rende &lt;allkiri&gt;
Kirjutaja Kask &lt;allkiri&gt;
