III)
Peter Hindus astub ette ja kaewab, et Tõnu Ulprecti poeg olla tema heina kuhja metsas põlema pannud. See heina kuhi on ühes wäikses koples olnud kus aeda mitte tarwis ei olnud ja on ka keeldus olnud, et sinna keegi ei tohi oma elajaid mitte laskta, aga Tõnu Ulprecht on oma elajad üle keelu sinna lasknud ja kui pois kuhja põlema on pannud siis on tema seal elajate karjas olnud, ja elajad on enne juba kuhja alt sau wäärt heinu ära söönud.
Tõnu Ulprecti naene Lenu ütleb, et tema elajad ei ole mitte kuhja söönud ega ka enne seal koples mitte käinud.
Kohus mõistab ja Peeter Hindus ka sellega leppib, et Lenu Ulprect maksab Peter Hindusele 120 Leisikat heinu ja nemad on teine teisega leppinud.
