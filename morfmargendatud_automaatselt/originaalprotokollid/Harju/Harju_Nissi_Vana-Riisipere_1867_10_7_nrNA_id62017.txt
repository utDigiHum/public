2) tulli ette kaewaja Kasiko Rein Reinperg ja kaewas omma poia Jürri peale et ei ande temmale mitte süa ennam kui temma wannaks sade nenda et temma ei sudaennam toed tehha ja kus temma siis sade kui temma ennam ei suda töed tehha 
ja Jürri kes nüüd Issa Reino assemel perremees on ütles süa ja jua minnaannan talle keik mis ma issi ja perre sööb
ja Rein ütles ei ma nüüd ka sömata polle jänud nüüd jouan weel töed teha agga kui ma wannaks saan kust ma siis wöttan
ja temma poeg Jürri ütles et Issa Rein tahhab need loiosed mis siis wassikad ollid nüüd ärra mua et need on temma jäggo ja tahhab ka perremehhe Jürri Heindega ennesele Obbost kaswadada ja nenda ennesele kasso tehha
ja kohto ees öölti temmale et leppi nüüd ärra omma poiaga ja mis maia lomad on need on nüüd Jürri sest temma on nüüd perremees ja sinna olle temma Issa kui perre söma lähheb siis minne ka sööma ja kui teised töle lähwad siis minne ka ja tee nendapaljo kui sudad ülle jou ei woi sinno käest ükski pärrida
ja Jürri lubbas omma joudo möda dedda toita ni kaua kui ellab ja leppisid kät andes teine teisega ärra!
