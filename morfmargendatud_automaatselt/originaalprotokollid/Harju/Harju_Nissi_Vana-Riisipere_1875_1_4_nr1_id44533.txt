Ollid kõus Tallitaja ja abbimehhed ette tullid walla koggu konna liikmed, wollimeeste seadmisse pärrast.
Ni kui se seadus on, et iggaaasta peab wollimehhed saama seatud, et kelle kolm aastad täis on, peab lahti saama, ja jälle ued assemele pandud.
a. Lahti saidPerremeeste hulgast			Losso Juhan Bekmann		Omma käe peal liikmede hulgast			Jurri Ingwerk		Perremeeste sullaste hulgast			Saida kõrtso Jürri Roswald		
b. Wallitsetud said.Perremeeste hulgast			Saida perre Juhan Paulerk		Omma käe peal liikmede hulgast			Söedi Hindrek Neimann		Perremeeste sullaste hulgast			Pärdi Jaan Waltsnep		
Sedda kinnitawad koggukonna nimmel abbimehedJürri Presmann XXX
Jaan Pahkjalg XXXTallitaja Jaan Willenthal [allkiri]
