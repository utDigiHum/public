Astus kohtu ette Harko moisa walitsus ja palus Truuta Laastri kaeptust üles kuulada.
Nimelt: Truuta Laaster kaebas moisa politseile, et tema linnast tulles ola näinud, et Mäe kõrtsu mehe emma ja õde ola 20ne Nowem õhtu kella 7 aegas Harku järwe ääres tema wastu tulnud ja on pool koorm jämedaid männa puu pakkusid peal olnud, tema ola mööda minnes taga hüüdnud aga naad ei ole temale mitte wastanu siis tulnud arwata weerand werst eemal metsawaht tema wastu  ta ola seda juhtumist metsawahile jutustanud ja metsawaht ola kohe taga läinud.
Metsawaht ütleb kül kohe Habesti järwe külasse läinud, aga ei ole sealt kõrtsmiku emad, ega ka puid leidnud siis tulnud ta kohe tagasi ja otsinud metsa läbi tema mets olla kõik terwe ja ilma puutumata, teisel pääwal jooksnud Truuta Laaster mõisa ja kaebanud seda Barunile, siin sai kõik Truuwist järel otsitud ja metsast ei olnud warastud.
Kortsmik sai ette kuulutud ja temale kaeptus ette loetud, tema tunistab seda waleks ja nouab kohud, miks Truuta Laaster tema peale on tühja juttu teinud.
Kõrtsmik toob tunnismehed ette, kes tunnistasid, et tema õde ja hobune a wana ema kõik sel õhtu kella seitse aegus kodu olid, seda tunistab Juhan Humberg, Karel Kask ja Jurri Tunneberg, ja ka metsawaht leidis, et hobune kodu oli.
Kohtu otsus: Truuta Laaster saab sele trahwiks 24 tuni peale wangi trahwi alla moistetud, miks ta seesugusid wale juttussid teiste inimeste peale teeb ja sel kombel mõisa ja ka walla politseid tülitab.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
On trahwi kätte saanud.
