2) a tulli ete Tallitaja Mart Meinwalt ja kaewas et üks siit Nurme walla mees Willem Lindman on Wardi waldas Rahhola küllas Waino Jani juurdes tenimas 1868 aasta kewade jubba sinna läinud ja ei tahha selle eest mitte pea rahha wälja maksta 1868 aasta eest üks pool			1 Rub 60 kop		ja 1869 aasta eest kirjotajate palga ja kortel kambri rahhaga kokko			3 " 57 "		ja 1869 aasta Nekruti rahha			 " " 25 "		pärri takse: summa			5 R 42		
agga et weel ühhe pole 160 koppikaga saab kannatud, agga 3 R 82 kopikke peab wiwi matta wälja maksma
b Selle pärrast sai kohtu ette kutsutud Jaan Kulör ütles essite et temma ei olle sedda kaupa kellegiga teinud et temma peab pea rahha maksma ja Wardi waldas ei olla keegi weel pearahha temma käest kussinud ja temma ei makste poisi pealt ka mitte
agga kui talle sai ärra selletud et Nurme wallal Wardi seadusega teggemist ei olla waid Nurme mehhe pealt peab Nurme Tallitaja kätte nenda maksma kudda keik teised walla Liikmed maksawad - ja ütles wimaks et ühhe korraga nii paljo ei woi mitte ühhe mehhe pealt wälja maksta - ja enne ei olla temma käest ei olle enne mitte küssitud 
d agga wanna Tallitaja Willem Petohwer tunnistas et temma on ka temma käest Nissi Laadal kässinud et teised on maksnud 1 R 97 kop ja sinna pead temma pealt ka sedda nüüd wälja maksma agga ei olle mitte maksnud
e) Jaan Kulör agga lubbas tullewa Pühhaks Tallitaja kätte anda 2 Rubla 50 koppike, et sest olla see kord küll
g kohhus agga ütles et temma peab tullewa Pühhal ärra maksma 3 Rubla 82 kopike ja nii pea kui jälle Tallitaja küssib et siis ka teise wälja peab maksma
Peawannem Juhhan Meinwalt
kõrwamees Willem Petohwer
kõrwamees Jakkub sesmiin [allkiri]
