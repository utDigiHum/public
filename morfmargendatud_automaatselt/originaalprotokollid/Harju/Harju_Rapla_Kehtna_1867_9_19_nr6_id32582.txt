Keawa koggukonna kohhus sel 19 mal Septembril 1867.
Koos ollid: Peakohtomees: Mats Roosberg
Kõrwamees: Ado Weinberg
" Tõnnis Spielberg
protokolli kirjutas: T. Klinge
Keawa mõisa wallitsus kaebab, eet teomeeste naised Kai Rebbane, Kai Pikkow ja Tio Maurer on erned wäljalt warrastanud, ja peale selle olla weel Kai Pikkow kaks kottitäid heino mõisa rohtajast warrastanud.
1. Kai Rebbane 28 aastad wanna astus kohto ette, ja ütles et temma on Ohekatkust erned tonud, ja üks nattukene mõisa ernestest wõtnud.
2. An Maurer 36 aastad wanna astus kohto ette ja tunnistas erned warrastanud ollewad.
3. Kai Pikkow 23 aastad wanna astus kohto ette ja üles et temma on kaks korda rohtajas heino tonud.
Kohhus mõistis: Kai Rebbane peab et temma on erned warrastanud, üks küllimit, omma palga ernestest ilma jäma ja Tio Maurer saab ka üks küllimit temma mona ernestest mahha wõetud ja Kai Pikkow et on heino warrastanud, peab üks rubla trahwi mõisawallitsusele maksma.
Peakohtomees: Mats Roosberg XXX
kõrwamees: Ado Weinberg XXX
" Tõnnis Spielberg XXX
