35
Lissa, Protokoll No13 punkt 30 juurde Kasanski Juhhan Umbergi Kaebtusse asjus Jakob Sucharowi wasto.
Tunnistusse mees Hinno perremees Tõnno Ruben astus kohto ette ja ütleb, et Kasanski Juhhan on mõisa Jakob Sucharowiga seltsis Meldema kõrtso tullema jäänud, ja minna läksin eele.
Kasanski Juhhan Umbergi poeg Juhhan Tunnistab, minna ollin Meldema kõrtsus ja Hinno Tõnno tulli enne Meldemale ja ütles et minno issa Jakob Sucharowiga seltsis tullema jänud, ja Jakob Sucharow tulli kõrtso ja ütles et minno issa krawi jänud, ja minna ütlesin ehk taal on rahha juurdes ja Jakob ütles põlle tal juurdes keddakid.
Jakob Sucharow ütleb selle wasto minna läksin eele Meldema kõrtso Talmeister Tonnuga seltsis, ja Hinno Tõnno jäggi Kasanski Juhhaniga järrel tullema, ja Kasanski Juhhani rahha minna ei olle warrastanud.
Kohhus mõistis: et selle rahha kaddumisse ülle selged tunnistust ei olle, ei wõi sedda wargust Jakob Sucharow peale mõista, ja et mõisa wallitseja tütruk Anna Lindwers sedda nugga ommaks tunnistab, saab temma omma kaddunud nua kätte.
Kaewaja Juhhan Umberg ei olle selle mõistmissega rahhul, waid tahhab suremad kohhut nõuda.
Kohtowannem: Jaan Sarik XXX" kõrwamehhedJürri Simson [allkiri]Jaan Lehbert XXXKirjutaja: KWiek [allkiri]
Protokoll Surema kohto ette wäljaantud 8mal Decemb 1886a.
