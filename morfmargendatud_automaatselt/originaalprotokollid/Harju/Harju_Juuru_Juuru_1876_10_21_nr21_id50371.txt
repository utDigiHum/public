Kohtusse kaebas Herkülla mõisa wallitsus: Tütruk Kai Ausmehe peale et olli mõisa aiast mittu korda käinud kapsaid ja kalisid warrastamas, mis kärnel Karro olli ärra näinud ja passima hakkanud, agga siiski olli see üks nim ütruk Kai Ausmees (öösi) õhto pimmedas temma eest ennast puude warjo ärra petnud ja kärnel ei olle tedda mitte kätte sanud, et agga kärnel tema peale arwas ja tedda öösi ka tundma olli õppinud, läks otse kohhe Kai Ausmehe emma sauna ja leidis sealt 7 mõisa suurt kaali murrakat ja kapstaid, need 7 kaali olli Kärnel kohhe mõisa toonud märgiks, et warras on leitud, aga kapstad olli sinna mahha jätnud, et ei olle suutnud nenda paljo kanda. Nenda olli mõisa wallitsusse kaebdus ja kärneli tunnistus kohtu ees.
Nüüd kutsus kohhus Kai Ausmeest ette ja kuulas kuidas ja mil wiisil sa olled sedda tohtinud tehha. Kai Ausmees püidis esiteks ennast wabbandada, agga see ei aidanud ühtigi, waid piddi süi ommaks wõtma.
Kohtu otsus
Kai Ausmees maksab nenda kuidas mõisa wallitsus nõuab kahjo tassumiseks 2 Rbl hõbbe ja weel selle häbbemata töö ja warguse eest 40 Cop hõbbe wala laekase.
Pirgu Herküla kogo wannem J. Korw XXX
" " Kõrwamees M. Ausmees XXX
" " " " J. Barnabas XXX
