Astus kohtu ette kaebaja Mikku Gustaw Kruusmanni naese ema Anna Kurell ja kaebas, tema laenas 12 a. eest kui tema oma tütre juure elama läks oma wäimehe Gustaw Kruusmanile 23 Rbl. raha, nüüd pärast tütre surma ei luba wäimees seda raha enam tagasi maksta.
2) Kewade kui tüttar ära suri, siis kauples wäimees teda oma wäätima lapse juure hoidjaks, ja ka ühtlasi tema maja perenaseks ja lubas selle ette palka maksta, 2 waka rukkid 2 odre ja üks wak tangu ja 2 tündri kartulid aasta palgaks, Heina aeg hakkanud wäimees Gustaw Kruusmann teda ilma mingi pohjusette roppusti sõimama ja aeanud tema oma juurest ära ja öölnud, et temal olla aidast lamba willu ära kadunud ja ka kaks tühja wõipütti, ja seda olla tema Anna Kurell warastanud, tema hing olla koigest sellest süüst puhas, niisamuti pidada wäimees ka tema kangas jalad kinni ja ei taha kätte anda.
Kustaw Kruusmann sai ette kutsutud, tema tunnistas tema olla ämmale 23 Rbl. raha wõlgu ja maksis seda ilma wastu waitlemata kohtu ees ära. Et tema ämm Anna Kurell temale palju kahju on teinud ja temalt woipütti ja lamba willad ära warastanud, siis ei woinud tema teda enam omas majas pidada ja ei luba, ka temale enne seda poole aasta palka kätte anda, kunni tema woipüttid ja willad kääs on, temal olla aida uks ikka lukkus olnud, ja teised ei ole senna sisse peasenud, kui Anna Kurell üksi siis peab tema ka teadma, kus need willad on saanud, ja need kanga jalad olla Anna Kurell temale kinkinud tütre pulma aeg.
Anna Kurel waitles selle wastu, tema hing on nende willa wargusest ilma süüta ja on ka täitsa teadmata, kas ülepea Gustaw Kruusmannil willu ongi warastud, tema pole mitte oma willu mõetnud, ja nüüd arwab aga umbes et olla 5 naela willu kadunud.
Temal käis aidas lapsed ja tütruk ka, kes teab kes need siis wõis wõtta.
Kanga tellid pole tema, Anna Kurell mitte oma wäimehele kinkinud, tema andis naad tütrele kangast kududa ja tüttar oleks need ka pärast tema surma omale saanud, aga et nüüd tüttar enne suri, siis nouab tema omad tellid wäimehe kääst tagasi.
Kohtu otsus:
Gustaw Kruusmann peab oma Ämmale (Anna Kurell) peale selle raha wõla, poole aasta teenistuse ette se lubatud palk andma 1 tünder kartulid 2 wakka wilja ja 1/2 wak tangu ja ka need kanga tellid, ja kannab isi oma wõipütti ja willa warguse kahju, sest temal ei ole mingi tunnistust ette tua, et Anna Kurell need oleks warastanud.
Kohtumees Juhan Humberg XXX
Korwasmehed Gustaw Reindes XXX Reinhold Korsen XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Otsus sai moistetud.
Leppitud ja makstud.
