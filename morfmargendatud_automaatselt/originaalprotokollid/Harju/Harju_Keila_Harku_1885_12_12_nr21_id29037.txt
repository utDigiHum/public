Ette tuli kaebaja Hans Marssi naene Anna ja kaebas et Juhan Oka naene Leeno on teda hirmsal wiisil sõimanud lapse tapjaks ja kolme mehe ooraks ja mitmel muul wiisil hirmsa sõnatega.
Ette sai kutsutud Leena Okas ja tunnistas et Anna Mars on teda ka lehmaks ja märaks sõimanud ja et A Marssi laps hilja aea eest ära surnud ning teised rääkisid et A Mars olla lapse ära maganud, siis ta öölnud seda temale.
Kohus kuulas asja mitmet pidi läbi ja leidis et Anna Mars on ilma süita sanud sõimata ning sepärast peab L Okas 2 rubla trahwi maksma ehk 12 tundi wangis istuma.
Kohtu wanem J Kallas XXX
Kõrwasmehed P Reining XXX ja W. Karik XXX
Kirjutaja M Käärt &lt;allkiri&gt;
Mõistuse täitmine jäi ueks aastaks.
