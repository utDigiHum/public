12.
Hans Perdi kaewab: Mina tõin 5 aasta eest ühe Pääpadja Madis Herrmanni käest ja andsin seda Mart Ahndo surnud naese Leeno kätte. See aasta suri Mart Ahndo naene Leeno ära ja Mart Ahndo ei anna seda patja mulle kätte, mina perin seda padja tagasi.
Mart Ahndo ütleb: Mina ei tea kas minu naene on seda pääpadja Hans Perdi käest saanud, ehk ei ole, minu käes seda patja ei ole.
See protukoll on nendele ette loetud ja nad ütlewad et see tõsi on mis nad on rääkinud ja et see õieti on üles kirjutud.
Mõistetud. Puuduliku tunnistuste pärast ei woi mitte sundida /Mõista/ et Mart Ahndo seda patja Hans Perdile maksab.
