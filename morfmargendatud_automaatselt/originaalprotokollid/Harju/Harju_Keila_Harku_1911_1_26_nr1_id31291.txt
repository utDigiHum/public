1911 aastal Januari kuu 26 paewal ilmusiwad kohtule isiklikult tuntud Harku walla Sorwe küla peremehed Otto Mänd, Karel Annus Otto Uula, Otto Mikler Mihkel Meriküll, Karel Meriküll, Jaan Uuder, Kustas Ehrman, Willem Arras Jaan Kritt, Juhan Uula Juhan Nigol, kõik Harku walla liikmed ja kelledel seaduslik woli on eneste wahel wabatahtlikusi lepinguid teha, ning palusiwad kohtu akti raamatusse järgmist lepingud sissekirjutada.
Harku wallas Sorwe külas ülemal pool nimetud peremehed tegiwad eneste wahel lepingu piima ühisust asutada ja piimad Hollandri ehk piima rentniku kätte wälja rentida, mille tarwis jää ja piima kelder tuleb ehitada Piima ja jää kelder tuleb ehitada waljas poolt mõetu 8 sülda pikk 3 sülda ja 2 jalga lai
Keldri ehitamise materjali weawad ühisuse liikmed koik ise kokku ja murrawad kokku nii palju kiwa kui ehitamise juures tarwis läheb. Iga üks ühisuse liige maksab esialgset liikme maksu materjali ostmise ja muude kulude katmisejs nii palju kui ühisud oma wahel haalte enamusega otsustab, aga Kadaka ja Timmiste kohtade peremehed kahekordselt ja seda raha ei saa keegi enam ühisuselt tagasi nõuda. Keldri ehituse raha ja mis weel peale keldri ehituse ühisusel ehitamise juures tarwis läheb, walib ühisus oma liikmede hulgast, kolm liiget, kes ehituse raha laenawad ja ühisuse liikmed maksawad selle raha mitte hiljem kui wiie /5/ aasta jooksul ühes protsentidega tagasi; Protsent maksab ühisus niipalju, kui laenatud raha pealt maksta tuleb. Laenatud raha maksawad ühisuse liikmed tagasi iga liikme piima toopide järele, mis hind iga piima toobi peale ühisuse poolt määratud saab, aga iga piima toobi pealt ei wõi mitte rohkem, kui kaks kop. olla. Ühisus walib oma liikmede hulgast ühe asjaajaja ehk esimehe ja kassahoidja, mis mitte lühema, kui ühe aasta peale on ja ka mitte ühisuselt palka ei saa. Esimees wõib ühisuse ja piima rentniku wahel asja ajada, mis mitte üle nelja rubla ei wõi olla. Kassa hoidja käes ei seisa peale ehituste muud raha, kui ainult see raha, mis wola tasumiseks piima toopide pealt kaswab; kaswawat summa peab hoiukassa raamatu peale pandud saama; lahtine raha ei woi mitte kassa hoidja käes kauem, kui kolm kuud seista ja kassa hoidja peab, kui ühisus nõuab iga kuus ühisusele wõla tasumise summast aru andma. 
Kui keegi ühisuse liige peaks ühisusest wälja astuma olgu missugusel juhtumisel, siis ei ole temal mitte oma ühisuse õigust ilma ühisuse nõus olemiseta teisele anda ja tema liikme õigus jääb ühisuse kasuks ja ühisus maksab selle raha kõik tagasi, mis lahkujal liikmel kanda oli, aga töö, mis liikmel ehituse juures teha oli, jääb tasumata, aga lahkuja liikme oigus wõib ka ühisuse nous olemisega tema järel tulija kätte jääda. Ühisuse õigus jääb aga pärijate kohtu täieste maksma ja nende liikmede pärijatele jääb kahe liikme õigus, kes ehituse kulusid kahekordselt on kannud. Kui ühisus arwab tarwis olema piima rentnikule ülespidamist anda, siis otsustab tema seda oma keskis häälte enamusega, kas liikme peale ehk piima toopide peale ära jagades. Selle juurde tuleb ka piima rentniku korteli üür arwata, nii kaua kui seda maksta tuleb. Ühisuse liikmedel ei ole õigust, kellegi teise käest piima wõtta ja piima rentniku kätte wiia; õigus jääb igaühel niipalju piima anda, kui tema oma lehmadest saab. Kui keegi wäljast piima salaja oma nimel piima rentniku katte toob, see maksab ühisuse kasuks kakskummend wiis /25/ rubla trahwi ja peale selle kaotab oma liikme oiguse ühisuses ära.
Kui keegi ühisuse liige solgitud piima s.o. kas wett sisse pannes ehk muud, mis meelega tehtud on, see maksab wiiskümmend /50/ rubla trahwi piima rentniku kasuks ja kaotab kohe oma ühisuse liikme õiguse ära.
Aedewahe O. Mänd &lt;allkiri&gt;
Araka K. Annus &lt;allkiri&gt;
Arakakopli Ottu Uula &lt;allkiri&gt;
Wanaõue Mikler &lt;allkiri&gt;
Wartu M MIkler &lt;allkiri&gt;
Wartu kaarli K. Merikul &lt;allkiri&gt;
Kadaka J Uder &lt;allkiri&gt;
Tamme K Ehram &lt;allkiri
Lepiku W. Arras &lt;allkiri&gt;
Loo J Kritt &lt;allkiri&gt;
Timmiste J Ula &lt;allkiri&gt;
Kusma XXX
Seesinane leping on peale sissekirjutamist koigeil lepingu osalistele ette loetud ja nende poolt allakirjutud.
Kohtu esimees O. Reinde &lt;allkiri&gt;
Liikmed H Meri &lt;allkiri&gt;
               R Korsen &lt;allkiri&gt;
               A Tobon &lt;allkiri&gt;
Kirjutaja Kasberg &lt;allkiri&gt;
