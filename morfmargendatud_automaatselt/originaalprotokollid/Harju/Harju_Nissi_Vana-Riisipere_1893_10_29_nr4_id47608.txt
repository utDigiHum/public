Ilmusiwad kohtusse Runepere moisa metsaherra Nikolai von Freij ja Kõrlinu Kustas Simbach selle palwega et nende wahel saaks järgmine kaup kinnitud
Kõrliku Kustas Simbach wõttab oma peale kewadel 1894 a. enesele üks uus eluhoone rehaalusega oma kulluga üles ehitada.
Palgid on tema juba moisa poolt kätte saanud.
Kustas Simbach saab selle ehitamise, palgi raijumise, koorimise, weddamise, aknade, uste, ja kattuse tegemise kui kaa hõlede eest ulepea 46 rubla 50 kop.
Kustas Simbach tunnistab et sellega rahaga on kõik makstud ja kohha äraandmise ajal ei ole temal ühtegi enam tasumist selle eest mõisa herra käest pärida.
N Gefria [allkiri]
XXX tähendab Kustas Simbach
Eesistuia H Gefria [allkiri]
liikmed J.Linroos [allkiri]
J.simson [allkiri]
T Wiik [allkiri]
Kirjutaja Wehiger [allkiri]
