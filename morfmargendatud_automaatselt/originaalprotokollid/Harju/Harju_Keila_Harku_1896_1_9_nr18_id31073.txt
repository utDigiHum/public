Üks tuhat kaheksa sada üheksakümmend kuuendamal aastal Januari kuu 9 päewal ilmusid Harku Walla kohtusse kohtule isiklikult tuntud Karel Gaumann Karle poeg ja tema isa Karel Graumann ja palusid järgmist kauba kontrakti akti raamatusse üles wõtta.
Karel Karlepoeg Graumann annab oma isa Karel Graumanile oma ostetud Peetri kohast, Harku mõisa järel, wiis dessatiini maad pruukida nii kaua kui isa elab, ehk tema kontraht edasi kestab.
Isa Karel Graumann tasub selle tükki maksud ja kohused ja maksab selle maatükki ette pojale 425 rubla ja wõlga jäänud summa pealt 5% Karel Graumann on maksnud 300 (kolmsada) rubla sisse. Põldu saab Karel Graumann 1 1/2 dessatiini, heinamaad 1 1/2 des. ja karjamaad 2 Dess.
Kui Karel Graumann tahab oma kohta müia, siis peab tema ise Karel Graumannile selle maa hinna ja need hooned mis seal peal on wälja maksma.
Pärast isa Karel Graumanni surma jääb see maa tükk 5 Dessatiini teise poja Hindrek Karle poeg Graumanni päris omanduseks ja kui Karel Graumann tahab seda maatükki omale saada pärast surma, siis peab tema Hindrek Graumannile selle hinna ja hooned wälja maksma.
Karel Grauman &lt;allkiri&gt;
Karel Graumann XXX
See lepping on pärast akti raamatusse kirjutamist pooltele ette loetud ja nende poolt allakirjutud.
Eesistuja K Tahwel &lt;allkiri&gt;
Kirjutaja as J Awing &lt;allkiri&gt;
