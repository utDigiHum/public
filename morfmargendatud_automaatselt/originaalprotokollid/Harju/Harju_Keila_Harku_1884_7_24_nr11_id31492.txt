Tisler Aleksander Faltin kaebas nenda; Pühapäew 15. Heina k.p. hommiku wara läksin maalt linna. Mustjõe randas tulid mulle kolm tundmata paras joobnud meest wasta ja kohe esimene walge peaga tuli minu wasta ja küsis: "Mis mees sa oled?" Ma wastasin: Teekäia! ja nii pea kui seda sain üttelnud, lõi ta mulle wastu rindu ja teine tuli ka kallale ja hakkasid mind kiwiga taguma ja tallasid jalgega. Neljapääwa olin ma üsna asemel haige. Kui paremaks sain, siis kuulsin, et need Habersti Jerwe küla poisid Juhan Minter, Gott. Kramann ja Kristjaan Jerwan oliwad.
2, Juhan Minter wastus selle kaebtuse peale: Kui ma Aleksander Faltinilt küsisin kuhu ta läheb, oli see teda kohe löönud, siis tema löönud pärast
3, Gottlieb Kramann wasatas, ma läksin Juhan Minteri aitama
4, Kristjaan Jerwan tunnistas, et tema põle midagi teinud, muud kui toonud Aleksanderi Faltini mütsi kodu. Arwas ennast ilma asjata kutsutud olewad.
Kohtu poolt sai loppetust pakkutud; aga see ei sündinud.
Kohus mõistis: a Juhan Minter maksab oma ilma asjata riio tegemise ja armuta peksmise ette Aleksander Faltinile 3 rubla ja peale selle 2 rubla walla laeka trahwi, b, Gottlieb Kramann ja Kristjaan Jerwan maksawad mõlemad 2 rubla laeka trahwi, oma oosesse ümber hulkumise ja liig joomise pärast, mis läbi tihti pahandused sünniwad.
Maksiwad.
Kohtuwanem XXX G. Suurmann
Kõrwamees   Gottleb Torbek &lt;allkiri&gt;
teine              XXX Andres Jaakmann
Kirjutaja Joh Tasson &lt;allkiri&gt;
Lõpetud.
