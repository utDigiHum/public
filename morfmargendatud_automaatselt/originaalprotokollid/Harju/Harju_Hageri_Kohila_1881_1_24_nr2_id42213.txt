Astus ette Angerja kaupmees Hans Einman et temma podi olla wõlga teinud juba kahhe aasta eest Hans Lewkoi 3 Rupla, 2) Jaan Walbet 3 Rub 60 kop, 3) Jürri Angerjas 1 Rubl 70 kop, 4) Prits Mikku 8 Rubl 60 kop.
Ette astus Hans Lewkoi Jaan Wolbet Jürri Angerjas ja nemad ei wõtnud omma wõlga mitte sallata waid tunistasid et nemad on sedda rehnungi podi teinud.
Kohus mõistis et need üllemal nimetud mehhed peawad omma wõlla ärra maksma kaupmehhel Hans Einmanil. Jüri Angerjas maksis oma wõlg 1 Rubl 70 kopik kohhe wälja. Ni sama maksis Jaan Walbet 3 Rub 60 kop, Hans Lewkoi lubbas wõlg 3 Rubl 7mal Kündlaku päwaks ärra maksta.
Prits Mikku ei tulnud mitte sel päwal kohtu ette.
