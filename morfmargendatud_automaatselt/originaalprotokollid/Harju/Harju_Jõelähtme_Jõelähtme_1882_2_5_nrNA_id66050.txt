Kaebaja Tõnno Kaljo ütleb, et Gustav Kiwimäe on teda Jegelehtme lada aegses kauplend ühe wooriga linna minna ja pidand siis seest 2 rubla maksma ja tema poeg on selle koorma linna wiinud aga se woori raha on alles maksmata.
Kohus arwas kaebtuse asust teiseks kohtu päewaks kutsuda.
alla kirjutanud
Kohtuwanem Jaan Kütt, kõrwamehed Hans Mitt, Jürri Maisa, kirjutaja Ovir.
Wata 2.sel Aprill protokoll No 10.
