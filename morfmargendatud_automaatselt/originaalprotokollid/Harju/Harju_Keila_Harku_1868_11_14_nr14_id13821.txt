Rannamõisas sel 14. novembril 1868
Koos olid Peakohtumees K. Trauberg kõrwamehhed K. Luker ja J. Lipp
Ette sai kutsutud üllewel nimmetud Nr 12. Trino Puh tunnistussemees Liso Pärn
Liso Pärn ütleb, et Trino Puh on wallet tunnistanud, Kadri Rammuse tüttar on Lõune aeal wäiksewankrega koeu tulnud ja 2 wäikest last olnud wankres ja teisel olnud 10-12 kartuhlest sülles, mis temma olla tepealt wõtnud, kust mõisa kartuhwlid weatud.
Weel sai ette kutsutud, Trino Puh teine Tunnistusse mees Maddis Linnep, (üllewel nimmetud Nr 13), ja Maddis Linnep ütleb ka, et Trino Puh on wallet tunnistanud, temma on Juhann Hulbergiga seltsis tulnud, ja J. Hulbergil põlle muud korwi sees olnud, kui leiwa kot ja lähker.
Kohhus kulas asja järrele ja leiti tõssi ollewad, mis tunnistusse mehhed ollid tunnistanud ja r ütles, et temma seda olla kiuste pärast teinud.
Sai mõistetud, et Trino Puh peab 1 rubla trahwi maksma, sepärrast et ilma asjata olli kaebanud ja kiuste pärast kohtusse tulnud.
