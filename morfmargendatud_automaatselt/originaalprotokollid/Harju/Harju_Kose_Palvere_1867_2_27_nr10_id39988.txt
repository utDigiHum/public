Rawwila mõis kaebab:Tarandi metsawaht Juhhan on leidnud,et Leppiko wabbadik Tomas Wiso on enne Joulud ühhe männi mõisa metsast raiunud ja ärra winud 21 jalga pitk,7 tolli ladwast jämme.
Mõis pärrib selle pu hinna 75 Kop.
Tomas Wiso tunnistab:et ta selle männi on tonud;agga se pu põlle mitte ni suur olnud.
Agga et selle pu surusse tunnistus seggane on:Mõistis kohhus et Wose külla kohtomees peab sedda watama minnema,ja rwama kui suur se pu on olnud.Kui se pu nenda pitk ja jämme on,kui üles antud on,siis Tomas Wiso peab selle pu hinna 75 Kop.maksma mõisale ja trahwi 45 Kop.wallalaeka.Agga kui ta wähhem on,siis peab pu hind ja trahw wähhendud sama.Kohtumehhe tunnistusse järel on se pu ni suur olnud,kui üllesantud.Trahw 45 Kop.wälja makstud.
