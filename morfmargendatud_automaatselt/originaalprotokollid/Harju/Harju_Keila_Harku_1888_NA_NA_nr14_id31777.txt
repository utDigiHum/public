Mari Silberg kaebas, et Harko kubja Otto naene Miina sõimas mino meest Juhanit mäniku taga selle pärast, et minu mees olla tema poega wiina pudeli wargaks tunnistanud. Mino mees wastanud, mina ei tunne sind.
Naene wastanud: Eksa tea Kubja Edi, Mees wastanud. Kust kubja Edi tema wois warrastada, ta ei olnud jo pulmas. Siis utlend ega sa pole kellegi naest wõtnud, sa oled Kadaka küla hoora wõtnud.
Nõmmel jälle üttelnud Edi ise, kül ma sinu worstid wälja wõttan ja selle peale wiskas mino mees klaasiga, aga mitte poisi peale; ka kõrtsmik keelas poissi.
Soo Lassu pilgutas silmi Andrese Kaarli poole kõrtsus. Mino mees ütles mis sa wiina waras silmi pilgutad. Selle peale Kubja Edi wastand, kül ma so worstid wälja wõttan.
