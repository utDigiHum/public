Sell korral sai uuesti se asi ette wõetud, mis 14mal Decembril pooleli jäi.
Hans Treimunth oli teise kohtu käsu wastu pannud ja ei olnud mitte kohtusse tulnud. Muud kohtulisi ei olnud sell korral ühtegi kohtusse kutsutud.
Järgmine kohtu pääw sai 18maks januariks 1886 määratud, seks korraks saab siis weel üks kord Hans Treimundile seda käsku antud, et peab kohtusse tulema.
