Ette sai kutsutud Hans Kammal ja Anna Kaskirilow. Hans Kammal (naese mees) kes liggi kaks aastat A. Kaskirilowiga hirmast hora ello on ellanud ja sees aasta Märtsi kuus on A. Kaskirilowil laps olnud ning nõuab nüüd, et H. Kammal peab aitama last toita. Agga H. Kammal kostis, et ta ennast kül on jubba kullutanud selle kahhe aasta sees kullutanud;			
Leiba, mis  A Kaskirilow  oma kahhe teiste lastega ärrasünud			
(Anna Kaskirilowil  olli 2 kassulast enne ja nüüd kolmas) peale						4 rubla eest		lihha mis on ta sanud			5 rub 60 kop		puhhast rahha annud			2 rub 10 kop		saia keigewähhemast			3 rub 10 kop		üks kamm ja peegel			30 kop		Summa			15 rubl 10 kop		
Ja et temma pölle mitte üksi A Kaskirilowiga ellanud, waid Maddis Andresson on ka temma jures olnud.
Ette kutsuti M. Andresson, ta tunnis ilma salgamata et ta 5 Augustil 1872 Anna Kaskirilowi jures on olnud ja tale selle eest 40 koppik maksnud.
Kohus mõistis, et M. Andressoni jäggo laps mitte ei wõi olla, sest et aeg sedda tunnistab sepärrast ei wõi temma ka mitte lapse toitmist makssa. Agga H. Kammal peab igga aasta 5 rubla kunni 12 aastat maksma.
Agga H. Kammal pallus et ta mitte ei tahha maksta, sest et ta jubba peale 15 rubla on maksnud ja ta pölle mitte A. Kaskirilowi jure läinud, waid A. K. tulnud temma jure; A. Kaskirilow pöidis sedda kül walleks aeada, agga ei wõinud töelikkult onneti mitte; sepärrast pallusid suremad kohhut nõuda, mis ka ei kelatud.
Kohtowanem K Gutman XXX
Kõrwasmehed O. Mänd XXX ja W. Mänd XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
Kohto allused nõudsid suremad kohhut
