Sisse kaebas Katri Tahwel, et Karja isand Egert ola teda wägise tahtnud ära naerda ja wiimaks tema üle tigedaks saanud miks ta tema soowi ei ole täitnud, ja kui nemad teise tütruku Mari Watsuwiga ola riidu läinud, siis on isand Egert teda toast wälja wisanud ja teda teenistusest ilma süüta lahti teinud. Tema läinud sele peale Harku mõisa walitsejale kaebama, miks ta ilma süüta kese aastal saab lahti lastud. Walitseja kutsunud isanda Egerti senna ja küsinud sele pohja järele, miks ta tütruku lahti tegi, siis ola tema tütruk seda häbemata kiusamist ülesrääkinud, sele peale tulnud isand Egert tema kallale ja peksnud teda walitseja ees, wastu pead mittu korda, seda tunistab walitseja Dittman tõeks.
Kohtu otsus: Karja isand Egert peab tütruku kunni Jüripäwani teenistuses pidama, ja maksab sele ette et ta mõisa politsei ees tütrukud ilma süüta wasta pead lõi 4 rubla trahwi walla laeka. lepitud.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Makstud
