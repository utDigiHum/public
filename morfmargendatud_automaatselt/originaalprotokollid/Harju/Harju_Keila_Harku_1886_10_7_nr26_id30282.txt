Astus kohtu ette Karel Annus ja kaebas Kadaka küla hooste karjane Juhan Eht söötis tema ädala heina koplist ära, ja poletas tema kopli aeast 20 sülda lati aeda ära.
Harku walla kohus kutsus Juhan Ehti kolm korda kohtu ette, et saaks tema süü Karel Annuse kaeptuse peale läbi kuulatud, Juhan Eht ei tulnud mitte walla kohtu ette ja läks Habersti kadakale elama.
Tunnismees Juhan Looding tunnistas toeks, et J. Eht omma Annuse aeda põletanud, ja tema on just näinud, kui ta roikaid wiis.
Kohtu otsus:
Et Juhan Eht kolme kohtu kutse peale walla kohtuse ei tulnud, moistis kohus kaebaja nõudmise peale, et Juhan Eht peab Karel Annusele 3 Rbl. kahju tasumist maksma kahe nädalaa sees.
Kohtumees: J. Humberg XXX
On seletud Karel Annuse ütluse peale.
