Nr 6
Rannamõisas sel 20. septembril 1867.
Koos olid Pkohtum. K. Trauberg abbim K. Luker, J. Lipp.
Mõisawallitsus kaebab, et Otto Kambel, Kristjan Kambel ja Otto Kaddarpik on mõisast taggasi läinud, et ei tahtnud külma wega heinamale minna, Siis on kohhus mõistnud, et naad peawad igga üks 30 kop. trahvi maksma.  
K. Trauberg XXX K. Luker XXX J. Lipp XXX
kirjotaja M. Kehrt
