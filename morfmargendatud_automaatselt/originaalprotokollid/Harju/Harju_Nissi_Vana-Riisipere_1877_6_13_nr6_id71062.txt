4) Sai ette kutsutud Riesepere moisa wallitsuse kaebdus (wata Protok. sest 25 April 1877 No4) mtesa warguse parrast.
Kohto ette kutsuti Wirrokülla peremees, Tuliko Jaan Reinberg, kes kaebduse järrel, moisa Selja metsast puud warrastand olli, se on:3 Mändi			3½-4 sülda pitkad			ja 9-12 tolli känd paks		
Kohhus küssis? kas Jaan need siin nimetud puud raijonud? Jaan ütleb: et tema neid keiki kütti puuks olla raijonud, aga need olnud kuiwad, seest mäddand ja üks tulest poleks murtud känd olnud ja et temal moisa polest olla lubbatud, aga seddelid polle temal herra poolt olnud.
Kohhus moistis: sedda asja läbbi arwades: sellepärrast et tema ilma moisale meldimata neid on raijonud, peab Jaan Reinberg kahjo tassumist moisale 2 Rubla ja walla laeka trahwiks 50 kop maksma. Jaan pallub,? et temal kogoni woimalik ei olle maksta ja pallub termini süggeseni pitkendada. Kohhus lubbas sedda, agga et Mihkle päwaks wist peab makstud ollema, kui agga mitte, langeb tema surema trahwi alla.
Kohto peawannem: Jakub winkel [allkiri]
Körwamehhed: Jurri Simson XXX
Hans Surow XXX
Kirjotaja: BGildeman [allkiri]
