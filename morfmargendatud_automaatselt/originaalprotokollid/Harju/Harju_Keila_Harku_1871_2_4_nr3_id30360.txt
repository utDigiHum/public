Ette tulli kaebaja Murrasti mõisa metsawaht H. Kärbis ja kaebas, et Kristjan Korel (Wänast) ona Murrasti mõisa metsast 3 kaske ja 1 aawapu mahha raiunud, metsawaht on arwatud 4 tolli tüikast jämmedad.
Kohhus kutsus K. Korelli ette ja temma tunnistas ilma salgamata, et ta on need puud mahha raiunud, ja tahtnud ommale pühhade pudeks (se sündis Jõulo ette 1870) raiuda.
Sai mõistetud, et K. Korel peab maksma Mirrasti mõisa wallitsussele 3me kasse eest 10 kop. tolli pealt se on: 1 rubla 20 kop. ja awapu eest üllepea 30 kop.  se teeb keik kokko			1 rubla 20 kop ja		senna jure			30 kop.		Summa			1 rubla 50 kop.		
Agga Kristjan Korel on issi mõisa wallitsussega nenda leppinud, et tahhab nimmetud rahha eest päiwi tehha. Sellega olli kohto otsus walmis.
