11.
III) Tuli ette: Kokka peremees Juhan Brenner ja kaewab et Mäekülast Mäejaani sead on tema kaera orase, mis tema kartuli maa peale teinud, ülestustinud ja mitmes kohas rehaga pärast ärasilinud et küll weel palju tustitud on. Se on Sutlepa kesa ja tema sead on üksi päinis seal käinud. Kohtumees ja Tallitaja Abi on käinud ja on leidnud et palju tustitud olnud, nõnda ka Pärdimiku peremehe oras.
Mäejaani peremees Madis Priimets sai kohtu ette kutsutud ja temale kaebtus ette loetud. Tema ütleb tema sead ei olle mitte tustinud, (et küll kohtu kõrwamehed, kes seal külas elawad, ütlewad tema sead on seal wilja wahel käinud) waid olla Sakka sead M. Priimets ütlemise järel seda paha teinud.
Mõistus: Et Sakka peremees haiguse läbi puudub jääb teiseks kohtu pääwaks seletada.
