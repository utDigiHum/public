Kohtu ette astus Juri Saalfeldt ja kaebas, et sauna mees Hindrik Remmert ola salaja juba kaua aega tema noort kasu metsa käinud warastamas, wiimaks olla ta teda metsast kätte saanud, kus ta mitto kubu juba hagu walmis olla rajunud ja wõtnud seal temalt westu kaast ära.
Hindrik Remmert ei salga seda mitte, tema ütleb kogemata teinud olewad ja ka ola tal hagu tarwis, ja saada pole kuskilt, Ta palub Jurri Saalfeldt kääst andeks, Wiimaks sai sell kombel leppitud, et kohus mõistis Hindrik Remerti 48 tunni peale wangi trahwi alla, miks ta salaja öösite teiste metsas wargal käeib. Leppitud.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Leppitud.
Kätte saanud.
