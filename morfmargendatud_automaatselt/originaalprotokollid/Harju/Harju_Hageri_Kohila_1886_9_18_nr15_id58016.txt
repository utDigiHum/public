I)
Kohtu ette astus Pahkla mõisa metsawaht Hans Linsi, ja kaebas, et mõisa herra ja walitseja olid leidnud Wärawa Juhan Karu sauna õuest männi puid, mida tema metsast salaja on toonud. Kohus küsis, kui palju neid puid seal oli mis ta oli salaja toonud? Metsawaht ütles: kolm sületäit umbes arwata,
Juhan Karu sai kohtu ette kutsutud, kohus küsis, kust wõtsid need puud mis sull õues olid? Tema ütles tõin mõisa metsast.
Kohus mõistis, et maksad trahwi walla laekasse 25 kopik selle pärast et oled ilma lubata mõisa metsast toonud.
