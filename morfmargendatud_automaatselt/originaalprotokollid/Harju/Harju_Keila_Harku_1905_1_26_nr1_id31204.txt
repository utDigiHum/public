Üks tuhat üheksa sada wiiendamal aastal Januari kuu 26 pääwal ilmus Harku walla kohtusse kohtule isiklikult tuntud Harku kogukonna talup. Ottu Josepi p. Elu 65 a wana, kellel seaduslik woli on wabatahtlisi aktisi teha ja palus järgmist suu sõnalist testamenti kohtu akti raamatusse üles kirjutada: Mina Harku kogukonna liige Ottu Josepi p. Elu terwe mõistuse juures olles ja pikema järel kaalumise järel teen oma juhtuvaks surma korraks järgmise wiimse seadmise oma waranduse kohta: 
Minu omadus on praegusel ajal Harku mõisa järel olew Putke sauna maa koht ühes tarvilise hoonetega, selle waranduse pärrivad pärast minu surma minu naene Anna Elu ja kõige noorem poeg Richard-Woldemar ühe tasa. Minu teised lapsed poeg Kustas, tütred Liisu Kowetant, Maria Elu, Anna Elu ja Emilie Wiirum kes juba täieealised, tarwilisikaswatuse saanud, minu juurest wäljas elawad ja minu wastu tänamata olnud ei wõi pärast minu surma minu warandusest midagi pärida.
Ottu Elu ei oska kirja tema palwe peale kirjutas alla Kirna walla liige Jüri Krist &lt;allkiri&gt;
See testament on pärast akti raamatusse kirjutamist testamendi tegija Ottu Elule ette loetud ja tema palwe peale, kirja mitte oskamise pärast Kerna walla liikme Jüri Krist poolt alla kirjutud.
Kohtu President M. Jerwan &lt;allkiri&gt;
Liikmed J. Wäljamäe  &lt;allkiri&gt;
               O. Reinde &lt;allkiri&gt;
               H. Meri &lt;allkir&gt;
Kirjutaja J. Rammus &lt;allkiri&gt;
