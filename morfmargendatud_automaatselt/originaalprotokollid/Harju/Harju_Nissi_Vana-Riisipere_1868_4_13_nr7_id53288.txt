Sel tännasel päwal tullid kokko Lehheto walla Perremeeste Sullased ja Liikmed kes omma käe peal ilma madeta ellawad, - ja wallitsessid, walla politsei üllewatamisse al, kaks Sadikud, Perremeeste Sullased wallitsesid perremehhe Sullase Juhan Lindmanni assemele, kes nüd Lehheto walla krundist wälias ellab, ja liikmed kes omma käe pael ilma madeta ellawad, wallitsessid Jakob Kuthmanni assemele, kes surnud, Sadiku.
Perremeeste Sullased ollid:
Jaan Kuthman
Jaan Wohlman
Jaan Petoffer
Andres Wohlman
Liikmed kes omma käe peal ilma madeta ellawad ollid:
Andres Ewart
Jürri Hoidorp
Mart Wohlman
Jakob Reinberg
Perremeeste Sullased wallitsessid Sadikuks:
Jaan Kuthmann
Liikmed ilma madeta wallitsessid Sadikuks
Andres Ewart
Wallapolitsei nimmel,
Tallitaia: Jakob Kuthmann XXX
