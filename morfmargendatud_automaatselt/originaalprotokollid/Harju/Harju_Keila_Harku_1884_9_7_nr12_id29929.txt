Ette tuli kaebaja Hans Eilwelt Alliku küla hobuse karjane, ja kaebas, et Kaharsöödi Juhan Annus olla teda ilma süüta peksnud, ja siis weel pööningult luugist alla wiskanud.
Juhan Annus sai ette kutsutud ja kaeptus ette loetud, tema tunnistas, et ta pole mitte peksnud, olla aga oma pööningult ära aeanud, et temal mullu aasta üks willa kamtsul ära kadus ja tänawu üks wikkati tahk, seda süüd arwas, ta Hobuse karjatse Hans Eilweldi süüks.
Et Hans Eilweldi märgid näha oli tunistuseks, et ta peksa on saanud, ja Otto Kurel seda näinud oli, siis mõistis kohus, et Juhan Annus maksab 3 Rbla trahwi sellest saab 1,25 kop Hans Eilweldile kahju ja kannatuse ette ja 1 Rbl 75 kop. walla laeka.
Peakohtumees Jürri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
ja    asemik       Hind Paramell XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
