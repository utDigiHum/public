15.
Rieseperre walla kohhus olli kohto korra pärrast walla kohtomaiase kokko tulnud, Kohtoette astus Riseperre wallast Selja metsawaht Hans Einbom ja kaewas, et temma on omma Lehma Selja Jürri Pukisarwe einama pealt maast leidnud purruks pekstud kontidega, ja arwab et sedda keegi mu ei olle peksnud kui Selja Jurri Pukisarw, sest Selja Jürri on isse üttelnud, et ta on temma lehma omma einama pealt ärra aianud ja lehm on temma ette mahha kukunud.
Lehm on nönda purruks pekstud et ta kümme pääva waewalt (pikkali maas) on ellanud, siis on temma isse tedda mahha lönud, sest et temma ommeti ei olleks ellama jänud.Selja Willem Tempel wöttis selle Lehma nahha ärra, ja wöib selle ülle selgemad tunnistust anda.
Tunnistussemees Selja Willem Tempel astub kohto ette ja tunnistab et ta on selle lehma nahka wöttes on leidnud, et lehmal ristlude pealt kont kahhest kohhast purruks lödud on olnud, ja kaks külle konti on ka purruks lödud olnud.
Kaebtusse kandja Selja Jürri Pukkisarw astub kohto ette, ja kohto küssimisse peale ütleb temma et ta on Selja metsawahhi lehma omma einamast wälja aianud, agga sedda lehma lönud minna mitte ei olle, minno sullane Kustav Webberg näggi pealt et ma lehma ei peksnud.
Selja Jürri sullane Kustas Welberg astub kohto ette, ja ütleb: Minna läksin omma perremehhega seldsis einamase, minna pannin härgi ikkese, ja perremees aias metsawahhi lehmad wälja, agga sedda ma ei näinud et perremees lehmi löi.
Selja metsawaht Hans Einbom teab rääkida et Hans Nokas on temmale üttelnud et ta on kulnud et Selja Jürri on selle lehma mahha lönud.
Kohhos möistis sedda asja teise kohto päwaks siis peab Hans Nokas kohto ette kutsutud saama.
16.
Kohto ette kaewas Selja Jürri Pukisarw et ta on purjus peaga omma hobbose Liva körtsmikkule neljakümne rubla eest münud, agga obbune on kuuskümmend rubla väärt.
Kaebtusse kandja Liva körtsmik Juhhan Raggastik astus kohto ette ja ütleb wälja: Selja Jürri Pukkisarw tulli 15mal Augustil meile, ja pakkus mulle süssi müia, minna ütlesin et mul süssi tarwis ei olle, siis pakkus ta mulle hobbust müia, ja minna ostsin se obbone temma kääst ärra ja andsin (40) nellikümmend rubla, minna maksin temmale rahhakätte, ja siis jöime ligud, 1 Selja Jürri wöttis 2 rbl 87 koppika eest se rahha on temmal wölgo, minna andsin ommalt poolt 2 rubla eest, pärrast tahtis Selja Jürri hobbost kätte sada, ja pakkus rahha taggasi, agga ma ütlesin maksa need kullud, mis ligo jomisega on kullutud, wälja, siis ma annan obbose taggasi, enne mitte.
Selja Jürri Pukisarw ja Liwa körtsmik Juhhan Raggastik leppivad kohto eest nönda: Selja Jürri maksab 40 rbla obbose rahha, ja 4rbl 87 kop. ligo rahha Liwa körtsmikule taggasi, ja siis annab Liwa körtsmik temma obbose kätte.
Kohhos olli sellega rahhul ja möistis ett Selja Jürri peab sedda rahha maksma, siis saab omma obbone kätte.
Jääb nönda.
Kohtowannem: Jaan Sarik XXXKörwamehhed: Jürri Simson XXXJaan Lehbert XXX
