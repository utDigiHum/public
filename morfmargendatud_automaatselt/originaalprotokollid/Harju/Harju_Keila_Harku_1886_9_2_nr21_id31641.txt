Kristjaani perenaene Anna Jerwan tuli kohtu ette ja kaebas ütteldes: Paari nädala eest on Kraamle Juhan Klenner, Fick'i herra kojamees mino poega, karjapoissi Ottot metsas peksnud teiwa jämeduse puuga nii, et poisi selgristluu peal üsna sinine oli ka pois kolm pääwa maas magas. See põhi mikspärast Juhan Klennet poissi oli peksnud, olla see, et pois olla tema lambaid peksnud ja loomad Juhani Kummbergi heinamaa peale ajanud. Wana kojamees, kes 20 aastad elas, ei keelanud ialgi loomasi käimast, mis tema nüüd keelab.
2. Juhan Klenner wastas Anna Jerwan'i kaebtuse peale nenda: Mööda läinud suil tegid Kristjaani pere hobused mulle palju kahju: nad sõid ja tallasid mo Kople heina ja tallasid mo Kartohwli ära, pois oli mo lambaid peksnud ja laudast wälja ajanud; seda ma kannatasin, et ma tüli naabride wahel ei tahtnud teha. Nüüd, kui pois ajas loomad Kummbergi heinamaa peale, keelsin neli korda, aga pois ei kuulanud, wiiendamal korral sain ma pahaseks ja wõtsin poisi oma käest karja witsa arwata weikse sõrme jämeduse ja lõin sellega kolm korda hirmutuseks selga riiete peale, aga mitte teiwa jämedusega, kuida Anna Jerwan räägib. Aga Kristjaani perenaene kiusab mind sellepärast, et ma mitte loomasi heinamaa peale ei woi lubada, nagu wan kojamees kes ei maksnud renti, ja kui loomad sõid, siis oli see sakste kahju, mina aga maksan kallist renti ja ei wõi selle pärast mitte seda lubada, mis wana kojamees lubas. Sellepärast nüüd teotab ja wintsub Kristjaani perenaene mind, aga mitte poisi löömise pärast, see on aga muido asjaks wõetud.
Kohtu poolt sai kohtuskäijatel leppitust pakkutud ja soowitud; aga Anna Jerwan ei wõtnud seda wastu.
Kohus mõistis:
1, et karjapois Otto Jerwan sõnakuulmata oli mitmekordse käskimise peale wõeralt krundilt oma loomasi ära ei ajanud, oli ta hirmutamise ära teeninud.
2. et Juhan Klenner seda poissi sõnakuulmata wiisi mitte enne poisi wanemtaele ei rääkinud, waid ise oma käe peal hirmutas, mis läbi poisi ema meel on äritud saanud, maksab 1 rubla walla kassasse trahwi.
3. Anna Jerwan peab oma lapsi parem walitsema, et nemad ka nii wäljas, kui kodu sõna kuulewad ja mitte oma sõnakuulmata wiisi läbi teiste inimestele kahju ja pahandust ei sünnita ja peab ise rahule kääma.
Anna Jerwan ei olnud selle otsusega rahul, palus protokolli wälja.
Üks Rbl on Joh Klenner Walla Laek maksud.
Kohtumees Gottleb Torbek &lt;allkiri&gt;
Kõrwamees Willem Andermann &lt;allkiri&gt;
teine             Karel Ambos &lt;allkiri&gt;
Kirjutaja Joh Tasson &lt;allkiri&gt;
Wälja antud 4 Oktobril
