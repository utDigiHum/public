Ette tuli kaebaja Murrasti mõisa pere emand A Tiedemann ja kaebas, et 3 Aprillil õhtu kelo 11 aeal on kolm Wäna noort meest nimetud moisa pere tupa tulnud ja pöle tüdrukuid mitte lastnud rahul magama minna siis on tema neid sealt walja aeanud, aga naad on teda wäga hirmaste sõimanud.
Nimetud mehed said ette kutsutud ja tunistasid, et naad olla linnast tulnud ning sinna sisse läinud piipusid suho panema ja siis olla neid sealt walja aetud aga naad pöle mitte sõimanud, kui aga asja mitmet pidi sai läbi küsitud, siis tuli wiimaks wälja et naad mitte linnast ei ole tulnud, waid kotto ulkumise peale wälja.
Kohus mõistis asja järele kuulates et nimetud mehed: Juhann Korn, Jüri Oraw ja Juhann Kadarpik peawad igaüks kas 3 rubla trahwi ehk 15 hopi witsu saama, nemad palusid aga raha trahwi enem, mis neese ka lubati et iga üks pidid 3 rubla maksma wala laeka.
Kohtuwanem O Rammus &lt;allkiri&gt;
Kõrwasmehed J. Kallas XXX ja M. Merikül XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
Mõistus täidetud nimetud pääwal.
