Kohtu ette tuli kaebama Tallinnast wiinapoe kaupmees Peter Kullerkann Habersti mõisa sulase Jaan Tammerika peale ühe heina ostmise pärast ja ütles nii: Minewa sügisi kauplesin Jaan Tammerik'i käest tema sulase heinad, arwata 100 puud 35 rubla ette omale ja maksin kauplemise juures kohe 25 rubla Jaan Tammerikile kätte. Pärast kuulsin, et Jaan Tammerik oli neid heina, pärast minu kaupa ka ühe teisele kauplenud, ja salgas minu raha ära. Suure meelitamise peale ütles wiimaks, et ta see 25 rubla minu käest oli saanud. Aga nüüd ta ei anna ei raha ega heino, ja ma kardan, et tema müüb salaja heinad ära ja ma jäen sell kombel omast rahast ilma. Palun kohut omale abiks olla, et ma oma kätte saan.
2, Jaan Tammerik tunnistas Peter Kullerkanni kaebtuse peale nenda: Ma olin purjus kui heino kauplesin ja 25 rubla raha wastu wõtsin. Heinad olid jo enne Jakob Suuraldile ära kaubeldud ja 8 rubla käeraha käes. Alamad 45 kop. puud, ma oma heino ei anna.
Kohus mõistis: 1, Jaan Tammerik peab praeguse heina hinna järel 35 kop. puud Peter Kullerkann'ile heino andma, et 25 rubla tasutud on, ehk jälle maksab kahe nädali aja sees puhta raha 25 Rubla tagasi.
2, Jättab Jaan Tammerik seda tegemata, siis saab wäewõimusega 25 rubla ette Jaan Tammerik'a heino müüdud ja Peter Kullerkannile wälja maksetud.
/Sai wäewõimusega 71 1/2 puuda heino wõetud
Kohtuwanem Gottleb Torbek &lt;allkiri&gt;
Kõrwamees Willem Andermann &lt;allkiri&gt;
teine             Karel Ambos &lt;allkiri&gt;
Kirjutaja Joh Tasson &lt;allkiri&gt;
Lõpetud.
