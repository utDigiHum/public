Aastal 1868 sel 21sel Märtsil üllese woetud Keiserliko ülle wataja kohto kirri mis wälja antud sel 16mal Märtsil 1858 No 146 al
Kuberneri Herra kirja järrel ma kele ümber kirjotud nenda kudda allamal seisab nüüd ma kele
I Eestima kubbrneri Herra-kui rõuge pannemisse kommissijoni peawannema kirri sest 1sest Märtsist sest aastast No 4 al kõige kihhelkonna kohtu Herradele.
Et mödalainud aastal mitmes paikus Eestima sees - rõugede tõbbi on likumas olnud siis on üks kubbermangu rõugepannemisse kommissijon sedda wägga tarwilikkuks arwanud kihhelkonna kohtu Herrade läbbi sedda käskida et nende noorde innimestele kes se aasta letri õppetusse al on peawad kohhe pärrast leri õppetust kihhelkonna rõugepannija läbbi ued rõuged pandud sama.
Selletarwis olleks waea et rõuge mäddast pudust ei olleks et essiteks rougepannija wõi kirriku mõisa lähhedalt waldadest mõnne lapsele rõuged saaks pandud kellepealt siis nende lerikäijade tarwis ommal aeal wõiks sama wõetud kedda selle ülle mis siin üllemal on raeatud saab kihhelkonna kohtu Herrade käest pärrida et ommal teatud aeal peawad kubbermangu rõuge kommisjonile arru andma.
II Eestima kubberneri Herra kui tallu asjadetoimetaja kommissijoni peawannema kirrisest 29mast Februarist selle aastast. No 59 al
Issiärranis tarwilikkuks arwab üks tallu asjade toimetaja kommisjon sedda kihhelkonna kohtudele ettepanna et selle seadusse läbbi neile peale pandud ülle watamisseammeti sees allamalnimmetud asjade ülle wägga terraselt peawad walwama.
1) Et need koggukonna kohtude polest mõistetud täie jõu sisse sanud otsused ilma wiwimata ja täielikkult sawad täidetud.
2) Et sedda mõda need koggukonna kohtude polest peale pandud rahhatrahwid holega sawad pärritud ja kuhhu nende kord sissemaksetud ja kirjapandud.
3) Et need kässud ja seadmised mis koggukonna kohtude seaduse lehhesees sest 11mast Juniku päwast 1866 aastast wälja antud ja kinnitud parrakraf 10 kunni 14 sees on üllese pandud koggukonna kasa wallitsemisse pärrast õiget wisi sawad üllese petud ja täidetud; ja
4) Et walla maggasini aitade wallitsesussed ühhel wisil ja nimmelt nende wastamiste al kui parrakrahf 1 kunni 9 sestsammast seadusest sedda ette raeab oiged wisi saiwad üllespandud.
Peawannem Otto Meltorf
kõrwa mees Jürri Kiel XXX
korwa mees Hans Einbach XXX
