Lehheto walla politsei polest saab seläbbi tunnistud et se Billeti peale wälja tulnud Matros Tallinna portowoi rodust Jürri Siker Lehheto walda taggasi tulnud sia hingekirja 1870 aasta I polest ülleskirjutud ja Puseppatöga ennast ellatab, temmal seddamöda selle, sel 25mal Junil 1867a. keigekörgemalt kinnitud Seaduse art. I ja II järrel se keigekörgemalt lubbatud abbirahha keiserlikkus Tallinna rentereis wasto wötta.
Lehheto walla politsei nimmel, Tunnistusseks; Tallitaja: Jakob Kuthmann XXX
Kirjotaja: G.Krüger [allkiri]
