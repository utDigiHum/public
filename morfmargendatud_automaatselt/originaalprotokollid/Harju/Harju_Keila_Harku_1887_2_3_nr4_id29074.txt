Ette tuli kaebaja Murrastist Mardi koha omanik Toomas Riko ja kaebas et Sörwest Warto koha omanik Mihkel Merikül on 16 Detsemb. öösi tema metsast 4 kuuske ära warastanud 1/2 süla kõrgused känud olnud 2 1/2 toli jämedad, tema ja talitaja abimees W. Sinep on jälgi mööda järele läinud, kunni Warto õue, seal olnud kuusa okaid maas, ja siis kadunud jäljed ära ja peremees pöle mitte kodu olnud, ning ema ütelnud, et olla Kartuhwlidega linna läinud, naad mõetnud ree ja hobose jäljed ära ja läinud kodo.
Teisel pääwal läinud naad ueste sinna ja mõetnud ree ja hobose jalad ära, mis täieste ühte läinud aga peremees öölnud et ta pöle muud linna wiinud, kui kartuhwlid ja mitte Jõulu kuuskesid ning sest wargusest ei teata tema midagi ta pöle ilmaski kuuskesid linna wiinud.
Seda samma kaebas ka Eriko koha omanik Juhan Pranso, et selsammal öösel ka tema metsast 6 kuuske on warastud ja sellesamma koorma peale wiidud kelle jäljed Wartu õue läinud Kõik need Kuused olnud 4,5 ja 6e tolised.
Ette sai kutsutud Wartu koha omanik Mihkel Merikül, ja tunistas et ta pöle mitte wargil käinud, ja ei tea ka mitte kes need kuused on warastanud ja tema õuest läbi wiinud tema olewa kartuhwlidega linnas käinud ja 4 kuuske olnud ka peal.
Selle peale kostis T Riko aga sa ütlesid meile, et pöle mitte kuuski linna wiinud?
M Merikül wastas: ma ei tahtnud seda siis kohe öölda.
Nenda olid tema jutud mitmel wiisi Kahtlased.
Kohus mõistis asjaliko üle kuula peale et keski muu ei wõinud neid kuuski tema õuest läbi wia (sealt ei käi teed läbi) kui tema ise ja ka et ree ja hobose jala mõedud täieste ühte läinud ja M Meriküla tunistused segased olid siis peab ta need kuused maksma.
Nimelt T. Rikule 4ja kuuse eest			1,40 Kop		peale selle trahwi			1 rub		J Pransule 6e kuuse eest			2 rub		peale selle trahwi			1 1/2 rubla		Summa			5 rub 90 Kop		
M Merikül ei olnud selle otsusega rahul ja palus protokoli et wõiks suuremat kohut nõuda mis ka taale lubati.
Kohtu wanem J Kallas XXX
Kõrwasmehed P Reining XXX ja W. Karik XXX
Kirjutaja M Käärt &lt;allkiri&gt;
Et seatud aeal protokolli pöle nõutud jääb mõistus muutmata ja peab täidet saama No 5
