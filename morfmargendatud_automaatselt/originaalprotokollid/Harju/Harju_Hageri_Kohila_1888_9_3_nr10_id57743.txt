Kihelkonna kohtu herra kirja mööda sest 30mast Jüri k.p.s.a. N 264 sai need peremehed kohtu ette kutsutud, kellel O. Wenderile kooli palk maksmata oli. Nimelt: Jüri Roodi, Hans Tamberg, Kaarel Bach, Martin Kullama, Anton Einlok ja Ann Kiideman ja Konstantin Kok.
Kohus mõistis, et Luteruse usku peremehed, se on Jüri Roodi, Hans Tamberg, Kaarel Bach ja Ann Kiidemann peawad wõlgu jäänud kooli palga wälja maksma. Õigeusku peremeeste kääst küsis kohus, kas lubate seda kooli palka wäljamaksta, mis teil P. Wenderile maksmata jäi? Martin Kullama ja Anton Einlok ei lubanud seda raha mitte maksta aga Konstantin Kok maksis oma jägu 20 kop. wälja.
Kohus ei wõinud õiget usku peremehi mitte sundida seda raha wälja maksta, sest et nendel ei olnud lapsi luteruse usku koolis.
(Mõistus sai täidetud)
