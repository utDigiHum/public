Üks tuhat kaheksa sada üheksakümmend seitsmendamal aastal Februari kuu kolmeteistkümmnemal päeval ilmusivad Harku Walla kohtusse Johan Willemi poeg Wekmann ja Hans Johani poeg Polna ning palusiwad järgmist oma wahel tehtud kaupa akti raamatusse üles wõtta:
Johan Willemi poeg Wekman on müinud Hans Johani poeg Polnale oma ostetud Tõnise kohast, Harku mõisa all, ühe dessatiini suuruse maatüki, wastu Rannas olla Tamme krunti, ühe saja wiie /105/ Rubla ette Polna pärisomanduseks. Selle ostu hinna 105 Rubla /üks sada wiis rubla/ on müija täielikult kätte saanud 25 Aprillil 1895 aastal.
Johan Wekman &lt;allkiri&gt;
Hans Polna &lt;allkiri&gt;
See kaup sai pooltele ette loetud ja nende poolt allakirjutud.
Eesistuja K. Ambos &lt;allkiri&gt;
Liikmed H. Kask &lt;allkiri&gt;
               T. Rikko &lt;allkiri&gt;
               J. Krims
Kirjutaja as. Awing &lt;allkiri&gt;
2/XII 25 a. on ärakiri wälja antud Woldemar Wekmannile
