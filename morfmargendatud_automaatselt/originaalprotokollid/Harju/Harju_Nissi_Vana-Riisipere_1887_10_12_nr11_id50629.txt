29
Riesepere walla kohto ette kaewas kirja läbbi Tallinnast wanna mõttuse kaupmees H. Hindreus, et temmal Rieseperre mehhe Peaso Jürri Sucharowi käest 2 rbl. 85 kop rahha sada on, ja pallub kohhut et ta sedda Peaso Jürri Sucharowi käest wälja pärrib.
Kohhus kutsus Peaso Jürri Sucharowi ette, ja sai temmale se kaebtusekirri ette loetud, Jürri Sucharow ütleb: et minnul 2rbl 80 kop Mõttuse kaupmehhele wõlgo olli, agga minna saatsin sedda rahha Kollowerre laadalt ühhe tuntud Tallinna mehhega temma kätte Tallinna
Kohhus mõistis sedda kirja läbbi mõttuse kaupmehhele teada anda.
Kohtowannem: Jaan Sarik XXXKorwamehhed:Jürri Simson [allkiri]Jaan Lehbert XXX
