Ette tuli kaebaja Andres Tilkman ja kaebas, et Tilgu koha omanik Willem Runsman ei anna mitte seda Tilgu sauna temale, kus ta sees on elanud ja tema wanemad on ehitanud, ning nüüd mõisa kirjas mõisa maeaks seisis.
Ette sai kutsutud W. Runsman ja tunnistas, et Lantrad Baron O. v. Butberg kelle kääst tema selle koha ostis on selle sauna temale lubanud ja sepärast ei anna ta seda mitte A Tilkmanile.
Asi jäi poleli kunni saab Baron O v. Putbergi Hera kääst järele kuulatud.
