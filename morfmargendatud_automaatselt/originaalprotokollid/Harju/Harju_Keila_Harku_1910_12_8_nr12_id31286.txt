1910 aastal Detsembri kuu 8 päewal ilmusiwad Harku walla kohtusse surnud Karel Bergi pärijad Miina Kewis, Liisa Berg ja Otto Berg, kes kohtule isiklikult tuntud on ja kelledel woli on wabatahtlikusi aktisi teha ning palusiwad, et nende wahel tehtud suusonaline waranduse jagamise proekt kohtu poolt akti raamatusse saaks sisse kirjutud ja ärakinnitud.
Kõik ülemal nimetud pärijad on 3 Nowembril 1910 a. Harku walla kohtu poolt päranduse õigustes kinnitud /waata akt No 170/ Pärijad annawd ülesseet jagotalo parandus on 450 rubla mis nad jargmiselt soowiwad jagada.
Miina Keewis wõttab Tanawaotsa koha, mis päranduse jäljast järele jäi oma pidada ja maksab pärija Otto Bergile kakssada kakskummend wiis  /225/ rubla wälja, kes siis Tanawaotsa kohast ja ülepea Karel Bergi pärandusest enam ühtegi ei wõi pärida, seda nimetud 225 rubla maksab ta käesolewa 1910 a. jooksul Otto Bergile kätte. Liisa Bergi osa ükssada kaksteist rubla wiiskümmend kop. jääb Miina Kewise kätte hoiju peale, kuna Liisa Berg ise Miina Kewise juurde elama jääb kes teda surmani peab ulewal pidama. Otto Berg ei taha Liisa bergi osast selle surma puhul uhtegi pärida Koha wõlgade eest wastutab Miina Kewis, niisama täidab tema ka kõik kohustused selle koha järele
O. Berg &lt;allkiri&gt; kirjutas alla enese ja Liisu Bergi eest wolikiri akt No 170/1910
XXX tähendab Madis Kewis, kes oma naese eest, kui seaduslik eeskostja allakirjutas
Seesinane waranduse jagamise proekt on peale sisse kirjutamist pärijatele ette loetud ning nende poolt allakirjutud mis oma allkirjadega tõendame.
Kohtuesimees O Reinde  &lt;allkiri&gt;
Liikmed  H Meri &lt;allkiri&gt;
                R. Korsen &lt;allkiri&gt;
                A. Tobon &lt;allkir&gt;
Kirjutaja Kasberg &lt;allkiri&gt;
