Ette tulli kaebaja Hans Heimann (randrütel) ja kaebas, et temma on linnast tulles ommas asjad: 1 habbemenuga, ühed kärid, 1 toop wina, 1/ waka, kaero, 1 särk, alluspuksid ja üks rätik ümber, annud Karel Korelli kätte, tema hobbose peale, Ja K. Korel on Kärid, habbemenoa ja wina pudeli rättikuga ära kautanud.
Ette sai kutsutud K. Korel ja tunnistas, et need asjas on Kallaste kõrtsu jures ärra warrastud; temma hobbune jooksnud sealt ärra ja ta saatnud Hindrik Kammala hoost saatnud tagasi toma ja kui H. Kammal hobbosega tagasi tulnud, pölle neid asjo mitte ennam peal olnud;
Teisel päwal läinud temma kohhe neid asju tagga otsima, ja Liso Plaum (H. K. perrenaene) on temale üttelnud, et ta neid asju näinud H. Kammala käes.
Ette sai kutsutud H. Kammal ja küssitud miks sa sedda teinud?
Temma püidis kül ennast weel wabbandada, mis ommile wägga kahtlane olli, ja wimaks tunnistas, ka, et ta sedda on teinud.
Sai mõistetud, et H Kammal peab need asjad wälja maksma, need tullid 203 kop. maksma ja peale selle 50 walla laeka trahwi.
Kohtowannem K Kuutmann XXX
Kõrwasmehed O. Mänd XXX ja W. Mänd XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
Kohhus mõistnud ja mõistus täidetud ja trahw makstud nimmetud päwal.
