Astus kohtu ette kaebaja Karel Annus ja kaebas, Karjatooma Karel Arras laenas tema kääst 2 Rbl. 50 Kop. raha ja ostis Mäe kõrtsus selle raha eest oma raha kotti leiuksid, üks korw olut, ja toop wiina, nüüd ei taha tema seda laenu tagasi maksta.
Karel Arras sai ette kutsutud, tema tunnistab seda toeks, et olla Karel Annuse kääst 2 1/2 Rbl. laenanud ja ka raha kotti liiku selle raha ette ostnud, Otto Pless olla teda petnud ja olla kõrtsus kiitnud, et tema raha kotti ola leidnud, ja muidu kätte ei anna, kui korw olut ja toop wiina leiuksi ei saa, tema Karel Aras olla seda uskunud ja olla liigid ära ostnud, siis juhatanud Otto Pless teda Hüüru Möldri juure, sest Hüüru mölder olla tema rahakotti leidnud 23 Rbl. rahaga ja tema nõuab nüüd, et Otto Pless peab selle õlle ja wiina wõlgu Karel Annusele ära tasuma, miks ta teda olla petnud.
Otto Pless tunnistas, tema pole mitte ütelnud et tema isi selle raha kotti leidis, tema ola aga ütelnud, et kui Karel Arras korw õlut ja toop wiina ostab, siis tema ütleb, kus raha kott on, sest Hüüru mölder olla teda käskinud kõrtsus järel küsida, kas keski pole oma raha kotti kautanud. Et Karel Arasel raha kadunud oli siis olla ka tema selle teaduse ette liiku nõudnud.
Kohtu otsus: Karel Arras peab Karel Annuselle tema laenu wõla ära tasuma ühe nädala jooksul, ja kannab isi oma liigu ostmise kahju, miks ta lasen ennast kõrstus joomameestest pette. Arras maksis kohtu ees oma wõla ära.
Kohtumees Juhan Humberg XXX
Korwasmehed Gustaw Reindes XXX Reinhold Korsen XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Leppitud ja makstud 17 Küün 1887
