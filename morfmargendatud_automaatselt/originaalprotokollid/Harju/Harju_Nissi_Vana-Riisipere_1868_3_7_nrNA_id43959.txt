3) tulli ette Fridrik Wilberg kedda Peterpuri Linnast seia sadik olli wangi wisil todud selle pärrast et temma seäl olli aigeks jänud ja siis passi aeg ülle joudnud ja Hakenrihi jurest ka Rehnung sadetud et Nurme wallal on Fridrik Wilbergi eest maksta 22 Rub 65 kop ja nüüd tahhab uut passi sada.
4) walla koggodus lubbas talle passi anda pole aasta peale selle 7mea Septembrini agga siis peab temma muist ommast wöllast ärra maksma keikse wähhem 12 Rubla öbbedad et nüüd peab walla laekast temma trahwi wälja maksetama 22 Rubla 65 kop ja eddasbiddi peab Fridrik Wilberg issi jälle katsuma keik omma wõlgo wallale ärra maksta.
5) Ja Fridrik Wilberg lubbas sedda hea melega omma jouu möda tassuda
Tallitaja Willem Petohwer XXXabbi mees Juhhan Meinwalt XXXabbi mees Juhhan Sepateos XXX
