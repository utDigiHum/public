Tallinnast kaupmees Gustas Kärp tuli kohtu ette ja kaebas, et Habersti Toomapere sulane Jüri Awik, kui linnas tööl käinud, tema poest 34 rubla 92 kop ette wõlga söönud, mis tänini maksmata on.
2, Jüri Awik astus ette ja tunnistas, et temal kül nenda palju wõlga on jäänud, kuida Gustas Kärp ülesannud.
Kohus mõistis, et Jüri Awik peab nüüd kohe omast wõlast 10 rubla äratasuma ja Mihklikuus 10 rubla ja 1885 aastal 10 rubla 92 kopp. Jüri Awik tasus 10 rubla ära.
Kohtuwanem XXX G. Suurmann
Kõrwamees   Gottleb Torbek &lt;allkiri&gt;
teine              XXX And. Jaakmann
Kirjutaja Joh Tasson &lt;allkiri&gt;
Lõpetud.
