Kohtu ette sai kutsutud Tomas Lasman (Wardi koolmeister) ja temma käest küssitud: Mis põhja peal temma weel palka pärrib. Tomas Lasman ütles: eks mul olle Kontraht mis peale ma palka pärrin. - Siis sai Kontraht ette loetud, (Mis on Wollimeeste Protokollis 87 lehhe külle peal ülles kirjutud)
Nenda: se Kontraht saab Lehheta walla poolt kinnitud Kolme aasta peale 1. Aprili ku päwast 1872 aast. senni kui 1se Aprili ku päwani 1875. ja saab igga aasta 15 rbl walla-laekast ja 15 rbl wallast kotjatud.
Agga Lehheta Kassa konto ramatus seisab üllewal Kolm kord 15rbl. wälja maksetud. Essimene kord 2.Aprilil 1872. Teine kord: 11. Aprilil 1873. ja Kolmas kord 8 Aprilil 1874.
Siia ütles Tomas Lasman: et temma olla üks aast. enne  sedda Kontrahti aega, ilma kontrahtita teinud, et se I kord, mis 2 Aprilil 1872 on wälja maksetud, selle möda länud aasta ette olnud, et wald olla temmal süggisel 1871. palka jure lubbanud 30 rbl. Agga wollimeste Protokollis 84. lehhe külges, 19.Oktober 1871- ei seisa mitte 30 rbl üllewal, waid agga nenda: Et meie lubbame ennem omma koolmeistrile palka jure, kui lapsi wõerase kohta pannema. Ja selle peale tunnistasid kõik koggokond: et nemmad on palka jure lubbanud; agga mitte sest üllemal nimmetud päwast kui sai lubbatud, waid sest aegast kui Kontraht hakkab 1.sest Aprilist 1872 ütteltes:egga meie ühhe aasta sees kaks kord kaupa ei te, meie wast Jürri päwast 1871 alles teggime kaupa, et T.Lasman lubbas selle aasta wanna koolmeistri palga arroga tenida, 6 rbl. 60. kop. (ja kolimaea koht 20 rbla eest) mis on ka walla laekast wälja maksetud 25mal Oktobril 1871. Kassa konto ramatus.
Siis ütles T.Lasman et temma om kaks kuud enne Jürri päwa tulnud, weel wiet aastat.
Siis mõistis kohhus: et nende 2 kuu ette on sul weel hõigus palka pärrida, ja lubbasid selle 2 ku ette maksa, mis ülle nelja aasta on teninud. Sest et Konto ramatus seisab üllewal 4 kord wälja maksetud, 3 kord 15 rbl. neljas kord 6.rbl 60 kop. sest missugguse aasta ette woime kaks korda maksa sest nelli aastad olled olnud ja nelli kord on maksetud.
Ja tunnistawad weel kõik koggukond et temma Tomas Lasman on kohhe palka ette küssinud kui kontrahti teinud, ütteltes: mul on wägga tarwis, ja on ka nenda saanud. sest nenda tulleb se siis, et kewwadel 1875 ännam palka pärrimist ei olle.
Siis ütles Tomas Lasmann: et ma ei pärri mitte ännam 15 rbl. waid 30 rbla sest olleks teie kohhe se 15 rbla wälja maksnud siis olleks sellega jänud.
Selle peale ei wõinud Lehheta walla kohhus ännam middagi mõista, waid Keiserliko kihhelkonna kohto holeks jätta.
Kohto mõistmisse jures ollid:
Pea kohtomees: Kustas Waldman XXX
Kõrwamehhed:
(Jaan Arro XXX
(Mart Wakker XXX
Kirjutaja JWiil [allkiri]
