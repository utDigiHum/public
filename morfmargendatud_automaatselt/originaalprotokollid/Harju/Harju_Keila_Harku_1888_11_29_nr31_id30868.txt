Astus Kohtu ette Saue walla liige Juhan Kelement ja kaebas tema tegi kahe aasta ette Hüüru mõisa teumehe Juhan Willbaumile üks paar saapaid, ja jai 2 Rbl. 80 kop. raha wõlgu, ja tema loosis ka oma tasku uuri ära, ja sai maha tehtud, kes uuri wõidab, se peab 3 Rbl. ette loosi wõtjatele olut ostma, Juhan Wilbaum wõitis uuri omale ja se kord ei olnud temal raha õlut osta, siis ostis tema Juhan Kelement oma rahaga selle õlle Kanamääe kõrtsus, ja J. Wilbaum ei taha seda raha ka mitte ära maksta.
Juhan Willbaum sai ette kutsutud tema tunnistas, seda toeks, et olla Juhan Klementile 2 Rbl. 80 kop. saapa raha wõlgu, aga seda õlle jooma raha ei tea tema mitte, tema wõitis küll loosi pealt uuri, ja uur sai tema kätte antud, siis nõudnud teised loosijad tema kääst liiku, et temal see kord raha ei olnud siis jäi liik ostmata.
Juhan Kellement olla pärast oma sõbradega Kanamäe kõrtsus kolme rubla ette õlut wõtnud ja ilma tema teadmata ära joonud, siis ei woi tema seda kõrtsu jooma rehnungid mitte maksta.
Juhan Kelemet ütles, et teised poisid, kes ka loosist osa wõtsis, aeasid tema peale, et peab liik ostetud saama.
Kohtu otsus:
Juhan Willbaum peab selle saapa tegemise wõla 2 Rbl. 80 kop. Juhann Kelementi kätte ära maksma kahe nädala jooksul.
Juhan Kelement kannab isi selle õlle jooma kahju, mis tema oma uuri loosijatele, ilma Juhan Willbaumi juures olemata Kanamäe kõrtsus ostis. 1) Temal ei olnud mingi seadusliku luba, loosimist osutata, ega ka ilma peremehe loata tema rehnungi peale õlut kõrtsust wõta, ka õppetuseks miks ta uuri enne kätte andis, kui weel liik kääs ei olnud.
Kohtumees: Karel Annus XXX
Korwasmehed Reinh Korsen XXX
                          Karel Kruusmann XXX
Kirjutaja G. Kollip​​​​​​​ &lt;allkiri&gt;
