Sai mõisa allitsusse kässo järrel kõige walla Tallo, ja wabbad kohha piddajadele teada antud: Nenda kui tallorahwa seadus: §66. sedda kässib.
"Et pärris Brõua on nõuks wõtnud: kõik kohhad tännewo süggise, laska ümber mõeta, ja krunti panna.
Et siis, kui uued krundid jubba teada on; wõiwad igga üks ommad kohhad taggasi rentida, ehk ka pärriseks osta."
Pea kohtomees: Jürri Hoidorff XXX
Korwa mehed:
( Jaan Petohwer XXX
( Jaan Simson XXX
Koggokonna kirjutaja JWiil [allkiri]
