Ette tuli kaebaja Johan Ütik. Et olli läinud Wadda Tõnno käest omma heinama wahhi palka sama 1 mest kord. Agga Tõnno polle annud. Et temma heinama olla ärra olnud södetud. Siis läinud Johann Ütik jälle teist kord palka sama, kus ta siis heasti s.roppi sanud.
Astus ette Tõnno Kulderknup. Ja ütles Et ta Johan Ütiku esiteks omma Keldrest wälja lükkanud ja pärrast õue wärrawas tedda wasto maad pannud.
Et Wadda heinama mitte wahhi holetusse pärrast polle kahjo sanud. Siis moistis kohhus. Et Wadda Tõnno maksab heinama wahhi palk 1 küllimit Rukkid ja selle lömisse ette 1 Rbl. walla laeka.
