Astus kohtu ette kaebaja Põlde koha wana rentnik Mart Wainu ja kaebas, et Harko moisa paris härra Barun Ungern Sternberg ei ole temale põlde koha pealt, õigel aeal üles ütelnud ja, et Mihkel Pihelgas ei maksa temale seda kulu ära, mis ta Põlde koha parandamiseks on teinud. Tema olla sele koha peale ligi 60 sülda kiwiaeda teinud ja rohu aea, ahju malmide ja keldri ette kuue aasta eest. Gustaw Põlderile 16 rub raha maksnud, ta nõuab, et uus ostu peremees Mihkel Pihelgas peab temale seda ära maksma.
Mihkel Pihelgas tunnistas, et se kõik tühine on, tema ola Põlde koha Baroni härra kääst ostnud, ilma mingisuguse kõrwaliste maksuteta, ega ole Mart Wainuga miski tegemist (Wain) Tema on Põlde koha pealt kõik looma laudad ära lahutanud, mis kohtu raamatu järele sele koha peal pidi olema, ja lubab need Mart Wainule seda kinkida, kui nemad siin kohtus leppima peaksid.
Koli raha ei sa mart Wainule moisa poolt mitte makstud, temale sai aasta enne üles antud, et kohad saawad müüdud ja tema kontrataeg oli täis saanud.
Kohus sundis neid leppima, Mart Waino ei wõtnud seda pakkumist mitte wastu ja palus protokoli kihelkonna kohtule ära kirjuta.
Peakohtumees Jurri Saalfeldt XXX
Kõrwasmehed Karel Andermann XXX
                          Juhan Humberg XXX
Sai kihelkonna kohtule ära antud.
