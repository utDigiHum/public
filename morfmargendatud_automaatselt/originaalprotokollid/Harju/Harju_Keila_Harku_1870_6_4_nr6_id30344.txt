Ette tulli kaebaja Rannamõisa kubjas Hans Kärbis ja kaebas, et temmal on Witti Kortsmiku Kristjani kääst 1 rubla rahha sada.Lihhawate Pühha Essimisse pühha pärrast lõunat tulnud H. Kärbis omma wennaga linna kirrikust; Paldeski maande peal Rakatka kõrtsis tulnud Witti Kõrtsmik ja mõnni mu Witti mees. Kõstsmik küssinud warsti H. Kärbise käest, maksa mulle ärra, mis sa minno jures ikked wõlga junud ja teised Witti mehhed tulnud warsti temma kallale tedda peksma, Temma annud kõrtsmikule warsti rubla rahha ja jooksnud ärra nende käest.
Witti Kõrtsmik Kristjan wastas selle peale, et ta olla ikka wõlgu jänud ja ka et ta pölle mitte tedda peksnud.
Kohhus mõistis, et Witti Kõrtsmik peab selle rubla jälle taggasi maksma, sest kõrtsi jomisse wõlga ei wõi ta mitte pärrida, agga H. Kärbise kätte ei sanud ka se rahha mitte ennam antud, waid walla laeka heaks arwatud. Sellega ollid mehhed leppitud ja kohhus otsas.
Peakohtomees K. Runsmann XXX
Kõrwam. K. Lensmann XXXX ja T. Korn XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
