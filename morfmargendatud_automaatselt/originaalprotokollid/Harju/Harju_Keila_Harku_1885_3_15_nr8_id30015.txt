Harku Hüüru walla kohtu ette astusid täna 15 Märzil s.a. õnsa Pere Mardi Andermanni wanem wäimees, ja tema waeste laste käämees Jüri Annus, Mardi wanem poeg Karel Andermann ja noorem wäimees Gustav Reindes, ja lasksid protokoli üles wõtta Pere Mardi kõige noorema poea Juri Andermanni waranduse, mis juba 7 aastad ilma indressita keskmise wenna Juhan Andermanni kääs seisab, ja peale selle, pool õmsa ema Katri warast, mis naad kahekesi pidid pooleks saama.1) Jüri omadus:		2 härga			150 Rbl. ette, Juhan müüs ära		1 siga			10 Rbl.		2 lamast talletega			10 Rbl		1 Wana wanker			5 Rbl		Rohuaea puud			10 Rbl		Pool heina küün			10 Rbl		kaks lauta			40 Rbl		Ema warast		pool osa raha			55 Rbl		pool osa lehmast			7 Rbl 50 KOp.		pool osa härga			30 Rbl		pool osa laudast			5 Rbl		Summa			3,32 Rbl 50 kop.		
Rohu aed oli kõige pohjaga Jüri omaks lubatud, mis Juhan peab ära maksma.
Peale selle üles antud waranduse, mis Juri osaks 1878 a. sai lubatud, jäi kõik seespidine maja kraam, ja ka see wara mis ema pärast juurde kaswatas Juhani kätte. Siis moistis walla kohus, et Juhan Andermann wiibimatta ühe aasta sees oma wenna Jüri Andermanni wara peaks Keiserlikku Kihelkonna kohtu sisse maksma, Juhan Andermann ei olnud selle otsusega rahul ja nõudis protkoli kihelkonna kohtule ära kirjuta.
Peakohtumees: Juhan Humberg XXX
korwas mehed Gustav Reindes XXX
ja                       Hind. Sarapuu XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
Protokol keiserliku kihelkonna kohtule ära antud.
