III.) Piddasid nõu Keiserliko Nissi kihhelkonna üllewataja kohto härrad palluda: selle maggasini wilja müimisse pärrast. Et willi on wägga sant, ja kallis hind selle ette, sest mitto on käinud jubba watamas, et on tahtnud osta agga on tühjalt ärra läinud, sest et majal mitmes kohhas on hind halwem ja ka willi parrem. Sepärrast arwasid koik wollimehhed ja Tallitaja selle hinna wälja:
Et 1 Tsetwert rukkid wõib 5 rubla ette ärra müa, ennem kui ta sees seisab, sest et rahha intressis tenib warsi selle kallima hinna kätte, mis aitas seistes suggugi ei teni. Kui sedda Keiserlik Nissi kihhel konna üllewataja kohto härra lubba annab.
Sedda tunnistab:
Tallitaja: Jürri Lindros. XXX
abbimees: Jaan Simson XXX
Wollimehhed
Jürri Reinberg. XXX
Jürri Trump. XXX
Juhhan Lindmann XXX
Koggokonna Kirjutaja; JWiil [allkiri]
