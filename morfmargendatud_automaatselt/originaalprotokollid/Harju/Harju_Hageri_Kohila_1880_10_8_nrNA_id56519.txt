Aga An tõendab kohtu ees et Hindrek olla teda sõimand kuradi lehm ja muud ja lükand wastu müüri ja kiskund jaki katki ja löönd teda neli kord. aga Hindrek ütleb, et ta ei ole teda löönd waid wisand sõimamise eest uksest wälja.
Anu Treitel rääkis kohtu ees, et tema alles nende tüli talis näind An tulnd talli ja and Hindrekule alwad sanad ja Hindrek wisand see eest tema uksest wälja kui ta tupa läind olnud An toas ja pühkind tuba ja tahtnud Hindrekut lua warega lüa kors löönd ta teda ja tema saand lua warrest kinni ja wiind Anne uksest wälja.
Aga Hindrek pole teda mitte löönd seda ütleb ta enast näind olewad.
Ja kohus mõistis, et on mõlemad wastamise tülitsend ja sõimand kumbki ühe rubla walla laeka maksma. Kahe nädala pärast peab se trahw kohtus makstud olema.
