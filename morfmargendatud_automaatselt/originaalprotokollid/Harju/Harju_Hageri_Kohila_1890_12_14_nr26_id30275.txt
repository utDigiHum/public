35. Rägapajo Mart Kullama Lesk Maria Kullama 38a. wana Õiged usku kaewab: Minu surnud mees Angerja wallast Rägapajo Mart Kullama kellega mina neli üks pool aastat abielus elasin, suri nüüd ära, minu wõerad lapsed matsid teda maha, ilma et mind tema matmise juure oleksiwad kutsnud, minul on temaga poeg Johannes sündinud kes nüüd 3a. wana on, kui mina temaga laulatud sain siis wiisin tema majase:
1 Wasikas = 6 R 40 kop
2 Hane = 2 R
1 Lamas pool aastat wana = 2 R
11 küünart takust riet 'a 13 kop = 1 R 43 kop
Summa 11 R 43 kop
ja enne surma lubas minu mees oma poja Johannesele:
1 Riidega kasukas = 10 R
1 paar wildsid püksa = 5 R
1 west = 3 R
nüüd ei anna minu wõeras poeg Mart Kullama neid ridid minu poja Johannessele kätte. Mina ei elanud oma mehega wiimsel ajal ühes, sest et meie läbi ei saanud, minu wõeras poeg Mart Kullama äritas minu surnud mehe minu wasto wihale.
Mina pärin oma surnud mehe warandusest
1) need assjad ja loomad, mis mina kaasa wiisin, see on:
1 Wasikas
2 Hane ja
1 Lamas
2) Need rided mis eespool kirjutud, see on:
1 Ridega kasukas
1 paar wildsed püksa ja
1 west ehk nende hind rahaga 18 R
3) Üle selle poja Johannesele tema isa Mart Kullama warandusest üks osa, nii kui seädus seda lubab.
Angerja wallast surnud Rägapajo Mart Kullama esimese abielo poeg Mart Kullama 27a. wana Õiged usku ütleb: Minu surnud isa ei jätnud mitte palju warandust järele, aga minu isal jäid kaa wõlad järele, nimelt:
Hans Perdigile 15 R raha ja 8 R Kartohwlide eest 8 tünd.
Liisu Ehkmann'le 2 R raha
Johann Käendler'le 15 R raha
1890 Moisa Intressid 35 R
1890 Landstaalaeka maks 10 R
Summa 85 R
ja üle selle ei ole mina seia saadik oma isa kääst palka saanud, mina pärin 20 eloaastast kunni 27 eloaastani palka, see on 7 aasta eest 'a 50 R = 350 R /kolmsada wiiskümend R/
Angerja wallast surnud Rägapajo Mart Kullama tüttar Maria Kullama 23a. wana Õiged usku ütleb: Mina ei ole oma surnud isa ja ema warandusest muud saanud kui süia, ja need rided mis mina igapääwases elus olen pruukinud mina pärin oma wanemate warandusest 25 R /kakskümend wiis R/
Angerja wallast surnud Rägapajo Mart Kullama 2 tüttar Leena Kullama 16a. wana Õiged usku ütleb: Minu surnud isa pani enne kui ta töistkorda abielusse heitis, minu tarwis Kihelkonnakohtuse 10 R raha, mina pärin oma isa warandusest 15 R.
Johann Kullama hoolekandja ja Wolinik pärib Leena Kullama eest seda samma 15 R Angerja wallast surnud Rägapajo Mart Kullama on järele jätnud warandust.
a) Rägapajo koht millest maksetud on 200 R /kakssada R/ ja
b) warandust on, mis takseeritud sai Kohtu eesistuja ja Kohtuliikme J. Kulderknup poolest nimelt:
1 Hobone = 10 R
1 Warss = 5 R
1 punasepöits härg = 30 R
1 punanehärg = 18 R
2 Wasikast 1 aastased = 7 R
1 punane Lehm = 15 R
1         dito          = 15 R
1 noor lehmikmullikas m.kint = 11 R
1 punane pull 2a. = 3 R
10 Lammast 'a 150 kop = 15 R
3 Wana Siga = 10 R
2 noort Siga = 2 R
3 Hane = 1 R 50 kop
1 Rautud wana Wanker = 3 R 50 kop
1        dito                        = 1 R 50 kop
1 Rautud Sahn = 50 kop
1 Rautud Regi = 1 R
1 Hobose rangid, waljad, ohjad = 50 kop
5 Wana wilja Kirsto = 2 R
2 Wiljanõu = 50 kop
2 wonanõu = 20 kop
1 wana Kast = 10 kop
12 Wilja Kotti = 1 R 20 kop
1 Kappi = 50 kop
3 Pada = 4 R
1 Sahk = 60 kop
10 Wikkatid = 1 R 50 kop
5 Sirpi = 1 R 50 kop
2 Kirwest = 1 R
2 Labidast = 60 kop
1 Raudkang = 50 kop
Summa 163 R 70 kop
Üle selle on wilja:
Rukkit 10 Ts 2 Tk.
Odre 5 Ts 3 Tk.
Kaero 5 Ts 4 Tk.
Kartohwlid 50 tündert kahes kuhjas ja kartohwlid 18 tündert keldres sööma tarwis. Kartohwlid ei olnud wõimalik külma pärast mõeta, on seepärast Martin Kullama ütlemise järele kirjutud:
Inimestele ja Loomadele saab wiljast arwatud kunni 23 Aprillini 1891
Rukkit 4 Ts 2 Tk.
Odre 5 Ts 3 Tk.
Kaero 5 Ts 4 Tk.
Kartohwlid 18 tündert mis Keldres;
Wiljast jääb waranduse juure:
6 Ts Rukkit 'a 7 R = 42 R
50 tündert Kartohwlid = 50 kop
Selle järele on Rägapajo Mart Kullama järele jätnud:
a) waranduss,
1) Rägapajo koha sees = 200 R
2) Loomad ja muu warandus = 153 R 70 kop
3) Wili ja Kartohwlid = 92 R
Kõik kokku 455 R 70 kop
b) Wõlad, mis töistele maksta, naago eespool kirjutud 85 R ja
d) Päriaid jäi surnud Rägapajo Mart Kullamal järele:
I abielust kolm last, nimelt:
1) Poeg Martin Kullama 27a. wana
2) tüttar Maria Kullama 23 a. wana
3) tüttar Leena Kullama 16a. wana
II abielust
4) Poeg Johannes Kullama 3a. wana
5) Lesk Maria Kullama 38a. wana
Selle järele jääb surnud Rägapajo Mart Kullamal warandust järele, kui wõlad on maha arwatud: 370 R 70 kop.
See Protokoll on nendel ette loetud naad ütlewad: Et see õiete on üles kirjutud ja on selle takseerimisega rahul.
Leppiwad:
1) Surnud Mart Kullama poeg Martin Kullama maksab oma isa wõlad, nimelt:
Hans Pärdig 23 R
Liisu Ehkmann 2 R
Johann Käendler 15 R
1890 Moisa Intres 35 R
Landstaalaekaraha 10 R
Summa 85 R
Sellega on kaa eespool kirjutud wõla saajad rahul.
2) Saab Lesk Maria Kullama oma mehe rided kätte; see on:
1 Riidega kasukas,
1 paar wildsed püksid,
1 willane west.
need said Lesk Maria Kullamale kätte antud.
3) Lesk Maria Kullama saab Martin Kullama kääst; mis ta mehele minnes sinna majase wiinud:
Jaanipääw 1891 - ühe lamba,
1 Septbr. 1891 - 2 Hane
1 Märtsil 1891 - 1 Wasika,
ehk selle hind 5 R rahaga.
4) Lesk Maria Kullama saab weel 20 R raha, nimelt:
1 Oktober 1891 - 7 R
1 Oktbr 1891 - 7 R
1 Oktbr 1893 - 6 R
Summa 20 R
5) Martin Kullama maksab isa warandusest: Õe Maria Kullamale 15 R ja ema warandusest: Õe Maria Kullamale 10 R = Summa 25 R ja see peab makstud saama 1892 aastal.
6) Martin Kullama maksab Õe Leena Kullamale 10 Nowb 1891a. 15 R /wiistöistkümend R/
7) Martin Kullama maksab wenna Johannes Kullamale 25 R /kakskümend wiis R/ see 25 R maksab Martin Kullama kohe praego wälja, ja Lesk Maria Kullama palub, et wallakohus seda oma kätte wõttab ja Intressi peäle paneb.
Sellega on kõik rahul surnud Mart Kullama Lesk Maria Kullama, ja tema esimesed abielo lapsed Maria ja Leena Kullama, ja tunnistawad seda oma nimedega.
Hoolekandjades Jaan Kulderknup ja Martin Kullama.
(27 Sep. 91. Martin Kullama on maksnud oma wõera ema Mari Kullama heaks wasika eest 5 R /wiis R/ ja 7 R /seitse R/ kokku 12 R. Raha Raamatu kwittung nr 93 - 1891.; 1893a. 27 Septbr Martin Kullama maksnud 25 R Raha Raamatus Nr 64/1893.; 10 Januar 1892 15 R maksnud Kassa Raamat Nr 10 - 1892.; Martin Kullama on maksnud oma wõeraema Maria Kullama heaks 13 rub. 30 Oktobril 1892 Kwiitung Nr 99.)
