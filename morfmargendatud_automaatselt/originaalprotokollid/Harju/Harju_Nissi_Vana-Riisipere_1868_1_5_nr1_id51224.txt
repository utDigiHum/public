Sel tännasel päwal astus Lehheta Moisa aidamees Hans Salem Moisa wallitsusse nimmel Lehheta walla koggokonna kohtu ette ja kaewas et Lehheta walla Wabbatmees Juhhan Bauman kes sel 28mal Joulu ku päwal astal 1867 teol on olnud ja Maschina Rehhe jurest wilia teise Rehhe jurde on widdand, tee peal härgi on armetumal wisil peksnud ühhe keppiga, mis agga Hans Salem isse ei olla näinud waid Moisa perremamsel olla näind.
Selle peale sai Moisa perremamsel Koggokonna Kohtu ette kutsutud ja tunnistas: et Juhhan Bauman olla Moisa härgi ühhe keppiga, mis ka Koggokonna Kohtu ette sai pandud, üks kaks künart pik ja ladwast üks pool tolli jämme kasse puust, on peksnud moda häria selga.
Juhhan Bauman kelle peale kaewati tunnistas et temma olla neid härgi lönud selle keppiga mis koggokonna kohtu ette sai näidatud agga mitte möda häria selga waid reisi möda.
Juhhan Bauman lubbas ilma kohtu moistmatta, Moisa sui heinaaegus kaks päwa tehia selle ette et temma neid härgi olle lönud.
Selle lubbamissega olli Moisa Aidamees Hans Salem kes Moisa wallitsusse nimmel olli, rahhul sellega et Juhan Bauman need lubbatud päwad Moisa teeb heina aial, agga kui härgadel sest peksust wigga sünnib siis Juhhan Bauman wastab selle ette.
Kohto Moistmisse jures ollid:
Koggo konna kohto peawannem Jaan Wohlman XXX
Koggokonna kohto korwamees Jaan Arro XXX
Koggo konna kohtu korwamees Karl Krussman XXX
