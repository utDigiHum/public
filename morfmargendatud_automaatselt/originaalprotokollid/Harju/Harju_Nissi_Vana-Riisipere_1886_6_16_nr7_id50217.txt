14
Kohto ette kaewas Metsa üllewataja herra Stern: Metsa põllemisse aial kui metsa jubba suremast jäust kustutud olli, siis piddid nelli meest weel sinna juurde wahhi peale jäma, agga Hinno perremehhe sullane Jürri Welk ei tahtnud mitte wahhi jäda ütles et temmal leiwa jures ei olle, Minna ütlesin minne Linsi perresse sealt saad sa süa, agga temma hakkas wanduma, ja mind sõimama, ja minna lõin tedda keppika.
Kaebtusse kandja Jürri Welk astus kohto ette ja kaewas et Metsa herra Stern tulli minno kallale ja loi keppi minno pähhe purruks ja lõi 7 korda minno pähhe nõnda et mo Pea wägga lõhki ja werrine olli, paljo werd jooksis et kõik mo riided werrised on.
Kohhus mõistis metsaherra Stern peab 2 rbl trahwi maksma, sest rahhast saab 1 rbl. 50 kop. Jürri Welk'ile peksu eest ja 50 kop. saawad walla laeka.
Metsa herra Stern maksis 2 rbl wälja, Jürri Welk wõttis 1 rbl. 50 kop wasto.
Kohtowannem: Jaan Sarik XXX" kõrwamehhed:Jürri Simson [allkiri]Jaan Lebert XXX
