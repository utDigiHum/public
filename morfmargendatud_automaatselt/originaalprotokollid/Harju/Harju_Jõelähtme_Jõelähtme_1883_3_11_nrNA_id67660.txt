3. Tunnistus Mart Hallik ütleb selle peale mis Mart Hawam 11. Webruar protukoll N 1 kaebas et se naine Marri Remmelt on kül Mart Hawami lapse tapjaks ja wargaks sõimand aga se Mart Hawam on ka nisamati wasto sõimand.
Kohtu käiad Mart Hawam, Jürri Remmelt ja tema naine Marri lepisid üksteisega ära. 
Lepitud 11. märz 1883.
4. Kaebaja Jaan Hawam ütleb: et Hans Katkumä on ise temale räkinud, et Jürri Rauam on temale nou and, tema (Jaan Hawam) aita ära lõhkuda ja käskinud linnast üks kolm hobust ja teisi seltsi mehi kaasa wõtta ja se Jürri Rauam olle siis weel Hans Katkumäele 3 rubla luband anda, kui ait oleks lõhutud sanud ja ütelnud: kui meie rahwast juhtub kiski seda nägema, sest ei ole ühtegi karta et se wälja tuleb.
Hans Katkumä ütleb: et se Jürri Rauam on temale seda tõesti sügise enne Jogelehtme lada aega Tallinnas Toropilli Körtsu ees räkinud.
Kohus arwas selle kaebtuse tühjaks sest se üle tunnistust ei ole ja teiseks et selle Hans Katkumäe juttu mitte ei woi uskuda
allakirjutand
Kohtuwanema asemel Andres Nairismä
Kõrwamees Gustav Nairismäe
Kirjutaja Ovir 
