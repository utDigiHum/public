1. Harkust saunamees Jaan Leesmann tuli Habersti walla kohtu ette ja kaebas nenda: Ta saatnud kahe nädali eest oma poega linna ja selle tee peal Habersti Mustjõe kõrtsi ukse ees on Humalast Juri Tõnsel ja Wäenast Juhan Annet rangaste peksnud, et pois mitto päewa maas haige olnud.
2. Pois Jüri Leesmann tunnistas: pool wersta linna pool Mustjõe kõrtsu sõitnud kaks meest kaks meest tema järele. Teine tulnud ree pealt maha ja löönud tema lume sisse ja küsind: Kus sa kurat silgupütti panid. Sidund tema käed kinni ja toonud Mustjõe kõrtsi juure tagasi. Tõnsu Jüri Tõnsel pärind silgu pütti ja löönud ta tee peale maha tagund saapaga temale selga. Temal jooksnud weri suust wälja. Kõrtsi naene peastnud tema käed lahti. Silgupütti olla üks naene tee pealt leidnud.
Juri Tõnsel ja Juhan Annet oliwad kirja läbi kohtu kutsutud; aga ei tulnud mitte.
3. Aprillis tuli Jüri Tõnsel kohtu ette ja tunnistas, et tema kangest joobnud olnud ja seepärast ühtigi ei mäleta, mis sündinud. Tema poeg rääkind, et Jüri Leesmann olla nende 10 naela silgu pütti ree pealt ära wõtnud. Poissi J. Leesmanni põle ta mitte peksnud. Seda wõib tunnistada Hindrik Hiiter.
Tõnso Jüri poeg Jüri Tõnsel tunnistas: Tema näinud läbi akna, et halli kuuega pois olla nende ree pealt pütti ära wõtnud ja linna poole läinud. Ta sõitnud Juhan Annetiga järele ja toonud poisi tagasi. Poisi käed olnud kül kinni seutud, aga peksta põle ta mitte saanud. Pois olla ise tunnistanud, et tema pütti ära wõtnud ja leppa alla pannud.
5, Leena Treüfeldt tunnistas et tema pütti rohkest 200 sammu linna poole Mustjõe kõrtsi tee pealt üles wõtnud ja silgu rida läinud linna poole. Arwas et nemad tugewast sõites pütti ära kautanud. Poissi põle ta näinud.
Juhan Annet jäi teistkord kohtu kutsumise peale tulemata.
6, 30 Aprillis tunnistas Hindrik Hiiter Jüri Tõnseli käemees: Jüri Tõnseli poeg tulnud tuppa ja üttelnud: Üks pois wõttis pütti ära ja läks linna poole. Juhan Annet sõitnud järele ja toonud poisi tagasi. Pois olla ise üttelnud: "Ma wõtsin pütti ja panin puu alla ja üks naene wõttis sealt ära. Ka seda näinud ta, et Jüri Tõnsel poissi lükkand ja et poisi hambad olla werised olnud, ja palja peaga.
7, Kohtu poolt kutsutud Tawet Luurmanne tunnistas; pool wersta linna pool Mustjõe kõrtsi tulnud temale pois halli kuuega wasta ja selle järel Juhan Annet. Löönud poisi lume sisse. Tulnud warsti tagasi. Trahteri ees üttelnud Treufeldi nane, et tema ühe silgu pütti tee pealt leidnud ja käskind teda kõrtsu ees nimetada. Ta annud kõrtsu ees teada. Jüri Tõnsel täis joonud tulnud tema juure ja löönud tema hobust kolm korda ja kippunud ka tema kallale. Teised kõik olnud täis joonud. Jüri Tõnsel löönud poisi pähe, see kukkunud tee peale, siis tagund saapaga selga. Juhan Annet karjunud ja pigistanud poissi. Poisil tilkud suust ja ninast werd wälja. Kõrtsu naene lükkand Jüri Tõnseli poisi kallalt  ära ja peast käed tal lahti.
8, Kõrtsi naene Emilie Palhberg tunnistas, et Jüri Tõnsel poissi peksnud saapaga ja pois olnud werine. Tema peastnud käed lahti.
Kohtu käijatele sai kohtu poolt mitmel korral leppitust pakkutud, aga see ei sündinud nende wahel mite.
Kohus mõistis:
a' Jüri Tõnsel, kes joobnust peast, kui meeletu, poisikest armutu peksnud, maksab poisile 5 rubla ja selle 3 rubla Habersti walla laeka trahwi et oma teed targa peaga peab käima.
b, Juhan Annet, kes poisikest kange külmaga kinni seutud kättega ja palja peaga, ilma et ise asja põhja selgeste oleks teadnud, on piinanud, maksab poisile 5 rubla ja 3 rubla Habersti walla laeka, et kolmel korral kohtu kutsumise wastu pannud ja ilma asjata kohtu käimist pikkendanud.
d, Pois Jüri Leesmann ei ole ise, ega keegi teine jõudnud selgeks teha, et pütt mitte temast põle wõetud saanud, maksab Jüri Tõnselile 10 naela silku 50 kopp. wälja ja 1 rubla Habersti walla laeka trahwi.
Kohtuwanem XXX J. Kandberg 
Kõrwamees: XXX Krist. Jerwan
                       XXX H. Grossfeldt
Kirjutaja: Joh. Tasson &lt;allkiri&gt;
Protokoll wälja.
