Hans Jolter kaebab: et Hindrek Sepa naene Leenu on rääkinud, et tema on Ohtu mõisa heinamaalt heinu wiinud härge ja wankrega. Hans Jolter ütleb, et temal wankert ei ole olnud, waid paljad härjad ja sau köis, need härjad ja köie tõin mina kohtu mehe Karel Malteri käest, ja wedasin nende härgega mõisa heinamaa peal seal kust ma heina kahasse tegin heinu kokku
Hindrek Sepa naene Leenu ütleb: Hans Jolter wiis mõisa heinamaalt heinu koju ja tuli minu tütre Tiu wastu tee peal ja maa ise nägin teda takka kui Hans Jolter heinakoormaga koju poole läks. Need heinad wiis tema härge ja wankrega: teine oli musta päits takka lüija, teine nriim wasakut kät.
Hans Jolter palub Protokolli: Saab antud Taehna Õhtu Harjumaa hakenrechter kohtu
Kohto peawanem Karel Malter 
Körwamehed Jaan Tasso 
                        Juhan Päilmann 
kirjutaja Kaarel Sumpach
