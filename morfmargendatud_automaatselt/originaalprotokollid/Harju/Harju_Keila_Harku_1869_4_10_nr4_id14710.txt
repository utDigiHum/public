annamõisas sel 10. aprillil 1689
Koos ollid Tallitaja K. Gutmann ja kohtu kõrwamehhed K. Luker, J Lipp.
Ette tuli Willem Lensmann, ja kaebas, et temma on Juhhann Lensmanni keldrisse sügisel 1868  36 tsetweriki kartuhwlid winud (seemne tarwis, ja talwe on neist 5 tsetweriki ärra tonud ja siis on weel piddanud 31 tsetweriki senna jäma); sedda  tunnistas ka Willem Lensmanni tüdruk Jula Pedupu, et temma on kõrwas olnud kui kartuhwlid said keldri panud. Agga kewwade Aprilli ku essimiste päwade sees 1869 on W. Lensmann omma kartuhwlide järrele läinud tüdrukuga ja põlle ennam leidnud, kui 18 tsetweriku ning 13 tsetweriki on ärra kaddunud.
Kohhus kulas asja järrele leiti et W Lensmanni tunnistus tõssine olli.
Agga Juhann Lensmann ütleb et temma ei tea, kus ned Kartuhwlid on sanud.
Sai mõistetud et Juhhann Lensmann peab kaks kolmantikku jäggu neist ärra kaddunud Kartuhwlidest maksma, sepärrast et on omma holealla wõtnud, ja 1/3 jäggu peab Willem Lensmanni kahjuks jäma, sepärrast, et temma põlle mitte neid kartuhwlid Juhann Lensmanni kätte mõetnud, waid on agga koddu moetnud. Sellega ollid ka mõlemad leppind ja Juhann Lensmann lubbas Wilem Lensmannile maksta 4 rubla 60 Copp., mis kohhus olli mõistnud nende 2/3 jäggu ärra kadunud kartuhwlide eest.
Peak. K. Trauberg XXX; K. Luker XXX ja J. Lipp XXX
Kirjutaja M Kehrt [allkiri]
