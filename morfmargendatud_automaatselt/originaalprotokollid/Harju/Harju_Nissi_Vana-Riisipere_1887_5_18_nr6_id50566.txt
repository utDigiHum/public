9
Riseperre walla kohhus olli kohto korra pärrast walla kohtomaiase kokko tulnud, kohto ette kaewas Riseperre walla Tallitaja Jürri Tamberg et Liwa Jaama Stalmeister Maddis kaks korda kui minna posti peal omma asjo käisin tõimetamas ja posti poiste käest kes Riseperre walla mehhed on, walla maksude rahha wasto wõtsin siis olli temma koera wiisil seal kõrwas aukumas, kus temmal tarwis wahhele räkida ei olleks olnud.
Kaebtusse kandja Liwa jaama Stalmeister Maddis astus kohto ette ja ütleb selle kaebtusse wasto, et ta jüst rummalaid sõnnu ei olle räkinud, waid on mõnned sõnnad sinna wahhele rääkinud.
Kohhus mõistis: Liwa jaama Stalmeister Maddis peab 1rbl. trahwi maksma Rieseperre walla laeka sellepärrast et ta Tallitaja asjade toimetamisse jurdes omma suud kinni ei pea.
Kohtowannem: Jaan Sarik XXX" kõrwamehhed:Jürri Simson [allkiri]Jaan Lehbert XXX
