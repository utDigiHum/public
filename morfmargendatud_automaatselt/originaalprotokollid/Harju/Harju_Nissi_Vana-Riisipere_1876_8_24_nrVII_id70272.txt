29.  Kohtu ette tulli mõisa wallitsuse kaeptus, et Leppiko s. Jaan Saarberg mõisa laudu ja lattisi on warrastanud.
Nende warrastatud laudade ja lattidega kojo minnes juhtus temma Rieseperre mõisa pärris herra Baron v Stackelbergi wastu tullema ja said need warrastatud lauad ja lattid, nenda kui ka üks wäike wanker, kerwes ja temma müts ärrawõetud
Mõisa wallitsus annab nüüd Leppiko s. Jaan Saarberg walla kohtu alla, et temma üllewel selgem tähhentud warguse pärrast ihhu nuhtlusega trahwitud saaks.
Kohtu ette astus kaeptuse kandja Leppiko s. Jaan Saarberg ja sai kohtu poolt küssitud: mikspärrast sa laudasi ja lattisi mõisast warrastasid? wastus: ma piddin neist ust teggema.
Kohtu otsus: kohhus mõistis, et Leppiko s. Jaan Saarberg peab sama 20 witsa hopi omma üllewel nimmetud warguse pärrast ja siis mõisa wallitsuse käest ommad tö nõud ja mütsi kätte palluma.
Kohus täidetud 24. August 1876
