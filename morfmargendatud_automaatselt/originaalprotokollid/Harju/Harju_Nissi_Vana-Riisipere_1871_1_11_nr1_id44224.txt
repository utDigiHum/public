Nurme koggukonna koli maease ollid kokku tulnud walla perremehhed ja sullased wollimeeste seädmiste pärrast; nenda kui se üllema kohtu poolt seätud on, et kelle aeg täis peab lahti suama ja ued jälle assemele.
a. lahti sai perremeeste hulgast Keldrema Juhan Nienewald
Mõisa sullaste hulgast Tõno Pillerpau
perremeeste sullaste hulgast Jaan Pressman
Wallitsetud said.
b. Perremeeste hulgast Kesknõmme Mart Roswalt
Moisa sullastest Jürrij Hinreksohn
Perremeeste sullastest Losso Jakob Janilind
Saunameestest Losso S. Jürrij Bekmann
Sedda kinnitawad koggokonna nimmel
abbimees Hans Waltsnep XXX
abbimees Juhan Siemson XXX
Tallitaja: Mart Meinwalt [allkiri]
