Et tuli kaebaja A. Minner (rätsep) ja kaebas, et ta on 8 Septembril linna minnes Kalaste Kõrtsi sisse läinud Siis tulnud Karel Taming kohe ta kalale ja lönud temale rusigaga wasto rindonenda et maha kukunud ja siis peksnud seal maas J. Postmaniga kahekste. Ja kõrtsi sulane Jüri Leopas näinud seda.
Kohtuwanem K Gutmann XXX
Kõrwasmehed O. Mänd XXX ja W. Mänd XXX
Kirjutaja M. Kährt &lt;allkiri&gt;
Jäi teiseks koraks.
