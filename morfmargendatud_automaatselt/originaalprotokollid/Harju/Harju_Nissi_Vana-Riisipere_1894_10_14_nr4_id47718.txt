1894 goda Oktjabrja 14 dnja Rizenbergskii krestjanin Rizenbergskoi volosti Jugan Kurfeld i upolnomotšennõi vlad. im. Rizenberg H. fon Frei litšno izvestnõje volostnomu sudu prosili o vnesenii v aktovuju knigu etogo Suda predstavlennõi imi pismennõi dogovor sledujuštšago soderžanija.
Ehitamise kaup
Metsataguse Juhann Kurfeld wõtab oma peale kewadel 1894 enesele üks uus elu hone, oma kuluga üles ehitada. Palgid ja materjal saawad mõisa poolt ilma rahata antud. Tema saab selle ehituse eest palgi rajumise koorimise, wedamise eest  katuse õlgedega, tegemise aknade ja ustega tükkis üle pea /20 rub./ kakskümmend rubla.
Juhan Kurfeld tunnistab et selle rahaga on kõik makstud ja koha äraandmise ajal, ei ole temal ühtki enam tasumist selle eest pärida.
N Gefria
J.Kurfeldi palve peale Juhhan Anneridieg [allkiri]
Dogovor etot po vnisanii jego v aktovuju knigu bõl protšitan storonami, tšto i svidetelstvujem.
Predsedatel Suda J.Sesmiin [allkiri]
Tšlenõ H Gefria [allkiri]
J.Linroos [allkiri]
J.Simson [allkiri]
Pisar Orgusar [allkiri]
