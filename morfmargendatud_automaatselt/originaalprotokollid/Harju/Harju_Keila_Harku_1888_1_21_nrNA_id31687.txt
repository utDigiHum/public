Kihhelkonna kohto herra poolest läbi waadatud: ja leitud, et 1) Et need Walla kohto moismised ärralainud 1886/87 aastal nimmetud No 20 ja No 21 weel täitmatta jänud, ja annan sedda käsko essimisse Kohtopäwal selletus tehha ja minnule siis kohhe teada anda et selle Walla kohto moistminne täidetud on
2) Polle mitte nähhe et No 10 ja No 11-kumne aal moistetud trahwi Rahhad on sisse tulnud, ja peawad walla moistmissed kohhe täidetud saama.
Woore mõisas 21 Näärikuu peawal 1888
Otto Joh Mühler &lt;allkiri&gt;
Kih Koh K. Pea Wanem.
