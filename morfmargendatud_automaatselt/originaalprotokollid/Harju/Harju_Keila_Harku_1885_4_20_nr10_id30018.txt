Prits Tiitsmann Kaebas, et tema ola õnsa Pere Mardile 15 aasta eest Pere koha käärahaks 80 Rbl raha laenuks annud, ja nüüd tahab Mardi poeg Juhan Andermann oma koha ära müüa, siis nouab ta oma wõlga tagasi, kõige indressitega.
Juhan Andermann ja Karel Andermann Mardi poead said ette kutsutud ja nendele kaeptus ette loetud, nemas tunnistasid seda tõeks ja lubasid oma Prits Tiitsmanni raha ära maksta. Kohus sundis neid indressidega kokku leppima ja naad leppisid sellega, et makswad see aasta selle wõla ära, ja 15 a. pealt 20 Rbl. indressid juurde (100 Rbl.) Leppitud.
Peakohtumees: Juhan Humberg XXX
korwas mehed Gustav Reindes XXX
ja                       Hind. Sarapuu XXX
Kirjutaja G. Kollip &lt;allkiri&gt;
leppitud.
