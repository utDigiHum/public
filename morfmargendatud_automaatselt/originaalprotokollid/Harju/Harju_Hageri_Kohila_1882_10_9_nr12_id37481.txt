15.
Tõnu Nickfeld andis kogukonna kohtule üles, et tema olla mõisa walitsuse lubaga Seppa tua kojasse kaks kammert oma materialist ehitanud, kelle eest tema tasumist nõuab. Mõisa walitsus on lubanud temale need ehituse kulud wälja maksta, seda hinda mis kogukonna kohus nende eest mõistab. Kogukonna kohtu ülewaatmise ja takseerimise järele said need kambred rohkesti kaks rubla wäärt arwatud.
(Prottokoll Nr 15 ja Nr 14 sai mõisa walitsusele wälja antud.)
