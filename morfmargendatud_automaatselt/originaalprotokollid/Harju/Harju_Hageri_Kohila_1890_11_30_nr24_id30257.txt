28. Kohila wallast Jüri Ems 42 a. wana Luth.usku kaewab: Minu naene Leena ütleb: Et mina olen surnud Jaan Elba raha 8000 R ära warastanud, ja linna wiinud, ta sõimab mind emis j.n.e.
Leena Ems Jüri naene 35a. wana Luth.usku ütleb: Minu ema wend Jaan Elba kes nüüd surnud on oma raha ise minu mehe Jüri Ems kätte annud, mina seda ei ole näinud, ega ei tea seda, aga mina kuulsin et keegi inimene ütles: Kohila mõisa põllul Kartohwli üles wõtmise aegas, et seda raha on 8000 R olnud, mina ei tea koa seda inimest nimetada, kes seda wiisi ütles. Minu mees sõimab kodo mind, meie tülitseme kodo, seepärast mina seda kaewan; tunnistused on kes teadwad:
1. Liiso Kant,
2. Anno Allann, mo ema.
3. Mari Elba,
4. Juula Ehkmann,
5. Jüri Mühlberg.
1. Liiso Kant Gustas' naene 61a. wana Luth.usku tunnistab: Mina ei tea, on Jaan Elba raha järele jätnud, ehk ei ole, ja ei tea kellele see raha on saanud jaetud, kui seda on jagatud.
2. Anno Allan Priidiko lesk /Leeno Ems ema/ 67a. wana Luth.usku tunnistab: Minu wend surnud Jaan Elba jättis raha järele nelikümend R /40 R/ mis mina Jüri Ems kätte andsin, ja minu väimees Jüri Ems mattis selle rahaga Jaan Elba maha; üle selle oli Jaan Elbal weel kolmkümmend Rubla /30 R/ selle jautasin Jüri Ems ja Mari Elba lastele, koa õppetajale sai 1 R antud; nõnda ei jäänud raha sugugi järele.
3. Mari Elba 73a. wana Luth.usku tunnistab: Minu surnud wend Jaan Elba jaotas enne oma surma oma raha ära, mina sain 19 R ja minu Õetüttar Leeno Ems sai on 30 R saanud, minu töine Õde Anno Allann sai 50 R ja mattose rahaks jäi 40 R. ükssada R oli temal Linnas selle laskis nelja aasta eest ära tuua, mina rohkem ei tea.
4. Juula Ehkmann Johanni naene 39a. wana Õiged usku tunnistab: Mina ei tea on surnud Jaan Elba raha järele jätnud ehk ei ole; neli aastat tagasi tõin 100 R temale Intressi peält koio, selle andsin tema kätte.
5. Jüri Mühlberg 70a. wana Oiged usku tunnistab; Mina ei tea on surnud Jaan Elba raha järele jätnud ehk ei ole, ja ei tea kellele tema raha on saanud, kui seda on järele jäänud.
Leena Ems Jüri naene ütleb weel: et mina sain Jaan Elba kääst 30 R seda olen mina ära pruukinud.
See Protokoll on nendele ette loetud naad ütlewad see on tõsi mis naad on rääkinud, ja et see õiete on üles kirjutud.
Keisri Majestäti Ukasi põhjusel Mõistetud:
1. Jüri Ems ja tema naene saawad kohtu poolest noomitud, ja kui naad edespidi weel kodo tülitsewad, siis saawad kiriko õppetaja juure saadetud noomida, naago Leeno Ems seda ise ütleb: Kui naad kodo ei tülitseks siis naad kaa raha wargusest ei räägiks; ja Jaan Elba oli Kohila mõisa sulane oma eloaeg sellest selgub et temal 8000 R raha järele jääda ega olla ei ole wõimalik olnud jätta.
2. Saab Leeno Ems trahwi alt lahti jäetud prg 8 punkt 3 põhjusel 1889 Sääduses Ajutised eeskirjad nuhtlustest, mis wallakohtude poolt peale pannakse.
