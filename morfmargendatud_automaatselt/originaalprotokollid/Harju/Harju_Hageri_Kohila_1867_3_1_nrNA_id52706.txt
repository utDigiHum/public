II)
Se perremees Tagadi Gustaw kaewab, et Kurtna walla perremees Saare Karel õhto sel 23m Traksi kõrtso jures ollnud, kus Karel ilja peale koddo minnes Leoperre perre Ouest läbbi läinud ja seal wargelwisil 1 Korm Ölgi peal pannud ja omma koddo wiinud, kus tema need Obbose jälge sees kuss Obbose raua sees üks Oak ärra olnud, mis Karli obbose raual ka ärra olnud, leidnud ja 16 Kubbo Olgi tema lauda pöningul leidnud.
Saare Karel tunistab et tema küll Õlle kormaga seal õuest läbbi omma koddo läinud agga need olled 20 Kubbo Süllaotsa Jürri Sild Angerjalt käst ostnud, mis sesamma Angerja perremees Jürri Sild kedda ka ette kutsoti ka selgeste tunnistab.
Kohhus tunistab Karlid süüst lahti, selle pärrast et mitte selge tunnistust põlle et tema need õlled warastand.
