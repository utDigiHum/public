Sel nimetud päewal ilmusiwad kohtusse Laitsest Jaan Kuller ja Keilawalla Ummala koggukonna liige Anton Kõmper selle palwega et järgmist kaupa kinnitada.
Anton Kõmper ostab Jaan Kulleri käest "Kase" sauna maad, mis 2 tessatini 1200 □ sülda suur on ja maksab kolmsada seitsekümmend wiis rubla. Ostja peab kõik teed mis selle koha pealt läbi weawad lahti pidama, ega tohi neid ära kautada. Ostu rahast on kakssadakakskümmendwiis (225) rubla ära tasutud ja puuduw raha 150 rubla saab järgmist termini moodi tasutud: Kewadel 1894 a. Jürripäewaks wiiskümmend (50) rubla ja Märtsi kuu 15 päewaks 1895 a. sadda (100) rubla. Nimetud wõla 150 rubla pealt maksab ostja müüjale 3 rubla aastas (kolm rubla) intressisi.
Jaan Kuller [allkiri]
Antun Kömper [allkiri]
Eesistuia J.Sesmiin [allkiri]
liikmed H Gefria [allkiri]
J.Linroos [allkiri]
J.Simson [allkiri]
Kirjutaja Wetleier [allkiri]
Selles kontraktis nimetud raha kõik kätte saanud 1895 aastal 24 Martsil
Jaan Kuller [allkiri]
Kohtu eesistuja JSesmiin [allkiri]
Kirjutaja Orgusaar [allkiri]
