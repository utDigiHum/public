1903 aastal Märtsi kuu 28 päewal Laitsi walla talupojad Jüri Fromann ja Jaan Antuk, kes kohtule isiklikult tuntud on palusiwad selle kohtu lepingute raamatusse oma suu sõnalist järgmist lepingut kirjutada
1 Jaan Antuk on Jüri Fromanni päralt olewa Kullapä koha krundi Uustalu nimelise maa tüki peale ehitanud oma kuluga üks elumaja, üks laut ja üks ait, mis sugused  hooned on 300 rbl. wäärt.
Nimetatud elumajal on rehe tuba külles ja ühe kattuse all on kaks lauta.
2. Jüri Fromann kohustab need nimetatud majad, mis on Antuki omandus, müimise alt, koha ära müimise korral, wälja jätma ja Antuki omanduseks tunnistama.
3. Nende majade seal kohapeal olemise eest ei nõua Fromann Antuki käest miskid tasu, aga kui Fromann koha ära müib, siis on Antuk kohustud uue omanikuga isi kokku lepima.
Koha omanik Jürri Promann [allkiri]
Maja omanik Jan Antuk [allkiri]
See leping on pärast lepingute raamatusse kirjutamist pooltele ette loetud ja on No1 all sisse kirjutud akti raamatusse.
Kohtu Eesistuja K Reinberg [allkiri]
Liikmed J Einberg [allkiri]
H Sauna [allkiri]
J Waker [allkiri]
Kirjutaja Melne [allkiri]
Sellest lepingust on Jaan Antukile 5 Detsembril 1903a. ära kiri walja antud.
