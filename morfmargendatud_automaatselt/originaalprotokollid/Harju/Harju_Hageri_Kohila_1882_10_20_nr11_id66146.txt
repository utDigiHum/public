7.
Tuli ette: Mõisa walitsus ja kaewab, Limando metsawaht Kristjan Grundström on mõisa üles annud, et Miili Ann Kärrakas on mõisa metsast ilma lubata, raiunud 17 puud. 1 puu 11 tolli, 6 puud 8 tolli 6 puud 7 tolli ja 4 puud 5 tolli tüükast jämedad; ja kolm sülda läbisegamine ladwa tutini pitkad.
Selle kaebtuse järele, sai Ann Kärrakas kohtu ette kutsutud ja temale see kaebtus ette loetud. Tema tunnistab, kui omale weikest lehma lauta läinud, on tema siis need puud Kirnult Nõukse peremehe käest ostnud ja oma maja juurest 4 puud raiunud 2 kuuse ja 2 männi puud, ja rohkem ei olla tema raiunud.
Selle peale ühendasid endid kohtu liikmed ja tegid see Mõistus: Se asi läheb Hakenrihteri kohtu seletada.
