Koha rentnik Gustaw Lauritz kaebas: Mudda Mart Gutmann, olli minno metsast 3 kasse puud raiunud, ühhe sain weel tema õuest kätte, kaks mitte.
Mart Gutmann tunnistas: Minna ei olle raiunud.
Siis tunnistas Gustaw: Minna wõtsin Mardi sealt metsast kinni, kui neid okse korristas, ja läksime temma õue üks puu olli seäl, agga kaks olli enne raiutud.
Mart tunnistas: Minna ei olle raiunud; waid korristasin neid okse, mis mahha olli jäätud.
Kohto otsus: Et Mardi maiast mitte neid kahte kaske ei leitud ja et need teisel päwal ollid raiutud, sellepärrast ei wõi tedda nende wargaks mõista; agga et Mart selle ühhe wasto waidleb, on tühhine, sest miks ta neid okse korristas, ja puu olli issi ka õuest leida, se on ommeti awalik, et ta selle warraas on, peab selle 8 tollise puu ette kahjo tassuma Gustawile 50 kopik ja 50 kop. trahwi walla laeka maksma tullewa kohtopäwal.
