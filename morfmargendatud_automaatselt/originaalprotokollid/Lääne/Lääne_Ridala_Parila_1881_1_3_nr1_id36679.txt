Kohto korra pärrast ollid, Parrila, Kebla ja Riddali koggokonna maias kokko tulnud kohto mehhed
Ette astus Jõesust kohto kämehhe Juhhan Krabbi naene Edo ja kaewas, omma walla mehhe Uetoa perremehhe Prido Reinporn peale, nenda: Suggise Kollowerre lada aeges on Prido Reinporn temma meest nenda peksnud Kullamä kirriko al kõrtso toas, essite wotnud tedda korrist kinne, kui kääd sealt lahti sanud siis wottis rinnust kinni ja lönud wasto müri ja siis rohhunud tedda ette tuppa ja lönud sellite wasta parrandan, kui sealt ülles sanud, pallunud tedda, agga sest ta ei olle kulanud ja üttelnud: ma tahhan sind kohtomehheks õppetada, ja lonud kolm kord, pähha ja siis jänud temma oimaseks ja ei tea kui paljo on pärrast weel peksnud.
Essite keis temma ka weel üllewel agga nüüid on temma jubba 7 naddalid üsna maas ja wahha on lota et weel hinge jääb. Kahjo mis ta sanud on: müts kindad ja piip, arwata 1 Rubla 10 kop ja sia male on rohtutega ärra kullunud 3 Rub 57 kop. ja pärrast weel rohto todud 75 koppika eest nenda on kokko 5 Rubla 42 kop.
Selle kaeptusse wasta räkis Prido Reinporn ja ütles minna polle temmale ühti muud teinud kui wõtsin kord tedda kõrri alt kinne ja lükkasin tedda letti takka wälja, agga kui sai ännam kulatud ja tunnistuste möda ütles Prido et on tedda ka kord wasto müri lönud, agga eestoas polle temmale keddagi teinud.
Et Prido Renpornil kahtlased juttud ollid, agga mitte üsna selged tunnistust kas temma keik sedda wigga on teinud selle pärrast arwas kohhus tedda ikka süüdlasseks moistis et Prisdo Reinporn peab sedda kahjo 5 Rubla 42 koppik ärra tassuma ja weel 3 Rubla tema on teotamisse eest ühte kokko 8 Rub 42 koppik.
Kohto wanem Jaan Jünter XXX
Kohto kämees Jürri Jürjer XXX
                         Mihkel Reints XXX
Kirjutaja T. Neider &lt;allkiri&gt;
Se rahha on makstud.
