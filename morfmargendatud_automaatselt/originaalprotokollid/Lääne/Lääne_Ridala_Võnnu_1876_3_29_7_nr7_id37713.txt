Kaebas tallitaja, et Herjapea küllas ollid kaks tüdrukud Jula Kassem ja Liiso Noksmann endid meesterahwa riidise pannud ja siis poiste kombel möda külla ja körtsusid hulkunud, ni kui on Tammiko ja Juriko körtsus käinud ja siis weel Toma Jani Wanna ja Uetoa perres käinud, öö aial, kus teised magganud. wasto hakkent kloppinud ja sisse kippunud, ja sigarid ja paberossid tullega suus olnud, ja siis perre püningule kippunud.
Selle peale tunnistas Toma Jani Wannatoa mees Hans Awik: Nemmad tullid meie tuppa meeste rahwa rided ollid ümber, paslikid peas, ja otsisid tüdrukud tagga, agga pea saime neid awwalikuks.
Siis räkis teise Toma Jani perremees Hans Fried: Minna maggasin, siis klopis üks wasto hakkent ja küssis teed, teggin ukse lahti tullid kord tuppa, warssi läksid wälja, ja tahtsid laudile minna tüdrukuid otsima, ma kelasin, naad ei kulanud, siis hakkasin kinni wiskasin uksest wälja ja ei tunnud mitte.
Siis tulli ette Jula Kassem ja räkis meie olleme Noksmaniga nenda nalja teinud, agga Noksman kutsus mind ja olli ennast enne nenda ehhitanud.
Liso Noksman ei olnud kohto ees, et kül olli kässo sanud.
Kohto otsus: Et Jula Kassem ja Liiso Noksmann rumalat nalja on teinud, peawad möllemad seädusse järrel Wata täiskoggo prottokoll, kus moistetud on, et ümber ulkujad pead kunni kolm rubla trahwi alla langema. Surema süüdlane on Liiso Noksmann et ta enne sedda assutas, ta peab 3 Rubla trahwi maksma omma kölbmatta nalja töö melle tulletusseks. Ja Jula Kassem 1 R. 50 kop. Möllemad kahe nädala aiga.
Kohtowannem: H. Awik &lt;allkiri&gt;
Körwasmees: Hans Allik Ado Kask
Kirjotaja: J Reinans &lt;allkiri&gt;
Liiso Noksmann maksis omma trahwi 3 Rubl. 10 Mail 1876.
ka Täidetud.
