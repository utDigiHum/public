Ette astus Jõesust Juhan Markus ja kaebas tema Pühapääw 8mal k.p. õhto Tabra kõrtsu läinud ja seal Kaarel Raagen käest lüüja saanud noaga mittu auku pähe ja Karel Raagen ähwardas ka teisa lüa kui aga nende liggi peasep sest meie olime kõrtsu tuppa jooksnud ja ukse eest kinni pannud.
Astus ette Juhan Põltsam ja tunistas et Kaaring on keige enne kõrtsu sisse tulnud ja kohe jälle wälja läinud; see peale olli Juhan Markus wälja läinud, kohe tagasi tulnud ja pea olli purruks löötud ja näha oli et noaga need haawad tehtud sest müts oli läbbi löötud; meie pidasime sest poolt ukse kinni et naad sisse ei saaks ja tahtsime küllast kohtu meest sinna kutsuma minna. Sell aial naad tulid akna taha ja ähwardasid nugga seina sisse lües, sinna Juhan Prulsein see nugga peab nii ka sinnu sees olema, kas ka kiriku tee peal. Kui nemad aru sait et meie külast abi pidime tooma läksid nad akna takka ärra.
Astus ette Juhan Prunsein ja tunistas sedda sama mis Juhan Põltsam ja weel peale seda et temma kuulnud et Kustas Kaaring käskinud Kaarel Raagend riidu tõsta et teistele wallu anda.
Astus ette Karel Raagen ja wabandas küll essiteks kui aga tunistus meeste wälja ütlemised said ette pantud tunistas üles et Kustas Karing tedda käskind riidu tõsta, aga ta kahatseb ja ei lubba enam mitte teiste kurja nõu järel teha.
Nemad leppisid isse keskis ärra ja Karel Raagen maksis teiste pääwad ja ka mütsi ühtekokko 255 kop.
Kohus moistis et Kaarel Raagen peab Ehmja walla laeka 5 rubla trahwi maksma T.r.s. §1270
Sellepärrast et teiste kurja nõu kuulda wõtnud.
Ehmja laeka 5 rubla trahwi täitmatta
