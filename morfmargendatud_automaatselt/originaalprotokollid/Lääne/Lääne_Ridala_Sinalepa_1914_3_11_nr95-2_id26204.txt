Sell ükstuhat üheksasada neljateistkümmendamal aastal üheteistkümmendamal Märti kuu päewal /1914 11 Märtsi k.p/ Sinalepa walla talu poeg Karel Mihkli p. Krabi 73 aastat wana, kes kohtule isikulikult tuntaw on, palus selle kohtu lepingute raamatusse oma suusõnalist järgmist tõstamenti ehk wiimast seadmist üles wõtta.
1. oma poja Willem Krabile jättan ma pärandada peale enese surma Pargel mõisa järele olewa "Pappa" talu koha No 14 kõige liikuwa warandusega päris omanduseks kõige õigustega, nagu ma ise seda kohta olen Pargel mõisa omanikult olen ostnud, mida maamõetja R. A. Weser on kaardi peale üles märkinud.
2) Minu poeg Willem Krabi peab peale mu surma kümne aasta jooksul mu teiste lastele wälja maksma selges rahas: 1) t. Leenale kolmsada wiiskümmend /350/ rubla; 2) t. Liisale kolmsada wiiskümmend /350/ rubla; 3) p. Karlale ükssada /100/ rubla; 4) p. Aleksandrile ükssada /100/ rubla. Kui ta aga 10 aastat peale mu surma mitte ei ole nimetud summasi ära maksnud mu lastele, siis peab ta neile 4% intressi aastas maksma nimetud summade pealt. Peale seda peab ta Pappa koha peal olewa Eesti maa mõisnikude Kredit Kassa wõla oma peale wõtma, mida ta ilma wastu waidlemata peab maksma. Pappa kohta ei ole mu poja Willem luba mitte wõerastele ära müüa, kui ta seda kohta enam pidada ei taha, siis on mu teiste poegel õigus seda omale osta, aga mitte kõrgemalt, kui mu oma sisse ostu hinnaga see om 2250 rubla eest. Kui aga mu teiste lastest keegi seda kohta omale ei taha osta, siis wõib ta seda wõerastele müüa selle hinnaga, kuda ta heaks arwab.
3) Nõuan oma poja Willemilt, et sell korral kui Jumal mulle elu päiwi annab ja kui ma enam ise ei peaks suutma enesele üles pidamist muretseda , ehk ennast aidata, et ta siis nurisemata mind toidab, katab, harib ja üle pea hea südamelikult hoolt kannab ja wiimaks mind auusaste maha matab.
Seda wiimast seadmist olen ma selge meele mõistuse, oma enese tahtmise järele teinud ja on see pärast lepingute raamatusse kirjutamist mulle ette luetud.
Parandus "2250" õige
Tõstamendi tegija Karel Krabi umbkirilik tema suu sõnalise palwe peale tema eest kirjutas alla: P Krabi
Tunnistajad J Uusland &lt;allkiri&gt; A Reets &lt;allkiri&gt;
Kohtu Presidendi asemik: K. Möll &lt;allkiri&gt;
Liikmed:  J. Triksberg &lt;allkiri&gt; J Piilbus &lt;allkiri&gt;
Kirjutaja G Krabi &lt;allkiri&gt;
11 jaanuaril 1925 aastal olen sellest testamendist ustawaks tunnistatud ärakirja wastu wõtnud.
W. Krabi &lt;allkiri&gt;
