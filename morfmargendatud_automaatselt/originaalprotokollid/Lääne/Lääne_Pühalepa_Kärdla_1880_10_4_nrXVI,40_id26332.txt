Mart Slemm kaebab, et Jürri Tarreste 3 aas. eest tema käest wõtnud ühe raamatu oma kätte lugeda (5 aas. jägu "Juttu toad" ühte poogitud) ja on seda ärakautanud. Raamat maksnud uuelt 2 Rub. 50 Cop. ja nüüd, et ta wana olnud, nõuab tema 1 Rub. 25 Cop. selle eest.
Jüri Tarreste tunnistab, et see nõnda on ja et raamat selle aja sees kaduma läinud kui ta kord siit ära käinud ja lubab selle eest 1 Rub. 25 Cop. M. Slemmile maksta, tulewa kuus.
Seega olid lepitud.
Et muud toimetust polnud sai kohtupääw lõpetud.
