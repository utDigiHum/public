A. lisa 26. Novbr. 1888, punkt 8 pag. 125
Astus ette Orrult Hans Elts ja rääkis wälja: Tema olla Peeter Peeters'i käest heina kuhja peale 6 rubla kätte saanud ja 6 rubla jäänud saamata; wiimaks ostja öölnud ära, et ta seda heina kuhja ei tahta, ta müünud siis selle kuhja 9 rubla ette ära ja saanud 3 rubla kahju,
Peeter Peetres rääkis wälja: Kui Hans Elts heina kuhja peale 6 rubla wötnud, siis jäänud hinna tegemine seks ajaks, kunni tema poolt heina kuhi pidand ülewaadatud saama. Selle küsimine ilma nägemata olnud 12 rbl. aga kui ta seda näinud, siis leidnud ta selle küsimise wäärt mitte olewad ja lubanud tale 8 rubla anda, ehk kui suuremat hinda saama pidi, siis ühe teisele äramüja.
Kohus moistis, et Hans Elts selle heina kuhja peäle kätte saadud rahast 4 rbl. 50 kop. Peeter Peetres'ile kahe nädala ajal peab wäljamaksma.
Hans Elts tellis enesele pitkemat maksmise aega ja lubas Jüripääwaks 1889 see 4 rbl. 50 kop. Peeter Peetres'ile wälja maksta, ja see leppis temaga niikaua aja peäle.
Ärakirja kätte saanud usalduse järele /Oro kohtu asi 96. a. No 29/
17 jaanino1895 a. (Oro kohtu asi No 82)
Hans Lauberg &lt;allkiri&gt;
