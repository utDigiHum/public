Ette sai kutsutud Käblast Nano perremees Jürri Wahtra koera laskmisse pärrast Janii perre sullane Jaan Jünter, Nüüd temma ei salga mitte sedda, waid ütles et temma om sedda koera lasknud. Nüüd kaebas ka Jaan Jünter et Jürri Wahhar on tedda selle pärrast sõimanud et ta temma kora on lasknud.
Et nemmad nüüd mõllemad süallused ollid ei lasknud nemmad mitte sedda kohto moistmisse alla, waid leppisid isse enne kahhekissi kohto ees ärra.
Kohto wannem Jaan Jünter XXX
Abbimehhed Ado Kreis XXX Mart Ie XXX
