11. Astus ette Anu Kork ja rääkis wälja: Aadu Kadanik laenanud 2 aasta eest tema käest 5 rubla raha, sest maksnud see 4 rubla ära ja 1 rubla olles wölga, mis see mitme kordse küsimise peäle ei ole temale wälja mitte maksnud.
Astus ette Aadu Kadanik ja rääkis wälja: Anu Korki wend Hans kauplenud teda enesele kattust tegema, kus ta neli pääwa tööl olnud, aga see tööraha 1 rubla olla sellel ka temale alles maksmata, sellepärast tema seda 1 rubla Anu Korkile ka ei taha maksta, enne kui tema tööraha ka käes on.
Kogukonna kohus moistis, et Aadu Kadanik maksab enese wöla 1 rubla Anu Korkile wälja ja nöuab oma tööraha Hans Korki käest taga, kis teda tööle kauplenud on.
Kohtuwanem Madis Nootmann XXX
Körwasmees Jüri Steenmann XXX
järjemees Jüri Druus XXX
Kirjutaja: A. Krein &lt;allkiri&gt;
