Suuremõisa metsaülema poolest sai 1 Junil s.a. kaibtuse kiri nende üle walla kohtu sisseantud, kes 1. Julist 1884-1. Junini 1885 tema metsawalitsuse jäust, puid warastanud ehk ilma stembeltamata metsast wälja wiinud:
a. Prähla Peter Pähel 3 mända 12'-16'' Nr 70 ärawiinud 17 Januar s.a. maksta a.p. 25 = 75 k. 
Pole ees, pooleli. Tunist. Kibuspu v.m. järel.
v.a. alt Tunnistus Kibuspu ütles, et Nigolas Wina on tale üles annud, et Ptr. Pähel need puud on warastanud.
b. Sarwe Andrus Brenner 3 palki 21'-5'' ilmastembeltamata Nr 18 wiinud talumaea ehitamiseks 28 Januar s.a. maksta a.p. 25 k. 75 kop. Andr. Brenner ütles, et waht Mats Männama luband, et wõib puud peale panna ja wäljawia. Numertud, aga tembeldud põle olnud.
Wahid Jõe ja Espenberg ütlesid, et Brenner üle nende keelu tembeldamata ärawiinud.
A. Brener lubas maksta 75 kop.
c. Juhan Küla 4 palki 21'-5'' u 7'' ilma stembeltamata metsatüki Nr 18 ärawiinud, talo m.e. - maksta R.1 ja 1 puu 21'-7'' palgipakuks saagind m.r.1.
Ta ütleb, et Mats Männama käskind, et wõib wäljawia, mis Nrdud aga mitte stembeltud, ja olle ka Espenberg lubanud, et wõis palgi paku, palgist saagida, kes ka ütelnud, kes 2e päewa sees põle palgid wälja wiinud, see ostku,- ja palgist põle mitte luband paku saagida. Ta ei maksa. Tunistus Jõe, Espenberg.
d. Juhan Proos Sarwelt 1 puu 15'-6'' pakudeks saagind 22 Jan.s.a maksta.
Põle ees olnd, pooleli. Tunist. Jõe, Espenberg.
e. Juhan Sepp Hellamalt 1 suur metsakuiwa puu, mänd Nr 18 koolipuuks saagind Januaris, maksta: (1 R)= 25 K. Tunnistuseks Jõe - metsawaht.
f. Peet Paiju Hellamalt 6 palki ilmastembeltamata Nr 18 wäljatoonud 24 Januar. Tunnistus Kibuspu. Paiju ütles, et ta on 3 metsawahili teada annud et stembeltamata puud wälja wiib.- Espenberg ütles ka, et tal sest süid ei ole.- sest et seda teada anud, et wälja wiib, süist klaar.
g. Juhan Panel Kukkast 5 palki petusega talumaea ehitamiseks wiinud 28 Januar. Tunist. Espenberg ütleb, et ta luband Keremale wija, aga Hagaste wiinud lühemad puud. Pannel aeas wastu, ilma õige põhjuseta. Tal maksta 1 r. 25 k.
Pooleli.
h. Peet Kalju poeg Kerdelt 1 koorm mändi 10'-3-4'' Kanapeksust wiinud 1 Febr. Tal maksta R: 1. Tunnistus Jüri Kaibald ka puudus.
Pooleli.
i. Peet Wöö Palukülast, 1 männa Hinsult Febr. wiinud tükkis okstega. 15''- u. 17'' Tunnistus wahimees Andrus Silm, maksta tal 1 r. 50 k.
Pooleli.
j. Prido Tau Heltermast Nõmba metsast 10 mända wiinud 9 tüki 6'' paks 1 -,,- 9'' -,,-. Märtsis. Tau ütles, et metsawaht tuli puud käskind wõtta.- Metsw. Jäger, et ta juhatand õige platsi, aga ta on undrehti platsi pealt, ja mitte tuli puid, waid palka wõtnud. Ta 5 R.50 kop. maksta, mis ka klaaris.
k. Peter Liwa Oiadelt, on Kallaste metsast 4 mända 10'-4'' nõustand 8 April. Kallaste metsawahi ülesandmise järel,- maksta 1 Rbl., mis ka maksis
l. Peter Remelkoor Wahterpalt ühe uue elumaea ehitand, kust on palgid saanud. Remelkoor kostis, et uusi palka põle. 3 on oma wenna käest saanud - ja kui ehitand, on Bäris Herra käest ka puud saanud, kui maead ehitand, mis üle jäänd. Metsaherra isi waatab maea üle.
m. Peet Remelkoor, Arukülast, on omale uued elumaea ehitanud, kust ehk kelle käest on ta palgid saanud. P. Remelkoor kostis, et ta on kewadel 1884 Kõrgessare mehe, Tomas Napp käest 30 wana palki 3 rubla ette ostnud! - Need on üsna wanad mäda puud olnud,- mis ta isi tahunud 6 tolli peale, nii et need nüid nagu uued puud wäljanäewad.
