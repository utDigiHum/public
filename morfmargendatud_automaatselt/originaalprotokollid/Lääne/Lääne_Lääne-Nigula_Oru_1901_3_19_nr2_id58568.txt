Üks tuhat üheksa sada esimesel aastal Märtsi kuu üheksateistkümnemal päewal ilmus Oro walla kohtusse Oro walla Talumees ja kohtule näulikult tuntud Mihkel Jüri p. Kelnik ja palus selle kohtu akti raamatusse järgmist tema wiimist seadmist sissekirjutada:
"peale minu surma jättan pärandada oma Oro mõisa järelt olewa mõisast lahutatud "Ado" pool talukohta oma noorema pojale Mihkelile."
See wiimne seadmine sai peale akti raamatusse sissekirjutamist wiimse seaduse tegijale Mihkel Jüri p. Kelnikule ja selle juures olewatele tunistajatele Kustaw Kallusmannile ja Mart Saarmannile, kes töendawad et temal Kelnikul, tõesti wiimse seadmises nimetatud warandus on olemas ja et ta praegu selge mõistuse juures on, etteloetud.
Wiimse seaduse tegija: Mihkel Kelnik XXX Umbkirilik
President: A Kaaw &lt;allkiri&gt;
Tunnistajad: Kustas Kallasman &lt;allkiri&gt; M. Saarmann &lt;allkiri&gt;
Kohut president: A Kaaw &lt;allkiri&gt;
Kohtu liige: J. Lindal &lt;allkiri&gt;
Kohtu liige J Kleman &lt;allkiri&gt;
Kirjutaja Wessart &lt;allkiri&gt;
Ära Kiri Mihkel Kelnikule wälja antud 13mal Aprillil 1909 a.
Mihkel Kelnik &lt;allkiri&gt;
