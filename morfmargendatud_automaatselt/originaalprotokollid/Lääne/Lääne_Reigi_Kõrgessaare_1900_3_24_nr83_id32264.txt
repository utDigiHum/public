1900 a. Paastu kuu 16 päewal Kõrgesaare mõisa omanik Grahw Ewald Ewaldi p. Ungern-Sternberg ja Kõrgesaare walla talupoeg Kustaw Peetri p. Mänd, kes mõlemad walla kohtule isiklikult tuntud on palusiwad selle kohta lepingute raamatusse kirjutada nendest sisseantud järgmist kirjaliku lepingut:" Kontraht "Tenasel päewal on Krahwiherra E. Ungern-Sterbnergi kui müüja ja talupoja Gustaw Mänd Mardi-anso perres Kõrgesaare waldas, kui ostja wahel järgmine müümine ja ostmine ära rägitud ja kinnitatud.
§1
Krahwi herra E. Ungern-Sternberg müüb nimetud Gustaw Mänd kätte 1872l aastal Suuresa samas männipuust ehitatud shoner "Prestö suuruses 30 Lasti, ilma laewa inwentarita.
§2. Laewa wastuwõtmisega peab ostja Gustaw Mänd kinnitatud summa (300) kolmsada rubla ära maksmma.
§3. Kõik selle müümise ja ostmise juures ettetulewad maksud etc. etc peab ostja üsna üksi tasuma. Et mmeie sedda kohtrahti kõige kindlamalt oleme teinud, olema meie ühtlasi tarwiliste tunnismestega seda kontrahti omade kääkirjadega kinnitanud seda wiisi et üheltki poolt mitte miskil wiisil sedda kohtrahti ei saa ühe ehk teise külje poole muuta, egga tagasi ajada.
Grahw UngernSternberg
Kustaw Mänd
Tunnistajad Maddis Wolens
Karel Klemmmm.
Kontrahtsai pärast lepingutte raamatusse kirjutamist pooltele ette loetud ja nendest allakirjutatud, mis meie sellega õigeks tunnistame.
