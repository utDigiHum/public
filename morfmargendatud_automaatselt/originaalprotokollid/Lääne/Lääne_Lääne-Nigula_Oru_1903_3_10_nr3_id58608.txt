Üks tuhat üheksa sada kolmandamal aastal Märtsi kuu kümnemal päewal ilmus Oro walla Kohtusse Oro walla liige, kes Kärbla Kogukonna Orjuse talus eob Mari Ado tütar Wiilmann (sünd. Ladin/ ja palus oma suusõnalist wiimast seadmist Walla Kohtu lepingute raamatusse sisse kanda: Mina luban om päralt olewa Orjuse talu koha kõige muu liikuwa warandusega oma wanema tütre Anna Metsale (sünd. Wiilmann) ja teen Kohuseks, et pärast minu surma, kui Anna Mets Orjuse talu koha on omandanud, minu teiste lastele järgmiselt wälja maksab:Mari Wiilmannile			200 rbl.		Leena Wiilmannile			50 rbl.		Liisa Wiilmannile			50 rbl.		ja Miili Wiilmannile			50 rbl.		
peale selle peab Anna oma õed, minu noormad lapsed oma juures ülewal pidama ja inimeseks kaswatama, see on täisealiseks.
Oma päranduse osa wõiwad pärijad Mari, Leena, Liisi ja Miili siis nõudma hakata, kui nemad täieealiseks on saanud. Ka on Anna kohustatud õdetele, kui nad mehele peaksiwad saama pulmad tegema.
Peale aktiraamatusse sisse kandmist sai see wiimane seadus seaduse tegijale Mari Wiilmannile ja tunnistajatele Mihkel Metsale (ette loetud) ja Peeter Undole ette loetud, kes tunnistawad et Mari Wiilmannil tõesti selle kohane warandus on olemas ja et tema seda wiimast seadmist terwemõistuse juures olles on teinud.
Wiimase seaduse tegija: Mari Wiilmann XXX on umbkirilik
President: J. Spuul &lt;allkiri&gt;
Mihkel Mets &lt;allkiri&gt; Peter Undu &lt;allkiri&gt;
President: J. Spuul &lt;allkiri&gt;
Kohtu liikmed: H. Silwer &lt;allkiri&gt; P Kuur &lt;allkiri&gt; M Eringas &lt;allkiri&gt;
Kirjutaja Joh. Tilling &lt;allkiri&gt;
