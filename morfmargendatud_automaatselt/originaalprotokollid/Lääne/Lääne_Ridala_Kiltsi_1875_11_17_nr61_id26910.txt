Ette sai kohto wahi mehe kauba kinnitamisse pärrast Pali Jaan Sõer kutsutud, (kus jures ka kolmest walast talitajad ollid.), ja tegid kaupa:
Et sel päwal kui kohtus wahhi meest tarwis on, saab kässoga nädali sees kutsutud ning wahi mees toob 10 kimpu witsu kohe kasas kohtu kel 12 louna on seal kas tarwis lähheb ehk mitte saab palgaks 50 kop päwa kohta isse weel kui hopisi lööb: saab 1 kop hopi eest.
Jaan Sõer lubab 1 aast seda kaupa wastu wõtta.
Wahi mehe kaup 1 aasta kohtus.
