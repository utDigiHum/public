Kohto korra pärrast ollid, Parrila, Kebla ja Riddali koggokonna maiase kokko tulnud kohto mehhed
Ette astus, Parrila moisa Rent herra J. Tuulman ja kaewas, uhhe Kidewa walla mehhe Jaan Tambargi peale, et on tenistus kauba teinud suiliseks, ja piddi palka sama 65 Rubla, ja kätte sai 5 Rubla karahha ja Jürripäwast piddi tenistus akkama, ja minna otasin näddal pärrast Jürripawa ja tulnud tedda ja ma watsin ue tema assemele jaa nüüd ma ennam tedda ei tarwita waid nouan omma ka rahha seadust möda.
Selle kaeptusse peale sai Kidewast Jaan Tamparg ette kutsutud ja temma rakis selle wastu: ma jäin haigeks ja ei olnud woimalik tulla ja nüüd ollen parrem ja hakkaks tenima agga moisa herra ei tahha ennam taggasi, ja kui haige ollin siis saatsin käsku et ei sa tenistusse tulla.
Kohhus arwas Tallorahwa seadusse §448 järrele et Jaan Tambärg maksab oma 5 Rubla ka rahha tagasi agga muud trahwi ei maksa tema ühti ennam.
Kohto wannem Jaan Jünter XXX
Kohto kämehhed Jürri Jürjer XXX Juhhan Krabbi XXX
Kirjutaja T. Neider &lt;allkiri&gt;
on kärahha tagasi maksnud
