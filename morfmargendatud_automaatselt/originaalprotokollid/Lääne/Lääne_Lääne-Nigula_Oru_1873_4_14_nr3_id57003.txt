2) Astus ette Juhhan Waarmann ja ütles, kui Hans Berg 9mal Märzil s.a. ennese kuhja labba haggud on tonud, siis on ta selsammal korral ka need nimmetud 13 puud raiunud; muist enne, kui ta ennese kuhja labba on peäle pannud ja muist pärrast, kui ta jubba kuhja labbalt koio pole on tulnud, sedda tunnistawad temma jäljed, sest et muid jälgi mitte suggugi nende raiutud pude jures ei olle käinud.
Astus ette tallitaja Jaan Kork ja andis tunnistusseks 3 laastu, mis ta metsast kändude jurest om wõtnud ja üks pool jalga pitk kassepu tuk, mis ta Hans Bergi öuest on wõtnud ja nende laastude ja selle pu tükki peal ollid ühhe laiused kaks krammi, mis selgeste näitab, et Hans Bergi kirwel on kaks hammast sees olnud, kellega need puud on raiutud.
Astus ette Hans Berg ja ütles, kui ta 9mal Märzil s.a ennese koormaga metsast on tulnud, siis on Jaan Olmann ja Jürri Pahlmann temmast möda tulnud ja need on näinud, mis taal peäl on olnud.
Et Jaan Olmann ja Jürri Pahlmann mitte kohto ees ei olnud, siis andsid kohto körwasmees Hans Olmanna, tallitaja Jaan Kork, ja Juhhan Waarmann nende eest sedda tunnistust, mis naad nende wasto on tallitaja öues tunnistanud:
Jaan Olmann olla tunnistanud, et ta ei olla mitte siis tähhele pannud, kui ta Hans Bergist möda on tulnud, mis taal peäl on olnud; ja Jürri Pahlmann olla tunnistanud, kui ta Hans Bergist olla möda tulnud, siis olla Hans Bergi koorma otsas olnud 4 kasse puud, mus jubba sui raiutud on sanud ja üks tores kask; on taal ehk koorma al ka olnud toorid puid, neid ei olla ta mitte tähhele pannud
Kohhus moistis, et Hans Berg peab 10 kop. höbb. igga puu eest, mis ta warrastanud, walla laekas heaks maksma.
Hans Berg ei olnud sellega rahhul waid noudis protokolli wälja ja läks Hakenrichtre herra jure.
Kohtowannem Jaan Willik
Kohto Körwamehhed Hans Neuwald Hans Olmann
Kirjotaja Abr. Krein
