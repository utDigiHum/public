Ette astus A. Jakobsohn, ülles antud Tamma metsa wargust õiendama Kohto ees wanna metsa wahti Jaan Paesülda ussutama, kas tema isse polle neid jurind, ja müünd re jallasteks, mis Jaan Sõer pärris Härrale kaewand 6 jallast on isse Reinut Karmiga neid juritud kohhad läbi näind, pärrast Jürri Koido käest  jutto kuulnd, et Jurna Jaan neid Jurind; Awiko Mihkel Karm kes Jürri Koido wasto räkind.
Jürri Koit tunnistas, et:"Mihkel Karm räkis minnule jutto aiades, rejallaste puduse pärrast, nimmetas ennast laisaks ja holetumaks: et talwe tulleb peale ja ei olle jallasid: Jallusiis saaks kui neid nouda, "wa Jurna Jaan (wana issa( käis karjas süggise ja jurind Tammalt.
Mihkel Karm (Awiko) aias tagasi, et temma polle mitte nisugust jutto räkind; waidlessid kohto kuuldes selle asja pärast.
Lahti saand metsa waht Jaan Paesüld, olli kohto ees ja ütles et temma polle mitte enni neid ladwu ja jurind kohtasi näind kui läbbi waatmine pärris Härrast tallitaja seal käind.
Endine Tamma rentnik Reinut Karm olli kohto ees, ja ütles et temal luba olnd oksi raiuda ja leidsin need kohad.
Jaan Sõer olli kohto ees ja ütles, et R. Karm näitas mulle neid kohtasi ja ütles weel "tulle wata kuida neid Jaan jurib."
Kohto mehhed jätwad asja polele, selle pärrast et ei olle täit tunnistust kuni kaebajad selgid tunnistust ükskord ette towad, et warast näha on; holeto metsa waht on jubba trahwitud, et ametist lahti on lastud.
Pudulisse tunistus pärrast polel.
