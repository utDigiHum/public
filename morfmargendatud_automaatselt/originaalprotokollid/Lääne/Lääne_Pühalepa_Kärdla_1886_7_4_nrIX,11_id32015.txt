Koos olid, Tallitaja H. Pisa, abimehed: W. Mölder ja W. Poola, kohtupääwa pidamas.
Harju Wenelane Peeter Sergejew astus ette ja kaebab et temal juba kuu aega mõne mehe käest raha saada on mis naad tema käest kaupa ostnud.
Peeter Leiger			on temale wõlga			1 rub.			81 kop.		Gustav Eller			-,,- -,,-			1 -,,-			22 -,,-		Madis Rehepap			-,,- -,,-			3 -,,-			-		Karl Sinik			-,,- -,,-			1 -,,-			80 -,,-		Johannes Püssim			-,,- -,,-			-			80 -,,-		Priido Huuk			-,,- -,,-			2 -,,-			40 -,,-		Gustav Kiil			-,,- -,,-			2 -,,-			70 -,,-		
Peeter Leiger jääwad Peeter Sergejewiga waidlema. Peeter Leiger ütleb et temal see wõlg Peeter Sergejewile maksta on, aga Peeter Sergejew on ka temaga wõlgu: 2 rub. 65 kop.
Kui ta enne minewal talwel tema juures korteris olnud, on tall wõlga jäänud 4 öö ja pääwa korteri ja kasti raha a 40 kop. Summa 1 rub. 60 kop.
3 särki ja 3 paari aluspuksa mis ta naene õmmelnud: 1 -,,- 5 kop
See teeb Summa: 2 rub. 65 kop.
Peeter Sergejew üteb et tema ta juures korteris olnud, ka lasknud pesu ommelda, aga kõik on tema ära maksnud.
Naad jääwad waidlema, kumbki jääb kindlaks oma sanade juure. Tunnistust ei ole kummagi poolt saada.
Gustav Eller, Madis Rehepap, Karl Sinik ja Johannes Püssim tunnistawad, et neil see wõlg P. Sergejewiga on, ja lubawad ära tasuda kui kuu täis saab, Tallitaja kätte, et P. Sergejew ise Wenemaale läheb. Priidu Huuk tunistab oma wõla tõeks, lubab äratasuda kui tööd saab, praegu on ta ilma tööta. Gustav Kiil lubab oma wõla äratasuda niipea kui ta jälle tööd saab. Praegu ei ole temal teenistust.
Sellega leppis ka Peter Sergejew.
Seega oli ka kohtupääw lõpetud.
