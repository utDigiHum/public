Kohtu ette astus Haeskast Jaan Mühl ja kaibas et temal on öösi 24/25 Juli kp. sa. aida uks lahti muugitud ja sealt kirstust wiitud, ja arwab et Hans Birk on seda wargust teind.1 pakko saag 1 R. 25 kop90 küünart linast räetik 18 R.1 poe palito 9 R.2 siidi rättikud 6 R. 20 kop1 pool siidi rättik 1 R.
1 sitsi rättik 18 kop1 willane rättik 90 kop1 walge lina 1 R. 50 kop1 kott 1 R.Wekslid 300 R.2 hõbe Rubla naiste kaela raha 2 R.
muud raha 5 R. 50 kop2 mütsi paela pool siid 5 R. 82 kop2 hõbe sõrmust 75 kop10 naela wõid 'a nael 25 kop 2 R. 50 kop.
15 September sai Hans Birk ette kutsutud ja ta tunnistas et on seda kül rumalusega teind ja kahetseb seda wäga, ja on ka need asjad keik puhtaks kätte annud mis weel ei ole leitud olnd, ja selle kahju tasumiseks on Jaan Mühl isi leppind.
Ja siis kaibas Jaan Mühl et tal on warastud 24 Febr. s.a. obuse riisto ullo alt1 nahk rangid 5 Rubla 50 kop1 telka 5 R1 Kleid 3 R 25 kop1 Waljad 2 R.
Hans Birk tunnistas seda wargust üles et Jüri Nõm on tal seks nõu annud et too Jaan Mühl riistad sealt ära ma saadan naad üle mere, ehk wiin Pernusse, ja oli siis need riistad Jüri Nõm juure wiind öösi.
Jaan Mühl andis kohtus teada et tema oli kohe pärast wargust Jüri Nõmmil teada annud, nii sammoti ka nende riistade märgid.
Jüri Nõm tunnistas et tema on Hans Birgil nõu annud neid riistu tua ja oli neid Hanso käest wasto wõttnd pärast teada andmist, aga wabandab ennast sellega et ta ei ole neid tunnud, selle riistade rikkumise eest on ta saan Mühliga ka ära leppind, ja ka tasund.
Kohus mõistis: Et see asi ei seisa enam kog. kohtu mõistmise al. kog. koht s. § 10.
(Hakenriht juure läkkitud.)
