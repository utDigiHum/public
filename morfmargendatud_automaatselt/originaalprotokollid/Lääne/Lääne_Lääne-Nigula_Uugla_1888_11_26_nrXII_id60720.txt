6. Astus ette Taeblast Peeter Peetres ja rääkis wälja: Jaan Blok wötnud 1878 a. Nääripääwast Jüripääwani tema käest 4 rubla ette kaupa ja ka raha.
7. Mihkel Tähe ostnud 1882 a. tema käest 6 tündert kartuhwlid a' 225 kop. kokku 13 rubla 50 kop. ja jookide wölga teinud 235 kop. ette ja 1886 a. see wötnud Küla laadal tema heina kottist 1 pudel wiina, mis 40 kop. maksnud, ilma tema lubata ära ja sest wölast mitte ühtegi seie maale weel ära maksnud ei olla.
8. Hans Elts Orro wallast wötnud 1883 a. tema käest 6 rubla raha ühe müüdawa heina kuhja peale wälja, aga see heina kuhi ei olla osta sündinud, see müünud siis selle kuhja ühe teisele ja pidand tema käest saadud raha tagasimaksma, aga ei olla rohkem sest seie maale mitte maksnud, kui ise teinud 2 pääwa ja ta naene teinud ühe pääwa heina töö juure, kelle ette 1 rbl. 50 kop. tasutud saanud ja 4 rbl. 50 kop. olla seiemaale weel selle käest tagasi saamata.
9. Jaan Kruus wötnud 1883 a. tema käest 1 tünder kartuhwlid, pidand selle ette 3 heinatööpääwa tegema, teinud aga ühe pääwa ära ja 2 pääwa jätnud tegemata, kelle ette ta selle käest nüüd ühe rubla saada nöudis.
10. Hans Hubert teinud 1883 a. pastlide, soola ja tubaka ostmisega ja jookidega tema juure 10 rubla wölga, sest maksnud 1887 a 6 rubla ära ja 4 rubla jätnud siis weel maksmata, ja ei olla seie maale seda mitte äramaksnud.
11. Jüri Luubert lanenanud 1883 a. tema käest 2 rubla raha ja ei olla seda seie maale mitte ära maksnud.
12. Andrus Alwin lanenanud 1884 a. tema käest 5 rbl. raha Orro möisast rukkit osta ja ei olla seiemaale seda temale mitte tagasimaksnud.
13. Willem Koha Widruka wallast teinud 1883 a. temaga pastlide ostmise ja jookidega 2 rubla 20 kop. wölga ja ei olla seie maale seda mitte äramaksnud.
Walla kohus moistis, et need wölgnikkud teisel kohtu korral peawad ette tulema endid öiendama.
Kohtuwanem Peeter Weidebaum XXX
Körwasmees Jüri Ulm &lt;allkiri&gt;
Körwasmees Hans Priidam XXX
Kirjutaja A. Krein &lt;allkiri&gt;
Ärakirja wastu wõtnud usalduse järele 17 Jaaninp 95 a.
Hans Lauberg &lt;allkiri&gt;
