Riddala Kihhelkonna Kohtu otsus selle Mäewalla koggokonna kohti Protokolli ülle 1874ma aastast sel 9al Detsember 1874.
Kohhus on ennamesti igga näddalas koos olnud, agga ei olle mitte rohkem kui kaks asjas kohto ette todud; need on oigel wisil kulatud ja moistetud ja moistmine täidetud sanud; agga No 27 jures olleks piddanud seädus, Estima talorahwa seäduse §1221, ka nimmetud sama. Protokol on puhtaste ja selgeste kirjutud.
Truta Prosowits ja Juhan Reets ei olle mitte ommad passid Kihhelkonna Kohtus lasknud kinnitada ja ei olle mitte seätud rahha, Truta 1 rub. 50 kop. ja Juhan 2 rubla maksnud.
Koggokonna kohhus peab neid ette kutsuma, sedda nimetud rahha laega jäuks korjama ja neid omma süi möda trahwima.
Co Middendorfh
Riddala Kihhelkonna Kohto peawannem
