Riddala Kihhelkonna Kohtu otsus selle Kiltsi, Assokülla ja Hapsalu kirriko walla koggokonna kohtu Protokolli ülle 1874ma aastast, sel 9al Detsember 1874.
Protokoll on puhtaste ja selgeste kirjotud ja Kohtu järrel kulamine ja moistmene on ka kolwalik ja selle aasta moistmised on täidetud.
Agga No 4 ja 5al saab se, kelle peale kaebtus tõstetud on, jubba kulamise ajal, enne kui assi selge on, monikord warras nimetud, mis enne süi moistmest ei olle mitte seäduse polest lubatud.
Ja No 22al on öldud loma kinni wõtmisse jures, et se kis kinni wottab woib kahjo tasumist ja trahwi sama, agga Estima talurahwa seäduse §1121 ütleb selgesti, et trahw saab laekase maksetud.
Agga 1873ma aastast kaks moistmised ei olle weel mitte täidetud ja peab kohhus neid wiwitamatta täitma, nimmelt Kiltsi laeka jäuks korjama No 8 möda Juhan Munki käst 1 rubel ja No 68 ja 72 möda Juhan Krüneri käst 3 rubla. Koggokonna Kohtu seäduse möda §15 möda need süallused ei woinudki suremad kohhud nouda, ja kui nemmad olleksid tahtnud ja woinud, siisgi ei olle mitte §21 möda teinud, ja selle pärrast peab §23 möfa Koggokonna Kohhus warmalt omma moistmist täitma.
Lukiddendorff &lt;allkiri&gt;
Kihhelkonna Kohtu peawannem.
