Kohto ette kaewab Mäe walla mees Ado Tenus, et Kidewa Mõisa talli pois Rein Wiinberg on 12küm hobbust nende Odra põllale lasknud minna ja kolm kuhhilast ärra laotanud ja Ö otsa sönud, kahjo on sadud Tallitajast läbbi wadetud ja arwatud kaks tündert odrad wäärt ollewad. Agga Ado Tenus ei pärri muud, kui sedda mis seadusse ramatus seisab 35. kop hobbuse pealt se on kokko 4 Rub. 20 kop.
Sai Rein Wiinberg ette kutsutud ja küssitud, kas se tõssi ollewad ja ta tunnistas sedda nenda ollewad ja lubbab se 4 Rub 20 kop. tännasel kohto päwal Ado Tenusel wälja maksa.
Kohto wan. Mihkel Sarijs XXX
Kae mehhed Willem Otsberg XXX Willem Otsberg XXX
Kirjotaja: A J Martensohn &lt;allkiri&gt;
