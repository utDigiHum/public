Kohto korra pärrast ollid Parrila, Kebla ja Riddali kirriko walla koggokonna maias kohtomehed koos.
Ette astus Parrila moisa Herra Woldemat Hunius ja laskis prottokolli ülles wõtta nenda: Parrilast Soltad Maddis Pruuli ollen ma moisa rehhe jurest warguse pealt kinni wõtnud 30mal Januari ku päwa õhtul kus temma essite müri laotas ja siis sisse läks ja toa ukse laua katki loi ja siis kaks loma nahka walja tõi ja siis wälja tulles rehhe wärrawa eest, patent Tabbalukku katki murdis ja siis wõtsin ma tedda kinni ja tõin kohhe tedda kohto maiase kinni. Sedda lohkumist ollen minna isse pealt watanud, et ma isse tedda walwasin selle peale et mul sedda teada anti et temma on lubanud nelja päwa eest, need nahhad ärra tua.
Selle kaeptusse peale sai Soltat Maddis Pruul ette todud ja temma ei ütlenud ühhegi asja peale wasto ja tunnistas keik nenda olnud, kuddas on kaewatud, ja tabba olli temma rehhe massina wõtmega katki murdnud.
Tallitaja Jürri Jürjer on ka sedda lõhkumist ülle watanud ja on keik nenda wisi lõhhutud olnud kui moisa herra sedda on kaewanud.
Nüüd ei woinud koggokonna kohhus mitte selle ülle otsust tehha et süi paljo surem on kui koggokonna kohtu woimus ullatab.
Kohto wannem Ado Kreis XXX
Kohto kämehhed Kristow Luntsi XXX 
Kohto kämehhe assemel Jürri Klaos XXX
on surema kohto ette läinud
Kirjutaja T. Neider &lt;allkiri&gt;
