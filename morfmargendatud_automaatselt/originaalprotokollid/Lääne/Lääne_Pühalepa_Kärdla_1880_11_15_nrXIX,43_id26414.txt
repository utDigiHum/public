Koos olid Tallitaja: P. Naaber, abimehed: S. Läck ja W. Kutser seatud kohtupääwa pidamas.
Kõrtsmik G. Liik kaebab, et Pühapääw sel 2 Novm õhtul Peet Tasane ja Peet Sepp kõrtsus teine teisega riidu läinud. Tema läinud wahele neid keelma, siis on P. Tasane kiwi maast wõtnud ja seega teda ähwardanud. Mõlemad olnud isi joobnud.
Kaebdusalused P. Tasane ja P. Sepp astusid ette ja sai see asi neil ette pandud, rääkisid wälja, et nemad esite joobnud peast sönutlema hakkanud ja pärast ka tülitsema ja et siis kõrtsmik neid äralahutanud; aga seda kiwiga ähwardamist ei mäleta nemad.
Kohus tegi otsust: Et P. Tasane maksab 1 Rub. hõb. ja P. Sepp 50 Cop. trahwi.
Et muud toimetust polnud sai kohtupääw lõppetud.
