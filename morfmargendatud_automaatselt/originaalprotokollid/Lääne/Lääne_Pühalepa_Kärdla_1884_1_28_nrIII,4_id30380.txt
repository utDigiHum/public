Koos olid, Tallitaja: A. Espak, abimehed: P. Püss ja W. Uusmann seatud kohtupääwa pidamas.
Mis Protk. I Ja 2 all, 7m Januaril poolele jäi sai ette woetud ja selle asja üle otsus tehtud.
Et tarm nüüd selle luubi hoopis on ära wiinud said Madis ja Johannes Kõmmus ettekutstud ja neilt küsitud: Miks naad seda luupi parem ei hoidnud? Naad wastasid et nemad on luubi wesi aja külge nööriga kinni sidunud ja nemad ei sa aru kuida seda tarm on lahti kiskunud.
Kohus tegi otsust: et Madis ja Johannes Kõmmus luubi omanikudele Willem ja Priido Kutser ja Hans Pachtmannile luubi hinda 5 rub. hõb. peawad wäljamaksma.
