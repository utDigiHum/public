A. Lisa 4. Januar 1884 punkt 2 pag. 63.
Astus ette Rägult Jaan Maandi ja rääkis wälja: Suistema körtsus Jüri Kork hüüdnud teda mittu kord "Maandi Jaan", wiskanud enese kasuka seljast maha ja kippunud ta kallale, küsinud ta käest wiina ja tahtnud temaga leppida.
Jaan Maandi wõttis enese kaebtust tagasi ja jättis kohtu käimise järele.
Kohtuwanem Jüri Silwer XXX
Körwasmees Juri Ulm &lt;allkiri&gt;
Körwasmees Rein Kiisberk XXX
Kirjutaja A. Krein &lt;allkiri&gt;
