Hellamal 5. November 1884
Sai pealkordlik kohtupäew ärapeetud, kus koos olid: Peakohtumees A. Koppel
kõrwasm. J. Wilo
-,,- J. Leht
Sai ettewõetud, mis Nr 87 al pooleli jäi, kus M. Espenberg (ema Ingel) ütles, et 31. Augustil oma riided senna aita pannud ja Oktobri kuu esiotsa leidnud, et need waeaka olnud. Talla nahk on suil Jaanipääwa nädalis aga rie Mihklipäewa nädalis (ehk nädal selle ette) ärawarastatud. Ridega seltsis põle mitte nahk warastud. 4 küünart willast riet on puudu.
Anton Espenberg tunnistas ka, et tema saapa talla nahk on suil Jaanipää nädalis warastud saanud, aga mitte Mihklipää nädali ette.
(Suuremõisa walitseja)
Johan Huuk ja Peet Kaew ütlesid, et nad on 3mekesi seltsis Kust. Hanekat ka, riide ja tallanaha ühekorraga nagu Nr 87 al.) ärawarastanud, ning 1x on aga wargas käind ja warastud asju mäelauda taha rugi naabrisse pannud, rugileikuse lõpul.
Kust. Hanekat kostis nagu enne, et ta põle mitte nende seltsis olnud.
Suuremõisa walitseja ütles, et rugist ei ole enam 31 August s.a. seal põllal olnud (kus wargad ütlewad ride jne. panud naabrisse olewad) waid kõik äraweetud olnud.
Sedasamma tunnistas kubjas Peet Kokla ka, et rugit ei ole mitte enam Pärdi (24/8) nädalis Suurepõllal olnud.
Et se asi suurem, kui wallakohus klaarida wõib, saab (kõrgemad) Hakenrihtri Herrat palutud neid ette wõtta ja klaarida.- Ingel Espenberg, (kar) kes Suuremõisa karjaperenaene ja Anton ta poeg paluwad, et neid ei saaks ettekutsutud, 1sel oma mõisa toimetamise pärast ei oleks aega ja Anton on wigane, wiletsa terwisega; aga Mih. Espenberg, nende ette ka kosta, mis eespool kirja son.
