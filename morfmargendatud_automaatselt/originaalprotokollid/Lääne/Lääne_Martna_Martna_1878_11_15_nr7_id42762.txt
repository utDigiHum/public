Astus ette Jaan Malmann ja kaewas, Lähtro mehhed peksid mind Raia körtsust tulles tepeal, meie tullime Jaan Holbergiga kahhekesti koio pole ja Lähtro mehhed tullid järrele kolm meest, kui nemmad mind peksid siis Jaan Holberg ütles minna tunnen teid kül kes teie ollete, siis läksid körtso pole taggasi agga pörasid ümber, ja ütlesid lähme anname temmale ka, ta tunneb meid ommeti, agga Jaan jooksis metsa ei sanud kätte, siis peksid mind weel ja ütlesid anname temmale weel kas ta jääb seia paika, siis läksid körtso jure taggasi körtsmik laskis mind sisse ja peksis Lähtro mehhed puga tagasi.
Sai Jaan Holberg ette kutsutud ja küssitud, kas sa tundsid need mehhed? meie ei tunnud neid mehhi kumbki, agga körtsmik Jürri Buhkwelt ütles minna tunnen need mehhed ja woin iggal pool teiega kohtus tulla need on Hans Jsan, Mart Allatu ja Jaan Tomis.
Said need mehhed kohto ette kutsutud ja küssitud kas teie ollete sedda Jaan Malmanni peksnud? meie ei olle enne tüllis olnud, miks pärrast meie tedda peksime.
Sai kortsmik Jürri Buhwelt ette kutsutud ja küssitud, kas sinna näggid kui se Jaan Malmann peksa sai? wastus minna ei olle mitte peksmist näinud, agga nende meeste nimmed ollen ma ülles annud kes meie Kötta Jaan Malmanni järrel tullid, ja tahtsid wina sada minna peksin neid taggasi ja ei lasknud sisse tulla.
Kohhus ei woinud nende asja mitte selletada, et need muist on Krono mehheks minnemas, sedda Jaan Tomis olleks üksi olnud trahwi moista, et temma walla kohto ees ütles Jaan Malmannil kül ma tunnes so teine kord, et sa minno peale kohto ees kaewad anname sedda tulli asja hakendrihtre kohto holeks, et selle kortso läbi allati rio tullid ette tullewad.
Kohtowannem: Mart Witin XXX
Käemehhed: Jaan Rehkalt XXX
Käemehhed: Jaan Sibolt XXX
Kirjutaja G Pleimann &lt;allkiri&gt;
Protokol wälja antud
