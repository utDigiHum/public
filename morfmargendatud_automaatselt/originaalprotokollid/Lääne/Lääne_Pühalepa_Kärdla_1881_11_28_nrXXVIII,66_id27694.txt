Koos olid, Tallitaja: A. Espak, abimehed: P. Püss ja W. Uusmann seatud kohtupääwa pidamas.
Ette sai wõetud see asi, mis Protk. XXVII ja 65 al sel 14 Novm. 1881 poolele jäi.
Karl Teras astus ette ja sai see asi tall ette pandud, rääkis wälja, et tema, Karl Sinik, Joh. Koid ja Gustav Kiiliga nimetud õhtul hilja sauemaja poole minnes on näinud et tüdrukud Martijooksmast koju läinud, seepärast läinud nemad järel ja tahtnud näha kas naad kodu weel tantsiwad. Nemad seisnud neljakesi seal kojas ja pole midagi kara teinud. Kui tulega kotta tuldud neid waatama, on nemad kolmekesi uksest õue tulnud aga Joh. Koid on kotta jäänud. Pärast on nemad teiste läbi kuulnud, et Joh. Koid pööningule läinud. Ei ole tema ka ial õhtuti seal akna taga koputamas käinud.
Karl Sinik astus ette ja sai see asi tall ettepandud rääkis wälja niisamma kui K. Teras. Pealeselle ütleb et see tõsi on et sauemaja aknate taga õhtuti koputud saab - sest ta elab isi ka seal - aga kes seal koputamas käiwad, ei tea tema.
Gustav Kiil astus ette ja sai see asi tall ette pandud, rääkis wälja kui K. Teras ja K. Sinik. Üleb, et üksainus kord on tema kord sauemaja aknast ühel õhtul sissewaatanud, aga mitte koputanud.
Johann Koid astus ette ja ütleb et tema üksi pööningul on olnud ja teisi kedagi.
Jüri Tareste astus jälle ette ja sai see tal ette pandud, rääkis wälja, et tema kuuldes on neid ikka 4-5 poisi mooda redelit pärast alla tulnud pööningult.
Said naad kõik suud suud wastu pandud, jäid igaüks kindlaks oma wäljarääkimise juure.
Kohus tegi otsust: Et Johann Koid, Karl Teras, Karl Sinik ja Gustav Kiil kesköö ajal weel wäljas hulkuwad, siis pealegi weel kortri majase lähewad teiste töörahwa rahu rikkuma, selle trahwiks makswad nemad igaüks 1 (üks) rubla hõb. walla laeka, mis kahe nädala aja sees peab äratasutud olema.
Et muud toimetust polnud, sai koosolek lõpetud ja igaüks läks jälle oma koju.
