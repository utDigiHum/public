Ükstuhat üheksasada üheksateistkumnemal aastal Küinla kuu 27 päewal ilmus Oro walla majasse Oro walla liige Juula Otmann sündinud Estholm ja palus walla kohtu aktide raamatusse omma isa suusõnalist seadmist wõi töstamenti mis weikese era paberi peal on kirjutatud üles kirjutada mis järgmiselt seisab:
"Leppiku Jüri Estholm lubab Lepiku koha omma tütre Juulale, kui tema sureb, kõige liikuwa waraga, Ja ema peab selle kohast üles pidamist, 25 rubla raha, ja tüttar Liisu saab 25 rubla, ja tütar Tiiu saab 110 rubla. See on tehtud 1906 a 25 Juuli k. päewal allakirjutanud Juri Estholm jo tunnistus mees: Ado Otmann
Selle Juri Estholmi wiimase seadmise juures olime ja tõendame et see nii sõna sõnalt on kirja pandud.
Juula Otman &lt;allkiri&gt;
A Otmann &lt;allkiri&gt;
Tunnistab õigeks Oro walla wanem Juri Baumann &lt;allkiri&gt;
