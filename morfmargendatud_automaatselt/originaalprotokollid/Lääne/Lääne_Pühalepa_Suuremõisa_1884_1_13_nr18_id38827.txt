Et Mihkel Keskla, Peter Kiwer, Juhan Rakki (Peter Kusik oli Hakenrihtri kohtus) ja Jüri Känd omale mõistetud trahwi Nr 10,11, 13 ja 15 al, 3 rubla mitte maksma ei tulnud:
Peawad kohtumõistmist mööda oma käsukuulmata oleko ja wastuhakamise pärast igaüks 2 rubla trahwi walla laeka maksma.
