Kaewama astus Juhan Raudsep Juhan Poots peale et hobbune 13 päwa waeg olnd, otsind 3 päwa wimaks (hobbune) leitud ned rauad alla pandud, neist raudest järrele pärrind kes rautand Juhan Poots tunnistas hoost omma hobbuseks pidand; need nimmetud päwad ja rautand; nüüd ei taha maksta üks rubl päwa kohta.
Juhan Poots tunnistab ilmma kawalusseta üsna kogemattaks, et hobused üsna ühte mödi ja minnul ostetud kewade ning kippub ärra; leidsin seda hoost ning hoidsin et ei jookse ärra, ja piddi omma (hobune) ollema; kui nüüd käest ärra wõeti, siis leidsin ommal puise Kulla alt (karja ma pealt); ollen kül seda wõerast hoost pruukind 10 1/2 sö,a wahhet, kui omma hoost.
Kohtumehed moistwad et Juhan Poots peab 3 R. 50 koppik hobbe weel peale rautamist jure maksma Juhan Raudseppale.
Moistminne täitetud.
