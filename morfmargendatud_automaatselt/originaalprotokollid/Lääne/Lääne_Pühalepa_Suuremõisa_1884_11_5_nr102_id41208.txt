Sai ettewõetud, mis Joh. Nõm Nr 88 al sissekaebas, kus tunnistus Juhan Männama ees oli ja lausus: et Johan Huuk ja Peet Kaew on ühes katlas liha (liha) keetnud. Ta ka süia saanud, wana sea suitsetud liha. Nad ütlend, et neil 4 sea reit ja 2 lambareit ja 2 lamba külge on, mis Johan Nõmme käest toonud; Teda käskinud sest waid olla, aga lihakintsusi ta põle näinud.
Tunnistas Tomas Pass ütles, et ta näinud Huki ja Kaewu sea liha keetwad, mis ta ka maitsnud niipaljo, et arusaanud, suitsetud liha olewad. Seda ei tea, kust wõtnud.
Tunnistas Andrus Sild lausus, et ta põle Huuk ega Kaew sealiha süüa saanud, aga Huuk üttelnud, et nad toonud liha ära. 1 sealiha kints kadunud ja Kaew on 1. kintso koeo wiinud.
Huuk ja P. Kaew ütlesid et neil enestel kodunt tuua liha olnud ja keetnud, mis J. Männamale ka süüa annud.
Tunnistus(ed) ütles(id), et see liha, mis ta Männama süüa saanud, pärast selle 1se liha keetmist ja söömist olnud.- Tunnistused säid see juure, et nende üttelused tõsised on.
Joh. Nõm andis üles, et tal warastud
3 sea sinki a 20 naela, a nael 15 k teeb 9 r.- k.
1 lamba külg 5 naela a 10 kop -,,- 50
Kohus mõistis tunnistute tunnistust mööda, et J. Huuk ja P. Kaew seda warastud liha kumbgi peab 4 r.75 kop. maksma ja oma warguse töö ette
sai J. Huukil 30 hoopi witsu ja
-,,- P. Kaewul 30 -,,- -,,-, mis ka kätte said antud.
