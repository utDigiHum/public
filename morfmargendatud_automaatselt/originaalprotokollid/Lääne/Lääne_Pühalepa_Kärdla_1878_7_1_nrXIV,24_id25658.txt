Koos olid Tallitaja: P. Naber, abimehed: S. Läck ja Wil. Kutser seatud kohtupääwa pidamas.
An Töu, kellel Protk. XI ja 14 al sel 29 Mayl s.a. ühtegi trahi pole möistetud saanud, astus ette.
Kohus tegi otsust: Et An Töu see eest, et oma wõerast last wargale on melitanud, maksab 1 Rub. 50 Cop. trahwi walla laeka.
