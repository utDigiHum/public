Ükstuhat üheksasada seitsmenda aasta Januari kuu 22 päewal ilmusiwad Oru walla Kohtusse ära surnud Oru walla liikme Hindrek Andruse poja Mändbergi pärijad tema endine lesk Mari Kurismann ja tema allaealiste laste Elwine ja Emilie Mändbergi eest nende eestkostja Priidu Wahtmann, kes awaldasiwad: Meie palume enestewahel ära toimetatud Hindrek Mändbergi waranduse ära jäutamist aktide raamatusse sisse kirjutada.
Hindresk Mändbergist jäi liikuwat warandust järele 36 rubla 75 kop. summas. Selle waranduse kohta on Kohus pärimise õigusesse kinnitanud Mari Kurismanni Hindrek Mändbergi endist leske ja tema allaealisid tütred Emilie ja Elwine Mändberg, kõik ühe suurustes osades, mille järele iga pärija saab:  Mari Kurismann			12 rubla 25 kop.		Elwiine Mändberg			12 rubla 25 kop.		Emilie Mändberg			12 rbl. 25 kop.		
Mõlemad alaealised lapsed jäewad nende ema Mari Kurismanni kaswatada. Mari Kurismann annab kumagi lapsele kolm rubla nende erawaranduseks Oru walla Kohtusse hoju pääle ja tarwitab üle jäedawa osa laste kaswatamise pääle ära. Selle lepingulise ärajäutamisega oleme meie täiesti rahul ja kirjutame alla.
Emilie ja Elwiine eestkostja Pridu Wahtman &lt;allkiri&gt;
Pärija: M Kurisman &lt;allkiri&gt;
See waranduse ärajäutamise leping on mõlema lepingu poolele enne nende poolt allakirjutamist ette loetud ja pärast Priidu Wahtmannist ja Kari Kurismannist oma käega alla kirjutatud.
President: G. Spuhl &lt;allkiri&gt;
Kohtu liikmed: K. Rein &lt;allkiri&gt; P Liin &lt;allkiri&gt;  J Altmann &lt;allkiri&gt;
Kirjutaja Joh. Tilling &lt;allkiri&gt;
Копiя выдано за No 6 1907г.
