Kohto korra pärrast ollid, Parrila Kebla ja Riddali koggokonna maiase kokko tulnud kohtomehhed
Ette astus Wanna moisa kohha piddaja Woldemar Medel ja kaewas nenda: 12ma selle ku päwa ösel jättis üks warras kes minno rehhe jurest kottiga tulli sedda mahha ja minna sain jälge peale ja tullid Parrilase Ue Jürri perrese Prido Jaaks jure kes ka pawa minno jures tööl olli, siis sai seal otsima akkatud Kulla kubja walla kirjutajast ja leitsime sealt weel üks wakane kot Odre lauda pealt heinde seest ja siis tunnistas isse ka et on selle minno rehhest tonud ja teise mis mahha olli jänud, ja need on arwata 2 uut wakka Odre, ja arwata et on uhhest lahtisest ullo allusest sisse läinud.
Selle peale sai Parrilast Ue Jürri Prido Jaaks ette kutsutud; ja temma ei selgand sedda ennam ja tunnistas et on sedda teinud.
Et se assi üsna selge olli moistis kohhus omma arwamist möda Priso Jaaksile 30 witsa hoopi ihho nuhtlust trahwiks, et temal rahha polnud leppis Medel sellega et Prido Jaaks temma kahjo tassumisseks raiub 20 sülda aggo 15 kopik süld mis 3 Rubla walja teeb.
Allakirjutanud
Kohtowannem Jaan Jünter XXX
Kohto kämehhed Jürri Jürjer XXX Juhhan Krabbi XXX
Kirjutaja T. Neider &lt;allkiri&gt;
