Kohto korra pärrast ollid Parrila Kebla ja Riddali koggokonna kohhus koos.
Ette astus Jõesust metsawahhi Jürri Markus naene Ann ja kaewas Kesso poisi Willem Nii peale nenda: et on temma poega peksnud kingiseppa toas ja perremees Willem Grüünpärg on tedda ärra lahhotanud.
Selle kaeptusse peale sai Willem Nii ette kutsutud ja temma ei salganud mitte ja ütles et on sedda poisi 2 korda lönud.
Et Willem Nii on isse tunnistand et on lönud, moistis kohhus et temma peab 3 Rubla trahwi maksma Jõesu walla laeka.
Kohto wannem: Jaan Jünter XXX
käämehhed: Jürri Jürjer XXX Juhhan Krabbi XXX
Kirjutaja T. Neider &lt;allkiri&gt;
on 3 Rubla maksnud
