Ette astus Rein Otsa ja kaebas et tema aitast sui heina aeges willo warastud ja arwab seda wargust Juhan Matsiko laste süüks; sellepärast et sel pääwal kui näha olnud et willa tükkid aida parandal maas olnud aga perenaene sest suuremad polle tähele pannud waid pärast ales leidnud willad puuduwad kust ka mitte keik ära ei olnud witud, muud kui kaks tuusti musti willu, üks mustem ja teine hallim; ja 3 tuusti kokku keratud walgid, oli Juhani tütar kui nemad isse heinamaal olnud nende rehaalt ühe wikkati wõtnud ja niitmas käinud; aida uks oli lukkust lahti jäänud sest et seal warast karta polle olnud; Nüüd aga kui willad kadund leidnud arwab et Marri Matsik kui wikkatid wõtmas käinud ka willad ärra wiinud, sest tema on enne warastanud ja wargust ka wastu wõtnud mis awalikkuks on tulnud.
Kohus wõttis seda asja ülekuulamisse alla ja selle asja tunistuseks tulewal kohtu pääwal ette kutsuda.
Kohtuwan. Peter Thomson
Kõrwamees Jüri Erik XXX
Kõrwamees Jaan Koplik XXX
Kirjut. M Kentmann &lt;allkiri&gt;
