Kohto ette tulid Karel Mäberg Weike Lähtrost 70a wana _ Mart Alato Karli p. Lähtrost Madis Padis Kustase p. Lihola hingekirjas Kasari walla al. ja palusid Karel Mäberg wiimist tahtmist kirja panna. Karel Mäberg täie mõistuse juures andis üles et mina olen oma naesega Liisoga oma wäimehe juures olnd ja nimelt Mart Alato juures, ja wiisin senna kõikkisugo maea kraami kaasa nüüd ma naen et ma just heaste enam Mart Alatoga läbi ei saa ja et tülide eest hoida seepärast soowin seda et mino täine wäimees Madis Padis mind om juure wõttaks, ja siis ka pool kraami Mart Alato käest kätte saaks pool kraami jääb Mart Alato kätte sest Mino naene Liiso 64 a. wana tahab Mart Alato juure jääda - 
Mart Alato andis üles et tema ka priitahtlikult annab poole saadud kraami oma äiale Karel Mäbergil kätte ja toidab oma ämma ei te kui Jumal terwist annab kuni surma tunnini.
Kraamist saab Karel Määberg kätte ja nimelt 29 Rbl. 05 kop. Kakskümmend üheksa Rubla wiis kop. Raha. _ üks tünder kardulid_ poolteist Tsätwerti Rukkid ja poolteist Tsätwerti odre. Wankrest pool hinda nüüdse takseerimise järel - üks adra raud, ühed wäljad ja Kleid, üks sonniku ang. -
Raha maksan iga aasta 10Rbl, 1891.a. saab kohe kümme Rubla wälja makstud. Wilja maksan wälja iga aasta üks tsetwert Rukkid, pool odre. Kardulid anna kohe üks tünder. lehm jääb ema jäoks keda mina toidan, nenna sammoti ka.
Karel Määberg üttles weel wälja et see kraam saab keik müüdud ja pool selle rahast saan mina ja pool jääb ema jäoks ja mis weel järele on mino kraamist ehk rahast jääb Madis Padis kätte.
Madis Padis ütles, et kui see pool Kraami keik tuleb tema kätte siis tema toidab äia taati kuni surma tunnini kelle peale keik kolm nimed alla kirjotasid     /Maddis Balleg/
                                                  /Mart Allato/
                                                     XXX     Karel Mäberg
Kohto poolt saawad ülemal nimetud nime allkirjad .................. tunnistud nenna sammoti ka selle leppituse. See akt on üles wõetud üks tuhat kaheksa sada üheksa kümmne esimesel aastal teisel Detsembri kp.
Kohtowanem: K. Karro
Kohtomehed: P Poll
                       : J.Palmer
                       : J.Saar
Kirjotaja: C.Redlich
