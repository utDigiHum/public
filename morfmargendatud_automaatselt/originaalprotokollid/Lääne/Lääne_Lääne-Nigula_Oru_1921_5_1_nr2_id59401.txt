Ükstuhat üheksasada kahekümne esimesel aastal Mai kuu 2 päewal ilmus Oru walla hoolekande kohtusse Oru walla kodanik Hans MIhkli p. Mark kes isiklikult selle kohtule teadupärast on ja awaldas: Mina oma selge meele ja mõistuse sees luban kõik oma liikuwa waranduse mis on Nuutre talu koha peal, peale oma surma oma lihase tütrele Anna Hansu t. Saarlepale kelle juures mina praegu elan ja kes mind on kohtustadud kuni surmani ülewel pidama. Minu teised tütred: Leena, Maria ja Liisa on oma osa kätte saanud ja selle pärast ei ole neil sellest pärandusest enam midagi pärida.
XXX Hans Mark kirja ei oska.
Kohtu esimees: Adu Kippermann &lt;allkiri&gt;
Selle töstamendi sisu ja tegemise tunnistawad õigeks: K. Krumholz &lt;allkiri&gt;
J Ment &lt;allkiri&gt;
Tunnistawad õigeks
Hoolekande kohtu
Esimees: A Kippermann &lt;allkiri&gt;
Liikmeks: A Kerm &lt;allkiri&gt; K. Pross &lt;allkiri&gt;
Seketär J Saar &lt;allkiri&gt;
