Gustav Märjamaa wöörmünder Madis Pilder astus ette ja kaebab, et Ado Õunapuu poeg minewa Kesknädal õhtul koolist tulles G. Märjamaa mütsi peast ära tõmbanud, G. Märjamaa seda Anna Rang seltsis A. Õunapuu juurest läinud ta poja käest kätte saama. Seal saanud A. Õunapuu pahaseks ja löönud G. Märjamaat 3 hoopi. Teise pääwa toonud ta poeg mütsi ta kätte.
A. Õunapuu poeg Johannes Õunapuu 12 a. w. tunistab, et ta seepärast G. Märjamaa mütsi peast tõmbanud, et ta arwanud et G. Märjamaa teda sõimanud. Üks on teda sõimanud, tema siiski selgest ei tea, kes seda teinud.
G. Märjamaa ja Wil. Tõu kes ka koolist tulnd ja G. Märjamaaga kõrwu käinud, tunnistand et ta pole sõimanud.
A. Õunapuu astus ette ja püüab ennast wabandada, et ta G. Märjamaat pole löönd.
Anna Rang astus ette ja tunnistab, et tema läbi akna, õues seistes selgesti on näinud et A. Õunapuu G. Märjamat 3 hoopi löönud ja ka G. Märjamaa nutu heale kuulnud.
Suud suud wasto jääwad esite waidlema, pärast andis A. Õunapuu järele.
Kohus katsus neid lepitada. Ado Õunapuu palus Madis Pilder ja Joh. Õunapuu Gustav Märjamaa käest andeks, ja leppisid nõnda üksteisega ära.
Seega sai kohtupääw lõpetud.
