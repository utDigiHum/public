10) Astus ette Jaan Kork ja kaebas, et Jaan Olmanni 2 härga on kaks korda pärrast Michklipäwa minno kapsa aedas käinud; essiminne kord on Jaan Olmanni issa need härjad minnu kapsa aiast wälja aianud ja teine kord leidsin minna isse need härjad seält seest ja aiasin wälja; ja nende kahhe korraga on Jaan Olmanni härjad 157 kapsa pead ärra sönud, mis kohtuwannem Jaan Willik ja kohtukörwam. Hans Neuwaldt kahju wadates ülles luggesid.
Astus ette Jaan Olmann ja ei salganud sedda mitte, waid tunnistas isse ka, et ta härjad on 2 korda Jaan Korki kapsa aedas käinud; agga peäle sedda tunnistas Jaan Olmann, et, enne, kui temma härjad seal aedas on käinud, on se kapsa aed ükskord weiksid üsna täis olnud.
Agga et üllewel nimmetud kohtu liikmed ei olle mitte neid kapsa päid, mis enne on södud, (mitte) ülle luggenud, waid agga need, mis üsna wärskid södud jured näitasid ollewad, sepärrast
Kohhus moistis, et Jaan Olmann peab selle kahju tassumisseks 1 Rubla höbb. Jaan Korkile maksma. Sellega leppisid kohtu käiad ärra.
Kohtuwannem Jaan Willik
KohtuKörwam. Hans Neuwaldt
Kohtujärjem. Jaan Berg
Kirjutaja Abr. Krein &lt;allkiri&gt;
