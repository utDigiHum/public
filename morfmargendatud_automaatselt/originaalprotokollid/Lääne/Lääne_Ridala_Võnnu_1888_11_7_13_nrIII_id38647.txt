Täna pidi Mari Wiidas tunnistuse mehed ette tooma, waata kogukonna kohtu prot. 24 Oktobr. 1888 a. No 12 pt. 5, aga ei ole tulnud. Seepärast sai Uemõisa walla talitajat palutud, seda trahwiraha Mari Wiidase käest wälja wõtta, see on 9 rbl. 5 kop. ja seda wiibimata siia walla kohtuse saata.
Kohtu peawanem: Jüri Karja XXX
Körwasmees: Mart Jök XXX Mihkel Pett XXX
Kirjutaja: J. Mets &lt;allkiri&gt;
