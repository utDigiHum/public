Oli Mäewalla kogokonna kohus koos
Ette astus Kustaw Spuhl ja kaebas:"Mino koha ma peal elab üks wabadik, Peeter Kõrges; kellega mina Jürri pääw 1887 aastal kauba tegin, et tema minole aastas 17 rupla renti pidi maksma selle ma tükki eest mis tema käes on. Nüüd ei taha aga tema poole aasta renti 8 rubla 50 kop. mitte wälja maksta; sellepärast palun kohtu läbi seda tema käest wälja nõuda.
Ette astus Peeter Kõrges, Oidrema wallast, Mihkli kihelkonnast ja ütles wälja:"Mina tegin Jürripääw 1887 a. Kustaw Spuhliga kauba, et selle ma tükki eest mis mino käes on, aastas 17 rbl. renti pidin maksma; ilma määramatta aja peale. Aga praego ei ole mull seda mitte wõimalik wälja maksta ja palun teda kunni 1se Mihkli k.p. kannatada, siis mina maksan I pool renti 850 kop. ja II pool maksan 1888 a. 1.sel Märtsil wälja.
Kohto poolt sai lubatud see maksu aeg, mis Peeter Kõrges eneselle wälja palus ja Kustaw Spuhl ei wõi teda enne seda aego oma maa pealt wälja ajada, ega kimbutada, waid wõib temaga tulewa aasta Jüüripääw uuesti kaubelda.
Kohtow: Mihkel Reets XXX
I Kõrwamees Andrus Kilemeth XXX
II Korwamees asemel Jaan Junter XXX (Rapla kohtum)
Kirjutaja: G. Aberg &lt;allkiri&gt;
