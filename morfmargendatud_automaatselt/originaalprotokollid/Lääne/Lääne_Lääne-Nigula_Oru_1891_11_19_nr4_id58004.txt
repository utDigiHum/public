Astus ette lesk Tiu Druus oma Käemehe Jüri Ewertiga ja palusid kohtu kirja üles panna Wankre Sauna koha hooned, mis lesk Tiu Druusi omandused on: üks eluhoone, kaks aita, üks laut, üks hagerik, üks köök August Tikbeini nime peale ja selle omanduseks. Neist hoonedest on elu tuba ja kamber, kaks aita ja laut ja hagerik Puuri talu koha Krundi peal.
Kohtuwanem A. Krein &lt;allkiri&gt;
Körwasmehed Juhan Ulm Hans Weide Jüri Ulm
