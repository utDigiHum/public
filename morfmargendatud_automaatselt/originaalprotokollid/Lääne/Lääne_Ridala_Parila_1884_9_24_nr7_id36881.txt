Ette astus Mart Nöör Wõnnust ja kaebas, et tema Hapsalu ladalt kodu tulles Tobra Kõrtsu ukse ees Ehmja poste käest peksa saanud, näitas et temal suured hawad peas olid mis wist pudeliga olid löödud ja ütles wälja, et on rohtude eest ärakulutanud 1 Rbl. ja 2 pääwa ärawiitnud kus eest tema pärib 1.60 kop., ja saatis tunnistuseks
1) Astus ette Karel Mikk, ja tunnistas, et tema on näinud et Ehmja poisid Karel Ragen, Madis Kreek ja Karel Tähw on seal (ukse) kõrtsu ukse ees peale selle kui mitmed moodi sonnadega ärritud teiste kallale läinud ja löönud; pimeduses polle arru saanud kellega ehk keda löödud, aga Mart Nöör on seal karjuma hakand ja oma haawu näidand, mis kangeste werd jooksnud.
2) Astus ette Johan Põltsam ja tunnistas seda samma.
3) Astus ette Marri Uuma, ja tunnistas ka seddasamma.
Ette astusid Ehmja poisid, Madis Kreek, Karel Ragen ja Karl Tähw ja wabandasid, et nemad olnud joobnud ja ei mäleta ühtegi riiust. See peale said suu suud wastu kutsutud tunnistuse meestega, kus läbi selgeks sai, et nemad need lööjad ollid, aga nad ei tunnistanud mitte, kes peamees nende hulkas. Nad leppisid isse keskis Mart Nööri'ga.
Kohus mõistis, et poisid igga üks walla laeka trahwi 3 Rbl. makswad Tallurahwa s. §1270 järrel.
Sellepärrast, et Kõrtsu ees riidu kiskuwad ja teemehi rahul käia ei lasse
Kohtuwanem Jaan Jünter XXX
Kõrwam. I Johan Krabbi XXX
Kõrwam. II Maddis Topp XXX
Kirjut: M Kentmann &lt;allkiri&gt;
Karl Ragen 3 Rubl trahwi
Maddis Kreek 3 rubl. trahwi
Karl Tähw​​​​​​​ 3 Rbl. trahwi
makstud.
