A. Lisa 4. Jan. punkt . pag. 52.
Kohtuwanem Jüri Silwer ja tallitaja Jaan Iling räkisid wälja, Mart Krausi jures käinud olewad ja Ann Krausi heinad sealt leidnud olewad, ja weel Hans Lorens tunnistanud näinud olewad, kui M. Kraus enese öe A. Krausi kuhjast heinu koormaks teinud, siis saanud M. Kraus nende poolt käsu, kohtu ette tulla, seda asja öiendama, aga ta pole käsku täitnud ega tulnud, sepärast
Kog. kohus möistis et Ann Kraus ühe abilise omal wötma peab, kes M. Krausi jurest ühe koorma heinu peäle teha aitab, mis tallitaja jures olemise korral peab sündima, ja peäle seda peab M. Kraus walla laeka heaks 3 rbl. raha trahwi maksma.
Kohtuwanem Jüri Silwer XXX
Körwasmees Jüri Ulm XXX
Körwasmees Rein Kiisberg XXX
Kirjutaja A Krein &lt;allkiri&gt;
