Astus ette Marri Habel (moisa korraline) ja kaebas et karjane tedda olli löönud tedda mahha lükanud ja silmad lõhki kiskunud, tema ei tahha mitte leppida sest karjane Jürri Jurberk on ähwardanud tedda ikka weel lüa.
Astus ette karjane Jürri Juurberg ja ütles wälja et Marri Habel teda enne olli löönud ja söimanud. "Nemad waidlesid suud suud wastu ja ei olnud kumagil tunnistust ette tua, ja ei lubanud ial üheteisega ärra lepida.
Kohus arwas öigeks et kumbki walla laeka 1 rubl trahwi peab maksma kokko 2 rubla.
Sellepärrast et ühte teist ähwardasid edaspidi weel lüa.
2 rubla trahwi makstud.
