Üks tuhat kaheksasada üheksakümmend üheksamal aastal Paastu kuu kahekümne üheksamal päewal Oro walla talupojad Karel Ado p. Matwer ja Jüri Andruse p. Mahkur, kes kohtule isiklikult tutawad on, palusiwad selle kohtu lepingute raamatusse oma suusõnalist järgmist lepingut kirjutada:"mina, Jüri Andruse poeg Mahkur, töutan üks sada nelikümmend /140/ rubla, mis Matweri käest ostetud talukoha eest wõlga jähin wiimsele ära tasuda ühes iga wölgnewa rubla pealt kolm kop. protsenti makstes järgmistel tähtaegadel: 1899 aastal 56 rubla, 1900 aastal 56 rubla ja 1901 aastal 28 rubla," ja mina, Karel Matwer awaldan oma rahulolemist Mahkuri poolt awaldud wõla aramaksmise tingimiste kohta."
See leping on pärast lepingute raamatusse kirjutamist pooltele ette loetud.
Karel Matnr &lt;allkiri&gt;
Jurri Mahkur &lt;allkiri&gt;
President: A Kaaw &lt;allkiri&gt;
Kohtu liige: J Kreekow &lt;allkiri&gt;
Kohtu liige: J Lindal &lt;allkiri&gt;
Kohtu liige: J Kleman &lt;allkiri&gt;
Kirjutaja Joh Weksar? &lt;allkiri&gt;
