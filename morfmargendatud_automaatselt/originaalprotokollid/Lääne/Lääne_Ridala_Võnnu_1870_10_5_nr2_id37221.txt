Tulli ette Kirrimäe wallast Jaan Saar ja kaebas: Hapsalo ladalt tulles Wönno Jurika körtso ukse ees, kus ma seisin, tullid ühhed kaks hobbust wankri ees, sure söiduga ja aiasid mind pool surnuks. Agga nüüd ollen ma kuulda sanud, et Wönnust Reino Karel ja Kertsama Jürri ollid olnud, kus ka Undo Jürri poeg Ado peal olli olnud.
Siis sai ette kutsutud Reino Karel ja Kertsama Jürri ja tunnistasid: Meie ei tea egga polle näinud.
Siis tulli Wönno walla tallitaja ja tunnistas: Undo Jürri poeg Ado tunnistas, et Karel ja Jürri on omma hobbustega sedda meest mahha aianud, ja Mart  Naab tunnistas nendasammuti.
Selle tunnistus peale teeb kohhus otsust Jaan Saar omma leppimist möda et Kaarel Söer ja Jürri Mick peawad kumbgi 2 Rubla 50 koppik Jaan Sarele wallo kannatamisse ette wälja maksma. 12mal Oktobril 1870.
Kohtowannem: Jaan Kisk.
Kõrwasmees: Gustaw Lauritz Jürri Sur
