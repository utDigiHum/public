Tulli Liko Willem Jög ja kaebas: Minno sigga mis aasta wanna leidsin minna ristlust wiggaseks löödud tannawas maas ollewad, ja on selgeste nähha, et Sarrapiko tule weske tiiw on sedda teinud, mis kül kegi ei olle näinud.
Se peale ütles mölder Hans Sarra, meie polnud issi koddo, kui weske on wõerastest jooksma pandud, issiärranes Willem Jög ennese töö, et weske jooksis.
Wiimaks arwasid sigga 6 Rubla wäärt ja leppisid nenda, et mölder poole sest maksab se on 3 Rubla kahjo tassumist.
Kohos agga möistis, et eespiddi ei tohhi mitte se weske karjasmaal ilma aeata jooksta; waid peawad aed ette teggema.
Kohtowannem: Jürri Soone
Körwasmees Mihkel Kelt Jürri Naab
Kirjotaja: J Reinans &lt;allkiri&gt;
