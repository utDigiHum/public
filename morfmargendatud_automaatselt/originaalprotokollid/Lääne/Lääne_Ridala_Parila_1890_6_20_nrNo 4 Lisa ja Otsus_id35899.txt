Kostja Hindrel Wallek ilmus kohtu ette ja ütles: mis moodi mina nende eest maksan, need läksiwad minu mesilindude peale ja tagasi pole neid wõimalik anda nüüd.
Otsus: Nõudjal on õigus ja on odawalt leppinud. See on maarahwal tuttaw asi, et tulnud mesilinnud saawad tagasi antud ehk wiis ehk kuus rubla nende eest makstud. Hindrek Wallek peab 3 rbl. ära maksma talitaja kätte. Edasikaebamist ei ole selle otsuse wastu lubatud.
Eesistuja: Jaan Kahm &lt;allkir&gt;
I Kohtumõistja
II Kohtumõistja Hans Rüü XXX
Kirjutaja L. Marleij &lt;allkiri&gt;
