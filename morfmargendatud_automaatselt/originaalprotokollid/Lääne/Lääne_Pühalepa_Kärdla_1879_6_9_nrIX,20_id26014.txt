Andrus Kõrges kutsti ette, et wabriku tööle ei taha minna ja alati kõrtsus istub, kus oma teenitud raha ära joob ja ometi on tall weel pearaha wölgo.
Et Andrus Kõrges ennast wabandada ei wõinud tegi kohus otsust: Et A. Kõrges oma korratuma elukombe parast istub 36 tundi wangis.
Et muud toimetamist polnud, sai kohtupääw lõppetud.
