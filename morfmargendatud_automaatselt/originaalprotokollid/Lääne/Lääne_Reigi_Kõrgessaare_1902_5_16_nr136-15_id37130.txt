Üks tuhat üheksa sada teisel aastal kuueteistkümnemal Lehe kuu päewal ilmusid Kõrgesaare walla kohtu ette kohtule isiklikult tuntud Kõrgesaare walla liige  Ann Jaani lesk Römmel kellel lepingu tegemiseks seaduslik õigus ja palus oma suusõna ülesandmist järgmiselt kohtu lepingute raamatusse üles wõtta:
Mina, Ann Rõmmel selge mõistuse juures olles, olen wara kohta, mida ma kui lesk, 18 aasta jooksul lapsi kaswatades olen ühes lastega korjanud, järgmised määrused teinud:
1.) Peeter Römmel saab:1			hobuse		1			warsa		1			mullika		4			wõrku					rau 1 wankri asside tarwis		2			wankri witsa					ja ühe wana wankri nii kauakas omale pruukida kui omale uue teeb.		
Muud asjad, peale wankri, jäewad Peeter Rõmmel omanduseks. Lelo koha majad ja maad jäewad Peeter Rõmmel ning minu wahel ühes kahe teise poja Jakob ja Jüriga pooleks pruukida nii et töö saab ühes tehtud ja koha saadused ära jäutatud.
Kui mina ise enam poole koha eest ei taha hea seista ja seda pruukida, siis on Peeter Rõmmel'il õigus kohta terwelt oma kätte wõtta kui tema ise seda eesõigust teiste wendadele ei taha anda;
2.) Pojad Jakob ja Jüri saawad kumbgiühe			härja		kumbgi 4			wõrku		ja koku ühe			hobuse		
ning elawad minu kõrwas poole koha peal. Peale selle saawad nemad oma kokku lepimist mööda Laewa "Linda" sees Peetri nime peal olewast 1/8 osast 1/4 jau (s. on. 1/32 osa laewast) kus juures teised Peetrile laewa osa wõlast 46 rubla peawad ära maksma:
3.) Kõik muu kraam jäeb minu oma kätte pruukida ja pärib peale minu surma see poeg seda, kes minu eest hoolt kannab ja kelle juure ma ära suren;
4) 2 Mai k.p. 1902a.peale hakkawad pojad iga üks omale ise warandust korjama ja muretsema, mis enam minu jautuse alla ei käi;
5) Kõik teised lapsed, s. o. tütred, on oma jäud juba kätte saanud.
See ülesandmine on pärast lepingute raamatusse kirjutamist mulle ette loetud.
Ann Rõmmel palwe peale, kes kirjutada ei oska, kirjutas alla: K Punn [allkiri]
Waata leping No 13/1904a.
