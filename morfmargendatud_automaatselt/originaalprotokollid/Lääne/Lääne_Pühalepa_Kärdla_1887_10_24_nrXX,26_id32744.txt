Koos olid, Tallitaja H. Pisa, abimehed W. Mölder ja W. Poola, seatud kohtupääwa pidamas.
Gustav Jürgens kaebab, et tema on Jüri Tareste kätte kolm paari saapaid parandada annud ja 50 kop. raha on tema ema ka talle annud naha ostmise tarwis. Kõige paremad saapad on aga Jüri Tareste ära kautanud. Mõne aja pärast küsinud tema oma saapade järel, Jüri Tareste lubanud neid pärast lõuna ära tuua. Et ta ei toonud, läinud tema paari pääwa pärast järel. J. Tareste ei olnud kodus. Tema toonud 2 paari saapaid ära, kõigeparemaid saapaid ei olnud seal leida. Pärast lõuna tulnud J. Tareste tema juure armu paluma, et need on tema käest ärawarastud.
Jüri Tareste astus ette ja sai see asi temal ettepandud, rääkis wälja, et tema need saapad oma jalga pannud, kõrtsu läinud, see 50 Cop. mis G. Jürgens'i ema annud, seal ärajoonud ja magama jäänud. Ülesärkades leidnud, et saapad olnud ärawarastatud tema jalast.
G. Jürgens nõuab saabaste ja 50 Cop. ette 2 rub. 30 Cop.
Otsus: Jüri Tareste peab G. Jürgens'ile 2 rub. 30 Cop. kahjutasumist maksma, selle pettuse eest, istub 48 tundi wangis.
Seega oli kohtupääw lõpetatud.
