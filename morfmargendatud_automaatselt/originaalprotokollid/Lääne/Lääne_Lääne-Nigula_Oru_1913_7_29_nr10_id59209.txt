Ükstuhat üheksasada kolmeteistkumnemal aastal Juuli kuu kahekümne üheksamal päewal ilmusiwad Oru walla liikmed Johannes Jüri poeg Nurkroos ja Juri Nurkroos ühes oma eestkostjaga Hans Hinnobertiga kes elle kohtule isiklikult tuntud on ja palusiwad oma wahel tehtud suusõnalist lepingid kohtu aktide raamatusse sisse kanda. Mina Johannes Jüri poeg Nurkroos jätan Kaasiku No 8 talu koha külge oma wenna Jüri osa 1/4 sellest kohast kes siis kui tema täiehealiseks saab, saab selle weerand kohta kätte ehk saab minu käest selle eest 700 рубlа. Mina Juri Nurkroos jättan oma pärantuse osa 1/4 Kaasiku talu kohast oma wenna Johannese kätte kes siis kui mina täiehealiseks saan mulle selle tagasi annab ehk mulle selle eest 700 rubla annab. Eestkostja Hans Hinnubert seletas seda mis Juri Nurkroos.
See määrus on enne alla kirjutamist meile ette loetud ja oleme sellega rahul ja kirjutame alla
Hans Hinnubert &lt;allkiri&gt;
Juhan Nurkroos &lt;allkiri&gt;
Juri Nurkroos &lt;allkiri&gt;
Tunnistab õigeks
President: W Põllus &lt;allkiri&gt;
liikmed: A Paius &lt;allkiri&gt; W Nootmann &lt;allkiri&gt;
Kirjutaja: J. Saar &lt;allkiri&gt;
