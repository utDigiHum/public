Kaebas Wõnnu möisa walitsus kirja läbi: Mõisa metsawaht Karel Madisoon on 29 Nääri k.p. 1887 a. Uuemõisa walla mehe Tönis Wiidas metsa warguse pealt kinni wötnud. Tema olla kuus kasepuud maha raiunud tüükast 7me - 3me tollini jämedad. Ja tahtnud kelguga ärawedada.
Ette tuli Tõnis Wiidas ja rääkis wälja: Mina ei ole enam raiunud, kui kask, kelle juurest mind Metsa kantsler Aadu Mäk ära ajas. Ja Kaarel Madisooni ma ei ole näinud.
Kaarel Madisoon tunnistas: Mina ajasin Tõnis Wiidast taga hobusega söites, kuni poole sooni, siis sain kätte ja töin need 5 kaske ära kuues oli kännu juures maas.
Kohtu otsus:
Mõlemi metsawahi Adu Mäk ja Karel Madissooni tunnistuse peale saab Tõnis Wiidas metsa wargaks mõistetud; ja peab iga kännu pealt 30 kop. trahwi maksma. Kokki 1 R. 80 kop. Waesuse pärast saab maksmise tärmiin 23 Aprillini s.a. määratud.
