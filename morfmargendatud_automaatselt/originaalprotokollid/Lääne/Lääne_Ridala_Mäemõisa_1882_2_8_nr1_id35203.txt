Ette astus Willem Prosowits; Mäe moisa rent herra Linde kaebtuse peale mis kirjalikkult olli sisse tulnud No 1 all 25mal Januaril, Ja ütles wälja. Temma loomad ei olle keid sedda kahju mis süggise Tallitaja waatanud mitte teinud! seal on senni ajani kui temma loomad kinni aeti üksi teistel käind, ja tema ei tahha mitte 10 rubla trahwi maksta.
"Kohus arwas heaks et Willem Prosowits 5 rubl rent herrale kahju tassuks ja ka 380 kop sööma raha ja leppiks moisa härraga.
Sellepärrast et Tallitaja süggise waatanud et seal ei olle mitte orrase peal looma, waid Hobbuse ja Lamba jägi nähha olnud.
Kohtuwan. Jaan Puhl
I Korwam. Mihkel Liiw
II Korwam. Jürri Siimon
Kirjt. M Kentman &lt;allkiri&gt;
