Wõnnu talitaja Hans Möll kaebas, et Palina Mihkel Pett on Wõnnu walla Magasinid 1879 aastast wõlga 48 karnits odre ja 10ne aastased protsendid 35 karnits.
Ette astus Mihkel Pett ja ütles: Mina olen waene ja ei jõua seda korraga maksta.
Kohus mõistis, et ta peab seda kolm korda maksma, iga kord 27 karnits, ehk 2 Rbl. 60 kop. Esimine termiin 16 Januaril 1889 a.
Allakirjutus:
Kohtuwanem: Jüri Karja XXX
Körwamees: Mart Jök XXX Mihkel Pett XXX
Kirjutaja: Joh. Mets &lt;allkiri&gt;
