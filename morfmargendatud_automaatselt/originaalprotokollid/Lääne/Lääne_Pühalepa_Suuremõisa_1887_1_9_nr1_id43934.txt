Hellamal koggukonna kohtomaias sai korrajärrel kohtopääw ärrapeetud kus kirja lõppul alpool nimetud kohto liikmed koos ollid.
Peakohtumees J. Murro
Kõrwasmees N. Johanibeer
-,,- -,,- J. Kari
Kirjutaja J. Kreupas
Kaebas Prido Wälja et ta läinud An Pildri jure ja pallunud et ta tüttar tooks talle Wabrikust üks pak tubbakast ja ohto läinud ta järrele ja August Wares olnud kasas kui naad tubba läinud üttelnd An Pilder kuule mis teie mo wasto maia kiiwa lopide ma küssisin kes, ta wastas et need kaks poisi kes siin on, se olli meite kohta, ma ütlesin ärra rägi tühja jutto ma polle lopinud selle peale An Pilder ütles eks ma põlle isse näinud kui sa meie lehma jures käisid ma wastasin ärra rägi tühja jutto küllab kohhus sedda õiendab ja teise korra ma läksin Juhan Känoga ja August Warresega seltsis siis tulli Leno Pilder soldati käe alt kinni olli pimme ta kuulis mo heale kui ma Känoga rägisin - Leno Pilder ütles, sealt tulleb Joe Prido lähhed sa jälle meie lehma juure, selle pärrast ma pallun et kohhus sedda oiendab ma ei olle nende Lehma jures käinud egga kiwidega loopinud se on üks walle jut mo peale tehtud ja teiseks soldatid käiwad ühte puhko seal jowad wina ja mangiwad pilli ühhe korra ma näggin kui soldatid jõid wina ja kaks kukke praetud ollid laua peal ja üks olli õlgede peal ja mängis pilli.
Jääb polleli
Et sekord middagi kohto asja ette ei tulnud sai koosollek lõppetud.
