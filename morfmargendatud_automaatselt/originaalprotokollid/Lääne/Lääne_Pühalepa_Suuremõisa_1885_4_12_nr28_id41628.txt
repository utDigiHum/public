Hellamal 12. Aprill 1885
Suurem. Kogukonna kohtumaeas sai korra järel kohtupäew ärapeetud, kus koos olid:
Peakohtumees J. Muro
Kõrwasm. N. Johanisbeer
-,,- J. Kari
Siis sai ettewõetud, mis 1884 Nr 111 al pooleli jäi, olid ees Kustaw Waldmann, Peter Nõmm ja Madis Proos, kes nendasamma rääkisid Nago 84 Nr 111 al. Et Kust. Waldmann Nimele käsu järele saatnud, laewaga loomi üle mere wiima minna, seks on järgmine tunnistus kirjalikult kohtu sees.
Sellega tunnistame meie, et Kustaw Waldmann meid käskis Peter Nõmme juure Hawikule minna. Tema Kustaw Waldmann ütles meile: minge senna ja wõtke Peter Nõmm seltsi tema peab tulema. Sellepeale siis Peter Nõm tuli ja tõi meie loomas Hapsalo ja maksime temale seda nõutud hinda. Seda tunnistab: Georg Holmann ja Peet Waldmann. Hapsalus 22 Märts 1885.
Et nad isi leppimisele ei saanud, siis mõistis kohus, et Kust. Waldmann on laewa 60 Rubla ette kaubelnud ja ka 5 Rbl. käeraha Nimel kätte maksnud, ning kinnitand, kes taganeb, maksab 20 Rbl. taganemist. Ja et ta seatud aeal, nagu kaup, (ja) mis tunitusmees tunnistab, põle laewa raha Peter Nõmel wäljamaksnud, misläbi kaup katki; peab K. Waldmann P. Nõmmel taganemise raha 20 Rbl. wäljamaksma. Ja et see kaup neil ülepea segaseks läks, peab ka Nõm, Waldmanni käeraha, kui kulu, koko 8 rubla tale tagasi maksma.
