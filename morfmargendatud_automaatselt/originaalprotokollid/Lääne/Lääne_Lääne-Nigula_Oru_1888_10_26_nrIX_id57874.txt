A. Lisa protokoll juure 9. Sept. s.a. pt. 2
Astus ette Lena Link 9 aastad ja rääkis wälja: Tema ei olla Orru mõisa põllal käinud ega ka sealt loomadele kaeru ette toonud.
Astus ette Kustaw Kouks 10 aastat wana ja rääkis wälja: Ei olle tema Orru mõisa põllult loomadele kaeru ette wiinud ega olla teinegi seda tegewad näinud.
Astus ette Aleksander Kansmann 12 aastat wana ja rääkis wälja: Tema ei olla Orru mõisa põllalt kaeru loomadele ette wiinud.
Otsus: Kogukonna kohus moistis Kustaw Jootmannile, Jaan Wiedas tunnistuse peale 1 rbl. Orru mõisa walitsusele kahjutasumiseks ja 25 kop. trahwiks Orru walla laeka heaks; Jaan Wiedasele Kustae Jootmanni tunnistuse järele 75 kop Orru mõisa walitsusele kahju tasumiseks ja 25 kop. Orru walla laeka heaks; Lena Link'ile Kustaw Konks'ile ja Aleksander Kansmmanile Jaan Wiedas tunnistust mööda iga ühele 75 kop Orru mõisa walitsusele kahjutasumiseks.
Sellsammal pääwal maksis Ann Bant karjatse Jaan Wiedas asemel 1 rbl wälja, Hendrek Link tütre Leno eest 75 kop. ja 16 Nowembri k.p. s.a. maksis Prido Konks poja Kustaw eest 75 kop., Andrus Kansman poja Aleksander eest 75 kop. ja "Wulbi" koha peremees karjatse eest Kustaw Jootmann eest 1 rbl. ja 25 kop. jähid maksmatta.
Kohtowanem: Jaan Welik XXX
Körwasmees: Aado Maaler XXX
Körwasmees: Juhan Waarmann XXX
Kirjutaja J Täke &lt;allkiri&gt;
Läbi waadatud 19 Decemb. 1888 Kihelkonna kohtu wanema asemel: &lt;allkiri&gt;
