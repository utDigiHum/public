Astus ette Mäewallast Willem Prosowits ja kaewas: Keblast, Kima Jürri Joltop on tedda Luntsi kige al kärristanud ja ütelnud, kedda sa Kidewa kubja lauda peal karristasid, ja minna polle agga mitte sinna sanud, ja ma maggasin Mä mõisas.
Selle kaeptusse peale sai Kimia Jürri Joltop ette kutsutud, ja temma ütles, se on kül tõssi ma kärristasin tedda, agga temma kärristas mind ka nisammati ja pealegi ähwardas temma mind enne weel ja lubbas mulle üks hea nahhatäis anda.
Et Willem Prosowits ja Jürri Joltop on mollemad ühte wisi süi allused ja teineteist kärristanud ja mitte sureste wigga sanud moistis kohhus omma arwamist möda mollemille 1 Rubla trahwi Kebla walla laekase. (§1223)
Kohto wannem Jaan Reints XXX
Kohto kämehhed Jürri Klaos XXX Maddis Pärten XXX
Kirjutaja T. Neider &lt;allkiri&gt;
Willem Prosowits on 1 Rub maksnu
1 Rubl. Jürri Joltop on maksnud
