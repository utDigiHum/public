Astus ette Madis Puur naene Leenu ja räkis wälja: Pühapääwa homikul s.a. 11mal Septembril s.a. kippunud lesk Ann Kiwi, poeg Hans Hubert ja tüttar Leena Kiwi tema tütre Tiu kallale ja tahtnud teda lüa, ta läinud wahele keelma, siis Ann Kiwi löönud ühe leppa puuga teda ühe hobi käepeäle, siis ta Päha, nenda et Pea werd jooksnud ja käsi üsna wigaseks jäänud ja siis lükkanud teda kiwi paranda peale selite maha.
Astus ette Tiu Puur ja rääkis wälja: Hans Hubert ja moisa tallipois Michkel N. läinud toapeäle (peal) kus Lena Kiwi maganud, tallanud tema heinade peal ja wötnud tikkust tulsd üles. Ta üttelnud neile, et se ei kölba pöhkude sees tuld üles wötta, siis Ann Kiwi tulnud enese poia Hansu ja tütre Leenuga ta kallale, Hans hakkanud ta körrist kinni, Ann ja tüttar Leena löönud tale wastu silmi, tema ema Leenu tulnud wahele neid keelma, siis Ann löönud leppa puuga emat raskeste, ja lükkanud ta maha.
Astus ette Ann Kiwi ja rääkis wälja: Tema poeg, tüttar ja moisa tallipois olnud toapeal, sepärast läinud ta toas Tiu Puuriga riidu, ta pidanud Tiut lööma, ema tulnud wahele ja saanud hoobi enese kätte ja lükkanud pealegi ta maha.
Kogokonna kohus möistis, et Ann Kiwi peab selle lömise ja lükkamise ette 3 rubla maksma, mis Leenu Puur rohude rahaks ja haiguse kannatamise ette enesele saama peab.
Kohtuwanem Jüri Silwer XXX
Körwasmees Jüri Ulm XXX
Körwasmees Rein Kiisberk XXX
Kirjutaja A Krein &lt;allkiri&gt;
