Üks tuhat üheksasajandamal aastal Paastu kuu 22 päewal tuliwad Kõrgesaare Kogukonna Kohtu awaliku koosolekule Kõrgesaare walla liikmed: Ado Andruse p. Tarto ja Kustaw Jakobi p. Olesk ja palusiwad üleskirjutada järgmist nende poolt ülesantawad suusõna lepingut:
Mina Kustaw Olesk Jakobi poeg müisin oma wabat koha "Põllumäeselja" kohalt omma päralt olewad ehitused; nimelt elammisemaja: tuba, hoone, hooneotsa kambri, koda, warju alus koja otsas, üs ait, üks laut, keldri, sepa paja, saun ja paargu, üks heina ladu kolmmsada rubla eest, mis suguse raha olen täieste wälja makstud saanud ostja Ado Tarto Andruse poja poolt ja tunnistan, et olen selle kohast prii ilma, et mull miski nõudmmist enam peaks olemma selle koha kui ka koha ostja Ado Tarto wastu. 
Kustaw Olesk kirja ei oska.
Kustaw Olesk XXX
Kohtuwanem: T Küttim [allkiri]
Sellega tunnistan, et olen Kustaw Jakobi p. Olesk käest ostnud tema "Põllomäeselja" koha pealt seal olewad ehitused mõnda kui Kustaw Olesk on  seda ülesannud, ja temmale nõutawa summa raha wälja maksnud.
Ato Tarto [allkiri]
Selle kauba lepingus nimetud inimesed Kõrgesaare wallaliikmed Kustaw Jakobi p. Olesk  ja Ado Andruse p. Tarto on Kogukonna Kohtule palelikult tuntud ja õiguse omanduslikud aktide tegemise juures, mis kohus oma allkirjaga tunnistab.
