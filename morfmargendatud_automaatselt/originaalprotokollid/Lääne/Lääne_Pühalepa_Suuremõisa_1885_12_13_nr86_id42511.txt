Kaebas Andrus Waldmann /Kustawi asemel/ et Kustaw Waldman 2. skp. on Hawalt oma heinamaa pealt Pett. Kaljo 2 hobust ja Kustaw Allik 2 hobust oma heinamaa pealt leidnud, ning kohtumaea juure kinni wiinud - nõuab 50 kop. hobuse pealt. /Allik 1 hobune põle kinni and./
Peet Kaljo ja Peter Allik ütlesid, et nende hobused on õuest wäljaläind, mis tagaotsind, ja talised mulgud juba maas olnud, ja nenda Waldmanni heinamaa peale läind, mis ju põle meeleli tehtud, waid kogemata juhtunud ja et ju talised mulgud maas olnud.
Kohus manitses pahanduse eest hoida ja peawad maksma, et suurt kahju jo põle tehtud § 1118,3 järel:
Peet Kaljo 2 hobune ette, a. 10 k = 20 k.
Ptr. Allik 2 -,,- -,,- 10 = 20
mis ka äraklaarisid.
