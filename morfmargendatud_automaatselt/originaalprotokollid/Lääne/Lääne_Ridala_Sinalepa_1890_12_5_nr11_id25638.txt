Wanamõisa rentnik Jaan Pihelberg kaebas: 9 aastat tagasi maksin mina talitaja põlwes 3 rbl. Hans Kahm eest Hakenrihtre juure trahwi raha wälja ja seie ajani ootan tagasi maksmist ikka.
Kostja Jaan Kahm ilmus ja tõendas: olen 3 rbl. wõlgu Jaan Pihelbergile ja maksan seda tulewa kohtu päewaks ära.
Otsus: Hans Kahm peab 3 rbl. Jaan Pihelbergile 2 nädala aja sees ära maksma.
Eesistuja Kustas Suurküla &lt;allkiri&gt;
I Kohtumõistja Mart Sök &lt;allkiri&gt;
II Kohtumõistja Mihkel Reets &lt;allkiri&gt;
III järjemees
Kirjutaja &lt;allkiri&gt;
3 rbl. makstud 10mal Detsembril 1890
