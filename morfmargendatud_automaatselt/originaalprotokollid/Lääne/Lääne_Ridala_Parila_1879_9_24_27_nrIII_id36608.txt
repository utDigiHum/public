Kaewas Reino Rein Reints nenda, et temmal on üks wanna lammas metsas haikeks jänud ja Wannakubja wannaissa on se kord karjas olnud ja kui on arwanud tedda surrewad, siis on temma tedda weel tapnud.
Selle peale sai Hants Wann ette kutsutud ja temma ütles et temma sedda on teinud agga mitte ülle kohto pärrast waid et temma on üsna surremas olnud.
Nüüd arwas kohhus sedda kahjo poleks agga Hants Wann palwe peale leppid Rein Reints nenda et Hans Wann maksan 5 koppi selle nahha eest Rein Reintsile.
Kohto wannem: Jaan Jünter XXX
Kämehhed: Jürri Jürjer XXX
Järjemees Mihkel Reints XXX
Kirjutaja T. Neider &lt;allkiri&gt;
