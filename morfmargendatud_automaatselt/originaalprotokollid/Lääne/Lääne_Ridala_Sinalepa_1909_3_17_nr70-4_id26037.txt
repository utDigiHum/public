Sell üks tuhat üheksa sada üheksamal aastal seitsmeteist kümnemal Märtsi kuu /1909 a. 17 Märtsi k.p./ Wõnnu walla talupoeg Mart Ado p. Jõkk 74 a. wana, kes kohtule isikulikult tuntaw on, palus kohtu lepingute raamatusse oma suusõnalist järgmist tõstamenti ehk wiimast seadmist üles wõtta.
Min Mart Ado p. Jõkk Wõnnu mõisa järele olewa Uudo-Hantso koha No 3 koha rentnik, annan oma talu koha ostu õiguse oma poja August Jõkk'le, selleks et ma Wõnnu mõisa ega oma poja August Jõkkilt miskit tasu koha ära andmise eest ei nõua.
Peale seda annan kõik oma liikuwa waranduse mis mull selle koha peal on oma pojale August Jõkkile päris omanduseks selle tingimisega, et ta peab mulle ja minu abikaasale tarwiliku kattet, peawarju ja ülespidamist kunni surmani andma ja heasüdamelikult minu ja mu abikaasaga ümber käima ja wiimaks meid maha matma.
See sinane testament on minu enese selge meele mõistuse ja oma enese tahtmise järele kirjutatud ja peale seda ette luetud.
Tostamendi tegija umbkirilik, tema suu sõnalise palwe peale tema eest kirjutas alla: F. Selg &lt;allkiri&gt;
Tunnistajad: August Reets &lt;allkiri&gt; Fridrich Selg &lt;allkiri&gt;
President: J. Koplik &lt;allkiri&gt;
Liikmed: M. Jaksman &lt;allkiri&gt; K. Selg &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Olen selle lepingust kohtu poolt ustawaks tunnistatud ära kirja wälja saanud 17 Märtsil 1909 a.
XXX Mart Jõkk
President J. Koplik &lt;allkiri&gt;
