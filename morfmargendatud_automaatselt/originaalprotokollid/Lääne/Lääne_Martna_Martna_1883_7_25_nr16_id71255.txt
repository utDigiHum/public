1.
Kohtu ette sai Mari Maritoph 63a.w. kutsutud ja temalt pühapääwa homikune juttustus J. Laithalile, päritud; Taa tunnistas oma juttu Jüri Timmi roika ladumise üle maanteele kohtu ees tõeks.
Jüri Timm oli aga säätud pääwal kohtu käsu wastu pannes ilma passita Tallinna jooksnud; sellepärast sai kohtumõistuse mööda temale sääduslik kiri Tallinna politseile saadetud ja palutud täda luurimise waral kinni wõttes kõwa wahi all oma walda saata.
