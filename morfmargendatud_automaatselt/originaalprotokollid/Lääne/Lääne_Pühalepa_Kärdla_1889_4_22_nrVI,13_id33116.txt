Koos ollid: Tallitaja A. Kusik abimehed A. Niman ja M. Laasma seatud kohtu päwa pidamas.
Kaebas Johanes Waar et Marri Kõllo kaubles mo naese käest palito 9 rubla ette 2 astad tagasi 7 rubla maksis ärra 2 rubla mis weel maksta on ei tahha äramaksta.
Mari Kõllo rägis ma ollin weel lapselik ja ostsin selle palito 9 rubla ette ja 2 rubla ei olle weel maksnud, se palito ei olle sedda wäärt seal ollid augud sees, ja sellepärast ma ei tahha sedda 2 rubla enam maksta.
Otsus Et Marri Kõllo ei olle kohhe sedda palitod taggasi winud kui ta ostis waid 2 astad omma ka pidanud ja kannund peab Waari need 2 rubla ärramaksma 2 kuu aja sees.
