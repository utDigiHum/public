Mari Wäli kaibas, et karjased, Kangru Jüri Walge, Saueaugu Kustas Liidemann ja Seppa tüdruk on seda pealt kuuland, kui need poisid teda sõimanud olid kelle mõisa padrikud sa wahid, matahan so kuradile näidata.
Kaebtuse alune Jüri Walge sai kohto poolt päritud kuidas see asi olnud oli, tema ütles et nemad olid aea äärest läbi läinud, ja ei ole kedagi sõimand, nende loomad olid Ehmja metsa läinud ja naad olid weel selle Mari Walile leiba annud.
Saueaugu Kustas Liidemann ütles et tema ei ole sõimand, ta oli üle aea tulnud waljastega, ja siis oli see Mari Wäli weel waljad tema käest ära wõttnud ja üttelnd, mis sa neist waljastest pääwa otsa taga wead anna naad siia ma panen naad kotta warnaotsa.
See tüdruk Seppalt ütles et tema ei ole kuulnud kedagi sõimawad tema oli weel selle Marile leiba annud, ja tema pole kedagi neile lausund.
Naad leppisid kohtu ees ära.
