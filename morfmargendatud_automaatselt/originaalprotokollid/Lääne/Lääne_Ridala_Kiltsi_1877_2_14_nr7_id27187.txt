Ette kutsuti Jaan Martini ja temma naese Liso kaebtusse peale ette tunistusseks Wana issa Jaan Martin, kas tõssi on et poeg Rein omma wenna Jani naest 2 hopi pähhe russikul löönd.
Wana isa tunistas:"Poeg Rein andis 2 hopi russikul wasto silmi, se on tõssi, aga poeg Jaan lõi taggasi neid hopa 2 kord witsaga mis tal peus olli. Aga et nemmad enne rido wäga waidlessid ja Rein käskis Lisut omma issa perse pugeda, selle eest lõi Liso enne käega reino wasto silmi.
Sisse kutsuti Jani naene Liso [Kaebaja] ja kaebtusse kandja Rein.
Liso Jani naene aias omma lömist, ennese pealt ärra lükkamisseks mis Rein rüssinal temma peale tulnd kui temma leiwasid ahjo panund, tullised sööd nurkas lassus ning temma olnd palja jallu, et kartsin tullise sütte sisse ennast lükkatawad, mis ommeti pärrast wallo oimantussega sinna kukkusin.
Nenda ei aiand rein tagasi mitte omma löma.
Kohto mehhed maenitsiwad neid, et peawad ni suggu äbbematta wastamisse rio eest endid hoidma ja äbbenema omma perrenaest lüia ning rielda omma wenna Janiga kes Suredoma maias keige kohtu kirjades peab perremees ollema, ja ka täna seda peab kuulma, et temma on perremees ja Wend Rein sullane ja temma naene kodakonne ehk tüdruk ka wana issa on wanna sullane.
Rein peab et tema ni kolwato olnd trahwi maksma 1 Rubl walla laeka.
Kohto mees Mihkel Poots
I Kõrwamees Jürri Rabba
II Kõrwamees Mihkel Mikkas
Kirjotaja: Jaan Grünär &lt;allkiri&gt;
