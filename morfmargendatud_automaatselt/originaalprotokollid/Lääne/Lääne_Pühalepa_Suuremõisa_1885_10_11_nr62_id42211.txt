Suuremõisa walitsuse poolest kaebati, et need alpool nimetud, on omad loomad enam kui üks kord käsuwastu, lasknud mõisa Leerimetsa heinamaa peal, Loja põllal orrasel ja kärbaste kallal käia.
Loome peremehed kostsid, et loomad on aga selle karjamaa peal kord käinud, mis mõis nende walla karjamaast sisseteinud. Neil käind:
Simo Pail			1			hobune		Juh. Aro			2+3			-,,-		Peet Pähel			2			-,,-		Juh. Singi			2			-,,-		Kust. Mast			1			-,,-		Peet Alber			1			-,,-		Prido Ala			1			-,,-		Jüri Alber			1			-,,-		Juh. Liit			1			hobune		Peet Sep			1			-,,-		Karl Wälja			2			-,,-		
Igaüks, kelle loomad kahju tegemas käinud peawad S. §1118, punk 3 järel 10 kop. looma pealt wallalaeka /nagu mõisaw. poolt see lubatud/ heaks trahwi maksma.
