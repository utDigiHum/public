Ette sai otatud Joesu walla meest Mart Kuutmanni, Kes Mihkel Lõpp kaebtusse peale piddi ette tullema.
Mihkel Lõp olli omma ärra warrastud wankert, mis kewade pari päiwi enne Lihhola laata Lõppe sauna Kuri alt warrastud ösi, leidnud Mart Kuutmani käest tö jures Hapsallo linnast, kohhe ärra wõtnud polegel soltati abbiga; M Kuutman ütlend Lihhola ladalt ostnud 23 Rubla ette.
Mart Kuutmann ei tulnd mitte ette; õlli kül tallitajast käsk saand.
Asja jäti tullewa korraks.
No 20
