Tullid kaebama Wönno mees Madis Kem ja Kirrimae walla mees Juhhan Kahem: Meie tulli Hapsalust töölt ja laksime Wönno Juurika Körtso sisse, siis saime seäl hirmus paljo peksta mölemad, kedda meie issi ennam ei möistnud luggeda, sest meie ollime ka nattuke wiinast wässinud. Ja muud innimessi seäl körtsus ei olnud, kui Uemõisa walast Hans Otsmann ja Wönnust Willem Uesild, Johan Jur ja Peet Arri ja Mihkel Arri.
Kohto ette tulli Uemõisa walast Hans Otsmann ja tunnistas: Teised poisid ei olle nende liggi puutunud, Johan Jur üksi peksis neid. Siis tunnistas Johan Jur minna ollin õues, siis tulli Madis Kem ja sõimas mind ja minna lükasin teda wasto maad ja Johan Kahem aias mind taga ja kukus issi maha. Pärrast tullid möllemad minno kallale, ja et naad joobnud ollid ei suutnud nemmad mulle ennam häda tehha, kui minna neile teggin.
Kohto möistus: Et Madis Kem ja Johan Kahem ni joobnud peaga ollid, sellepärrast peawad sellega rahul ollema, mis on saanud, Agga Johan Jur saab meeletuletusseks, et ta nenda ennam ei te 25 hopi witso ihhunuhtlust selsammal pääwal.
Kohtowann: Willem Kel
Körwasmees: Jaan Nano Jürri Naab
Kirjotaja: Joh. Reinans &lt;allkiri&gt;
otsus on täidetud
