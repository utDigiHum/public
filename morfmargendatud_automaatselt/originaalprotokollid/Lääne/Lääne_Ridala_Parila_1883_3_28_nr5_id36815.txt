Astus ette Jõesu Liiwa kõrtsu tütrik Kersti Nee ja kaebas et Jaan Well on tema Jakki katki kiskunud ja ei taha temale seda kahju tasuda.
Astus ette Jaan Well ja ütles wälja et tema on wallatuse läbbi seda kogemata teinud ja wõi mitte ütleda et se temma läbbi ei olle sündinud.
Nemad lepisid ärra ja Jaan Well maksis kahju tassumist.
Kohus moistis et peab 50 kop walla laeka trahwi maksma T.r.s. §1270
Sellepärrast et mitte kohe ei tahtnud kahju ärra tassuda wait kohtu läbbi 
Kohtuwanem Jaan Jünter XXX
Kõrwamees Juhan Krabbi XXX
Kõrwamees Madis Top XXX
Kirjutaja M Kentmann &lt;allkiri&gt;
50 kop trahw.
kohe makstud.
