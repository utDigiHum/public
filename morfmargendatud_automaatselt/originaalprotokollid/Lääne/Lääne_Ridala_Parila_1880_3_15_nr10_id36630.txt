Ette astus Parrilast Jani perremees Jaan Lamp ja kaewas omma sullase, Ungru wallamehhe Hans Heigel peale; et on 1879 süggise, aasta kauba teinud, Mihkli päwast, ja nüüid on temma 9mal selle ku pawal ärra läinud ja aasta polele jätnud, ja palka on temma jubba kätte sanud 14 Rahha palka 2 Rub. 4 kop. pearahha ja 1 kuub wabriko krässitud ridest 1 poolwillase ridest west ja 1 paar kindud, ja tülli egga pahhantust ei olle meil temmaga ühti olnud mis tema woiks kaewata.
Selle peale sai Hans Heigel ette kutsutud, ja temmal seadust ette loetud, agga temmal ei olnud, selle wasto üti ütlemist ja ei olle mu wigga pärrast ärra laind kui wannemade pärrast, et nemad koddo abbi tarwitawad.
Kohhus teggi otsuseks nenda: Et Hans Heigel, ei olnud Tallorahwa seaduse §468 ühhegi punkti peale wastata sundis kohhus, Tallorahwa Seaduse §456 möda, et temma peab omma aasta täis tenima, kunni Mihkli päwani, kui tema on akkand.
Kohto wannem Jaan Jünter XXX
Kohto kämehhed Jürri Jürjer XXX Juhhan Krabbi XXX
Kirjutaja T. Neider &lt;allkiri&gt;
Hakendrihtri kohto läinud
