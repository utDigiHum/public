Hellamal 14 Detsember 1884
Sai korrajärel kohtupäew ärapeetud, kus koos olid Peakohtumehe ette J. Wilo
kõrwasm. J. Leht
-,,- -,,- P. Paes
Peakohtumees oli oma asjatoimetamise päras Tallinnas.
Sai ettewõetud, mis Mõisawalitsuse poolest Nr 114, a-ü al metsawarguse üle sissekaebatud.
a. Joh. Masil ütles nagu enne, et põle warastand Metsawaht Jäger ütles ka nagu enne,- et põle kinni saanud, ega tea seda selgeste, mis aga arwanud.
Kohus mõistis, et J. Masil sest 5 rbl. maksust lahti on, sest et waht ei julge ta peale seda ütelda. Jäger õiendagu isi seda, te segasti ülesannud,- mõisawalitsusega.
b. Nr 114 f alt Peet Post ütles, et hobuse wabriko jõkke aeanud,- puud pealt äraladunud ja hommiko puuaeda wiinud ja äraladunud.
Waht Ptr. Jäger ütles, et ta Posti hommiko puu aeast puid ladumast leidnud ja mõtlend warastawad, - ei tea selgisti
Kohus arwas, et Post sest 2 rbl. maksust lahti on sest et Jäger tunnistus kahtlane on. Seda mitte selged ülesandmist seletago metsawaht Jäger isi mõisawalitsusega.
c. Nr 114 g all ütles Madli Arbu, kes ees oli, et ta ei ole koguni Peet Alber käest puid ostnud ega tunnegi seda meest ja et ta waese inimesel korraga ei ole koormad halgu jõuda osta olnudgi.
Wahimees Ptr. Jager ütleb, et kui jälgi kaude järel läinud, on koorma halgu Arbu õuest leidnud ning taga jutustanud, mis Arbu wastu aeas, mis tõsi. Arbu ütleb, et ei mälleta seda. P. Alber ütleb, et on seekord 1 Laupäew wabriko innimest omale warule toomas käinud ja mitte puid warastanud mis peale Jäger lausus, et ta põle ka mitte Albert puukoormaga kättesaanud.
Kohus mõistis, et Jäger põle Albert puudega kätte saanud, on lahti, aga Arbu, kelle õues Jäger tunnistust mööda warastud puud olid, maksab see 2 rbl. ära ehk andko warga, kes tal toonud üles, kes siis maksab ja ta prii.
d. Nr 114 h alt. Metsawaht Ptr. Jager ütles, et M. Ristmägi ta puudega kinni wõtnud, aga põle teadnud, ega uskuda wõind, et ta kooli puid winud, mis mõisa nagu warguse ülesannud. Ristmägi õtles, et need puud sandi peo päras wiimata kooli jäänud, mis praegu kodu on.
K. m. et Jäger eksituse läbi Ristmägi puud, mis koolile tõi, nagu warguse üles andis, saab 2 rbl. trahwi maksust lahti, ja wiigu need kooli puud kohe ära, mis Jäger mõisa walitsusega isi õiendagu, et nagu warguse seda ülesannud, mis mitte seda ei ole.
e Nr 114 i alt. Johan Lõppe ütles, et põle puid warastanud, ega Madli Kastanil mitte wiinud.
Wahimees T. Wermes ütles, et ta põle mitte Lõpet näinud ega kinni wõtnud, aga tunud need saetud puud ära, mis naese õues olnud. Ta poeg Peet Wermes näind, kui Lõppe ja Andrus Silm naese wärawas koormatega olnud. Lõppe ütleb, et on üksik mees ja üksi saagida ei sa puud maha. Madli Kastan ütles, et üks mees tale 2 koormad agu toonud, keda põle näinud ega tunnud, et haige olnud maas. Ta lapsed on 50 kop. nende 2 koorm hagude ette mehel wäljamaksnud.
Pooleli. Peet Wermes ette.
f. Nr 114 j alt maksis Andrus Kübar omale mõistetud puude raha
3 rubla 50 kop. (A. Tõke läbi) ära.
g. Nr 114 i alt. maksis Johan Kokla oma puude trahwi raha
2 rubla ära.
h. Nr 114 m. Wahimees P. Wöö ütles, et P. Merjama on 2 puud raijunud, mis tuul maha aeanud- ja 1 puu, mis wäljas saetud ta ligi, on nad Tikerpuga ka ta rehnungi peale arwanud. ütleb et põle seda mitte warastanud. Ta isi on wabrikus wahiks öösil ja ta lapsed on öösil kuulnud, kui 1 puud saaginud, ja naene tal haige ka olnud, kuis ta wõib seda maksta, mis wäljas saetud.
Wöö ütleb, et terwelt on ta kaks puud (raijund) sagind. Ta ütleb, et Tubala Tasane on ühe aeru puuks wiinud, mis näinud on.
Kohus mõistis, et Andrus Tasane saab ette tulla ja P. Merjama ka, kui raijund, maksab 80 k. Peet Wöö, mis ühe puu hukka ülespannud ta peale, õiendagu mõisawalitsusega ehk maksab 80 k ja Peet Merjama maksab 1 puu ette 80 k. Merjama maksis ka see 80 kop. ära.
i. Nr 114 n. Tomas Wermes wahimees ütles, et ta on luba annud, kui A. Heinapuu mõned teiwad tellinud, aga ta on 5 puud wõtnud mis peale Heinapuu ütles, et mitte enam põle kui need 2 lubatud puud mändi wõtnud ja aeaks teinud - ja muist oma heinamaa pealt raijunud ja aeaks teinud.
Kohus mõistis, et Wermes need isi oma woliga lubatud 2 puud peab maksma s.o. 1.20 k 3 mis tingimimise al, maksab Wermes 60 30/ 90 -,,- Heinapu 60 30/ 90
j. Simo Mägi ütleb, et 2 puud raijunud, aga wahimees T. Wermes ütleb, et ta on 3 raijunud. 1 tingimise al.
Kohus mõistis, et kui Mägi lubata need 2 raijus siis wois kül ka 3 raijuda, mis peab wäljamaksma a. puu 60 kop. teeb 1 r. 80 kop.
k. Nr 114 p. Peet Wöö ütles, et ta on nii ülesannud: enne wana peremehe aegus, kus Simo Erk on raijutud, aga ta põle üttelnud, et Erk just raijunud. S. Erk ütleb, et krundi pealt, kust olnd tehtud, on raijunud, aga mitte mõealt.
Kohus mõistis, et wahimehed Wöö ja Tikerpu arwamise järel Erk jäuks kirjutand 3 puud, aga ta põle raijunud, mis ka(ha) wahimehed põle näinud jääb Erk sest 2 r.40 kop. trahwi maksust lahti. ja wahimehed klaarigo segast ülesandi mõisa walitsusega.
l. Nr 114 s. Wahimees P. Wöö ütles, et Peet Kiil on 4 puud raijunud, ja nende Kiil poisid ta wasto seda rääkind. Kui waatma läind, on puud põletud olnd. Mihkel Kiwer näinud, kui Kiil puud raijunud.
Pooleli. M. Kiwer tunnistuseks ette.
m. Nr 114 ss. Jüri Raea (poeg) ütles, et oski on raijunud, aga mitte jäme puid. Wahimees Peet Wöö: jälgest on järele läinud ja seekord 2 kändo leidnud, mis ka Peet Otsa ütelnud, et Raea sealt tulnud - Otsa käest sai küsitud, ütles, et Raeal on oksad peal olnud - kuiwad puud. Wöö jääb kindlaks, et need puud raijunud.
K.m. et Raea peab wahimees P. Wöö tunnistust mööda need 5 puud trahw 4 rubla maksma, mis ees poole weel kuulatud saab.
n. Mõisa walitsuse poolest sissekaebatud, et Andrus Karjamets 2 koormad halgu warastanud ja wabriko alewi rahwal wiinud, - tal 4 rbl. maksta. Juhan Karjamets ütles, et kes seda näinud ja teda kinniwõtnud, kui warastanud. Wahimees Ptr. Jäger ütles, et kui ta oma isaga puu koormad on mahalaksnud ja on neid uue maea ees kinni wõtnud, ja et nad warastanud on, mis wastu Karjamets aeab.
Kohus mõistis, et metsawaht Ptr. Jäger, kes wannutud mees, tunnistab teda kinni wõtnud, peab 2 koorm. halu trahwi 4 rbl. maksma moisa.
o. Nr 114 ä. Metsawaht Siim Jõe tunnistas, et need 3 jämedad puud, mis Willem Pärisma raijunud, nende karjastest raijutud puude (v. Nr 114 ö al) seas ka kirjas, aga mitte isi äranis põle arwata. W. Pärisma ütleb, et 1 puu raijunud.
Sellejärel ei ole Pärismal seda 3 puu trahwi 2 r.40 kop. maksta.- Isi asi on kui tal Nr 114 ö al (karjaste trahwi) seas maks tuleb.
p. Nr 114 ö. Metsawaht Siim Jõe ütles, et ta on seda üles nende karjaste käest saanud, kes puid rikkunud, nagu nende nimed Nr 114 o al kirjas on. Olid ees: Heinapu, Pärisma, Mägi ja Paljasma. Muist karjasid puudusid, seeüle jääb pooli.
Et muud seekord ette ei tulnud, sai koosoleks lõpetud.
