Sisse kaewas Rein Otsa omma naese pärrast, mis Ahli Madise koer 10mal August küllawahes maandi peale salliga järrel jooksnud ja jalla säere maha närrinud, ni et liha tük maha kukund ja sealt sai hobusega teda ärra widud ja nüüd ei sa naene omma jalgega mitte ouegi."
Koera perremees Madise Juhan Kem lubbas kohtu ette, et temma rohtude raha maksab ja Reinole abbi annab, esiteks 1 Rubl. ja kui assi pitkemale lähheb siis weel.
Kohto mehed manitsesid selle naese eest hoolt kandma. aga et koer ärra saab surmatud ei tohi sa kellegele tedda müa.
Kohto mees: Mihkel Poots
I Kõrwamees: Mih. Warn
II Kõrwamees Ado Rannus
Kirjotaja Jaan Grünär
Polel. No 57ni
