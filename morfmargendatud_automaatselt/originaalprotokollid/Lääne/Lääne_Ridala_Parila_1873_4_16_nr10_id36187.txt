Kohto korra pärrast ollid, Parrila Kebla ja Riddali kirriko walla koggokonna, maias kohtomehhed koos.
Ette astus, Mäewallast, Kristow Prosowits, ja kaewas, et temmal on, Keblast palka sada kus temma on perremeest teninud: Mihklijanilt: 1 Kuub ja 2 särki ja pearahha 7 Rubla 78 koppik. Ja Hindrikult jälle 1 wak Rukkit, ja 1 Rubla 78 kopp. pea rahha
I
Sai Kristow Prosowits kaeptusse peale, Mihklijäni perremees Rein Reiser, ette kutsutud; ja kohhus küssis: miks pärrast sinna polle omma sullase, palka ärra maksnud? woi kas sul on temma peale süüd tõsta? Rein Reiseril, ei olnud, omma sullase Kristow Prosowits peale ühtigi kaewata; waid tunnistas kohto ees, et temma on heaste teninud ja on minno omma süi, et se maksmatta on.
Nüüd sundis kohhus, et Rein Reiser peab omma sullase wõlgo jänud palga täieste ärra maksma.
Se asso on seletud.
II
Sai Hindriko perremees Andrus Okkapu ette kutsutud: Ja kohhus küssis: miks sinna ei olle omma sullase palka mitte ärra maksnud. Andrus Okkapu wastas: temma tenis mind ühhe aasta ja palk on maksmatta ja sellepärrast ma ei maksnud et temma teise aasta peale kauples ja lubbas, weel aasta tenida ja ei olnud ennam kui seitse näddalid sedda teist aastat teninud.
Andrus Okkapu wasto räkimmisse peale sai Kristow Prosowits ette kutsutud; ja temma ütles, et nemmad polle mitte aasta peale kaubelnud.
Selle peale tunnistas kohto wannem Jaan Reints, et temma on seal jures olnud, kui kaup on tehtud ueste ühhe aaasta peale.
Nüüd moistis kohhus et Andrud Okkapu peab Kristow Prosowits enne täis tenitud, aasta palga wälja maksma, 1 wak Rukkit ja 1 Rubl 78 koppik Pearahha.
Ja Kristow Prosowits peab Andrus Okkapule, tallorahwa seadusse §447 möda, et temma on enne aastat ärra läinud, sedda kahjo tassoma. 1 Rubla 78 koppik.
Kohtowannem Jaan Reints XXX
Kohto kämehhed Jaan Kreis XXX Jürri Klaos XXX
Kirjutaja T. Neider
Kohhus on moistnud ja kohto moistus on täitetud.
