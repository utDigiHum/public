Maddis Niemann (wabriko päwa wahhimees) kaebab kohto ees, et 3mal Sept: on Prido Tammik joobnud peaga wabriko ouest läbbi tulles, tedda hakkanud seal sõimama ja wimaks lubbanud tedda weel jökke wissata, ilma, et temma talle enne ühte sannagi olleks lausunud.
Prido Tammik tunnistab, et temma sedda kül jobnud peaga on teinud, tunneb omma süüd ja lubbab ennast parrandada.
Et ta nenda omma süüd tundis ja allandlikkult andeksandmist pallus - lubbas M. Niemann temmale sedda sekord andeksanda.
Ja sellega olli otsus tehtud.
