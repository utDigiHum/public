Ette astus: Jaan Jaaks ja kaewas, et temal on aasta eest, koplist kaks Tamme ärra warrastud, ja nüüd on Uetoa Kaarli Wiispärgi aida pealt pöhkude alt, weel selle tamme tügas leitud, mis just selle kenno otsaga just ühte läks, ja Karel Wiispärg ütles, minna tõin need Mämõisa Kubja Kustase kääst ja pärrast kui sai se kenno otsa pandud siis ütles minna ei tea kust se on sia on tulnud.
Nuud sai Karel Wiispärg ette kutsutud ja temma räkis selle kaeptusse wasto nenda: Kui Lepiko Prido neid roikud mahha aeas, siis leidis selle Tame tükki sealt roikaste wahhelt, Agga minna polle kül mitte tedda sinna pannud sest ma ollen, Jürri päwast sadik ärra olnud, sedda woib olla sinna kis tahhab winud.
Sest et se Karel Wiispärgi maiast on leitud jäi temma süi alluseks; Nüüd pärris Moisa Herra, nende ärra kaddund kahhe tamme eest 7 Rubla (seitse Rubla) ja peale selle moistis Kohhus omma moistmist möda Karel Wiispärgile 2 päwa wangi trahwi.
Kohto wannem Jaan Reints XXX
Kohto kämehhed Jürri Klaos XXX
Kohto järje mees Willem Luntsi XXX
Kirjutaja T. Neider &lt;allkiri&gt;
Kohto moistus on täitetud 7 Rubla Maks ja 2 päwa olli wangis
