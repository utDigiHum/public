Kohto ees kaewab Ann Tomingas, et Liso Silmer on tedda ilma süta kanga wargaks teinud.
Liso Silmer ütleb et kahhe aasta eest temma kanga otsast, mis plekimas olnud 12küm künart riet ärra leigatud ja Ann Tomminga mehhe Emma Marri ütlenud et meie Annel on üks wodi linna, mis sedda modi ridest on kui sinno kangas olli ja lubbanud näidata.
Liso läinud watama ja arwanud temma ride töest ollewad; pärrast ride tükkide ühte sõlminud ja Ann Tomminga linnast 21 lõnga järrele jänud, Siis on Ann Tommingas omma mehhe Emma peale kippunud miks tühja juttto minno peale teggid selle peale on ta läinud Liso Silmeriga ridlema et ta polla sedda nenda räkinud ja Liso Silmeri meest Reino kaks korda lönud russikaga, mis ta isse tunnistab.
Kohhus teggi otsust nende rio asja ülle nenda
I Et Liso Silmer pühhapääw hommiko ilma Tallitajatta omma warguse warra otsima läinud ja siis ridlema ja wanduma, peab üks rubla trahwi maksma ja Marri Tommingas teise rubla et Rein Silmerid kaks korda russikaga lönud ilma asjata, agga Rein polle mitte wasto lönud, waid andis kohto selletada.
Koos ollid kohto wan. Mihkel Sarijs XXX
Käemehhed Willem Otsberg XXX Jürri Triksberg XXX
Kirjotaja: A J Martensohn &lt;allkiri&gt;
