Ette sai wõetud Andrus Kork ja ta naene Anna, kes mitte selle naese tahtmist möda omma mehhega ühhes ei tahha ellada, waid omma mehhest püab eemal olla. Kihhelkonna kohhus möistis 1874 18 Tetsembril, et Anna peab Andrusele, nüüd, kui ta mitte ühhes ei tahha ellada, maksma 30 Rubla, selle krami ette, mis Andrusest temma kätte jänud.
Ja et naene kogguni ei olle tännini mehhe liggi annud, sellepärrast nöuab se mees nüüd walla kohtu üllekulamise läbbi, selle üllemal nimmetud 30 Rbl naese kaest kätte.
Andrus on jubba Anna käest ärra tonud üks lehm mis kohtuwannemad 15 Rubla wäärt arwawad, ja Andrus tunnistab weel naesel ollewad üks lehm ja üks lammas.
Anna agga ütleb, et temmal ennam lehma egga lammast ei pea ollema.
Kohhus annab neile nõu ühhes ellama hakkata ja leppiwad weel teineteisega selle lootusega, et ellu ajaks ühte ellama jäwad.
Kohtu wannem Willem Nothmann
Körwamees Andrus Lehmloch
Körwamees Gustaw Thalheim
Kirjutaja J. Simson &lt;allkiri&gt;
