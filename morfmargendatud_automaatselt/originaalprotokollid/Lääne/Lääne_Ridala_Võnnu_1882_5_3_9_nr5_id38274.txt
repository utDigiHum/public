Teiseks tuli ette Wõnnu mõisa metsawaht Mihkel Kölli ja rääkis kohtu ees nõnda:
Tanawu talwe, se on 4 Februaril 1882 a käisin mina, oma ammeti järel, mõisa metsa järele waatamas ja leidsin et kolm tamme olid maharaiutud ja ärawiidud. Teisel päewal läksin neid warastud tammesi otsima ja leid neist kaks tükki Kirimäe küla Prooso õuest, üks oli 10 tolli ja teine 5 tolli tüükast paks, mis Ehmja walla mees Kustaw Kemberg oli warastanud. Üks warastud tamm, mis tüükast 10 tolli paks oli leidsin Nihka küla Mardi kambrist, mis Kirimäe walla mees Kustaw Kelmann olla warastanud. Siis läksin ma seda leitud wargust mõisawalitsusele üles andma. Mõisawalitsuse käsu peal andsin ma tamme wargadele käsku et nemad pidid tammed mõisa tooma ja nemad tõid. Ja nüüd pean ma mõisa walitsuse käsu peäl seda kohtusse andma, et süüalused saaksid trahwitud. Pealegi olid need tammed saega maha wõetud.
Ette tulid tamme wargad Kustaw Kemberg ja Juhan Kelmann, Siis sai metsawahi kaebusneile ette loetud. Nemad ütlesid see kaebdus õige olewat ja tunnistasid endid süüaluseks.
Et see warguse trahw suurem on kui walla kohus seaduse järel wõib mõista, see pärast jäeb see trahwi mõistmine, mõisa walitsuse soowimise mööda, hakenrichteri kohtu hooleks. Selle üle sai täna siit protokoll hakenrihteri kohtulle wälja kirjutud ja ära saadetud.
Kohtu peawanem Willem Lien
Kõrwamehed Mart Jöeg Adu Salme
Kirjutaja J. Klanmann &lt;allkiri&gt;
Kohtu päew sai nimetud ühe nadali prst.
