Ette tulli sisse Kaewama Kiltsist Lesk Marri Rabba poeg Mihkliga: et Nende küinest heino ärrawarastud 1 Koorm ei tea millal; aga Joulo nädal selle pühapäwa tõin heinad sealt (järwe takka) ärra. Kuhhi jäi weel, esmaspä (Joulo Rede) järele piddin minnema. Läksin järrele (hommiko) olli ärra widud! Keik siamale otsitud ja kulatud. Nüüd on ni paljo arro sadud, et Tomajani Mihkel Jänt sel esmaspä ösi ouest heino aiand laudile, Kui Sõero Juhan Madsik seal käind õlle käärma tõrt pallumas homikust ööd. (ja ei saand tõrt) Seda räkis Juhani naene An 8mal Januar ommas toas pealt kulas rätsep Reinut wakker, kes seal tööl olli kassukast tegemas.
9mal Januar Kulasin Juhan Madsiko oma käest, kes hopis teisite räkis; et pühade wahel tema heino näind Mihklit aiawad, mis Wõlast toond, aga et Mihkel Jänt süi allune peab ollema:"Kuulsin mina Kondilt.
9mal Januar Kulasin Kondile, räkis Kondi emma, et temma olli Juhan madsiko käest küssind, (mis eni omma laste käest kuulnd), Juhan aga ütlend ennast essimist kord nisugust jutto kuulwad. 5mal Januar (kui wastas) Poeg Mihkel ütleb:"Keisin Lauri Mihkli jures seda jutto kulamas, (mis Kondi perre lapsed wanna kõrtsus Lauri Mihkli suust kuulnud) ja Mihkel Laur ütleb:"Näri (1e Januari) õhto ollime wõeraks Lauri Jani jures: Minna (M. L.), Sep, Mihkel ja Juhan Madsik rääkis, et:"Mihkel Jänt, Hanso poisi heinad ärra toond." Ka räkis mõnne mu asja, mis Mihkel Jänt warrastand, mis ülles tulnd, nago Wigardi Jani aia roigastest, selle leppimissest."
Kohto mehed jätwad tullewa kohtuks keik suud, suus wasto ette kutsuda.
Kohto mees Mihkel Poots
I Kõrwa mees Mihkel Warn
II Kõrwa mees Ado Rannus
Kirjotaja Jaan Grünär &lt;allkiri&gt;
Kaebtus.
No 7 protokol al suud wastamisi
