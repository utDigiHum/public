Astus ette Jaan Ormus ja rääkis wälja: Sügise 1882 a. Adu Wiilmanni sead tustinud tema rukki orase üles, ta noudnud selle kahju tasumiseks ühe wakka rukkit saada ja lubanud sellega leppida; aga A. Wiilmann pole seda seie male temale mitte tasunud. Niisammuti olnud Adu Wiilmanni 3 siga tänawu sui tema rukkide sees, ta pidanud 3 pääwa need enese juures kinni, aga A. Wiilmann pole mingisugust kahju tasumist annud, waid aianud sead muidu ära. Ta noudis selle ette 2 rbl. kahju tasumist saada.
Kogukonna kohtuwanem Jüri Silwer ja körwamees Jüri Ulm tunnistasid waatamas käinud sügise 1882 olewad ja ka otsuseks teinud, et A. Wiilmann selle tustitud rukkima pidanud leikama ja enese pöllust ni palju terwet rukkimaad Jaan Ormusele andma; aga ka seda pöle Adu Wiilmann teinud mitte. Sepärast
Koguk. kohus moistis, et A. Wiilmann selle 1882. a. sügise sigade tustimise ette 1 wak rukkit ja selle tänawu sui 3 sea rukkis käimise ette 1 rubla raha Jaan Ormusele tasuma peab.
Kohtuwanem Jüri Silwer XXX
Körwasmees Jürri Ulm &lt;allkiri&gt;
Körwasmees Rein Kiisberk XXX
Kirjutaja A. Krein &lt;allkiri&gt;
