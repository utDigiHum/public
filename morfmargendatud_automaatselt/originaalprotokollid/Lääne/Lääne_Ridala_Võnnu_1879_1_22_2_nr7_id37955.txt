Tulli tallitaja kässo peale Hans Rü kohto ette omma Pearahha wõlgade pärrast. Temmal on wõlgo: 1875 aasta IIne pool 2 Rubla 74 kop. ja 1873 aastal koolimaja ehhituse rahha 25 kopik. koko 2 Rubla 99 kopik.
Hans Rü tunnistas: Minnul ei olle praego rahha, egga saagi ni pea sedda makstud; waid ma tahhan sedda maksta Jüri päwaks 1879a.
Kohhos lubbas temmaga kannatada ja kui seks ajaks polle makstud, siis saab temma warrast se jägo müdud.
Kohto wannem: Jürri Soone
Körwasmees: Jürri Naab Mihkel Kelt
Kirjotaja: J Reinans &lt;allkiri&gt;
