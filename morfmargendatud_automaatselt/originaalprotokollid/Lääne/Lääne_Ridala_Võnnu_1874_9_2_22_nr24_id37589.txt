Tulli ette Mihkel Linz ja kaebas: Minno pöllal on üks ruki kuhilas ärrasöötud ja hulk kartuhwlisi ärratöngutud. Ja pärrast ollen ma Madise Ado Kullij sead kaks kord sealt pöllalt leidnud ja kätte aianud, ja möisa pöllowaht ütles kaks kord neid wälja aianud ollewad.
Ette tulli tallitaja abbimees Jaan Ollik ja tunnistas: Sedda pahantust nähes arwan ma kahjo ollewad saanud 2 külimato rukit ja 1 wak kartuhwlid ja möisa pöllowaht tunnistas, et Madise Ado 7 zigga ollid sedda teinud.
Ette tulli Ado Kullij ja tunnistas: Minno sead ei olle seal käinud.
Kohtowannem Willem Kel tunistas: Minnagi nägin, et Ado Kullij sead Mihkli Linz pöllal ollid ja aiasin neid wälja.
Kohto otsus: Et Ado Kullij wasto waidleb, on tühine; waid peab Mihkel Linz'ile kahjotassumisseks maksma 3 Rubla 50 kopik kahe nädala aia sees.
Kohtowannem: W. Kelt
Körwasmees: J. Nano J. Naab
Kirjotaja: Joh. Reinans
On täidetud 23mal Septemb. 1874 a.
