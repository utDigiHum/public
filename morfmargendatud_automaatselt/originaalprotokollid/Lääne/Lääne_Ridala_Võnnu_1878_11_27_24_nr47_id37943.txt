Ette tulli Soone Jaan Jög ja ta naene Marri ja kaebasid: Meie ajasime Könno Jaan Prussaki hobbose omma kessa pöllo pealt kinni ja warssi tulli Jaan Prussak hobbose järrele, ütles Jaan Jög, kui mind koddo ei olnud ja olli minno naest löönud ja mahha lükkanud.
Jaan Prussak tunnistas: Minna ei olle mitte löönud, egga mahha lükkanud, ma polle kät ta külge puutunud.
Selle peale tulli Mihkel Kelt naene Anno ja tunnistas: Minna ollin Soone wärrawas, kui Jaan Prussak senna õue läks ja hobbose Soone rihalt ärra töi, agga löömist ma ei näinud.
Ja nenda räkisid nemmad omma tülli asjust mitto ja mitto, kellel ommetegi selget otsust ei olnud, egga ka tunnistust.
Kohto otsus: Et se kohtul awalik on, et Soone ja Könno wabbad rahwas ikka teine teisega tüllis on igga tühja asja pärrast, selle pärrast teeb kohhos otsust, et möllemad peawad trahwi alla jääma. Jaan Prussak saab 15 hoopi witso ja Soone Marri Jög peab 24 tundi trahwi istuma tänna. Soone Jaan Jög ei olnud sel korral koddo.
Täidetud
