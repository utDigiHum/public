Astus ette Michkel Markus ja kaebas, et Hans Alwin ei pea enese honed tema krundi pealt mitte ära koristama, kelle asemele ta enese honed tahab jälle ehitada; teiseks, Hans Alwin ei pea keiki koha pöhudest tehtud sönikud mitte kätte andma ja nisammuti ei ole H. Alwin 4 wakkama rukki körre kündmist temale mitte tagasi maksnud.
Kog. kohus moistis, et H. Alwin peab enese honed Alt-Hansu krundi pealt päris omaniku eest ära koristama, kuhu se enesel woib jälle ehitada; ja nisammuti peab H. Alwin keik söniku, mis made pöhudest tehtud on M. Markusele andma ja sekke 4 wakkama kündmatta rukki körre ette aä 67 kop. ühtekokko 2 rbl. 68 kop. maksma.
Kohtuwanem Jürri Klemann XXX
Körwasmees Adu Weidebaum XXX
Körwasmees Jürri Jaanmees XXX
Kirjutaja A Krein &lt;allkiri&gt;
