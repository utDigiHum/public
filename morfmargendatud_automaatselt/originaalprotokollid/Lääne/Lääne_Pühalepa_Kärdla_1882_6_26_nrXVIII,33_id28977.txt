Koos olid, Tallitaja A. Espak, abimehed: P. Püss ja W. Uusmann seatud kohtupääwa pidamas.
Sel 20m Junyl s.a. õhtu hilja on tislar Mihkel Kuru joobnud peaga õllepoodi ees suurt lärmi teinud ja ähwardamise sanu rääkinud, sellepärast et teda enam senna sisse ei lastud, - aeg oli hilja ja pood kini. - ja sai selle pärast seks ööks wangi kambri kinni pandud et ennast wõis wälja magada, kus ta aga wangi kambris weetoobi äralõhkus.
Mihkel Kuru astus ette, sai tall see asi ettepandud ja otsus tehtud,- Et Mihkel Kuru selle eest et wangikambris weetoobi äralõhkes, maksab 50 Cop. trahwi.
