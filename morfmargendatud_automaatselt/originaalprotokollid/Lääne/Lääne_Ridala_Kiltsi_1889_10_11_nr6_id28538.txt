Urädniku kaebtuse peale, et Paralepa Kõrtsis 14. Sept. s.a. riieldud, käratsetud ja tema peale kukutud, astus ette Jüri Laur ja ütles wälja: Mina olin küll wähe joobnud, aga kas mina nii halwasti seal olin, kuda Urädnik kaebab, seda ma ei mäleta enam. Paha meelt mul Urädniku wastu ei ole olnud.
Kohus mõistis: Jüri Laur peab walla laeka 1 rbl trahwi maksma ühe nädala jooksul, seepärast et tema pole Urädniku keeldu kuulanud, waid on wastu hakanud.
Kohtuwanem: J. Kask
Kõrwasmees: J. Rannus
Kõrwasmees: J. Koplik
Kirjut. Marlei &lt;allkiri&gt;
trahw on ära makstud
