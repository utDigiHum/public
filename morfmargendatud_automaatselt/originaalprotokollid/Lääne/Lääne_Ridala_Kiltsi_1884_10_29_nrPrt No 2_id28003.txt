Ette astusid Juula Linnas ja Juula Kroon ja ütlesid seda ama walja mis Tallitaja ja kohtumehe wastu enne olid tunistanud. Kohus kutsus neid Marri Matsikiga suu suud wastu. Marri Matsik hakkas aga ikka kangesti wabandama ha ütles et keik tema peale tühja räkiwad ja teda warkags teha tahtwad aga tema on sest puhas. Ka astus jälle tema issa taa est wälja ja ütles et tema tüt on ilma süüta ja sest wargusest puhas.
Kohus moistis et Marri ja Juhan Matsik peawad mõlemad 48 tundi wangi trahwi saama ja otsus jääb teiseks kohtu pääwaks.
Sellepärast et Marri ja Juhan Matsik keiki walelikkuks teewad ja et kohtu otsust nisuguse kabge wastupanemisel ette lugeda ei woi ja et ka ehk paremad tunistust weel woiks saada.
Kohtuwan. Peter Thomson
Kõrwamees Jüri Erik XXX
Kõrwamees Jaan Koplik XXX
Kirjutaja M Kentmann &lt;allkiri&gt;
täitetud 48 tundi wangi
48 tundi wangi täitetud
