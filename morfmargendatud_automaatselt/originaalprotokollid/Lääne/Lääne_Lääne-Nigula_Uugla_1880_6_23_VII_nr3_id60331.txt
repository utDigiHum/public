Astus ette Jürri Liwert ja kaebas, et Hans Alwin peab enese sigadega wäga holetu olema ja neid mitte hoidma, ta olla 5 siga enese niitmata heinama kätte sanud, enese jure aianud, kust Hans Alwin jälle wäewöimusega ära aianud
Kogokonna kohus moistis, et Hans Alwin peab iga sea peält 15 koppik, ühtekokko 75 koppikat Jürri Siwertile kahju tasumist maksma.
Kohtowannem Jürri Klemann XXX
Körwasmees Ado Weidebaum XXX
Körwasmees Jürri Jaanmees XXX
Kirjutaja Abr. Krein &lt;allkiri&gt;
