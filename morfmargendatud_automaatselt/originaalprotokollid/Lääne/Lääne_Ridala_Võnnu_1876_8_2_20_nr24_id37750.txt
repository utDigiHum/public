Kaebas Wönno mõisa wallitsus kirja läbbi nenda: Mudda Mart Gutmann olla Kutser Jaani heinama pealt 67 noored kasse puud ärra raiskanud, selle läbbi, et ta nendel ossast ladwad ja pooled puud olla ärra raiunud, kedda ta tunnistab lua oksteks Hapsalo wiinud, ja arwab, et se kellegi suur süü ei olle. Metsa waht Gustaw Rätsep teab sedda parrem tunnistada. Pallun tedda kohto läbbi trahwida ja mõisale kahjotassumist pärrida kroono taksi järrele.
Sellepeale tulli ette Jõeso mees Mart Gutmann ja räkis: Minna ollen kül sedda teinud, agga need ei olle kellegi tarwilissed.
Gustaw Rätsep tunnistas need puus kännust jämmedad ollewad 1/4 kuni 3 tolli.
Kohto otsus: Et Gutmann neid puid kül tühjaks arwab, siiski on ta ikka puu ja riigi seädusse järrel köwwaste keeldud noort metsa raisata. Ja et sest hoolimata on sedda siiski teinud peab ta igga ühhe puu ehk känno eest 10 kopik maksma, teeb 6 Rubla 70 kop. wälja. Sest saab mõisale kahjotassumisseks 4 Rubla 70 kop. ja Jõeso wala heaks 2 Rubla. Makstud peab saama ühe näddala aiaga.
Makstud 4R. 70 kop. Mõisale 5al Aug.
Trahwi raha on Jõeso walla wallitsuse holeks antud sisse korjada.
