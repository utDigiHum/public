Astus ette Auaste wallast Hans Ermann ja räkis wälja: Uglast Michkel Kelnik annud tale tenistuse eest heinamaa; ta teinud selle heina walmis, ja sanud ühe koorma suruse kuhja. Aga Uemoisa wallast Adu Pedoli, kes enne ka seda heinama kohta teinud, wiinud nüüd tema heinad ära.
Kog. kohus moistis, et Adu Pedoli peab teisel kohtu korral ette tulema.
Kohtuwanem Jürri Kleemann XXX
Körwasmees Adu Weidebaum XXX
Körwasmees Jürri Jaanmees XXX
Kirjutaja A Krein &lt;allkiri&gt;
