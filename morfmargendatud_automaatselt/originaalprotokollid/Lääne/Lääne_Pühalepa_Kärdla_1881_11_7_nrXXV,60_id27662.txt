Keiserliku Hiomaa kihelkonnakohtu otsuse järel sel 2 Novembril 1881 a. sai täna siin kohtu poolt Thomas ja Kadri Waar järeljäänud wara nende pärijatele kättemaksetud, ja nimelt:
Thomas Waar tütre Kreet Waarile raha:oma isa T. W. mattuse kulu:			10			Rub			-,,-			Cop.		oma wanematte kaitsmise ja toitmise eest			11			-,,-			15			Cop.		Osa raha			11			-,,-			15			-,,-		Suma			32			R.			30			Cop.		
Peale seda sai Kreet Waar kõik muu kraam mis temal nimetud kohtu otsuse järel pärida oli.
Simmo Waar lesk Kreet Waarile: 11 Rub. 15 Cop.
Jüri ja Madly Waar waeste lastele: 11 Rub. 15 Cop.
Liiso Takking osa 17 Rub. 15 Cop. raha ja ema riided saawad weel senni siin kohtu poolt ülewel hoitud, kunni tema käest wastus tuleb, kas tuleb iseneid siit wastu wõtma, wõi peab talle kätte saadetud saama. Talle on juba selle pärast kirjutud saanud.
Et muud toimetust polnud, sai kohtupääw lõpetud.
