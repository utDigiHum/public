Sai ettewõetud, mis Nr 12,13 al kaebatud, (Mihkel) K. Ristmägi ütles weel, et Peet Tõkke ja Juhan Rist Kustaw Madikud toast wäljatoonud,- ning siis Madik raijutud saanud.
Juhan Rist 22 2/3 aast.w. ütles, et tema põle mitte K. Madikud wäljatoonud ega ka mitte raijunud, mis teised ka ei tunnista, et ta raijunud on.
Peet Tõkke 19 3/4 aast.w., ütles et ta ei ole koguni mitte Kustaw Madik'ud raijunud noaga, ega põle ka näinud, kui teda raijutud saanud.
Tunnistus Peet Ristmägi, 18 1/4 a. wana, ütles et ta on näinud, kui Kustaw Madik toast wäljatulnud, on Peet Tõkke tale käe sisse noaga löönud.
Sellewastu waidles Peet Tõkke, et ta teda /Madikud/ raijunud ei ole, aga seda tunnistas, et kui Kustaw Ristmägi ta järele jooksnud ja 3 hoopi teibaga päha löönud, ning ta pitkali maas olnud - on noa saanud ja teda /Mih. Ristm./ siis raijunud hädaga, sest Kust. Ristmägi on aea sirt aeast otsind ja teda luband tappa, ööldes, et sa pead mu käe al surema.
Seewastu waidles (M.) Kust. Ristmägi, et Peet Tõkke põle ted maas olles mitte, waid järeljookstes raijunud; ja et põle neid ähwardamise sõnu ka mitte räkind.
Kustaw Madik ja Peet Ristmägi ütlesid, et kui Kustaw Ristmägi Peet Tõkked keelma läinud,- on Prido Tõkke senna läind Kustaw Ristmägi palito lõhki tõmbanud ja pudeliga päha löönud.
Prido Tõkke, 20 3/4 a. wana ütles, et Kustaw Madik ja Johan Rist ühteteist peksnud ja Kustaw Ristmägi wahele läinud- ja ta on (ta on) K. Ristmäge keelnud, et katsgo nad isi oma jõudu,- on Kust. Ristmägi ta kaela salli kaelast ärakiskunud ning ühe hoobi kõrwa peale löönud ja ta on ka teda /Ristmäge/ ühe hoobi löönud; siis on ta ennast /Kust. Ristm./ paljaks teinud,- ning tale ruusikaga päha löönud,- aga noa raijumist ta põle teinud ega näinud ja ka pudeliga mitte löönud.
Kustaw Pado, 20 1/2 a.w. ütles, et Kustaw Ristmägi on teda teibaga ka löönud, mis ta niisama ka wastu teinud.
See wastu ütles Ristmägi Kustaw, et ta seda ei tea, kas ta löönud ehk mitte.
