Ette astus Jürri Mik Wönust ja kaebas:"Mina läksin minewal aastal Karel Arru juure temale töö abiliseks ja tema lubas minuga maad pooleks wõtta, ma harisin maad tubliste selle tarwis ja ehitasin selle wastu et sinna pidin jääma; nüid aias ta mind sealt ära ja ei ole mulle ühtegid tassund seepärast pärin nüüd tema käest 4 wakka rukki seeme eest 8 Rubla küüni ehitamise eest 3 rubla, aida ehitamise eest 1 rubla ja rehaaluse paranda tegemise eest 75 kop. Suma 12,75 kop.
Ette astus Karel Arru ja ütles wälja et tema pole Jürri Mikkiga mitte kokku leppind ja on sellepärast lahku löönud, muud tööd tema lubas tassuda aga mitte rukki seemed, sest et temal ola ka tööde eest tagasi pärimist.
Kohus moistis et Karel aru peab Jürri Mikile 475 kop. töö eest wälja maksma aga kui Karel aru ei taha rukid wälja maksta siis leikab Jürri oma külwatud rukid, maksab selle maa jägu renti ja anab õled tagasi.
Sellepärast et Karel Arrul ei olnud mitte tunistusi ette tuua mis eest tema Jürri Milkki käest weel omale töö raha pärib kui temast lahku löönud.
Kohtuwanem Jaan Jünter &lt;allkiri&gt;
Kõrwamees I Kustas Neider XXX
Kõrwamees II Jaan Lamp XXX
Kirjutaja M Kentmann &lt;allkiri&gt;
