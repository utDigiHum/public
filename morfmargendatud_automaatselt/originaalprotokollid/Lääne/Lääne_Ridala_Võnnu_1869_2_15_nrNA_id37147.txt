Kohto tulli Juhhan Sihwer ja kaebas Hindrek Wira ja Mihkel Pet warrastasid minno kambrist, (kus nemmad hakna klasi ollid katki teinud ja siis eält haknast ollid sisse läinud.) 1 pooltündrid silko ärra.
Siis kutsuti Hindrek Wira ja Mihkel Pet ette ja naad ei teinud nüüd ennam wabbandamist, mis naad enne kül ollid teinud.
Kohto otsus: Hindrek Wira ja Mihkel Pet peawad mõllemad 3 rubla maksma, pool sest rahhast Johhan Sihwerile kahjo tassumisseks ja pool wallalaeka haeks. Hindrek Wira selsamal päewal ja Mihkel Pet 1sel Abrillis wälja maksma.
Kohto peawannem: Jaan Kiskmann
Kõrwasmehed: Mihkel Lahhe Ado Ües
