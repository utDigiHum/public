Kohto ette kaebas mõisa pollitsei Kalts mõisa teomest, kes ollid massina jurest wilja warrastanud: Ado Kre 4 küllimatto Rukkid ja Karel Soer 1 küllimat odre.
Kohto ette tullid need üllewel nimmetud mehhed ja ei teind kaebtussele wabbandamist.
Siis mõistis kohhu, et Ado Kre piddi omma sü ette 2 rbl trahwi maksma ja nenda sammuti Karel Söer 50 koppik, selsamal päewal walla laeka kassuks.
