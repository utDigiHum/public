Ette tuli Jaan Sepel Taiblast ja kaebas: Tunamulle sügise wahetasin weike Lähtro mehe Gustaw Saarjäär-iga hobust, ja ta pidi mulle 14 rubla 50 kop peale andma, aga see on alles saamata ja ta ei luba seda maksta. Selle tunnistuseks on Jüri Uussild Wõnnust.
Ette tuli Gustaw Saarjäär ja rääkis walja: Et ta kauba juures mind pettis seepärast ma ei maksa. Mina kauplesin seda hobust terwe hobuse wastu, aga et tema hobune loukas ja ta ise ütles et ta rautamisest see wiga on saanud, see läbi et üks nael natuke sügawasse läinud ja see wiga warsti pidi ära kaduma, seest pärast jähi meie kaup nõnda: Kui hobune terweks saab siis maksan sulle seda pealse raha aga, kui ta wigaseks jääb siis mitte. Aga et hobune terweks ei saanud, siis tahtsin ma 5 päewa pärast taganeda, aga ta ei tahtnud.
Jüri Uussild käis ees ja tunnistas et ta seelle pealse rahast ja nende tingimistest üsna selgeste ei tea rääkida.
Kohtu otsus:
Kohus mõistis Jan Sepel oma pealse rahast ilma, sest et ta kauba juures teist walega pettis.
Seega ei olnud Jaan Sepel rahul ja nõudis luba kirja Ridala kihelkonna kohtusse minna. ja sa.
Peawanem W. Lien XXX
Kõrwamehed Mart Jöeg XXX Aadu Salme XXX
Kirjutaja J. Klanmann &lt;allkiri&gt;
