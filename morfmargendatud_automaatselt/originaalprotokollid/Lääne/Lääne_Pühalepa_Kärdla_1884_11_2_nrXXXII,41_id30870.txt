Koos olid, Tallitaja: W. Pisa, abimehed: W. Mölder ja W. Poola seatud kohtupääwa pidamas.
Rätsep Gustav Läck astus ette ja kaebab, et temal juba kaua aegaGustav Kiil käest			2 rub			50 Cop.		Jacob Miikmann käest			3 -,,-			-		Karl Sinik käest			1 -,,-			10 -,,-		
raha saada on; Ta on neile nimelt riidid teinu, ja tööraha wõlga jäänud. Ta on neile seda meele tuletanud aga naad ei tee sest wäljagi.
Gustav Kiil, Jacob Miikmann ja Karl Sinik astusid ette ja sai see asi neil ettepandud, rääkisid wälja, et neil see wõlg G. Läck'uga on, aga neil seiamaale wõimalik polnud seda äramaksta.
Otsus: Et G. Kiil, J. Miikmann ja K. Sinik oma wõlga ühe kuu aja sees G. Läck'ule peawad äratasuma.
