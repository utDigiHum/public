Tulli möisa ja walla metsa Kanslaur ja Kabrametsa külla heinamawaht Ado Mäg ette ja kaebas: Wönno külla heinama waht Willem Lii 2 sigga käiwad igga pääw Kabrametsa heinamas. Neil on kül traadid ninnas, siiski tallawad ja teggewad kahjo, ja ta ei hoia neid, et kül ollen tedda käskinud.
Willem Lii tunnistas: Minna ollen igga pääw koddont ärra Hapsallus tööl, wõib olla, et naad on käinud.
Kohto otsus: Et kül Willem Lii on issi ärra, on teada, agga naene ja lapsed olleks piddand siggo karjama, ja et ta sedda polle teinud langeb ta 1 Rubla trahwi alla walla laeka heaks. Maksis tänna.
Kohtowannem:
Körwasmees: Hans Allik Ado Kask
Kirjotaja J Reinans &lt;allkiri&gt;
Täidetud 12 Junil 1878.
