Astus ette Nöwalt Jaan Lepson ja rääkis wälja: Aadu Liwert toonud minewal aastal tema käest 3 tündert kartuhwlid 6 rubla ette ja 3 koormat kasepuid 2 rubla 35 kop. ette ja raha laenanud tema käest 1 rubla 90 kop. Seesamma toonud temale ühe kattusest lautatud ölgi kolm koormat; neid ölgi wastuwõttes ei olle neil mitte kaupa olnud, mis need maksma pidand. Naad pidanud minewal kewadel eneste wahel aru tegema, aga see wiibinud edasi, kunni seie maale, nüüd Aadu Liiwert surnud ära ja asjad jäänud segamisi. Ta arwas need öled 6 rubla wäärt olnud ja nõudis 4 rubla 25 kop. surnud lese käest weel saada.
Astus ette lesk Leenu Liwert ja rääkis wälja: tema mees wiinud 4 koormat õlgi Jaan Lepsonile, aga kui palju leisikaid kokku olnud, ehk mis hinda need maksma pidand, seda ta ei teadnud; aga 3 tündert kartuhwlid saand toodud ja puid ka, kelle paljust ta ei teadnud, niisammuti seda raha laenamist.
Astusid ette Hans Laubert ja Jaan Roos ja tunnistasid wälja: Nemad wiinud Aadu Liwerti ölgi kui abilised 67 leisikat ja see ise wiinud 63 leisikat ühte kokku 130 leisikat Jaan Lepsonile ja 8 kop. olla iga leisika hind olnud. Ka Hans Laubert näinud, et Adu Liwert 1 rubla raha see kord Jaan Lepsoni käest laenanud.
Lesk Leenu Liwert lubas 1 rubla 25 kop. Jaan Lepsonile weel maksta ja see leppis sellega.
Kohtuwanem Mihkel Kuur XXX
Körwasmees Jüri Kleemann XXX
Körwasmees Jüri Saar XXX
Kirjutaja A. Krein &lt;allkiri&gt;
