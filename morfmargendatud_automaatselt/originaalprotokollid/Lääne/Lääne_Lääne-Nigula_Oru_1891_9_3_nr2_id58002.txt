Hans Dikbein näitas ühte tunnistus kirja ette, kellega lesk Tiu Druus enese päris Kelja poole kohta temale ja tema naesele Mari Dikbeinile päris omanduseks lubanud oli ja seda juurelisades, et selle koha peäle üks kotrath ja kaart Hans Dikbeini nime peäle tehtud saab.
See tunnistus kiri oli Auaste walla walitsuse poolt öigeks tunnistud, et see Tiu Druusist wälja antud oli. See tunnistus kiri sai Oro walla kohtu poolt Lesk Tiu Druusi ja Hans Dikbeini juures olemisel kinnitud.
Kohtuwanem A. Krein &lt;allkiri&gt;
Tunnistan sellega et olen selle tunnistus kirja algus kirja kätte saanud.
Hans Tikbein &lt;allkiri&gt;
