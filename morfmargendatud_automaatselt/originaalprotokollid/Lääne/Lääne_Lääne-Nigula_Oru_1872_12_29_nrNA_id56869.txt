Astus ette Tio Aleppi ja kaebas Hans Aleppi peäle, et Hans Alepp ei maksa temmale ühhe aasta tenistusse ette üks kuub mitte wälja.
Astus ette Hans Alepp ja pallus Tio Aleppi kannatada ja lubbas 1874 aastal Jürripäwal selle kue wälja maksta.
Selle peäle leppis Tio Alepp, Hans Aleppiga ärra.
Kohtowannema assemel 
Kohto Körwamees Hans Willik XXX
Kohto Körwasmees Hans Kork XXX
Kohto Korwamehhe assemikkuks tallita Jaan Kork
Kirjotaja A. Krein &lt;allkiri&gt;
