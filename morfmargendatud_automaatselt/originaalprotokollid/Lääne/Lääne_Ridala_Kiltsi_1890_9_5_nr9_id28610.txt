Kiltsi mõisa politsei poolt saab palutud MundiKüla peremehe teenijaid trahwida ja minule Kahjutasumiseks 2 rubla nende käest sisse nõuda, et otsa tüki halwasti kokku koristasiwad.
Kostjad Mihkel Kreis ja Jüri Manni oliwad ette kutsutud; ilmusiwad ja ei wabandanud, et oliwad töö wähe halwasti teinud.
Neid soowis kohus mõisa Härraga leppima minna ja nõutud kahju mõisale ka siis ära maksta ja teatust tuua, kas Härraga lepitud.
2 Rbl. kahjutasumist makstud.
