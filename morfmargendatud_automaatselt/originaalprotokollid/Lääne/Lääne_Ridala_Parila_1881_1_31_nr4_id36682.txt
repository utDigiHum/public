Kohto korra pärrast ollid Parrila, Kebla ja Riddali koggokonna maiase kokko tulnud kohto mehhed
Ette astus Sure Lähtro walla kirjutaja C Allapärg ja kaewas: Parrila Raea körtso mehhe Karel Mihkels peale nenda: tema wöttis minno kääst 10 aasta eest ühhe ue raudwitsadega, tame puust õlle wadi, wina alla, ja nüüid olli se, sia aiani temma kääs muido tarwitad olnud, ja nüüid ei anna ennam kätte ja ütleb, et on mulle 6 Rubla 30 kop. maksnud agga ma polle, ühte koppikast sanud, muud kui 20 koppik on mul temaga wõlgo
Selle peale sai Raea kõrtso mees Karel Mihkels ette kutsutud, ja temma ütles se on tõssi, et wadi ollen temma kääst tonud, agga sellepärrast ei anna kätte et temmal on mulle kõrtso jooki maksmatta, mis on tükkati woetud mitte ühhe korraga wahhel ollen sedda pärrinud ja siis lubbab maksta agga ikka on jänud nenda et on maksmatta 5 Rubla 65 koppik, agga 20 koppikast ma ei tea ni wähha ei olle temma korraga wõtnud ja sedda ma polle ka ütlenud et temmale 6 Rub 30 kop ollen maksnud.
Selle wasto räkis C Allapärg: minnol ei olle temmaga ennam wõlgo kui 20 kop. ja keik mis Karel Mihkels rägib maksmatta ollewad sest minna ei tea
Nenda waidlesid nemmad ilma tunnistuset, ja kohhus arwas et kõrtso mehhel igga kord polle mitte tunnistust kui tema kaupa müib, sepärast arwas ka Kortsomehhe Karel Mihkelsil oiguse ollewad ja moistis, et C Allapärg peab oma maksmata jänud 5 Rubla 65 kop ärra maksma ja siis wiib ka Karel Mihkels selle wadi C Allapärgi kätte.
Kohto wannem Jaan Jünter XXX
Kohto kämehhed Jürri Jürjer XXX Mihkel Reins XXX
Kirjutaja T. Neider &lt;allkiri&gt;
on nenda jänud
