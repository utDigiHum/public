Sell üks tuhat üheksa sada üheksamal aastal kümnemal Martsi kuu päewal /1909 a. 10 Märtsi kuu päewal/ Wõnnu walla talupojad Mihkel Jüri p. Karja ja Jakob Ado p. Suur, kes kohtule isikulikult tuntawad on, palusiwad kohtu lepingute raamatusse üles wõtta oma suusõnalist lepingut.
Mina Mihkel Karja, Wõnnu mõisa järele olewa Seppa koha rentnik, annan oma koha ostu  õiguse Wõnnu walla talupoja Jakob Ado p. Suurele selleks, et ma Wõnnu mõisa ega ka Jakob Suure käest miskit tasu ei nõua selle tingimisega, et Seppa kohast jääb minu kätte pruukida rendi peale üks neljas jagu põldu ja heina maad tarwilise jao karjas maaga nõnda et hobuse ja lehma woin pidada. Selle maa eest mis minu käes pruukida on, maksan ma iga aasta Jakob Suurele renti nii palju, kui iganes üks neljas osa terwe koha maksudest aastas wälja teeb. Peale seda täidan kõik muud kohused ühe neljandama jao Seppa koha pealt, mis edaspidi peaksiwad peale pandud saama. Peale minu surma jääb aga minu käes olew üks neljas osa Seppa koha maad selle koha ostja Jakob Suurele päris omanduseks, nõnda et minu pärijatel miskit nõudmise õigust selle maa kohta ei ole.
Ülemal nimetud lepinguga oliwad Mihkel Karja ja Jakob Suur täiesti ühel meelel.
See sinane leping on pärast lepingute raamatusse kirjutamist meitele ette luetud, ning et meie oleme seda lepingut eneste selge meele mõistuse ja kokku lepimise järele teinud, mida oma allkirjadega tõendame.
I Lepingu tegija Mihkel Karja umbkirilik, tema suu sõnalise palwe peale tema eest kirjutas alla Artur Jurr &lt;allkiri&gt;
II Lepingu tegija: Jakob Suur &lt;allkiri&gt;
Tunnistajad: August Reets &lt;allkiri&gt; Willem Krabbi &lt;allkiri&gt;
Kohtu President: J. Koplik &lt;allkiri&gt;
Liikmed: T. Nuut &lt;allkiri&gt; M. Jaksman &lt;allkiri&gt; K. Selg &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Olen selle lepingust kohtu poolt ustawaks tunnistatud ära kirja wälja saanud 10 Märtsil 1909 a. Mihkel Karja umbkirilik, tegi oma käega kolm risti.
XXX
President: J. Koplik &lt;allkiri&gt;
