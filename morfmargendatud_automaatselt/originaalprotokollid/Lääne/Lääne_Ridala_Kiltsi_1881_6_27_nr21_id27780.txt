Ette astus Kaewaja Juhan Raudsep Hap kirriko wallast ja ütleb: et Jani pä õhto Mihkel Lauriga ühes wankris koio tulles Parali kõrtsist te peal sõites ollime ridus, hooste laitmissest ja kiitmissest. Simo wärrawas (wäike ahli küllas) karwustas M. Laur mind, wärawa lahti tegemisse aegas wõttis ta aia eest teiwa tük 4 jalga pitk, jooksis järele Jurna tuleweski al, peksis 4-5 hopi.
Kaebtus allune M. Laur olli ees. Ei aiand mitte tagasi; waid wabandas ennast et joobnud peaga ei mälleta mis teind; aga peksmist ei olnd.
Tunistus mees Jaan Kuher, kes ühes wankris seltsis tulnd, ütles: Te peal nemmad püüdsid nägelda hooste pärast, aga mina ja Jaan Altom püüdsime neid ika leppidada. Simo wärawas lahutasin neid karwust riidlemissest ja jäin wankrist maha käima, J. Raudsep söitis eli M. Laur ja Jaan Altom jooksid järrel, koos nemad ollid, aga kas nad peksid ei näind, sest et olli ösi. Jaan Altom tunnistas te peal kui jälle ühte saime, et: üks hoop läind tema käe peale, mis jälle hoidmisseks ette panund. Joobnud ollid nemmad keik; agga minna mitte."
Kohto otsus jääb tullewa koraks kui Jaaan Altom ja waike pois Jaan Marri poeg Raudsep kes ühes wankris olli, ka ees on.
Kohto mees: Mihkel Poots XXX
I Kohto Kõrwamees Mih. Juhkam XXX
(II Kohto Kõrwamees olli Mihkel Madsikud madmas asse olli tühhi)
Kirjotaja: Jaan Grünär
No 22
