Sell ükstuhat üheksa sada neljateistkümmendamal aastal esimesel aprilli kuu päewal /1914 a. 1 aprilli k.p./ Kana walla liige Eduart Jüri p. Laur 50 a. kes kohtule isikulikult tuntaw on, palus selle kohtu lepingute raamatusse oma suusõnalist järgmist tõstamenti ehk wiimast seadmist üles wõtta.
Oma abikaasa Ingel Toomase t. Laurile /sündinud Wakku/ jättan ma pärandada peale enese surma: maja mööblid, loomad, selge raha, wäärt paberid ja üle pea kõik muu waranduse mis mull olemas on.
Seda wiimast seadmist olen ma selge meele mõistuse, oma enese tahtmise järele teinud ja on see pärast lepingute raamatusse kirjutamist ette luetud.
Tõstamendi tegija E. Laur &lt;allkiri&gt;
Tunnistajad: L. L?? &lt;allkiri&gt; A Reets &lt;allkiri&gt;
Kohtu President: K Kroon &lt;allkiri&gt;
Liikmed: K. Möll &lt;allkiri&gt; J. Triksberg &lt;allkiri&gt; J Winglas &lt;allkiri&gt;
Kirjutaja G Krabi &lt;allkiri&gt;
Olen sellest lepingust ära kirja wälja saanud
1 aprillil 1914
E Laur &lt;allkiri&gt;
