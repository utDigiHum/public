Kaebas Mihkel Harri et ta on teinud Mihkel Ristile ühhe riedes kassuga ja ühhe palito ja käinud Juhan Nauriga neid ridet ärrawimas ja saand selle töö ette 2 rubl 50 kop ja männik rukkid, wilja ma jätsin senna ja pallusin et piddi Juhan Kersna jure sadetud sama, sealt ma olleks kätte saand, agga Willem Kersn tõi wilja ärra ja peab ommal, ta ütleb et ma tal wõlgo ollen sellepärrast ei anna wilja kätte.
Willem Kersn ütles et Mihkel Harri on tal 2 rubla wõlgo ja enne wilja kätte ei anna kui wõlla ärramaksab.
Mihkel Harri ütles et ta polle enam Kersnale wõlgo olnud kui 32 kop, 30 kop ma maksin ärra ja 2 kop jääb weel.
Tunnistus. Mihkel Rist tunistas Kassari walla wallitsuse ees, teada antud Kassari walla walitsuse polest sia kirja läbbi Nr 20 al et Mihkel Harri olla utelnud et tal Willem Kersenile wõlgo on. Essimesse korra kuulnud Mihkel Harri õues ütelnud, "Se on hea et polle naese kuuldes küssinud, ma pallun jätta muist wõlgo teise korraks. Minnu omal on ka tarwis, edespidi toimetame jälle.
Teise korra kui Usi Juhanil warrul olnud küssin Kersen sellesamma Harri käest "Maksa mo wõlg ärra", Mihkel Harri palunud wasto ja lubband koik maksa kui tenitud saab.
Kohus mõistis, nonda et Willem Kersn annab wilja Harile tagasi ja Hari maksab Kersnale üks rubl, et kahjo poleks jääb, sest et Kersnal ei olle õiged tunistust kui paljo Hari tal wõlgo on.
(Hari ei olnud kohtu otsusega rahhul.)
