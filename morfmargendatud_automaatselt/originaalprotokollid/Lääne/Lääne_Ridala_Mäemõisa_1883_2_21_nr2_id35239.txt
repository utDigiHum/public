Astus ette Hindrek Reets ja kaebas et tema hoowi pealt 19 Februaril õhtu on saan ärra warastud. Wargad on ühe kinda ja piitsa ärra kaotand ja need on Nakama kõrtsmik ärra tunud et Masso mehe Jaan Siiman jägu on.
Kohus moistis et teiseks kohtu pääwaks peawad ette kutsutud saama ja ka teine mees Ärma Juhan ja Torro Jaan Siiman.
Kohtuwanem Juhan Killemet XXX
Kõrwamees Mihkel Liiw XXX
Kõrwamees Jürri Siimon XXX
Kirjutaja M Kentman &lt;allkiri&gt;
