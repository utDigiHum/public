Ette astus Mart Leismanni kaebtusse järrel Hans Tott ja räkis wälja: Kolme aasta kaupa polle meil olnud, Mart tahtis ühhe, ja minna kahhe aasta peale ja siis lubbasin 10 Rubla, kui ta piddi kohe eest otsa ride leikust ka õppetama. Siis ütles Mart olgo siis peale kaks aastat, agga siis peab ta üks kuub nenda walmis teggema, kui russikas silma aukus.
Mis moodi se siis on, on teadmatta?
Agga nüüd wõtsin poisi ätta, et ta weel suggogo ei mõista, ride löikust; Mina ollen 10 Rubla lubbanud ja maksan ka kohe wälja, kui Mart olleks poisi nenda paljo õppetanud, et ta keige wähhemagi ride walmis wõib tehha, nenda kui kaup olli.
Siis tulli rätsep Mart ette ja räkis Minna ollen ikka tedda õppetanud, ja kui ta ei õppi, mis ma siis wõin tehha.
Siis tulli pois Willem Tott ja räkis: Temma polle mulle ennam ride lõikust näidanud, kui üks ainus kord wiskas westi möeto minno ette, ja ütles: Mind on sin kästud õppetadam säh õppi nüüd issi, kui sa tahhad.
Sellepeale tullid rätsep Mart ja Hans Tott ette ja leppisid nenda ärra, et Hans Tott lubbab Mardile 3 Rubla anda. Ja nenda sai ka makstud, ja leppisid ärra.
Sel sammal korral tunnistas rätseo Mart et Temmal olla Wönno möisa Hindrek Kleppergi 3 Rubla wõlga ja lubbas se 3 Rubla Hindrek Klepper wölla tassumisseks, kedda ka aidamees Willem Selg, Hindrek Klepperi kätte siis tänna.
Kohtowannem: H. Awik &lt;allkiri&gt;
Körwasmees: Hans Allik Ado Kask
Kirjotaja: J Reinans &lt;allkiri&gt;
