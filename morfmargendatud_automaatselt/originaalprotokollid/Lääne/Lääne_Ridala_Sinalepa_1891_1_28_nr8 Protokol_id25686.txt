Ette astus Kustaw Kiwi, kaebtuse alune ja ütles wälja:"Minul on temale maksta wõetud wilja kraami eest aga kui palju seda ma ei tea. Muist olen töödega tagasi maksnud ja nimelt:2 päewa heina töö			80 kop.		2 päewa rukki lõikusel			80 kop.		3 päewa kartohwli wõtmisel			60 kop.		1 waka rukki raha			2 rbl. 50 kop.		2 hobuse päewa			1 rbl		Summa			5 r. 20 kop.		
Nii sammuti on see mul nüid ühe waese teomehel palju ühekorraga tagasi maksta aga kui mulle Maij kuuni aega lubatakse  siis tasun auusasti ära, 5 rbl. 80 kop.
Alakirjutanud Kustaw Kiwi XXX
Ette astus Hans Eigel ja ütles wälja: "Minul oli kül Gustaw Pihelgasele wilja 1/2 stwt. odre eest 4 rbl. maksta, aga seda olen oma töödega temale tagasi maksnud2 päewa hobune masina juures			80 kop		2 päewas sõnikut wedamas			1 r 20 kop.		hobuse waljad wiis minult			1 r 20 k.		hobuse look			50 k.		Summa			3 r 70 kop.		
Puuduw raha 30 cop. maksan wälja kui nõutakse.
Alakirjutanud Hans Eigel.
Kaebtuse aluste palwe peale sai otsus ilma kaebaja Gustaw Pihelgas'eta juures olemiseta ära tehtud.
Keiserliko Majesteedi käsu peale.
Otsus:
Et Kustaw Kiwi 5 rbl. 80 kop. ja Hans Eigel 30 kop. Gustaw Pihelgasi heaks peawad ära maksma.
Kustaw Kiwi maksab 5,80 kop. kuni Maij kuu 23tama k.p. s.a. ära ja Hans Eigel 2e nädali aja sees.
