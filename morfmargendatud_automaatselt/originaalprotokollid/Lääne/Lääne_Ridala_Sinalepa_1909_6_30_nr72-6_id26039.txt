Sell üks tuhat üheksa sada üheksandamal aastal kolme kümnemal Juuni kuu oäewal /1909 30 Juuni k.p./ Sinalepa walla talupojad Willem Jakobi p. Spuhl ja Juhan Kustase p. Suitsberg, kes mõlemad kohtule tuntawad on, palusiwad kohtu lepingute raamatusse akti paberi peal kirjutud järgmist kirjaliku lepingut üles wõtta.
"1908 aastal 23 Juuni kuu päewal Sinalepa walla talupojad Lambajani koha omanik Mäewallast Willem Jakobi p. Spuhl ja Juhan Kustase p. Suitsberg oleme alamal nimetud lepingu eneste kokku leppimise järele teinud.
Mina Lambajani koha omanik Willem Spuhl müün Lambajani koha No 18 karjasmaast Juhan Suitsbergile päris omanduseks 4 wakkamaad /1600 □ sülda/ maad Laskma wälja koha külge, mis selle koha wasta käib selle tingimisega, et ostja Juhan Suitsberg maksab mulle iga wakka maast 35 rubla; kokku üks sada neli kümmend rubla /140 rubla/.
Ostja maksab kõik maksud ja kannab kõik kohused, mis seaduse järele Lambajani koha peale on panud ehk edespidi pandud saawad oma ostetud maa suuruse järele.
Kui müüja ehk tema pärijad tahawad seda nimetud maa tükki tema Suitsbergi käest ehk tema pärijadelt ära wõtta, osta ehk et see muul seaduslikul teel saab ära wõetud, siis ei wõi tema seda mitte keelda, aga müüja peab ostjale kõik ostu raha tagasi maksma; kui see maa ära wõtmine alla 20 aasta sünnib, siis peab müüja ostjale weel peale ostu raha maa ülestegemise kulu tagasi maksma. Kui aga see maa ära wõtmine peale 20 aastat sünnib, siis saab ostja müüjalt ehk tema pärijadelt ainult ostu raha tagasi makstud, aga muud tasu mitte ei saa.
Müüja on kõik selle müüdawa maa tükki hinna ükssada nelikümmend rubla /140 rubla/ kätte saanud.
See sinane leping on pärast lepingute raamatusse kirjutamist meitele ette luetud, ning et meie oleme seda sinast lepingut oma eneste tahtmise, kokkulepimise ja selge meele mõistuse järele teinud, mida siin oma allkirjadega tunnistame tunnistajate kuuldes.
Lepingutegijad: W. Spuhl &lt;allkiri&gt; J Suitsberg &lt;allkiri&gt;
Tunnistajad: G Krabi &lt;allkiri&gt; A Reets &lt;allkiri&gt;
Kohtu President: J. Koplik &lt;allkiri&gt;
Liikmed: M. Jaksman &lt;allkiri&gt; K. Selg &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Algus kirja lepingu, akti paberi peal 70 koppeka hinna järele /20 kop. stembel markides peale kleebitud/ olen kätte saanud 30 Juunil 1909. aatal.
J Suitsberg &lt;allkiri&gt;
3. veebruaril 1930 a. olen ärakirja sellest lepingust vastu võtnud
W. Spuhl &lt;allkiri&gt;
