Astus kohto wanna Suredoma Jaan Martin, omma surnd wenna ja wenna naese ainus järrel jäänd poeg Mihkli pärrast, kes praego alles lapse moistussega tenib Haeska waldas; 6-7 aasta eest jäi emma surmasat temal warra järrel kellega pois 4 aastat kaswatadi Juhan Sõelseppa käes, ning keik kraam jäi wollimeeste moistmisse järrel poisi toitmisseks, agga selge raha piddi kehelkona kohto hoida sama, mis ka senna widud, agga paljalt 10 rubla hõbe, kus siis muist raha on, sest temal olli eni surma omma õele trutale näitata peale 30 rubla muist üsna õbbetükkid.
Tunnistajaks astus poisi täddi [emma odde] Truta Hanso Lesk ja ütles:"haiguse aial käisin õde watamas, temma ütles minnule:"rahha 29 rubla h. on Wanna Kõrso Juhan Söelseppa, ta wiis sedda ärra omma kätte hoida"" [jubba enni haigust nägin mina seda raha sest ta näitis 10 rubla hõbe tükid, teine raha olli paber]. Näddali pärrast läksin senna temma olli surnd. 1871 Testember pärast surma leidsime weel temma kirstust kastist 4R 40 kop. sel korral kui meie tedda Kirsto pannime; wälja, muud ma ei tea tunnistada.
tulewa kord J. Sõelsep ette k.
Kohtomees: Mihkel Poots XXX
I Kohto Kõrwamees: Jürri Rabba XXX
II Kohto Kõrwamees: Mihkel Mikkas XXX
Kirjotaja: Jaan Grünär &lt;allkiri&gt;
Kaebtus tunistussega
No 24 pr. moistmine
