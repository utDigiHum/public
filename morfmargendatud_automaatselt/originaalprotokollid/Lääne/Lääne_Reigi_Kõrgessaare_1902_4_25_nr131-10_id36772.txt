Üks tuhat üheksa sada teisel aastal kahekümne wiiendamal Jüri kuu päewal astus Kõrgesaare walla kohtu ette kohtule isiklikult tuntud Kõrgesaare walla liige Jüri Tooma poeg Jõgi kellel lepingu tegemiseks seaduslik õigus on ja palus oma suusõnalist ülesandmist järgmiselt kohtu lepingute raamatusse üles wõtta:
"Mina Jüri Jõgi tunnistan sellega, et olen Kõrgesaare walla liikme Peeter Andruse poeg Laid käest kakssada üheksa kümmend (:290:) rubla sula rahas laenanud ja luban nimetud summat tänasest päewast arwates ühe aasta jooksul temale kuue protsendiga tagasi maksta."
See ülesandmine on pärast lepingute raamatusse kirjkutamist mulle ette loetud ja minult alla kirjutatud.
Jüri Jõgi [allkiri]
Waata leping
No 14. - 1908a.
No 3 - 1909a.
No 3 - 1910 a
