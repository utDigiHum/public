Lähtro kohto ette astosid Jakob Priso Sure Riudest ja palus Lähtro walla kohut et tema pime on ja ei wõi maeale minna seepärst seda ära kinnitada et tema on müünd oma Wapra Arro Talokoha keigi oma paberitega Romann Sisminil 5000 Rbl.eest, ja palus oma nime allkirja tõeks tunnistada.
Leppijad pooled olid kohtol tuntud Määratud sai Jakob Piirso kui ka Romann Sismin nime all kirjad tõeks tunnistada.
Kohto wanem: K.Karro
Kohtomehed: P.Poll
                       : J.Palmer
                       : J.Saar
Kirjotaja: C. Redlich
Tähendus.
Pooled ei wõind Raamato oma all kirja anda, et sel aeal weel Raamatud ei olnd kätte jõudnud
Kohtowanem: K.Karro
Kirjotaja: C.Redlich
