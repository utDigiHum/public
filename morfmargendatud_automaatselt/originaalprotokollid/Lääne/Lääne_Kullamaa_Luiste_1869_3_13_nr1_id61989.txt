Wanna põllu-vaht Käkki Andrus Waelmann surri 4-nal selle ku päwal ärra ja said temma võllad ülles kirjotatud;
Saueaugo Karlele on temmal 1 wak rukkid wõlgo.
Mä Mardile ------ 1 " -----
Uetoa Antonile ------ 1 " ------
walla rahha on temma laenanud 5 R hõb.-
Et nüüd temma poeg Jaan sedda järrele jänud pärrandust pärrib, siis sai Jaanile ütteltud, et temma ka need wõllad peab maksma.; kui mitte, siis saab se lehm walla rahha eest auksioni wisi ärramüdut.
Janil ep olnud ka mitte suurt waidlemist selle wasto.
