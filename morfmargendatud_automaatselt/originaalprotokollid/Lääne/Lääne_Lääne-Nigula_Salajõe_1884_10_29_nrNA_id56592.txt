9. Astus ette pilleti soldat Jüri Kuuskmann ja rääkis wälja: Temal olla üks 10 aastane õde Anna, kelle eest ta seie maale hoolt kannud, aga nüüd ei olla tal seks mitte enam jöudu, ei ka aega abiks olla; ta olla üksik mees, kes mõisas tööl käimisega ennast toitma peab. Tema noorem wend Jaan olla Kroonu teenistuse minemisest kergitust saanud ja noorema wendade ja ödede holekandjaks jäetud. Jüri Kuuskmann noudis wenna Jaani käest 20 rubla saada, lubas siis om öe Anna eest hoolt kanda.
Astus ette Jaan Kuuskmann ja lubas oma öe Anna kaswatamiseks enese wenna Jürile 20 rubla maksta ja leppisid ära.
Kohtuwanem Hans Uus XXX
Kõrwasmees Andrus Kerm XXX
Kõrwasmees Kustaw Wiikmann XXX
Kirjutaja A Krein &lt;allkiri&gt;
