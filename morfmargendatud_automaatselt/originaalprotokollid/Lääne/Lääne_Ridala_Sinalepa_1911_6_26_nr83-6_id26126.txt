Sell üks tuhat üheksada üheteistkümmendamal aastal kahekümne kuuendamal Juuli kuu päewal (1911 aastal 26 Juuli kuu päewal/ Sinalepa walla talupoeg Hants Juhani p. Hansmann 75 aastat wana, kes kohtule isikulikult tuntaw on, palus selle kohtu lepingute raamatusse oma suusõnalist järgmist testamenti ehk wiimast seadmist üles wõtta.
Oma poja Jüri Hansmanni poja Joann Hansmannile jättan ma pärandada peale enese surma Kiidewa mõisa järele olewa Kiidewa Kala külas Sauna koha peal olewa elu maja ühes õue aidaga ja kõik muu liikuwa waranduse nii kui üks raud ja teine puu assidega wanker; hobune, üks mere püü paat ja kõik kalapüüsed. Oma lihase poja Jüri Hansmannile ei jäta ma midagi pärandada, sellepärast et ta muu wastu liiga sant on, mulle üles pidamist ei anna, riidleb ja tülitseb ühte puhku, ei pea mind mitte isa asemel.
Nõuan oma poja pojalt, et sell korral, kui Jumal mulle elu päiwi annab ja kui ma mitte ei peaks enam ise omale (enam) suutma ülespidamist muretseda ehk ennast aidata, et ta siis nurisemata mind toidab, kattab, harib ja üle pea hea südamelikult hoolt kannab ja wiimaks mind maha matab.
Seda wiimast seadmist olen ma selge meele mõistuse ja oma enese tahtmise järele teinud ja on see pärast lepingute raamatusse kirjutamist mulle ette luetud. Läbi joonitud "enam" mitte arwata.
Tõstamendi tegija Hants Hansmann umbkirilik tema suusõnalise palwe peale tema eest kirjutas alla: A. Reets &lt;allkiri&gt;
Tunnistajad: K. Neider &lt;allkiri&gt; K. Liimann &lt;allkiri&gt;
Presidendi asemel K. Selg &lt;allkiri&gt;
Liikmed: K. Möll &lt;allkiri&gt; W. Luntsi &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Olen sellest lepingust wõi tõesdamendist ära kirja wälja saanud 13 Detsembril 1911 a.
Johannes Hansmann &lt;allkiri&gt;
