Tulli ette Peter Ro ja töi tunnistusse kirja ette, et Tallinna linna Tompa koggo temma poia Ado Ro omma hinge kirja ülles wötwad.
Selle peale möistis kohhos Tallitajaga, et Ado peab enne oma Pearaha wölga 9 Rubla ärra maksma ja selle aasta IIne pool 2 Rubla 74 kop. Ja Peter Ro lubbas.
Siis sai wäljaminnemesse seddel antud.
Kohto wanem: Willem Kelt
Kirjotaja: Jaan Nano Jürri Naab
Joh Reinans &lt;allkiri&gt;
Täidetud 8al Now, 1875
