Prido Nurk kaibas, et tema käes on Kattla koht (Rannamõisast) rendi peal olnud ja see koht on kolme aasta eest müüdud Rein Siramäele, kaup oli kuue aasta peal olnud, aga kolme aasta järel olla koht käest ära wõetud ja müüdud ilma et Jakobi pääwast on üles öldud.
Ja tema on seda kohta parandand nii kui al seisab:48 sülda uut kraavi tehtud								üks ait ja looma laut ühe kattuse al			30 Rbl					raud kiwid põllo seest wälja wedand kolme pääwa töö, 'a p. 50 kop			1 Rbl			50 kop		50 koormad sõnnikud, 'a k 10 kop			5 Rbl					1 Tsetwert rukkid maha tehtud 6 kordne seeme 'a Tsw. 10 Rbla			60 Rbl					Summa:			96 R.			50 k.		
Ja tema palub kohut kohta kätte anda, sest ta oleks wõinud seda isi osta, aasta rent on 45 Rbl olnd.
Kohus mõistis, Et see asi mitte enam walla kohtu mõistmise al ei seisa, saab auustud Keisrl. St. Martna Kihelkonna kohut palutud seda seletada E.T.r.S. § 728.
(Kihelkonna kohtu läkkitud, sel 17 April 86.)
