Astus ette Leenu Nöupuu ja rääkis wälja: ta olla 1887 a. Kolme kuninga pääwast Aadu Wiilmanni juure karja hoidjaks läinud, see lubanud tale teha ühe seeliku, 1 särgi, üks paar sukki ja ühe jäkki, särgi, ja seeliku saanud ta kätte, aga jäk ja sukked jäänud saamata. Ka läinud ta ue pastlidega senna ha see saatnud teda ilma pastliteta enese juurest ära 2 nädalat peale Mihklepääwa 1887.
Kohus moistis, et Aadu Wiilmann peab teisel kohtu korral ette tulema.
Kohtuwanem Mihkel Kuur XXX
Körwasmees Jüri Kleemann XXX
Körwasmees Jüri Saar XXX
Kirjutaja A. Krein &lt;allkiri&gt;
