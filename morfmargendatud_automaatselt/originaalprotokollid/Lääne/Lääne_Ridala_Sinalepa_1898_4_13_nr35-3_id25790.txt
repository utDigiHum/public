Sell üks tuhat kaheksa sada üheksa kümend kaheksamal aastal, kolmeteistkümemal Jüri kuu päewal, ilmusiwad Sinalepa walla kohtusse Kebla mõisa omanik Hugo Aleksandrowits Frichmann ja Käbla walla talupoeg Isak Mihkli p. Pruul, kes mõlemad kohtul tuntud on, ja palusiwad järgmist suusõnalist palwet kohtu aktide raamatusse üles wõtta.
Tänasel päewal annan mina Kebla mõisa omanik Hugo Frichmann Isal Pruulile luba oma mõisa maa peale enesele sauna ehitada ilma määramata aja peale järgmiste tingimistega:
1) Et nii kaua kui Isak Pruul minol mõisa teenistuses on, wõib tema saun minu maa peal olla ilma maksuta, aga nii pea kui tema ilma põhjuseta ära läheb enne wiiteist kümend aastat minu teenistusest, teise koha peale teenima, siis on minul õigus seda sauna omale asta takseeritud hinna järle; nii wiisi et takseerimise juures peawad kaks walla poolt ja kaks mõisa poolt takseerijat olema.
2) Teistele inimestele ei ole Pruulil oma sauna mitte luba müüa, mis minu maa peal on ehitud.
3) Kui aga enne wiitteist kumend aastat, kudagi wiisi Pruul juhtub tõbiseks jääma ja ei suuda minu teenistuses enam olla, siis on minul jälle õigus tema saun punkt I järele amole osta ja teda sealt ära ajada.
4) Kui Pruul on juba wiisteistkümend aastat täis mind teeninud siis ei aja mina teda sealt mitte ära, waid temal on sauna õigus minu minu maa peal ilma määramata ajani, kui ta küll enam ei suuda minu teenistuses olla.
5) Peale seda peab J. Pruul minu sõna kõigis asjas kuulama ja antli mees olema. Kui aga tema mitte minu kasku ei täida ja wastu hakkab, siis on jälle minul õigus teda ära ajada ja tema saun eneselee osta takseeritud hinna järe.
See leping on pärast lepingute raamatuse kirjutamist saanud pooltele ette loetud, mida nemad oma all kirjadega õigeks tunnistawad.
Hugo Frichmann &lt;allkiri&gt;
Isak Pruul &lt;allkiri&gt;
Kohtu Presidendi asemik: G. Suurküla &lt;allkiri&gt;
liikmed J Piilbus &lt;allkiri&gt; J karja &lt;allkiri&gt;
Kirjutaja G. Krob &lt;allkiri&gt;
