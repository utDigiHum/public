Kaebas Ado Kullij: Minnul on woorimees Hans Awik kääst 15 Rubla 24 kopik reisi raha sada, ja temma ei lubba mulle sedda maksta.
Siis räkis Hans Awik, kui Prido Selg ja Ado Kullij Tallinast kallewi pakkid töid, mis Hioma Kerteli wabriko piddid saama, siis olli ühhe pakkil auk sees, mis läbbi minnul 200 Rubla woori raha kinni petakse kahjotassumisseks, ja selle pärrast ei wõi ma Adole maksta, sest minno kahjo on paljo surem kui Adul; waid ma nouan, et Prido Selg ja Ado Kulli peawad minno kahjo weel wähendama.
Ado Kulli tunnistas: Prido Selg töi selle paki Tallinast ja Hans Awik käskis Hapsalo Bluumbergi poe ees mind sedda pakki peale wötta ja saddama wiia, kui ma wõtsin, siis ei watanud, kas se pak olli enne jubba katki, woi ei, agga Hapsalo saddamas olli ta katki.
Kohto otsus: Et Ado Kullij ette watamatta selle asja jures olli, peab ta kahjo kandma, ja jääb otsus polele, ni kauaks, kui Ado selget tunnistust selle asjast toob.
poolel.
