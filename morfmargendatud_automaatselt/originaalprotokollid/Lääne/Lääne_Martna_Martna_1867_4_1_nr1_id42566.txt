1867 Tunnistusseks
ohtla walla koggokonna Prottokol
ohtla walla kohhos on kokkotulnud sel essimessel Aprilli kuu päewal Ado mäggi Ja Lähtrust Ja Kustas Kruus ohtlast on kohto ette tulnud Kustas on Ado käest sani osdnud Ja pärrast Ado ei olnud mitte sellega rahhul siis kohhos moistis et Ado sai omma saani taggasi Ja maksis Kustasele selle rahha Taggasi Ja siis Leppisid sellega ärra
Tunnistusseks Pea kohtomees Karel peiken XXX
Kohto abbimees Kustas Tenrek XXX
Teine abbimees Jaan Rehkalt XXX
Kirjotaja Kustas Tegenberk XXX
