Sai kutsutud kohto ette Kustas Kolga metsa wargusse pärrast, (mis No 7 32se lehhe külje peal polele jäi.) et tõeks tehha tunnistusse ees, kas temma on Jaan Spuhliga Tõnnisomihkli heinama mestast puid raionud.
Tullid ette Kustas Kolga ja Simo perremees Jaan Spuhl ja tunnistusmees Rannamihkli perremees Jaan Reets ja temma poeg Hindrek.
Jaan ja Hindrek tunnistawad, et nemmad on ühhel päwa õhtul näinud, kui nemmad on tulnud mõisa rago leikamist, et Kustas Kolga ja Simo Jaan Spuhl ja temma poeg mihkel on kahhe hobbose ja wankriga Tõnnisomihkli heinama peal olnud ja siis on kaks kaske sääl ka maas olnud mis jubba raiotud olnud; agga kui nemmad senna on läinud siis on Kustas Kolga ja Jaan Spuhl sealt ärra joosnud agga hobbosed on senna jäänd.
Tulli ette Kustas Kolga, kes selle tunnistusse wasto rägib, ja waidleb, et temma senna metsa polle saand, ei polle Rannamihkli Jani egga Hindrekud näind, ega polle ka kaskesid sealt raiund weel wähhem hobbosega sealt wiind.
Tulli ette Simo Jaan Spuhl, kes ka selle wasto waidleb, agga ommeti ütleb et temma seal hobbosega käind ja toond ühhe kasse mis enne seal maas olnud, agga keggani polle temma seal wõera ma pääl raiond.
Et selle wasto waidlewad ja ommeti wargad on, jääb selle pärrast polele, et Simo Jani poeg se kolmas süallune Mihkel Spuhl polle koddo, on passi seddeli peal tööl.
Koos ollid Kohtowannem: Juhhan Kellemet
Jürri Walgemäe
Jakub Spuhl
