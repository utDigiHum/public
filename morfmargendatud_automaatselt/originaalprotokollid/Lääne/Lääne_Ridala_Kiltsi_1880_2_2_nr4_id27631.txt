Ette sai wõetud 1879 aasta assi, mis Kaewaja Juhhan Suurkül su sõnnaga kohto mehele kaewad nüüd kohto peegli ette.
Kaewaja Juhhan Suurkül Tanski küllast, ütleb et Jani pulma öse enni Joulut (tüdre pulmad) pühhapäwa õhto kel 10-11 aial, wisid pulmalissed 1/2 lammast minno toa lae alt suitsust warrastussega ärra pimmedas toast. wargad on: Willem Koppel Sanika küllast; Rehhe Kustaw Wil küllast; ja Rehhe Hans Wätse küllast. Öse pulma perres keetnud ja söönd; selle peale woib mitto tunnistust sada; ja et assi ni kauaks wiwis, kulasin ma salla mahti omma tunnistussed Teie pärrast läbbi, ja kohtul ei olnd woimalik Teid enne kutsuda.
Essimine Kaebtus allune: Willem Koppel, ütleb:"Minna polle Hanso Toa (kus J. Suurkül ellab) ukse külge kässi mitte pannud egga toas käind, hommiko kui maast ülles tousin, sõin lihha ja kartohwlid, ja ei tea sest salla asjast ühtegi.
Teine Kaebtus allune: Gustaw Prints, ütleb: Ei tea minna, mis laua peale pandi sõin ma; poled kartohwlid ja lihha.
Kolmas Kaebtus allune: Hans Kahm ütleb: Hommiko kui pulm süia sai, sõin ka mina enni päwa touso. Ösi kefetud kramist ei tea minna egga sest sömissest." peale selle on keik kaebajad ühhest suust Juhan suurkülli peal kohto ees ja ütlewad "Kui Hanso Juhan ommale kohto ette toob üht ainust tunnistust, et meie need kolm olleme ösi keetnud ja sönud, siis olgo kohhe meie süi. Maksame ilma kohto ette tullematta wälja.
Kohto mehed moistwad asja ni kauaks polele, kui Juhan Suurkül neist tunnistust kohto peegli ette toob.
Kohto mees: Mihkel Poots XXX
I Kohto Kõrwamees Mihkel Juhkam XXX
II Kohto Kõrwamees Jürri Laur XXX
Kirjotaja: Jaan Grünär &lt;allkiri&gt;
assi polel et polle kaebtussel tunnistust.
