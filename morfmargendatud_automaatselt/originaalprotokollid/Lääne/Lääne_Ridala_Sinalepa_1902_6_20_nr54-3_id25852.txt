Sell ükstuhat üheksa sada teisel aastal kahe kümnemal juuni kuu paewal astusiwad Sinalepa walla kohtu ette Sinalepa walla talupojad Jaan Mihkli p. Wiinberg ja Jaan Madjuwi lesk Liisu Madjuwi kes mõlemad isikulikult kohtule tuntawad, palusiwad kohtu lepingute raamatusse järgmise suusõnalise lepingu üles wõtta
Mina Jaan Mihkli p. Wiinberg Kiidewast Wiina koha omanik, müün tänasest päewast edasi arwatud Sinalepa walla liikme Liisu Madjuwile Jaani lesk (sündinud Kelemith) seitsme kümne wiie rubla (75 rubla) eest Wiina kohast No 9 kaks wakka maad heinamaad ja üks wakkamaa põllu maad pärisomanduseks elu ajani.
II
Ostja Liisu Madjuwi on ostu raha 75 rubla juba mulle wälja maksnud.
III
Peale Liisu Madjuwi surma jääb see kolm wakkamaad maad ühes majaga Liisu Madjuwi lihase wenna Jaan Mihkli poja Klemetile päris omanduseks.
See sinane leping on pärast lepingute raamatusse kirjutamist meitele ette luetud ning et meie oleme seda sinast lepingut oma enese tahtmise, kokku lepimise ja selge meelemõistuse järele teinud, mida siin oma allkirjadega õigeks tunnistajate kuuldes tõentame.
Tähendus: läbi joonitud "Kelemit", peal kirjutud Madjuwi õige ja punkt IIImas, läbijoonitud "Madjuwi", ei ole tarwis arwata.
Lepingu tegijad: Jaan Winberg &lt;allkiri&gt; Lisu Madjowi &lt;allkiri&gt;
Tunnistajad: Hindrek Reets &lt;allkiri&gt; Aleksander Reinds &lt;allkiri&gt;
President: G. Surküla &lt;allkiri&gt;
Liikmed: J. Kahm &lt;allkiri&gt; J Karja &lt;allkiri&gt; K Awik &lt;allkiri&gt;
Kirjutaja: G. Krabi &lt;allkiri&gt;
16 veebruaril 1929 a. olen sellest lepingust ärakirja vastu võtnud.
Liiso Madjow &lt;allkiri&gt;
