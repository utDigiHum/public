Koos olid, Tallitaja: A. Espak, abimehed: P. Püss ja W. Uusmann seatud kohtupääwa pidamas.
Et seda juttu kuulda oli, et Kodeste rahwa loomad Tareste Heinamaal käiwad, läks Kärdla wabriko walla Polizei sell 29 Aug. senna ja leidis loomad heinamaa pealt, kaks auku olid aja peale wõetud ja wäraw eest ära tõsdetud. Neli karja last olid juures ja loomi oli 32.
Karjane Madly Lees 17 a. wana, astus ette ja räägib et nende loomad heinamaale peasenud, aga aja peal olnud enne juba augud ja wäraw eest ära.
Karjane An Simmer 13 a. w. astus ette ja räägib nisama.
Karjane Jacob Post 11 a. w. astus ette ja räägib, et tüdrukud juba Laupääw enne seda, tahtud loomi heinamaale ajada, nemad seisnud wasto. Aga sell pääwal on Madly Lees ja An Simmer ühe augu aja peale teinud, tema ja Andrus Lüll teise augu ja lasknud loomad heinamaale.
Andrus Lüll 12 a.w. astus ette ja wabandab ennast.
Suud suud wasta tunnistawad aga et see nenda olnud kui Jacob Post wälja rääkind. Aga kes wärawa eest ära tõstnud, ei tea nemad. Tunnistawad ka et nende peremehed neid käskind kohtu ee waletada, et naad mitte pole loomi sisse ajand, seepärast pole naad warsi tõtt tunnistanud.
Nende peremehed astusid ette ja neid sai noomitud, et naad oma karjatsed kohtu ees käskind waletada. Ka sai küsitud: kas naad oma karjaste eest trahwi tahawad maksta wõi peawad naad ise seda kansma? Naad soowisid et karjatsed ise oma trahwi kannaks, et naad siis teine kord endid mõistwad hoida.
Kohus tegi otsust: et Madly Lees, An Simmer ja Andr. Lüll istuwad selle süü eest 3 tundi wangis. Jacob Postile ei saanud trahwi mõisdetud, et ta warsi tõtt tunnistas.
