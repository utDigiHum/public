Ükstuhat üheksasada neljandamal aastal Januari kuu kahekümnekuuendamal päewal ilmus Oro walla liige Hans Jaani poeg Hinnobert, isiklikult walla Kohtule tuntud ja palus, oma wiimase seadmisele, mis 1898 aasta 21. jõulu kp. Oro walla Kohtu aktide raamatusse sisse on kantud järgmine suusõnalise lisanduse juure teha: "Oma kasu poja Jaan Mihkli poja Suberti jätan mina kõigist minu päranduse pärimise õigustest ilma, selle pärast, et tema wastu minu tahtmist minu juurest ära läks ja et tema ei ole mind kui isa auustanud."
Mahatömatud "aastal" peale kirjutatud "päewal" õige.
See suusõnaline wiimne seadmine on pärast lepingute raamatusse sisse kandmist wiimase seaduse tegijale ette loetud.
Hans Jaani poeg Hinnobert XXX
Mitte kirja oskamise pärast tegi Hans Hinnobert oma nime juure om käega kolm risti.
Meie, kes siia alla kirjutanud tõendame, et seesama wiimase seadmise tegija Hans Hinnobert praegu terwe täie mõistuse juures on olemas.
H Truus &lt;allkiri&gt;
J Truus &lt;allkiri&gt;
mahatömatud: "aastal"
President: J. Spuul &lt;allkiri&gt;
Kohtu liikmed: H. Silwer &lt;allkiri&gt; P Kuur &lt;allkiri&gt; M Eringas &lt;allkiri&gt;
Kirjutaja Joh. Tilling &lt;allkiri&gt;
