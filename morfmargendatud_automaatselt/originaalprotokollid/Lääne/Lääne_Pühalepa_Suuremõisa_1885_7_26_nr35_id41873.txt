Hellamal 26. Julil 1885
S. Kogukonna kohtumaeas sai korrajärel kohtupäew ärapeetud, kus koos olid
Peakohtumees J. Muro
Kõrwasm. N. Johanisbeer
-,,- J. Kari
Suuremõisa metsaülema poolest sai kaibtuse Protocoll 17. Junist 1885 wallakohtu sisseantud, mille kaibtuse sise järgmine on, nagu kaibtus Protocollist lühetelt wäljawõetud.
1885 a. 4 Jani k.p. kel 7 homiko on mets põlema hakanud Kappasto Jüri pere rabas Nr 15 tüki sees. Tuli oli töömeeste Karl Korst ja Peet Jõe käest õnnetuseks lahti peasnud, mis kange tuule keer laiali lautand; seda on Peet Jõe kohe jooksnud teada andma Nõmba külasse, et abi saaks. Seda teadasaades on kustutama tulnud: Mäebli küla mehed, Nõmba ja Allato metsawaht, Kapasto metsawaht ja metsakubjas Espenberg,- Loja kubjas oma sulastega, Tubala talitaja oma küla meestega,- Tammela ja Undama küla mehed, Kerdla wabriko Baroni Herra oma kubja ja 20ne mehega ja Kassari Baroni Herra, Nõmba ja Tubala wabatmehed /kus mehed kodu olid./ Tulekustutamine on Kerdla w. Baroni hoolsa juhatamise al toimetud nenda, et see 5. Junil s.a. kel 3 p.l. suutumaks kustutud saanud. Polend mets(a)maa on kasinasti 1 werst pitk ja 450 sammu kõigelaiemast kohast lai,- mis ka kõik raba ja risu mets ja wanad oskad,- head etsa on wäha,- ka on kasinasti 2 tiinu seementud maad.
Tuld põle kustutama mitte tulnud Nõmbast Johan Tara, Mihkel Sepp, Andrus Karjama, Tubala Nigolas Tross, Undama Peter Karus, Juhan Luk. Enne tulekustutamist koeoläinud Nõmba Pedo perest Huuk ja Peet Kaew.
Juhan Tara kostis, et ta põle seeläbi saanud kustutama minna, et põle kodu olnud, waid S.mõisas käind- ja teisi põle tal enam kodu, kui naene olnud.
Mihkel Sep sulane Andrus Sarnak 14 a.w. ütles, et peremees on teda kustutama saatnud, mis aga kahtlane, kas tõsi, et ta kustutamas olnud ehk ei.
Andrus Karjama ütles, et ta põle mitte kodu olnud, waid hobust otsimas. Teine päew pidand minema,- Kaew ta wastu tulnud, ütelnud et tuli ju kustund, mis üle ta põle enam läinud.
Nigolas Tross põle kodu olnud waid on siis S.mõisas käinud renti maksmas.
Peter Karus põle kustumas käind, ega ka täna mitte kohtu ees olnud.
Juhan Luk põle kustutamas käinud ega ka täna mitte kohtu ees olnud.
Tema p. (Willem) Huuk, ütles, et Kustaw, et ta kustutamas käinud,- homiko ruttu ilma söömata läind ja p.l. kel 5 koeo enast toitma läinud.
Peet Kaew, enne kustutamast koeo läinud ja põle täna ka mitte kohtu ette tulnud.
Kohus mõistis, et tuli on Karl Korst ja Peet Jõe käest lahtipeasnud on Talor. S.r. § 638 järel suure kohtu kui trahwi al, aga et see "õnetus" on nend ekäest nõnda juhtund, jääb "Päris Herra" jäuks neile armu anda enda, ehk neid suurema kohtu kaebata, aga et põle muist kustutama läinudgi ja weel enne aeugu äratuld - ja kui tuli oleks weel laiemale laind igawest kahju teinod ja ka elud maead põletand, kes oleks siis selle eest - jõudnud kosta. § 631.
Mihkel Sep, et kül lapse kustutama saatnud, mis põle arwatagi, Andrus Karjama makswad kumbgi 1 Rbl. trahwi.
Nigolas Tross põle kodu olnud - on trahwist lahti.
Juhan Tara -,,-
Peter Karus ja Juhan Luk ja Peet Kaew makswad igaüks 1 Rbl. trahwi ja Kustaw Huuk kes enne aegu, kui tuli kustund, maksab 50 kop. trahwi.
