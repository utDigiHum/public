A. Lisa 1. Junil 1883. punkt 3, pag. 56.
Astus ette Mihkel Markus ja rääkis wälja: Tema ei olla mitte rohkem hoonid Alt-Hansu koha peält pärinud, kui üksi need hooned, mis moisa herra temale kätte näitanud. Moisa hertra poolt olnud tallitaja Adu Wiilmann kihelkonna kohtu körwasmees Juhan Ulm hoonede kätte andmise juure kutsutud, ja nende juures olemises herra näitnud missugused hooned temale saanud ja need olnud: üks ait, kellel parandat al ei olnud, mis Hans Alwin alt ära wötnud ja ühe laeta kuuri peäle lasusse pannud, need paranda puud saanud temale antud; aida körwas olnud üks kolme seinaga lamba laud, ka see saanud temale aidaga ühes antud, sest see ait olnud iseennesest wana ja lagunud. Kolme seinaga aita ei ole Alt-Hansu koha peäl olnudki. Ühe ue lauda olla ta koha hooneks saanud ja seda olla herra temale annud.
Koguk. kohus moistis, et Adu Wiilmnann ja Juhan Ulm peawad teisel kohtu korral ette tulema.
Kohtuwanem Mihkel Kuur XXX
Körwasmees Jüri Kleemann XXX
Körwasmees Jüri Saar XXX
Kirjutaja A. Krein &lt;allkiri&gt;
