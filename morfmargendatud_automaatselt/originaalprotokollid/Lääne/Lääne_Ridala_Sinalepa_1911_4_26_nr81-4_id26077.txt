Sell üks tuhat üheksa sada ühe teistkümmendamal aastal kahekümne kuuendamal aprilli kuu päewal /1911 a. 26 aprilli k.p./ ilmusiwad Sinalepa walla kohtusse Weetsa walla liige Liisu Jaani t. Weltsmann 47 a. wana, Mari Weltsmanni hoolekandja Jüri Jaani p. Piile 30 a. ja Jaan Mardi p. Martens 51 a. wana, kes kohtule isikulikult tuntawad on, palusiwad kohtu lepinkute raamatusse oma järgmist suusõnalist lepingut üles wõtta:
Mina Jaan Martens olen Wätse külast Reinoado No 16 talu koha Liisu Jaani t. Weltsmannilt ostnud, kus juures sai meite wahel leping tehtud nii wiisi, et mina annan Liisu Weltsmannile ja tema tumma õe Marile Reinoado elu majast ühe elamise tua weikese kojaga ja selle jauks tarwiliku küttuse elamiseks, ühe wakkama põllu maad Raja koha piiri äärt mööda ja selle üles harimiseks omalt poolt hobust ilma maksuta wõimalust mööda. Peale seda luban temale ühe lehma jauks suwel karjasmaa õiguse oma karjasmaal, ja talwel ise äralise ruumi lehma jauks, keda Liisu Weltsmann ise oma kulul peab ülewal pidama ja sõniku tarwitab oma põllu wakka maa peale ära. Selles lepingus nimetud tingimist peab Martens täitma nii kaua kui Liisu Weltsmann elab, kui ta aga peaks mehele minema ehk ära sureb, siis ei ole Martensil seda lepingut mitte enam waja täita. Kui aga Martens peaks selle koha ära müüma, ehk jälle Weltsmanni enne surma sealt koha pealt ära ajama ehk ülemal nimetud lepingut mitte täitma, siis peab Martens ega aasta Weltsmannile nelikümmend rubla /40 rubla/ selget raha tasu maksma kunni surmani.
See sinane leping on meite eneste selge meele mõistuse ja oma eneste kokkulepimise ja tahtmise järele kirjutud, mida oma allkirjadega tõendame.
J Martin &lt;allkiri&gt; L Welsmann &lt;allkiri&gt;
J Piile &lt;allkiri&gt;
Tunnistajad: J Jaaks &lt;allkiri&gt; A. Reets &lt;allkiri&gt; 
President J. Koplik &lt;allkiri&gt;
Liikmed: T. Nuut &lt;allkiri&gt; K. Möll &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Olen selle lepingust ära kirja wälja saanud 12 Septembril 1911 a.
L. Weslman &lt;allkiri&gt;
