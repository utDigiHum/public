Kohtu ette tuli Karel Krants ja kaibas, et tema peremees Andrus Stein, keda ta 3 aastad teenind ja kauba järele pidand riide palga saama. Muud riided olla peremees küll kätte annud, aga üks willane kuub ei tahta ta enam mitte wälja maksta.
Kohtu poolest sai kaibtuse alune Andrus Stein wastusele woetud. Tema ei saand seda mitte salgada, waid wabandas ennast sellega, et ta olla palju pisikesi riidid Kaarlile kingituseks teinud.
Kohus moistis: Andrus Stein maksab oma sulasele Karel Krants'ile se willane kuub kätte, mis tema kauba järele luband ja sellega on nende kohus lõpetud.
