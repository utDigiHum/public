12. Astus ette Juhan Krekow ja rääkis wälja: Temal olla Willem Tuiberg'i käest 1 rbl 75 kop. raha saada, see wõlg olla juba 3 aastat selle käest saada ja see lubanud ikka maksta, aga kunni seie maale weel mitte seda ära õiendadanud ei ole. Nüüd nõudis seda kätte saada ja ütles mitte wõimalik olewad temaga enam kannatada.
Astus ette Willem Tuiberg ja tellis seekord selle wölaga temaga weel kannatada ja lubas seda selle aasta sees weel ära maksta.
Kuhan Krekow lubas temaga weel kunni ueaastani kannatada.
Kohtuwanem Hans Uus XXX
Körwasmees Aadu Merjer XXX
Körwasmees Mihkel Kliss XXX
Kirjutaja A. Krein &lt;allkiri&gt;
