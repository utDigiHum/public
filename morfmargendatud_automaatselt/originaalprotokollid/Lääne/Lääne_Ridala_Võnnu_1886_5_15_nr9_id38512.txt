Ees käis Liisu Naap ja kaebas et ta mees Jaan wäga wali temaga olla ja pealegi haiguse ajal teda peksnud on.
Ette tuli Jaan Naap ja rääkis wälja: Tema ei kuule minu sõna waid sõimab mind ja räägib wastu ja selle süü pärast ma kord kergeste kääga lõin.
Kohtu poolt said molemad kowaste noomitud edespidi kõige pahanduse ja tüli eest hoida, ja jääwad walla walitsuse üle kuulamise alla.
Sellega leppisid mõlemad ära.
Peawanem Jüri Soone XXX
Kõrwamehed Aado Salme XXX Jaan Selg XXX
Kirjutaja J Klaanmann &lt;allkiri&gt;
