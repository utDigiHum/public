Kaebas Soone Jaan Jög et Undo Hanso Mardi kaks hobbost olla temma orrase pöllal käinud se aastal, kedda temma naene Marri olla wälja ainud ja kohhe Mardi tütrele ütlenud.
Ette tulli Undo Hanso Mart ja ütles: Minna sest asjast ei tea middagi, polle mulle sest kegi enne räkinud, kui tänna.
Jääb poolele, kunni selget tunnistust saab toodud.
Kohtowannem: H. Awik &lt;allkiri&gt;
Körwasmees: Hans Allik Ado Kask
Kirjotaja: J Reinans &lt;allkiri&gt;
