Et Keina koggukonna liige Tönnis Andruse poeg Tiik ennast omma naese ja lastega siit, walla hinge kirjast, tahhab wälja wötta ja oma issa Andrus Tiik ei tahha temma mitte oma seltsis ärra wia, se härrast töotab temma et temma keige se aeg mis temma issa ellab omma issa ülles piddamisse eest,tahhab murret piddada, nende et temma ilmaski wallale waewuks ei sa ollema.
Seda tunnistab sellega Tönnis Tiik et, temma omma saime alla ristid tömbab.
Tönnis Tiik XXX
Tallitaja: Hans Petersil
