Ette tulli Taewla walla mees Ado Eldema ja nouab omma käe raha 20 R 48 kop. mis Nowembri kuus Rein Jurs walla maksudega ärra selletas, et minno poia Hindriko eest piddi soltatiks minnema, aga sest ei tulnd ühtegi, et ei olnd ni ligi sugulassed kiriko ramatus.
Rein Jurs olli ka kohto ees; ei lubba taggasi, et tema aeg ärra raisatud; waidlesid.
Kohto mehed moistwad, et Kahjo peab poleks sama. 10 R. 24 kop. peab Rein Jurs tagasi maksma.
Kohto käiad leppesid selle moistmissega. Rein Jurs pallus aega, et Mai kuus siis arwas woimalik wälja maksa. Ado Eldema lubbas ka se aeg.
Moistetud ja lepitud.
