Kohto ette kaewab Edo Seppi, Mina Wiinbergi peale, kes temma õe mehhe teiseks naiseks olnud; ja et õe mees Hants Löpper aasta eest ärra surnud ja nüüd Mina Wiinberg teisele mehhele läinud ja lapsi ei olle surnud Hants Löpperil kummagi naisega olnud. Nüüd pärrib essimise naise õdde Eddo Seppi, sedda warrandust, mis siis seal olnud, kui Hants Löpper on koddo wäiks tulnud ja temma wannemade koha peale, ja õdde Marrid naiseks wõtnud: Üks nelja aastane ruun hobbune, kirstud astjad ja mitmed maja kramid, keik sinna jänud ja temma, kui kangemaks sanud wõera ette tenima läinud; pärrast ka mehhele sanud ja üks lehm ja kaks lamast kül sanud ja se wanna Saun on ka Hants Löpper, kui teise naise wõtnud ärra tassunud, agga mitte muud.
Mina Wiinberg tunnistab et temma muud asjo ei olle kuulnud wannast seal ollewad, kui ühhed kässi kiwwid ja üks wanna kerst ja astas. Need lubbab ta Edo Seppile kätte anda ja sedda on ka Hants enne surma käskinud.
Agga Eddo Seppi ei olle nendega mitte rahhul, waid pärrib sedda hobbust
Kohhus ei teinud mitte selle asja ülle ühte otsust siis tahtis Edo Seppi sedda kehhelkonna kohtus ette panna.
Kohto wannem Mihkel Sarijs XXX
Abbi mehed Jürri Triksberg XXX Willem Otsberg XXX
Kirjotaja: A J Martensohn &lt;allkiri&gt;
Se assi on Kehhelkonna kohhus ärra selletanud.
