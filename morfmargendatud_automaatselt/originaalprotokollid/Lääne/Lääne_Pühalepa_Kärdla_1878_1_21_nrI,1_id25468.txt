Koos ollid Tallitaja N. Mähle, abbimihhed G. Norwit ja H. Pissa seatud kohtopäwa piddamas.
Et mingisuggust kohtoasja polle toimetada olnud, siis sai maksu arrulised hinged ülleloetud ja krono ja walla maksud hingede peale ärrajautud ni kui Maksuarru ramat sedda 1878 a. kohta näitab, ja pärrast sedda läkks iggaüks omma koju.
