2. Astus ette Jüri Preis ja andis üles: Temal olla Söeruda poe rehnungi wõlast järgmiste siit walla liikmete käest raha pärida:1)			Rebaste Juri käest			1 rub 70 kop.		2)			Hans Landis käest			1 rub. 15 kop.		3)			Toma Aado käest			3 rub. 70 kop.		4)			Hans Uus käest			2 rub. 13 kop.		5)			Aua Mihkel käest			10 kop.		6)			Siimu Willem käest			50 kop.		7)			Tooma Kustaw käest			1 rub.		8)			Willem Landis käest			21 kop.		9)			Kustaw Wiikmann käest			30 kop.		10)			Jaan Nõupuu käest			7 kop.		11)			Wanamangu Ano käest			15 kop.		12)			Juhani Miina käest			21 kop		13)			Kustaw Kanau käest			10 kop		Ühte Kokko			11 rub 51 kop.		
Nende wõlglastest elab Hans Landis Tallinas ja ei ole kohtu kutset saanud, Tooma Kustaw om surnud ja Aua Mihkel on waewalise terwisega ja kaib armuandeid saamas.
Ja wõlgadest maksid:Rebaste Jüri			1 rub 70 k.		Toma Aado			3 rub 70 k.		Siimu Willem			50 k.		Willem Landis			21 k.		Kustaw Wiikman			30 k.		Jaan Nõupuu			7 k.		Wanamangu Ano			15 k.		Juhani Miina			21 k.		Kustaw Kanau			10 k.		Kokko			5 rub. 33 kop.		
Hans Uus lubas selle wõla 4 nädala ajal 2 rub 13 kop. Jüri Preisile äratasuda.
Kohtuwanem: Andr. Grosjääger XXX
Kõrwasmees: Juhan Krekow XXX
Kõrwasmees: Siim Kärm XXX
Kirjutaja: J. Tähe &lt;allkiri&gt;
Kõik poe wõlg on äratasutud.
See raha kõik on sissenõutud ja Jüri Preisil wälja antud.
Kirjutaja J. Tähe &lt;allkiri&gt;
