Ükstuhat üheksasada kümnenda aasta Märtsi kuu 29mal päewal ilmus Oru Walla Kohtusse Oru walla liige Gustaw Jüri poeg Tuulberg, kes isiklikult selle Kohtule tuntud on ja palus oma wiimast seadmist kohtu aktide raamatusse sisse kirjutada, mis järgmine on: Oma liikuwa ja liikumata waranduse, mis minult pärast minu surma järele jäeb annan mina oma lastele järgmiselt pärandada: Uugla mõisast ära lahutatud Kogri No 17 talukoht 44 teset. 945 ruut sülda suur annan mina minu mõlemale pojale Johannile ja Jaanile ühe wõrra pärandada, niisamuti ka kõik liikuw warandus. Poeg Johann maksab õe Liisale 50 rubla, ehk selles wäärduses üks lehm ja kaks lammast ja maksab wenna Jakobile, kui see nõuab 10 rubla. Poeg Jaan maksab õe Maiele 50 rubla, ehk selles wäärduses üks lehm ja kaks lammast ja maksab wenna Jakobile kümme rubla, kui see nõuab. Minu tütar Leena, kes mehel on, on oma osa kätte saanud ja ei saa peale minu surma minu warandusest enam mitte midagi. Kui minu naine Mari pärast minu surma elab, jäeb tema mõlema poja Johani ja Jaani juure Kogri Talu koha peale elama. Mõlemad pojad Jaan ja Johan on kohustatud tema eest kõikipidi hoolitsema, see on: kõiki elu ülespidamise tarwidusi ühe wõrra andma.
Peale selle saab minu naine minu liikuwast warandusest ühe lehma, tema oma soowimise järele. Lehma ülespidamise annawad Jaan ja Johann Tuulberg ühe wõrra.
Wahele kirjutatud: "üheksasada."
Mahatõmatud: "ehk."
See minu wiimane seadmine on minule enne minu poolt alla kirjutamist ette loetud.
Kustas Tuulberg &lt;allkiri&gt;
Mei Oru walla liikmed Jüri Jüri poeg Nõupu ja Johan Hansu poeg Bauwalt tunnistame oma allkirjaga, et Gustaw Tuulberg praegus selge terwe mõistuse juures on olemas:
Juhan Bauwaldt &lt;allkiri&gt;
Jüri Nõupuu &lt;allkiri&gt;
Kustaw Tuulbergi Johan Bauwaldti ja Jüri Nõupu omakäelised allkirjad tuinnistab Oru Walla Kohus õigeks.
President: G. Spuhl &lt;allkiri&gt;
Kohtu liikmed: H. Truus &lt;allkiri&gt; J Pros &lt;allkiri&gt;
Kirjutaja Joh. Tilling &lt;allkiri&gt;
