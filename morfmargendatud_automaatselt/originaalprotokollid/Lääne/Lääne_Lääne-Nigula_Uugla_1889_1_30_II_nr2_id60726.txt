Astus ette Kustaw Kleemann ja rääkis wälja: Tema olnud mödaläinud aastal Jüri Jling'it teenimas, see lubanud kauba ajal tale peale raha palga 1 koorm heinu anda ja aastased kogukonna maksud maksta; aga ei see ei olle heinu annud, ei kogukonna maksusid tema ettemaksnud ja peale seda pidada tema palgast 1 rubla ja lubatud käsi rahast 1 rubla 50 kop. kinni. Ka olnud ta ühe kuu aega peremehe kaupa mööda moisa härgi hoidmas ja see lubanud tale iga öö peaöt 10 kop. maksta, aga ei olla seda lubamist mitte täitnud.
Astus ette Jüri Iling ja rääkis wälja: Kustaw Kleemann olla teda möda läinud aastal teenimas olnud, 10 rubla ta lubanud selle palka ja 1 rbl. 50 kop. raha anda, aga heinu ei olla mitte lubanud. Ta olla sellele 13 rubla 25 kop. raha juba annud; sest mis peale 10 rubla wälja wöetud, saada 1 rbl. 50 kop. käsi raha, 1 rbl 46 kop. kogukonna maksud makstud ja 29 kop. olla see rohkem weel saanud, kui nende kaup olnud.
Kogukonna kohus moistis, et kumbagil tunnistust ei olnud, selle heina koorma hinda pooleks, nenda et Jüri Iling peab selle heina koorma hinnast poole hinda se on: 2 rubla Kustaw Kleemann'ile tasuma, kelle hinnast 29 kop. juba enne wälja makstud on.
Härgade öö hoidmise palga pärimise pärast sai moistetud moisa walitsuselt järel kuulata.
Kohtuwanem Peeter Weidebaum XXX
Körwasmees Jüri Ulm &lt;allkiri&gt;
Körwasmees Hans Priidam XXX
Kirjutaja A. Krein &lt;allkiri&gt;
