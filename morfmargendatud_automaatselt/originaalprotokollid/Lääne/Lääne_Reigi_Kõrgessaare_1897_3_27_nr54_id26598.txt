Ükstuhat kaheksa sada üheksa kümmend seitsmendamal aastal Paastu kuu kahekümne seitsmendamal päewal tuliwad Kõrgesare Kogukonna Kohtu awaliku koosolekule Kõrgesare walla liikmed Jüri Pitk ja Kustaw Wiin ja palusiwad nende poolt järgmist lepingud üleswõtta:
"Mina, Jüri Pitk Andruse poeg annan omma talukoha Mudaste külas oma wäimehe Kustaw Wiin Jüri poja kätte selle aasta 23 Jüri kp. ise jäed omma naese, tütre ja isaga senna koha peale kortri, ühe kambri sisse mis toa otsas on, ait jäeb ka minu kätte nii kaua kui seal kortris olen.
Loomadest jäewad tema kätte kolm härga ja üks 2a. wäss, muud midagi; nendest loomadest maksab Kustaw Wiin aeg ajalt mulle tagasi nõnda kuidas see temal on wõimmalik ja sell ajal, et see mitte töösi ei takista. Nii kuidas mull tahtmist on saan haitama töö juures, aga mitte suntuse teel, aga prii tahtlikult oma kauba kinnituseks oleme mõlemad alla kirjutanud.
Kustaw Wiin [allkiri]
Juri Pitk [allkiri]
Selle kauba lepingus nimetud inimesed Jüri Pitk Andruse poeg ja KustawWiin Jüri poeg on Kõrgesare kogukonna Kohtule (oma allkiri) palelikult tuntud ja õiguse omanduslikud aktide tegemise juures, mis kohus oma allkirjaga tunnistab.
