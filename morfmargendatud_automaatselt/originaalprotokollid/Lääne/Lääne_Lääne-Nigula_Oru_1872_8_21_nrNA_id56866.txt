Astus ette endine Maddise kohha rentnik Jaan Niggor ja kaebas nüüdse Maddise kohha rentniko Hans Olmanni peäle, kes temmal 60 sülla jäggo aia puid, roikad ja ka teiwad, ja weel 250 uut roikast ja 150 teiwast, 200 kubbu haggo, 1 paar pastlid, 2 küllimitto rukki jahho, 1 küllimit odre, 1 wakk Läätsa ja 84 rubl. rahha Kirstust ärra wötnud ja siis tühja Kirsto aidast wälja wiskanud, ühhed hobbose rangid ja ühhed sedelkad ja 2 wanna nahkmütsi, kumbki 25 kopp. wäärt, löhki kiskunud, 3 1/2 naela tubbakast ja 25 wihko õlga, 4 kolme süllast ja 2 kahhe süllast awa palki ja weel mõnda lühhikest palki tükki, 2 pu wankre rattast ja 30 kattuse arripuud, ärrawarrastanud ja raiskanud.
Hans Olmann astus ette ja tunnistas, et ta polle Jaan Niggori üllewel nimmetud Kramidest egga warrast muud prukinud egga wötnud, kui neid üllewel nimmetud 2 pu wankre rattast wötnud ja wee weddamisse wankre alla pannud, kellega wet saab Koio todud, kust ka Jaan Niggur omma wee tarwidust wõttab ja moisa herra lubbaga nende üllewel nimmetud kubbudega ennese rehhed kuiwatanud, sest need kubbud ollid Maddise Kohha made peält raiutud ja nisammoti moisa herra lubbaga 10 sülda pu aeda, üllewel nimmetud Jaan Niggori aia pudest teinud.
Kohhus ei moistnud kumbagile õigust sest meil ei olnud tunnistussi, ei Kaebajal egga kaewatawal.
Kohtowannem Ado Oltman XXX
Körwasmees Hans Willik XXX
Körwasmees Hans Kork XXX
Kirjotaja A. Krein &lt;allkiri&gt;
Tunnistust ei olnud sepärrast ei moistnud Kohhus ühtegi öigust, jäi Kihhelkonna Kohto holeks.
