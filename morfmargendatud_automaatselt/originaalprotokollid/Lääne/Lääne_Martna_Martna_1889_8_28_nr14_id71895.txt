Kohtu ette tuliwad Jaan Kriintal ja Kustas Matweus, nad antsiwad üles ei neil kodu nabruses, teine teisega tüli olla olnud ja tahta leppida, aga ei arwata kodust leppimist mitte kindlaks, sellepärast paluda nad kohust seda toimetada!
Kohus noomis neid selle peale ja tuletas neile meele et se mitte hea ei ole kui naabred tülitsewad, waid et nad peawad rahu sees teineteisega elama ja edaspidi ikka meeles pidama et nad siin kohtu ees on ära leppinud.
* Se protokol sai politseisse saadetud.
