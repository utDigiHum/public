Astus ette Rein Päll ja ütles wälja et temma oma poeg Jürri Liiso Pall kaswandikuks lubbab sest et Liiso Päll temma (Jürri) eest jubba lapsest saatik on muretsenud ja tedda kaswatanud nüüd tahawad seda kohtu läbbi kinnitada.
Liiso Pall astus ette ja ütles wälja et nemad Rein Pälliga on sedda kindlast teine teise wasta lubbanud et Jürri Päll Reino poeg Liiso Päll kasso poiaks jääb sest et Liiso omal mitte lapsi ei olle.
Kohus moistis et peawad Kihelkonna kohtu ette kinnituseks seda wiima.
Sellepärrast et need asjad Kihelkonna kohtu ette tulewad.
