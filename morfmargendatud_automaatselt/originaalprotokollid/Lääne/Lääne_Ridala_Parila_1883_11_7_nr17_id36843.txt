Mihkel Täht astus kohtu ette selle otsuse peale ja ütles et tema seda ial ei sa täitma ega ka kohus temale seda peale ei woi panna temma teada seadused parreminni kui kohus.
Kohus moistis et Mihkel Täht 24 tundi saab wangi pantud.
Selleparrast et kohtu ees teotawaid sõnnu pruugib
Kohtuwanem Jaan Jünter XXX
Kõrwamees Juhan Krabbi XXX
Kõrwamees Madis Topp XXX
Kirjutaja M Kentmann &lt;allkiri&gt;
24 tundi wangi täitetud
