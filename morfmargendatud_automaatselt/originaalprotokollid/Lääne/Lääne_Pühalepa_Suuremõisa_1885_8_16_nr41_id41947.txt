Sai ettewõetud, mis Nr 36 al sissekaebatud. Peter Pähel oli ees ja ütles, et ta põle mitte neid 3 puud warastanud,- mis tõsi.
Tunnistus Nigolas Wina ütles, et ta põle seda mitte näinud, ega põle wõinud ka mitte Jüri Kibuspu wastu tunnistada, et Peter Pähel warastanud need puud. Kibuspu on seda muidu arwanud, et Pähel pärast metsas olnud, ehk aga warastas - ja nõnda nime ülesannud.
Et see asi segane ja ilma õige tunnistuseta jääb niisamma.
