Andis endine (talitaja) Peet Regi üles, et tema on 1881. aastal Hapsalu postimoona, kaerde ette omast käest muist raha 8 rbl. 93 3/4 Kop. maksnud, mis ka kwitungid näitawad, aga kohus ehk wallawalitsus ei taha tale seda nüid maksta ära.
1881 a. maksnud P. Regi postikaerde raha, 17 Tsehv. 7 Tsch. ette (a. Tsetw. 6 r. 50 kop.) kokko 116 r. 18 3/4 k.
Transport 116 r. 18 3/4 k.
Walla walits. poolt antud raha ligi 107.25
Oma käest maksnud 8 r. 93 3/4 k.
Kohus arwas, et postikaerde ette sissemakstud odre seesamma (1881 a) aastal 15 mänik äramüinud a 1.75 kop. Saand männiko pealt 25 kop. kasu, teeb 15x25k: = 3 r. 75 saab tagasi arwatud, sest ta oleks pidand odrad Hapsalo wiima ja mitte paristama.
Tagasi kätte saada 5 r. 18 3/4 k.
mis eespoole saab weel õiendud taga.
