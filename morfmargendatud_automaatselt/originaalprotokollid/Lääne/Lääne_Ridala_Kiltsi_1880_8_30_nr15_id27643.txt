Kohto mehed ollid koko tulnud Katko Kolimaiase. Ja wõtsid ette Kirjotaja J. Grünäri Kaebtusse mis naabri mees Juhan Luk ja Willem Altom omma pollo aeda ei te üsna lausa maas peawad ja kui J. Grünär lomad põllo peale tullewad ilma innimeste näggematta aiawad naabrid neid, nende omma põllole kahjo teggema.
Juhan Luk ja Willem Altom ollid ka ees ninig ütlesid:"maa möödo ahhel on aed iga üks hoidko omma lomad; tullewad naad meie põllo peale, aiame kohhe temma omma põllo peale."
Kohto mehhed taplesid ni sugguse ülle meeltse teo eest ja moistsid et: Juhhan Luk omma pole ma rentiaga Willem Altomiga, peawad põllo aia ni tuggewaste ülles tegema et lomad ja sead kinni peab; ühhe näddala aia sees, ei tohhi mitte melega teise wilja lomi aiada pahhandusseks. Kople aed peab poleks tehtama.
Kohto mees: Mihkel Poots XXX
I Kohto Kõrwamees Mihkel Juhkam XXX
II Kohto Kõrwamees Jürri Laur
Kirjotaja: Jaan Grünär &lt;allkiri&gt;
Moistetud.
