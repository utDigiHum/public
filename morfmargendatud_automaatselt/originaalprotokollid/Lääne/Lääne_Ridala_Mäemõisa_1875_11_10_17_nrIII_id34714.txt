Said ette kutsutud Johan Kroni Kidewa wallast ja Ado Kreis Parrila wallast ühhe kirja peal, mis Kidewa Koggukonna Kohtus No 14 al antud ja seal Kohto ees on olnd nende ärra kaddund wõrkude pärrast.
Tullid ette Johhan Kroni ja Ado Kreis ja nende wastane Mäewallast Lambajani Jaan Sass, ja teggid siin kohto ees mollemilt poolt leppitust, sest mollemad ollid selle läbbi Kahjo saand; se Parrila laewa kipper on Mäewalla Jaan Sass laewa meeste wõrkudest nelli wõrko ärralõhkund omma üllelaskmissega, ja Jaan Sass omma wõrkude watmissega nendel kaks wõrko lahti lasknud mis koggoni kaddund on; arwasid mollemilt poolt kahjo tassumist tullewad, siis leppisid siin kohto eest ärra.
Allakirjotand: Kohtowannem
Willem Prosowits XXX
Mihkel Reets
Hindrek Reets
Kirjotaia J. Spuhl &lt;allkiri&gt;
