Riddali Kihhelkonna Kohtu otsus selle Parrila, Kebla ja Riddala kirriko walla koggokonna kohtu Protokolli ülle 1874ma aastast sel 9al Detsember 1874.
Protokolli ramat on puhtaste ja heaste kirjutud, järrel kuulamine ja moistminne on kolwalik ja ei olle mitte tühja wiwimest olnud ja kohtu au on üllespetud. Agga moistmesed on muist weel täidmatta; laega jäuks on nimmelt trahw maksmatta No 10 Tõnnis Reints 1 rub. 50 kop. ja No 12 Prido Jaaks 1 rubel.
Middendorff &lt;allkiri&gt;
Riddala kihhelkonna kohtu peawannem
