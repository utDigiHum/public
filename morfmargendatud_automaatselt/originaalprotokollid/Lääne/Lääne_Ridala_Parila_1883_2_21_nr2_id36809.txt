Astus ette Willem Niin ja teudas kohut ja ütles et kohtumehed pidada sübruses kohud ja ei olle temaga õigust tehtud; temale sai küll ärra seletud et wõib suurema kohtu kaebta kui arwab omal oigust olewad aga tema teotas edasi ja ütles et keegi kohus õigust ei tee.
Kohus moistis Willem Niin 48 tundi peab wangi saama pantud 1221 § T.r.s.
Selepärast et teine kord enam ei tohi oma rumalaid sõnu kohtu wasta pruukida.
Kohtuwan, Jaan Jünter XXX
Kõrwamees Juhan Krabbi XXX
Kõrwamees Madis Top XXX
Kirjut. M Kentmann &lt;allkiri&gt;
täidetud 48 tundi wangis
