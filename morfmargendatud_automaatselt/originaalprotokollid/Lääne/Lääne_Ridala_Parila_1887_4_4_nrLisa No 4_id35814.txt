Ette sai tootud Kaarel Iie ja ütles wälja et tema pole mitte Liiwa kõrtsus salaja juttostand, ei ole tema ka Peeter Wau'ga kusagil seltsis käind.
Ette astus Peeter Wau ja ütles seda samma wälja mis Kaarel Iie.
Ette astus Unesti walitseja A Andershon ja ütles wälja:"Mino wasto tulid ja hoidsid endid silm näud mino eest kõrwale, sõitsid Kaarel Iie kodust mööda sest sain arro et oma wälja sõito mino eest wabandada tahtsid; sellepärast kuulasin mina järele kus nemad käinud ja sain kuulda et nemad Martnas käinud ja sealt weel eemal. Ja Peale selle on ka Hans Rätsep mino wasto tunnistanud et Kaarel Iie on temal rääkinud et seltsis Peeter Wau ja Kaarel Noks'iga need hobose riistad olla warastanud.
Ette sai tootud Kaarel Iie ja ütles wälja peale Andersoni ülesseande ette lugemist:"Mina käisin Kaarel Noks'iga Lähtro Arro kõrtsus kala kuulamas, meis sõitsime 30mal Märtsil õhti senna ja 31sel tulime tagasi, õhti enne päewa loojak, käisin Kõusalo saunas Hans Rätsepa juures wihtlemas, ja ei ole temale seal ühtegid rääkinud. Andersoni eest pole meie entid warjand, waid olime joobnud.
Jääb poleli et tulewa kohto päewaks, Andersoni tunistused ette kutsuda ja Kaarel Nokkas ka kohe wahi alla panna.
Kohtowanem: Jaan Junter XXX
I Kõrwamees: Kustaw Neider XXX
II Kõrwamees: Jaan Kahm XXX
Kirjotaja:
