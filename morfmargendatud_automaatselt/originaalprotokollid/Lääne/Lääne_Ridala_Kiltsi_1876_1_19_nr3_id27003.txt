Ette tullid kässo järrel 1875 aasta 13mal Januar No 4 protokol kohto al wabbandajad (No 7 protokol) Mihkel Jänt, ja tema wasto, endine kohto ees (No 16 protokol 31el Märts 1875) wabbandaja, No 65, 74 ning No 75 22el Tetsember 1875 tunistaja Juhhan Madsik, kes ikka No 75 protokol tunistust kinnitab (1875).
Mihkel Jänt ütleb ja wabbantab, et tema Juh Madsik Laupä tört käind pallumas, ja tema aiad heino re pealt püksata hobbuse ette eies mis kessi sees olli; pühhade kaupä (1874/75)
Kohtomees (M. P. ja Kirjotaja , wannad) mälletawad selgeste et wahhe juttude sees ei räkindgi Mihkel Jänt mitte enni Joulo pühhi heino aiawad laudile, waid pühhade wahhel (No 7 3mal Webruar 1875)
Kohto mehed küssisid kus nemmad tahawad kohto otsust sada, woi edasi minna..?
Mihkel Jänt ja J. Madsik mõllemas sowisid et "siin".
Siis moistis kohhus: Et Mihkel Jänt on tõeste süi allune, ja peab teist modi heinad wälja maksma kui se No 16 protokolis Juhan Madsiko peale moisteti. M. Jänt maksab 5 Rubla heinad ja laeka 4 Rubla trahwiks, Suma 9 rubla.
Juhan Madsik, et wabbandust otsind terwe aast ja naabri häbbi tahtnud katta, ommeti trahwi maksma laeka 2 Rubla hõbbe.
Kohto mees Mihkel Poots XXX
I Kõrwamees Jürri Rabba XXX
II Kõrwamees Mihkel Mikkas XXX
Kirjotaja Jaan Grünär &lt;allkiri&gt;
Hagenrehteri kohtu antud.
Seal on sedda kohto otsust kinitud ja tallitaja R. Laar kätte oksjoni tehha moistetud. No 26
Hagenrehteri kohtus: J. Madsik maksis 2 R. laeka
