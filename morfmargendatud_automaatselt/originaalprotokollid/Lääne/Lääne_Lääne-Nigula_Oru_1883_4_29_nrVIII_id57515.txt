B. Lisa 4. Märts s.a. punkt 4 pag. 37.
Astus ette Pridik (Willem) Toner Kerwlast ja rääkis wälja: Kui Peter Kesker esimese heina kuhja 8 rubla eest Jaan Antonsoni käest ostnud, siis Antonson üttelnud, sest kuhjast 2 koormat ja weel rohkem heinu saawad, aga kui ta P. Keskeriaga heinu peäle teinud, pole naad enam, kui 1 1/2 koormat saanud.
Kogukonna kohus mõistis, et Jaan Antonson peab ueste ette tulema.
Kohtuwanem Hans Hennubert XXX
Körwamees  Jüri Steenmann &lt;allkiri&gt;
Körwamees Madis Nothmann XXX
Kirjutaja A Krein &lt;allkiri&gt;
