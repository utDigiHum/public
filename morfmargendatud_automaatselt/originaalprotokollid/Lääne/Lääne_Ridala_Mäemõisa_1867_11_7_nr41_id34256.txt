Ette tulli Rein Sass ka kaewas: et Prido Lahhe temma käest rahha kolm rubla kuuskümmend koppik (3,60 Kopp.), Kui nemmad Perno tepeal on olnud; selle lubbadussega, et Kui Koio saab siis Kohhe ärramaksan. Nüüd ollen mitto kord temma kaest samas käind kus ta mind agga haugutab ja rahha mis wõlga ei maksa.
 Prido Lahhe sai ette kutsutud, Kes tunnistab et temmal wõlgga on, agga meie jõiime selle rahha te peale seltsis ärra pole mul maksa.
Kohhus moistis; et Prido Lahhe peab pole sest rahhast maksma, kui pearahha maks tulleb, ja teise polega lubbab Rein kannata Kunni Näri Kuni, Kui siis ei olle keiki makstud peab tallitaia temma merrepüüstest wõtma, et ni paljo hinda saab, ja Reinole maksma.
Koos ollid kohtowannem Jaan Sass XXX
abbimehhed Jaan Spuhl XXX Juhhan Karjus XXX
