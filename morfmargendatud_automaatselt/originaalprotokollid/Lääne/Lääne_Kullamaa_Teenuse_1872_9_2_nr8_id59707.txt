Kohtu korra pärrast ollid Tenuse walla kohtuse kokko tulnud.
Kohtu wannem Hans Ribert
Kohtu kõrwa mees Jaan Otsus
Kohtu kõrwa mees Prits Paumann
Kirjutaja Pridik Selmann.
1. Astus ette Kustaw Nugga, ja kaebas et tema on Paisootsa Jaan Tammerti ühhe sui peale tenima läinud, ja Jaan Tammert on temmale palgaks lubbanud 2 1/2 Tündert Rukkid ja 2 1/2 Tündert Odre ja üks paar linnased püksid, üks särk, üks paar sukkad, üks paar kindad, üks poolküllimitto linno mahha tehha, üks Tündremaa heinamaad; Agga seitse näddalid enne Mihkli päwa ajas ta mind ärra, ja ei tahha mind mitte ennam tenima.
Sellepärrast nouan ma omma palka mis peale meie olleme kaupa teinud, Jaan Tammerti käest.
Jaan Tammert wastab, et nemmad on metsas heinamal olnud, ja wikkati on sandiste otsa pantud, misga Jaan Tammerti tüttar niitis, ja sellepärrast on nemmad tüllise läinud, ja Kustaw Nugga on selle tulli pärrast ärra läinud. Ja Jaan Tammert lubbab Kustaw Nuggale sest palgast mis peale nemmad kewwadi ollid kaupa teinud, weel anda üks Tünder Rukkid ja üks Tünder Odre ja 4 Rubla 50 Kop raha.
Ja Kustaw Nugga olli sellega üsna rahhul, sest ta ei olla ennam immustanudgi, ja nemmad leppisid ilma kohtu moistmisseta.
Kohtu wannem: H. Riberg
Kohtu kõrwamees: J. Otsus
Kohtu kõrwamees: P. Paumann
Kirjutaja: P. Selmann
Leppitud.
