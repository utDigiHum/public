Tulli kohto ette Mäewalla perremees Karel Kreek ja kaewas, et temma wõrgo maiast Kalla randas on üks wõrk ärra warrastud ja nüüd ollen tedda leidnud Kidewa walla wabbadmehhe Ado Winglas ette, Kes tunnistab, et temma ei olle mitte sedda wõrko warrastand, waid sedda on üks teine temma maia lae peale toond, ja temma on sedda sealt leidnud kotti sees, ja polle sedda teadnud pakkuda kellegil.
Et tunnistust ei olle, ja se wõrk tõeste Kaarli omma on, et wõrgul märklaud peal, ja kegi sedda ei woi pärrida, mõistis Kohhus, et Ado Winglas peab seda wõrko Kaarli kätte andma.
Koos ollid Kohtowannem: Juhhan Kellemet
Jakub Spuhl
Jürri Walgemäe
