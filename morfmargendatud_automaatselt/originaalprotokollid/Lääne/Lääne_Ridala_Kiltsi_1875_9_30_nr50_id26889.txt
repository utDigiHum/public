Ette Sai wõetud Kiltsi päris Härrast kaewatud poste Mihkel Saar ja Gustaw Toomseni peale, et Kewade Kiltsi moisa seest, poisid on 3 laste mängi Kurni ärra wiind ja enni mitte teada tulnd kui nüüd kes need ärra wiad olnd et kül kurnid jälle kohe kätte todud, Kutserile.
Mihkel Saar 17 aastane olli ees ja ei aiand tagasi, wait ütleb: et tama anslangri töös olnd Gustaw Toomseniga, päwa need kurnid näind, ühe tasko panund, õhto Gustawile näitand oues, Gustaw ommale ka tahtnud; et aknad lahti (lahti pärrast) üpasin sisse ja wõtsin kaks weel ärra, 1 sai Gustawile ja 2 andsin kodo teiste poistele, meie ei arwand neid ennam sakstel tarwis miniwad, ja prügi sisse jäetud, pärast kui neid järrel kaebati, wisin Kutseri kätte.
Kohto mehed taplesid poistega ja otsesid wahi mees kes witsu pidi andma, poisid pallusid muud trahwi, ja siis pidid iga kurni eest üks pä Kärneri jures aedas tööl ollema.
Kui mitte sawad witsu 5 hopi Kurni eest.
Moistetud.
M. Saar täitnud 28mal Now. ue sundimissega.
No 63
