Üks tuhat kaheksa sada üheksakümmend üheksamal aastal, Nääri kuu kahekümne wiiendamal päewal Räägu-Kerwla mõisa omanik Richard Bader ja Oro walla talupoeg Aleksander Andruse poeg Kliss, kes kohtule isiklikult tuttawad on palusiwad selle kohtu lepingute raamatusse oma suusönalist järgmist lepingut kirjutada:"mina, Richard Bader, annan Aleksander Klissile 50 /wiiskümmend/ sülda kase halupuid, nii wiisi, et pooled süllad on 4 jalga körged ja 7 jalga laiad ja halud ise 18 tolli pitkad. Neid puid wõib Kliss metsast wälja wedada siis kui tahab, aga mis metsa jääwad weel käes olewa aasta suweks, nende kadumise korral wastutamise wõttan oma peale, ja annan luba Klissile oma hobust Kärbla mõisa härja koples pidada selle kordadel kui neid puid Hapsalusse ehk mujale weab."
Mina, Aleksander Kliss, maksan nende puude eest Richard Baderile tänasel kauba tegemise päewal 75 rubla, aga wõlgnewa summa 112 rub. 50 kop. maksan 2sel Paastu kuupäewal 1899a. nende samma puude eest temale, BNaderile, wälja.
R Bader &lt;allkiri&gt;
A Kliss &lt;allkiri&gt;
Lepingut tegijad pooled on oma käega allakirjutanud.
President: A Kaaw &lt;allkiri&gt;
Kohtu liige: J Kreekow
Kohtu liige: J Lindal
Kohtu Liige: J Kleman &lt;allkiri&gt;
Kirjutaja Joh Weksar? &lt;allkiri&gt;
