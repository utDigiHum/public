7. Astus ette Willem Blink ja räkis wälja: Jaan Olmann räkinud 2. Nääri k. p. s. a. oma wenna Hansu wastu ja pärast tunnistanud Kerbla Rägo kog. kohtus et (ta) minu hobone olnud sügise Kerbla Kopleküla Retsepa pere aedas sömas, ja üttelnud, seda minu enese käest kuulnud olewad, et hobone seäl olnud; ja ta käinud 3mal Näärik. p. Jaan Olmanni jures, agga pole ta se kord mitte teadnud seda räkida, et ta hobene nimetud kohas söömas olnud, ja enne ega pärast pöle ta senna saanud.
Astus ette Jaan Olmann, ja tunnistas wälja, enese wenna Hansu wastu 2. Näärik. p. räkinud olewad Willem Blinki enese juttu peäle, et ta hobone nimetud pere aedas sügise söömas olnud, nisammuti tunnistas wälja, et Willem Blink 3. Näärik. p. tema wastu räkinud, et ta hobone seäl olnud.
Otsus: et Jaan Olmann 2. Januaril oma wenna Hansu wastu, Willem Blinki enese juttu peäle ütleb räkinud olewad, et ta hobone seal olnud, ja nisammuti ütleb seda 3. Januaril Willem Blinki käest kuulnud olewad, sest oli tunda, et J. Olmann wallet on räkinud, sepärast
Kog. kohus moistis, et J. Olmann selle wale juttu eest peab walla laeka heaks taha trahwi 3 rubla höbb. maksma. J. Olmann maksis kohe wälja.
Kohtuwanem Hans Hennubert XXX
Körwamees Jüri Steenmann XXX
Körwamees Maddis Noothmann XXX
Kirjutaja A Krein &lt;allkiri&gt;
