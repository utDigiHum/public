Üks tühat üheksasada kolmeteistkümnemal aasta Nääri kuu seitsmendamal päewal ilmus Oru walla kohtusse, Oru walla liige Mari Mihkli tütar Loorents kes kohtule isiklikult tuntud on ja awaldas mina palun walla kohut aktide raamatuss üles märkida järgmist: Minul on Oru wallas Madise talu poole koha peal Johannes Maltsa maja kõrwa üks kamber ehitadud mille ehitamise kulu mul maksma läks 300 rubla Juhtumise korral kui Johannes Malts oma koha ära müib ehk oskjuni teel koht ära müiakse et mul õigus on pärida, Maltsa käest. Maja wõib Malts ühes kohaga tükkis müia kellele tahab kui kaupa saab. Johannes Malts wõib selles kambris rahulikult elada ainult et poole kammert mul tarwitada on.
Oru walla liige Johannes Au p. Malts tõendab et tema maja külgis Mari Loorenti kamber on ehitatud mille sees Mari Loorents praegu elab ja milles sees mina Malts praegu elan.
See akt on enne meite alla kirjutust ette loetud millega meie rahul olem.
Mari Lorents kirja ei oksa tema palwe peale kirjutas alla K Niiler &lt;allkiri&gt;
J Malts &lt;allkiri&gt;
Tunnistab õigeks
President: W Põllus &lt;allkiri&gt;
Liikmed: A Paius &lt;allkiri&gt; W Nootmann &lt;allkiri&gt; A. Nork &lt;allkiri&gt;
Kirjutaja: J. Saar &lt;allkiri&gt;
