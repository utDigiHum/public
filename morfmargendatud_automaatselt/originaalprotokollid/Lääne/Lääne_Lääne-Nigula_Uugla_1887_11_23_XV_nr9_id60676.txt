Astus ette lesk Mari Dannbusch ja rääkis wälja: 3mal Oktobri k.p. s.a. ta olnud enese lehmadega Uemoisa walla heinamaa peal, ja Ugla walla heinama waht tumm Mihkel Kruus tulnud ta juure ja löönud teda esiteks ühe paiju keppiga selga ja pähä, kui see katki läinud, siis ta wötnud ühe sarapuu keppi ja löönud sellega weel, nenda et ta neid hoopisid ei olla lugeda wöinud. Tal olnud 2 rättikud Pea ümber, need läinud neist hoopidest löhki, ja Pea paistetanud tal üles: ta näitanud enese haawatud Pead tallitaja abimehe Hans Lauberg'ile.
Astus ette Jaan Dannbusch ja rääkis wälja: Tumm Mihkel Kruus olla tema ema palju peksnud, kellega ta leppida ei wõiwad; iseäranis möned walla peremehed olla tumma enestele heinamaawahiks pannud ja temale luba annud neid peksta, kes loomadega heinamaa peäle lähewad.
Mihkel Waripuu (wollimees) ja Hans Lauberg (tallitaja abiemees) rääkisid wälja: Juhan Ulm, Jüri Ulm, Mart Luubert ja Jaan Steenberk olla tumma Mihkel Kruus enestele heinamaa wahiks pannud ja temale luba annud iga ühte peksta, kes loomadega heinamaa peäle lähewad. Hans Lauberg ütles ka näinud olewad, et Mari Dannbusch'i Peas hoopidest, mis tumm löönud suur üles paistetanud muhk olnud.
Astusid ette karjahoidjad: Kaarl Beetsman, Mihkel Waripuu ja Tiu Kruus ja rääkisid wälja: Nemad näinud, kui tumm Mihkel Kruus ühe piitsa warre jämeduse paiju ja sarapuu keppiga lesk Mari Dannbusch'i löönud, nemad ei olla neid hoopisi mitte arusse panna joudnud, sest neid olnud palju. Nende loomad olnud Uemoisa walla maa peal ja ise naad olnud Ugla walla maa peal.
Kogukonna kohus moistis seda asja hakenrihteri kohtu alla.
Kohtuwanem Mihkel Kuur XXX
Körwasmees Jüri Kleemann XXX
Körwasmees Jüri Saar XXX
Kirjutaja A. Krein &lt;allkiri&gt;
