Kaewas härra Hirsi asemisel Priidik Pussman et Wiio Poom Sr. Lähtrost Hirssi käest 2 tünd 2 waka kartoflid wõtnud ja mitte seda maksnud pole se on 3 rubla ja palus seda kohut sundida.
Poomanni käest kas se wõlg oige on mis tunnistust sai ta tunnistas seda oigeks ta ütles seda niisammuti puudose pärast maksmata jäänud olewad.
Kohus mõistis et W. Pooman peab selle wõla 3 rubla tänasest päwast arwates 4 nädala aja sees härra Hirssile äramaksma ja kwiitund kohtule ette näidata ei ole se mitte sündinud siis maksab ta 1 1/2 rubla trahwi walla laeka.
Kohtuwanem: M. Reinberg XXX
Körwamehed: K. Tendrik XXX P. Laal XXX
