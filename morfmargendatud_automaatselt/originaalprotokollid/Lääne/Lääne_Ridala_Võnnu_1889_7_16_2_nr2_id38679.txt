Täna oliwad asjalikult koos Tallitaja Hans Möll
ja kaks selle asja tunnistajad I Jaan Prik II Hans Unma
Kirjatometaja G. Genther &lt;allkiri&gt;
Ette astus Jõesu walla mees Tõnnis Mang kes Unesti mõisa maa peal elustab ja kaebas Öösel sell 12/13 Juulil lõikasiwad kaks poisi minu aedas nugadega minu noore õunapuu sisse kaks krammi.
Sell 16ml Juulil 1889 a. seda järelwaadates leidsime meie, et tema kaebtus õige oli Kuritegu mis need poisid on teinud on õunapuuga sündinud, kes elumajast 19 jalga eemal aedas kaswab,tuwist kuni ladwani 4 arsinat pitk, tuwi jäme 3 1/4 tolli; maast mõetes 1 jalg korguseni on nugadega kaks krammi sisse lõigatud, esimme kramm kell sügawus on ühelt poolt 3/4 tolli teiselt poolt 1/2 tolli, nenda et see puu üsna ära rikutud on.
Selle asja tunnistused kirjutasiwad omad nimed alla.
Tallitaja: Hans Möll
Tunnistuse mees I Jaan Prik XXX Hans Unma XXX
Kirjatoimetaja: G. Genther &lt;allkiri&gt;
See protokol on Maapolitsei IIse jauskonna Noorema abilisele sisse saadetud 19ml Jaanuaril 1890 a. No 1 all saatekirjaga
