ad 4.
Kohtu ette sai kutsutud Adu Timm ja sai temale ette loetud, mis protokollis 4 Nowemb. s.a. 21sel mõistmisel temale sai mõistud, küsitud mis ta seda ka nüid nelja nädala aia sees põle täitnud, aga ta rääkis suurelised ja rumalad sanad wastu, kohtu toast wälja minnes, pani ta kohtu toa ukse nii waljusti kinni, et kõik kohad põrusid.
Kohus mõistis A. Timmile ta suurelise ja rumala wastupanemise pärast 30 witsa hoopi, mis ka kohe täidetud sai.
