Kohto ette tullid 6ma Septembri Kohto Käiad Loa Jaan Sõelsep ja Rein Tam oma poiaga, ja ütlesid et nemmad polle käind moisa Härra jures kumgi ja sowiwad kohtu otsust.
Kohto mehed kulasid weel korrd neilt järrele wõera hooste pärrast ja ei olnd ühtegi muud wastust, Siis mõistwad, et Rent Härra peab essite sedda Kahjo maksma 13 Rubla suruseks, ja kui hooste poisist süid leiab saatko teda kohtu.
Kohto mees Mihkel Poots XXX
I Kõrwamees Jürri Rabba  XXX
II Kõrwamees Mihkel Mikkas XXX
Kirjotaja: Jaan Grünär &lt;allkiri&gt;
Mõistetud. täitetud.
Hagenrehteri kohtu sundmussega 1877 18mal April.
