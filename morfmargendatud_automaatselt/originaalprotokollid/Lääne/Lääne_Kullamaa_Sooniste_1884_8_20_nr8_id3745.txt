Kohtu ette kaebas metsa wörster Runge, et Ans Wisitam olla temale kaebanud: Et metsawaht Jüri Ilwes olla metsast 4 puud ära müünud tema teada kännud kuskohas naad olla ilma märkimatta.
Ette sai kutsutud Ans Wisitam ja üle kuulamise läbi tuli wälja, et Ans Wisitam jälle tühja kaebanud oli.
Sepärast mõistis kohus, et Ans Wisitam maksab 3 rbl. (kolm rubla) trahwi.
Kohtuwanem: J NeumannKõrwamees: J Tomann" : J LaursonTalitaja: J Wesiradt [allkiri]Kirjutaja: M Kruup
