Tulli ette Ristimäe wabbatik Leena Pad ja kaebas, et Kabrametsa külla poisid Ollikse Willem, Kömmi Jaan, ja Madise Willem ja Ado ollid meite kaali aedas käinud, ja paljo kahjo teinud wargusega.
Tallitaja abbimees Johhan Jallak räkis: Minna ollen sedda kahjo näinud, et 24 kalikast olli ärra widud, ja poisid ollid karjas lõkket teinud, ja kupsetud kali kored ollid maas.
Siis tullid need Kabrametsa külla poisid ette ja waidlesid wasto, et nemmad ei olla käinud egga toonud; waid Tame Mart ja Padama Willem olla warrastanud.
Jääb tänna polele ja saab neile käsk antud, et poisid peawad tullewal kohto päwal keik ees ollema.
