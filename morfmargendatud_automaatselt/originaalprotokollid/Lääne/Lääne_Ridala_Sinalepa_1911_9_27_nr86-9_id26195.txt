Sell üks tuhat üheksa sada üheteistkümmendamal aastal kahekümne seitsmendamal Septembri kuu päewal /1911 aastal 27 Septembri kuupäewal/ Wõnnu walla talupoeg Mihkel Jaani p. Ostmann 73 a. wana, kes kohtule isikulikult tuntaw on, palus selle kohtu lepingute raamatusse oma suusõnalist järgmist testamenti, ehk wiimast seadmist üles wõtta.
Oma wanema poja Gustaw Ostmannile jättan ma pärandada peale enese surma Wõnnu mõisa järele olewa Sadulsepa koha peal elu maja, nõuda et ka noorem poeg Juhannes Ostmann wõib seal sees elada kunni surmani; kui aga nad mitte üheskoos elamisega seal ei peaks läbi saama, siis peab wanem poeg Gustaw noorema poja Juhannes'ele sellest majast poole wäärtust selges rahas wälja maksma takseerimise järele, kui aga wanem poeg mitte ei peaks tahtma nimetud maja omale wõtta ja selle koha peal elada, siis wõib noorem poeg seda maja omale pärida, peab aga niisammuti siis wanema pojale maja wäärtuse järele poole hinda selges rahas wälja maksma.
Minu tüttar Liisa on juba mehel, tema on omal ajal mehele minnes kõik oma osa warandust selges rahas kätte saanud, sellepärast ei ole temal peale minu surma miskit parimist ega nõudmist ülemal nimetud elu majast.
Seda wiimast seadmist olen ma selge meele mõistuse ja oma enese tahtmise järele teinud ja on see pärast lepingute raamatusse kirjutamist mulle ette luetud.
Mihkel Ostmann umbkirilik tema suusõnalise palwe peale tema eest kirjutas alla: G. Krabi &lt;allkirI&gt;
Tunnistajad: Jürri Kokwal &lt;allkiri&gt; A Reets &lt;allkiri&gt;
Presidendi asemik T Nuut &lt;allkiri&gt;
Liikmed: K. Möll &lt;allkiri&gt; W. Luntsi &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Olen selle leppingust ära kirja wälja saanud 27 Septembril 1911 a.
XXX
