Ette astus Abraham Laurits ja kaebas: Minu naene ei leppi minuga enam koku ja kaebab ja riidleb ühtepuhku ja toggib mind. Et mul waene terwis ja tema wastu ei suuda hakkata, seelabi saawad ka pererahwas, kus juures me kortris olleme tüütud, ni et meie paigal ei saa olla.
Ette astus Abrahami naene Natalie ja ütles wälja: Minu mees on ka minu wastu sant, ja annab oma Kraamisi wõerastele, et mul pärast tema surma ühtegi ei olle.
Nad said suu suud wastu ette kutsutud ja Natalie ei wõinud mingit moodi tõeks teha, et ta mees Kraamisi ärra olleks pilland.
Kohus noomis teda tublisti ja käskis oma mehega jälle koos ellada.
Kohtuwanem: Jaan Jünter XXX
Kõrwamees I Johan Krabbi XXX
Kõrwamees II Maddis Topp XXX
Kirjut: M Kentmann &lt;allkiri&gt;
Toimetud
