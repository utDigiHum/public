Wata 28ma August 1874 a. Mihkel Ran kaebtus, kus seatud kohto päwa kord ei olnud.
Ette astus Mihkel Fried ja tunnistas: Minna ei olle kolme aasta kaupa teinud. Ja et pois ärra tulli, olli se wiga, et ta haigeks jäänud, ja nüüd ei taha pois ennam taggasi minna, ja meie olleme kinksepaga selle asja poolest raho teinud, kedda ka pealt olli kuulmas kirriko herra kubjas Ado Kop.
Ado Kop, tunnistas kirja läbbi nenda: Minna kuulsin, et Mihk. Fried tehtud woodi Ran'ile ja Mihk. Ran tehtud sapad poisile piddid wastamessi jääma, ja muud ühtegi.
Kohos andis neile aega nende tahtmisse järrel tullewa kohto päwaks selgemat tunnistust ette tua.
