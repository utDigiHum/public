Üks tuhat üheksasada neljandamal aastal Oktobri kuu 25 päewal ilmus Oro Walla Kohtusse Oro Walla liige Jaan Mihkli poeg Kleemann ja palus kahe tunnistaja juures olekul, oma wiimast seadmist kohtu aktide raamatusse üles wõtta.
Mina Jaan Mihkli poeg Kleemann luban selle waranduse, mis minu naine Anna abielusse astumisel kaasa tõi, temale ilma mingisuguse takistuseta, pärast minu surma pärida, aga nimelt üks lehm wäärt 18 rupla üks lammas 4 rupla, üks tsetwert rukkid 10 rupla, wiissada rubla selget raha ja wäike maja mis Salajõe Pömma talu koha maa peal seisab, wäärt 40 rupla.
Kõige selle waranduse kohta ei ole kellegi teisel peale minu naise pärimise õigust. Et mina kirjutada ei moista, seepärast palun Jaan Kuuskmanni enese eest alla kirjutada.
Jaan Kleemanni eest Jaan Klemann Jaan Kuuskmann &lt;allkiri&gt;
Meie, kes siia alla kirjutame tunnistame oma all kirjaga, et see sama Jaan Klemann kes om wiimase seadmise seia üles laskis kirjutada, praegus täie mõistuse juures on olemas.
K. Kell &lt;allkiri&gt;
J. Kuuskmann &lt;allkiri&gt;
See wiimane seadmine on pärast aktide raamatusse sisse kandmist Jaan Klemannile ette loetud ja tema palwe peale Jaan Kuuskmannist alla kirjutatud.
President: J. Spuul &lt;allkiri&gt;
Kohtu liikmed: H. Silwer &lt;allkiri&gt; P Kuur &lt;allkiri&gt; M Eringas &lt;allkiri&gt;
Kirjutaja Joh. Tilling &lt;allkiri&gt;
