Sell üks tuhat üheksa sada üheksamal aastal kümnemal Märtsi kuu päewal /1909 a. 10 Märtsi k.p./ Wõnnu walla talupoeg Ado Arwo p. Pruul 59 a. wana, kes kohtule isikulikult tuntaw on, palus kohtu lepingute raamatusse oma suusõnalist järgmist tõstamenti ehk wiimast seadmist üles wõtta.
Mina Ado Pruul Wõnnu mõisa järele olewa Kokkawalja No 85 talu koha rentnik, annan oma talu koha ostu õiguse oma wäimehe Asuküla walla liikme Willem Peetri p. Nuut'le, selleks et ma Wõnnu mõisa ega Willem Nuut'ilt miskit tasu koha ära andmise eest ei nõua.
Peale seda annan kõik oma liikuwa waranduse mis mull selle koha peal on oma wäimehe Willem Nuutile päris omanduseks selle tingimisega, et ta peab mulle ja mu abikaasale tarwiliku kattet, peawarju ja ülespidamist kunni surmani andma ja heasüdamelikult minuga ja minu abikaasaga ümber käima ja wiimaks meid maha matma.
See sinane testament on minu enese selge meele mõistuse ja oma enese tahtmise järele kirjutatud ja peale seda ette luetud.
Tostamendi tegija: Ado Pruul &lt;allkiri&gt;
Tunnistajad: Anton Jurr &lt;allkiri&gt; August Reets &lt;allkiri&gt;
President: J. Koplik &lt;allkiri&gt;
Liikmed: T. Nuut &lt;allkiri&gt; M. Jaksman &lt;allkiri&gt; K. Selg &lt;allkiri&gt;
Kirjutaja G. Krabi &lt;allkiri&gt;
Olen selle lepingust kohtu poolt ustawaks tunnistatud ära kirja wälja saanud 10 Märtsil 1909 a.
Ado Pruul &lt;allkiri&gt;
