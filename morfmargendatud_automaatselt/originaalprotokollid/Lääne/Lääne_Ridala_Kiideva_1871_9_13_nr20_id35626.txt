Tullid ette kaewaja Anna Krihwa ja Juhhan Birk, tunnistusse mees retsep Mihkel Kruus, ütleb sedda walle ollewad, mis Anna Krihwa kaewand, et temma olla seal sure heälega wanduma akkand ja pealegi perrenaise ölla nukki otsa kolm korda russikaga kopputanud, et kuub peab nüüd ka tehtama. Perremees Juhhan Birk ütleb: et nende kaup pölle olnud temmaga mitte täie kue peal, waid pool kube, ehk 1.50 kop. rahha, sedda rahha on mitto korda pakkutud; agga temma polle mitte wasto wõtnud, waid ikka kube tahtnud.
Anna Krihwa tunnistab kül sedda kaupa tõssi ollewad, agga pallub, et kohhus temmale pool kube, ehk 150 kop weel jure mõistaks.
Agga kohto poolt ei sanud ühtigid mõistetud waid nenda kuida endine kaup olnud, sai sel päwal 1.50 kop temmal kätte antud ja assi olli selge.
Kohto wannem: Mihkel Sarijs XXX
Abbi mehed Willem Otsberg XXX Jürri Triksberg XXX
Kirjotaja A J Martensohn &lt;allkiri&gt;
