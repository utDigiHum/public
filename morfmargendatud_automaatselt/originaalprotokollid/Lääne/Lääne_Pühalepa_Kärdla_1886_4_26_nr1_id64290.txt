1)Andrus Markus,Körgesaare walla mees tunnistab siin Kärdla wabriku kogukonna kohtus,et temal nöu on siin Kärdel,wabriku poolest Suuremöisast renditud maa peale elumaja ehitada,mis pärast tema surma Karl Tarningi poeg Willem Tarning enesele pärib.(Wabriku walitsuse poolt on temal luba seia maja ehitada,selle tingimisega,et pärast tema surma ehk kui ta teise kohta elama läheb,Willem Tarning ehk üks üks wabriku töömees selle maja pärib.)Andrus Markus wötab ka Karl Tarningi poja Willem Tarningi,kes praegu 8 aastat wana on,sünd.2 April 1878 aastal,omaks kasupojaks,sest et temal enesel poega ei ole.Karl Tarning lubab ka oma poega Andrus Markus kasupojaks anda.
Tallitaja:H.Pisa/allkiri/
abimees:W.Mölder/allkiri/
abimees:W.Poola/allkiri/
Kirjutaja:G.Bransfeld/allkiri/
A.Markusele Copie antud 30 April 1886 a.N 111 all.
