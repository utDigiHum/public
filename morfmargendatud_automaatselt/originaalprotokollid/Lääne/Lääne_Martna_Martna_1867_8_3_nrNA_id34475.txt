Sure Lähtro perris Herra von Ramm sowimise järrele said selle walla koggokpnna perremeestele ued rendi kontraktid etteloetud, ja selletud et Rent ühtekokko päwadega, mis abbi päwaks nimmetud 15 Rbl. allam oli kui enne. Perremehhed ei olnud keik mitte sedda ette luggemist kuulma tulnud et käsk wägga hilja antud. Selle pärrast sai se kirri walla koolmeistri kätteantud et igga ühhele saab ette loetud ja teada antud.
Pea kohtowannem Karl Nappus      XXX
Kohto korwamees Jaan Elmann      XXX
Kohto korwamees Prido Laur         XXX   
Protokolli kirjotaja JMedell
