Üks tuhat kaheksasada üheksakümmend üheksamal aastal Jüri kuu kaheksammal päewal tuliwad Kõrgesaare Kogukonna kohtu awaliku koosolekule Mihkel Jago Toomma p. ja Mihkel Aljas Andruse p. ja palusiwad nende poolt järgmist lepingud üles wõtta.
"MIna Mihkel Jago müisin oma päralt olewa laewa: Sekstandi osa (üks seitsmas osa laewast) kaks sada kaheksa kümmend rubla eest, mis suguse raha olen kätte saanud, Mihkel Aljase Andruse pojale; nii et miski omandust selle nimetud laewast ommale ei tunnista ja pärimist ei ole.
Mihkel Jago [allkiri]
Sellega tunnistan, et olen Mihkel Jago käest tema seitsmendama osa laewa "Sekstant" ära ostnud, nii et MIhkel Jagol enamm nimmetud laewaga tegemist ei ole nõutawa raha kakssada kaheksakümmendrubla olen temale täieste wälja maksnud.
M. Aljas [allkiri]
Sellekauba lepingus nimetud inimesed Kõrgesaare walla liikmed Mihkel Tooma p. Jago ja Mihkel Andruse p. Aljas, on Kõrgesaare Kogukonna kohtule palelikult tuntud ja õiguse omanduslikud aktide tegemise juures, mis Kõrgesaare Kogukonna kohus oma allkirjaga tunnistab.
