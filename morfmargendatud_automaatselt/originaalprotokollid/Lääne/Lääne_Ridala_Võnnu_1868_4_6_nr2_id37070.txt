Tulli ette Marri Hansi ja kaebab et temma omma endise wanna peigmehhele on pandiks annud asjo, mis kohto arwamisse järrel 7 rbl. wälja teeb ja 2 rbl. rahha.
Siis sai ette kutsutud se peigmees Jürri Sone, ja küssitud, kas se nenda tössi on. Selle peäle ei te Jürri mitte kaebtust walleks, ja et se abbiellusse heitminne walleks jäi on meil möllemilt poolt tulnud.
Sellepeäle on kohus otsust teinud, et Jürri Sone peab selle seitsme rublast pool se on 3 rbl 50 kop. Marrile ärramaksma ja ka se 2 rbl laenatud rahha essiteks peab temma need 2 rbl. 1sel Mail 1868 ärramaksma ja 3 rbl 50 kopp. 29mal Mihkli kup. 1868 ärramaksma kohto ette.
Kohtowannem: Jaan Kiskmann &lt;allkiri&gt;
Abbimehed: Mihkel Lahhe Ado Ües
