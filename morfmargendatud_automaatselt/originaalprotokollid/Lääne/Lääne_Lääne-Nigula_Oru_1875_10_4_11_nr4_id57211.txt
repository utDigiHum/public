Ette sai wõetud se assi, mid minnewa kord Gustaw Steenmann kaewas No 4 al, Jaan Ollmanni ülle.
Jaan Ollmann tunnistab, et on kül need jännesed linna winud, agga olle omma wenna jure läinud ja andnud jännesed Jürri Paalmanni kätte. Pärrast saand kuulda, et Jürri Paalmnann linnas kinni pandud, agga enne olle ühhe jännese ärra münud 48 kop. ette.
Jürri Paalmann tunnistab, et need jännesed olle temma holeks jänud ja ühhe münud ta ka ärra 45 kop. ette ja tedda pandud kinni politseisse ja ta kraam ja 2 jännest pandud keldrisse. Agga kui tedda lahti lastud, ööldud et kassid olle jännesed ärra sönud. Sedda möda möistab kohhus:
Jürri Paalmann maksab kahhe jännese rahha arwata 90 kop. Gustaw Steenmannil ja üks jääb Gustaw Steenamnn kahjuks.
