Astus ette Adam Peetsmann ja maksis kihhelkonna kohtus moistetud 20 Rbl. höbb. wälja ja kaebas sega ühtlasse, Ugla wallal olla tale wiimse aasta kolitamisse ja kirjutamisse ette 1 tsetwert 4 tsetweriki wilja palka maksta ja noudis sedda kätte sada.
Jaan Iling ja Michkel Warripu tunnistassid, et Adam Peetsmannil ta wiimse tenistusse aasta kohta, se nimmetud palk samata on.
Kohhus moistis, et Wald peab sel kewwadel Maggasi aidast 1 tsetwert 3 tsetweriki wilja palka Adam Beetsmnannile wälja maksma, ja need maksjad liikmed, kes 1870-75 aastani wallamaksude wilja karnitsat maksmata on jänud, peawad enneste wöllad Maggasisse sisse maksma.
Kohtowannem Michle Kelnik
Körwamees Jürri Silwer
Körwamees Hans Hinnobert
Kirjutaja Abr Grein &lt;allkiri&gt;
