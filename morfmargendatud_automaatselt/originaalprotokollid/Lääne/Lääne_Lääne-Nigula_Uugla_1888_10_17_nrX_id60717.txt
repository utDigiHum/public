A. lisa 27 Septbr 1888, punkt 4, pag. 120.
Astusid ette Kiidikast Aleksander Juhkendal, Willem Spuul, ja Jaan Dragon ja rääkisid wälja: Nemad ei olla 10. Septembri öhtul s.a. mitte üks ainus Jüri Luubert'i lauda peal olnud, ega seda tüli, mida Hans Liiwerti ja Kustaw Wuulmann'i wahel olnud, mitte näinud.
Astusid ette Willem Nömm, Aadu Luubert, ja Aadu Loorens ja ütlesid 10. Septembri k.p. öhtul s.a. Jüri Luuberti lautade juures käinud olewad, aga mitte lauda peal käinud ega seda tüli näinud.
Astus ette Jüri Iling ja rääkis wälja: Tema käinud 10. Septembri k.p. öhtul s.a. Jüri Luubert'i lauda peal ka.
Astus ette Orrult Willem Peetsmann ja rääkis wälja: Tema läinud 10. Septembri k.p. öhtul s.a. üht kirja Jüri Luubert'i tüttartele wiima, mis nende wend Aadu Luubert temaga Tallinnast saatnud. Et need juba lauda peal maganud, siis pidanud ta seda kirja senna nende kätte wiima.
Otsus: Et naad keik ühed öö hulkujad olnud, seepärast
Kogukonna kohus moistis et igaüks: Willem Nömm, Jüri Iling, Aadu Luubert, Aadu Loorens ja Willem Peetsmann maksab 1 rubla 50 kop. raha trahwi walla laeka heaks kahe nädala aja sees wälja. ja
Kiidikast Aleksander Juhkendal, Willem Spuul ja Jaan Dragon said ilma trahwi kandmata, puudulise tunnistuse pärast lahti lastud.
