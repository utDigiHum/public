Ette astus Ann Kadanik ja kaebas:"Minu issal, Kes praegu haige ja mitte Kohtu ette ei woi tulla, on Jaan Kahmi Käest 7 Rubl. ja mo omal 2 1/2 Rbl. raha pärida. Jaan Kahm ei taha mitte mul seda raha wälja maksa, sellepärast, et ta mulle üks talwe süüa annud, et agga mina, Kui tema naese õde, nende tellimise peal seal olin, ja Keik tegin, mis majas tarwis oli, Jaan isse olli linnas tööl, naene olli haiglane, ma aitasin teda Kuni Jürripäwani, Kus isse paremaks sai.
Ette astus Jaan Kahm ja ütles wälja:"Minul on Ann Kadaniku käest pärrida:Jula Kadaniku passi eest			SR. 2		1 Keila reisu eest			SR. 2. 50 Kop.		6 nädala sööma raha			SR. 2. 50 Kop.		Liso Kadanaku Kirstu laude eest			SR. 1		Summa			SR. 8		
Peale selle jääb Ann Kadanik mul weel wõlgu talwise sööma raha, Kui se 1 1/2 Rbl. mulle jätab, siis enam ei pärri.
Nemad astusid suu suud wastu ette ja ei wõinud ühtegi üheteise pärimiste ülle tõetunnistust tuua.
Kohus mõistis, et Jaan Kahm Ann Kadanikule 4 R. 50 K. weel peab maksma, ja peale seda saab Ann Kadanik Jula Passi ja reisu raha ette 4 Rbl. 50 Kop. puusärgi laudade ette 1 Rbl. 50 Kop.
Sellepärrast, et nendel tunnistusi ette tuua ei olnud, mis Kaubaga Ann Jaani juurest Kortsis olnud, ja on arwata, et iga hinimene oma tööga sööma äratasub. Sellega olid mõlemad rahul, ja lepisid ära.
Kohtuwanem: Peter Thomson
Kõrwamees I Jürri Erik XXX
Kõrwamees II Jaan Kopel XXX
Kirjut. M Kentmann &lt;allkiri&gt;
Täidetud.
