A. Lisa 4. Januar 1884 punkt 1. pag. 62.
Astus ette Kirrimäelt Peter Eilmann ja rääkis wälja: Kustaw Kasim tellinud sügise enesele 1 Tsrt. odre osta ja tema ostnud Roosnast Jaan Schönberk käest 10 rubla eest se Tsrt. odre ja münud seda Kustaw Kasimile.
Astus ette Roosnast Jaan Schönberk ja tunnistas wälja Peter Eilmanil 1 Tsrt. odre 10 rubla eest münud olewad, ja näitas ka ühtlase seda samma seltsi odre, mis ta Peter Eilmannil münud oli proowiks ette, kellest näha oli, et need prowi odrad, Kustaw Kasimile müdud odradega üsna ühed olid.
Otsus: Et Jaan Schönberk tunnistas Peter Eilmannil 1 Tsrt. odre münud olewad ja see jälle need sammad Kustaw Kasimile münud oli ja et Jaan Schönberk'il weel neid sammu odre prowiks järel oli, mis ta enne münud oli, seepärast
Koguk. kohus möistis, et Hans Alepp peab need odrad, mis ta weskist ära toonud, Kustaw Kasimile kätte andma.
