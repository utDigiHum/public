Kaebas Mihkel Kõlo, et Mihkel Pöial ja Juhan Aru on teda wargaks teinud, et ta olle Partsi wõrk maeast 2ed kingad ja 1ed saapad warastanud.
Mihkel Pöial ütles, et tal saapad wõrkmaeast warastud, pöiad leidnud, kel sääred äraleigatud, mis ette näitas. Minewa aasta on M. Kõlo poea käest Juhan Aru oma tiku toosi kättesaanud, mis kuue taskust warastud, seepärast arwanud Kõlo peale ja seal otsimas käinud.
Juhan Aru ütles, et tal kingad warastud - ja seepärast M. Kõlo peale arwand ja seal otsimas ka käinud, et minewa aasta oma tiku toosi Harju Jõe Andrus Laupa käest kättesaanud, kes ütlend Madis Kõlu käest saanud olewad.
Mihkel Kõlu ütles, et ta pois on selle toosi leidnud ja mitte warastanud. Juh. Aru, kuis wõis ta seda leida, mis ta kuue taskus olnd ja kuub ka wõrk maeas paigal seisnud, aga tiku toos sealt ärakadund.
Tunnistus Juhan Aru /1 teine/ ütles, et kui nad Kõlu juures otsimas käinud, et midagi paha põle seal tehtud.
Toogu selge tunnistus, siis saab klaaritud, muidu jääb niisamma.
