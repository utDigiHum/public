Kohtu korra pärast olid Piisalu kohtu hoones kokku tulnud:
Kohtuwanem: Jaan Kaaring
Kohtukorwamehed: Kustas Sewermann
Jaan Liiw
Mari Meering ja Rein Kuulepp, kelle kaebtuse asi No 14 all sai sisse kaebatud leppisid kohtu ees ära.
Muud kohtu käijad ei olnud ega seletus ei olnud.
Kohtuwanem: Jaan Karing (allkiri)
Kohtukõrwamehed Kustas Sewermann XXX
Jaan Liiw XXX
Kirjutaja: P. Hirsch
