Kohto ette astus Nibi metsawahhi karjane Marri ja kaebas Sallaja walla metsawahhi Ado Matwei naese Mina peäle, kes tedda 2 hopi witsaga on lönud.
Mina Matwei ütles: Ta põlleks muido lönud agga et Karja laps polle lasknud lömi heinamast wälja aiada sepärrast on Mina tedda 2 hopi lönud.
Nüüd moistis kohhus: et Mina Matwei peab selle eest, et löminne keeldud on selle lapsele, kedda ta lönud, pool rubla hobi peält maksma se on kokko 1 Rubla höbbedat.
Et Mina Matweil ei olnud kohhe rahha maksta, siis moistis kohhus, et 19mal Oktobril peab se üks rubla kohto ees wälja makstud sama.
Kohtopeawannem Andrus Kroosjäger XXX
Körwamees Andrus Kerm XXX Kustaw Sündewa XXX
Kirjotaja Abr Krein &lt;allkiri&gt;
