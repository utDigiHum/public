5. Moisa herra Knauff kaebas Niiwi wallast Jüri Kärmann ja Andrus Jenmann üle, kes ilma lubata tema krundi peäl kalu püüdmas olnud, mis jo ammu Niiwi wallal teada saanud antud, et neil luba Sallajõe meres kalla püüda ei ole.
Astusid ette Niiwist Jüri Kärmann ja Andrus Jeenmann ja tunnistasid Sallajöe meres kalu olewad püüdnud.
Otsus: Et herrä Knauff üles andmist mööda Niiwi wallal teada oli, et Sallajöe meres ilma luba küsimata kala püüda ei woi, mid aga Jüri Kärmann ja Andrus Jenmann ilma aegu wabandamiseks wötsid, et see neil teadmata olnud, sepärast
Koguk. kohus mõistis, et molemad kui käsu üle astujad 50 kop trahwi raha Sallajöe walla laeka heaks peawad maksma.
