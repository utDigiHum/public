Kaebas kõrtsmik Karl Baumann, et Juhan Umberg ja Karl Umberg olla püha pääwal kui ta kirikus olnud, ja temal wäike tüttarlaps koduliseks olnud, kõrtsi tulnud ja jutluse ajal seal pilli mänginud, mis ta ilmasgi pole lubanud ja wägise tema elu tuppa tunginud ja pahaste rääkinud.
Järgemööda saiwad ette kutsutud Juhan Umberg ja Karl Umberg, nemad ei wõinud oma süüd mitte wabandada. Siis mõistis kohus, et Juhan Umberg maksab 1 rbl. ja Karl Umberg 1 rbl. trahwi.
Kohtuwanem: J NeumannKõrwamees: J Tomann" : J LaursonTalitaja: J WesiradtKirjutaja: M Kruup [allkiri]
