Ette astus Kustas Söönberg ja tunnistas: Mina nägin kui Sassa Grünbergi aita Kingiseppa Koha pealt ärawedasin, et elumaja uksed ja aknad ja ka lae ärrakistud olli, nenda et seal sees elada enam ei woinud. Meid olli seal 6 Meest, kes sedda nagid.
Ette astus Joesust Liiso Niin ja tunnistas: Mina kuulsin sel päwal kui maija ärapõlles, et Leena Krünberg ütles; ma nägin küll kui maia polema hakas, et Anna Nöör, kes maja perenaene sest suurt tähele ei pannud, ja arwan, et Anna Nöör seda isse põlema pand, sellepärrast, et Sassa Krünbergiga ikka riiu mehed ollid.
Ette astus Leena Krünberg, ja ütles wälja: Ma nagin kui maia kattusel juba tulli olli, ja ei mäleta ühtegi, mis ma ehmatusega ollen rakinud. Anna Nööri tütar tulli karjutes põllal ja ütles:"Wana maja polep!" Aggi nemad ki hüüdnud waid mina, kui oleks wet kääpärast olnd, siis olleks ka wõind kustutada.
Ette astus Anna Nöör oma tütrega ja see ütles wälja:"Ema ajas mind kodu kartohwlid koorima, ma läksin nuttes ja ei näinud siis ühtegi, nattukene aega pärast seda läksin wet tooma ja nägin, et wana majas suits tõusis ja läksin põllale karjudes.
Kohus mõistis seda asja suurema Kohtu oleks
Sellepärast, et siin paremat tunnistust ei sanud.
Kohtuwanem: Jaan Jünter XXX
Kõrwam I Johan Krabbi XXX
Kõrwam II Madis Topp XXX
Kirjut: M Kentmann &lt;allkiri&gt;
Surema kohtu ette läinud.
