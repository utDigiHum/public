Peter Lorrens kaebas Jaan Adop. Danbuschi, Jürri Mihklep. Õunpu, Jürri Jürrip. Klemann ja Michkel (tum) Jaanip. Kruus olla Jöulu wiimse pühha õhto temma toas käinud ja ühhed sapad ärrawarrastanud.
Jaan Danbusch, Jürri Õunpu ja tum Michkel Kruus astusid ette ja ütlessid, et naad on Peter Lorensi toas käinud ja Jaan Danbusch ka neid sapaid watanud, agga naad ei olla neid mitte warrastanud; egga ärrawinud.
Otsus: Et selget tunnistust ei olnud, et need poisid just neid sapaid warrastanud on, sepärrast kohhus ei woinud neid mitte süalluseks moista, waid se assi jäi selgema järrel kulamisse läbbi teise kohto päwaks.
Kohtowannem Michle Kelnik
Körwamehed Jürri Silwer Hans Hinnobert
Kirjutaja Abr Grein &lt;allkiri&gt;
