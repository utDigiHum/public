Hakenrichteri herra kässo peäle astus ette tallitaja Jaan Kork ja kaebas Jürri Korki ülle, ta poia Andrus Korki pärrast, kes sel aastal 1872 Orro walla eest nekrutiks läks ja nüüd ennese issa käest 6 tsetweriki odre ehk sedda wäärt rahha hinda pärrib, selle eest, et ta on minnewal süggisel 1871, 6 tsetweriki odre wanna wõlla tassumisseks Maggasisse maksnud, mis ta issa Jürri Kork möda läinud aastade sees Maggasisse wölga jätnud.
Jürri Kork astus ette ja ütles, et ta on Andrus Korkile nende 6 tsetweriki odrade eest ühhe wanna hobbose annud, ja peäle sedda nüüd Andrus Korki eest, made renti, mis moisa herraga wõlga jänud 25 päwa ja 1 rubla 67 1/2 kopp. höbb. rahha peab Jürripäwaks ärra maksma.
Kohto körwamees Hans Kork tunnistas sedda tõe ollewad, mis Jürri Kork, Andrus Korkile nende 6 tsetweriki odrade eest on annud ja ka se made rent, mis Jürri Kork ka weel Jürripäwaks peab moisa herrale maksma.
Kohhus moistis Andrus Korki tahtmist sellepärrast tühjaks, et ta on issa käest nende 6 tsetweriki odrade hinda jubba kätte sanud.
Kohtowannem Ado Oltman XXX
Körwasmees Hans Willik XXX
Körwasmees Hans Kork XXX
Kohhus ei moistnud Andrus Korkile ühtegi ennam.
