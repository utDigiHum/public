Parila Talitaja Madis Topp kaebas: Tõnis Reints ei maksa kooliõpetaja palka, "1 rbl. raha ja 1 süld hagu" ära, et temal kooliõpetaja käest 1 rubla pärida olla. Ka Jüri Jürjer'i nõudmise peale "miks Karjasmaad tükati sisse piiratakse", olen mina seie käsutanud Jaan Jürjer'i ja Jüri Kukers'i
I Kostja Tõnis Reints ilmus kohtu ette ja näitas kooliõpetaja kirja ,et 1 süld hagu on ära makstud; 1 rubla maksan kodus talitaja kätte, aga nõuan kooli õpetaja J. Meinwaldi käest 3me puuda heinade raha 1 rubla kätte. Karjasmaad wõtsin aja sisse sell põhjusel, et minu heinamaad üks niisama suur tükk walli taga karjasmaaks on.
II Kostja Jaan Jürjer ilmus ja kostis: Mina wõtsin ühe külimitu maa liiwahauku aja sisse, et tahtsin linaleotamise urka sinna teha ja minul on karjasmaad 1 tessatiin teistest rohkem ja parem ka.
III Kostja Jüri Kukers ilmus ja ütles: Mina tõstsin põllu aeda tänawa poole, et hang talwel iga kord orasele waewa ei teeks ja arwasin selle põllu söödi kirjas kaardi peal olewat, aga ei ole, siis parandan kitsaks saanud tee koha aja nuki juures sündsaks, siis on jälle kõik korras.
Otsus: Rahukohtu mõistjale nuhtl. Seaduse punkt 29 põhjusel: Edespidi, kes tahab karjamaad sisse piirata, peab kõik oma karjamaa sisse piirama, ehk walla meestega enne oma nõuu ja põhjused läbi rääkima, muidu on tema töö tühjaks ja wastu seadust. Tõnis Reints peab, kui teised oma wallid ristikiwide peale peaksiwad wiima, siis kaks külle aeda tegema. "Teen"! XXX Tõnis Reints. Jaan Jürjeri ja Jüri Kukers'i põhjused on rahuloldawad ja õiged.
Eesistnik: Rein Reints &lt;allkiri&gt;
Kohtumõistja I Juhhan Prul &lt;allkiri&gt;
ja  II XXX
Kirjutaja: &lt;allkiri&gt;
