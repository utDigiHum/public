Suremõisa wallitsus kaebas sisse et Sakla ja Alla talloperremeeste lomad on Loja mõisa heinama ja põlloma peal olnud. Juhan Arru 4 hobbust Juhan Liit 2 hobbust Willem Pallade 1 hobbune 2 weist Kustaw Mast 1 hobbune Peet Pähhel 3 hobbust 9 weist Kustaw Sprit 2 weist Juhan Singi 2 hobbust 10 weist, Simo Pael 3 weist Peet Alberil 10 weist Andrus Juhhel 9 weist Tonis Kokla 1 hobbune Peet Marro 1 hobbune Tõnis Pihlamets 6 weist ja Andrus Särglep 1 hobbune üllepea 16 hobbust ja 51 weist.
Tunistus. Astus ette Peet Eddasi Loja mõisa wahhimees ja andis teada.
1) 13 Jani k.p. ajas karjamees Mihkel Kusik 15 külla hobbust Loja moisa heinama ja põllo pealt, Loja mõisa sisse, need hobbused ollid Sakla ja Alla küllast, ja wiisid sel samal päwal Johan Arro 4 hob Peet Pähhel 2 hob. Johan Liit 1 hob. ja Tonis Kokla üks hob. sure ähwardamissega ärra, kelle ulkas P. Kokla keikse rohkem wandus ja sõimas, ja karjamees on isse nende kätte annud. Kustaw Mast ja Willem Palade 2 hobbust wiidi sallaja mõisa tarra peal wälja.
2) 21 Jani k p ajasin mina 8 lehma Loja mõisa karjama pealt Loja mõisa, nendest wiidi 4 lehma Peet Pähhel ja Joh. Singi ommad sel sama päwal ärra sandi sõnnu jäggades, need teist 4 lehma Willem Pallade Pet Alber ja Simo Pael omad wiidi öösel karja aja seest ärra
3) 28 Jani k.p. ajasin mina jälle 43 weist Loja mõisa karja ma pealt Loja mõisa, 26 weist P. Pähhel Joh. Singi, Peet Alber, Kustaw Priit wisid selle sama päwal ommad ärra omma wõimussega 17 weist mis agga lautas lukko tahha jäid wiis Tõnis Pihlamets naene öesel ärra, kui karjamees Mihkel Kusik sedda näggi ja järrel jooksis, ähwardas temma mahha lüa ja läks suure kissa ja kärraga eddasi
4) 3 Juli k p. ajas karjamees Mihkel Kusik 1 hobbune mõisa heinama pealt Loja mõisa, ja on se nisamoti sallaja Andrus Särgleppa naese poolest ösel ärra wiidud.
1) Astus Juhan Arru kohto kutsumisse peale ette ja ütles et tal on 4 hobbust seal olnud wäggise ma ei olle winud ja trahwi põlle nõutud.
2) Juhhan Liit ütles et tal on 2 hobbust sees olnud ta poeg Peter on neid ärratanud, olleks ta wanna innimene olnud egga ta polleks sedda teinud.
3) Willem Pallade ültes et 1 hob. ja 1 härg tõi pois päwa ärra ähwardan ta ei olle.
4 Kustaw Mast et ta isse polle tonud waid ta naene 1 hobbune on tal kini olnud.
5) Peet Pähhel ütles ma ei olle isse tonud agga mo poeg jo se aed on ni sand et lomad sealt läbbi lähwad egga lomadel mõistust ei olle miks Partsi rentnik ei lasse sedda aeda parandada.
6) Juhan Singi ütles et wahhimees on ta lomad ta tütre kätte annud ja mõisa aed on wägga sand lomad peaswad wahhelt läbbi.
7) Tõnis Kokla ütles et 1 hobbune on tal sees olnud ja keddagi ta ei olle laimanud.
8. Andrus Juhhe ütles et tal 7 loma on agga mitte 9. wälja ta ei olle aianud egga ta perrest, õhto tullid lomad isse koddo ja isse ollin ma Talinas kohtus.
9) Peet Alber ütles et ta isse ei olle käinud, egga ta perrest polle kiisgi järrel käinud, isse ma ollin Talinas kohtus.
10) Simo Pael ütles ta isse ei olle käinud, sest ma ollin Talinas kohtus ja kes neid lahti laskis sedda ma ei tea.
11. Tõnis Pihlamets ütles et ta isse ei olle käinud lomi wäljaiamas agga mo naene wõttis ukse akest lahti ja need lomad mis seal sees ollid tullid wälja, ja mo laps seisis ilma pimata ja karjus, sest ta ei imme emma rinda.
12. Pet Marro ütles et tema hobbune ei olle kinni olnud kiisgi ei olle tal teada anund kui ta kül kinni on olnud, kes tedda lahti laskis sedda ma ei tea ja karjama pealt tõin ma tedda koddo.
