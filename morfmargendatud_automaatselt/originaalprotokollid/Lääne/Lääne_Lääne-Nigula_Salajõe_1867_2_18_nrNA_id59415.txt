Andrus Suurwäl tulli Kohtuse et temma olli Jaen Millerid, pool Aastad tenind Mihkle päwast Jürri päwani ja et nendel pole Aasta kaup olli, ja tahtis Jaen Milleri keast omma pole Aasta palk kätte sada.
Jaen Miller ei tahtnud temmale ühtegi maksta sellepärast et temma ei olle Aastad teninnud, sellepärast et nendel pole Aasta kaup olli, moistsid Kohtomehhed et Andrus Suurwäl piddi Jaen Milleri keäst omma pole Aasta palk käte sama, üks werik rukkit ja üks werik odre.
Kohtomees Andrus Kroosjäger XXX
Kõrwamehhed Andrus Kärm XXX Johann Kreko XXX
Kirjotaja Andrus Lemloh &lt;allkiri&gt;
