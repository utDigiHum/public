Üks tuhat üheksasada kolmanda aasta Aprilli kuu Kahekümne kaheksamal päewal ilmusiwad Oro walla kohtusse, sellesama walla liikmed Ado Kipperman ja Jaan Ado poeg Kippermann ja palusiwad eneste wahel tehtud suusõnalist lepingut walla kohtu lepingute raamatusse sisse kanda.
Mina, Jaan Ado poeg Kipperman andsin oma isale Ado Kippermannile 200 rupla, kui tema Hindreku talu koha ostis. Seda summa ei nõua mina praegus mitte tagasi, aga kui Ado Kipperman sureb, ehk seda kohta kellegi teisele peaks ära müüma, siis nõuan ma nimetud suma temalt ja surma järele ta pärijatelt tagasi.
Mina Ado Kippermann olen selle poja Jaan Kippermanni ettepanekuga täiesti nõuus.
See sama leping on pärast lepingute raamatusse sisse kandmist lepingu osalistele ette loetud, ja Jaan Ado poja Kippermannist oma käega alla kirjutatud
Jaan Kippermann &lt;allkiri&gt;
Ado Kippermann, et ta kirjutada ei mõista, tähendas oma nime juure kolm risti.
Ado Kippermann XXX
President: J. Spuul &lt;allkiri&gt;
Kohtu liikmed: H. Silwer &lt;allkiri&gt; P Kuur &lt;allkiri&gt; M Eringas &lt;allkiri&gt;
Kirjutaja Joh. Tilling &lt;allkiri&gt;
Ärakiri sellest lepingust Ado Kippermannile wälja antud 22. Septembril 1903 aastal.
Wõtsin wastu Ado Kippermann XXX
Et Ado Kippermann kirjutada ei mõista tegi tema oma käega kolm risti
President: J. Spuul &lt;allkiri&gt;
