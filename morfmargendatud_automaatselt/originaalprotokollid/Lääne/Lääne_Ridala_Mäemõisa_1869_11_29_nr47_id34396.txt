Toi Kaebtust kohto ette Tallitaia Juhhan Kellemet, et Kidewa wallast Otsa Hindreko naene Truta Palberg on Prido Lahhe Kramist, mis tallitais on kirja pannud, et piddi sama müdud oksioni wisil; ärra warrastand üks wokk.
Sai kutsutud Kohto ette, Truta Palberg, Kes tunnistas, et temma sedda wokki on wõtnud, ja et temmal sedda olla õigus pärrida, et se temma wenna Prido Lahhe wokk on.
Kohhus moistis, et Trutal polle sedda mitte õigus pärrida, waid peab selle eest wiiskümmend koppik hõbbedat maksma, et sellega maksa, mis Prido Lahhel wallaga wõlga on.
Truta Palberg on maksnud wiiskümmend Koppik tallitaia Kätte sedda mis wok wäärt olli.
Koos ollid Kohtowannem: Jaan Sass
Jaan Spuhl Juhhan Korjus
