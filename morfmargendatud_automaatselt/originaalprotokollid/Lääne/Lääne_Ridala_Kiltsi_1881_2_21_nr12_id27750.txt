Ette astus mis 14mal Webruar No 6 protokol Kaebaja Rein Sep ja Rein Nuut, mõllemad Peetri tallo kohha rentiad, ja ütlewad:"Se heina küin olli laggund ilma muist sarikata. Jaan Rabba andis meile ülles et Hindriko Jaan Lõpp omma poiaga palkisi wiind; rutto käisime waatmas, leidsime et 2 allust korda weel maas; otsisin, leidsin Hindriko Jani ouest; kuine pitkus on 12 jalga, laius 9 jalga kõrgus 5 korda; 3 korda on waeg 4 sarikast kattus ja roikad ollid eni ärra mädand. Kohto mehhed küssiwad, mis arwate wäärt ollewad; ei teand ükski neid wäärt arwata.
Kohto mehed moistwad 2 rubla need 3 korda 4 s. wäärt, peale selle 2 rubla walla laeka trahwi H. kiriko walda.
Kohto mees: Mihkel Poots XXX
I Kohto Kõrwamees Mihkel Juhkam XXX
II Kohto Kõrwamehe assemel Tallitaja J. Koblik XXX
Kirjotaja: Jaan Grünär
Moistetud
moistmine täitetud 21sel Webruar 2 Rubla Kirriko walla laeka.
