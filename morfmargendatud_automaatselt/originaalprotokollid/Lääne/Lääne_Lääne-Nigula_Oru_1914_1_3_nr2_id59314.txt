Ükstuhat üheksasada neljateistkümnemal aastal nääri kuu 3 päewal ilmusiwad Oru walla kohtusse Oru walla liikmed Mart ja Hans Marti pojad Luubertid kes selle kohtule isiklikult tuntud oliwad ja palusiwad oma wahel tehtud suusõnalist leppingud aktide raamatusse sisse kanda.
Meie Mart ja Mari Luuberti seaduslikud pärijad jagame Enniste koha oma wahel järgmiselt ära: Weski aluse põllu saab Mart Luubert omale Lauda taguse põllu saab Hans Luubert omale aga mis weskialune põld suurem on selle eest saab Hans Luubert wainu põllalt selle jao juure, mis Hansust wainu põllalt üle jääb saab Mart omale. Suur umbaed saab pikkiti lõhki üks kõik kumb pool kummagil Altsoo heinamast saab selle poole Hans omale mis Ulmi pool on. Sõer piki lõhki kumbki pool teine teisele see pool Hansule mis Ulmi wastu on ja peale selle annab Mart oma polest selle osa juure Hansule mis weski aluse põllu söödiga temale läks Hüitsost saab Puuri talu poolt poole Hans omale Auasteloo pikki lõhki Ulmi poolt pool Hansule. Koppel mis wana elumaja juures on jääb Martile selle eest saab Hans karja arust niisuure ma tuki omale kui koppel. Weski alune põllu sööt nii kui ülemal nimetadud saab Martile. Karjasma saab pikku lõhki üks kõik kumb pool kumbki wõtab. Mart maksab Hansule elumaja ja karilauta eest 100 rubla, teised aidad saawad Hansule kuna teised Martile jääwad. Wilja nõud ühe wõrra kummagile. Heina küinid pooleks ära jagada. Need noored õunapuud mis lao otsas ja seppapajo juures on saab Hans Luubert omale, aga suur rohu aed jääb terwelt Martile. Et Mart poismees on saab tema pulma kuluks üks lehm ja järele jäänud wiljad, aga kardowlid mis emast järele jäänud saab ära müidud ristide ostmiseks aga misjärele jääb jagawad oma wahel ühe wõrra Seppapaja ja köök saab Hans omale. Weski ja Wihtlemise saun jääwad Martile
Kumb pool seda leppingid murrab see maksab teisele 300 rubla kahjutasu.
See lepiing on enne allakirjutust meile ette loetud ja kirjutame alla.
Mart Luuberg &lt;allkiri&gt;
Hans Luuberg &lt;allkiri&gt;
Tunnistame õigeks
Esimees: W Põllus &lt;allkiri&gt;
liikmed: W Nootmann &lt;allkiri&gt; A Nork &lt;allkiri&gt;
Kirjutaja: J. Saar &lt;allkiri&gt;
ärakiri wälja antud Hans Luubergile
H. Luuberg &lt;allkiri&gt;
гербовой сбор уплачень
