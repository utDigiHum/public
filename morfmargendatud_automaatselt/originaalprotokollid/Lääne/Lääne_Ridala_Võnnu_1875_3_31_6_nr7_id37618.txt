Siis kaebas Mihkel Lien: Minnul on kaks koormad heino metsast ärra warrastud.
I Soo heinamast 1 koorm, kus Hans Rü wahhiks on, ja näddal jubba olli wargusse aiast mödas, kui järrele läksin, ja waht weel ei teadnudki, et warrastud olli, ja selle poolest olli wahil süüd.
II Tihho heinamast 1 koorm, kus waht Hindrek Rein mulle kohe selle päwa öhto teada andis, ja sellepärrast ei möista ma temmale suurt süüd.
Ett tulli waht Hans Rü ja räkis wälja: Teised keik wiisid ommad ärra, agga Mihkel jättis ennesel liig kauaks metsa.
Kohto otsus: Et meie heinama wahid liig wähhe palka sawad, selle pärrast ei wöi nemad keik kahjo kanda agga et ta ennem käsko ei toonud, selle ette peab Hans Rü Mihkel Lienile 2 Rubla kahjotassumisseks maksma.
Möllemad leppisid sellega ärra, et Hans Mihklile piddi selle raha ette piddi sui tööd teggema.
Ja teise wahi kääst ei nõudnud Mihkel Lien middagi kahjo tassumist.
