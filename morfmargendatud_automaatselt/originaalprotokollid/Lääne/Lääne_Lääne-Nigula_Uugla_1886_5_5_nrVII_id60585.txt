körwasmees Jüri Saar oli haiguse pärast tulemata jäänud, selle asemel oli tallitaja Jaan Kruus.
B. Lisa 1. Junilk 1883. punkt 3, pag. 56
Astus ette Adu Wiilmann ja tunnistas wälja: Üks ait, üks kuur ja üks kolme seinaga lamba laut saanud ühes kous ühe aida ette herra poolt Mihkel Markusele antud. Ka see uus laut, mis Mihkel Markus Alt-Hansu hoonedest saanud, olla herra poolt temale antud. Need hooned, mis Hans Alwin'ile jäänud, olla praegu keik alles.
Otsus: Selle Adu Wiilmanni tunnistuse peäle Koguk. kohus moistis Hans Alwin'i pärimise tühjaks, sest et Mihkel Markus Alt-Hansu koha ostmisega, need hooned omale ostnud, ja moisa herra poolt temale kätte antud saanud on.
Hans Alwin ei olnd selle koguk. kohtu moistmisega mitte rahul, waid noudis suuremat kohut.
Kohtuwanem Mihkel Kuur XXX
Körwasmees Jüri Kleemann XXX
Körwasmehe asemik  Tallitaja Jaan Kruus XXX
Kirjutaja A. Krein &lt;allkiri&gt;
