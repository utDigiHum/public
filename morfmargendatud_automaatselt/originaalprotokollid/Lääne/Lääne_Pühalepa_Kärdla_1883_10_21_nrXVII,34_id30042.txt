Pühapääw 16m Octobril s.a. oli Johann Mammuspuu oma naese ja tütrega Kärdla Mäe kõrtsus endid nii täis wiina joonud et kõik tee peale maha jäid kust Kärdla wabriku walla Polizei neid leidis.
Johann Mammuspuu, tema naene Kreet ja tütar Liisa astusid ette, ja sai see asi neil ettepandud, ei wõinud endid wabandada, waid tunnitasid, et see seekord nõnda nende käest on juhtunud.
Otsus: Selle süü pärast et endid nii joobnuks joonud et tee peale maa jäid saawad trahwi, Johann Mammuspuu 24 tundi, ta naene Kreet 12 tundi ja tütar Liisa 12 tundi wangisistumist.
