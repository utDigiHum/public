Kidewamoisa pärris, Herra von Gernet kirja peal sai kohto ette kutsutud Mäewallast Launadosauna Josep Korjus, kes Kidewa moisa wallitseja käest kewwadel 1877 on wasto wotnud käerahhaks tenistusse peale üks rbl. wiis koppik rahha ja üks wak rukkid, agga jäänd tenistusse tullemata.
Josep Korjus tunnistab, et temma ei olle mitte üks koppik egga üks terra rukkid Kidewa wallitseja käest tenistusse peale wasto wõtnud; agga minna ollen kolm aastad järgemöda seal teninud ja sain aastas palgaks pastli rahhaga kokko, kolmkümmend seitse rubla rahha ja ühheksa tündrid wilja. Kewwadel 1877 olli mul weel tenida kunni kaks näddalid enne Janipäwa siis piddi aasta täis ollema; agga mul olli ka weel palka selle aiaks sada kaks wakka rukkid ja üks rubla rahha; Nellipühhi laupääw siis wotsin wallitseja käest wiis küllimatti kalla ja need arwati üks rubla wiis koppik ja need rukkid wõtsin Ristapääw wälja, siis on agga wiis koppik rohkem sadud kui õigus, agga wallitseja ei tahha ni paljo palka anda kui Herra lubband on.
Kohhus sundis Josepid tenistusse minna ja omma rehknung selletada Kidewa moisas.
Allakirjotand Kohtuwannem: Andrus Spuhl XXX
Mihkel Spuhl Jakob Wuus
Kirjotaja: J. Spuhl &lt;allkiri&gt;
