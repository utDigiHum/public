Ette sai kutsutud Rein Mold Peter Tohmsen siggade pärrast et tagga metsa heina maad ärra tongund. Ollid Kohto ees, ja ei leidnud ühtegi wabbandust. Said kohto mehhe käest kõwwaste nomitud ja moistetud et tullewa Redi lähheb kohto mees maad waatma selle aia sees peawad siggade perremehed pahhandust ärra parrandama; polle agga siis mitte parrandud, sawad keik perremehed omma siggase pärast trahwitud.
Kohto mees: Mihkel Poots XXX
I Kohto kõrwa mees Mihkel Juhkam XXX
II Kohto kõrwa mees Jürri Laur
Kirjotaja Jaan Grünär &lt;allkiri&gt;
Moistetud ja 11mal Oktober Otsa Peetrist üksi täitetud.
