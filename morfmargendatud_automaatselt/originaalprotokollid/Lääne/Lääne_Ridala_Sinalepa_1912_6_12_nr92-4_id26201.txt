Sell üks tuhat üheksa sada kaheteist kümmendamal aastal kaheteistkümendamal Juuni kuu päewal /1912 a. 12 Juuni k.p./ Sinalepa walla talupojad Matsi Kustas Kustase p. Kroon, Abi Willem Jaani p. Anderson, Laoso Kustas Jüri p. Korjus, Arro Jaan Naani p. Madison, Warawa Willem Willemi p. Rõõmus, Loppi Priidik Hendriku p. Birkenfelt ja Mardi Mari Anderson, kes kõik üle 25 a. eanad on, walla kohtule tuntawad, palusiwad kohtu lepingute raamatusse oma järgmist suusõnalist lepingut üles wõtta.
1910 aastal meie ülemal nimetud isikud ja peale seda Rikku koha rentnik Jaan Reinu p. Juldop ja Warawa koha rendnik Willem Rõõmus /wiimane on nüüd ka koha omanik/ tegime eneste keskel kokku lepimisel Abi talu koha peal järgmise suusõnalise lepingu
et Puiatu küla wahe tee mida meie kõik tarwitame nii halwas seisu korras on et märja ajaga läbi pääsemine üsna wõimatu on, sellepärast otsustasime seda teed parandada, nii wiisi et tee laius jääb 3 sülda ja mõlemile poole teed saab 4 jala laiune kraaw kaewatud, mille muld saab kesk tee peale pandud selle täiteks, ja iga maa omanik kelle krundist see tee läbi käib, teeb 2 jalga maad kraawist eemal aja loomade kaitseks ja sise poole aja jääb jala inimestele läbi käimiseks jalg tee. Kui aga kohalik maa omanik mitte ei taha aeda teha, siis wõib ka mõni teine isik seda tänawast wälja jäänud maad pruukida, peab aga selle maa pruukimise eest aja ette tegema. Selle tee paranduse kulu pidime kõik ühe tassa kandma, kus juures Rikku ja Wärawa koha rentnikud lubasiwad kumbki 2 rubla iga aasta tee paranduse kulu maksta nii kaua kui nad rentnikud on, kui aga kohad ära saawad ostetud, siis maksab ostja puuduwa tee paranduse kulu ühe korraga wälja.
Edaspidi saab see tee ära jagatud, nõnda et iga üks saab ühe worra oma parandada ja korras hoida, kui kruusu saada on, siis peab tee kruusega saama parandud ja korras hoitud. Peale seda on selle tee sees 4 trummi teha ja korras pidada, see on iga kahe pere peal üks trumm.
Tänawa aed saab tehtud: kui kiwi kord all, siis üks traat peal, kui aga ilma kiwi korrata, siis peab kaks traati ülestiku olema ja kui aja tegija soowib wõib ka puust teha.
Juure lisame et 1912 a. sai tee parandus ära lõpetud mis läks maksma üks sada neli teist rubla neli kopik /114 rubla 4 kop./
alla kirjutajad lepingu tegijad tõendawad, et see sinane leping on nende eneste kokkulepimisel, selge meele mõistuse ja tahtmise järele tehtud, pärast lepingute raamatusse kirjutamist ette luetud mida oma all kirjadega tõendame.
Lepingu tegijad: J Madison &lt;allkiri&gt; K Korjus &lt;allkiri&gt; K Kroon &lt;allkiri&gt; W Röömus &lt;allkiri&gt; P Birkenfelt &lt;allkiri&gt; W. Andreson &lt;allkiri&gt;
mari Anderson umbkirilik tema palwe peale tema eest kirjutas alla A. Krabi
Kohtu Presidendi asemik: K. Möll &lt;allkiri&gt;
Liikmed:  J. Triksberg &lt;allkiri&gt; J Winglas &lt;allkiri&gt;
Kirjutaja G Krabi &lt;allkiri&gt;
Olen sellest lepingust ärakirja vastu võtnud
P Birkenfelt &lt;allkiri&gt;
G. Korjus &lt;allkiri&gt;
J Juldop &lt;allkiri&gt;
28.12.31 olen sellest lepingust ära kirja vastu võtnud
G Kroon &lt;allkiri&gt;
