Tulli ette Hans Jallakas ja kaebab et Hans Pärri Kerwlist, tulnud ja temma käest on üks raudwankri rattas ärrawarrastanud.
Kohhus kutsus Hans Pärri ette, kaebtust tõeks teggema, siis tunnistas ta, et ta sedda rattast selle pärrast ärrawinud, et Hans Jallakal temmale on 1 rbl. 15 koppik anda, Siis tunnitas Hans Jallakas, et assi nenda tössi on.
Kohto otsus on se, et Hans Pärri peab sedda rattast wäljamaksma 60 kopp. 4mal Nowemril 1867 kohto ette ja ka Hans Jallakas peab sedda 1 rbl. 15 koppik ka selsammal päwal wälja maksma kohtomaias.
