Astus ette Mihkel Utow ja kaebas et Willem Pihelgas Wannamoisa rentnik on tema käest raha lainand ja on jubba ammu tehtud termini aeg ümber agga ei maksa wälja, nüüd on poeg sureks pillijaks Willemi warraga ja tahan see peale kinnitust.
Astus ette Willem Pihelgas omma naisega ja ütles wälja et temal on 50 rubla Mihkel Utowile maksa, andis oma loomad pandiks ja lubas kunni 15. Oktobri k.p. rahha wälja maksa. 
Kohtuw. Jaan Jünter XXX
Kõrwam. Juhan Krabbi XXX
Kirjut: M Kentmann &lt;allkiri&gt;
