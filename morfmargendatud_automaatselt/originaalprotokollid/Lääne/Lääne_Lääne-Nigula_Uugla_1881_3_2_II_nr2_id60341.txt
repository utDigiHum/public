Astus ette Jaan Ulm ja räkis wälja, Hans Lorensil olla temale ühede saapa säärede ette 1 rbl., ja selget raha 3 rbl. 35 kop. ja 4 sakki kattuse sindleid maksta.
Astus ette Hans Lorens ja ütles mitte ise, waid ta naine olla se raha J. Ulmi käest saanud; ta olla ise nattuke wälja pragitud sindlid J. Ulmi käest tonud ja selle eest temale saapaid parandanud.
Astus ette Hanso naene Liisu Lorens ja räkis wälja: Hans teinud J. Ulmile saapa waasud otsa, ta winud need kätte, J. Ulm maksnud saabaste hinnast 3 rbl. temale wälja ja peene raha puduse pärast jänud 35 kop. saamata, teise saapa särel olnud auk sees, ta winud taggasi, kinni lappida, aga pärast J. Ulm ei olla wastuwötnud, siis jänud saapad ja 3 rbl. raha molemad nende kätte.
Kog. kohus moistis, et Hans Lorens peab need 3 rbl. ja saapa säärede eest 75 kop. J. Ulmile taggasi maksma.
Kohtuwanem Jürri Klemann XXX
Körwasmees Adu Weidebaum XXX
Körwasmees Jürri Jaanmees XXX
Kirjutaja A Krein &lt;allkiri&gt;
