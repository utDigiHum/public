Ette astus Mäemõisa walitseja herra Liiwa ja kaebas et Reinholdt Silm mitte oma rendi kondrackti ei täita selle rendi koha üle mis tema käes on. Tema peab oma kondrackti järele 99 päewa suwel ja 110 päewa talwel mõisa tegema, mis 4 päewa nädalis ümber aasta tegewad, aga nüid jättab tema ikka 4ast päew nädalis tegematta.
Ette astus Reinholdt Silm ja ütles wälja:"3 päewa nädalis teen mina kül, aga neljat päewa ei luba ma mitte teha, sest see on selle Tsetwerti rukki ja 4 wana wakka odrade eest mis mulle mõisast antakse. Mõisa herra wõib seda wilja tagasi saada ja neljat päewa ma nädalis ka ei luba teha.
Kohus mõistis et Reinhold Silm peab need päewad kondracti järele mõisa ärra tegema, sest kondracti lehe peal ei ole mitte ööldud et Reinholdt Silm neljas päew nädalis selle wilja ette teeb. Peale selle peab Reinholdt Silm Jürri päewast wõlgo jäend 9 päewa a' 60 kop. ühte kokko 5 rbl. 40 K. mõisa kahe nädalai aja sees ärra maksma.
Kohtow. Mihkel Reets XXX
I Kõrwam: Hindrek Reets XXX
II Kõrwam: Hindrek Tõnts XXX
Kirjut: L Marleij &lt;allkiri&gt;
