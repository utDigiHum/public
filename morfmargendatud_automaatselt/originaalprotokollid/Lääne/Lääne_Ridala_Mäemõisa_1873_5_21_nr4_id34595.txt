Ette sai woetud se Kristohw Prosowits sapa wargus, mis No 2 polele jäi Küünla Ku 12 päwal 1873.
Tulli ette Marri Okkapu Kebla wallast, kes tunnistab, et Kristohw Prosowits on keige wimaks senna wankri jure jäänd kus sapad peal, agga polle ma sedda näind kes need sapad on warrastand.
Kristohw Prosowits tunnistab et temma polle mitte üksi senna wankri jure jäänd, waid Marri Okkapuga seltsis sealt wankri jurest ärra tulnd ja Kohhe Marriga ühhes seltsis linnast wälja sõitnud ühhe hobbose peal, ja te peal Hapsalo ja Tromi kõrtso wahhel on nemmad weel Parrila wallast Sinnika Jaani enneste hobbose peale wõtnud, ja sõitnud kunni Tromile.
Kohhus on leidnud selle tunnistus järrele Kristohw Prosowits ilma süta, ja kutsuti Juhhan Joldop ette, kes sedda keige esmalt wargaks tunnistas omma kangussega wandudes (wata lehh. külg 104) et ta piebli tallitaia ollema, ja Kristohw tõeste warras, nüüd tunnistas et temma ei olle näind egga kegi teine tedda warrastawad neid sapaid, moistis Kohhus, et Juhan Joldop peab Kristohw Prosowitsil need päwad mis ta Kohto ees on käind ja polle mitte teol olnd maksma, se on kahheksa teo päwa, siis peab Juhhan Joldop Kristohwil kaks rubla kahheksa kümmend koppik, mis ühhe näddala aia pärrast peab makstud ollema.
Kohtowannem Willem Prosowits
Hindrek Reets
Mihkel Reets
Kirjotaia J Spuhl &lt;allkiri&gt;
Mõistetud ja mõistus täidetud.
