man olli Päkohtomees    Johan Lucht
    "      "     kohtomees     Jaan Koch
    "      "       ---------      Johan Uint
   walla wannem    Jaan Uint
Tulli kohto ette selle walla perremiis Märt Tobro nink kulutus et Jürripäiwal s. a. tallo kontraht henda poja Johani pääle lasknu kirjotada nink pallus 
Prottocolli kirjotada et henda tallo kraam 
nimmelt      2  Obbust
                    4   karjaellajat
                    2   Raudwangrit
                   12   wakka Touwilja seement
        ja muu tarwilik tallo kraam
        keik henda poja Johani kätte annap.
        sest et henda terwus ennamp ei kana tallo perremehhe ammetit piddada.
       Es selle maja kontrahti erra andmisse perrast wasto ütlemist ei olle 
     
        tunnistawa   Walla wannem   Jaan Uint    x    x    x
               "            Päkohtomees      Johan Luht   -| --| --|-
               "             kohtomees          Johan Uint     x    x     x
               "             kohtomees          Jaan Koch    -| --| --|- .
