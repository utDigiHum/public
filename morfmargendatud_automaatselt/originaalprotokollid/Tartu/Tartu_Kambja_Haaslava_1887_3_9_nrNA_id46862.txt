Sel 9 Märzil 1887
Kaibas Otsa Jürri Luha, et kui tema Wallawanemb ollnu om tema Mõrrakülla koolimaja päiwa ratta ja wõlli teha lasknu, mis 16 rubl massnu, tema om selle raha ärra massma pidanu ja ei olle seda Kogukonna käest weel senni aijani kätte sanu, tõiselt om tema sis sama Kaubmees Mondsoni jurest Kooli tarre ahju tarwis 2 rubl 76 kop rauda tua lasknu, mis temal ka samata om ja keda tema Mondsonile wälja massa om pidanu.
Jürri Luha nõuab seda raha Linki käest kes päle tema wallawanemba assemenik ollnu ja temale raha massma pidanu.
Johan Link wastutas, et Jürri Luha sel aijal kui temma wallawanemba assemenik ollnu, seda raha tema käest nõudnu ei olle, tema andno ommal aijal rehnung ja Kogukonna raha praegutse wallawanemba Jaan Soo kätte ärra ja ei wõi nüid kuskilt seda raha massa.
Jürri Luha andis ülles, et tema selle raua Kessa Johan Zobeli läbbi tua om lasknu, ja selle et Mondsonil ahju ust ja trati ei olle ollnu, om Johan Zobel sis Faure jurest muist tonu ja selle est warsti ärra massnu.
Otsus: et Johan Zobel ja Koolmeister Jaan Antik ette om kutsuta.
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
"  Johan Raswa XXX
