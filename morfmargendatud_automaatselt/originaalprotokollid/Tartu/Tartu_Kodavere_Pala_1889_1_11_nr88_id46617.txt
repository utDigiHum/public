Kadunud Josep Sallo waeste laste wöörmünder Karel Waddi astus ette ja andis aru 1888 a. Lauri-Märdi talus sissetulnud ja wälja läinud rahade üle järgmiselt:
                        Sissetulek:
Rukkide eest			46 R.		Nisude eest			12 R.		Lina seemnete eest			11 R.		Otrade eest			23 R.		Linade eest			30 R.		                                         Summa			122 R.		
                       Wäljaminek.
renti			120 rubla		wallamaksu			 16 rubla 32 kop.		saapa raha			 11 rubla     -		puu raha			 12 rubla    -		soola raha			  4 rubla    -		raua raha 			  5 rubla    -		    Summa			174 R.    32 kop.		     Sissetulek 			122 R.       - 		     Jääb puudu			  52 R.   32 kop.		
Linad olla küll weel müümata, aga et tänawa aasta nende hind mitte rohkem ei ole, siis on tema  neid 30 R. pääle arwanud kaal. Pääle selle olla weel rukkid, kellest ehk kewade poole üks osa 
äramüüa wõib järele jäänud. Wäljaminek 52 rubla 32 kop. suurem kui  sissetulek. Kewadese rendi tarwis ehk saaks rukide müümise läbi raha aga mõisawõlga ei tulla tänawa kuskilt sisse.
Otsus: tulewaks kohtupäewaks Karel Waddi ja Johan Pärtin Otsuse tegemiseks ettekutsuda. 
                                               Peakohtumees: J. Sarwik.
                                                     Kohtumees: W. Oksa.
                                               Kirjutaja O. Seidenbach.
Märkus. Revidint one 12 Januar 1889.
                                E. Mühlen. (allkiri)
