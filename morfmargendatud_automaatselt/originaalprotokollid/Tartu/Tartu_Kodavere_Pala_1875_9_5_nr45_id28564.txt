Peale otsuse olli seie tulnu Jakob Rättsepp, kes ütles, et temma  tänna essimene käsk sanu seie tulla, ja ka tulnu.Tunnistaja andis küssimise peale protokolli ad 1. Karel Kool on rükkinurm kül kõrraperrast harrinu, wäetanu ja  külwnu, agga orras on kül ülles tullemise järrel sõrre olnu.
2. ristikheina külwist temma ei tea , et erme aedda ärraläinu.
3. Krawit, mis kessanurmes on puhastut sanu.
4.aijad on süggise kül parrandamata jänu, agga kuidas kewade luggu olnu, temma ei tea.
5. Küini kattus  sanu minnewa suwwel heinaaeg parrantud.
6. Söti ei olla Kool kül temma aeg, se on minnewa aasta Jürripäewast Jõuluni, juurde teinu, ei ka nurme sötis jätnu.
7. Nurme ordnungi seggamise perrast tunnistaja middagi ei tea.
Sai                                                                                       mõistetud:
                         Kohtukäijat üksi teises kohtupäewas ettekutsu.
                                          Peakohtumees Paul Willemson
                                           Kohtumees: J. Hawakiwwi
                                           Kohtumees:J.Rosenberg
                                            Kirj. allkiri.    
