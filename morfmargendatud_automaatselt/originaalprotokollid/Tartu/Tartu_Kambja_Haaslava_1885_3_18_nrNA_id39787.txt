Sel 18 Märtsil 1885
Kaibas Kristjan Toom Surest Kambjast, et tema om suwel 1878 astal Tanni Johan Toomi jures tenistusen sis om tema pidanu 6 wakka Rükki ja 6 wakka keswi palkas sama, ent ei olle rohkemb sanu kui 2 wakka Rükki ja keswi est 7 Rubl raha ja pallus et kohus seda palka massma sunis.
Johan Toom wastutas, et Kristjan Toom om heina tegemise allustussen ärra lännu ja om selle tenitu aija est massetu sanu.
Kristjan Toom andis Papa Johan Luha tunistajas ülles, et tema keik suwwi teninu om.
Otsus: et Johan Luha ette kutsuta om.
Päkohtumees J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
 J. Krosbärk /allkiri/
