Haslawa kogokonna kohus 20 Augustil  1880
Juures istus:
Peakohtumees  Mihkel Klaos
kohtumees Joh. Link
dito  Joh.  Udel
dito Rein Anko
Tuli ette  Johan Klaossen ja kaebas   Johan Tüüri pääle:
1. on  Joh. Tüüri hobused tema tõuwilja söötnud ning nõuab selle eest 5 rubl.
2. tõiseks on  Tüür teda loomawargaks sõimand ning nõuab tema trahwimist.
3. kauba järel ei ole Tüür hoonid parandanud ei ka aeda ning nõuab selle eest 25 rubl.
4. 13 wakamaad on Tüür pidanud tõiks [?] tegema ning on tegemata jätnud à wakamaa 20 rubl. - 260 rubl.
5. heina on lasknud ära mädaneda, sest et õigel aeal ei ole tehtud. - 60 rubl.
Sai  Johan Tüür selle üle küsitud ning wastutas:
ad 1. ei salga, et üks kõrd hobune on wiljas olnud.
ad 2. salgab Joh. Klaosseni loomawargaks sõimanud;
ad 3. ei ole hoonid parandanud ei ka aeda
ad 4. ütleb, et temale ei ole peremees näidanud aga tegemata on jäänud.
ad 5. heinad on wihma saanud, aga ei ole mitte mädad. 
