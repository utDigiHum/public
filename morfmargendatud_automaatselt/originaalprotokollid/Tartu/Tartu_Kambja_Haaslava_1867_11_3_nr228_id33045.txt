Lillo Meijer kaibap, et Michel Traat laimanut temma kaest lootsiku, Michel Traat ei olle ännamb lootsiku taggasi andnut sial kost wotnut nink lasknut lootsika ahhelat ka ätta warastada kiige lukkuga. Lillo Meijer nowwap lootsik ahhila ja lukkuga Michel Trati kaest taggasi nink lootsiku est üri Rubla hbd.
Michel Traat wastutap, et temma lainanut kül lootsiku Lillo Meijer kaest wiinut agga jälle pääle tarwitamist lootsiku taggasi nink pandnut illusaste kinni ja andnut wothi Michel Traat man ärra. Lootsik ollewat agga parhilla Lepko hainamaa pääl.
Otsus: Michel Traat piap tunnistajate läbbi selgest teggema, et temma lootsiku taggasi wiinut ja illusaste lukku pandnut.
Jaan Michelson saab Koggokonna kohtu ette tallitedus.
Peakohtomees: Johan Hansen XXX
Kohtomees: Ado Ottas XXX
Kohtomees: Jakob Jansen XXX
