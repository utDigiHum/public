Sel 29 Aprilil 1885
Selle otsuse päle sest 15 Aprilist s.a Jaan Semeli kaibuse asjan wastu Peter Luha olli kutsumisse päle Wastsest Kambjast Johan Trossek ette tullnu ja tunistas et minewa ehk ülle minewa asta om Tarto Linan Jaan Semmel seda 8 Rub Peter Luha käest küssinu. Peter Luha om sis üttelnu, et praegu ei olle raha ja lubanu tõine kõrd massa.
Mõistetu: et Peter Luha se 8 Rubl 8 päiwa sehen selle kogukonna kohtu kätte sisse massma peab, kust sis se raha Wastse Kambja Kogukonna kohtu kätte Johan Semmeli perranduse hulka saab sadetu, selle et Peter Luha se tunnistaja wälja ütlemisse perra wõlgu om ja se raha Peter Luha jubba Johan Semmelile wõlgu om ollnu.
Se mõistus sai Jaan Semmelile kulutado ja õpetus leht wälja antu sel 20 Mail 1885 sai se mõistus Peter Luhale kulutado ja õpetus leht wälja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
 Peter Iwan /allkiri/
