15b Piri Jaan Annijerw wõttab omma tallo mis Piri lanes om 1 aasta päle se om Jürripaiwast 1861 seni kui 1862 Teoorjuse päle 156 jalla paiwa est.
Ras.d. 14. August 1860
Jaan Annijerw XXX
(Allkiri)
16.b Piri Peter Warres wõttab ommal tallo mis Pirilanes om 1 aastas se om Jürri paiwast 1861 seni kui Jürri p. 1862 Teoorjuse pale 156 jalla p. est.
Ras. d. 14. August 1860
Peter Warres XXX
(Allkiri) Moisawall(itsus)
17.b Jacob Juksar wöttab se "Owrawa" maja 1 aasta päle se om Jürri p. 1861 seni kui Jürri p. 1862ni Teoorjuse päle ja teeb aastas 104 jalla p. ja 2 süldu puid moisa. 
Rasin 14l August 1860
Jacob Juksar XXX
(Allkiri) Moisawall(itsus)
24.a Hindrik Zirna ja Jacob Wori wöttase se "Anni" /Adami/ Tallo Uelejoe küllast 1 aasta päle Teoorjuse päle ja tewa  köik nenda kui wannast, agga obbuse paiwi tewa nad egga Tadre päld üts rohkemb sis om neil ütte kokko 154 obbuse ja 157 jalla p. aastas tetta. Aasta allustab Jurri p. 1861 seni kui 1862ni.
Rasin d. 14. August 1860.
Hindrik Zirna XXX
Jacob Wori XXX
(Allkiri) Moisawal(itsus)
18b Pedo Peter Tamm wöttab se "Muga Jago" maja mis Lahhul om  ütte aasta päle se om Jürri p. 1861 - 1862ni 104 Jalla paewa est Teoorjuse päle ja Tob wel 2 sülda puid moisa.
Rasin d 14 August 1860
Pedo Tamm XXX
(Allkiri)
19.b Cristjan Mertman wõttab se "Wahhi" maja Mae silla otsast ütte aasta päle teoorjuse päle ja teeb se est 104 jalla p ja tob 2 sülda puid moisa. Aasta allustab 1861 seni kui 1862ni.
Rasin d. 14. August 1860
Christjan Mertman XXX
(Allkiri) Moisawall(itsus)
