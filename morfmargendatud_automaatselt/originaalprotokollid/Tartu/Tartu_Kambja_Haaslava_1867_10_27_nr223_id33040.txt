Ado Ottas kaibap, et Johan Pappi hobbene olnut temma seemne ristikhaina seen nink teinut sial weiga palju kurja, temma wotnut hobbest kinni nink nowwap kahju tassomist. Johan Pappi kaest.
Johan Papp wastutap, et temma hobbene olnut kül kinni Ado Ottassi man, temma arwap agga, et si hobbene oma karja söödi päält kinni ajatut.
Moistus: Ado Ottas ei olle lasknut kahju ülle kaija, ei woi ka keddagi kahju tassomiseks nouda. Pandirahha piap Johan Papp 8 päiwa seen 1 Rubla Ado Ottassil ärra massma.
Pähkohtomees: Johan Hansen XXX
Kohtomees: Jakob Jansen XXX
Kohtomees: Jaan Purrik
Moistus sai kuulutut, nink surremba kohtukäimisse õppetusse kirri wälja andtu.
