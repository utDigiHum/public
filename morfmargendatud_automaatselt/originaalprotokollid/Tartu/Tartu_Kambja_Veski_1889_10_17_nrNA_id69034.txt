Sellsamal päewal 17 oktobril 1889.
Jüri Soosaare kaebtuses Johan Tiitsu wastu lamba warguse perast kutsuti ette tunnistaja Juhan Soosaar, ja tema wastas küsimise pääle, et tema tundwat oma wenna lambaid, üks lammas olewat pitka höörikude sarwedega, tõine wähem tänawune lammas olla lappiku sarwedega, aga kolmandal kahe aastasel lambal olla sarwe otsakeisi ainult naha all, willu sees tunda. -
Otsus: Kui Johan Soosaar ja Peeter Mitt wande läbi oma tunnistust tõendawad, siis wõib Jüri Soosaar omad lambad kätte saada. -
