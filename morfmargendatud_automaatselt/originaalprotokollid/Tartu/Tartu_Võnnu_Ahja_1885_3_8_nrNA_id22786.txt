Tulid tänasel pääwal kohtu ette Joosep Kurg (Kurik) ja Jaan Rooma koolnud Jaan Piirsoni tütre mees ja palusiwad protokolli alanseeswat lepingu ülespanna:
Joosep Kurg maksab Jaan Rooma näise Mari Rooma sündinud Piirsoni hääks 600 rubl. tänasel pääwal ära ja ei ole temal siis edespidi enam midagi oma isa ega ema pärandusest nõudmist, mis testamentis üles tähendatud on. Selle lepinguga oliu ka ema Ann Piirson käemehe Dawit Raudsepa juures seistes rahul, mis peale nemad omad nimed alla kirjutasid.
Jaan Rooma &lt;allkiri&gt; Joosep Kurg XXX
Dawit Rautsep &lt;allkiri&gt; Ann Piirson XXX
Otsus: Et Joosep Kurg 600 rubl. Jaan Rooma hääks, see on tema naise Mari hääks wälja maksnud on ja et nemad nõnda leppinud, kui ülewal s??l tahendatud - saab sellega kogukonna kohtu poolt tunnistatud
Peakohtumees: P. Kripson XXX
Kohtumees K. Iwanthal XXX
Kohtumees: P. Hindrikson XXX
Kirjutaja: Chr Kapp &lt;allkiri&gt;
