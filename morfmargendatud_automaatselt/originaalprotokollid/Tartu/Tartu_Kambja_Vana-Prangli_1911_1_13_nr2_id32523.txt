1911a. Januaril 13päewal on WanaPrangli walla liikme Kusta Jaani p. Nuggin ja Aakre wallaliikme Juhan Jaani p. Aleksandersoni wahel alljärgnew rendi lepping kindlaks tehtud ja maha kirjutatud: 
§1. Kusta Nuggin annab tema peralt olewast WanaPrangli walla "Erle" talu maast Laksbergi talu piirist mõõdetud summa kümme (10) wakkamaad rendi pääle Juhan Aleksandersonile kaheksa (8) järgestiku aasta pääle see on 23 April 1911a kuni 23 April 1919 aastani ja maksab raha renti Juhan Aleksanderson iga aastal kolmkümmend wiis (35) Rubla 1 Aprillil iga aasta täiesti ette ära. 
§2. Selle renditu maa pääle wõib Juhan Aleksanderson oma tahtmise järele hooneid ehitada, mis tema omaks jääwad igal ajal. Kui see rendi leppingu aeg möödas on, siis on Kusta Nuggin kohustatud uus rendi lepping tegema ilma rendi kõrgendamata. Juhan Aleksandersonil on õigus saada weel rendile oma krundi weerelt põllu maad wiie (5) wakkamaa kuue (6) aasta pääle, kus juures rendi hind mitte üle kolme rubla wakkamaa päalt ei tohi olla; need wiis wakkamaad jääwad ka edasi rendile, kui aga Juh. Aleksanderson soowib endise rendiga. 
§3. Kusta Nuggina karjas käiwad kõik Juhan Aleksandersoni lambad ja teeb iga lamba eest ühe jala päewa (mees ehk naene); kui päewasid ei tee, siis maksab tasu iga lamba eest 80 kop.
Wäljakäimiseks tee peab Juhan Aleksanderson ise tegema, aga sillade ehitamiseks materjali annab Kusta Nuggin. 1911a. kewadel annab K. Nuggin wiis (5) koormat sõnnikud Juhan Aleksandersonile, põllu pääle panemiseks, kui häin ära tehtud on ja põld kokku pantud, siis on waba Juhan Aleksandersoni loomadel käia ka Kusta Nuggina heina ja põllu maade pääl. 
§4. Rentnikul ei ole õigust, ilma omaniku lubata ühtegi elaniku selle maa tükki ei ka oma hoonetesse wõtta; ei ka kohta teisele ära anda, renti ehk omme hooneid ära müia. Kui peaks juhtuma et wargust, ehk meelega kirja tegemist ette tuleb, siis lääb see lepping tühjaks ja omanik wõib igal ajal Juhan Aleksandersoni wälja laske tõsta ning maa tükki temalt ära wõtta. 
§5. Kui kumbki pool enne selle rendi aja lõppuni peaksid ära surema, siis kestab see lepping ka perandajate wahel edasi
Esimese aasta rent on täna ära maksetud kõik. [arusaamatu märkus diagonaalis. homme?]
Rendile andja: Kusta Nugin [allkiri]
Rendile wõtja: Juhan Aleksanderson [allkiri]
Et see rendi lepping on tehtud wallakohtule isiklikult tuttawate Kusta Jaani p. Nuggin ja Juhan Jaani p. Aleksandersoni wahel; nendele ette loetud ja nendest oma käega allakirjutatud; nendel on õigus aktisid teha. See kõik saab Prangli wallakohtu poolt tunnistatud, sell 13 Januaril 1911 aastal. Kohtu eesistuja: K. Pala [allkiri]
Kohtu liige: P Keis [allkiri] JWirmann [allkiri]
Kirjutaja: J Weske [allkiri]
No 2. 
