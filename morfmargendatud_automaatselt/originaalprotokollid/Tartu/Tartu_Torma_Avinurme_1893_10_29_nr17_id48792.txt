XXI Awinurme Wallakohtus sel 29 Oktobril 1893
koos olid:
Kohtueesistuja Jaan Kalaus
Kohtuliikmed Jaan Paas
                         Joosep Saar
Astusid ette Andres Pihlak rätsep Adrakust ja Andres Karu Wadilt ja palusid oma kauba lepingut prottokolli ülesse kirjutada ja nimelt
Rätsep Andres Pihlak wõttab Andres Karu omale õppipoisiks rätsepa töö juure oma leiwa ja korteri pääle ning muretseb ka magamise aseme ja magamise riiete eest. ühe aasta jooksul, ja maksab aga Andres Karu selle ühe aasta õppetamise eest Andres Pihlakale nelikümmend (40) rubla oppetuse raha 20 rubla maksab nüid kohe ära ja 20 rubla maksab siis kui aasta tais saab, peab aga Andres Pihlak oma õppipoissi Karud ilusasti ja täiesti õppetama nii palju kui tema isi oskab ja ei woi midagi asja salaja teha et poisile ei taha näidata, nin
Keiserliku Majesteedi käsu pääle
Sel 29 Aktobril 1893 on XXI Awinurme Wallakohus I Jurjewi Ülema Talurahwa kohtu ringkonnas Joosep Kaunissaare ja Mihkel Tederi kauba lepingu üle kuulanud, ja et wallakohtul selle wastu ei ühtigi ütlemist ei ole siis kinnitab tema seda kaupa etse kindlaks jääb.
Jaan Kalaus. (allkiri)
Jaan Paas (allkiri)
Joosep Saar (allkiri)
