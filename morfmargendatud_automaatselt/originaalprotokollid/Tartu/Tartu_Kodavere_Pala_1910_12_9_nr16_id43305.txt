1910 a.9 detsembril Pala mõisaomaniku Alexander v. Stryki woliniku Carl Jaani p. Nemwalzi ja Karel Jüri p Soiewa wahel kokkulepitud mõisamaa pidamise kontraht Kingseppa N XXVII talu üle
ustawaks tunnistus. Üks eksemplar kirjakogusse jäetud.
Esimees K. Wahi
liikmed J Otter J Uusen P Härma
Kirjutaja Brükkel.
