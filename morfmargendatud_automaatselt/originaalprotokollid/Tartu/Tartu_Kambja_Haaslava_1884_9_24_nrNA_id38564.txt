Haslawa Kogukonna Kohus sel 24 Septbr 1884
Man ollid: Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
Kaibas Johan Mällo, et kuu aija est om temma omma wanembit linna satma lännu sis ollnu wässind ja eidanu tee werde magama sis om Johan Klaosen temma jallast ütte Sapa ärra tõmanu, karmanist wõtnu 4 Rubl ninna rättiko ja kübara pääst ja wienu need asjad ärra.
Johan Mällo nõudis neid asju Johan Klaoseni käest ärra.
Johan Klaosen wastutas selle päle, et tema hurjutanu Sulpiga, kes enemb Roijolt ärra lännu, tema lännu perra ja leidnu selle Johan Mällo tee weren, tema arwanu et Sulp ollnu ja se magaja käsknu tõist sabast maha tõmmata, tema tõmanu sapa maha mitte warguse ja wiinu kodu, raha rättiga ja ka mütsi ei olle tema wõtnu.
Päle selle lepisiwa nemma nenda ärra et Johan Klaosen massab 4 Rubl ärra ja annab selle sapa warsti kätte se 4 Rubl peab Johan Klaosen 8 Oktobril s.a. seija sisse massma, kost Otsa Jürri Luha selle raha Johan Mällo assemel wasta wõttab.
Otsus: Seda Protokolli ülles panda.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
" Peter Iwan /allkiri/
Sel 12 Nowembr 1884 massis Johan Klaosen 4 Rubl Johan Mällole ärra.
Ollen se raha kätte sanu.
Johan Mällo XXX
