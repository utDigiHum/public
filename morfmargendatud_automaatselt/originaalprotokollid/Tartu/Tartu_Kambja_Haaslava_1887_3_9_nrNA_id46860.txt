Sel 9 Märzil 1887
Selle otsuse päle sest 24 Nowembrist 1886 Jaan Treiali kaibuse asjan wasto Anna Kroman olli kutsumise päle ette tullnu Sisaski Jaan Klaosen tunistas et tema seda ei tea kelle perra se heinamaa om. Üits kõrd ollewat mõtsa herra üttelnu et Arju tallo perra om üts lisna tük.
Kohtumees Johan Kliim andis ülles, et seda heinamaa ollnu enne ärra põimetu se olli Anna Kroman ülle nita lasnu ja neid tõisi heino wõis olla üts ruga ehk 20 punda. Tema käsknu Jaan Treialil need heinat ärra wõtta ent se ei ollewat wõtnu ja et se heinama tük joba enne Treiali tallo jagu om ollnu.
Kohtumees Jaan Hansen andis nisamma ülles. 
Mõistetu: et se heinamaa tük Treiali tallo omanikule jääb ent Jaan Treiali heina nõudmine om tühjas mõista, selle et Jaan Treial essi neid heino Kohtumeeste kässu päle ärra ei olle wõtnu ja et Anna Kroman 3 rubl 8 päiwa sehen waeste heas trahwi peab massma, selle et tema omma wõimoga Treiali tallo heinamaad nitma om satnu. 
Se mõistus sai kulutedo ja õpetus leht wälja antu ja Anna Kromani assemel selle mehele Jaan Kromanile.
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
"  Johan Raswa XXX
