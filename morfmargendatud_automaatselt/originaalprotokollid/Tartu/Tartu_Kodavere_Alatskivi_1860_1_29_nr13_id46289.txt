Iwan Nikiforow kaibab: "Teda olewa Nefed Nikifin 4 kõrd kõrwu pite löönud ja 2 kõrd ialaga taha, ja tema /Nefedi/ naine olewa teda /Iwani/ sõimanud: "Kurat on sind ristinud!"
Wastab Nefed Nikitin sellepääle: "Se kaibus olewa wale; Iwan Nikifonow olewa tema naist Awdotja Waiwischnat litsiks sõimanud ja 2 kõrd kõrwu pite löönud," mis ka naine tõeks ütleb, aga salgab, et tema ristmist ei ole nimitanud.
Gurjan Iwanow, Raskolnik, tunnistab ja nendasamoti ka Matrena Nikolajewa: "Nefed olewa Iwani 4 kõrd löönud."
Timofei Iwanow ja Iermolai Lewdokimow, mõlemad Õigeusulised tunnistawad: "Iwan Nikifonow olewa naist 2 kõrd rindu löönud, aga ristmise sõimamist ei olewa olnud."
Et kaibajal ja süüdlasel omad tunnistajad, kes kaksilde tunnistawad, ja tunnistajit wannutada ei wõi, ei saa siin midagi mõistetud, ja wõiwad nemad suuremas kohtus õigust otsida.
