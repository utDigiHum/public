Protokoll
Awwinorme Wallakohtus sel. 13 Märtsil 1898.
koos olid
Eesistuja Mihkel Pihlak
kohtumees Jaan. Pärn
kohtumees Josep Ambos
Astus ette  Awwinorme valla ja Kaewusaare küla talu koha omanik Andres Sildnik ja palus Wallakohut selle üle üks kauba lepping teha et tema oma päristalu koht Posti N 191. Awwinorme inimese Maddis Unti kätte renti peale annab ja on nende kauba sees järel nimetud tingimised: 
1. Andres Sild Marti poeg annab oma Talukoht Posti N 191. Awwinorme inimese Maddis Unt Toma poja kätte kümne 10. Aasta peale renti peale. selle renti ette 90 rubla Aastas ja maksab Maddis Unt peale selle renti veel krono rent ja teised krono ja wallamaksud mis selle krunti ette maksa tuleb, Maddis Unt maksab pool renti kewade. 23 Aprilil 45 rubla ja teinepool renti süggise. 23 Oktobril 45 rubla igga Aasta, praeguse 1898 Aasta ette maksab Maddis Unt nüid kohe ükssadda rubla (100) rubla. ühekorraga ära mis rahast 10 Rubl. 1899 Aasta ette jäeb 
2) Maddis Unt rentnik wõttab talumetsast keik põletuse ja aja puud nenda palju kui tarwis, agga ei tohi mitte müija nendasamuti ei tohi rentnik talumaade pealt sadud põllu põhku ehk heinu müija
3) Rentnik Maddis Unt peab taluhoned mis tema kätte prukimiseks sawad heakorra peal hoidma. ja nende kottusid kui tuul neid rikkub, parandama. nendasamuti ka keik ajad kohendama
4. Rentnikku Maddis Unti kätte saab 7 wakka alla külwatud rukki põldu ja peab Maddis Unt kontrakti aja lõppetusel ja talu taggasi andmisel 6 ehk 7 wakka alla külwatud rukki põldu taggasi andma.
5. Kui Wallas peaks tulekahjusid juhtuma siis peab rentnik Maddis Unt se jaggu materiali põlendmaja juure veema mis selle talu pealt wija tuleb, ja võttab palgid ja lattid talu metsast., mis raha tuleb maksa, maksab renti peale andja Andres Sildnik
6. Kui peaks selle väljarentitud Posti talu peal tulekahju juhtuma teadmatta põhjusel siis peab rentnik Maddis Unt nenda palju kulusid kandma mis Tulekahju seltsi poolt antud abbist puudu jäeb., Kui agga tulekahju pikse läbbi sünnib, siis kannab sedda kahju koha omanik Andres Sildnik.
7. Rentnik Maddis Unt peab keik walla orjused ja teeteud ja Õppetaja vilja maksud mis selle talu pealt täita tulewad oma poolt ära täitma ja maksma.
8. Selle ostetud metsa järel mis Andres Sildnik on Wenewere mõisa küllest omale ostnud, watab rentnik Maddis Unt.
Selle kauba kinnitamiseks on molemad kauba teggijad omad nimed selle kontraktile oma käega alla kirjutanud.
Andres Sildnik (allkiri)
Maddis Hunt (allkiri)
1898 Aastal Märtsikuu 13 pääwal on se eesseisaw suusana lepping ja kontrakt Awwinorme Wallakohtule I Jurjewi Ülema talurahwakohtu ringkonnas Awwinorme Wallamajas kinnitamiseks ettekantud Awwinorme walla ja Postitalu peremehe Andres Sildnik Marti poja ja Awwinorme inimese Maddis Unti Toma poja poolest kes Awwinormes elawad. talukrunti renti peale andmise pärast ja on nende lepping ja kontrakt tänasel pääwal Akti ramatuse N. 5 all üles kirjutud ja on kauba tegijad omad nimed oma käega allakirjutanud ning kohus nende leppingud kinnitanud.
M. Pihlak. (allkiri)
J. Pärn (allkiri)
J. Ambus (allkiri)
Kirjutaja Schulbach (allkiri)
