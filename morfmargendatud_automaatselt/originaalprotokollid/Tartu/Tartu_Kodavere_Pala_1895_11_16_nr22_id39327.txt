1895 aasta Nowembri kuu 16 päewal kinnitatud wendade Jaan ja Paul Jakobi pg. suu sõnaline leping: ,, Paul Laumets läheb kroonu teenistuse ja jätab oma waranduse osa wenna Jaani kätte 1 hobune wäärt 30 rbl. Üks lehm, wäärt 15 rbl ja üks siga wäärt 6 rbl. kokku 51 rbl. Peale selle 1 setwert rukkid, 1 tsetwert odre ja üks tsetwert kaeru mis  mitte hinna alla ei ole pandud. Talu koha eest on mõisa raha maksetud, sellest lubab Jaan oma wenna Paulile 80 rbl ja isa poolt 5 rbl wankri raha. Seega jäeb Pauli warandust wenna kätte 135 rbl  raha ja 3 tsetwerti wilja.
                          Lepingu tegijate alla kirjutus.
                          Kohtueesistuja A Saar
                                    Kirjutaja   KSepp.
