Said ettekutsutud Jürri Talli waestelaste wöörmündrid Gustaw Soo ja Josep Soiewa selle üle aru andma, kudas nemad Talli warandust walitsenud on. Majakraam olla keik alles nii kui protokoll N. 41 seda ülestähendab,  muud kui üks palitu olla ära müüdud saanud 94 Rubla 20 Kop. raha, mis wõlgade eest sissetulnud, on Tartu pangas intressi pääl, ja 100 R. Gustaw Soo käes olnud, kellest tema 19 R. wälja andnud, nii et tema käes praegust seisab 79 rubla.
                                    Sissetulek.
tööde eest			15 R. 13 Kop.		G. Soo käest kapitali			21 R.  -		intressi			-       37 Kop.		                            Summa			36 R. 50 Kop.		                            Summa			 36 R. 50 Kop.		Wäljaminek maha 			36 R. 50 Kop.        		Jääb järel			 -  R.  -   Kop.		
                                    Wäljaminek.
1 setw. rukkid			7 R.		1 setw. odre			6 R. 50 Kop.		poisile saapad			2 R. -		       Transport			15 R. 50Kop.		tütrele saapad			 1 R.  30 Kop.		saapa parandamine			 1 R.  20 Kop.		põletamise eli			 1 R.  -		Kalad			 2 R.  -		2 puuda soola			 1 R. 20 Kop.		pojale kooliraha			10 R. -		puude eest			 2 R. 30 Kop.		maa rent			 2 R. -		                        Summa			36 R. 50 Kop.		
Selle järel on Jürri Talli laste raha Tartus pangas 94 R. 20 Kop.
Gustaw Soo käes                                                       79 R. -
                                                                        Kokko 173 R. 20 Kop.
Otsus: seda, kui sündinud, ülestähentada.
                       Peakohtumees: J. Sarwik.
                              Kohtumees: J. Stamm.
                              Kohtumees: M. Piiri.
                       Kirjutaja O. Seidenbach.
