Haslawa Kogukonna Kohus sel 13 Augustil 1884.
Man olliwa: 
Kohtumees: Johan Klaosen
Peter Iwan
Johan Grossberg
Kaibas Haslawa Kõrtsimees Mihkel Rattasep, et temmal 27 Rubl Jaan Posti käest sada om ja pallus et kohus seda massma sunnis.
Jaan Post wastutas, et temma seda raha Mihkel Rattasepale wõlgu ei olle ja nõudis 4½ wakka Rükki Kaibaja käest, mis tema 3 asta est kaibajale Magatsist omma pulka päle andno om ja ka neide rükki 3 asta odus.
Mihkel Rattasep wastutas et tema rükki wõttnu ei olle ja selle raha perrast teab Otsa Jürri Luha ja Jaan Sawik tunistada.
Otsa Jürri Luha tunistas et tema üts kõrd kuldnu, kui Jaan Post üttelnu, et mina massan se 20 Rubl ärra kui mina Jürri Luha käest saan.
Jaan Sawik tunistas, et tema kuldnu kui Mihkel Rattasep 25 Rub Jaan Posti käest nõudnu sis om Jaan Post üttelnu kül minna massa sinnu omma ka ärra.
Roijo Jürri Klaos tunistas et 2 nädali est om seija kohtu eentuan Jaan Post ja Mihkel Rattasep omma wõlgu selletanu, seda raha ollnu 24 ehk 25 Rub sis om Jaan Post üttelnu, et mes meije kohtu ette lähema kül mina massa se raha ärr.
Endrik Teder tunistas, et tema ollnu jures kui Jaan Post selle Mihkel Rattaseppa wastu üttelnu: "anna minule weel 5 Rubl päle minul om Wallawanemba käest 30 Rubl sada sis saad sinna oma raha ütte kõrraga wallawanemba käest kätte.
Jaan Post andis ülles, et temal tunistust ei olle, et tema 4½ wakka Rükki Mihkel Rattasepale andno.
Mõistetu: et Jaan Post se 27 Rubl wõlg raha 8 päiwa sehen Mihkel Rattasepale ärra massma peab selle et tema se raha tunistuste perra wõlgu om, Jaan Posti rükkinõudmine om tühjas mõista selle et tunnistust ei olle.
Se mõistus sai kulutadu ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
Peter Iwan  /allkiri/
