Josep Rosenberg kaebas: Temma on 5 aasta eest Karel Rosenbergile üks ellomaja rehi ja kamber ühtekokko 13 sülda pik ja 5 sülda lai ülles teinu, nimmelt keik puu tööuksed ja aknat teinu. Pealesele roowinu temma kattus.
 Keike selle töö eest olla teinu agga üksi 110 rubla, saanu , mis temma ka Karel Rosenbergi  käso järel töömeestele wälja maksnu, et kaup nõnda olnu, et Karel omalt poolt ja omma kuluga andma peab. Peale selle nõuab temma  kaebaja, et poträtschiu olnu, igga päewa eest  1 rubla, se on ülle ültse 76 rubla, kellest agga 18 rubl. 77 kop. kätte saanu, nii et temmal  weel nõuda jäeb 57 rubla 20 kop. /: Sissu folis 15 :/
/: Sissu folis 13:/
Karel Rosenberg andis sellepeale wastust. Temmal olnu kaup, et Josep Rosenberg pidanu selle maja juures keik puutöö tegema omma töömeestega 80 rubla eest.Temma on aga weel peale kauba 20 rubla rohkem maksnu ja ka 40 päewa abiks andnu, nii et temmal kaebajale midagi enam maksa ei olle. Peale selle kaubelnu ka Josep töömehed. Josep Rosenberg andis selepeale wastust, et temma 80 rubla eest üksi agga  seinat ülesraiuda lubasin ja andis tunnistajaks Maddis Hawakiwwi, Mart Oksa ja Kusta Eber.
Mõisteti:Tunistajad terwes kohtopäewas seijekutsu ja see Josep Rosenbergist tehtud puutöö kahest õpinud potratschikutest üllekaeda lasta, et kumbgi kohtokäijatest ühegi tunnistuse läbi selgest teha ei jõua, kuidas hakkatusel kaup olli selejärel  ka üksiktöö wäljanäitama peab, mis ta wäärt on.
 Peakohtomees: Kusta Kokka xxx
 Koht.                   Kusta Nukka xxx
 abikoht.              Tomas Welt  xxx
                                  /: Sissu folis 28:/
