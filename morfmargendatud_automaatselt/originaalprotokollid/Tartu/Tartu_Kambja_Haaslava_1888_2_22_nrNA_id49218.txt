Sel 22 Webruaril 1888.
Kaibas Josep Otsa Wanna Kustest, et 1 Mail 1887 müinu tema oma hobuse üttele wõerale mehele, kos jures  Tenzu  Jaan Roiland olnud ja selle et sel hobuse ostjal nisugutsit taosit ei ollnu pallunu se tema käest taosit ja kui tema wõeral inemisele anda ei olle wõinu sis Jaan Roiland omma wastutamise päle need taosed wõtnu ja neid 3 nädali sehen temale tagasi tua, ent ei olle seni aijani neid taosit tonu selleperrast pallus et Jaan Roiland sunitu saas neid taosit temale ärra andma ehk 10 rubl neide est massma.
Jaan Roiland wastutas, et  Endrik Goldberg, kes taga Wõrro Kakku weski peal ellab selle hobusse om ostnu, tema ei ollewat neide taoste est wastutada lubanu.  Goldberg tullewat Jürripäiwal  Pokka weski päle. Josep Otsa andis tunistajas ülles Jaan Pruks Haslawast ja Jaan Andrei Wanna Kustest ja  Dawit Marga Wastsest Kustest kes jures ollnu kui Jaan Roiland need taosed omma päle wõtnu.
Otsus: et tunistajat ette om kutsuda.
Päkohtumees Johan Link XXX
Kohtumees: Johan Opman XXX
"  Jaan Hansen /allkiri/
4 Aprilil se edesi
