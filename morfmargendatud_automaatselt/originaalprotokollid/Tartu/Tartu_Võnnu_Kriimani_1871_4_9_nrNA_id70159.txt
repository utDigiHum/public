Krimanni koggokonna kohtus 9mal Aprilil 1871.
Kohto lauan olliwa:
Peakohtomees Juhan Mina 
Kohtomees  Karl Põdderson 
Abbi " Hindrik Sulbi 
 Jürri Klaos  tulli ette ja kaebas: et minnul on weski koast 3 wakka rükkid keige kottiga ärra warrastud ja nüid olli jälle teistkorda sealt sammast lae pealt weski koast 49 toopi rükkid ütte kotti pealt ärra warrastud ja leidsin need rükkid  enda weskipois  Jaan Tabbuniga 7 ööd ja päewa wahtinud agga üttel ööl olli ommetegi se kot ilma näggemata Saekoa pealt ärra widud ja siis läksin Kohtumees  Jaan Põddersoniga otsima ja leidsime se tühhi kot kus rükkid sissen olliwad  Hans Wäna Sängist ja ka need rükkid temma kirstust 49 toopi ja 2¼ wakka kaero mis Ta minno rehhest olli warrastanud ja enne sedda  rükkiste wargust kaddus mul jälle sealt weski lae pealt 5 wakka nisso ja 1 wak rükkid ärra ning nõuan keik sedda warrastud wilja kahjo  Hans Wäna käest seperrast et Temma käest ollen üks wak warrastud rükkid kätte sanud.
 Hans Wäen tulli ette ja ütles: se on õige kül et minna ollen se rükki kot Saekoa pealt leidnud agga mitte weski koast warrastanud sest minna läksin weski poisi kässo peale Saekoa pealt linna luid Tammi ette toma ja leidsin siis enda kot mis mul enne sedda ärra kaddus sealt linna luie alt ja peale se teisel wõi kolmandal ööl ollin minna ösi se rükki kotti enda pole ärra winud agga mitte weski koast warrastanud ja se 2¼ wakka karo ollin ma kül rehhe mant tonud ja need on ka  Klaose kaerad agga Rehhepappi lubbaga ollin ma need kaerad rehhe mant sanud.
Rehhepap Jaan Mina tulli ette ja ütles: minna ei olle mitte  Hans Wäenale kaero annud ja se on kül õige et Temma üks kord minno käest kaero küssis, siis ütlesin minna sedda lubba minna sulle mitte ei annan agga kui sa ilma minno näggemata saad siis wõid wõtta ja sedda ei olle minna mitte näinud kui Ta rehhe mant on warrastanud.
Jürri Klaos tulli ette ja ütles: minno Kubjas olli ka talwel üks ösi näinud kui Wäen olli rehhe mant kaero warrastanud ja pallun et Kubjas selle ülle trahwitud saab et Ta mulle põlle üttelnud.
Kubjas Karel Kulber tulli ette ja ütles: minna näggin kül üks kord ösi et Wäen ¼ wakka jaggo rehhe mant kaero warrastas ja ütles mulle, et minna wiin hobbusele agga minna es mõistnud Perremehhele sest middagi üttelda.
Tunnistaja Wolmar Eidemann  tulli sel 16mal Aprilil ette ja ütles: et Hans Wäen kõnneles üks kord Tarto turro peale enda Wellele nenda et minna ei tahhan mitte sedda meest kohtule üllese anda kes need rükkid Weski koast Saekoa peale wiis sest et se wäega hea mees on ja siis koggonist hätta jäeb kui üllese ütlen.
Tunnistaja Andres Luuk tulli ette ja ütles: sedda ma ei tean mitte kes need rükkid olli warrastanud agga Kubjas Karel Kulber käskis üks kord mind ja Karel Aamanni et minge Weski koa pealt warrastage rükkid ja toge minno kätte siis saate napsi rahha ja kül ma need rükkid paigale pannen agga sedda ei olle meie mitte Temma kässo peale teinud.
Karel Kulber tulli ette ja ütles: se ei olle mitte õige et ollen käskinud neid rükkid warrastata ehk enda kätte tua.
Mõistus: 
Koggokonna kohhus pallub allandlikult se nimmetud  Hans Wäen  Keiserliko Silla kohto ülle kullemise alla et Keiserlik Silla kohhus Temast ehk parremad tunnistust sawad leidma et kas Temma need rukkid weski koast lukko taggast on warrastanud.
Kohto lauan olliwa:
Peakohtomees Juhan Mina 
Kohtomees  Juhan Raswa 
Wöörmünder  Mart Kärik 
 
