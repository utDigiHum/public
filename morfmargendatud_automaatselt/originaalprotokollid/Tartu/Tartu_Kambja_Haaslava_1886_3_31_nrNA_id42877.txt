Sel 31 Märzil 1886
Kaibas Sikka tallo perremees Jaan Kroman, et temal 240 Kop Sillaotsa Koolitarre raha tagasi sada om ja pallus et Kohus Jürri Luhat sunis seda raha tagasi massma, selle et Jürri Luha seda raha wasto om wõttnu.
Jürri Luha wastutas, et tema seda ei mälleta kas Jaan Kroman seda raha tagasi om sanu ehk mitte ja pallus ramatust perra kaeda. Se ramat om praegu keiserliko Tarto Kreiskohtu jures.
Otsus: et se assi ni kauges seisma jääb, kui se ramat Kreis kohtust tagasi om sanu.
Päkohtumees: Mihkli Iwan /allkiri/
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
