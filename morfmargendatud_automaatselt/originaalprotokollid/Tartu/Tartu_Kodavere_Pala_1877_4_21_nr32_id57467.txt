Palla Mõisa kärner Mihkel Öbius kaibas, et tema olla Wahhimees Otto Rõika kätte 4 Schinki annud suitsu panna aga kui se neid on  suitsetet ärra tond olnud aga 3 Schinki alles ja tema küsinud kus üks on jänud, Otto wastanud Halliku schingid ollid ka siin suitsus siis sai wist senna widut  sest Halliku kärner on 14 Schinki ärra winud. Agga sealt olla 13 Schinki suitso todut.           /wälja/
    Jaan Sirak Halliku kärner andis protokolli et tema   13 Schinki  Pallale tonud ja ka 13 tükki ärra winud agga mitte rohkem sest 8 tükki on Mõisal olnud ja 5 tükki on tema omad olnud. Muud tema ei tea. /wälja/ 
   Abram Stamm Mõisa puusep tunnistas et tema on keldri ukse lahti teinud ja Palla wahhimees Josep Adow on aidanud Halliku kärneril Schingid kelldrist wälja (wia) kanda ja on nemad keldri ukse ees ülle luggenud ning olnud 14 tükki ree peal ja keldri polle ennam Schinka jänud. M. Öbius andis weel jurde, et Mik Puusepp ja Ottu Mõisa ütlewad ülle pea 14 Schinki on keldri pannud ja kui ennem Schinke keldri ei jänud siis piddi Jaan Sirak 14 tükki winud olema.
    Kohtu otsus: Tullewa kohtupäwal tulewad tunnistajad: Otto Mõisa, Mik Puusep, Josep Adow siis saap Mõistus tehtud.
              See kohtumees Kustaw Kokka xxx
               Kohtumees       Kustaw Nukka xxx
               abi                     Tomas   Welt   xxx
