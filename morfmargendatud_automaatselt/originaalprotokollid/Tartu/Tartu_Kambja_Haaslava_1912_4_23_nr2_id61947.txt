Protokoll № 2.*
Haaslawa wallakohus Haaslawa wallamajas Jurjewi maakonnas Liiwi kubermangus.
Koos on: esimees  Jaan Koort, kohtu liikmed  Kusta Kolt,  Karl Leonhard Klaos.
Tuliwad ette:  Haaslawa walla liige Rodna talu omanik  Johan Jaani p. Grossberg, elab nimetatud talus, ühelt poolt ja  Raadi walla liige  Jakob Kusta p. Laas, elab praegu Raadi wallas Lohkwa külas tõiselt poolt, annawad suusõnal alljärgnewa lepingu kinnitamiseks ette.
§ 1.
 Johan Grossberg annab oma Rodna talu küllest ühe poole (½) wakamaa osa maad, mis siia maale Johan Johani p. Grossbergi käes pidada oli ja kus sellel oma majakene, mis ta nüüd  Kusta Laasile ära müünud, kolmekümne seitsme aasta pääle s.o. 23 Aprillist 1913 a. kuni 23 Aprillini 1950 a. rendile.
§ 2.
Laas maksab selle maa eest iga aasta kaheksa (8) rubla renti iga rendi aasta eest ette aprillikuu lõpul. Esimene maksmine on nimelt 1913 a. Aprilli kuul.
§ 3.
Maa piirid on rentnikule kätte näidatud.
§ 4. 
Rentnikul on õigus selle platsi pääle oma nägemise järele hooneid juurde ehitada ja endiseid parandada.
§ 5.
Rentnikul ei ole õigust ilma maa omaniku lubata seda maad edasi rentida, ega oma hooneid niisama rentida ehk müüa.
