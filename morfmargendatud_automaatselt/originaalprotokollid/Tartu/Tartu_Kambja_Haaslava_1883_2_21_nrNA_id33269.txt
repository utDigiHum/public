Sel 21 Webruaril 1883
Kaebas  Jaan Roiland, et temmal Otsa  Jürri Luha  käest antut palka est 10 Rubl, 2 sülda puude est 7 Rubl sada on. Jürri Luha om 30 wakka Rükki temma palka päle Magatsi aidast wälja wõttnu ja om enegi 180 cop wakka est massnu, ent 3 Rubl om Rükki wakk kaubeltu ollnu ja pallus et kohhus seda wõlg rahha summa 53 Rubl ärra massma sunis.
Jürri Luha  wastutas selle päle et temma palka, puud ja ka need Rükkist sanu om, ent seda om keik ärra massnu ja  Jaan Roiland om raha iks ette wõtnu. Tunistust massmise jures es ütle ollewat.
Mõistetu: 
et  Jürri Luhha se  Jaan Roilandi nõudmine 53 Rubl selle Jaan Roilandile 4 nädali sehen ärra massma peab, selle et Jürri Luha essi tunnistab neud asjad keik sanu ollewat, ent tunistust ei olle et tema neide est tõeste massnu olles.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees Jaan Link  /allkiri/
Kohtumees:
Johan Klaosen XXX
 Peter Iwan /allkiri/
 Juhan  Krosberk /allkiri/
