Sel 15 Junil 1887.
Kaibas Jaan Rosenthal, et tema minewa asta arwata 8 Wakka maad  Johan Kitzingile om rentinu ja nüid sel kewadel om Johan Kitzing essa  Margus Kitzing poja ärra aijanu ja om keik tema põllu põhust tehtut sõnniku tõise perremehele ärra müinu. Kaibaja nõudis seda sõnnikut tagasi, keda 2 wakkamaad täis om sanu, ja tõiselt nõudis Jaan Rosenthal selle maa est, kos Margus Kitzinge ellomaja ja lauda peal seisawa, 15 rubl ega asta pält üiri ehk renti.
 Margus Kitzing  wastutas selle päle, et tema minew asta tõwwu kõrrele sitta ommast käest wisanu ka rükkile sitta pandno ja minewa sügisse ollewat tema poeg tema lauda sittast puhtas wedano ja minewa asta sanu tema ennegi 1½ wakkamaad rükki ent sügise küllinu tema päle 2 wakka rükki maha ja et Jaan Rosenthal enne aego maa ärra om wõtnu. Sitta, mis tema Raudsepa perremehele müinu, ollewat tema wäljast sisse todu põhust tennu. Kui temmal maad ütten ollnu om temma 10 päiwa ega asta Jaan Rosenthalile tennu ent ilma maata tarre asseme est temma 5 päiwa ega asta tennu.
Jaan Rosenthal ütles selle wasto, et Margus Kitzing sõnnikut tõwo kõrrele pandno ei olle, rükkile ollewat Kitzing werekesse päle pandno, ent rohkemba jao peale ollewat Kitzing Soost mulda wedanu.
Margus Kitzing andis Jaan Alman tunistajas ülles, kes nänu et tema rükkile ja tõwe maale minewa asta sitta om pandno.
Otsus: et Jaan Alman ette om kutsuta.
Pääkohtumees: Johan Link XXX
Kohtumees Jürri Kärn XXX
"  Johan Opman XXX
" Jaan Hansen /allkiri/
