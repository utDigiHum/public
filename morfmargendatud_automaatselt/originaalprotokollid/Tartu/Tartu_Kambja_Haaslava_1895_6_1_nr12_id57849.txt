№ 12.
2 Jurjewi Ülema Talurahwa Kohtu ringkonna 14 Wallakohus  Haslawa   wallamajas 1 Junil 1895 a.
Juures oli Eesistuja: Johan Hansen.
 Haslawa walla  liige Logina talu peremees Johan Laar ja Wana Kuuste walla liige  Jaan Albre ja palusid järgmest lepingut prottokolli panda.
 Johan Laar  annab üles, et tema wõlgneb   Jaan Albertile viiskümmend kaks rubla /52 rbl/ ja laseb selle wõla ette  Jaan Albre nime peale ära kirjutada oma waranduse tükid ja nimelt:			Wäärt rbl		1 hobune			10		2 lehma à 15.-			30		2 siga à 4,5 rbl			9		1 lammas			3		Summa			52 rbl./wiiskümmend kaks rubla/		
Jaan Albre tunnistab Johan Laar üles annet õigeks ja wõtab  nimetatud waranduse tükid 52 rbl ette omaks. 
Johhan Laar /allkiri/  Jaan Albert /allkiri/
Lepingu tegijadest on Johan Laar selle wallakohtul tutaw palelikult inimene ja Jaan Albre on Wana Kuuste wallawalitsusest wälja antud tunistuse järele sest 12 Januarist 1894 a. Nr 31 selgeks teinud et tema tõeste Wana Kuuste walla liige Jaan Albre on ja et need lepingu tegijad ise oma käega lepingule on alla kirjotanud tunnistawad sellega 
Kohtu Eesistuja: J. Hansen /allkiri/
Kirjutaja: K. Laar /allkiri/
/Pitser/
