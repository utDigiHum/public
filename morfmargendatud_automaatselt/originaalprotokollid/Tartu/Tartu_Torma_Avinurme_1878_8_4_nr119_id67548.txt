sel. 4 Augustil 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jaan Mänd
kolmas kohtomees Jürri Tomik
Astus ette Kärrasikülla perremees Jürri Kõrre ja pallus koggokonnakohhut sedda Protokolli ülles kirjutada et temma on omma Tallumaadest mis 9 Thaldrid ja 9 Grossi suured pooled omma Wäimehhe Karel Leppale lubbab agga mitte enne kui se temmaga 10 Aastad ühhes on ellanud. Kui agga temma Woimees enne temma juurest ärra lähheb siis ei saa temma need maad.
Karel Lepp tunnistas et temma omma äija Jürri Kõrre lubbamissega rahhul on. ning tedda 10 Aastad teni lubbab.
Moistus
Nende omma leppiminne saab Protokolli ülles kirjutod et se kindlast jaeb.
Josep Laurisson XXX
Jaan Mänd XXX
Jürri Tomik XXX
