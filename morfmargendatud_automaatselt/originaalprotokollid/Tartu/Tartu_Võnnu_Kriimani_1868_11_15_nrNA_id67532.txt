Krimanni koggokonna kohtus 15 Novbr. 1868.
Tarto linna koddanik  Adam Stamm tulli koggukonna kohto ette ja kaebas et kui minna kewwade Roijo Sae weskid ollin ehhitamas ja üttel õhtul kui minna jo omma tö mehhe  Karel Moritsega polluti peal maggamas  ollime siis tulli perrenaene ommast tarrest wälja ja ütles mulle Meister Teie ollete omma rahha Bortmanni seie mahha pillanud siis ütlesin minna olge nenda hea ja wõtke maast üllese enda kätte kül sis hommiko kätte saan, agga kui hommiko üllese tõusin siis küssisin perrenaese  Dorothea Klaose käest kus minno rahha portman on mis õhto ärra wõtsite ja antke nüid kätte siis ütles Dorothea Klaos minna wõtsin õhto se Bortmanni ja pannin Teie kõrwale kus Teie maggasite ja minnul olli seal Bortmanni sees 12 rubla rahha Kammeri mõisast üks rahha Kwitung 45 Rubla suur ja 2 klasi leika Tiamanti mis kokko 16 Rubla maksiwad.
Dorothea Klaos tulli 29 Novbril ette ja ütles: minna ei tea sest asjast middagi agga ni kui unne eest olleks mul wedike meles et üks kord Temma Bortmanni maas olli ja kui minna ka maast sain Temma wõtma siis sain ma õkwa Temma kätte ärra andma, agga mul ei olle mitte suggugi ennam melen ja kui Temmal se Bortmanni ärra olleks jänud siis olleks Tema kohhe minno käest nõudnud agga Ta ei olle mulle selle perrast kunnagi mitte üks sõnna lausunud.
Karel Moritz tulli 21 Febr. ette ja ütles: minna kulin kül kui Dorothea Klaos ütles et Meister on omma rahha Bortmanni mahha pillanud siis ütles: Meister Adam Stamm olge head ja wõtke üllesse agga muud pitkemalt ei tean ma sest middagi kõnnelda.
Mõistus 11 Aprilil 1869.
Koggokonna kohhus mõistis et Dorothea Klaos Adam Stammi Bortmanni wastutamisest koggonist priiks jäeb
1) seperrast et Temmal ühtegi selged tunnistajad ei olle et se Bortmann Dorothea Klaose kätte on jänud ja
2) et Adam Stamm siis kohhe ei olle kaebanud kui Dorothea Klaos se Bortmanni Temmale kätte ei annud.
Peakohtomees Jürri Bragli 
Kohtomees Juhan Mina 
Kohtomees  Ado Lentzius 
