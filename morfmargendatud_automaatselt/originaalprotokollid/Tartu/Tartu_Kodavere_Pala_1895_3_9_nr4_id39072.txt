Kohtu ette tuli Peramets N: 4 poole talu omanik Jüri Tõnise pg Priks, 75 a wana, selle palwega, et tema wiimane tahtmine poja Willemi juures olemisel saaks üleskirjutatud ja nimelt, et pärast tema Jüri Priksi surma saab wanem poeg Willem selle Perametsa N:4 poole talu peremeheks,  ja peab noorema wennale poole liikluwat warandust andma ning 1000 rbl raha. Siiski jäeb mõlemate poegade wahel nende eneste leping maksma, aga kui Willem Alexandrile raha ei maksa, siis kõigest warandusest pool kumbagile saab, aga Willemi nimel saagu Kontraht ümberkirjutud. Kui ka Willem oma naise rahaga tõise koha peaks omale ostma, siis jäeb ka isa poolt see tunnistus, et Willem tema käest selleks mingit abi ei ole saanud. Tütred, kes ju ammugi mehel on, ei saa talu warandusest mingit osa, ei ka talu enese wäärtusest midagi, waid see jäeb poegade omaks mõlematele ühe suuruses osas, kus juures weel noorema pojale ühisest warandusest pulmad tehakse.
                                                                                         Jürri Priks
Kohtumäärus: Liiwi maa Talur.Sead. § 1013 põhjusel Jüri Priks suusõnalist testamenti ustawaks tunnistada.
        Kohtueesistuja: Abram Saar
        Kohtumõistjad: Peeter Holst
                                    Juhan Märtinson
         Candidat           Jakob Reinhold
         Kirjutaja KSepp.
              
