Haslawa  Kogukonna Kohus sel 23 Mail 1883
Man ollid:
Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
Peter Iwan 
Johan Grossberg
Kaibas  Lieso Kuck omma mehe Ain Kucke man ollen, et tema wend Jaan Kahma teda ilma süita pesson om. Jaan Kahma om enne russikaga sälga lönu ni et temma maha kukkunu ja sis maan om Jaan Kahma 4 ehk 5 kord jallaga temale ristlude päle lönu. Tema palge ollnu paistetadu sest tema kukunu palge päle maha ja ristluud ollnu ka paistetu ja perran seda nädal otsa ei olle tema ristluist kõndi sanu ja ollewat ristlud praegu haige. Palged om tema Kohtumees Peter Iwanile näidanu. Lieso Kuck nõudis 150 Rub neide lögide est. Tunistajas andis ülles: oma sõssar Ann Kahma, kes lugemada om ja Karl Roots.
Jaan Kahma wastutas selle päle et tema Lieso Kucke lönu ei olle, tema om sellesama käest ütte tokki ärra wõtnu sis om Lieso Kuck teda karwust kisknu ja om temale jallaga Ellakotuse päle lönu. Lieso Kuck ei olle tema naisel Ellajat wälja aijada lasknu sis om se tülli tullnu.
Karl Roots tunistas et tema lömist nännu ei olle seda om nännu kui Jaan Kahma ütte tokki Lieso Kucke käest ärra wõtnu.
Tunistaja Lies Waher tunistas 11 Julil s.a et tema kakelust nännu ei olle.Marri Aese tunistas et Jaan Kahma naene om mehe kässu päle lehmat laudast wälja lasknu sis om Lies Kuck sinna tulnu, lönu temale ütte kepiga käe päle.
Lies Sakna tunistas et Lieso Kuck om Jaan Kahmat kõiki möda wandunu sis om Jaan Kahma käega Lieso Kuckele lönu alla tõuganu sis om Lieso Kuck ärra länu kändnu weel ümbre ja üttelnu: pikne sagu sinno põlletama.
Ann Kahma tunistas sel 25 Julil et tema nännu et Jaan Kahma om Lies Kuckele tagast (ei loe välja) lönu ni et se maha kukunu ja sis om Jaan Kahma weel jallaga lönu ja Liis Kuck om sis karwustanu Jaani.
25 Julil 1883 sai Mõistetu: et Lies Kuck 2 Rubl ja Jaan Kahma 2 Rubl trahwi waeste ladiko heas 8 päiwa sehen massma peawa, selle et nemma sure südame ja omma õigusse perrast tülli om tõstanu. 
Se mõistus sai 8 Augustil kulutado ja õpetus leht wälja antu.
Päkohtumees Jaan Link /allkiri/
Kohtumees: Johan Klaosen XXX
Peter Iwan /allkiri/
Johan Krosbärk /allkiri/
