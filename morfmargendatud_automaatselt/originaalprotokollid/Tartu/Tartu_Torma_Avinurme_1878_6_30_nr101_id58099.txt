sel. 30al Junil 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomes Jürri Tomik
Renthärra Kijmenthal kaebasid et ühhel pääwal kui nemmad on koddust wälja sõinud. ning paar versta Mõisast ärra olnud on hobboso room katki lähnud ja nemmad kesk teed häddas olnud. on nemmad Möldri poisi Jürri Masikod appi kutsunud kes teisel pool jõgge heina ajanud. se agga ei olle nende kutsumist miskist pannud. ja nendawiisi Mõisa politsei seisust naeruks pannud. ja palluwad nemmad sellepärrast koggukonnakohhut selle ülle moistust tehha et kas üks innimenne kes Mõisa piiris ellab Mõisa politsei käsku peab kuulma ja pealegi kui üks hädda assi on.
Jürri Masik räkis et Härra on tedda küll appi kutsunud agga et temma teine pool jogge olnud ja jõggi wägga süggaw ei olle temma läbbi joe minna sanud. ja et Härra tedda ümber Wälja silla kaudu minna käskinud. on se te wägga kauge peale wersta olnud. ning temma ka kartnud et perremees temmaga taplema hakkab. et temma aega widab.
Moistus
Et Jürri Masik nenda jämme ja mõistmatta olnud. ning mitte Härrale appi ei lähnud kui nendel hädda olli ja sellega Moisa politseid ja temma woimust teutanud siis maksab temma selle eest 2 Rubla wallalaeka trahwi.  
Josep Laurisson XXX
Andres Kiik XXX
Jürri Tomik XXX
