Haslawa Kogukonna Kohus sel 5 Detsembril 1883
Man ollid:
Päkohtumees: Jaan Link
Kohtumees: 
Johan Klaosen
Johan Grossberg 
Selle otsuse päle sest 7 Nowembrist s.a. olli  Karl Lätti kutsumisse päle seija tullnu ja tunistas et tema sest asjast midagi ei tea.
Mõistetu: et Lies Werno kaibus tühjas mõista om, selle et tunistust ei olle.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees Jaan Link /allkiri/
Kohtomees:
Johan Klaosen XXX
Johan Krosbärk /allkiri/
