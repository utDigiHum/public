Sel 11 Webruaril 1885.
Tulli ette  Jürri Türki laste Wölmünder Jaan Klaosen ja andis ülles, et selle Jürri Türki perranduse ülles kirjutamise jures ei olle seal tallun ollnu willi ülles kirjutado sanu ja nimelt:
13 wakka Kaero
10 " Keswi
10 " Rükki
2  "  Rükki jahu
40 " Kartoflid
1  "  Linasemnit
1½ " Tatriku
ja pallus et se willi Jürri Türki perranduse hulka saks arwatu. Jaan Kroman om se wilja omma kätte sanu. Nisamma on Jaan Kroman räise wilja 11 mõtu 7 karnist Rüki 14 mõtu 2 karnist Keswi ja 14 mõtu 2 karnist Kaero Magatsist wälja sanu, mis ka Jürri Türki perranduse hulka tulleb.
Jaan Kroman, Anna Türki mees, andis ka ülles, et temma se willi kül majapidamisses Jürri Türki warrandussest  sanu om ja lubas seda wilja ka sis ärra anda kui temma selle tallo lastele ärra annab, nisamma om ka räisse wilja wälja sanu ja annab seda ka sis ärra.
Otsus: Seda Protokolli ülles panda.
Päkohtumees: J. Link /allkiri/
Kohtumees:
J. Krosbärk /allkiri/
"  Jaan Pruks /allkiri/
