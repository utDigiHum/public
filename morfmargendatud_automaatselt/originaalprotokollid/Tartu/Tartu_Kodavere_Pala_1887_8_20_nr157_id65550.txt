Protokolli peale 25 Juunist s.a. N:124 sai ette kutsutud Karl Uuk Hallikult ja andis üles et tema mitte seda ei tea kas Mats Kronki lehm wiimasel ajal terwe wõi wigane oli sest see lehm ei käi tema karjas. Jakob Uuk Hallikult tunnistas niisama kui Karl Uuk.
 Mats Kronki naine Lena Kronk ja wastas küsimise peale, et tema kunagi seda ütelnud ei ole et Widrik Ertis nende lehma ristluiest rikunud on.
Otsus Widrik Ertis Ranna wallast kes täna wäljajäänud i ettetarwitada.
           Peakohtumees J. Sarwik
           Kohtumees        J. Stamm.
           Kohtumees        M. Piiri.
           Kirjutaja G Palm.    
