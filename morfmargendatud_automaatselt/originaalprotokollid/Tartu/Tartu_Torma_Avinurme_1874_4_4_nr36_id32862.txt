Awwinorme Mets kogokona kohtus sel 4 Aprillil 1874
koos ollid.
pea kohtomees Josep Kask
teine kohtomees Juhhan Koppel
kolmas kohtomees Maddis Sildnik.
Karel Pilleson astus ette ja pallus kogokona kohhut temma ja ta wenna Maddis Pillesoni wahhel wahhet tehha sest et temma aastal 1868 on Negrutist antud ja tema assemelle assemik palgatud sanud, siis on selle mehe ette 273 Rubla arra kulunud, mis tema aga keik ära teninud ja tema teada keik tassa peaks olema, nüüd aga tema käest weel seda raha nõutakse, ja olnud temma rehnung nenda
omma raha olnud temal 31 Rubla
walla poolt kingitud - 10 -
wennemaal teninud ja maksnud Andres Pillesonile 50 -
mehhele raha saatnud. 58 -
ja omma wenda Maddist on tema 4 aastat teninud kelle eest se pidanud maksma 124 -
Summa 273 Rubla
ning peab tema sedda wisi selle rahhaga tassa ollema.
Maddis Pilleson räkis et tema wend Karel on aga tema jures 3 Aastat teninud ning rehendab tema 25 Rubla aastas palka summa 75 Rubla, ning et tema rohkem maksa ei taha.
Juhhan Pilleson nende issa tunistas et tema norem poeg Karel on Maddist teninud 4 Aastat agga neljas Aasta kül mitte täieste, sest et Maddis omma wenda süggise ärra ajanud.
Nende wend Andres Pilleson tunistas et temma noremb wend Karel on Maddist liggi nelli aastat teninud, ja on Maddis Karlid neljandamal aastal enne aasta lõppetust ärra ajanud.
nende wend Jakob Pilleson tunnistas nenda sammoti.
Karel Pilleson räkis et temma wend Maddis on temale kindlat palka 30 Rubla lubanud, aga ka üttelnud, et kui teised rohkemb lubbawad, siis annan minna ka rohkem.
Moistus
Selle järrel et Karel Ja Maddis Pillesonide issa Juhhan ja ka nendasamoti nende wennad Andres ja Jakob tunistawad, et Karel on liggi nelli Aastat omma wenda Maddist teninud, ja Maddis isse omma wenda wasto talwe ärra aianud, siis moistab kogokonna kohus et Maddis Pilleson peab oma wena Karlile 4 täie Aasta eest palka andma igga aasta eest 30 Rubla summa 120 Rubla, ja maksab Karel Pilleson se 4 Rubla ommast käest nende wõlla andjatele ärra.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
