Haslawa Kogukonna Kohus sel 30 Julil 1884.
Man ollid: Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
Johan Grossberg
Selle otsuse päle sest 23 Julist s.a. Kristjan Toom kaibuse asjan wasta Johan Zobel olli kutsumise päle tullnu Johan Link ja tunnistas, et tema seda ei mälleta et Johan Zobel 5 Rubl Mihkel Klaose kätte Kristjan Toome päraha massnu olles.
Johan Luha tunistas et tema sest midagi ei mälleta.
Mõistetu: et Johan Zobel se 7 Rubl 50 Cop Kristjan Toomi päraha wõlla kistutuses Haslawa wallawalitsusse kätte kuni 29 Septembril 1884 ärra massma peab, selle et selle ülle tunistust ei olle, et tema 5 Rubl Mihkel Klaose kätte massnu olles.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
J. Krosbärk /allkiri/
Sel 7 Webruar 1885 massis Johan Zobel 7 Rubl 50 Kop Wallawalitsuse wälja.
