Haslawa Kogukonna Kohus sel 18 Nowembril 1885
Man ollit päkohtumees Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
Kaibas Jaan Roiland, et kui tema Jaan Kalpuse ellajat kinni aijada om tahtnu sis om karjapois Jaag Kalpus teda kiwitega pillnu ja ähwartanu et kui sinna minuga kirmi koko aijat kül sinna sis teenit. Ja kui tema üts kõrd kodust lännu sis om Jaag Kalpus rehe mant kiwiga temale wissanu se kiwwi putunu hobuse kül ja seda modu wõib se pois teda koolus lüja. Tunistus ei wõi tema allati ütten sedada ja pallus seda poisi kelda ja protokolli wõtta kui sis edespidi midagi juhub wastutab pois.
Jaag Kalpus wastutas omma welle Jaan Kalpuse man ollen selle kaibuse päle et temma Jaan Roilandile kiwidega wissanu ei olle ja es taha ka konnagi seda teha.
Otsus: et se Jaag Kalpus selleperrast nomita om et tõine kõrd mitte enamb se ette ei tohi tulla.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
"  Peter Iwan
