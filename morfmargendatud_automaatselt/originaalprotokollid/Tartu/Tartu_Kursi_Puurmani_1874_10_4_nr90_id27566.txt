Sesama kord.
Mart Narwa kaibab, et Kadri Jallakas olla räkinud, et tema olla lubbanud Kadri sauna põllema panna, ja olla ka weel rääginud, et Mardi poeg olla lehma selgas käinud ning tema olla oma me... sedda näinud ning pallub, et Kadri sedda selgeks teeks.
Kadri Jallakas wastas, tema olla aga rääkinud, et tema ei woi senna sauna ellama minna sest Mart on lubbanud sidda arra kaudada.
Ütlep ka, et se pois on küll ... õhwa peal olnud ning tema ei tea mis pärrast tema sedda on rääkinud aga ütlep pois polle mitte kobbematta ... teinud.
Kohtuotsus.
Et Kadri Jallakas nisuggust teutust on rääkinud maksab 2 Rubl wastelaekje ning kohtu jurest ärra kargamisse eest istub 24 tuni kinni.
....
Hans Perre XXX
Tönnu Bauer XXX
Jürri Luhha XXX
