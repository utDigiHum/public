Karl Kõiwu kaebduses Widrik Kõressaare wastu löömise pärast,protokoll N131, olivad kutsumise pääle Karl Kõiw ja Widrik Kõrressaar tulnud. Neile saiwad tunnistused ette loetud.
Mõistus: Avalik on, et Karl Kõiw'u ja Widrik Kõressaare wahel on kaklemist ja tüli olnud, aga ei saa mitte awalikuks, kumb nendest selle tüli juures enam süüdi on. Sellepärast peawad Karl Kõiw ja Widrik Kõrressaar kumbgi 1 rubla - summa 2 rubla trahwi maksma walla waeste kassasse 14 päewa sess tüli tegemise pärast.
Mõistus kuulutati § 773 seletamisega.
Karl Kõiw ei olnud rahul ja sai õpetuse leht suuremat kohut käia.
