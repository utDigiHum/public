Krimanni koggokonna kohtus 12mal Märtsil 1871.
Kultna karjamõisa Rentnik  Heindrich Freyfeldt tulli ette ja kaebas et minna sain 1868mal aastal  Jürri Klaose käest 5 Puda ristik haina semet agga et se seme mitte ei kassunud nõuan selle 20 wakkama külwatud ristik haina eest igga wakkama pealt 2 koormad ristik haino se on minna ollin külwanud 5 Puda seemnega 20 wakkamaa kust minna koggonist ilma hainata jäin.
 Jürri Klaose tulli ette ja ütles:  Freyfeldt  on se ristik haina seme minno käest Aprilli kuu sissen sanud ja ka illusti wasto wõtnud agga sedda ei tea minna mitte kui paljo ta olli wakka ma peale külwanud ehk mil ajal olli Ta se seme külwanud.
 H. Freyfeldt tulli ette ja ütles: minna olli se seme keskpaiga Mai kuu sissen külwanud ja ollen ka 10 naela wakka ma peale külwanud.
Mõistus: 
Koggokonna kohhus mõistis: et  Freyfeldtil ei olle õigus sedda ristik haina seemne kahjo  Klaose käest nõuda seperrast et Ta siis õkwa ei olle mitte kaebanud ja
2) Et Ta ei olle mitte kohtul lasknud enda ristik haina ülle kaeda kuis Ta sel korral wälja näitas.
Kohto lauan olliwa:
Peakohtomees Juhan Mina XXX
Kohtomees Juhan Raswa 
"  Karel Põdderson 
Freyfeldt on Protocolli wälja wõtnud 19mal Märtsil 1871.
Kirjutaja assemel: M. Baumann /allkiri/ 
