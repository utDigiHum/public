Tuli ette Karel Sallo ja kaebas et Aleksander Rosenberg olnu minewa suwel ühte kirja Tartu postkontoris  tuua, Karel Sallo nime peal kelle sees kolm rubla olnud  wäljawõtnud ja seda kirja ja
raha tänase pääwani mitte kätte andnud. Nõudis seda kirja ja raha taga.
Tuli ette Alexander Rosenberg ja ütles küsimise pääle et minewa suwel tulnud üks niisugune kirja tunnistus Palla kontori ja wõtnud tema  teda wallawanema lubaga Tartu postkontorist wälja ja andnud teda Jürri Alliku kätte sellepärast et tema arwanud et tema see Karel Sallo on sest Jürri Allik oli enne seda kirja taganõudnud kui se kiri weel Tartust ära toodud sai.
Otsus tulewa kohtu pääwaks  mõistus teha.
See otsus sai kuulutud.
                              Pääkohtumees O.Kangro
                                     Kohtumees J. Soiewa
                                     Kohtumees O. Mõisa
                             Kirjutaja allkiri.  
