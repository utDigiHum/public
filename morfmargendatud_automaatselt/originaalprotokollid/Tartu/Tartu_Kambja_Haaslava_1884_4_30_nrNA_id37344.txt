Haslawa Kogukonna Kohus sel 30 Aprilil 1884.
Man ollid: Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
" Peter Iwan
" Johan Grossberg
Kaibas  Jürri Einman  Reolast, et Margus Kitsing 7 lamba nahka temale pessnu om, ent nenda alwaste et will nahku küllen lahti om.
Jürri Einman  nõudis 1 Rubl 50 kop ega naha est ja annab need nahad Margus Kitsingile tagasi.
Kohtumehe  Johan Klaosen, Peter Iwan ja Johan Grossberg kaesiwa need nahad ülle ja leidsid, et 3 nahka head ollid, mis Einman ka tagasi wõttis, ent 4 nahha willo om kül koguni lahti.
Margus Kitsing olli tännases päiwas kolmas kõrd selle kohtu ette sellen asjan kutsutu ja nimelt temale üttelda lastu, et kui temma tänna kohtu ette ei tulle saab kohus ärra mõistma ent siski es olle temma tänna kohtu ette tullon ei ka henast wabandata laskon. 
Mõistetu: et Margus Kitsing need 4 halwaste pessetut lamba nahka tagasi wõtma ja 4 Rubl neide est Jürri Einmanile kahjutassumist 2 nädali sehen massma peab. 
Se mõistus sai Jürri Einmanile kulutado ja õpetus leht wälja ant.
Sel 14 Mail s.a.sai se mõistus Margus Kitsingile kulutedo ja õpetus leht wälja anto.
Päkohtumees J. Link /allkiri/
Kohtumees  Johan Klaosen XXX
Peter Iwan /allkiri/
J. Krosbärk /allkiri/
Kirjutaja: J. Weber /allkiri/
Sel 9 Julil 1884 sai Jürri Einman se 4 Rubl wälja massetu.
Ollen se raha kätte sanu: Jürri Einman /allkiri/
