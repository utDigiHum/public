                 Raha rendi kontraht.
Hawakiwi talu N: 25 mis on Tartu maakonnas Kodawere Kihelkonnas, Palla mõisa all.
Willem Peramets, nii kui üks selle koha omanik annab selle tallo Palla mehe Karel Hawakiwwi, Jaani poja kätte rendi pääle, nagu see on Liiwimaa talurahwa seaduse § 196 ära täita allnimetud paragrahwide järel nagu see on kokku räägitud ja seia ära kirjutud ja Palla walla kohtu poolt kinnitatud saanud.
                                                                                                        § 1.
Willem Perramets annab rendi pääle Hawakiwwi talu koha põllud ja heinamaad mis N: 25 all on kuue aasta pääle, se on 20. Aprillist 1892 aastast kuni 23. Aprillini 1898 aastani, Palla walla alla üles-
kirjutud Karel Hawakiwi kätte, kellele seesinane Hawakiwwi talu maamõõtja kaarti järel kõik üles haritud sõnniku põllumaa ja heinamaa on kätte antud saanud.
                                                                                                         § 2.
Kui palju hoonid selle talu koha päralt on ja mis moodi need nüüd on ja ka kuda selle koha seisus, sellest annab selgemad teadust, see lisakiri, mis selle kontrahti juures seisab ning mis mõlemilt poolt Palla walla kohtu ees on kinnitud saanud.
                                                                                                          § 3.
Raha renti selle Hawakiwwi talu koha üles haritud  põllu ja heinamaa eest maksab rentnik aastas üle keige nelisada (400) rublad hõbedad ja nimelt järelseiswa terminide sees 1 Aprillist 1892 a kakssada (200) rubla ja 1. Oktobril 1892 aastal kakssada (200) rubla iga kord poole aasta rent ette ära maksa.
                                                                                                           § 4.
Rentniku kohus on seda tema kätte  rendi pääle antud Hawakiwwi talu kohta iga pidi sündsa põllu harimise kombe ja seaduse järel pidada ja kui rendi aasta mööda, siis peab tema seda talukohta all nimetud wiisil pärisomanikule tagasi andma nenda et
a) Hawakiwwi N: 25 talu koha hooned ja nimelt nende kattused olgu terwed ja kõwitud:
b) rukki wäli olgu sõnnikuga heaste wäetatud, kaheksateistkümmend 18 wakkamaad ja heaste haritud ja õigel ajal  hea täieline seeme sinna peäle külitud iga wakkamaa pääle seemet 1½ wakka, ja keige hiljem kuni kahekümne Augusti kuu pääwani maha tehtud olema;
d.) Kõik aijad olgu hea korra pääl ja rentniku kulul terwed hoitud;
e) heinamaad olgu iga aasta juurtega puhastud hagudest, aga puid mis jõe kallastel kaswawad, ei ka heinamaa pealt suuremaid puid ei tohi rentnik raidu, wait ainult noort wõsu wälja puhastada, et heinamaa wõsu alla ei kaswa;
g) Kõik kasu, ilu ja wilja puud, nenda sama ka marja põesad mis selle koha peal on, peab rentnik terwed ja rikkumata hoidma ja kui mõni puu wai põõsas ära häwitud saab maksab rentnik omanikule kahju tasumist.
h.) Kõik wiimane sõnnik, mis loomade toidust ja põllust on saanud, nõnda sama ka üle jäänud põhk, õled ja heinad peab ilma tasumata koha pääle jääma kui rentnik ära lähab.
i.) talu piirid olgu piiriristikiwid ja kuppitsad ilusaste alles hoitud ja wälja sees kraawid puhastetud.
k.) tööde poolest eesseiswa töö aasta peale peab olema kõik rukki kõrs üheksa wakkamaad ja ristikheina wäli üheksa wakkamaad üles künnetud sügise kewwadest  koha käest andmise aegast.
                                                                                                       § 5.
Rentnik peab iga kewade üheksa wakkamaa rukki masse sisse ristikheina seemne omast käest külwama, ja igaüks. Üheksa wakkamaad ristikheina wäli seisab kaks aastad niido all nenda iga aasta kaheksatõist kümmend wakkamaad ristikheina pealt tehakse.
                                                                                                        § 6.
Kui rentnik heinu ostab, ei saa tema minu käest kahjutasumist ja ei wõi ka heina wõlga Hawakiwwi talu heintega maksta  kui peaks rentnik heinu wõlgu wõtma.
                                                                                                         § 7.
  Rentnik peab tulekassa raha maksma walla  hulka, kui juhtub tulekahju nende talu hoonetele, et siis jäeb walla poolt abi ehitusele saab.
                                                                                                          § 8.
Rentnik parandab ja ehitab selle talu hooned ja ehituse puu saab minu käest, mis rentnik oma kuluga peab raiduma ja wedama, aga  kiwi ja lubi peab rentnik ostma oma kuluga, mis maja parandusele tarwis lähab.
                                                                                                           § 9.
Kui rentnik on kulutanud ennast majade parandamise  või ehituse pääle, ei saa minu käest selle eest tasumist wait peab iga kewadel kohast wälja minema ilma maksuta.
                                                                                                            § 10.
Rentnik peab seda  ülesharitud põllumaad üheksas  wäljas et siis iga aasta kaks wälja rukki alla käib, ja peab rentnik keik wiimane sõnniku rukki maa pääle panema ja ei tohi suwe wilja põllu pääle sõnnikud  panna ja linu mitte üle kolme wakkamaa  külwada.
                                                                                                             § 11.
Rentnik teeb heina jõe äärest ilma wee maha laskmata ja ei wõi minu käest kahjutasumist nõuda.
                                                                                                              § 12.
Koha omanikul on see õigus, et tema igal ajal ja nii mittu korda kui see tema meele järele on seda talu kohta ja selle tallitust wõib kas ise läbi waadata, wai omal wolimehel laske seda teha.
                                                                                                               § 13.
Rentnikul ei ole luba wõeraid karjasid selle rentitud krundi  pääle laske käia, peab seda järele waatama ja oma karja ei ka hobusid enne heina tegemist heinamaade peale mitte, ja mäda heinamaale ei millagi, et heinamaa ei saaks ära rikkutud.
                                                                                                                § 14.
Liiwimaa talurahwa seadusest 13. Nowembrist 1860 järele ja kohalise wallawalitsuse iga aastase ära jautamise ning seadmise järele on rentniku kohus seda jagu, Kroonu ja kiriku ning wallamaksusid ja muid  sundimeisi ja käskusid mis selle Hawakiwwi N: 25 tallu ja N: 26 Hawakiwwi weski talu koha pääle tulewad täieste maksa ja ära täita, see on nimelt 42 taadrid 56 krossi maa eest, keik teede tegemised ja küüdis käimised ja maksude ära wiimised täita, ning Ritterschafti maksud ära maksta rentnikul oma kuluga täieste.
                                                                                                                  § 15.
Rentnik saab Hawakiwwi weski põllumaad Palla  poole üle  sawiku silla paremat kätt tee äärest wiis wälja see on 11. wakkamaad  süda põldu ja Raja talu N: 24 all maad kakskümmend  kaheksa 28 wakkamaad see on põllu ja heinamaa wasta Haawakiwwi N:24 talu piiri kõik oma kätte ja  weski järwe ärest üks wakkamaa heinamaad jääb minu kätte, mis  Haawakiwwi N: 25 päralt on ja nenda 
on siis keik rent eesseiswa rendi sisse ära arwatud, renti maksab 400 rubla. 
                                                                                                                   § 16.
Rentnik saab oma kätte pruuki  majasid
1) Üks elumaja kolme kambriga rehi reiaalune, kõik ühe õle kattukse all kiwi wundamendi peal kahe ahju ja korstnaga, raud hinged uksedel, ja lukk kambri ukse küljes ja toppelt aknad kambridel.
2) Hait wana, hõle kattus peal, lukk ees ja raud hinged uksel.
3) Kaks aita ühe õle kattukse all, kaks ukst ja kaks lukku, raud hinged.
4.) Kaks lauta ühe õle kattukse all kiwi wundamendi peal raud hinged uksedel.
5.) Laut, õle kattus, õlestik,wankri hoone, keik ühe kattukse all, raud hinged uksedel.
6.) Kiwi kelder, õle kattus, raud hinged ukste külles, ja krambid
7.) Saun, sauna esik ühe õle kattukse all, raud hinged uksedel.
8.) Köök wana ja raud hinged uksel ja haak ukse küljes.
9.) Kaew uus ja raud pulk ülewel lingi samba sees ja rautatud pang hingi kongu otsas.
10. Üks wäike ait laastu kattukse all ja raud hinged uksel õunapuu aias ja üks wana laut ja warjualune, õle kattus peal.
11. Kolm heina küüni, keigil õle kattuksed heinamaade peal.
     Keik neid hooneid peab rentnik parandama. 
                                                                                                     § 17.
Rentnikul ei ole luba ilma minu teadmata ega tahtmata maad ühele teisele wälja rentida.
                                                                                                     § 18.
Rentnik K. Hawakiwwi on pannud kautsjoniks selle koha eest kakssadda 200 rubla, mis tema kätte saab ilma intressi maksuta, kui koha kontrahi järel käest ära annab, mis puudub, saab kautsjonist maha arwatud ja weel kautsjooniks seisawad wiistõisskümmend sarw karja elajad ja wiis hobust, mis rentnik oman käen hoiab, hea toidu peäl ja need wastawad ka selle koha  eest kui raha ei peaks ulatuma ja rentnik peab hoidma need kautsjoni elajad täieste sõnniku tegemise tarwis. 
                  Nenda leppitud: Hawakiwwil Sel 23. Aprillil 1892 aastal, koha omanik Willem Perramets.
                                                                                                                    Koha rentnik Karel     Hawakiwwi.
Et eesseisew kontraht XII Palla wallakohtu poolt on ette loetud ja rendiwõtja ennast selle tingimustega on rahul üttelnud olewad ja rendiandja kui ka rendiwõtja oma käega kontrahti alla   on nimmed pandnud, saab XII Palla wallakohtu poolt selle läbi tunnistatud /:§/ tõise rea pääl on ,, kuue`` kohtu juures kirjutud.)
                                  Pallal 11. Juunil 1892.
                                         Kohtu eesistuja Abram Saar.
                                         Kirjutaja Seidenbach.
Märkus: wiimase lõigu teksti kõrwal N: 107.
Protokolli alguses märkus Ärakiri.
                                                                                                 
                                                                                                    
                                  
