Awwinorme Koggokonna kohtus sel. 26mal April 1868
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Jaan Saar.
Astus ette Mihkel Inno ja andi tunnistust et temma olnud kõrtsis kui Aleksander Plauet peaga Andres Reisenpukkile möda rindo ja silmi peksnud. nenda et Andres keik werrine olnud, ja üttelnud, egga Lojus ei tea mis ta teeb.
Astus ette Maddis Mick ja räkis et temma küll sedda näinud. kui Aleksander Plauet essiti peaga Andres Reisenpukkile möda rindu peksnud., pärrast tulnud agga temma ette kõrtsi peale ja olnud rinnuli lettilaua peal. tulnud siis Andres Reisenpuk ja wõtnud Aleksander Plaueti karwust kinni, ja tõmbanud tedda põrrandalle mahha, ja et nemmad siis teine teist kõwwaste peksnud.
Moistus
Et tunnismeiste läbbi selgest nähha on et Aleksander Plauet sedda tülli allustanud ning Andres Reisenpukki peksnud, maksab temma selle pärrast 6 Robla wallalaeka trahwi ja Andres Reisenpuk maksab selle pärrast 3 Rubla trahwi, et temma Aleksander Plauetid karwopiddi letti pealt mahha tõmbanud
Mis tunnistawad
Mart Jaggo. XXX
Mart Liis XXX
Jaan Saar. XXX
