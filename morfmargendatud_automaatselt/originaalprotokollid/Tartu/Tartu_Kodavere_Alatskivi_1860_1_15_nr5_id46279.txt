Kewade 1858. aastal kaibas siin Kogokonna kohtu ees tüdruk Krõõt Mesipuu, et Madis Räni temale lapse soetanud, mis mees ka ei salanud. Et Madis Räni alles noor pois oli, kel midagi ei olnud ja kel weel oma wana ema toita, ei saanud temale sekõrd muud trahwi mõistetud kui 30 witsa lööki.
Nüüd aga tuleb tüdruk ja tahab poisi käest abi nõuda lapse toiduks; poisi waesuse pärast aga ei wõinud muud mõista, kui, et tema tüdrukule 1 wak ruki maksab.
Krõõt Mesipuu ei taha selle mõistmisega raha olla, ja wõib suuremas kohtus õigust otsida.
