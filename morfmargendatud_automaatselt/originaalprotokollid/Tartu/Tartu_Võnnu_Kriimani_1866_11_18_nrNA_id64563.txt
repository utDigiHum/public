Krimanni  Koggokonna kohtus 18 Nowbr 1866.
 Mihkel Läett tulli 18 Nowembril koggokonna kohto ette ja kaebas, et pühhapäew ösi 6 Novembril on temma aedast ärra warrastud 1 Reiwas ja 10 hamet ja arwan et  Karel Aman ja Jaan Mällo on warrastanuwad sest nemmad on kattekeisi sel õhtul Roijo kõrtsi man salla jutto ajanuwad ning pärrast sealt mõllemad ärra kaddunuwad.
Siis sai  Jaan Mällo 25 Novembril ette kutsutud ja ta ütles: minna ollin kül sel õhtul Roijo kõrtsi man ja tullin sealt üksinda küllase ning läksin Arraka tallo küini ja heitsin senna maggama ja ütles  Karel Aman jäi minnust kõrtsi juurde kui ma sealt ärra tullin ja Mihkel Läetti hamme wargusest ei tean ma middagi üttelda.
Selsammal päewal sai Karel Aman ette kutsutud ja ta tunnistas et temma sel õhtul polle mitte Roijo kõrtsi manno sanud egga ka Jaan Mällud silmaga sel õhtul näinud ja ütles: et temma on Roijole (koddo) läinud ja weski mant otse kohhe ilma Kõrtsi manno minnemata Rehte maggama läinud ja ütles: et ta mitte Pekste külla kaudo polle koddo läinud.
Tunnistajad Jaan Ossep ja Jürri Steinberg said 6 Decembril kutsutud ja nad tunnistasiwad mõllemad üttest suust et Karel Aman olli sel õhtul Roijo kõrtsi man ja ostis Jaan Mällole ½ kortenid wina ja kaddusiwad peale se mõllemad sealt kõrtsi mant ärra.
Juhan Klaos tulli 12 Decembril ette ja tunnistas: et sel õhtul 6mal Novembril olli Karel Aman ütte teise mehhega Päkste tännawal Mihkel Laetti tarre kohhal agga teisest mehhest ei sanud ma mitte arwo kes ta olli sest ta tõmbas enda pea kassuka sisse agga Karel Amanni tundsin ma selgeste ärra.
Karel Aman sai ette kutsutud 12 Decembril ja ta salgas ärra et tema ei olle mitte sel õhtul 6mal Novembril Päkste külla tännawale sanud.
Mõistus:
12 Decembril Koggokonna Kohhus mõistis  Karel Amani wargaks sepärrast et temma
1) wallet olli tunnistanud et temma mitte sel õhtul polle Roijo kõrtsi man käinud egga Jaan Mällud suggugi näinud,
2) Juhan Klaose tunnistuse peale et temma sel ösil on ütte teise mehhega Mihkel Lätti wärrawas saisnud kui need reiwad warrastud sanud 
maksab Karel Aman Mihkel Laettile nende reiwaste eest 10 Rubla, 2 näddali aja sissen peab se rahha mastud ollema.
Man olliwa:
Peakohtumees Josep Kerge 
Abbikohtumees   Peter Püir
"  "  Jaan Müirsep
