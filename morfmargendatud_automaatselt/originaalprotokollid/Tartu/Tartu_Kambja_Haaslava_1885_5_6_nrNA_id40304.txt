Haslawa Kogukonna Kohus sel 6 Mail 1885
Man ollid: päkohtumees Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
Tännasel päiwal said surno Simo Tallo perrisostja Johan Hanseni allaealiste laste Wölmündres nimetado Peter Hansen Haslawast ja Jaan Lüde Sarrakusest ja surno Zirma Tallo perrisostja Lillo Meijeri allaealiste laste Wölmündres said Kirbo Tallo perrisostja Jaan Rosenthal ja Sillaotsa tallo rentnik Johan Raswa nimitedo, neile sai se kulutado, kes ka selle ameti wastu wõtta lubasiwa.
Otsus: seda protokolli ülle panda ja kinnituses kirjaliko 4ta Tarto Kihelkonna kohtule ette panda.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
 Peter Iwan /allkiri/
Need Wölmündret om Kihelkonna Kohtu polt kinnitedo sel 27 Mail 1885 Nr 3478 all.
