Jakub Tiidt Nüplist kaebas, et Karl Niklus temale ühe kuu töö palkast 2 rubla mitte ära ei makswat. Kätte olla saanud 1 rubla. See kuu olnud Märtsi kuu 1885. aastal. Kaebaja nõuab see 2 rubla kätte saada.
Karl Niklus ütles, et töö palka, mitte lepitud olnud. 1 rubla küll andnud, aga oma tahtmisega.
Mõistus: Jakob Tiidt kaebdus Karl Niklus wastu saab tunnistuse puuduse pärast tühjaks mõistetud. 
Mõistus kuulutati §773 seletamisega.
 
