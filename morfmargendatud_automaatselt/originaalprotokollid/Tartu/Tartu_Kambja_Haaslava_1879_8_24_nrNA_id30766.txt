Haslawa kogokonna kohus sel 24 Augustil 1879.
Juures istus:
Pääkohtomees: Mihkel Klaos
Kohtumees Johan Link
dito Johan Udel
dito Rein Anko
Ette tuli  Liis Kessa ja kaebas  Lena Annok ja  Krõt Pähkne peale et nemma peksiwa minno Erso nurme peal paijotse witsaga mööda kintse, et temma nende heina kotti oli ärra käkkinu, peksmist näi Miina Ers.
Lena Annok wastas kaebuse peale et ta mitte omma sõrme otsaga temma külge ei ole pannu.
 Krõt Pähkne wastas kaebuse peale middagi Liis Kessale tegewat aga lönud seda ähwardanud kui Kessa meije heina kotti ära ei oles juhatanu siis lubbanud Langemõisa temma heina wargust üles anda.
Miina Ers tulli ette ja tunnistas et temma ei ole midagi nännu et Liis Kessa kellegi käest peksa oles sanu, eli kuuli et kõwaste kõneliwa.
Mõistus:
Et Liis Kessal tunnistust ei ole et ta peksa on sanud jäeb kaebtus tühjaks.
