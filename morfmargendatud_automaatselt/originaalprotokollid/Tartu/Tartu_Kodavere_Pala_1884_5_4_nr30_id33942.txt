Tulli ette Karel Annuk ja kaebas et temal on  Jakob Kaijo kääst 9 Rbl 40 Cp renti raha saada ja nõudis ka weel omma jala waewa eest miss tema 12 korda kogokonna kohtus käinud  se on 3 Rbl 60 Cp. Summa kokku 13 Rbl.
Sai Jakob Kaijo ette kutsuda kes wastas et temal ei ole ni palju maksa tema on omma renti tö tehdega tassa tehnud.
4 wakkama heina nitnud			4 Rbl		1 wakama rükkid lõikanud			1 Rbl  20 Cp		3 wakama tõugu lõikanud			3 Rbl 60 Cp		17½ tuba puid lõikanud			4 Rbl 37 Cp		Ja puhast raha on temal			3 Rbl  		                                           Summa			16 Rbl 17 Cp		jäänud on 3. W. kaaro 12kg			 3 Rbl 60 Cp		                                           Summa			19 Rbl 77 Cp		
ja nende kaub olnud et 20 Rbl. rendi maksa ja saab  ja saab töödega tassa tehtud.
 Jakob Karel Annuk wastas selle peale et tema kaub olnud Jakob Kaijoga  et maksab 20 Rbl rendi ja 4 waka. heina  2 wak wilja kokku panna
Ja  2 wakka maa eest			2 Rbl 80 Cp.		17½  tuba puide eest			4 Rbl 37 Cp.		ja puhast raha			3 Rbl					10R    17 Cp.		
aga kasu ei ole tema rendi eest sanud need on niidete ette jänud sest tema Jakob Kaijo Willem Perrametsa käest 2 Rbl eest  wõlgu tõise Hallikul on tema  kuwe ärra toonu Jaak Tõrwa 1 Rbl  65 maksnud.
Tuli ette Jürri Sarwik ja tunnistas; et kui tema Karel Annukaga kauba tehnud siis on Jakob Kaijo ka kauba tehnud ja kaub olnu 20 Rbl renti 4 wakkamad heina ja 2 wakkan wilja ilma hinnada kokku
panna ja Jakob Kaijo on pidanud 30 sülda puid se raituma kelle eest 25 Cp sülla pealt   saab.
Kusta Annuk tunnistas  nii kui Jürri Sarwik.
Mõisteti: et tunnistus selge on et Jakob Kaijo 20 Rbl renti maksab ja 4 wakkamaad heina 2 w. wilja ilma hinna kokku panneb, siis peab Jakob Kaijo, Karel Annukal 9 Rbl 40 CP puudus jäenud renti raha  ära maksma, 2 näddala perrast ja et mitte korda kohtu ette tulnud ei ole selle pärast läheb 24 tunnist wangi.
Se mõistus sai ette kuulutud Jakob Kaijo ei olnud rahul sai Appelation.
                                               Kohtomees: Josep. Soiewa.
                                               Kohtomees: Otto Mõisa.
                                                Kohtomees: Mihkel Mölder. 
