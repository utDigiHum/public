Tüdruk Mari Simson kaibab: "Märt Limper olewa temaga lihalikult ühes kous olnud ja temale lapse teinud, ja ei tahtwa nüüd last toita."
Märt Limper salgab: "tema tüdruku külge ei olewa puutunud."
Tüdruki isak, Märt Koido, aga tunnistab: "tema ise olewa Limperi tüdruku juurest onnist ära ajanud."
Sellepärast et tüdrukul selgemat tunnistust ei ole ja kuulda on et tüdrukul tõistega ka tegemist olnud, mõisteti: Limper maksab tüdrukule üks ainus kõrd 2 wakka ruki ja 2 wakka odre; misga mõlemad jaud ka rahul jäid.
