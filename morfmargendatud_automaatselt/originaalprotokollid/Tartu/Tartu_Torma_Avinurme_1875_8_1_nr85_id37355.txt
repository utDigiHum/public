Awwinorme Metskoggokonnakohtu sel 1 Augustil 1875.
koos ollid
peakohtumees Josep Kask
teine kohtumees Juhhan Koppel
kolmas kohtumees Jaan Mänd
Et Koggokonnakohhus olli Josep Krupile oksjon kuulutud et temma ei tahtnud se kihhelkonna kohtu poolest mõistetud Metsatrahw ärra maksa, siis astus Josep Krup ette ja pallus se trahw wastu wõtta ning maksis 7 Rubl. 40 Cop. Koggokonna kohtu kätte ärra.
Josep Kask XXX
Juhhan Koppel XXX
Jaan Mänd XXX
