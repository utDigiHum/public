Awwinorme Koggo konna kohtus sel. 29mal Julil 1868
Kohto juures olliwad
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Jaan Saar
Astus ette Jakob Kull ja räkis, et temma sekord, kui temma Mihkel Saksa wölmönder olnud, ja temma issa tallomaad omma kätte wõtnud, on siis temma Leppiksarest sinna talluse põhku ja pikki õlgi weenud, ja et nüid temma sedda Tallo jälle Mihkel Saksale kätte annud, ja seal 50 Kubbo õlgi järrele jänud, ja pallus kohhut, et temma need õlled ärra wõttab.
Moistus
Kohhus moistab et Jakob Kull need järrelejänud õlled 50 Kubbo woib ärra wõtta, selle eest, mis temma sinna talluse olli wenud
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Jaan Saar XXX
