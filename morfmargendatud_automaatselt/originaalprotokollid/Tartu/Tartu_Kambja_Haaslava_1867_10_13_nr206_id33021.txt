Märt Mats kaibap et Johan Pürrik hulkup ümbre nink warrastanut üttel pühhapäiwa öösil temma kaest kats wakka rükki ärra; Märt Mats nowwap, et koggokonna kohhus Johan Purrik tolle eest trahwis nink nowwap weel enda 2 wak rükki taggasi.
Johan Pürrik wastutap, et temma ei hulku ümbre nink ei olle ka Märt Matsi rükki warastanut, ei wotta temma tunnistajast, siis mullemba ollewat temma wihhamehhe.
Kriimanni koggokonna liikme Karl Sarrap tul ette ja tunnist, et Johan Purrik wissanut kot rüäga leppistikku temma läinut mannu siis üttelnut Johan Purrik: "minna tõin rüat koddust nink tahtsin körtsin wija", agga Johan Pürrik ei olle koddopoolest, wait Märt Matsi poolt rüatega tulnut.
Kriimanni koggokonna liikme Jaak Turba tul ette nink tunnist et kats jooksnuwat temmal wasto, üts olnut Johan Purrik, ja tõine olnut Karl Sarrap, Karl Sarrap üttelnut et Johan Purrik toonut M. M. keldre päält rükki nink wissanut mõtsan, Johan Purrik pallelnut jälle, et temma Jaak Turba ei pia ärra ütlema.
Moistus: Johan Pürrik om se Märt Matsi rüa warras, sis temma ei woi selgest tehha kost temma neit rüat tol öösil sanut, mis temma mõtsan wissanut nink piap Märt Matsil 2 wakka rüa eest 7 rubla hbd. massma ja saab warastamise eest 30 Wiza löökiga trahwitud-
Pähkohtomees: Johan Hansen XXX
Kohtomees: Jakob Jansen XXX
Abbi Kohtomees Jaan Wirro XXX
Moistus sai kuulutut, nink suuremba kohto käimisse õppetusse kirri wälja andda.
