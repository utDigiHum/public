Ette tuli Jaan Kubja ja kaibas nemad olla oma wenna Karliga mõnni aasta üht rendi kohta  kokko poolest pidanud ja selle rendi asjas põllu jagadise perrast selle koha pääl 108 rubl. wälja maksnud ja nüd  kui tema selle koha pealt on ära läinud ja seda wennale jätnud, siis  kaevab tema,et Karel sel maksu ajal poole wilja on saanud, et see temale nüd sest rahast poole peab maksma. Karel wastas, et nüd kewade kui nemad on oma wahet teinud ja Jaan ühe wälja 5 wakkamaa põldo pidada lassi ning see nimetud raha ei olle sugugi tema visi: temma olla ka Jaanile  see nimetud maa kätte antu.
Karel Wadi tunnistas  wendade wahe? olnud nii, et Karel on Jaanile lubanud kaks wälja kumbgi 2 1/2 wakkamaad se on 1 wälli ja 8 wäli sellega olnud nad rahul, need wäljad olnud Jaani oma sügise kasudmatta jäänud kaera kõrs linade tarwis ja teine wastu Waddi raja Kartoli maa.
Tawet Willemson tunnistas niisama.
Kohto otsus: Karel peab Jaanile need nimetud wäljad andma ja Jaan wasto wõtma aga raha  naad  neise polle Jaanil Kaarli käest mitte.
See otsus sai Kohto käijatele ette kuulutud ja et Jaan rahul ei olnud sai temale appellatsion wälja antu.
                                                       pä Kohtomees: Hindrek Horn
                                                             Kohtumees: Josep Soiewa
                                                              Kohtumees: Dawet Willemson     
