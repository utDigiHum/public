Tulid ette isiklikult tuntud Roela walla liikmed Gustaw Madise p Jõgi, elab Roela wallas, tema poeg Juhannes Gustawi p. Jõgi, kui oma weikese laste Olga Rosine Jõgi ja Leonard Jõgi loomulik eestkostja, elab Pala wallas ja teewad järgmise kinkimise lepingu:
1. Gustaw Jõgi kingib oma lastelaste Olga Rosine ja Leonhard Juhannese laste Jõgidele, Pala walda Lanewälja talusse omanduseks:
3 lehma, a` 23 rub			69 rub.		1 hobune			60 rub.		1  wanker			30 rub.		3 siga			70 rub.		wedru äkke			10 rub.		12 wakka kaera seemet 			25rub.		6½ wakka rukki seemet			19 rub.		1½ wakka nisu seemet			 6 rub.		4 wakka odra seemet			10 rub.		                                            Kokku			299 rubla		
II Olga Rosine ja Leonhard Jõgide eestkostja ja isa Juhannes Jõgi wõtab oma laste nimel nendele antud kingituse wastu ja on selle juba kätte saanud.Juhannes Jõgi (allkiri)
Gustaw Madise p Jõgi ei oska kirja, tema isiklikul palwel kirjutab alla : Jaan Märtsin (allkiri)
Pala wallakohus tõendab, et see leping isikliselt tuntud Roela walla liikmete Gustaw Jõgi ja tema poja Johanes Jõgi, kui oma laste Olga Rosine ja Leonhard Jõgi loomuliku eestkostja wahel tehtud, nendel säädusline lepingu tegemise wõimus,  leping nendele etteloetud ja nemad selle oma käega on allakirjutanud, pääle Gustaw Jõe, kelle palwe pääle Kudina walla liige Jaan Kaarli p Märtsin allakirjutas, Jõe kirjaoskamatusel.
Esimees A Priks
Liikmed J Wadi K Elken
Kirjutaja Brükkel.
