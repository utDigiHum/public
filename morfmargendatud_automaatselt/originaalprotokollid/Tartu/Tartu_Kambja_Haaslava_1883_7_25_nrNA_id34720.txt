Haslawa Kogukonna Kohus sel 25 Julil 1883
Man ollid:
Päkohtumees: Jaan Link
Kohtumees Johan Klaosen
Peter Iwan
Johan Grossberg
Kaibas  Jürri Wuks, et tema om 8 wankri tsõri, mis a 2 Rubl  16 Rbl maksab, 2 pari wankri Redelit a 1 Rubl koko 2 Rubl, 3 raudpuud a 50 Cop  Endrik Kärikule tennu om ja pallus et kohus sunis seda raha temale massa. Päle selle om tema Molliko  Petre poja wäitse leidnu ja  Endrik Kärik om selle wäitse wägise tema käest ärra wõtnu ja tema peab 1 Rubl selle wäitse est kaotajale massma selleperrast nõusb tema 1 Rubl selle wäitse est. 
 Endrik Käriko käest om tema sanu:218 (ei loe välja)			35 Cop		Selged raha			60 Cop		20½ naela leiba sanu			61 Cop		Summa			156 Cop		
Selle perra jääb temal weel 18 Rubl 94 Cop sada.
Endrik Kärik wastutas selle päle, et se Jürri Wuks 4 wankri tsõri siadnu om, 2 paari reddelt tennu om ja 1 raudpuu om temale tennu. Selle töö est om Jürri Wuks tema käest sanu7⅔ hobuse päiwa a 1 Rubl			146 Cop		1 hobuse päiwa est lubanu rükki põima ent ei olle tennu			50 Cop		4 nakla tubakut om tema tonu massab			60 Cop		5 nakla silku massab			30 Cop		10 nakla heringat massab			80 Cop		2 nakla "			16 Cop		26 nakla leiba			78 Cop		Selged raha			60 Cop		Summa			374 Cop		
 Jürri Wuks om kül temale 2 nakla tubaku est ütte wäitse andnu ja kui  Jürri Wuks selle tubaku ärra massab annab tema wäitse kätte.
Mõistetu, et Endrik Kärik 53 Cop Jürri Wuksile wälja massma peab, muido om keik nõudmise tühjas arwatu ja Jürri Wuks om nomido, et tema enamb tõine kõrd nenda kerge melega ilma aigo kohut ei tülita.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees: Jaan Link /allkiri/
Kohtumees:  Jaan Klaosen XXX
Peter Iwan /allkiri/
 J. Krosbärk  /allkiri/
