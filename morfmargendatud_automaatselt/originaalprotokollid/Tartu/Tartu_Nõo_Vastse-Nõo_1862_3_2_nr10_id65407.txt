Kohtomeeste wallitseminne
Koon olliwa päkohtomees Margus Laas, abikohtomees Margus Lauberg, abikohtomees Simmon Sohnwald ja walla wöhmönder Karel Frei ja weel olliwa koon allan nimitedu perremehet1			Andrese Karel Soonwald		2			Sillamatzi Jaan Peddajas		3			Krassi Jaan Nigol		4			Putso Peter Pourson		5			Peddaja Ehristson Pourson		6			Lauri Hans Thomson		7			Nigula Margus Pourson		8			Soome Karel Pourson		9			Pörsa Jaan Frei		10			Piiri Jürri Sild		11			Lallimäh Jürri Sion		12			Konnaawwa Jaan Sourson		13						
Remly Margus Lõhmus					14			Pikkomatzi Jaan Kengmann		15			Soone J. Sohnwald		16			Laaska Johann Laas		
Soiwa üllewal nimiteda perremehet ütsi küssitus ketas nemma päkohtomehes wallitsewat 5 ütliwa wanna pakohtomees Margus Laas 12 ütliwa abbikohtomees Margus Laurberg jago päkohtomehes.
Es Margus Laurbergil ennamb ellisit olli jäije temma päle 2, saije küsitas ketas teie siis Margus Laurbergi asemele wallitsad 4 üttliwa Pörsa perremiis Jaan Frei 17 ütliwa Remlj Margus Lõhmus.
Es Margus Lohmusel ennamb ellisit olli saije temma päle 3, saije küsitas ketas teie siis võiks abbikohtomehes wallitsade 6 ütliwa wanna abbikohtomees Krasi Simmon Sohnwald jagu 13 ütliwa Putso Peter Pourson jägo.
Es Putso Peter Poursonil ennamb ellisit olli jöije temma 4, saije küsitus ketas teie siis walla wohmöndres walitsade keik ütliwa wanna walla wolmönder Karel Frei jägo.
Keigide man olliwa perremeeste nimmel kirjotawa wannad kohtomehet ende nimmet seija alla.
Üllewal kirjotedu protokoll saije moisa wallitsusest ette pantas, et moisa wallitsas sädase perra neid wästsest wallitseda kohtomehi kinnitas.
Es moisa wallitsas selle wallitsemisega rahhel om panneb temma moisa perrishärra lattoga enda nimme sija alla.
