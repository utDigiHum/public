Sel 23 Julil 1884.
Johani Tallu ostja Johan Klaosen andis omma Tallu rentimist Johan Madisonile ülles, selle et tema tallu ärra müinu om, sis peab Johan Madison se Johani tallu Jürripäiwas 1885 selle tallu ostjale Ado Ottase kätte andma, mes ka Johan Madisonile kulutado sai kellega Johan Madison ka rahul lubas olla. Ado Ottas Surest Kambjast olli ka seija tullnu ja andis Johan Klaosenile ülles et tema ni kui neide kaub om selle Johani Tallu Jürripäiwal 1885 kätte tahab sada.
Otsus: Seda Protokolli ülles panda.
Kohtumees:
  Peter Iwan /allkiri/
 J. Krosbärk /allkiri/
Jaan Pruks /allkiri/
