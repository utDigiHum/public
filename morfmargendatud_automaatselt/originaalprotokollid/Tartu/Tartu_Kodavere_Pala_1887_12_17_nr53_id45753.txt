Protokolli peale 10 detsembrist s.a. N. 48 ja 49 saiwad ettekutsutud Kustaw Wipper ja Karl Tragun ettekutsutud ja neile awaltud et nemad nagu Miina Puusepa Jüri lese ja laste wöörmündriks walitud on. Karl Tragun aga andis üks wäga tähtjas takistus mis tema perekonna kohta seeläbi sünnib wabanduseks ette ja et see kogukonna kohtule teadaw oli siis, tegi kohus otsuse.
Otsus: Karl Tragun sellest ametist wabasdada ja tema asemele metsawaht Willem Kukk nagu Miina Puusepa laste wöörmündriks walida ja Willem Kukkele seda awaltada. See otsus sai Karl Tragunile Kusta Wipperile ja Willem Kukele awaltud.
                           Peakohtuimees J. Sarwik. 
                           Kohtumees        J. Stamm
                           Kohtumees        M. Piiri.
                           Kirjutaja allkiri mitteloetaw. 
