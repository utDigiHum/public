№ 76
Krimanni s sel 15mal Mail 1870.
 Jaan Poedderson  perrismaapiddaja tulli sel 1. Mail kohto ette ja kaebas sannamees  Hendrik Kõra  peale minna kaubelsin Hindrik Kõra tüdtar Lena endale karjuses ja sötsin täis 5 näddalat sedda tüdrukud, agga Hindrik Kõra tulli se pühhapäiw enne Jürripäewa ja wiis omma tüdre minno mand ärra, nüüd nõuan minna karjust.
Siis sai  Hindrik Kõra 15mal Mail kohto ette kutsotud ja ta ütles: se on wõls sinno naene on minno naese käest tüdtard abbi karja tallitama wõtnud ja 30 kopka minno naise kätte rahha annud, minna ei tea kauba teggemissest middagi muud kui et tüdtar abbis karja tallitamas on, sest minnol on issegi ulga päewi tehha, egga ma ei wõi endal seddagi särgi säljast ärra wõtta laske kui ma weel tüdre ka ärra andma pean, agga tüdtard minna mitte ei anna ja se peale on tüdtard minno naene annud et kui Jürripäiwani.
Mõistus: Krimanis sel 15mal Mail 1870.
Koggokonna kohhus mõistis:  Hendrik Kõra  peab oma tüdtar Lena Jaan Poeddersoni karjuses taggasi andma ehk 10 kop igga päewa sömisse eest maksma mis s. 3 rubl. 50 kopka. 
Kohto lauan olliwa:
Peakohtomees :Jürri Bragli XXX
Abbi "  Ado Lenzius  XXX
"  "      Johan Mina   XXX
Kirjutaja  W. Kogi /allkiri/
 Hindrik Kõra ei olnud Koggokonna kohto mõistmissega rahho ja nõuab suuremad Keiserlikko kohhud, Endrik Köra wõttis Protokolli sel     Mail wälja.
