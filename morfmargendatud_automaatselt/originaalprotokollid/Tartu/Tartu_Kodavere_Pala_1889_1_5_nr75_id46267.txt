Tännasel pääwal andis Amalie Tõldsepp kadunud Bernhard Adelsoni lesk oma sissetulek ja wäljaminek järgmiselt üles ja nimelt:
                                 Sissetulek:
25 wakka kaeru a`4 R. 			34 Rbl. 66 kop.		 6 wakka rukkid a` 2.33			14 Rbl. -		üks härg			31 Rbl.		ristiku seemned  			62 Rbl.		                            Summa			141 Rbl. 66 kop.		                 Wäljaminek maha          			 32 Rbl. 50 kop.		                            Jääb järel			109 Rbl. 16 kop.		
kellest weel Aidame talu maksud maksa tulewad, mis 185 Rbl. wälja teewad.
                                      Wäljaminek:
üks pütt heeringaid			16 rbl.		2 kotti soola			 4 rbl.		1 paar saapid			 5 rbl.		2 paari poolsaapid			 5 rbl.		tüdrukule saapad			 2 rbl. 50 kop.		                                             Summa			32 Rbl. 50 kop.		
Wöörmündrid Karel Kubja ja Karel Hawakiwwi tõendawad seda õigeks.
Otsus: Jüri Tõldsepp oma kwiitungitega ette tarwitada.
                                     Peakohtumees:  J. Sarwik.
                                            Kohtumees: J. Stamm.
                                            Kohtumees: M. Piiri.
