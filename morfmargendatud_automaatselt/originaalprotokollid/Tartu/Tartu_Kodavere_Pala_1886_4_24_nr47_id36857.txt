Protokolli pääle 27 Märtsist s. a. N:41 saiwad ette  kutsutud Maddis Abramson ja Jürri Sarwik ja neide tunnistuste wäljakutsumised mis siin kohtu juures käinud, awaldtud ning ei olnud mõlematel enam senna juure ütlemist.
Kohus tegi otsus et tunnistused selgeks teinud et Jürri Sarwiku elajad Maddis Abramsoni heinamaad rikkunud maksab Jürri Sarwik Maddis Abramsonile 
1, Hobuse kinniwõtmise eest 1 rubla
2, heinamaa rikkumise eest 2. saadu heinu
3, Selle eest et Jürri Sarwiku naine Ann hobuse äratoomise juures tüli teinud maksab Ann Sarwik kolm rubla trahwi waeste laeka.
See otsus sai kuulutud, ei olnud Maddis Abramson sellega rahul ning anti Tal. Säd. raamatu § 772, 773 ja 774 põhjal Madis Abramsonil appellationi leht selsamal korral kätte kõige tarwiliste õppustega.
                                                   Pääkohtumees J. Hawakiwi. 
                                                          Kohtumees J. Stamm.
                                                          Kohtumees M. Piiri.
                                                           Kirjutaja allkiri.
