Tuli ette Joosep Saarwa Ahja wallast ja andis üles, et tema tahab töistkorda abielusse astuda ja lubab oma esimesest abielust sündinud lastele Liisa 9a. Emilie 7a. ja Idale 3a igaühele 5 rubla ühe aasta jooksul selle walla kohtu kätte ära maksa. Muud warandust temal lubada ei ole, ning ei ole surnud abikaasast midagi järele jäänud. Seeni kui raha weel ära maksetud ei ole wastutab Joosep Saarwa kõige oma warandusega.
Joosep Saarwa &lt;allkiri&gt;
Et kohus alakirjutajad Joosep Saarwat selgeste tunneb ning et see oma käega ala on kirjutanud saab sellega tunnistatud.
Eesistuja J Kooskord​​​​ &lt;allkiri&gt;
Kirjutaja Puusepp &lt;allkiri&gt;
