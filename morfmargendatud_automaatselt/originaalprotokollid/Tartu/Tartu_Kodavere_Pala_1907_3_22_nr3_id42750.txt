                                                                                  Rospiska.            Kopija
Ja nižepodpisavšijisja Emanuel Lanne prusski podannõ sim udostoverjaju, tšto polutšiv ot Rižskoju obštšestva vzaimnoju  otrahovanija ot  nestšastnõh slutšajev jedinovremennoje  vozpopražojenija v razmer trista rublei (300 rub-kop) za uvedomlenijem, polutšennoe mnoju 27 fevralja 1907 vo vremja pabotõ v imeniju Palla i  za vse moguštšija proizoiti ot  etogo nestšastnago slutšaja posledstvija, priznaju sebja vpolna udovletvorepnõm i za sim nikakih dalneiših trebovaniji ni k võšeupomjanutomu obštšestvu ni  k moemu  togdašnemu rabotodatelju ili k ego služaštšim  ne imeju.
                                                                                                                                                                                                                                                                    Emanuel  Lannoja
Pallaski volostnoi Sud Jurjevskogo ujezda Lifljandskoi guberniju sim udostoverjajet, tšto predstojaštšaja podpis utšipela sobstvennorutšno vsem  sud prusskim poddannõm  Emanuelem Lanne volostnomu sudu litšno po vožnõm proživajuštšim v imeniju Palla Jurjevskogo ujezda. Po aktovoi knig N 3.
                Predsedatel K. Reinhold
                Tšlenõ K Holst W Tamming
                Pisar Brjukker              
