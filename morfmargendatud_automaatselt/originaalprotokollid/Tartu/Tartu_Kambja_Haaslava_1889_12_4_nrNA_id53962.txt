Sel 4 Detsembril 1889.
Kaibas Karl Klaosen, et kui tema puu kormaga mõtsast tullnu, wõtnu Jaan Roiland tema tee peal kinni, wõtnu puu, keutse ja pääkotti ärra ja tahtnu teda püssiga maha laske. Karl Klausen nõudis 3 rubl 50 kop puude est ja 1 rubl keutse ja päkotti est.
Jaan Roiland wastutas, et ösel tullnu kaibaja Karl Klaosen warguse pält puu kormaga ja lönu tema koera maha, mis peale tema Karl Klaoseni hobuse kinni wõtnu ja puu korma maha panda lasknu, keue ja päkotti andno tema Karl Klaosenile kätte, seal jures ollnu Jürri Pruks ja Jürri Schwalbe.
Karl Klaosen andis tunistajas ülles Kusta Märtin, kes Korzul ellab ja et tema Jaan Roilandi koera lönu ei olle ja et tema puid warrastanu ei olle.
Otsus: et Jürri Pruks, Jürri Schwalbe ja Kusta Märtin ette om kutsuta.
Päkohtumees: Jaan Wirro XXX
Kohtumees:    Jakob Saks /allkiri/
"   Jaan Treial /allkiri/
