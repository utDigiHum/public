Ette tulli Willem Perramets ja kaibas et temal on 2 märtsil künis heinu warastud ja tema on need Palla  kaupmees Kirsi jurest  jälgi pidi
 perra minemise läbi kätte saanud ja nõuab nende eest 10 rubl. sest tema on nied walla wöörmünder Reinhold Stammil   pera kaeda lasknu . Kaupmees Ans Kirs wastas tema on need heinad Hallikult Jakob Ernits ja Mart Kronk käest ostnud sel 3 Märtsil homiku wara. Reinhold Stamm wastas tema on kül need heinad perra watanud ja leidnud, et need heinad mis Willemil warastud ja ka
mis Kirs ostnud on ühed ja ja need samad olnud ja arwab neid 10 puda olla.
  Kohtu otsus: Et kaibaja kui ka kaibatu wahel selkeks on saanud, et kaupmees Kirs heinad on  wõtnud siis peab tema walla wöörmündri Reinhold Stami tunnistuse peale 10 puda heinte eest mis tema  ise on wastu wõtnud  5 rub Perrametsal maksma ja wõib müiatte peale kaebata, kui arwab, et need südi on. 
 See mõistus sai kohtu käiatelle appellationi sädusega ette loetu ja et  Kirs rahul ei olnud sai temale appelation lubatud.
            pä Kohtumees Hindrek Horn
            Kohtumees      Märt Piiri
            Kohtumees      Josep Soiewa.   
