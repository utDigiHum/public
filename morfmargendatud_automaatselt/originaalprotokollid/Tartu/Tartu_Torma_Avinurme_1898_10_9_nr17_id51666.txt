Protokoll
Awwinorme Wallakohtus sel. 9. Oktobril 1898.
juures olid
Eesistuja Mihkel Pihlak
Kohtumees Jaan Pärn
Abikohtumees Jaan Haaw
Tuli ette  Awwinorme walla ja Teadosaare küla pobbol inimene Jaan Kallaiis Maddise poeg. ja palus Wallakohut sedda Protokolli üles kirjutada et tema oma liikuwa ja liikumatta varantus se on oma maja kraam ja majaloomad ning oma Pobboli krunt N 475 mis Awwinorme Wallas ja Teadosaare külas peale oma surma oma teise abbi elu naisele Ellu Kalaiisile sündinud Roode pärida lubbab, ja ei ole tema suggulaistel tema varantuse küllest ei mingisuggust jaggu saada, ja on tema sellepärast oma varantus oma teise abbielu naise Ellu Kalaiisile ükspäinis pärida lubanud. sest et temal oma esimeses ja teises abbielus ei ole lapsi olnud; ja palub tema keiki kohtusid ja kohtu ametnikkusid seda testamenti selle jõu sees piddada kuddaviisi tema on seie protokolli üles kirjutada lasknud ja et se muutmatta jäeb. ja lubab tema Jaan Kallaiis et ka keik tema majad mis tema krunti peal tema naine Ellu Kallaiis pärib.
Jaan Kallaiis ei moista kirjutada tema palwe peale kirjutas alla Jaan Tammik (allkiri) 
Otsus
Wallakohus on Jaan Kalaiisi sowimist taites tema lubatus ja testament Protokolli üles kirjutanud et se peale tema Jaan Kalaiisi surma tema parijattele saab teada antud.
M. Pihlak (allkiri)
J. Pärn (allkiri)
J Aaw. (allkiri)
Kirjutaja Schulbach (allkiri)
