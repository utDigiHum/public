Protokolli pääle 24 Aprillist s.a. N:44 Otto Mõisa ja Josep Mõisa kaebuse asjus sai ette kutsutud Mokku koolmeister Willem Kirik ja wastas küsimise pääle et  Dr Masingi tüdrik Roosi Kondrat olnu rääkinud et Kodawere õpetaja saatnud tohtrile kirja kelle järele Liisa Mõisa Josepi tütar tohtri poolt läbiwaadatud saama peab muido ei wõeta lauakiriku wastu (ei sa peakoolik? lahti tehtud) Liisa Mõisa  Otto tütar olnu ka sedawiisi Koolmeistri wastu rääkinud. 
Josep Mõisa andis üks tunnistus Dr Masingi poolt kelle järele tema Josep Mõisa tüttart kunagi läbi waatnud ei ole. Õpetaja poolt siia saadetud laimamise kirjad üle Liisa Mõisa, mis õpetaja kätte saadetud olid, anti Koolmeistri kättewaadata ja ütles see et üks kiri seda moodi kirjutud olla kudas tema koolilapsed kirjutama õpiwad aga kes seda kirja kirjutanud ei wõi selgest ütelda. 
Ette kutsutud Sohwi Peterson ja wastas küsimise et tema kedagi üle Liisa Mõisa ei tea.
Ewa Sallo Jürri naine tunnistas kohtu juures et tema muud ei tea et Rosi Kondrad ja Liisa Mõisa Otto tütar ühe lugija lapse küsinud kas üks mees õpetaja juures käinud, kas üks lugija tüdruk õpetaja juure kutsutud ja peale selle ütelnud et üks lugija tüdruk ei sa lauale wõetud saama waid tagasi saadetud, se tüdruk peab ligidal Palla wallas elama.  Nime ei ole nimetanud.
Liisa Mõisa Otto tüttar ei olnud tulnud.
Otsus: Liisa Mõisa, Otto Mõisa ja Josep Mõisa tulewa kohtupääwaks ettetellida.
                                     Pääkohtumees J. Hawakiwwi.
                                           Kohtumees J. Stamm.
                                           Kohtumees M. Piiri.
                                           Kirjutaja allkiri.
