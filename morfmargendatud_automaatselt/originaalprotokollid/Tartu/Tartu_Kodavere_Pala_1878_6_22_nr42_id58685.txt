Ette tulli Kadrina walla innimene Marri Somus oma 16 aastase tütre Liisaga ja kaebas Rein Kütt kõrwas, et Palla mees Tawet Kukk on Ranna otsa weski jurest laudu toonud ja te peal sel 19 Mail Piri mõtsas karja jures tema tütre Liisa kinni wõtnud ja selle lapse ärra  /: riikkunud:/ narrinud mis järgi nüd se  laps wägga haige olla. Tawet Kukk salgas ja andis wastust tema polle ühtegi karja näinud waid  on ennegi laudu tonud ja paljalt Piiri kõrtsis sees käinud.
 Kohtu otsus Liisa Somus peab sama Tohtri poolt läbbi watatud  sama kas tema on meeste rahwast rikkuda saanud ja siis saab  eddasi tallitud.
                                Pä kohtumees: Kusta Kokka        +++
                                Kohtumees: Thomas Welt           +++
                                    Abi kohtumees  Mihkel Lallo   +++   
