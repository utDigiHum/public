Tuli ette Lena Õunap Josepi lesk  eestkostjaga ja kaebas et Josep Otsa wiie aasta eest tema käest 10 rubla lainanud ja seda raha tänase pääwani mitte tagasi maksnut ei ole sellest asjast teadwad endised kohtumehed Hendrik Horn, Josep Soiewa ja Otto Mõisa. Nõudis seda raha kätte. Josep Otsa ettekutsutud wastas küsimise peale et tema Lena Õunapule kedagi enam wõlgu ei ole.
 Ettekutsutud endine kohtumees Hendrik Horn ja wastas küsimise peale et tema seda asja enam ei mäleta. Ettekutsutud Otto Mõisa ja andis protokolli et üks jutt 10 rublast mis Josep Otsa käes Lena Õunapu raha olla  olnud aga õiged otsa ei tea tema ka mitte.
 Josep Soiewa ei olnud tulnud. Otsus Josep Soiewa tulewa kohtupäewaks ettetellida.
                      Kohtumees        J. Stamm
                      abi Kohtumees J. Tagoma
                      Abi Kohtumees   K. Stamm
                      Kirjutaja  C Palm.(allkiri)
