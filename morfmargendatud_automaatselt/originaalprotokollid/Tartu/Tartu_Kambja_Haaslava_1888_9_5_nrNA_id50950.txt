Sel 5 Septembril 1888.
Kaibas Karl Lesta Haslawa Kogukonna nimel, kelle ülle Wolli-kirja ette näidas, et enine wallawanemb Jaan Soo ei ollewat ütte wennelase käest 15 rubl Karl Taari massu kistutuse päle wasto wõtnu ja passi andno ja se wennelane lubanu weel ega kuu 5 rubl Karl Taari massu kistutuses edespidi massa, sest Karl Taari ollewat 38 rub päraha wõlgu ja saanu sügise päle selle soldatis antu, selleperrast nõuab tema 38 rubl Jaan Soo käest, tõiselt ollewat Jaan Soo surno wallawölmündre Jaan Grossbergi 3 kuu palka 375 kop ommale wõtnu ilma Nõukogu lubata ja nõudis seda Jaan Soo käest tagasi. 
Enine wallawanemb Jaan Soo wastutas, et se nõudminne temma ametlikko tallituse külge puttub, kelle ülle enegi Keiserlik Kihelkonna Kohus mõistab, selleperrast ei anna tema sellen asjan rohkemb selletust. Rehnungi om tema ega asta Nõukogule andno.
Mõistetu: et Karl Lesta kaibus wasto Jaan Soo wastu ei olle wõtta enge tagasi om lükatu selle et 1866 astal wälja antu Kogukonna säduse § 20 perra wallawanemba wõimusse piiri sehen passi andmine ja ka palka massmine om, selleperra nisugune kaibus enemba Uellewatja kohtu ette wija olli.
Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu sel 12 September 1888.
Päkohtumees: Johan Link XXX
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan  Hansen /allkiri/
