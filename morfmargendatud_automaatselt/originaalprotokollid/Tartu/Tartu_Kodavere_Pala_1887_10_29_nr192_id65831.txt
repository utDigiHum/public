Protokolli peale 20 Augustist N.157 oli Widrik Ertis wahi all Ranna wallapolitsei poolt ettetarwitud ja ka tulnud ning ettekutsutud ning wastas küsimise peale et tema küll Jakob Willemsoni matmise aeg oma heinamaa pealt ühe wõera lehma kinniwiinud  ja metsawaht  wärawa juure kinnipandnud, metsawaht ei ole kodu olnud ja olla see lehm ka salaja sealt erawiidud, see lehm olnud üks hall lehm ja niipalju kui tema teab mitte wigane olnud ei ka tema Ertise läbi wigastud saanud.
 Otsus: Mats Kronk ja Karl Waht tunnistuste awaldamiseks ettetarwitada.
                    Peakohtumees J. Sarwik.
                    Kohtumees        J. Tamm.
                    Kohtumees        M. Piri.
                    Kirjutaja G.Palm.   
