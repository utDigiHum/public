№ 105
Sel 6 Augustil 1890
Kaibas Johan Woites Krimanist, et Jürri Kitzing tennu temaga 1890/91 asta päle sullase kauba, ent ei olle tingitud aijal tenistussi ilmunu. Palka olli asta est lubbatu 40 rubl raha ja 2 wakka Kartoflid maha panda ja selle kauba kinnituses sanu Jürri Kitzing 1 rubl käeraha. Johan Woites nõuab nüid Jürri Kitzinge käest kaubeltu asta palk ja se antu käeraha ja pallus et Kohus seda Jürri Kitzingel massa panes.
Joh Woites /allkiri/
Jürri Kitzing Haslawalt wastutas selle päle, et temal ollnu enne talwel omma walda Tilba perremehe jurde kaub tehtu, se Johan Woites wienu teda kõrzi, jotnu teda seal ja sundinu sullase kauba tegema ja andno temale üts rubl. tema ei mälleta paljo palka ollnu kaubeltu ja mis tarwis se rubl tema sanu. Temmal ollewat sügise wäetenistusse minna ja ei wõinu suguki asta tenistusse kauba tehta. Kui temal raha olles sis massa tema käeraha tagasi. Temal ei ollnu kauba tegemise tarwis wälja poole walla piiri tähte ollnu.
Jürri Kitzing ei mõista kirjutada.
Johan Woites wastutas, et tema Jürri Kitzingega kül enne kõrzin käinu ja tema tennu kodun Jürri Kitzinge kauba. Kauba tegemise jures ei olle Jürri Kitzing temale suguki üttelnu et temal kaub omma walda tehtu om enge üttelnu, et tema wend Johan Kitzing tema est Tilba perremehe jurde kauba tennu ollewat ja üttelnu, et tema Tilbale tenistussi ei läha. Kauba tegemise jures ei olle Jürri Kitzing jobnu ollnu ja luba tähte ei ollnu Jürri Kitzingel mitte. Johan Woites andis tunistajas ülles Karl Klaosen Haslawast ja Willem Wälk ja pallus et kohus neid tunistajat ülle kulas.
Päle selle lepisiwa Johan Woites ja Jürri Kitzing nenda ärra et Jürri Kitzing massab 20 Augustil s.a kaits rubl selle kohtu kätte Johan Woitese heas ja kui Jürri Kitzing 20 Augustil s.a seda raha ärra ei massa, massab tema 2 rubl jurde.
Joh Woites /allkiri/    Jüri Kizinke asemel August Klaosen /allkiri/
Otsus: Seda protokolli ülles panda.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks /allkiri/
"  Jakob Saks /allkiri/
