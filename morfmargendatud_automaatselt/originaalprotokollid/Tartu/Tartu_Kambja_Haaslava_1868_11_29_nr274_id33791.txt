Jaan Kliim ja se Liiso Luiska leppisiwat nendawiisi:
Jaan Kliim lubbap selle latse kaswatamissest, mis temma selle Liiso Luiskaga teinut, egga aasta 1 wak rükki ha 1 wak keswi massa 4 aastat läbbi, se om üllepäh 4 wak rükki ja 4 wak keswi.
Liiso Louiska olli sellega rahhul.
Otsus: Neide lepping saab prottokolli ülles pandtu nink jääb kindmas.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
