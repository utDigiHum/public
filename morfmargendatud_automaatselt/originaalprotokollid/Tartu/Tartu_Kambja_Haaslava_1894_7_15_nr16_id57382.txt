№ 16
14  Haaslawa  Wallakohus 15 Julil 1894 a.
Juures oli  Eesistuja: Joh. Hansen
Juuresistuja: Jaan Tekkel 
Johan Klaos 
 Johan Pilberg 
 Haslawa Kitse talu  omanik  Rein Anko palus oma rentnikule  Johan Pokats kuulutada, et tema 23 Aprillil 1895 aastal see Kitse talu tema käest ära wõtab.
 Rein Anko /allkiri/
See ülesütlemine sai  Johan Pokatsile Walla Seaduse § 215  järele 1860 a. kuulutatud, aga Johan Pokats wastab, et tema seda ülesütlemist mitte wastu ei wõta, selle et neil oma wahel tehtud kirjaliku lepingu järele rendi aeg weel 10 aastad edasi kestab. 
 Johan Pokats  ei mõista kirjutada.
Kohtu Eesistuja: J. Hansen /allkiri/
Juuresistujad: J. Pilberg /allkiri/
J. Tekkel /allkiri/
J. Klaos /allkiri/
