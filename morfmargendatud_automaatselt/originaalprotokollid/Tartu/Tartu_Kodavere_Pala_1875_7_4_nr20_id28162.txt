Karel Kool Kadrinalt kaebas: Temma müinu se kewade Eberhardt Ehrlichile 200 wakka kartulit 75 rubla eest ärra ja on temma rõuku juurde tulnu, kartulit ärra andma. Essite mõõtnuwa summa 50 wakka ärra, mis järrel Ehrlich mõõtmise seisma jätnu, ja nemmad kokkoleppinuwa, ja ka sellepeale käed andnu , et  Ehrlich ilma mõõtmata se rõuk 200 wakka eest ja wiimselt et mullajaggu mahha jääb 190 wakka eest wastawõttab. Nüüd nõuab temma Ehrlichi käest 190 wakka raha se on 71 rubla 25 kop: Tunnistajat leppingu jures olnu Märt Grünthal Kaddrinalt ja Jaan  Perrajerw Pallalt.
Eberhardt Ehrlich wastas selle kaebealuse peale: Temma kaubelnu 200 wakka kartulit Karel Kooli käest 75 rubla eest ja on sisamate Karel Kool üttelnu peale, selle kui wakka mõdetu olnu. Mõõtke  
si ärra, mis pudus tulleb kül ma sedda järrel maksan. Peale  mõõtmise leidnu temma agga 170 wakka, kelle rahha mõisawallitsus, et Kooli käest kohaomandamise perrast  nõudmist, kinni pandnu.
Karel Kool andis weel tunnistajaks Jakob Ant Ranna wallast. /:wälja:/ Ettekutsutu tunnistaja Märt Grünthal; kes protokolli andis: Ehrlich ja Kool läinut kartuli rõuku jures tülli kahe wakka kartulite perrast. Wiimselt pakkunu Kool Ehrlicile sedda rõuku 190 wakka eest, mis järrel   nemma koddo läinut ja perrast  Kooli ütlemise järrel keik rahhu teinut ollema. Kuidas nemma leppinuwa, ei tea temma mitte, kartuli rõuk sanu peale selle jälle kinnipantud ja sõitnuwa  nemma koddo /:wälja:/
Kohtoliikmet mõistsit:
Teises Kohtopäewas Kohtokäijat, Jakob Int Rannawallast ja Jaan Perrajerw Pallalt ettekutsu.
                        Kohtomees:Josep Hawakiwwi
                         Kohtomees: Josep Rosenberg
                         Abbi kohtomees: Michel Lallo.
                                  Kirj. allkiri
                          
