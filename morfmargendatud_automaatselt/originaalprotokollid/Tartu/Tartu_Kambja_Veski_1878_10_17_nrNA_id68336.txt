         Sel 17nel oktbril 1878.
Sai se hobbune oksjoni wiisil ärrä müidus 8. rbl 15. kop eest Karl Paltzale. Kõrb ruun wanna hobbune, mes Weskimõisa walla Matzi Peter Kul-
bergi nurme pealt olli leitu ehk kinni wõetud, 16nel oktbril 1878. hommungo warra, ja keik se laade aeg kunni 17neni oktobreni peale lõunat kella kolmeni säduslikkun paigan keige selle laade rahwa silma een ülles näidatud, ja ommanikud ei olle tulnud sis om oksjoni wisil selle põhja peal ärrä müidud, et nellä näddälini wõip weel taggasi ostja käest saada, kos ostja om(m)a raha säduslikko toido raha mannu masmisega taggasi peap wõtma, ja hobbune temma ommanik kätte wõip saata, selle ülle om kohhos tälle tähhe andnu.
                                                   Peakohtomees Jakob Kulberg  XXX
                                                        Kohtomees Johan Kittus  XXX
                                                                 - " -     Jaan Pelowas  XXX
