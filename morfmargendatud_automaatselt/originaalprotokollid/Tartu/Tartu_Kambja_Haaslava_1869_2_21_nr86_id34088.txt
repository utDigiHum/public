Haslawa moisawalitsusse nimmel tulli ette A. Muddanik ning nowwap selle moisa orjaja kandimehhe Jürri Sirg kaest warsti allannimmitud wõlg kätte saada:1.,			sel aastal 1886 2 wak keswi			4 rubla		2.,			sel aastal 1886 2 wak kartuflit			1 rubla 20 Kop.		3.,			13 puuta heina eest a' 25 Kop			3 rubla 25 Kop.		4.,			sel aastal 1868 2 wak seeme kartoflit			3 rubla		5.,			sel aastal 1868 1 wak seeme keswi			3 rubla 50 Kop.		6.,			sel aastal 1868 3 wak seeme rükki			12 rubla		7.,			sel aastal 1868 11 2/3 tõpra päiwi eest a' 50 K.			5 rubla 83 1/2 Kop.		8.,			sel aastal 1868 4 5/6 jalgsi päiwi eest a' 35 K.			1 rubla 69 Kop.		9.,			sel aastal 1869 24 1/3 tüpra päiwi eest a' 50 K.			12 rubla 17 Kop.		10.			sel aastal 1869 42 1/2 jalgsi päiwi eest a' 25 K.			10 rubla 62 1/2 Kop.		Summa			57 rub. 27 Kop.		
Jürri Sirg wastutap selle moisawalitussse naudmisse pääl:
Ad 1, 2 ja 3 om eige.
Ad 4, 5 ja 6 se naudminne ollewat eige, agga temma saanud neid seeme kartoflit, keswi ja rükki tullewa aastatse taggasi andmisse pääl ja mitte rahha eest.
Ad 7,8,9 ja 10 temma ollewat kül moisale weel niipaljo päiwi wõlgo, lubbap agga neid päiwi illuste kiik ärra orjata.
Moistus: Selle Jürri Sirg warrandus, mis koggokonna kohto polest olli kinni pantu, saab mõisawalitsusse naudmisse pärrast ärra oksitud.
Kohtomees Jürri Luhha XXX
Abbi-Kohtomees Jaan Opmann XXX
Abbi-Kohtomees Märt Labber XXX
Moistus sai kuulutud ning suuremba kohtokäimisse õppetusse kirri wälja andtu.
