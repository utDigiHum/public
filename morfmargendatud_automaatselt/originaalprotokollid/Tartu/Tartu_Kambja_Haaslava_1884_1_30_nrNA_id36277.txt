Haslawa kogukonna kohus, sell 30 Januaril 1884.
Man olid:
Peakohtumees: Jaan Link
Kohtumees:
Johan Klaosen 
 Peter Iwan
Kaibas  Hendrik Jaska Haslawa mõisa luha waht et enne Talliste pühi see nädali olewat Jaan Willemson nelli mõisa kuhja ära lahkunu ja kohtumees Peeter Iwan om seda üle kaenu, tema nõuab mõisa walitsuse nimel selle lahkumise eest 20 Rbl. neid hainu om löitu tema see om Jaan Willemsoni Kambre pealt ja om mõisa heinuga ütte sündinu.
Wastutas Jaan Willemson selle kaibuse pääle, et tema ei ole üttegi mõisa kuhja lahknu ja need heinad olewad tema oma heina, mis mõisa heinuga ütte sugutse omma.
Kohtumees Peter Iwan ütles, et olnu säält küll heinu warastedu ja need sündinud küll Jaan Willemsoni heintega ütte.
Mõistetu: Et Jaan Willemson  piab 5 Rbl heinte warguse eest mõisa ja 3 Rbl waeste ladiku 2 nädali sissen ära masma, selle et tema neide kuhje lahkjais tuleb arwata, mis ka tema heintest tunda om, kuna temal endal ei ole muidu üttegi heina.
Se mõistus sai kuulutedus ja õpetus leht wälja antus.
Pea Kohtumehe asemel kohtumees : Johan Klaosen XXX
Kohtumees 
Peter Iwan /allkiri/ 
Kohtumees
 Johan Krosbärk /allkiri/
