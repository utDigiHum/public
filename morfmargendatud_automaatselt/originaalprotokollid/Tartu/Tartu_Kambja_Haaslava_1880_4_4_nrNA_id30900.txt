Haslawa kogokonna kohus 4 Aprillil 
Juures istus:
peakohtumees M. Klaos
kohtumees Joh. Udel
dito  Rein Anko
Tuli ette  Hans Ostra  Wastsest-Kuustest  ja kaebas, et tema   Jüri Adamsonile 2 wakka keswi, 1 wak linaseemnid ja  25 puuda heino lainanud ja nõudis, et see äramaksetud peaks saama.
 Jüri Adamson ette kutsutud ei salga neid asju lainanud olema, aga tema on oma poja Kusta Adamsonile toonud 9 aasta eest kui poeg peremees on olnud.
Kusta Adamson ütles kaebtuse peale, et tema mitte seda wõlga ei ole teinud kui tema isa on toonud, siis wõib tema ka massa.
Mõistus:
Et  Kusta Adamson sel lainu aeal peremees on olnud ja seda wilja ning heino oma kasuks on pruukind, peab tema 8 päewa sees 2 wakka keswi, 1 wak linaseemnid ja 25 puuda heino  Hans Ostrale tagasi maksma.
Peakohtumees  Mihkel Klaos
dito  Joh. Udel
dito  Rein Anko
Mõistus sai kohtu käijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ning õppus antud, mis tähele tuleb panna kui keegi suuremat kohut tahab käia ja ei olnud  Kusta Adamson sellega mitte rahul.
