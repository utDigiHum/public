Haslawa kogokonna kohus sel 20mal April 1879
Juures istus:
Pääkohtumees Mihkel Klaos
kohtumees Johan Link
dito Johan Udel
dito  Rein Anko
Tuli ette Haslawa mõisa walitsus ja palus kogokonna kohut, et see Haslawa karjamõisa Hiljamõisa rehe põlemine sel 6 April s.a. seletud saaks, mis läbi tule  kahju sündinud - ja selle pääle üks ära kiri protokollist wälja anda.
Sai Hiljamõisa rentnik  Alexander Nõmmitz ette kutsutud ning ütles küsimise pääle wälja, et tema on rehe man käinud, kus tema poisid linu arinud ja kui tema säält ära tulnud, on tema poisid ka linu käest ärra andma tulnud, mil ajal arwata kella 7-8 aeal rehi põlema läinud. Mis läbi tulekahju sündinud ei tea tema mitte.
Hiljamõisa sulane   Karl Mutso ütles et tema sest midagi ei tea, kus läbi tule kahju tulnud, sest sel päewal ei olewat ka mitte rehte küttetud.
Linapuhastaja  Jürri Kõrw  ütles wälja, et tema raha eest Hiljamõisas linu rabanud ja kui tema rehe mant linu käest ärra läinud andma, on rehi pärast põlema akkanud ning on temal senna sisse ärra põlenud 1 palito, 10 rubla raha ja 1 wöö.
Lina puhastaja  Jaan Utter tunnistas et tema nõnda sama ka raha eest Hiljamõisas linu rabanud ja kui  tema rehe juurest ära on läinud linu käest ära andma, on rehi pärast põlema läinud.
Sel päewal ei olle ka mitte reht küttetud. Jürri Kõrw  kui ka  Jaan Utter ei tea üttelda, mis läbi rehi põlema läinud.
Mõistus.
Seda prottokolli ülespanna ja üks ära kirri Haaslawa mõisa walitsusele anda.
Kohto lauas olliwat:
Pääkohtumees Mihkel Klaos.
Kohtumees Johan Link
dito Johan Udel
dito  Rein Anko
