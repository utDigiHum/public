Tänasel päewal oli kogukonna kohus Palla karjamõisa läinud ja seal Ludwig Sallo heaks tema ärakadunud raha tasumiseks 2 rbl. 40 kop. eest ( protokoll 16 Oktobrist 1886 N: 103 )  Märt, Mihkel ja Josep Wilka warandusest ühtekoku 4 rehast ja 1 tagumine telg 1 rbl. 80 kop. ja Mihkel Tedder nuka saag 60 kop. keelu alla pannud.
 Otsus need asjad wäljamüüia.
                   Peakohtumees J. Sarwik
                       Kohtumees J. Stamm
                        Kohtumees M. Piiri.
                         Kirjutaja G. Palm.
