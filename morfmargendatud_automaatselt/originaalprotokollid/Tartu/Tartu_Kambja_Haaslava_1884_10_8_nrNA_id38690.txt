Haslawa Kogukonna Kohus sel 8 Oktober 1884
Man ollid: 
Kohtumees:  Peter Iwan
Johan Grossberg
Peter Luha
Kaibas Roijo karjamõisa ostja Jürri Klaos,  et möda lännu Jürripäwal om Ferdinand Tomson, kes Kirrepi mõisan praegu ellab ja ene Jürripäiwa tema jures Pimarentnik ollnu, teda Roijo kõrtsin ähwartanu maha laske ja üttelnu et tema enne kaibaja ja sis essi henda püssiga maha saab laskma ja pallus et se inemine nisugutse ähwaruse est trahwita saas. Haslawa Päkohtumees Jaan Link om seda kuldnu ja ka paljo tõisi inemissi.
Ferdinand Tomson Kirrepi mõisast olli mitmekõrtse kutsumise päle seija tullnu ja wastutas selle kaibuse päle, et tema Jürri Klaost sedamodo ähwartanu ei olle temal ollewat 12 Rubl 40 Cop weel kautsioni raha Jürri Klaose käen sehen ja pallus et Klaos seda massma sunitu saas.
Jürri Klaos wastutas, et Ferdinand Tomson om keik kautsioni wälja sanu, ennegi ütte 100 Rubl protsent wõtnu tema kos arwata 10 ärra, selle et Ferdinand Tomson pidanu 100 Rubl sulla raha kautsioni sisse massma ent om 100 Rubl pabberi sisse massnu. Selletuse jaus Jürripäiwal ollnu Päkohtumees Jaan Link jures: ähwarust om temma monames Johan Parremb ka kuldnu.
Otsus: et Jaan Link ja Johan Parremb ette kutsuta om ja sai Ferdinand Thomsonile ütteldu, et tema 5 Nowembril selle kohtu ette tulleb.
Kohtumees  Peter Iwan /allkiri/
"  J. Krosbärk /allkiri/
" P. Luha /allkiri/
