Keisri Majesteedi käsu peale. 1897 aastal Mai kuu 24 päewal waatas Wallakohus  I Jurjewi Ülema Talurahwakohtu ringkonnas Palla wallamajas awalikul koosistumisel, eesistuja Jakob Reinholdi ja kohtu liikmete Gustaw Soo ja Willem Otsoni  osawõtmisel ning kirjutaja Kristjan Seppa juuresolekul Ranna walla liigete Willem ja Kustaw Kuusiku Jakobi poegade lepingu nende palwe peale ustawaks tunnistamise pärast ja leidis, et Willem ja Gustaw Kuusik on wallakohtule isiklikult  on tuttawad. 2) Lepingu tegijatel on täieline seaduslik õiguse wõimus. 3) Nende poolt ettepandud akt on tempelmaksu seaduse põhjusel kirjutatud, ja nimelt kunni 300 rbl. stempel paberi peal mis üks rubla 25 kop. maksab ning ei tõuse oma hinna poolest mitte üle 300 rubla. 4) pooled on akti sisuga täieste rahul ning on teda allakirjutanud.
                              Willem ja Gustaw Kuusik.
Sellepärast sai 1889 a. Wallakohtu sead. II jau § 278 järele määratud:
Willem ja Gustaw Jakobi pg poolt ettepandud kirjalik lepingu akt ustawaks tunnistada ja wallakohtu akti raamatusse N. all 1897 a. sissekanda, algupäraline akt aga tarwiliku pealkirjaga pooltele wälja anda.
                                   Eesistuja: J Reinhold
                                   Kohtu liikmed: G Soo.
                                                              W. Otson
                                   Kirjutaja: KSepp.
