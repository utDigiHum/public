Awwinorme Mets Koggokonna Kohtus sel 21 Deetsembril 1873.
koos ollid
pea kohtomees Maddis Tamm 
teine kohtomees Josep Reisenpuk
kolmas kohtomees Josep Kallaus.
Jürri Müür astus ette ja pallus koggonna kohhut, et temmal ommal lihhalikke lapsi ei olle, siis on temma ommale üks kasso poeg nimmega Jaan Jallak wõtnud, kes 11 Aastat wanna, ja lubbab temma sedda poisi kui omma last ülles kaswatada ning omma warra temmale pärida jätta.
Selle poisi emma Kadri Jallak tunnistas et temma omma poega Jaani, Jürri Mürile kassu pojast annud sest et temma isse lesk ja tedda kaswatada ei joua.
Moistus.
Nende omma sowwiminne saab seaduse § 952 põhja peal Prottokolli ülles, et se kindlast jääb.
Maddis Tamm XXX
Josep Reisenpuk XXX
Josep Kallaus XXX
