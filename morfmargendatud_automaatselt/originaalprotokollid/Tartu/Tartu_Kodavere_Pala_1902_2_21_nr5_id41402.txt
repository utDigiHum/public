1902 aasta Weebruari 21 päewal kinnitatud Mekkatsi mõisa rentniku Georg Lippi ja kandi koha pidaja Jakob Seppa kandi koha orjuse kontraht:
                                                                                                                                             1.
 Mekkatsi mõisawalitsus annab seda Luige mõisamaa kandikohta, kelle wäärtus on 12 tadrit 82 krossi 3 aasta peale, see on 23 aprillist 1902 kuni 23 aprillini 1905 a. Jakob Sepp kätte orjuse rendi peale.
                                                                                                                                             2.
Nimetud koha eest maksab Jakob Sepp aastas ülepea 17 rbl. 50 kop. raharenti ja nimelt:
                                         1sel aprillil     8 rbl. 75 kop. ja
                                         1 sel oktobril 8 rbl. 75 kop.
ette ära, ja peale selle on tema kohus weel järgmised osatükid teha: tema peab
                        1. 6 wakkamaad talwewilja lõikama ja kuhja panema.
                        2. 12 wakkamaad suwewilja lõikama.
                        3.  2 wakkamaad  ristikheina tegema.
                        4. 14 wakkamaad maaheina tegema
                        5. -                      wikkid niitma.
                        6.2 wakka alla kartohwlid wõtma ja peab neid ärawedama kuhu mõisawalitsus käseb.
                        7. 10 sülda puud 1 arsin pikkad  Mekkatsi mõisa wedada.
                        8. 118 ½ sülda teed teha, kui mõisawalitsus käseb, 1 süld, 7 jalga.
                        9. 90 päewa tegema.
Nende osa tükkide panemisel saab 13. Nowembril 1860 wäljaantud Liiwimaa talurahwa seaduse § 168 järele nõuetud.
                                                                                                                                          3.
Hoonete parandamise wõi ehitamise palgid ja lattid rendikoha pidaja kuni Mihkli päewani üles andma, ning peab kandikoha  pidaja ennast kõige asja poolest metsa seadmiste alla heitma; kui ta
nende seadmiste wastu eksib, kaotab tema oma koha käest.
                                                                                                                                          4. 
Selle kandikoha pidaja peab tema käes olewat kohta kõige sündsa põlluharimise seaduste järele tallitama, ja kui selle kohtrahti aeg mööda on, peab tema seda kohta järelseiswal wiisil jälle mõisale tagasi andma:
a). Kõik hooned ja nimelt nende katukse olgu heaste parandatud
b). rukki wäli heaste haritud ja õigel ajal hea seemnega külwatud;
c.) aid heaste parandatud.
d.) heinamaad hagudest puhastud ja kõik kraawid roogitud
e.) tööde poolest eesseiswa töö aasta peale peab rukki kõrs üles küntud ja ära äestatud olema;
f.) põletud puud sülda  1 arsina pikkused koha peale jätma.
                                                                                                                                          5.
 Peaks kandimehel mõisawalitsuse käsu peale uueste hoonid ehitada ehk  ka parandada olema, siis peab kandimees seda oma kuluga ilma kahju tasumise nõudmiseta tegema.
Niisuguse ehituse juure  annab mõisawalitsus ilma maksuta palgid, aga kandimehed peawad neid oma maa  ja suuruse järele ilma maksuta ehituse platsile wedama niisama ka  hulga
kaupa ilm maksuta oma maa suuruse järele, mõisawalitsuse wäljaarwamise järele, hoonete katmise õlgi andma. Karjalauda kiwi wundamendi tegemine saab mõisawalitsuse poolt ruutsülla wiisi maksetud.
                                                                                                                                          6.
 Omad põllud mõisamaa kandikoha pidaja neljas jaos tarwitama, nii et :
                                                                                                                               7½ wakka alla rukki all seisab,
                                                                                                                               15   wakka alla suwewilja all seisab,
                                                                                                                                 7½ wakka alla kesanurmen seisab.
ja ei ole temal õigust ühtegi suwe wilja kesanurmesse teha.
                                                                                                                                          7.
 Õlgi, heinu ega muud loojuste toitu ei tohi selle koha pidaja ei kellelegi wälja laenata ei ka ära müüa.
                                                                                                                                          8.
Mõisawalitsusel on õigus selle koha majapidamist igal ajal läbi waadata, ehk omast wolinikust läbi waadata lasta.
                                                                                                                                          9.
Lina wõib pidada 2 wakka alla.
                                                                                                                                          10. 
Kui mõisawalitsus tahab selle koha pidajale üles ütelda siis peab 23 oktoobril enne kontrahti aja lõpu tegema, ja selle koha pidajal ei ole õigust ei ühtegi töö wõi kulu eest tasumist nõuda.
                                                                                                                                           11.
Selle koha pidaja paneb kautsjoniks nende enese pääle wõetud tööde üle kõik oma warandust, kelle sees kõige wähem 2 hobust ja 6 suurt weist peab olema, kõik heas korras üles peetud.
                                                                                                                                           12.
 Kui tule hooletuse läbi mingit kahju sünnib wastutab tema kõige waraga sündinud kahju eest.
                                                                                                                                            13.
Koha pidaja peab iga kord, mõisa käsu pääl wälja tulema päewi tegema.
                                                                                                                                             14.
Koha pidaja ei tohi koeri lahtihoida ega hulkuda laska, kui koeri hulkumas leitakse lastakse maha.
                                                                                                                                              15.
Jätab kohapidaja mõne asja täitmata, siis kaotab tema koha ilma et tasumist nõuaks.
                                                                                                                                              16.
                                                                                    Luige rendikoha hoonete seisus on:
                                                                    1.) Elurehi                             hea.
                                                                    2.) Ait 2 polne                     hea.
                                                                    3.) 2 karjalauta ja õlestiku keskmised.
                                                                    4.) Saun                                kõwa.
                                                                    5.) Koda Kambriga               kõwa.
                                                                    6.) Kaew                                hea.
                                                                    7.) Õunaaed                        korral. 
                                                                                                                                          17.
Wilja redelid weab kantnik mõisa lisa tööl ilma maksuta kuhu tarwis on.
                                                                                                                                           18.
Koha pidaja peab tegema iga nädala kaks mehe päewa kolmapäewa ja neljapäew, iga päewa eest  mis tegemata jäeb maksab seitsekümend kop. Mõisa töö juures peab olema kantnikul omad tööriistad.
                                                                                                                                            19.
Rukkid peawad 20 augusti, kuu päewaks maha tehtud olema.
                                                                                                                                            20.
Kantnikul ei ole ilma mõisa lubata kohta tõiste kätte anda.
                                                                                                                                             22.
Et see  sinane kontraht rentnikule Jakob Sepp täieste ette loetud on saanud, ja ta kõigi nende tingimistega rahul on seda tunnistab tema oma käekiri (allkirjad) 
                                                  Esimees: J. Soiewa
                                                  Kirjutaja Sepp.   
Märkus:kaadritel 1-10 on nimede register. 
   
                              
  
