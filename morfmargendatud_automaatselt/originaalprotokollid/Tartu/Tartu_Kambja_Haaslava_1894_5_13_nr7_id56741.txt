№ 7.
Haaslawa  14 Wallakohus 13 Mail 1894 a.
Juures olid  Eesistuja Johan Hansen
Juuresistuja  Jaan Tekkel 
Johan Klaos
Karl Lesta 
 Kristjan Undritz  Sootaga walla liige, elab Haaslawal, selle walla Kohtole tundud palelikult tundud, annab üles, et  Jaan Roiland  Haaslawalt temale wõlgneb 36 rbl. mida summa lubab 15 Oktobril 1894 aastal äramaksta.
Jaan Roiland tunistab õigeks et 36 rbl.  Kr. Undritzele wõlgu on mis eelnimetatud terminil lubab ära maksa.
 Jaan Roiland /allkiri/   Kristjan Undritz  /allkiri/
Otsus: See eelnimetatud leping sellega heaks tunistada. 
Eesistuja: J. Hansen /allkiri/
Juuresistuja: J. Tekkel /allkiri/
"  K. Lesta /allkiri/
" J. Klaos /allkiri/
Kirjutaja: K. Laar /allkiri/
