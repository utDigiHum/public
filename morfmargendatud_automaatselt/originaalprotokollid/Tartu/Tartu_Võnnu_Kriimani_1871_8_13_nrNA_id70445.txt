Krimanis sel 13mal August 1871.
 Willem Laetti  tulli  sel 23mal Julil kohto ette ja kaebas: minna osti  Vogli  käest püssi ja müid wõttis Kihhelkonna kohhus püssi minno käest ärra sest minnol tulli se püss 18rubla maksa.
Se peale ütles koggokonna kohhus sinna olled jo Kihhelkonna kohtust 5 rubla sanud, kui sa sanud olled siis ei olle sul mitte üttegi õigust nõuda.
 Heinrich Vogel tulli ette ja ütles: minna ei olle sedda püssi mitte kustkilt sanud, sest minna ollen se püss löitnu lombist ja  Willem Laettile 5 rubla eest ärra müinud.
Mõistus: Krimani koggokonna kohtus sel 13mal August 1871.
Koggokonna kohhus mõistis: et   Willem Laetti  5 rubla Kihhelkonna kohtus sanud ja se rahha siis kätte sanud mis Willem Vogelile maksnud peab obis rahho jäma, et tal Heinrich Vogel käest õigust middagi nõuda ei olle.
Man olliwa:
Peakohtomees Johan Mina XXX
Abbi "  Karel Poedderson  XXX
Abbi "  Jaan Poedderson  XXX
Kirjutaja W. Kogi /allkiri/
