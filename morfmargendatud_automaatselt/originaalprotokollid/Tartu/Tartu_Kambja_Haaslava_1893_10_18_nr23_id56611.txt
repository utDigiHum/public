№ 23.
Haaslawa  14 Wallakohus 18 Oktobril 1893 a.
Juures olid  Eesistuja Jaan Treijal
Juuresistuja  K. Ango
"  P. Iwan 
 Ette tuli Haaslawa walla liige  Johan Zobel ja andis üles, et   Wastse Kuuste walla liige  Peter Reinberg tema eest üleskirjutatud pristawist hobuse eest 25 rbl., lehma eest 20 rbl 1 sea eest 10 rbl., 3 lamba eest 6 rbl. ühte summa 61 rbl. /kuuskümmend üks rbl./ ära on maksnud mis summa temma Johan Zobeli kätte wõlgu jääb.
 Johan Zobel ei mõista kirjutada, tema eest kirjutas tema palwe pääle Daniel Koosapoig /allkiri/
Ette tuli Wastse Kuuste walla liige Peter Reinberg ja andis üles, et see eelnimetatud raha summa 61 rbl. tema Johan Zoobeli eest ära maksnud on, mis summa Zobel temale wõlgu jääb.
 Peter Reinberg ei mõista kirjutada, tema pallwe pääle kirjutas alla  August Link  /allkiri/
Et eelnimetatud inimesed selle walla Kohtule tuntud on, saab eel pool üles antud Kohtu poolt kinnitatud.
Eesistuja J. Treijal /allkiri/
Juuresistuja  Peter Iwan /allkiri/
"  Karli Anngo /allkiri/
Kirjutaja:  K. Laar /allkiri/
