Awwinorme Metswallakohtos sel 25 Julil 1875.
koos ollid
peakohtumees Josep Kask
teine kohtumees Juhhan Koppel
kolmas kohtumees Jaan Mänd
Jakob Tamm kaebas et temma sullane Jaan Part temma sanna ei kuule ning keik omma tahtmist teeb, ja peab pääwad otsa maggama, ning kui temma on Jaan Parti hobbust weema saatnud on temma keik pääw ärra jänud ja alles teisel hommikul koddo tulnud.
Jaan Part räkis et temma millagi ei olle Jakob Tammele wastu hakkanud ja kui temma on hobbost weema lähnud on temma haige olnud, ning ei olle selle pärrast koddo lähnud, ka kaebas Jaan Part et Jakob Tamm peab tedda täitanust ja litse pojast sõimanud.
Jakob Tamm tunnistanud et Jaan Part on kroppist räkinod ja on temma siis sedda wiisi Jaan Partile ütlenud.
Moistus
Koggokonnakohhos moistab et Jaan Part ilma omma perremehhe lubbata öesiti ärra käib ja töeaega widab. siis saab temma selle eest 15 witsa löögiga trahwitud. ja et Jakob Tamm issi tunnistab et temma on Jaan Partile litsepoeg ütlenud ning temmale temma eksinud sündiminne ette wissanud siis maksab temma selle eest 2 Rubl. trahwi walla laeka.
Josep Kask XXX
Juhhan Koppel XXX
Jaan Mänd XXX
Et Jakob Tamm ei olle se moistetud trahw 2 Rubl ärra maksnud. siis on se trahw ümber poratud sanud. ja istub temma 24 tundi kinni.
