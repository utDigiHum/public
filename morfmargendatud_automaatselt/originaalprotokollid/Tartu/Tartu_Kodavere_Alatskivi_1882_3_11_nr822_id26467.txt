Alexei Dolgoschew kaebab Josep Saar, Mardi poea peale, se heitnud suwwe peale tööle 90. rubla hinna al saanud 43 rubla kätte, ja polle mitte ühte paewa teeninud, nõuab nüüd doppelt rahha 86. rublaga.
Josep Saar tunnistab kaebduse tõeks, on walmis 42. rubla. 25 kp taggasi maksmas, agga ei olle rahha., pallub et tännawo aasta teenistuse wõttaks
Kohhus moistis:
On Josep Saar suwwe peale kauba tehnud ja 42. rubla 25. kp. rahha wälja wõtnud, ja ommetigi mitte paewa teeninud, siis moistis kohhus, et Josep Saar peab doppelt kässirahha 84 rubla 50. koppikad Alexei Dolgoschewile wäljamaksma ja nimmelt
rukki Maarja paewal 40 rubla -
Mihklepaewal 1882 44. - 50.
ja saab peale selle 2x24 tunni peale koggukonna wangihoone kinnipandut, et temma meelega kohtu käskude wasto on pannud,
/: Otsus kulutud sel 11 Martsil c. ja oppuse kirjad wäljaantud :/
A. Krimm [allkiri], pea
Maddis Erm, khtm
A. Pärn [allkiri], -"-
Jaan Piiri, -"-
