Protokoll № 1
Haaslawa wallakohus Haaslawa wallamajas 11 Januaril 1902 a.
Juures Eesistuja:  Johan Koort 
Kohtu liige: Johan Klaos
Tuliwad ette Haaslawa walla  liige  Johan Hansu p. Tekkel, elab  Haaslawa wallas ja  Jaan Hendrike p. Otti   Mäksa walla liige, elab Haaslawa  wallas, paluwad nende wahel tänasel päewal maha tehtud lepingut selle kohtu lepingute raamatusse maha kirjutada ja ustawaks tunnistada.
Johan Tekkel müüb priitahtlikult need oma liikuwa waranduse tükid, nimelt: üks riide kapp, poleeritud, neli sulgedest pääaluse patja, kaks lammast, 15 punti masindatud linu, kollakat karwa poolwillane undruk ja jaki, willane rohiline undruk, must poolwillane undruk, pruun poolwillane undruk, kaks meeste hamet, kuus woodi lina ja pruunikas poolwillane jaki ühes undrukuga , mis 9 Januari kuu päewal s.a. Eduard Klaose nõudmise pääle selle wallakohtu poolt 9 Märtsil 1901 aastal № 88 all wälja antud täitmise lehe järele aresti alla pantud nimetatud Klaose wõla ja täitmise kuludeks - Jaan Ottile 37 rub. 88 kop. eest ära.
Jaan Otti maksis Tekkelile puhtas rahas kolmkümmend seitse rub. 88 kop. wälja.
Johan Tekkel seda raha 37 rub. 88 kop wastu wõttes tunnistab ülemal nimetatud asjad Jaan Otti omaks ning maksab saadud raha 37 rub. 88 kop selle walla kohtu kätte Eduard Klaose wõla ja nõudmise kulude tasumiseks. (Waata kohtu raharaamat № 22 s.a.)
Johan Tekkel /allkiri/
Jaan Otti /allkiri/
1902 aasta Januari kuu 11 päewal  Haaslawa wallakohus selle läbi tunnistab, et mõlemad lepingu tegijad Johan Tekkel ja Jaan Otti, kes Haaslawa wallas elamas ja  selle kohtule isiklikult tuttawad, selle lepingu suusõnal ette kannud on, mõlemad oma käega selle lepingu alla kirjutanud ja et mõlematel seaduslik õigus on lepingu tegemiseks. 
Kohtu eesistuja:  J. Koort /allkiri/
Kohtu liige: Jogan Klaos /allkiri/
Kirjutaja: J. Wäljaots /allkiri/
