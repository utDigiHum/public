Otto Otsa Pallalt ütleb oma wenna Josep Otsale üles et ta peab 23 prillil 1884 tema juurest ära minema ja need maad mis tema käes olid pruukita temale tagasi andma.
Selle kinnituseks on ennast alla kirjutanud
                      pääkohtumees Hindrek Horn
                      Kohtumees Josep Soiewa
                      Kohtumees Otto Mõisa.
