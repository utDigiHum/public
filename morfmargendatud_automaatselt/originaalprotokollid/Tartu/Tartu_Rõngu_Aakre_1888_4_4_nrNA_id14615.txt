Tartu Keiserliku III. Kihelkonnakohtu poolt saab seega kohtu petsatid allapannes ja nööri petsatiga kinni pannes-tunnistud, et selles Aakre kogukonnakohtu protokollide raamatus- üks sada nelikümmend neli(144) nummerdatud lehte on.
Suure Rõngus, III. Tartu Kihelkonna Kohtus,
4. aprillil Märtsil (mahatõmmatud)1888.
Kihelkonnakohtu härra abi: E. v. Lilienfeldt(allkiri)
Notaire: loetamatu allkiri
