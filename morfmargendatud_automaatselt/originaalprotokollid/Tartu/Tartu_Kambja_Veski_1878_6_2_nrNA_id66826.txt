                            Sel 2. junil 1878
                                                  Olliwa man Päkohtomehe abbi Johan Kittus
                                                                      Kohtomees. Jaan Pelowas
                                                                    abbi     do       Joh Androwitz
Krüidnerist Marri Warn, kaibap et Jakob Kulberg Weskimõisa wallast om temmaga ütte ellanu, möda lännu aasta 1876.a Orrike ladest sanik ehk enne latet üts näddal se on 7tast oktobrist 1876. Tõne lats sündi enne Talliste pühhi 3 näddalit ja 3. päiwa nimelt nelläpäiwal 1sel detzbril 1877. Eddimatse latse sündimine olli nustako lade päiwal 1876.a olli kalu sündinu.
Minna nõwwa latse toitmise rahha 50. rbl. Selle tõise latse ajal kokko ellamist ei tija keagi, aggä eddimatse latse ajal kokko ellämist tijawa. Märt Pokker, ja tütruk Krüdnerist Ann Jawer, se lats om pois lats, ja om Jakob Jakob Kulbergi päle ristitu, Eddimäne kokko ellamine sai tõise latse aigu allustut, enne Talliste pühhi 3. näddälit  se om 4da detzbrist, 1876, päle se käis ta egga kõrd kos ta ennegi kokko sai seni kui süggiseni.
Jakob Kulberg üttel se Marri Warn kaibuse kik puhhas walle ollewat ja üttel et temma sest ütte ellamisest middagi ei tija.
Kohhos mõistis se Marri Warn kaibuse asja tühjas. 
Marri Warn ei olle sellega rahhol, ja pallup prottokol
                          Sai kaebus leht wälja antud, 8tal junil.
Jakob Kulberg pallub 8. päiwa mõtlemise aiga.
                                                 Päkohtomehe abbi Joh. Kittus  XXX
                                                 Kohtomees -   Jaan Pellowas  XXX
                                                abbi    -           Johan Androwitz  XXX
