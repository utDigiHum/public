sel 28 Oktobril 1874.
koos ollid.
pea kohtomees Josep Kask
teine kohtomees Juhan Koppel
kolmas kohtomees Maddis Sildnik
Astusid ette ärra surnud Kärrasi külla perremehe Jürri Õuna pojad, Andres, Maddis ja Karel Õun ja pallusid oma issa järrele jänud warrandust Prottokolli ülles wõtta mis on nende pärrida jänud.
2 hobost wäärt 100 Rubla.
2 härga " 60 -
2 pullikest " 20 -
5 lehma " 75 -
8 sigga " 56 -
1 wanker " 15 -
2 wassikad " 6 -
puhhast rahha 235 -
Summa 567 Rubla.
Selle warra pärriad on Emma Ann, poeg Andres poeg Maddis, poeg Karel, tüttar Leno, ja tüttar Kai, ja saab selle järrele igga pärrijalle 94 Rubla 50 koppikud mis, warra agga keik nüüd wannema poia Andrese käes on, ja temma igga ühhele lahkudes peab wälja maksma.
Josep Kask XXX
Juhan Koppel XXX
Maddis Sildnik XXX.
