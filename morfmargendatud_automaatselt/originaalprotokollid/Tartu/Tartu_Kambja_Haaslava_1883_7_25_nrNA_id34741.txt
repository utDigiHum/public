Sel 25 Julil 1883
Johan Järgawitz om käsustado Päraha wõlgu mittme käsu päle ei olle Johan Järgawitz seda Päraha masson ei ka passi tonu sest Keiserliko 4ta Tarto Kihelkonna Kohtu poolt om jubba kolmas käsk selle inemisse perrast tullnu ja 2 Rubl trahwi om massa.
Mõistetu: et Johan Järgawitz 2 Rubl Kihelkonna Kohtu trahwi raha massma peab ja om 15 witsa lögiga trahwito selle wastopanemise perrast.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees: Jaan Link /allkiri/
Kohtumees:  Jaan Klaosen XXX
Peter Iwan /allkiri/
J. Krosbärk  /allkiri/
