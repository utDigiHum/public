1900 aasta Jaanuari 13 päewal kinnitatud Ranna wallast Juhan Kaarli pg. Jürgensoni suusõnaline testament.
                                                                                                                     Testament.
Mina Juhan Kaarli pg. Jürgenson luban oma ostetud pool Peedu N: 16, mis Ranna mõisa järele Kodawere kihelkonnas Jurjewi kreisis pärast oma surma  ja oma naise  Anne sünd Kittask surma
 oma sõsara Liisa tütre Anne tütrel  Liisa Willemi t. Annukale päranduseks kõige õigustega ja kohustusega, mis selle taluga ühes on, ühes kõige liikuwa warandusega mis siis  talus olema saab, just nagu sellkorral talu seisu kord on. Selle juures Liisa kohta tingimisi tehes, et tema Liisa Annuk omale selle talusse wõib mehe sisse wõtta, aga iialgi mitte talust wälja mehele minna, selle kostusega, et siis kõik see lubamine tühiseks tehakse ning seaduslised pärijad kõik päranduse omale saawad. Kui aga enne tema surma mehe koju wõtab, siis peawad Liisa ja tema mees, tema , päranduse
jätja sõna kuulma ja nende wastu laste kohusid wanemate wasta tähele panema.
 Juhan Jürgenson ei oska kirja. Tema palwe pääle kirjutas alla Karel Weskimets.
                                                 Kohtu eesistuja J. Soiewa
                                                 Kirjutaja Sepp
      
