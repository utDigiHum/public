Kadri Rookla kaibab: "Karl Träss olewa temaga lihalikult ühes kous elanud, kellest temale laps saanud, aga nüüd ei tahtwa tema last toita aitada."
Karl Trass wastab sellepääle: "Tema olewa Kadriga kül lihalikult ühes kous olnud, aga maksnud igakõrd raha wälja, mis tüdruk lepinud, kas 20, 25 ehk 50 kop., ühtekokko 5 rubla. Ka olewa tüdrukul mitma tõisega tegemist olnud.
Mõistetud: Karl Träss maksab Kadri Rooklale 5 rubla h. ja saab 25 lööki.
