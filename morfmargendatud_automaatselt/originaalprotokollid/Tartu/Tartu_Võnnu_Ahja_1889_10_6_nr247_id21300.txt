Keiserliku Tartu Kreiskohtu eeskirja põhjusel 1 Septembrist s.a. No 3618 sai Sulbi talu wõlglaste seast mis kog. protokollis 1 Märtsist 1888 aastast No 33 üles tähentud on ning neilt küsitud Sulbi talu müümise pärast; nemad selle müimist ka soowiwad. Wõlglaste seast oli tulnud, Juhan Kaan, Eduardt Küün Liisa Matsoon, Johan Saadik ja köster Donner, kes kõik oma wõlgasid kätte saada nõudsid ja kui mitte müidu, siis Sulbi talu awaliku ära müimise läbi Kreiskohtu poolt.
Tulematta oli jäänud: Kaupmees Knochenstirn, Juhan Neumann, Dawit Mark, J. Laan ja mõisa walitsus.
Otsus: Seda Keiserliku Tartu Kreiskohtule teadustada ja tulematta jäänud wõlglased Knochenstirn, Juhan Neumann, Dawit Mark ja J. Laan ning mõisawalitsus 27 Octobriks s.a. uuesti ette tallitada.
Peakohtumees: Kotta Piir &lt;allkiri&gt;
Kohtumees: P. Pedusoon XXX
Kohtumees: Joh. Rootslane XXX
Kohtumees: Kusta Saarwa &lt;allkiri&gt;
Kohtumees: Johan Wastalo &lt;allkiri&gt;
Kohtumees: Hindrik Tootsi &lt;allkiri&gt;
Kusta Naggel &lt;allkiri&gt;
Kirjutaja as. K. Weske &lt;allkiri&gt;
rewidist sel 20 Oktobril 89
K.k.k ??
