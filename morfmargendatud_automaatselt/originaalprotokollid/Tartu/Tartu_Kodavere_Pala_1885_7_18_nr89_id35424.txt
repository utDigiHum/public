Tuli ette Piiri kõrtsimees Josep Nõmm ja kaebas et Karel Mägi tema naist  laimanud ja teotanud ja nimelt et Piiri kõrtsinaine olnu Willem Tõntsu elu rikkuja.   Palus kohut ülekuulata kui kaugeni see ütlemine Karl Mägi poolt, õige on. Tunnistajad seal juures olnud Palla mõisa herra von Stryk.
Ette kutsutud metsawaht Karel Mäggi ja wastas küsimise peale, et tema need sõnad mitte ütelnud ei ole.
Otsus Palla mõisa herra von Stryki paluda selle asja sees tunnistust anda.
                                                      Kohtumees J. Soiewa.
                                                      Kohtumees O. Mõisa.
                                                      Kohtumees M. Mölder.
                                                  Kirjutaja allkiri.
