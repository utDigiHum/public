XXI Awwinorme Wallakohtus sel. 7 Mail 1897.
koos olid
kohtueesistuja Mihkel Pihlak
kohtumees Andres Kalaus
kohtumees Josep Ambos
Tuli kohtu ette Awwinorme Metswalla inimene Andres Raja Jaani poeg Adrakust ja palus Wallakohut seda Akti ramatuse üles kirjutada et tema on Marie Kasikulle 25 Rubl lehma ostmise tarwis laenanud. selle peale et Marie Kasik temale se raha eddespidi peab ära maksma kui tema on raha teninud. ja et Marie Kasik selle wõla ette oma pärantuse rahaga wastab mis temal intressi peal on. ja wallakohtu hoiju all seisab Andres Raia (allkiri) 
Marie Kasik kes 19 Aastad wana räkis oma Wölmöndri Jaan Kasiku juures olemisel et se tõsi et tema on oma tädimehe Andres Raja käest 25 Rubla lehma ostmise tarwis laenanud ja lubab tema se raha nenda pea kui tema on tenistuse läbi raha sanud se wõlg Andres Rajale ara maksa ja wastab tema selle wõla ette oma pärantuse rahaga mis temal Wallakohtu juures hoida on mis lubamisega Wöölmönder Jaan Kasik rahul oli.
Marie Kaasik (allkiri) 
Jaan Kasik ei moista kirjutada
1897 Aastal Maikuu 7 pääwal on se eesseisaw susanna leping kinnitamiseks XXI Awwinorme Wallakohtule I Jürjewi Ülema talurahwakohtu ringkonnas Awwinorme Wallakohtumajas kinnitamiseks ette kantud Awwinorme  Walla ja Adraku küla inimese Andres Raja Jaani poja ja Awwinorme Walla inimese Enniksaare külas elustawa Marie Kasiku Juhani tüttre poolest 25 Rubl laenamise pärast ja on nende lepping tänasel pääwal Akti ramatuse N 11 all üleskirjutud ja on kaubategijad omad nimed oma käega alla kirjutanud ja kohus nende leppingud kinnitanud 
M. Pihlak. (allkiri)
A Kalaus (allkiri)
J Ambus (allkiri)
Kirjutaja Schulbach (allkiri)
