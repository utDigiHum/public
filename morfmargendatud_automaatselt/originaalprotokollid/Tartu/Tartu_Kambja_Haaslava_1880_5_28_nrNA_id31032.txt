Haslawa kogokonna kohus 28 Mail  1880
Juures istus:
kohtumees  Joh. Link
dito Joh. Udel
dito  Rein Anko
Tuli ette  Karl Laber  ja kaebas, et temal ühe aasta pearaha  Johan Zobeli käest saada 5 rubl. 35 Kop. ja nõudis, et see  maksetud saaks.
 Joh. Zobel  ette kutsutud salgas ja ütles et temal mitte pearaha kaubeldud ei olnud Karl Laberile.
Joh. Zoo tunnistas; et Johan Zobel kauba tegemise juures Karl Laberi pearaha pidanud maksma.
Mõistus.
Johan Zobel peab 8 pääwa sees  Karl Laberi eest 5 rubl. 35 Kop. wälja masma.
Kohtumees Johan Link
dito Johan Udel
dito Rein Anko
Mõistus sai kohto käijatele T.S.R. pärra a 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi suuremad kohut tahab käija ja ei olnud Joh. Zobel mitte sellega rahul. 
