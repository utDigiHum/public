Sel 12 Junil 1889
Selle otsuse päle sest 6 Märzist Mihkel Rehessaare kaibuse asjan wastu  Johan Kalpus  olli  Ado Kalpus Ropkast tullnu ja tunistas, et tema 1½ wakka rükki jahu, 1½ wakka keswi, 6 naela raswa ja 10 naela liha  Rehessaarele om wienu se ollnu kaubeltu. Kartoflid lubanu Johan Kalpus  tõine kõrd ärra wieja. 
 Mina Roiman  Haslawast tunistas, et minewa kewade lõpnu Kalpusel üts põrs ärra, se ollnu essi üts haigus ja ei olle mitte karja tüdruku süi läbbi surno.
Ann Kalpus, kes Tarto linnan Karlowa ulizen Nr 56 ellab tunistas, et tema kauba tegemise jures ei olle ollnu, ent karja tüdrukese emma ollewat essi kõnnelnu, et 1½ wakka rükki ja 1½ wakka keswi palkas kaubeltu ollnu.
Mõistetu: et  Johan Kalpus weel 2 wakka kartoflit ja 1 nael willo ehk 50 kop  Mihkel Rehessarele 8 päiwa sehen ärra peab massma.
Se mõistus sai  § 773 ja 774 perra Mihkel Rehessarele kulutedo ja õpetus leht wälja antu. Sel 3 Julil mõistus § 773 ja 774 perra Johan Kalpusele kulutedo ja õpetus leht wälja antu.
Päkohtumees:  Jaan Wirro XXX
Kohtumees:   Jaan Pruks  /allkiri/
"  : Jakob Saks  XXX
