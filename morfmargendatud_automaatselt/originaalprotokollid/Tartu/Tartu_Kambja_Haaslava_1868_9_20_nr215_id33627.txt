Se mõtsawaht Jaan Kroomann kaibap, et se Ann Labber warrastanut kassu mõtsast noore leppa ladwa, temma pandistanut rätti nink nowwap kahjotassomist 1 rubla 50 kop. hõbd.
Ann Labber wastutap Jaan Müüri eestkostmisen, et temma toonut neid leppa lehte Seppa Märti lompist ja mitte moisa mõtsast. Se mõtsawaht Jaan Kroomann ei ollewat tedda ka mitte mõtsast kinni wotnut, enge temma enda koddu mant ja pesnut tedda hirmsaste nenda, et temma parhilla weel eige aige om. Eddimelt löönut Jaan Kroomann tedda mahha ja siis olnut põlwega temma rindu pääl. Liis Wahher ollewat ka sedda pesmist näinut.
Jaan Kroomann üttel, et temma ei olle mitte Ann Labberit pesnut, temma wotnut muud kui rätt ärra ja Liis Wahherit ei ollewat temma ka mitte näinut.
Tul ette Liis Wahher, kis ellap Tarto linnas karja ullitsen, Willem Wuksi majas nink tunnist, et temma kuulnut üts weiga suur tännitaminne, temma läinut siis tollepääl liggidal ja näinut et se mötsawaht Jaan Kroomann tahtnut Ann Labberi kaest suurt rätti ärra wotta, Ann Labber ei olle agga lasknut, enge tännitanut ja pandnut wasto, siis löönut tollepääl se mõtsawaht sedda Ann Labberit mahha ja wotnut weikse rätti ärra.
Moistus: Se Ann Labber om kurja päält kinni woetu nink om wasto pandnut pandistamissel, ja ei woi enda man keddagi löömisse märki ülles näidata, siis piap temma pandirahha ütte weikse rätti est 50 kop. mõtsa kassa hääs sisse masma.
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohto käimisse õppetusse kirri wälja andtu.
