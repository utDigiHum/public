Jakob Jürgenson wastu Hans Kirs. Ette tulli Hans Kirs ja andis ülles nende kaup olnud 40. rubl. ja olla see tema käest saanud:
1. puhast raha			6 rubl.		2. kaupa raha			23 rubl. 22 kop.		3. haige olnud 6 nädalat 			39 rubl.		4. kolm päwa wiita			 3 rubl.		5. Kosti raha ära jäänud			 3 rubl. 80 kop.		                             Summa			75 rubla. 2 kop.		
ja arwab, et temal see järgi kaibaja käest 35 rubla 2 Kop. saada.
 Selle peale wastas Jakob Jürgenson tema polle mitte perremehe ajast haige olnud waid ennegi peale selle oma  koddus ja et tema oma rahaga ostja olnud siis  on need kässi rahad mis ära  jänud
tema kahjo olnud, see ära wiitetu 3 päwa olla tema peremehe loaga pulmas olnud.
 Tomas Welt tunnistas Jakob Jürgenson on kül kewade poolt talwe haige olnud ja mitte haiguse perast  wäljas sõita saanud.
 Lena Tralla tunnistas Jakob olla enne jõulu wäljas sõitnud aga peale Jõulu olla see tihti haige olnud.
 Mõlemad kohtu käiad andsid ülles et nende tenistuse kaup on olnud Mihkli päwast Jürri päwani.
 Kohtu osus: Et kaibaja ja kaibatu  wahel ühegi tunnistust nende palga suruse  ülle ei olle siis mõistis kohus, et 40 rubla 7. kuu tenistuse eest kül on, ja et kaupmees tõeks on teinud et, kaibaja tenistuse ajal haige on olnud siis mõistab kohus maksab tema nende haiguse läbi 6 ära  wiidetud aja eest peremehele 8 rubl 25 kop. poest kaupa eest 23 rubl 22 kop. palka Kirsi ütteluse perra 6 rubl. Kässi rahad mis ära jäid 3 rubl. 80 kop. Summa 41 rubl. 27 kop.
Aga et, kaibaja 3 päewa  sõsara pulmas on wiitnud ei wõi perremees neid nõuda. Ja peab see järgi kaibaja 1 rubl 27 kop. Kirsile tagasi maksma. See mõistus sai kohtu käiattelle  appellationi sädusega ette loetu ja et kaibaja rahul ei olnud sai temale appellation wälja antu.
                     pä Kohtumees: Hindrek Horn
                           Kohtumees: Märt Piiri
                           Kohtumees: Josep Soiewa.
