Puurmannis sel 18. Octobril 1874.
Koos olliwad: Peakohtumees: Hans Perre
kõrwalistja Tõnnu Bauer
abbi Karel Adolf.
Sel 22 Novembril 1873 N 56 Proto koll sai tänna kohtukäiattele ette loetud ning Ewert Land nouab, et tema kaibuse jure peab weel kirjutud sama Adu Kisar olla siin kohtu jures ölnud ma lei tedda.
Ning annab weel tunnistajaks ülles Jürri Kütt.
Ette kutsutud Jürri Kütt ja wastas tema polle neid näinud egga ei tea middagi tee kinnipiddamisest.
Ette kutsutud Adu Kisar ja wastab. Tema olla kül Landi ja Jaani mollemid lukkanud omma hobboste eest ärra ning Land olla teed kinni piddanud 31/2 tunni ni et tema ennast wägga ärra külmetanud arwata 6 werst peal.
Ette kutsutud Mihkel Ott.
se wastas se on kül õige, et tema Jaan Rätseppa mütsi peast on ärra wõtnud, ning jälle taggasi wankri wiskanud agga se polle tõssi, et Jaan Ratsep on kussinud: kas tõt woi nalja teed, ehk tedda seppaks nimetand waid üttelnud temale Landman.
Ette kutsutud Mihkel Gros. se wastas: See on puhhas walle, tema polle selle te peal ei Landi ei ka Jaani silmaga näinud. tema polle keddagi innimest ähwardanud waid ennegi Mõreda mõtsa wahhel olla tema oma julguseks üttelnud: üks püssi pauk ja 3 meest püssi polle temal olnud
Land noub oma lögi eest 50 Rbl.
Jaan Ratsep nouab oma mütsiwotmise eest Seppa käest 50 Rubl. ning rentniku kaest ähward.
Kohtuotsus.
Adu Kisar peab selle lükkamise eest Landile 5 Rubl. maksma.
Mihkel Ott peab, et tema Jaan Rätseppa peast mutsi on visanud Jaanile se häbbiteggemise eest 5 Rubl. maksma
Mihkel Groos et tema nisuggust ahvardust on räkinud 3 Rubl. walla ladiku maksma. walja antud sel 22 Oct.
Hans Perre XXX
Tõnnu Bauer XXX
Karel Adolf XXX
Jaan Ratsep ja Land polle mitte se otsusega rahhul. Land kaibp et tema wastused polle kolm kord järgi möda sia tulnd.
