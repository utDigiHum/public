Jaan Looga ei ole kutsumise pääle kohtu ette tulnud ja ka wabandamata jätnud.
Mõiste: Jaan Looga maksab 30 kop. kogukonna kohtu liikmete hääks 8 päiwa sees wälja.
Kohtumees:  J. Ottan XXX
Kohtumees: P. Pedoson
Mõiste sai Jaan Loogale kuulutatud T.S.R. §772-774 järele aastast 1860 ja õpus antud mis tähele panda tuleb kui keegi kõrgemat kohut nõuab ning oli Jaan Looga rahul.
Kirjutaja: Chr Kapp &lt;allkiri&gt;
