Järgmine suusõnaline lepping sai üleskirjutatud ja kinnitatud.
Kusta Raudsepp jätab see wõla raha mis temal August Kuusiku käest saada August Kuusiku kätte selle tingimisega et Kuusik temale tänasest päewast pääle arwatud kahe aasta sees seitsekümmend wiis rubl. ära ja wiimase aasta eest wiis protsenti aga endiste aastade eest protsenti ei nõua selle eest jääb üks ait ja küün mis ühe katuse all Raudseppa omanduseks ja wõib Raudsepp teda sellele müia kellele tahab. - maha kustutud "rubla" ei tule lugeda.
A. Kusik &lt;allkiri&gt; K. Rautsepp &lt;allkiri&gt;
Et Kohus kokkulepijaid pooli selgeste tunneb ja et nemad mõle ise oma käega on allakirjutanud saab seega tõeks tunnistada.
Eesistuja J Willipson &lt;allkiri&gt;
Kirjutaja: Puusepp &lt;allkiri&gt;
