1897 aasta 24 aprilli kuu päewal kinnitatud Ranna walla talupoegade Joosep Nõmme ja Kaarel Nõmme wahel järgmine suusõnaline leping:
,, Joosep Nõmm annab Kaarel Nõmmele luba oma Wolodi metsa heinamaa peale maja ehitada. Milleks Kaarel pool wakamaad saab tarwitada, ja mille eest Kaarel iga aasta kolm rubla renti maksab, iga aasta Jaani päewal. Aega ei määrata.``
                           Kohtu eesistuja:
                                    Kirjutaja KSepp.
