Tulid ette Kaarel Soiewa ja Pala mõisaomaniku wolinik Nemwalz paludes nende  poolt kokkulepitud Kingseppa N: XXVII mõisa maakoha pidamise kontrahti ära kinnitada. On isiklikult tuttawad ja nende lepingu tegemise õigus pole kitsendud. Karel Soiewa ei oska kirja, tema palwel K Welt
                                                                                                                                                        C Nemwalz
Kohus otsustab: ettepandud kontraht ustawaks tunistada.
Esimees K. Wahi.
Liikmed J Otter J Uusen
Kirjutaja Brükkel. 
Märkus: Koopia.                                                                                                                                                
