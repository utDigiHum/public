Awwinorme Mets kogokona kohtus sel 20 Septembril 1874.
koos ollid
pea kohtomees Josep Kask
teine kohtomees Juhan Koppel
kolmas kohtomees Maddis Sildnik
Salla kogokona kohto nõudmisse peale sel 17 Septembril No 21. Tallinamaal, andis tännasel kohto päwal Josep Welt sedda tunistust, et Johanes Grüüntal on Gustaw Tebbeda ommale tenistusse kaubelnud, kongseppa töö peale ja lubbanud palka 55 Rubla Aastas, ning üttelnud et Gustaw Tebbe ei prugi wällis tööd teha wait aga kingseppa tööd sest et temal ommal sullased on põllo ja wällis töö tarwis.
Leonahrt Welt tunnistas nenda sammoti et Johannes Grünthal on Adrako tulnud ja Gustaw Tebbeda ommale tenima palkanud ja olnud palk 55 Rubla Aastas, ja et Gustaw Tebbe muud tööd ei prugi teha kui kingseppa tööd.
Moistus.
Se Prottokol saab Salla kogokona kohto sisse satetud, sest et selle walla innimessed Josep ja Leonhart Welt ei woi kauguse pärrast sinna minna tunnistust Andma.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
