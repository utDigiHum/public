Halliku mõisa pärishärra kirja täitmiseks 12. Oktobrist 1892 a. N: 37 olid kohtu ette kutsutud Halliku walla Krongi N:34 talu rentnikud Jakob ja Kusta Laumets ja nendele nim. kiri kui ka Halliku mõisa pärishärra
ja Johan Waddi wahel tehtud eeskontraht ja päris kaubakonttraht etteloetud ja nendele säädus ostmise eesõiguse kohta ära seletatud.
                                                     Jakob Laumets Kustaw Laumets
Otsus: seda, kui sündinud , ülestähentada.
                                                Kohtu eesistuja Abram Saar
                                                Kohtumees        Joosep Hanst
                                                Kohtumees       Jakob Reinman
                                                Kirjutaja            O. Seidenbach.
                                       
                                 
                                        
                              
