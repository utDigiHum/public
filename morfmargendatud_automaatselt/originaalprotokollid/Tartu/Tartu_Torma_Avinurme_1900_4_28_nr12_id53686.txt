Protokoll
Awwinorme Wallakohtus sel 28 Aprilil 1900.
juures olid
Eesistuja asemel Mihkel Toming
kohtumes Jaan Laud.
Abikohtumes Mart Kask
Tulid ette Awwinorme Walla inimene Jaan Saar Abrami poeg ja Wottigwerre walla inimene Abram Poom Jaani poeg ja palusid Wallakohut nende kaupa ja leppingud kinnitada mis nemad eneste wahel tehnud.
Abram Poom Jaani poeg kes oma krunti pidamise õiguse. mis Awwinorme Wallas ja Adraku külas talu N 273. oma wäimehe Jaan Saarele ära müinud ning omale üks tükk maad ja ükks wana talu elumaja omale piddanud need ka oma wäimehe Jaan Saarele ära müib selle summa ette kuwekümne wiie rubla igawese aja peale ja läheb tema Abram Poom isi Wottigvere valda oma poja juure elama, selle maja juures on 10 palki 1 1/4 kopiku põletuse puid. ning muud veikest puu kraami ning lehma sõnnik need sawad keik Jaan Saarele eespool nemetud summa ette; Jaan Saar maksab se raha 65 Rubla Abram Poomile siis ära kui Abram Saar Wottigwere walda ära läheb kas kuu ehk kahe parast. selle kaub kinnitamiseks on molemad kauba tegijad omad nimed allakirjutanud oma käega Jaan Saar (allkiri)
Abram Poom ei moista kirjutada tema palve peale kirjutas Isak Tomik (allkiri)
1900 Aastal Aprilikuu 28 pääval on se eesseisaw kaup ja lepping suusanal Awwinorme Wallakohtole  Awwinorme  Walla majas Protokolli üles kirjutamiseks ja kinnitamiseks ettekantud Awwinorme Walla inimese Jaan Saar Abrami poja  ja Wottigwerre valla inimese Abram Poomi Jaani poja  poolt kes Awwinorme vallas elavad ja Walla kohtule tuntud ja teatud ja nende kaup ja lepping tänasel pääval kohtu Akti ramatuse N 12 all üles kirjutud ja kohtu poolest kinnitud sanud. ja on Jaan Saar isi oma käega oma nime kirjutanud Abram Poomi palve peale agga Awwinorme Walla inimene Isak Tomik allakirjutanud
M. Toming. (allkiri)
Jaan Laud (allkiri)
Mart Kask (allkiri)
Kirjutaja Schulbach (allkiri)
