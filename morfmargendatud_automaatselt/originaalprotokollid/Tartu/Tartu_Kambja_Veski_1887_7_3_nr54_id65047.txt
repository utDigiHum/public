Weski Kogukonna Kohus sell 3 juulil 1887.
                                  Koos oliwad Pääkohtumees: M. Karru
                                                             Kohtumees: J. Aleksanderson
                                                             Kohtumees: J. Kullberg.
Wana Piigaste kogukonna kohtu kirja järele 5 juunist sell aastal No 166, milles siinset kogukonna kohut palutakse Johan Trooni juures tee-
nistuses oleja Johann Hõim käest 7 r. 50 kop. Märt Hõim pärijate hääks ja wallamaksud 2 r 82 kop keelu alla panna.
                                                   ----------------------------------------
                                              Summa 10 rubl. 32 kop.
Keiserliku IV Tartu Kihelkonna kohtu käsu kirja järele 18 juunist s.a. No 3643 kästakse Juhan Hõim käest 18 rubla 75 kop sisseriisuda Wast-
se Piigaste mõisawalilt. 
Kutsuti ette Johann Troonj Johann Hõimu peremees ja tema ütles küsimise pääle, et Juhann Hõimul om 40 rubla tänuwuse aasta teenis-
tuseks lepitud. 20 rubla olla tema omast palgast juba kätte saanud ja 20 rubla olla weel peremehe käes.
Kutsuti ette Juhan Hõim ja tema ütles küsimise pääle oma palga kohta needsamad sõnad, mis tema peremees. -
                              Otsus: Juhan Hõimu saamata ja teenimata palk 20 rubla nimetatud wõlgade hääks peremees Juhan Trooni kätte
                                           kinni panna. -
