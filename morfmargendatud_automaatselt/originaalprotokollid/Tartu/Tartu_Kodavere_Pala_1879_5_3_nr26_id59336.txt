Ette tulli Willem Kangur ja kaibas, et tema olla Jakob Waddi poja Willemi omale karjaseks wõtnud ja se pois olla nelli nädalat tema jures olnud ja nüüd kui karri on tarwis wälja saada olla pois ära läinud. Jakob Waddi andis wastuse kaibaja olla ilma tema teadmatta poisi omale wiinud, lisas weel küsimise peale jurde, et Willem olla kül temale üttelnud, et pois tema jures on, tema wastanud, kui Teie läbi ei saa siis ma wõttan ära.
 Kohtu otsus: Et ni kui Jakob Waddi issi sisse on annud, et tema selle poisi on ära wõtnud siis maksab tema perre mehele käsi raha ja söögi eest 5 Rubl. tagasi ja saab, et oma  wõimuga on poisi ära wõtnud 1 Rubl. waesteladiku trahwitud. Se Otsus sai Kohtu käijattele appelationi seadusega ette loetud ja et Jakob rahul ei olle sai temal oppuse leht wälja antu.
                               pä Kohtumees: Kusta Kokka +++
                                    Kohtumees: Kusta Nukka +++
                                    Kohtumees: Mihkel Lallo + ++   
