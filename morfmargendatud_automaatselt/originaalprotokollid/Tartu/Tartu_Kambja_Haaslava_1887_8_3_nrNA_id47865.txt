Sel 3 Augustil 1887.
Kaibas Jürri Luha, et Johan Kieter om tema 12 asta wannat poiga tema, Kieteri, sauna põlletajas nimitanu päle selle ollewat Johan Kieter teda ähwartanu ja pallus et Kohus seda inemist selle perrast trahwis.
Johan Kieter wastutas selle päle, et Jürri Luha poeg käinu tema sauna man ja kui se poisikene tema jurest ärra lännu, lännu saun pallama, tema ei olle Jürri Luha  poiga põlletajas sõimanu ei ollewat ka Jürri Luhat ka ähwartanu.
Tunistaja Ihaste kõrzimees Kristjan Undritz tunistas et tema om kuldnu kui Johan Kieteri naene om üttelnu et Luha poeg om sauna põllema pandno, nüid om ta sanut, mis tahtis.
Peter Luha tunistas nisamma ja et Johan Kieter ähwartanu et neide korsna peawa nisamma musta ollema kui mino tukki, kellele se ähwarus ütteldu sanu ei tea tema.
Johan Kieter ütles, et tema naine haige om ja tulla ei olle sanu.
Otsus: et se assi Keiserliko Tarto Maakohtu mõistuse alla om anda.
Päkohtumees Johan Link XXX
Kohtumees Johan Opman XXX
"  Jaan Hansen /allkiri/
