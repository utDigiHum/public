Protokolli pääle  23 Julist s. a. N: 100 sai ette kutsutud Kusta Tõltsepp Saarewallast ja temale awaldud et tema poeg Jaan Tõldsep  Jakob Wened kõwasti karjas peksnud ja tegi kohus selle 
Otsusse: Jaan Tõldsep saab selle teo eest 5 witsalöögiga trahwitud ja saab tema palgast Kadri Wene kahekordse käimise kulud ja Jakob Wene rohu raha äramaksetud.
Se otsus sai  Kusta Tõltseppa ja tema pojale awaldud ja wõttis Jaan Tõltsep wiis witsalööki wastu.
                                                         Pääkohtumees O. Kangro
                                                               Kohtumees J. Soiewa
                                                               Kohtumees O. Mõisa
                                                               Kirjutaja allkiri. 
