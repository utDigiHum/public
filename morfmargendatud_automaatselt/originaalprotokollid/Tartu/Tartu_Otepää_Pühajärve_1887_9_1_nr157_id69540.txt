Tuli ette Juuli Pettai eestkostjaga oma isaga Peeter Pettai'ga ja kaebas, et tema olla Kusta Pettermann'iga kokku elades, tütalapse saanud, kes kolm kuud wana olla. Kaebaja nõuab Kusta Pettermanni käest lapse kaswatamiseks iga 24 tunni jaoks kunni lapse 10 aasta wanaduseni 15 kopkat maksetud saada.
Kutsuti ette Kusta Pettermann ja wastas küsimise pääle, et tema olla küll Juuli Pettaia lapse isa ja lubab maksa lapse ülespidamiseks aastaks 6 rubla. Enam tema ei jõudwad oma teenistuse järele maksa. Kui enam maksu nõutakse, siis tahab last henda kätte kaswatada saada.
Juuli Pettaieestseisjaga ei luba mitte last oma käest ära anda lapse isa Kusta Pettermanni kätte.
Mõistus: Kusta Pettermann, oma ütlemise järele Juuli Pettaja lapse isa, maksab Juuli Pettajale igal aastal lapse ülespidamiseks 6 rubla. Seda esimest 6 Rubla maksab ühe kuu aja sees ära ja siis aasta pärast igal aastal 6 rubla kuni lapse 10 aasta wanaduseni, see on summa 60 rublas.
Kohtu mõistus kuulutati § 773 seletamisega. 
