sel. 6 Mail 1877
koos ollid
teine kohtomees Jaan Mänd
kolmas kohtomes Jaan Mänd
neljas kohtomees Mihkel Sild
Raja külla perremehhed: Jürri Jallak, Josep Tomik Jaan Lepp ja Metsawaht Jaan Pihkwa kaebasid et Josep Pihkwa on nende karjamaast üks suut plats heinamaad umbes 10 wakka maad rokinud ja tahhab sellele aed ümber tehha ning ka üks küin peale tehnud ja palluwad nemmad koggukonnakohhut. et se aed mitte tehtud ei sa, ning et se küin saab mahha kistud ja need puud külla ajade tarwis prugitud saawad.
Josep Pihkwa räkis et se olnud üks wanna räggastik ja mets ja on temma siis seal mitto Aastad rookind ning paljo tööd tehnud ja laggastanud, ning ei olle Raja perremehhed tedda kelanud rookimast; ja on Krono maamõõtja se maa temma nimme peale kaarti ülles wõtnud; mis pärrast temma ka siis selle maale tahtnud aed ümber tehha ja ka sinna peale üks küin tehnud. need puud on temma agga muist wõerast metsast muist agga sealt samma karjamaa pealt raijunud.
Perremehhed Jürri Jallak, Josep Tomik Jaan Lepp ja Metsawaht Jaan Pihkwa lubbasid et Josep Pihkwa woib sealt küll heina tehha agga ei tohhi mitte ennamb seal karjamaad ülles künda egga põllust tehha sest et nende karjadel siis söödamaad ei olle. ja nõuawad nemmad selle plani eest 10 Rubla renti aastas
Moistus
Et Josep Pihkwa on omma lubbaga Raja külla karjamaad rookinod. ning sealt ommale põldu ja heinamaad tehnud ning sinna hulk lattisid ja üks küin külla metsast raijunud ja perremehhed sellega leppiwad. et need puud saawad külla ajadest prugitud ja et nemmad need heinamaad ja maatükkid 10 Rubla rendi eest Josep Pihkwale prukida anda lubbawad. siis moistab Koggokonnakohhus et weel Regulerimisse aktid ei olle wälja antud siis on se rogitud heinam weel perremeiste külla karjama. ja nendel õigus renti nõuda. ja peab Josep Pihkwa kui temma need heinamaad pruki tahhab Raja külla perremeistele Aastas 10 Rubla renti maksma. ning sawad need raijutud lattid ja ka need küini palgid, mis ka lattid on külla perremeistele aja puude tarwis sest et perremehhed on kannu trahwi rahha jätnud ja agga need puud taggasi nõudnd  
Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastal 1860 §§ 772 ja 773 kuulutud ning nende kohto käijatelle need säduse jarrel Appellationi õppetusse kirjad wälja antud.
Raja perremehhed Jürri Jallak, Josep Tomik, Jaan Pehkwa ja Jaan Lepp tunnistasid et nemmad selle moistusega rahhul on.
Andres Kiik XXX
Jaan Mänd XXX
Mihkel Sild XXX
Josep Pihkwa andis sel. 13 Mail koggukonnakohtule ülles, et temma selle moistusega rahhul ei olle et temma peab. selle Maatükki ja heinamaa eest 10 Rubla renti maksma perremeistele, mis temma issi suure waewa ja töega on rookinud. ning kronomaamõõtjad temma nimme peale mõõtnud ja kaarti ülles wõtnud ja pallus Appellationi wälja suurema kohto minna, mis temmale ka nenda kohhe N 148 all. sai wälja antud. 
