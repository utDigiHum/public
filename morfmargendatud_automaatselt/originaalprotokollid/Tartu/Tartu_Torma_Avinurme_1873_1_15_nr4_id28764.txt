Sel 15nd Januaril 1873. 
koos ollid
peakohtumees Maddis Tamm
teine kohtumees Josep Reisenpuk
kolmas kohtumees Josep Kallaus 
Tännasel kohtu pääwal said selle walla liisu mehhed ette kutsutud, ning nende käest järrel küssitud, kas neil kellegi käest nõudmist on,
N 1. Liisumees Andres Pern astus ette ja kaebas et temma Wend temmale issa parrandussest 1 kuub lubbanud; mis temma agga nüid anda ei tahha, ning teine kuub mis temma omma rahhaga ostnud, ning sedda seal majas ärra kullutanud, ning tahhab temma nende eest 15 Rubla.
Tomas Pärn räkis et temma sedda üht kuube küll ärra õiendab, agga teist kuube temma maksma ei hakka, sest et se on issa ajal ostetud ja kullutud sanud.
Moistus
Koggokonnakohhus on moistnud et Tomas Pern peab omma wenna Andressele teise kue eest 6 Rubla ja teise kue eest mis Andres olli ostnud ja pool piddane olnud 4 Rubla wälja maksma.
N 2. Liisumees Jaan Sõbber kes ilma passita Teilitsa jaamas tenib, ei olnud Koggokonna kohtu ette tulnud
N 3. Liisumees Tomas Reisenpukk ei olnud koggokonnakohtu ette tulnud.
N 4. Liisumees Jaan Ambos tunnistas et temmal kellegi käest nõuda ei olle.
N 5. Mihkel Kotto kaebas et temmal Jakob Pärna käest 1 Rubla saada.
Jakob Pärn maksis se rahha wälja.
N 6. Liisumees Jürri Aun ei olnud Koggokonna kohtu ette tulnud.
N 7. Liisumees Mihkel Pomm kes ilma passita Teilitsa jaamas tenib, ei olnud koggokonna kohtu ette tulnud
N 8. Liisumees Andres Pern Josep p. astus ette ja tunnistas, et temmal kellegi käist sada ei olle,
N 9. Liisumees Maddis Müür tunnistas et temmal kellegi käest sada ei olle.
N 10. Liisumees Juhhan Jallaka issa Mihkel Jallak astus ette ja kaebas, et temmal Jaan Jallaka käest 10 Rubla 80 koppikad ja Mihkel Jallaka käest 7 Rubla 85 koppikad saada ja Juhhan Tomika käest 2 Rubla 25 kop.
Jaan Jallak räkis, et se õige on et temmal nenda palju maksta, agga et temmal nüid woimalik ei olle ärra õiendada
Mihkel Jallak tunnistas nenda sammuti et temma nenda paljo wõlgu, agga et temmal ka nüid woimalik maksta ei olle.
Moistus.
Koggokonna kohhus on moistnud, et Jaan Jallak peab omma wõlga 10 Rubla 80 kp. ja Mihkel Jallak omma wõlga 7 Rubla 85 kop. kahhekümne nelja tunni sees Mihkel Jallakalle wälja maksma.
N 11. Liisu mees Tomas Pern tunnistas et temmal kellegi käest nõudmist ei olle.
N 12. Liisu mees Mihkel Kiik And. p. kes ilma passita Teilitsa jaamas tenib ei olnud koggokonna kohtu ette tulnud.
N 13. Liisu mees Maddis Kiik kaebas, et temma annud omma riided Juhhan Sõbberi kätte Teilitsa Jaamast koddu tua 1 kuub wäärt 7 Rubla 2. paari püksisid wäärt 4 Rubla, mis agga Juhhan Sõbber on ärra warrastada lasknud, ja nõuab temma sedda kahju Juhhan Sobbra käest.
Moistus
Et Juhhan Sõbber ei olle kohtu ette tulnud saab se rahha, kui Maddis Kiik peaks negrutist minnema temmale wälja maksetud, ning Juhhan Sõbra käest sisse nõutud.
N 14. Liisumees Josep Kallaus astus ette ja tunnistas, et temmal kellegi käest sada ei olle
N 15. Liisumees Tomas Juur ei olnud Koggokonna kohtu ette tulnud.
N 16. Liisu mees Jaan Ilwes tunnistas et temmal kellegi käest sada ei olle.
N 17. Liisu mees Karel Sildnik ei olnud koggokonna kohtu ette tulnud.
N 18. Liisu mees Tomas Kuusk ei olnud koggu konna kohtu ette tulnud.
N 19. Liisu mehhe Maddis Laurissoni issa Tomas Laurisson astus ette ja tunnistas et temma pojal kellegi käest sada ei olle.
N 20. Liisu mees Mihkel Lepp astus ette ja tunnistas, et temmal kellegi käest saada ei olle.
N 21. Liisu mees Andres Saar astus ette ja andis üllese, et temmal omma wenna käest omma issa pärrantust 33 Rubla ja nelja Aasta palk 80 Rubla sada üllepea 113 Rubla.
Jaan Saar räkis, et temma sedda rahha omma wennale wälja maksab.
N 22. Ado Liiw ei olnud Koggokonna kohtu ette tulnud.
N 23. Mihkel Kukk Marti p. ei olnud Koggokonna kohtu ette tulnud.
N 24. Liisu mees Tomas Kallaus ei olnud Koggukonna kohtu ette tulnud.
N 25. Karel Kasik ei olnud Koggokonnakohtu ette tulnud. 
Maddis Tamm XXX
Josep Reisenpuk XXX
Josep Kallaus XXX 
