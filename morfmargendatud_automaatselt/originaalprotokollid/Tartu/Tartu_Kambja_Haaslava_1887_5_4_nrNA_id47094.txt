4 Mail 1887
Adu Lentsius Wasulast tuli ette ja andis üles, et temal on selle walla mehe Jaan Pruksi käest 6 rubla ühe waka linaseemnede eest saada, Jaan Pruks olla neid seemneid tema käest wõtnud; aga nüid ei tahtwat selle eest midagi tasumist anda.
Jaan Pruks tuli ka ette ja ütles, et tema olla küll ühe waka lina seemneid Lentsiuse käest seemneks wõtnud; aga seemned ei ole mitte kasunud; sellest olla juba 12 aastat mööda, ja maksmata jäänud selle perast, et Lentsius ei ole tema käest lina seemneid wastu wõtnud, waid tahtnud rüki selle asemel.
Johan Raswa, kohtumees teadis ka sellest asjast; see ütles et tema olla Lentsiuse käest neid samu seemneid kolm wakka seemneks wiinud ja need olla kasunud.
Otsus: tunnistajad Jaan Mootsi Kriimanist ja Jaan Kold ette kutsuda.
Pääkohtumees: Johan Link XXX
Kohtumees: Jüri Kärn XXX
Kohtumees  Jaan Hansen /allkiri/
Kohtumees: Johan Raswa XXX
