Ranna walla mees Josep Kusik kaebas Palla mees Jacop Willemson olla tema jures üks sõidu wankri puwerk tehha ja wõtnud pealegi tema käest 5 Rubl. kässi rahha ette, et Kusik wankri peab terminiks  walmis tegema ja ütelnud kes termini ei pea kautab kässi rahha, tema on wankri walmis teinud . Willemson tulnud terminis sinna aga polle rahha annud egga ka wankrid wõtnud waid öelnud ma tullen teine kord agga polle ka teine terminis tulnud.
 J. Willemson wastas tema on kül tellinud aga  nad on termini eddasi jätnud ja tema on sest käerahhast 1 Rbl taggasi annud. Leppisid esti ärra nenda, et Willemson maksab käsirahha 4 Rub. ja 2 Rubl. otuseraha ning 14 R. wankri eest ja toob se wankrin Juliks ärra.
                 Peakohtum. G. Kokka
                 Kohtumees Kusta Nukka
                   abbi Tomas Welt.
