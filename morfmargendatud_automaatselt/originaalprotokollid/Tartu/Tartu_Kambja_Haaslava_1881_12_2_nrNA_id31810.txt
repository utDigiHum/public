Haslawa kogokonna kohus 2 Decembril 1881.
Juures istusid: Pääkohtumees  Mihkel Klaos
kohtumees Johan Udel
"   Johan Link
"  Rein Anko
Tuli ette nekrut  Hendrik Laar, kes Tammistus elamas on ja kaebas: temal on oma sõssare mehe  Karl Saali käest 25 rubl. raha saada, mis tema selle kätte hoida annud, ning palub seda nüüd kätte saada.
 Karl Saal kaebtuse üle küsitud ütles wälja: see raha saanud tema naise kätte ja on see nüüd ära surnud.
Mõistus:
Karl Saal peab see 25 rubl. 8 pääwa sees Hendrik Laari hääks wälja maksma.
Pääkohtumees Mihkel Klaos  
Kohtumees Johan Link 
"  Rein Anko 
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi kõrgemat kohut tahab käija ja ei olnud  Karl Saal mitte sellega rahul.
