1900 aasta Webruari kuu 24 päewal kinnitatud renti kontraht Mari Tosso Künnapuu ja Johan Puuseppa wahel.
Mina omanik Mari Künnap annan oma poole Künnapuu koha N: 7 mis on Halliku wallas Koddawere kihelkonnas Jurjewi kreisis, rentniku Johan J. poja Puuseppa kätte üheks aastaks rendi peale; see
on 23. aprillist 1900  a. kuni 23. aprillini 1901 aastani selle Künnapu koha seaduslikude piiridega 9½ taalrit 3 grossi ehk 91½ wakamaa arw järele siin kontrahti lõpus nimetatud muutmistega.
Rentnik Johan Puusepp maksab renti Künnapu koha N:7 eest iga aasta üks sada (100) rubla ja nimelt: 1 märtsil peab ½ aasta rendist ette maksma ja enne 1. oktobri kuud teine ½ jagu, mis
täht päewast rendi summast wõlga jäeb, selle eest maksab rentnik ühe protsendi kuu pealt juurde. Tulekassa raha peab 1. märtsil igakord ette maksma, kui tule kassa seltsi eestseisus seda nõuab.
                                        Allkirjad:
                                        Kohtueesistuja: J. Soiewa
                                        Kirjutaja: Sepp.    
