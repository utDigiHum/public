Haslawa kogokonna kohus 16 Decbr. 1881.
Juures istusid: Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
"   Johan Udel
Tuli ette  Karl Miina ja kaebas:  Mihkel Rattassep on teda teotanud, et tema kurjan tõben ja tema pesso waras olewat ning nõudis selle eest 50 rubl. kaebatawa käest.
 Mihkel Rattassep kaebtuse üle küsitud, ütles wälja: kaebaja üttelnud temale, et tema kubemed walutawad, mispääle tema nalja pärast wastanud: "Siis oled sina kurjan tõben!" Ka on kaibaja tema pöningelt heino wiinud, mispääle ka tema pesso säält ära warastud ja on tema kül Karl Miinale üttelnud: "Kui sina pöninge päält mino heino wiisid, siis wõisid sina ka ehk mino peso ära wia!" pääle see on Karl Miina teda ka kõigeks sõimanud ja teotanud.
Jaan Klautz tunnistas: Mihkel Rattassep üttelnud Karl Miinale: "Sina olet mino pöninge päält heino wiinud, wõib olla, et sina ka mino pesso ära wiisid!"
Tunnistaja Haslawa mõisa mölder ja selle pois ei olnuwad mitte sija kohtu ette tulnud.
Mõistus:
Et  Karl Miina ja Mihkel Rattassep üks ühte wastastiko on sõimanud ja teotanud, siis peawad kumbgi 3 rubl. 8 pääwa sees walla waeste laadiku trahwi maksma ja peawad rahule jääma. 
Pääkohtumees Mihkel Klaos  
Kohtumees Johan Link 
" Johan Udel
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi kõrgemat kohut tahab käija ja ei olnud Karl Miina mitte sellega rahul. 
