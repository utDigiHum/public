Protokolli peale 11. Augustist  s.a. N: 91 sai ettekutsutud Karl Limbak ja wastas küsimise peale, et tema Jüri Sallo  karja kohta kedagi ütelda ei tea.
 Walla wöörmünder Kustaw Nukka andis küsimise peale üles et Jürri Sallo  härrale ainult ühte lehma näitanud kes lonkanud kas see löömise läbi, ehk wana wiga olnud ei wõi tema teada.
 Jaan Kokka on Põltsamaal töö peale läinud. Jüri Sallo jättis Jaan Kokka   nagu tunnismeheks maha. 
 Otsus tulewa kohtupääwaks kohtukäiad tunnistuse kuulutuseks ette tarwitada.
                       Peakohtumees J. Sarwik 
                       Kohtumees       J. Stamm
                       Kohtumees      M. Piri 
                       Kirjutaja Carl Palm. (allkiri)
