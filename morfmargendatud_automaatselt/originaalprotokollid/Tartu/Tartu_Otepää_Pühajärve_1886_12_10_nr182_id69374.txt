Tuli ette Jakob Tiidt ja kaebas, et Jaan Maraniku naese käes olnud temal 28½ küünard poodi lõimega toimist rõiwast kudada. Tema aga ei saawad säält oma rõiwast mitte kätte ja on temale üteldud, et rõiwas ärawarastatud olla. 
Nüüd nõuab Jaan Maraniku käest tasumist selle rõiwa eest küünar 70 kopkat = summa 19 rubla 95 kopkat.
Jaan Maranik ütles küsimise pääle, et Jakob Tiidt'u rõiwast on olnud 28 küünard. Jakob Tiidt tulnud tema poole rõiwale järele, pakunud küll 3 rubla ühe wana rehnungi tasumiseks raha laua pääle, aga ei ole rõiwa kudamise hind a 1 rubla mitte äramaksa tahtnud. Waid lubanud ühe nädala pärast järele tulla, aga on weel kahe kuu pärast tulnud. Sellel wahe ajal aga rõiwas tema käest ärawarastatud.
Jakob Tiidt ütles, et kudamise hinda mitte lepitud olnud, waid Maranik ilma hinnata kudada lubanud.
Mõistus: Et Jaan Maranik'ul raha laua pääl olnud ja temasellest mitte oma rõiwa kudamise hinda ärawõtnud ja rõiwast kätte andnud, siis on tema süüdi arwata ja peab selle pärast Jakob Tiidt'ule rõiwa eest, küünar a 40 kop, summa 11 rubla 20 kopkat maksma.
Sellest läheb rõiwa kudamise hind 1 rubla maha ja jääb 10 rubla 20 kopkat ja peab Jaan Maranik see 10 rubla 20 kopk. Jakob Tiidtule 14 päewa sees äramaksma.
Mõistus kuulutati §773 seletamisega.
