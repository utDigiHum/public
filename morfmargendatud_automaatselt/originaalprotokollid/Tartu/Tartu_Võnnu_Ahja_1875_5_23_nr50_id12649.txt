Kaibasiwad Sawa Jaan &amp; Peter et Remmali Jaani 10 Wakka alla RüaOrrast neide Piiri sisse kuknud wastse Maamõtmisse perrast, ja pellesiwad Remmali Jaani sundida neile tolle est Maad Assemelle anda.
Remmale ettekutsutud wastotas, et kah temma Piiri sissen om töiste Rahwa RüaOrrast 11 Wakka alla.
Sai moistetud: Remmali Jaan ei pruki mitte Sawa Jaan ja Petrele Maad andma.
Se Moistminne sai kulatud §772 &amp; 773 jarrele.
Hindrik Torg XXX  Joh Klaosson  XXX  Peter Kulp XXX Hind Lipson XXX J Hanson  XXX
