Sel 10 Webruaril 1886
Kaibas Johan Bergman, et temal 3 Rubl 51 kop tööraha  Jaan Puhmu käest sada om ja pallus et kohus seda massma sunis.
Jaan Puhm wastutas et tema se tööraha wõlgu om ja massab kui omma Pistoli kätte om sanu, mis 4 Rubl 50 kop wärt om ja poig Woldemar Bergmani kätte om sanu.
Mõistetu: et Jaan Puhm se 3 Rub 51 kop tööraha 8 päiwa sehen Johan Bergmanile ärra peab massma. Pistoli wõib Jaan Puhm selle Woldemar Bergmani käest nõuda, kelle kätte se sanu om.
Se mõistus sai kulutu ja õpetus leht wälja antu. 
Päkohtumees: Mihkli Iwan /allkiri/
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
" Jaan Hansen /allkiri/
Massetu sel 1 Dets 1886
