Astus wallakohtu ette Halliku walla liige, pobul Jürri Laumets, Karli poeg, ja palus oma wiimast tahtmist protokolli üleswõtta, mis järgmine on:,, lubab oma liikuw warandus /: liikumata ei ole:/
oma naesele Ann Laumetsale tema tüttre Miili ja pojale Willem ühte wõrra, nii et tema kolme jakku jagatud saab. Kui aga üks nendest kolmest peaks ära surema, siis saab, warandus järel jäänudele, ühte wõrs iga ühele. Tõistel pääle tema surma pärimise õigust ei ole.
                        Jürri Laumets ei oska kirjutada ja teeb kolm risti +++
Otsus: seda, kui sündinud, ülestähentada.
                        Kohtu eesistuja  Abram Saar
                        Kohtumees        Joseb Rosenberg
                        Kohtumees        Jakob Reinman
                        Kirjutaja             O. Seidenbach.              
