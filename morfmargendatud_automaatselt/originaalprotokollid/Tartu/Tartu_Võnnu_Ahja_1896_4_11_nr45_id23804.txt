1896 aasta Aprilli kuu 11. päewal sai XVIII Ahjawalla kohtu poolt kirjalik lepping (Mõisamaa sulaase kontraht) kinnistatud mis 11. Aprillil s.a. kolmes exemplaris Ahja mõisawalitseja Ludwik Gustawi pg Klinge ja Ahja walla talupoja Peeter Mootise wahel on maha tehtud ja mille järele esimene töisele Lokko No 5 12 taalrid koha pruukida annab Jüripäewast 1896 a kuni Jüripäewani 1897 orjuse ja päewade eest.
Eesistuja J Kooskord &lt;allkiri&gt;
Kirjutaja: Puusepp &lt;allkiri&gt;
