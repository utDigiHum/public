Haslawa Kogukonna Kohus sel 12 Nowemb 1884.
Man ollid: Päkohtumees: Jaan Link
Kohtumees Johan Klaosen
"  Peter Iwan
"  Johan Grossberg
Selle otsuse päle sest 13 Augustist s.a Johan Toome kaibuse asjan wasto Jaan Hagel nõudmisse perrast ollid kutsumise päle ette tullnu
Karl Luha Haslawast tunistas et ülle minewa asta om Johan Toom seda 100 Rubl Kontrakti raha Jaan Hageli käest nõudnu sis om Hagel wastutanu et minul ei olle rahha anna minule weel päle.
Karl Ernitz Ahjalt tunistas et Jaan Hagel om kül üts kõrd Tarto Linan 100 Rubl Johan Toomile massnu sis om kõnne ollnu et keik se raha kontrakti est tassa om sanu.
Widrik Ernitz Ahjalt tunistas et üts kõrd Tarto Linan om Jaan Hagel 100 Rubl Johan Toomile massnu sis om kõnne olnu et keik tassa sanu ja Toom lubanu Kontrakti kätte anda.
Peep Hagel Kamerist tunistas, et tema sest midagi ei tea kas Jaan Hagel weel Johan Toomile wõlgu om ehk mitte.
Mõistetu: et Johan Toomi nõudmisse kaibus wastu Jaan Hagel tühjas mõista om, selle et tunistuste wäljaütlemisse perra Jaan Hagel se 100 Rubl Johan Toomile ärra massnu om ja Johan Toom Kontrakti Jaan Hagelile kätte andno om.
Sel 19 Nowembril 1884 Jaan Hagelile kulutado. Sel 26 Nowembril 1884 sai se mõistus Johan Toomile kulutado ja õpetusleht wälja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees: Johan Klaosen XXX
"  Peter Iwan /allkiri/
"  J. Krosbärk /allkiri/
