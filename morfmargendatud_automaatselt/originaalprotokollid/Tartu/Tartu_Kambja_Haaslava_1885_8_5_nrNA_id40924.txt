Sel 5 Augustil 1885
Kaibas Jaan Anderson, et temma Jutra tallo selle wangin ollewa Jaan Põdra laste Wölmündre Mihkel Rattasepa ja Johan Luha käest keige kramiga rentinu om, ent nüid om 2 nüswat Lehma, 1 aher lehm ja 1 pullikene kohtu läbbi ärra müidut ja nõudis omma kahju 300 Rubl selle perrast.
Jaan Põdra laste Wölmündre Mihkel Rattasep ja Juhan Luha wastutasiwa selle päle, et nemma seda krami ärra müija ei olle käsknu selleperrast nemma ka seda kahju ei massa. Kohtu poolt sai mannu pantu et ne nimmitedo Ellajat Keiserliko 4ta Tarto Kihelkonna Kohtu kässu päle sest 16 Maist № 3212 ja 18 Julist s.a № 4755 ärra müidut said, selle et Wölmündret seda nõutawat raha 67 Rubl 15 Kop Jaan Põdra est ärra ei massa ja need ärra müidut Ellajat selle kogukonna kohtu protokolli perra sest 8 Julist s.a Jaan Põdra omma ollit.
Mõistetu: et Jaan Andresoni nõudmisse kaibus tühjas om mõista, selle et temal üttegi tunistust ei olle, et temma selle tallo rentnik om, sest sijn kohtu een ei olle midagi ärra tehtut ja kogukonna kohus pidas Uellemba kohtu käsku täitma.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumee J. Link /allkiri/
Kohtumees: Johan Klausen XXX
"  Peter Iwan /allkiri/
"  Jaan Pruks /allkiri/
