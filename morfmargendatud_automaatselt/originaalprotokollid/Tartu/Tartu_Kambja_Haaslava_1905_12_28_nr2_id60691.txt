Protokoll № 2.
Haaslawa wallakohus,  Haaslawa wallamajas,  Jurjewi maakonnas, Liiwi kubermangus 28 Decembril  1905 a.
Juures: esimees:  Johan Otsa 
Tuliwad ette  Haaslawa walla talupojad  Peeter Johani p. Hansen ja  Johan Johani p. Hansen.  Mõlemad annawad üles ja paluwad protokolli kirjutada, et päranduse jagamise lepingu § 1 punkt 3 järele, mis siin wallakohtus 17. Webruaril 1895 a. № 4 all kirjutatud, Johan Johani p. Hansen oma päranduse osa kaks sada /200/ rubla oma wennalt Peeter Hansenilt kätte on saanud.
 Juhan Juhani p. Hansen  /allkiri/ 
 Peter Johani Poek Hansen /allkiri/
Haaslawa walla kohtu esimees Johan Otsa selle läbi tunnistab, et eesolew leping   Johan ja Peeter Hanseni poolt suusõnal awaldatud ning mõlemate poolt oma käega alla kirjutatud ja et mõlemad isiklikult kohtule tuttawad on.
Kohtu esimees:  J. Otsa /allkiri/
Kirjutaja: J. Wäljaots /allkiri/
