Sel 26 Julil 1882 
Kaebas Johan Meinhart Krimanist et Unikülla kõrtsimees temma käest rahha risunu ja sis ka weel riedet ärra wõttan. Küsimise päle andis  Johan Meinhart  ülles et  Josep Tenno  tema käest 8 pudeli õlle ja 3 naela est 1 Rbl 30 cop wõttan ja sis küssino tema weel 2 pudelt õllut ent  Tenno andno korw õllut wälja ja ütelnu: joge poisi ja kui õllu otsa sanu küssnu tema  poistele mõni pabberos anda ja küssinu: mes nüüd rehnung om ja üteldu 4 Rbl 50 cop. Temma andno selle päle 6 Rbl kaits kolme Rublalist ja Josep Tenno ei olle midagi enamb tagasi massnu ja andno temale weel 1 Nael napsi selle ütlemissega: nüüd om tassa, selle päle lönu tema selle wiena klaasi keige wienaga põrmandat wastat ja päle selle Klaasi weel 3 Klasi lönu tema purrus ja kõnelenu: mes om nüüd rehnung. Tenno koston: 24 Rbl ja kui temal enamb rahha massa ei olle ollu om Tenno üttelnu: "kassuk mahha" mes päle tema sis kassuka mahha wissanu ja tõisel päiwal lännu tema kohtumehega kassukale järgi sis om Josep Tenno naene 26 Rbl kassuka est nõudnu, tema kassukat wälja ei olle wõttnu.
Josep Tenno wastutas selle kaebuse päle et tema seda andno mes Johan Meinhard nõudnu ja ka massa lasknu. Johan Meinhart om 8 õlle klasi ja 5 wähembat wäikest pudelit neist 2 täis katski lönu, ja neide est om temma 24 Rubl nõudnu ja Johan Meinhard om essi omma kassuka mahha wissanu ja sis sinna kõrtsi jätnu. Tunnistajas andis ülles Jaan Jutras, Johan Sulp, Jürri Jansen ja Daniel Roijo.
Johan Meinhart andis tunnistajas ülles Mihkel Ers  Wassulast, Jaan Põdder, Jürri ja Jaan Woltre Haslawast.
Johan Meinhard om Josep Tennoga ärr lepinu.
Otsus: Protokolli ülles panda.
Päkohtumees: Mihkel Klaos XXX
Kohtumees:
Johan Udel XXX
Peter Luha XXX
