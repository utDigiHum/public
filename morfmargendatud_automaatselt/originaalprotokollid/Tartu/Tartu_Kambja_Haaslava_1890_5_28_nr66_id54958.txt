№ 66
Sel 28 Mail 1890.
Kaibas  Ann Sirak, kes  Tähtwere Leidlofi Karja mõisan ellab, et temal 20 rubl kapitali ja selle raha 3 asta prozent 6 rubl  August Klaoseni käest sada om ja pallus et kohus seda raha ärra massma sunis. 
 Ann Särak ei mõista kirjutada.
 August Klaosen  wastutas selle päle, et tema minewa talwel  Ann Sirake mehe kätte 15 rubl ärra massnu ja jänu enegi 5 rubl wõlgu, kohe jurde tema ka 2 rubl intressi massa lubanu.  Ann Sirak ollewat käsknu seda raha ka tema mehe kätte ärra massa. Kui wallawanemb temale palka annab lubab tema se raha   Ann Sirakele ärra massa, prozenti ei olle kaubeltu. Kui tema se 75 rubl  Ann Sirake mehele massnu ollnu  Johan Willemson, kes Wassula wallan ellab, ka jures ja sellega ütten ollnu üts tõine linna mees, keda tema ei tundnu.
 August Klaosen /allkiri/
 Ann Sirak wastutas, et tema mees seda raha  August Klaoseni käest sanu ei olle.
Otsus: et  Josep Willemson Wassulast ja  Ann Säraki mees Johan Sirak Tähkwerest ette om kutsuta.
Päkohtumees:  Jaan Pruks /allkiri/
Kohtumees:   Jakob Saks /allkiri/
"  Jaan Treijal /allkiri/
