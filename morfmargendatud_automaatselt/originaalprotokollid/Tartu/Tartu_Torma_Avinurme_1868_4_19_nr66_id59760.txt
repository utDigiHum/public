Awwinorme Koggokonnakohtus sel 19mal April 1868
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Astus ette Mihkel Mäggi ja kaebas koggu konna kohto ees, et temma wannemb poeg Jürri kes temma tallo peale perre mehheks jänud ei tahha tedda omma juures piddada egga toidust anda., ning pallus kohhut sedda asja järrel kuulata.
Kutsuti ette Jürri Mäggi, kes räkis et temma ei olle omma issada ärra ajanud, wait et temma issi omma hea tahtmisse järrel ärra lähnud. noorema poja Josepi juure.
Astus ette Josep Mäggi ja räkis et temma tahhab issa toitmisse eest Jürri käest maad sada ja et issa selle pärrast ärratulnud, et Jürri naine temmast ühtegi ei holi.
Siis sundis koggo konnakohhus neid issi kokko leppima, ja leppisiwad Jürri ja Josep Mäggi nendawiisi, et Jürri nende tallo maade küllest Josepille annab loima wäljalt üks põld üüsajast teine tük ja kolmastük Undiaugomäe põld poolest, ja heinamaad Koppel wäljaallusega ja õue juurest kalda allune, ning moistis Koggokonna kohhus et need maad ni kauaks kui issa ellab Josepi kätte jävad, pärrast issa surma woib Jürri neid omma tallo külge taggasi nõuda. 
Mis tunnistawad
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
