Tulli ette Johan Wösso nink kaibap omma Poia Jani päle et temma poial üts wöras pässul nätta olli nink pallup kohtu mehhet et ne tulles nink temma poia käest sedda pässulat (mes Essa arwates warrastedu om) ärra wöttas nink selle kelle omma ta om kätte sadasi, 
Kohtu mehhet Peter Zirn n. Josep Tensing omma sinna lännuwa Pässulat otsma nink omma paljo muid asjo Zuwwa nahka ja Rauda Tange ja üts Saag ja üts Sawwino wina kruus mis penikest wina täüs omma olnu (mes Essa ütlep kiik warrastadu ollewat) neid kiik omma kohtu mehhet ärra wötnu nink kinni pandu
Johan Kärik kelle pässul omma om nöwwap 2 Rublat taggan otsmisse nink waiwa eest,
Kohus moistap et jaan massap uts Rubla wiiskumend kop Johanile nink Saap katskümmend witsa lööki ehk 4 Rubla obbedat, kiik töise asja jääwa kohtu meeste holde, kui rahhul ei olle wöttap Protokolli
Peter Maddisson XXX
Josep Tensing XXX
Peter  Zirn XXX
Josep Arik XXX
