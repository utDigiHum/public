Ette tuli Märt Piiri Mihel Oebiuse asemel ja näitas wõlla tähe ette kelle perra temal Karl Kubja käest  16 rubl. 13 Kop. saada on ja palus, et see raha mis ju 2 aasta eest oleks pidanud maksetud olema kohus sisse nõuaks.
Karel Kubjas wastas temal olla kül see wõlg maksa aga keiki korraga tema mitte praegu tassuda ei jõua waid  taha poole sest ära tassuda ja palub poolega kannatada.
Kohto otsus: Karel Kubjas peab 8 rubla 8 pääwa sees wälja maksma ja 8 rubla 13 Kop. Mihkli päwast: 1882 ära tassuma.
See otsus sai kohto käiattele etteloetu.
                         Pä Kohtomees:Hindrek Horn
                           Kohtumees: Josep Soiewa
                           Kohtumees: Dawet Willemson                 
                                
