Johann Haawakiwwi kaebab Willem Perrametsa peale, temma olla selle juure aastal sullaseks heitnud, 18 Aprillist 1882 kunni 18 Aprillllini 1883, kelle aea eest temma 80. rubla rahha palka 1. wakka
olla linnamaad pidanud saama, agga 5. näddalad taggasi olla Willem Perramets tedda teenistusest lahti lasknud. Palka  olla ta 26 rubla 63. Kop. kätte saanud, ja nõuab nüüd Kaubeldut aasta palk.
Linna seeme olla kaebaja mahhakülwanud ½ wakka.
Willem Perramets kostab kaebduse peale, et neil olnud kirjalik kaup tehtud 23. Aprillist 1882 kunni 23. Aprillini 1883. aastani 80. rubla aastas, kuu peale 6. rubl. 66 2/3 koppikad, linnamaad  polle kaubas olnud. 24. Juulil 1882. aastal  lasknud temma  sullase järgmiste süüde pärrast lahti
1/ polle puu warrast  kinni wõttnud kes weski truubi silla juurest silla puud warrastanud.
2/ müüs 1. küllimit kaeru keige Kottigga Ranna tee peal ärra.
3/ müinud jälle 1. tühja kottii ärra.
4/ müinud 1. tühja kotti weski pealt ärra.
5/ Kibbedal heina kokku panneku aial olnud 4. päewa ilma lubbata ärra. ja teine kord jälle 1. päew sel 20. Julil.
6/ Ilma suggematta pannud linnad likku.
Rahha oll 28. rubl. 63. Kop. kätte annud. Eddasi oll, tedda laimanud, et oll 2. Halliku walla posti warrastanud. Sullane polle reht peksnud, rukkid leiganud, egga teed tehnud. Hawakiwwi nimmetab, et sullasel olla  Kontraht käes ja olla  sullane linnaseemned ilma temma lubbata mahhatehnud.
Selle peale kostab punktide peal Johann Haawakiwwi
1/ Katsunud warrast kül kinni wõtta agga polle suudanud.
2. polle kaeru  kottiga ärramüinud
3, polle tühja kotti ärramüinud waid se jänud se Kord sullase kätte.
4,weski pealt annud se kotti perremehhe kässu peale Maddis Waddile.
5.Käinud perremehhe loaga heina aal ärra 2. päewa, ja 20. Juulil 1. päew
6. Linnad saanud soetud ja likku pandut.
Eddasi polle ta kellegi töödele wasto pannud egga perremeest laimanud. Johann Hawakiwwi näitas omma pool Kontrahti Kohtule ette, mis tal taggasi antud sai.
Mihkel Bitter tunnistab kohtu ees, kedda mõllemad kohtukäijad kui tunnistajad üllesse ollid annud, et peremees saatnud Johan Haawakiwwi puu warrast kinni wõtma, se lähnud, polle agga kinniwõtnud, siis saatnud perremees , tedda tunnistajad, ka appi kinniwõtma, agga Juhan Hawakiwwi tulnud wasto ja öelnud:,,Kudda minna wõin kinni wõtta, kui kohtumehhed talle lubba on annud. Selle peale pöranud temma ka ümber, agga Hawakiwwi pole tedda mitte kulanud kinniwõtmisele. Siis wõtnud Willem Perramets  Johann Haawakiwwile üllesse, ja aanud tedda ärra.
Perremehhe kässu peale polle Johann Haawakiwwi  sitta wedama lähnud. Ka polle rehhe ahju parranduse peale egga jõe kaewamise peale lähnud; muud wastapanemisi temma ei tea. Se tühhi kot saanud temma kätte Rannas, agga üks wõeras mees wõtnud kotti temma käest ärra. Johann Haawakiwwi olnud heina aal 4. päewa ja teine kõrd 2. päewa ärra kas ilma perremehhe lubbata, ei tea temma. Johann Hawakiwwi olnud linnad ärrasuggenud. Laimamist polle tunnistaja kuulnud. Kohhe peale linnateggemise teadnud peremees, et  Johann Hawakiwwi linnad mahhatehnud agga ta polle selle ülle suggugi nurrisenud.
Otsus: tullewal kohtupäewal asja eddasi toimetada, ja  üllesantud tunnistajad üllekuulata.
                                  Peakohtumees: H. Horn
                                        Kohtumees: Märt Piiri
                                        Kohtumees:Josep Soiewa.
