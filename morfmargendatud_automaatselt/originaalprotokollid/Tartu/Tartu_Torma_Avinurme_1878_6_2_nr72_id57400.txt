sel. 2. Junil 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jaan Mänd
kolmas kohtomees Jürri Tomik
Et need karja poisid Mihkel Laurisson, Mihkel Kukk. Jakob Hallik, ja Mart Pomm on karjas tüllitsenud ja taplenud ning nende Wannemad neid issi ei karrista wait weel õigust annawad. siis on koggokonna kohhus moistnud et Mihkel Laurisson 3 witsa löögi, Mihkel Kukk 5 witsa lögi, Jakob Hallik 5 witsa loogi ja Mart Pomm 5 witsa logiga nende issade juures ollemissel trahwitud sawad.
Josep Laurisson XXX
Jaan Mänd XXX
Jürri Tomik XXX
