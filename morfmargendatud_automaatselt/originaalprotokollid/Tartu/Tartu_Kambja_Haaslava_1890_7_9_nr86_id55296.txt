№ 86
Sel 9 Julil 1890.
Selle otsuse päle sest 7 Maist s.a Nr 47 Mihkel Kõiwo kaibuse assi wasto Jaan Sawik olli Jaan Sawik kutsumise päle tänna ette tullnu, sest tema om wahe aijal haige ollnu ja 11 Junil olli tema Uellemba tallo rahwa kohtu ette kutsutu ollnu, selleperrast om se assi siin seisnu. Päle selle kui se Mihkel Kõiwo kaibus ette olli loetu wastutas Jaan Sawik selle päle, et temal se raha sõssar Mina Sawikele ärra om massnu ja nimmelt 1885 asta enne 15 rubl suwel ja 17 rubl Septembri kuul. Jaan Sawik arwas et sekõrdne wallawanemb Jaan Soo jures ollnu ehk teab kui tema se 15 rubl massnu ja kui tema Septembri kuul 17 rubl massnu ollnu Jaan Toom Haslawast juures kes surno om ja Jaan Tarrik, kes Haslawan ellab. Sawik pallus neid tunistajat ärra kulata ja Mihkel Kõiwo nõudmine tühjas mõista.
Jaan Sawik /allkiri/
Otsus: et Jaan Soo ja Jaan Tarrik ette om kutsuta.
Päkohtumees:  Jaan Wirro XXX
Kohtumees:   Jaan Pruks /allkiri/
"  Jaan Treijal /allkiri/
