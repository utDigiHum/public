Haslawa  Kogukonna Kohus sel 25 Aprilil 1883
Man ollid:
Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
Peter Iwan
Johan Grossberg
Tännasel päiwal olli Päraha Wõlla ja Magatsi wilja Wõlla perrast Oksion kulutado ja ka wõlglaste warrandus enne ülles kirjutado.
Jürri Siem ja selle poeg om 22 Rubl 84 Cop Päraha wõlgu, selle Jürri Siemol olli kohtumehe Johan Grossbergi läbbi ütteldu et tema kodun peab ollema, ja sis saab se ärra kirjutado lehm kohtumaja jurde todu ja ärra müidu. Kohto polt said kohtumees Peter Iwan ja Johan Grossberg selle lehmale perra sadetu, needsamat ollid Peter Ers ja Johan Ers abbis wõtnu, ent se Jürri Siem es olle seda lehma ärra andno, olli enne püssi wõtnu, keda tema poig ja naene ärra wõttnu, sis om Jürri Siem raud lapju wõtnu ja sis lauda läwe ette seisnu ja üttelnu: "tulge nüüd mina näe kes om se mees, kes lehma wõtma tuleb". Selle ähwaerduse päle ei olle need abbiliset Peter ja Johan Ers ka manu minna julgenu ja kohtumehe tulnuwa ärra.
Otsus: et se Jürri Siem Tallurahwa Säduse Ramatu 1860 astast § 1035 perra Keiserliko Tarto Sillakohtu kätte trahwimisess anda om.
Päkohtumees Jaan Link /allkiri/
Kohtumees: Johan Klaosen XXX
Peter Iwan /allkiri/
Johan Krosbärk  /allkiri/
