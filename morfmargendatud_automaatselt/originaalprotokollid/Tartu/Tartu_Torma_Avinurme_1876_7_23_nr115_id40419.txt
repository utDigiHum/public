sel. 23 Julil 1876
koos ollid
peakohtomees Josep Kask
teine kohtomees Josep Laurisson
kolmas kohtomees Jaan Mänd
Kadri Jallak kaebas Wölmöndri Mart Tomiku juures ollemissel et Krono metsawaht Jakob Wigi on temma juures 2 1/4 werand Aasta korteris olnud, ning ei olle temmale selle eest ühtegi annud, ja nõuab temma keige selle Aja eest 30 Robla üüri.
Metsawaht Jakob Wiigi räkis et se on õige et temma on Kadri Jallaka juures 2 1/4 Aastad korteris olnud. ja kui temma seal olnud on Metsaüllewataja Jürgenson Kadri Jallaka käest küssinud mis sa üüri tahhad ja on siis Kadri Jallak kostnud mis üüri meil waija on, kui 
metsawaht meil aitab töed tehha. siis on küll ja on temma siis ka seal majas töed tehha aitanud ning ommad põlletamisse puud sinna majase annod.
Anna Ilwes tunnistas et temma olnud sekord Kadri Jallaka juures tüdrukust kui Metsawaht Jakob Wigi seal korteris olnud. ja on siis küll arwast Metsawaht omma kohhust sinna Majase töele annud. agga temma issi ei olle millaski seal töed tehnud.
Jaan Loddo tunnistas et temma on üks kord nähnud kui Metsawaht Jakob Wiigi on Kadri Jallakal aitanud aeda tehha ja teine kord annud Jakob Wigi jälle omma hobbost ühhest pääwast puid weddada. Mihkel Jallak on agga jälle Metsawahhil aitanud Maja raijoda.
Karjapois Josep Kallaus kes 16 Aastad wanna tunnistas et Metsawaht Jakob Wigi on ühhest päwast omma hobbost aja ritwo widdada annud Kadri Jallakalle.
Moistus
Et Metsawaht Jürgenson siin kohto ees ei olle. siis jaeb se assi tullewase kohtopääwa peale selletada.
Josep Kask XXX
Josep Laurisson XXX
Jaan Mänd XXX
