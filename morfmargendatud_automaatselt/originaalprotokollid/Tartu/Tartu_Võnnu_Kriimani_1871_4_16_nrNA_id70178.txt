Krimannis 16 Aprillil 1871.
 Karel Kulber  tulli ette ja kaebas et minnul on  Jaan Mina  käest 5 Rubla h. sada ja pallun et ma kätte saan.
 Jaan Mina  tulli ette ja ütles: se on õige kül et mul Temmale 5 Rubla maksta on ja pallun et  minno perremees  Jürri Klaos se rahha Temmale wälja maksa.
 Jürri Klaos  tulli ette ja ütles: kui Ta eddesi piddi tenib siis wõin Temma palgast maksta.
Mõistus:
Koggokonna kohhus mõistis et  Jürri Klaos  peab Temma palgast 25 Septembrist se nimmetud 5 Rubla  Karel Kulberile ärra maksma.
Kohto lauan olliwa:
Peakohtomees Juhan Mina XXX
Kohtomees  Juhan Raswa  XXX
Wöörmünder  Mart Kärik  XXX
Kirjutaja assemel: M. Baumann /allkiri/
