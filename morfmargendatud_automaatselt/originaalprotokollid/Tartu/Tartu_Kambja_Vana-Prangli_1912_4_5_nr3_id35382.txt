5 Aprelja 1912 g. polutšila ja, Leena Joganovna Vaarmann, urožden. Jar (Jal) ot mojego brata Kusta Joganova Jar (Jal) svoju nasledstvennuju dolgo trista (300) rublei, s protsentami i sim izjavljaju svoju soglasije na unitštoženije obremenitelnoi stati na usadbe Kimmo, pod imenijem StaroVrangelsgof, Jurjevskago ujezda, za krp. N 131, vnesennoi tuda soglasno nasledorazdelnago akta javlennago u Vrangelskago vol. suda 20 Nojabrja 1908g. za N 7, kasatelno dolgov i prodaži usadbõ Kimmo. 
Leena Vaarmann i jeja mužskii sovetnik Petr Jurjev Vaarmann negramotnõ za nih po ih litšnoi o tom prosbe rospisalsja krestjanin: Hans Polakene [allkiri]
Pjatago Aprelja 1912g. akt sei javlen k zasvitelstvovaniju Vrangelskomu Vol. sudu Leenoju i Petrom Vaarmann, živuštšimi v sei volosti i litšno izvestnõje vol. sudu i imejuštšimi zakonnuju pravosposobnost k soveršeniju aktov pri tšem Vrangelskii Vol. Sud sim udostoverjajet što akt sei podpisan za negramotnõh suprugov Vaarmann kr-nom Gansom Polakene sobstvennorutšno.
Predsedatelj suda: K. Pala [allkiri]
Tšlenõ suda: A. Popakov? [allkiri] K Eli [allkiri]
Pisar JWeske [allkiri] 
