Mõisamaa meeste kontraht.
§ 1. Palla mõisawalitsus annab Wahhitarre N: XXVI mõisa maakoha Jakob Mäggi kätte, Jüripäewast 1909 kunni Jüripäewani 1912, kelle eest Jakob Mäggi mõisale orjab 170 /sadda seitsekümmet/ päewat suwel (§6) ja 215 /kakssadda wiisteistkümmet/ päewat talwel.
§ 2. Kõik hooned, kaewud ja aiad, üks külwatud rukki põld ning üks künnetud ja äestetud rukki kõrre põld, igaüks 8/kaheksa/ wakkaalla suur ja .....wakkaalla hagudest puhastetud heinamaad peab Jakob Mäggi terwelt wasta wõtma ja niisamati ka kontrahti lõpetuse ajal äraandma. Kõik parandamine ehk uueste ehitamine, olgu siis elumaja ehitus (§ 4), on majaniku kohus, ja ei wõi tema mõisa poolt muud materjali nõuda, kui palgid ja latid. Maakoha kraawid peab majanik alati puhtad hoidma, et kraawi wesi wõib ära jooksta. Heinamaad saab majanik mõisa heinamaadest.
§ 3. Jakob Mäggi peab need põllumaad säetud korrajärele tarwitama ja ei tohi sest Wahhitare N: XXVI talust ei sõnnikut, heinu, õlgi ega põllutoitu wälja wedada ehk ära anda kui üksi need õled, mis teiste mõisamaa meeste kattustele tarwis lähewad.
§ 4. Jakob Mäggi peab kõige Palla mõisamaa meeste hulgas nende kohtade elumajad ehitama ja materjali ilma maksuta juure wedama, selle põhja peale, mis mõisawalitsus ja majanikude selts enne iga ehitust ettekirjutab.
§ 5. Jakob Mäggi saab aastas 2½ /kaks-üks pool/ 6-jalalist sülda 3/4 arssinapikkused põletamise puid ja 150 kubbo walmis hagu, mis tema mõisa käsu peal, kas mõisa ehk wõera metsas ise peab raiuma ja ärawedama.
§ 6. Kui Jakob Mäggi ilma et tema mõisale teadust annab, kontrahtis säetud päewadel mõisa tööle ei tule, siis maksab nädali orjusse iga puuduwa päewa eest 40 kop. suwel, 30 kop. talwel. Need
päewad, mis Jakob Mäggi rohkem teeb, kui nädala orjus ettekirjutab, saawad temale suwel 40 kop. ja talwel 30 kop. tasutud. Suwe orjus arwatakse päewad Jüripäewast kunni 10. nowembrini, talwe päewad 10. nowembrist kunni Jürripäewani, ja saab nendel terminidel rohkem tehtud ja puudus jäänud päewade üle rehnung ärapeetud. Puuduwate päewade rahha peab igal 29 Septembril suwe ja talwe päewade eest igal 23 Aprillil äratasutud saama.
§ 7. Üle selle, mis § 1 nimetab, peab majanik aastas 12 wakamaad heinamaad oma hobusega tegema, niisama ka 4 wakamaad ristikheina niitma, ja redelite peale panema, 4 wakamaad rukkid ehk nisud, 4 wakamaad odrad wõi ernid ja 4 wakamaad kaerad lõikama ning kuhja panema, nii kui mõisa seda nõuab, ja saab temale nende tööde eest järgmiselt maksetud:
               Heinamaa ja ristiko wakamaa eest 1/üks/ rubl.
               Talwewilja wakamaa eest 1 /üks/ rubl. 30 /kolmkümmet/ kop.
               Suwewilja wakamaa eest 1/üks/ rubl.
§ 8. Kui majanik peaks enne kontrahti aea lõpetust ära surema, siis jääb see kontrahti õigus tema pärijate kätte, kunni tulewa 23. aprillini.
§ 9. Majanik peab Wahhitarre N: XXVI talun aasta üle pidama kõige wähem 6 /kuus/ suurt karjalooma ja 1 / üks / hobust. Kui majanik on ennast kulutanud selle talu parandamise heaks, ilma mõisawalitsuse kirjaliku tunnistuse nõudmiseta,  siis ei wõi tema selle eest midagi nõuda.
§ 10. Kõik õnnetused, mis majanikul tule, loomakatku j.n. e. peaksiwad juhtuma, kannab tema oma kahjuks.
§ 11. Majanikul on luba üks õuekoer pidada, koer peab aga ketti otsas peetud saama; wastasel korral on mõisawalitsusel luba neid koeri mahalasta, mis wäljaspool õue leitakse. Kui majaniku koer wäljaspool õue ümber hulgub, siis juhtub ka see, mis § 12 nimetab.
§ 12. Kui majanik ühe selles kontrahtis nimetatud paragrahwide wastu peaks eksima, siis kautab tema selle kontrahti õigust enne nimetud aega, ja mõisawalitsusel on õigus temale siis kohe üles ütelda. Kui majanik mõisa warandust warastab, ehk muu warguse pärast trahwitud saab, siis on luba teda kohe kohast wälja saata, ilma et tema kahjutasumist wõiks nõuda.
§ 13. Jakob Mäggi wõib iga aasta oma kohta ära anda, kui tema, õigel ajal, nimelt mitte hiljem kui 23. detsembril mõisawalitsusele seda nõu teada annab. Kui mõlemist poolt õigel ajal ei saanud ülesöeldud, nimelt 29. Septembril enne kontrahti lõpetust, siis käib see kontraht weel üks aasta edasi.
§ 14. Tähendus! § 2 selles lepingus muudetakse nõnda! Rotatiwa käib nõnda: 2 põldu rukki all, 1 ristiku all, 2 kesa ja 3 tõugu wiljad. Linade all ei tohhi majanik iga aasta rohkem piddada kui kümnes põllumaa ossa. Tema peab mõisale 45/nellikümmet wiis/ wikkid semme tagasi anda, kui tema kohast wälja lähab. Tema peab see Wahhi N. XXVI uue kohha kantnikul lubada, Wahhitarre rehhen oma wilja peksa.
                                          Hoonete seisus: 
    1) Elumaja korstnaga schindli kattusse all uus.
    2) Laut õllestikuga õle kattuse all hea.
    3) Kaks Aita.
    4) Saun õle kattusse all kõlblik.
    5) Õwe ümber aijad, kaew korralik. 
        Raud hinget ja muu raud kram on mõisa oma.
       Palla sel 29
       Rendile wõtja: Jakob Mägi (allkiri)
       Rendile andja wolinik: C Nemwalz.
Tõsjatšja devjatsot devjatogo goda, janvarja mesjatsja dvatsat vtorogo dnja dogovor sei javlen k zasvidetelstvovaniju Pallaskomu volostnomu Sudu, v pomeštšeniju suda nahodimsja v Pallaskim volostnom dom Jurjevskogo ujezda proživajuštšim v imenija Palla, poverennõm vladeltsa imenija Palla Aleksandra fon Stryk Karl Nemwalts litšno  volostnomu sudu izvestnõmi i imejuštšimi zakonnuju pravosposobnost k soveršeniju aktov. Pri etom volostnoi Sud udostoverjajet, tšto dogovor sei dogovarivajuštšimsja litsam protšitan i imi sobstvennorutšno podpisan  k aktov N: 3.
        Predsedatel: K. Reinhold
        Pisar: Brjukkel.
   Pala vallakohtu pitsati jäljend.
