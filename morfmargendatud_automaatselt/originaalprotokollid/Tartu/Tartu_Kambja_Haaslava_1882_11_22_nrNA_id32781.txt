Sel 22 Nowemb 1882
Kaebas Haslawa Rentiherra Rapp et mõisa kõrtsimees Willem Munna 100 Rubl kõrtsi renti wõlgu om ja pallus et kohus sedda massma sunnis.
 Willem Munna wastutas, et temal praegu rahha massa ei olle ja Rentherra wõib temma Kautsioni rahast se rahha ära wõtta et temmal om 600 Rbl kautsioni sehhen.
Rentherra wastutas et se 600 Rbl kautsion sehhen om ja sis sedda esite renti 100 Rbl kautsioni rahast wõtta tahab.
Otsus: seda Protokolli ülles panda et Rentherra Kautsioni rahast se wõlg rent wõtta wõib.
Päkohtumees: Mihkel Klaos XXX
Kohtumees Johan Link XXX
" Johan Udel
