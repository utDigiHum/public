Sel 10 Oktobril 1888.
Kaibas Jürri Sallo, et Mihkel Taari teda ähwartanu ollewat ja pallus et kohus seda selletas.
Mihkel Taari wastutas et tema kaibajat ähwartanu ei olle.
Jürri Salo andis tunistajas ülles Kusta Annok ja Johan Luha.
Kusta Annok tunistas, et ähwarust kuldnu ei olle.
Otsus: et Johan Luha ette om kutsuta.
Päkohtumees: Johan Link XXX
Kohtumees: Jürri Kärn XXX
" Johan Opman XXX"
" Jaan Hansen /allkiri/
