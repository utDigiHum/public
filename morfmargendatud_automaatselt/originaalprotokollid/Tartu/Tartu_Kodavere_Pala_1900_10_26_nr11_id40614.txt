1900 goda oktjabrja 26 dnja utveržden batratšijei dogovor meždu Gallikskim mõzn. Upr. i krestjaninom Janom  Johanovõm Vilk.
                                                                                                                   1.
Gallikskoje mõznoje upravlenije sdajet mõznuju batratskuju usadbu Vilka, stoimostju v 4 tal. 5  43/112 grošei v arendu srokom na  tri goda, stšitaja s 23 apr. 1901 g. po 23 apr. 1904 g.
                                                                                                                   2.
  Arendator objazan ježegodno bezplatno  proisvodit sledujuštšije rabotõ a imenno:
                                                      1. žat i klast v skirdõ v lofš.
                                                     2. žat 8 lofš. jarovogo hleba
                                                     3. kosit 2 lofš. klevera
                                                     4. kosit 10 lofš. sena
                                                     5. uravnivajet 100- 150 soženei dorogi tam, gde mõznoje upravlenije prikažet.
                                                     6. 47 peših dnei iz kotorõh dolžen postavit 42  dnja do Mihailova dnja, a ostalnõje zimoju.
                                                                                                                3. 4. 5.
                                                                                                        Kak v protokol N: 6.
                                                                                                                     6.
Svoi sobstvennaja polja arendator mõznoi batratskoi usadbõ dolžen obrabotõvat v tšetõreh tšastjah tak tštobõ nahodilis 
                                                                                                                                             2½ lofš. pod rožeju                                                                                                                                                                                                                                                                                                                         5 lofš. jarovõm hlebom 
                                                                                                                                              2½ lofš.gorošek,
                                   pritšem on ne imejet prava zasvat pod jarovõm hlebom ili lnom.                                                 
                                                                                                                       
                                                                                                                                                                                                                       
                                                                                                                                                                   
                                                                                                                 7. 8. 9. 10. 11. 12. 13. 14. 15.
                                                                                                        Kak v protokol N: 6.
                                                                                                                              16.
                                                                                          Sostajanije strojeniji usadbõ Vilka. 
                                                                    1. žilaja riga-novaja
                                                                   2. Ambar- horoši
                                                                   3. Hlev horoši 
                                                                   4. Banja horošaja
                                                                   5. Kolodets starõi 
                                                Predsedatel Suda: J. Soieva
                                                                     Pisar: Sepp.                                             
                                                                                       
