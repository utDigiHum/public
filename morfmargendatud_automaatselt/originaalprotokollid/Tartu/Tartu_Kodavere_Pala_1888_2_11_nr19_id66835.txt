Tuli ette  Willem Peramets ja andis üles et tema Hawakiwi talu rentnik Josep Hawakiwi temale 1883 aasta renti 237 rbl. 27 ja 1886 aasta Aprili Termin 360 rbl. wälja olla s.a. summa 537 rbl. 27 kop. ning ühe aasta ja kahe kuu  protsenti selle summast 6% 37 rbl. 51 kop. Nõudis seda raha kätte. Josep Hawakiwi kes üle Tartu I Kihelkonna Kohtu tarwitud oli, oli ka tänasel pääwal wäljajäänud.
 Otsus Josep Hawakiwi weel üks kord läbi Tarti I Kihelkonna kohtu ettetarwitada.
               Peakohtumees J. Sarwik
               Kohtumees       J. Stamm
               Kohtumees       M. Piiri.
               Kirjutaja Carl Palm.(allkiri)
                                
