Protokoll № 19
Haaslawa wallakohus, Haaslawa wallamajas 9 Augustil 1900 aastal.
Juures: eesistuja Johan Koort.
Tuliwad ette Haaslawa walla liige Ritsu talu № 13 omanik  Jaan Johani p. Luha ja tema tõine naene Mari Jüri t. Luha, sünd Kriis.
Mari Luha annab üles, et ta käesolewal aastal 21 Mail s.a Jaan Luhale mehele minnes kolmsada /300/rubla abielusse ühes on wiinud, mida, kui tema mees Jaan Luha enne teda piaks surema, Jaan Luhast järele jäänud warandusest enesele tagasi nõuab, kui Jaan Luha pärijatega ülespidamise andmise poolest, mis Luha 19 Mail s.a. selle walla kohtu juures sellesse protokolli raamatusse № 13 all määranud, miskisuguseid wastu tõrkumisi ehk sündimata olekut piaks ette tulema. Punase tindiga maha tõmmatud sõna, kakskümmend tuleb siis lugeda ainult: "Kolmsada rubla".
Jaan Luha annab üles et tema praegune naene Mari Luha tõeste kolmsada /300/ rubla abielusse ühes on toonud, ja et kui tema, Jaan Luha, enne oma naest piaks ära surema, siis naesel õigus on maha jäänud warandusest seda enesele wälja nõuda, kui pärijatega enam läbi ei saa.
Mari Luha ei oska kirjutada, tema eest kirjutas alla Haaslawa walla liige J. Hansen /allkiri/ 
Jaan Luha /allkiri/
1900 aasta Augusti kuu 9 päewal on ülemal olewal leping lepingu tegijate Jaan Luha ja tema naese Mari Luha, kes mõlemad isiklikult walla kohtule tuttawad Haaslawa wallakohtule suusõnal awaldatud ja Jaan Luha poolt oma käega alla kirjutatud, kuna Mari Luha eest Johan Hansen allakirjutas, mida Haaslawa walla kohus selle läbi tõeks tunnistab juure lisades, et lepingu osalistel lepingu tegemiseks seadusline wõimus on.
Eesistuja J. Koort /allkiri/
Kirjutaja J. Wäljaots /allkiri/
