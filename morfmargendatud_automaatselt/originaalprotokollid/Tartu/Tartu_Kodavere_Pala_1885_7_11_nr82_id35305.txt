Tuli ette mõisa walitseja Gleverfeld nii kui Palla  mõisa politsei ja kaebas et Johann Kossu /:Palla mõisa moonamehe / naine oma lammast lasknud mõisa rukkisse minna ja kui wälja waht Lippsan ja kirjutaja läinuwad lammast kinni wõtma wõtnud Kossu naine kiwid kätte ja lubanud kirjutajad ja wahti maha lüüa. Nõudis kohtuliku trahwi.
Ette kutsutud Johann  Kossu naine Lena Kossu ja wastas küsimise peale et tema oma lamba kraawi rukki ääre sööma pannud aga mitte kirjutajad ja wäljawahti kiwidega mahalüüa ähwardanud.
Ette kutsutud Palla mõisa kirjutaja Eduard Kusik ja andis üles, et tema walitseja käsu peale läinud lammast äratooma aga Kossu naine Lena wõtnud kiwi kätte ja lubanud teda maha lüüa kui
lammast puudub.
Ette kutsutud Jakob  Lifesan Palla mõisa wälja waht ja tunnistas niisama kui kirjutaja Eduard Kuusik.
Otsus: Lena Kossu maksab ähwardamise ja wastu  panemise pärast, mõisa politsei wastu 1 rubla/:üks rubla:/ trahwi waeste laadiku, kahe nädala sees.
Se otsus sai kuulutud ja ei olnud Lena Kossul selle wastu midagi ütlemist.
                                         Pääkohtumees O. Kangro
                                         Kohtumees       J. Soiewa
                                          Kohtumees      O. Mõisa
                                          Kirjutaja allkiri.
