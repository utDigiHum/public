Mõisa maa meeste kontraht. 
§1. Palla mõisa walitsus annab Karja N: IX mõisa maakoha Willem Wilkmanni kätte, Jüripäewast 1904 kunni Jüripäewani 1905 kelle eest Willem Wilkmann mõisale iga näddal oma töönõudega 3/kolm/ mehhe jalla päewa teeb Neljapäew Reedi ja Laupäew, kuni suwe pool aastat 74 (seitsekümetnelli) päiwa täis saavat, ja talwe pool aastat 52 (wiiskümmend kaks) päiwa  na kui tema aastas ülleültse aga 126 (sadda kakskümmendkuus) päiwa teeb.
§ 2.Kõik hooned, kaewud ja aiad, üks külwatud rukki põld ning üks künnetud ja äestatud rukki kõrre põld, igaüks 3 (kolm)  wakkaalla suur ja 12 19/25 wakkaalla hagudest puhastetud heinamaad peab Willem Wilkmann terwelt wasta wõtma ja niisamati ka kontrahti lõpetuse ajal äraandma. Kõik parandamine ehk uueste ehitamine, olgu siis elumaja ehitus (§ 4), on majaniku kohus ja ei wõi tema mõisa poolt muud materiali nõuda, kui palgid ja lattid. Maakoha kraawid peab majanik alati puhtad hoidma, et kraawi wesi wõib ära jooksta. Selle heinamaa tükki asemel mis selle kohha jaoks  Kontjalla heinamaal olli saab tema N: XIII Urrosoon.
§ 3. Willem Wilkmann peab need põllumaad säetud korra järele tarwitama ja ei tohi sest Karja N: IX talust ei sõnnikud, heinu, õlgi ega põllutoitu wälja wedada ehk ära anda, muud kui üksi need õled, mis tõiste mõisa maa meeste kattustele tarwis lähewad.
§ 4.Willem Wilkmann peab kõige Palla mõisa maa meeste hulgas nende kohtade elumajad ehitama ja materiali ilma maksuta juure wedama, selle põhja peale, mis mõisawalitsus ja majanikude selts iga ehitust ettekirjutab.
§ 5. Willem Wilkmann saab aastas 1½/poolteist /6 jalalist sülda 3/4 arssinapikkused põletamise puid ja 150/ kubbu walmis hagu, mis tema mõisa käsu peal kas mõisa ehk wõera metsas ise peab raiuma ja ärawedama.
§ 6. Kui Willem Wilkmann ilma et tema mõisale teadust annab kontrahtis säetud päewadel mõisa tööle ei tule, siis maksab nädali orjuse iga puuduwa päewad eest 50 kop. Need päewad, mis Willem Wilkmann  teeb, kui nädala orjus ettekirjutab, saawad temale suwel 40 kop. ja talwel 25 kop. tasutud. Suwe orjus arwatakse päewad Jüripäewast kunni 10 novembrini talwe päewad 10. nowembrist kunni Jüripäewani, ja saab nendel terminitel rohkem tehtud ja puudus jäänud päewade üle rehnung ärapeetud.
§ 7. Üle selle, mis § 1 nimetab, peab majanik aastas  10 wakamaad heinamaad oma hobusega tegema, niisama ka 4 wakkaalla ristikheina niitma, ja redeli peale panema 4 wakamaad rukkit ehk nisud, 3 wakamaad odrad wõi ernid ja 3 wakamaad kaerad lõikama ning kuhja panema, nii kui mõisa seda nõuab, ja saab temale nende tööde eest maksetud:
Heinamaa ja ristiku wakamaa eest 1 (üks) rubla. Talwewilja wakamaa eest 1 /üks/ rubla 30/kolmkümet/ kop. Suwewilja wakamaa eest 1 (üks) rubla.
§ 8. Kui majanik peaks enne kontrahi aega lõpetust ära surema, siis jääb se kontrahi õigus tema pärijate kätte, kuni tulewa 23. aprillini.
§ 9. Majanik peab Karja N: IX talun aasta üle pidama kõige wähemb 2 (kaks) suurt karjalehma ja 1 (üks) hobust. Kui majanik on ennast kulutanud selle talu parandamise heaks, ilma mõisawalitsuse kirjaliku tunnistuse nõudmiseta, siis ei wõi tema selle eest midagi nõuda.
§ 11. Majanikul on luba üks õuue koer pidada, koer peab aga ketti otsas peetud saama; wastasel korral on mõisa walitsusel luba neid koeri mahalasta, mis wäljas pool õuue leitakse. Kui majaniku koer wäljas pool õuue ümber hulgub, siis juhtub ka see, mis § 12 nimetab. 
§ 12. Kui majanik ühe selle kontrahtis nimetatud paragrahwide wastu peaks eksima, siis kaotab tema selle kontrahti õigust enne nimetatud aega, ja mõisawalitsusel on õigus temale siis kohe üles ütelda. Kui majanik mõisa warandust, ehk muu warguse pärast trahwitud saab, siis on luba seda kohe kohast wälja saata, ilma et tema kahju tasumist wõiks nõuda.
§ 13. Willem Wikmann wõib iga aasta oma kohta ära anda, kui tema õigel ajal, nimelt mitte hiljem kui 23. detsembril mõisa walitsusele seda  nõuu teada annab. Kui mõlemilt poolt õigel ajal ei saanud ülesöeldud, nimelt 23. detsembil enne kontrahti lõpetust, siis käib see kontraht weel üks aasta edasi.
                                    Hoonete seisus:
        1) Ellumaja korstnaga õle katuse all wana.
        2) Laut ja õlestik   õlekattuse all   hea.
        3) Ait õlestik õlekattuse all     wana.
        4) Saun  õle kattuse all wana.
        5) Õuwe ümber aiad ja kaew korralik.
            Kõik raud kram on mõisa oma 
          Palla sell 29 januaril 1904 aastal.
                          Willem Wilkman
                         C.Nemwalz
           
