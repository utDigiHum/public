Sel 28Januaril 1885
Kaibas Jürri Udel, et tema 2 põrst 10 Rubl 50 Kop est Jaan Hageli käest ostnu, need põrsat om tema poolpäiwal kodu winu ja tõisipäewal koolnuwa needsamat ärra ja nõudis omma raha tagasi.
Jaan Hagel wastutas selle päle, et temma need põrsat turru päl kaibajale müinu ja needsammat kodu wiinu, tema ei taha seda raha tagasi massa, selle et se kaub neide wahel ei olle ollnu ja põrsa müimise aigo terwe om ollnu.
Mõistetu: et se kahju kohtu poolt poles om arwata selle perra peab Jaan Hagel 5 Rubl 25 Kop Jürri Udelile 8 päiwa sehen tagasi massma.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
"   Peter Iwan /allkiri/
"  J. Krosbärk /allkiri/
