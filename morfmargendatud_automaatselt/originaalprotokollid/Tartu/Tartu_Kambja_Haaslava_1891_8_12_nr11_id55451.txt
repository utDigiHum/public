№ 11.
1891 asta Aktist № 49 wälja kirjutedo.
14 Haslawa  wallakohus sel 12 Aug 1891 a
Juures olid: Eesistuja J. Treijal
Kohtomees: Peter Iwan
"  Gottfried Meos
Tuli ette Haslawa kogokonna liige lesk  Liisa Turba, kes Tarto linnas Stapel ul. № 5 elab ja andis üles, et  tema abielusse tahab astuda ja palub selle tarwis luba. Edimatsest abielust om temal lapsi: poeg Johan 18 a wana, tüt. Anna 16 aastad wana, poeg Endrik 6 a wana. Poeg Johan ja tüt. Anna olewat tema koolitanud ja lubas ka poeg Endriko koolitada. Warandust ei ollewat esimesest abielust midagi perra jäänud, nii, et temal lapsile esimesest abielust midagi lubada ei ole.
Otsustud: et Liisa Turbale teise abielu sisse astumise tarwis luba täht wälja om anda.
Eesistuja: Jaan Treijal /allkiri/
Kohtomees: Peter Iwan /allkiri/
"  Gottfried Meos /allkiri/
Kirjutaja: J. Weber /allkiri/
