Sel 2 Detsembril 1885
Kaibas Johan Zobel, et temal 25 Rub renti raha oma rentniku Jürri Utteri käest sada om ja pallus et kohus seda massma sunis.
Jürri Utter wastutas et tema se 25 Rub renti kül wõlgu om, ent Johan Zobel ei olle kauba perra rükki kätte andno ja tarrel ei olle aknit een, selleperrast ei massa tema seda renti ärra.
Mõistetu, et Jürri Utter 8 päiwa sehen se wõlg rent 25 Rubl Johan Zobelile ärra massma peab, Jürri Utteri nõudmisse om tühjas mõista.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees J. Krosbärk /allkiri/
"  Jaan Pruks /allkiri/
