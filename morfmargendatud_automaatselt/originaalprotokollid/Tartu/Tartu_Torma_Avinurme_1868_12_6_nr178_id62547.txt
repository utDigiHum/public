Awwinorme Koggo konna kohtus sel 6mal AwwinormeDecembr 1868
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Astus ette Haljalla kihhelkonna ja Essu Walla mees Johhan Esula, ja räkis et temma Wend Jakob Essula, kes ennast Walla Negruti Mihkel Usai eest Krono teenistusse lähnud, Aastal 1866. nüid ärra surnud, ja et temma nüid kui liggemb pärrija  omma Wenna tingitud tenistusse rahha, mis weel järrele jänud Jaan Usai kätte 126 Rubla tahhab kätte sada
Jaan Usai sai ette kutustud kes räkis et temmal agga keigest weel Jakob Essulalle 124. Rubla anda. ja et temma enne sedda rahha kellegille ei anna, kui temmale üks tunnistus sest Polgust ette näidatakse., et Jakob Essula tõest surnud on.
Moistus
Koggokonna kohhus kirjutab selle asja pärrast Essu Moisa Wallitsussele, kust tunnistust antakse. et kas se innimenne tõeste selle rahha pärrija on. kust ka ühtlaisa üks Polgo tunnistus seie sadetud peab sama et Jakob Essula surnud on, kus siis koggo konna kohhus sedda nimmetud summa Jaan Usai käest wälja nõuab.
Mart Jaggo
Mart Liis 
Tomas Pärn
