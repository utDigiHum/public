1911 goda Avgusta 18 dnja dan ja, Avgust Karlov Rozental, siju rospisku v tom, što polutšil ot brata mojego Petra Karlova Rozental trista (300) rublei, kakovõja dengi ja imel polutšit ot nego soglasno §3 nasledorazdelnago akta, o nasledstve mojego pokoinago otsa Karla Romanova Rozental, javlennago vo Vrangelskom volostn. sude 4 dekabrja 1908g. za N 8. Pri etom izljavljaju soglasije na pogašenije sih trehsot (300) rublei po krepostnõm knigom JurjevoVerroskago Krep. Otdelenija na usadbe Kulla, krep. N 1388 pod im. StaroVrangelsgof, Jurjevskago u. 
Krestjanin Avgust Karlov Rozental [allkiri]
Tõsjatša devjatsot odinnadtsatago goda Avgusta vosemnadtsatago dnja akt sei javlen k zasvidetelstvovaniju Vrangelskomu Vol. Sudu krestjaninom Avgustom Karlovõm Rozental,  živuštšim v St. Vrangelskoi volosti, litšno vol. sudu izvestnõm i imejuštšim zakonnuju pravosposobnost k soveršeniju aktov; pri etom vol. sud udostoverjajet, što akt sei podpisan sobstvennorutšno imeže Avgustom Rozental.
Predsedatel suda: K. Pala [allkiri]
Tšlenõ suda: P Keis [allkiri] JVirmann [allkiri]
Pisar: J Weske [allkiri]
N. 10
