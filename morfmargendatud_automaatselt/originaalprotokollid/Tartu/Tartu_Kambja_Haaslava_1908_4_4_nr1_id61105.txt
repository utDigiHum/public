№ 1.
Haaslawa wallakohus   Haaslawa  wallamajas 4 Aprillil 1908 a.
Juures: esimees  Willem Põderson 
Kohtuliikmed Jaan Rosenthal, Peeter Reino
Tuliwad ette  Kawilda walla liige Mihkli lesk Ann Martinson ühelt ja Jamburgi linna kodanik Johann Märdi p. Rootsi tõiselt ja Haaslawa walla liige Ewerti talu omanik Johan Hendrike p. Kõõra kolmandalt poolt, paluwad alljärgnewat suusõnalist lepingut selle kohtu lepingute raamatusse kirjutada ja ustawaks tunnistada.
Leping on järgmine:
"Mina, Ann Martinson, müüsin oma elumajakese, mis Haaslawa wallas Kurepalus Ewerti talu maa pääl on, oma wäimehele Johan Rootsile kahekümne rubla eest ära. Selle tingimisega, et Rootsi mulle selles majakeses kuni minu surmani prii korteri piab andma. Suwe kuudel wõib Rootsi maja seesmised ruumid suwitajatele wälja üürida."
Johan Rootsi on nende tingimistega rahul ja kohustab täitma. Pääle selle tõutab maaomanikule Johan Kõõrale, et tema sinna majasse niisugusid inimesi elama ei lase, kes kõlwatu elukombetega ja maaomanikule ehk naabritele kuidagi pahanduseks on ja et kui maaomaniku ehk walla ametnikude hoiatused tähelepanemata jääwad, siis piab Rootsi maja selle maa päält ära wiima.
Selle pääle awaldas Johan Kõõra lepingu kohta rahul olemist.
Leping hakab maksma tänasest päewast ja wältab kuni 23 Aprillini 1928 a. Rootsi peab iga aasta Aprillikuu sees terwe aasta rendi wiis rubla ette ära maksma. Maa piirid, mis Rootsi tarwitada jääb, on täna Rootsile kätte näidatud.
Lõppeks lepiwad Rootsi ja Kõõra kokku, et kõik kohtu asjad, mis selle lepingu kohuste täitmata jätmisest tõusma saawad, selle waranduse olu kohalises kohtus seletatud piawad saama, selle pääle waatamata, kus üks ehk tõine lepingu osaline tollel ajal elab.
Juhan Rootsi /allkiri/
J. Kõõra /allkiri/
Ann Martinson ei oska kirja, tema palwe pääle kirjutas alla Haaslawa walla liige J. Laber /allkiri/
 Haaslawa wallakohus selle läbi tunnistab, et ülemal olew leping  suusõnal kohtule ette on kantud; lepingu osaliste Johan Rootsi ja Johan Kõõra poolt oma käega alla kirjutatud, kuna Ann Martinsoni eest, kes kirja ei oska, Haaslawa walla liige Johan Anne p. Laaber allakirjutas. Kõik lepingu osalised elawad Haaslawa wallas ja on kõik selle kohtule nagu lepinguwõimelised palelikult tuttawad.
Haaslawal, 4 Aprillil 1908 a.
Kohtu esimees:  W. Põderson /allkiri/
Kohtu liige: J. Rosenthal /allkiri/
P. Reino /allkiri/
Kirjutaja: J. Wäljaots /allkiri/
