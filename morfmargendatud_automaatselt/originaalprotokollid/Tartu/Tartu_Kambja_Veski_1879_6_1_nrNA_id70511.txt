Sel 1. junil 1879.
                            Päkohtomees abbi Johan Kittus
                             Kohtomees, Jaan Pelowas
                       abbi   -  " -         Johan Androwitz
Weskimõisa kog. kohto abbi kohtomees Johan Androwitzale om Weskimõisa wallast Mutte Johan Ellen, sel 23tal aprillil 1879. Weskimõisa kõrtsin kõnnelnu, ja üttelnu, kus te sedda wisi ollete tennu, tee ollete wäega alwaste päkohtomehhele tennu, mina piddasi sinno õige meh-
hes, nüid ollet sinna ka kurrati passipossigute sekka lönu, ja ollete kik nüid üttesuggutse pasiposino. Kurrati wallawannemb, särane sama passiposik   Kas te tõiste es wõi tetta, milles tee ollete päkohtomehele 100. rbl trahwi mõistnu nimelt latse toitmise rahha, kas ta teid om ärrä narnu
Tõiselt om ka Johan Ellen kohtomehel Jaan Pelowassele Pusso kõrtsin kõnnelnu, et mis tee ni halwaste ollete päkohtomehele tennu 
Kas ta teid om ärrä narnu, mis sul sest kops täis om, sedda kuulse ka walla wöölmonder Jaan Alleksandersohn mes Johan Ellen kõnnel.
Johan Ellen üttel, et minna ei olle Johan Androwitzili middagi kõnnelnu Weskimõisa kõrtsin, ja ei olle ka kohhut ei wallawanembat teota-
nud. Ja Jaan Pelowassile ei olle ma ka midde latse toitmise rahha masso perrast middagi kõnnelnu, mina kõnneli selle kohto eenistmise ärrä wõtmise perrast kõnnelnu Pusso kõrtsin, et ta saap essike süggise aja lõpsil walla, mis teil sis waja selle asjale ni päle tükki tarwis om.
Kohhos mõistis, et Johan Ellen saap selle kohto trotsmise eest kik süid temma palwe peale andis antud ja kohholt ärrä leppitus.
                                                    Päkohtomehhe abbi Joh. Kittus  XXX
                                                             Kohtom.      Jaan Pelowas  XXX
                                                   abbi      -"-         Joh. Androwitz  XXX
