Haslawa Kogukonna Kohus sel 19 Nowembril 1884.
Man ollid: Päkohtumees: Jaan Link
Kohtumees Johan Klaosen
"  Peter Iwan
"  Johan Grossberg
Kaibas Jürri Wirro et temal 2 wakka Ruki ja 2 waka keswi Tiko Johan Udeli käest sada om ja pallus et kohus seda massma sunnis.
Johan Udel wastutas selle päle et kaibaja 5 nädali est tenistusest ärra pagenu om, tema pidanu Märtsini ollema ja om warsti päle Mihklipäiwa ärra lännu ja Suwel jänu 5 nädalit tenistusest ärra, tema ollnu silmiga haige kui kaibaja omma aija ärra teninu olles sis olles tema 2 wakka Rükki ja 2 wakka Keswi sanu ja jättis kohtu holde selle puduwa aija est palkast maha wõtta. Kaibaja om allati wastaline ollnu.
Jürri Wirro wastutas et tema suwel enegi 1½ nädalit haige om ollnu ja päle Mihkli päiwa aijanu Johan Udel tema ärra.
Johan Udel andis ülles, et Jürri Wirro ütten ärra winu 4 amet ja 1 ame sälgan, 1 särk, püksi, watt, saapad ja 1 paar tsuwi ja pallus et Jürri Wirro sunitu saas neid tagasi andma.
Jürri Wirro andis ülles et tema 1 amme mis sällan ollnu ärra tonu om, temal jänu omma amme sinna ja särk, püksi, watt ja saapa om tema käen neid annab kätte. Tema om tsuwa ärra pidanu ja jänuwa need tsuwat neide assemile.
Tiko tallo tüdruk Ann Wirkus tunistas, et Jürri Wirro om suwel 3 ehk 4 nädalit silmiga haige ollnu ja om päle Mihkli päiwa ilma aijamada tenistusest ärra lännu, asju wargusest ei tea tema midagi ja et Jürri Wirro kül enne ärra minekit perrremehega tülli allustanu.
Mõistetu: et 1 wakk Keswi selle ärra wiedetu aija est perremehe heas jääb, mis perra sis Johan Udel 2 wakka Rükki ja 1 wakk Keswi 8 päiwa sehen Jürri Wirrole ärra massma peab sis peab Jürri Wirro se särk, püksi, watt ja saape Johan Udelile tagasi andma, mis tema ärra tonu om. Muide Johan Udeli kaibuse om tühjas mõista.
Se mõistus sai kulutado ja õpetus leht wäja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees: Johan Klaosen XXX
Peter Iwan /allkiri/
J. Krosbärk /allkiri/
