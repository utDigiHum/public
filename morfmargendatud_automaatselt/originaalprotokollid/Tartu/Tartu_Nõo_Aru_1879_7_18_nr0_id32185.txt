Arro Kohto majan sel 18 julil 1879. Kaan olliwa walla nöumehhe.
Nöumeeste Kaggole pand wallawannab ette, et magasi aida kattus sant om, mes toest nüüd prawitud peab saama ja sepäle arwasid nöumehhe, et ni suuri Kattus Kiwwe Kussgi sada ei olle, kellep sedda kattust parandada wois; seperrast Kiwwi Kattus mahha wotta ja sihindli Kattus magasi aidale päle katta nink wanna Kattuse Kiwwi okssüni pääl ärra wiiwwa.
                Nöumeeste arwaminne sai haas arwatus; nink jääb walla wannamba holele, sindri Kattuse Katjat murretseda.
                Nöumehhed:    Johan Jakobson
                                           Johan Pulk
                                           Peter Ewerdson
                                           Peter Jönson
                                           Mihkel Kukkemelk
