Sel 6 Junil 1888
Haslawa mõtsaherra Holz kaibas mõisawallitsuse nimmel et järrelseiswa perremeh wõlgu om:Talu			Peremees			Rbl			Kop		Mikko			Johan Opman			146			48		Jugari			Jaan Klautz			58			55		Mällo			Mihkel Iwan			62			20		
ja pallus et Kohus sel nädalil massma sunis, seal om Kredit kassa prozenti ja mõisa prozenti wõlg.
Johan Opman wastutas, et tema se raha wõlgu om ja lubas massa ent pallus kannatada.
Jaan Klautz ja Mihkel Iwan pallusiwa ka selle massuga kannatada ja lubasiwa pea ärra massa.
Mõistetu: et  Johan Opman, Jaan Klautz ja Mihkel Iwan 4 nädali sehen se wõlg ärra massma peawa, mis neile kulutedo sai.
Päkohtumees: Johan Link XXX
Kohtumees: Jürri Kärn XXX
"  Jaan Hansen /allkiri/
