Se külla karjus Endrik Jaska kaibap: temmal ollewat weel karjusse palk saada Käo Jaan Opmanni kaest ütte ahtra lehma eest 1/6 wak keswi ja päle Mihklipäiwa karjan käimisse eest 1 wak kartoflit. Jaan Märteni kaest katte wallaleste ellajate eest 1 külmit keswi.
Endrik Jaska nowwap kiik warsti kätte.
Jaan Opmann wastutap, et nende kaup ei ollewat mitte nendawiisi olnut. Temma ei ollewat mitte enda lehmi päle Mihklipäiwa ännamb karjan saatnut.
Jaan Märten wastutap, et nende kaup ei olle nendawiisi olnut.
Se perremees Jaan Soo tulli ette ja tunnist: kaup olnut karjussega, kas kiik saadawa woi ei pöle Mihklipäiwa ellajat karja, kui karjus ennegi karja lähhäp, siis piawa perremehhi 1 wak kartoflit massma. Eggaütte wallalesse woi ahtra lehma päält 1/6 wak keswi.
Moistus: Jaan Opmann piap warsti 1 wak kartoflit ja 1/6 wak keswi, Jaan Märten agga 2/6 wak keswi Endrik Jaskal ärra massma.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
