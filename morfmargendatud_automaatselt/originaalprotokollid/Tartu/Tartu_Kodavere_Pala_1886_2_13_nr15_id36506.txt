Tuli Karel Tragun ja kaebas et endine pääkohtu mees Otto Kangro temale 350 rubla wõlgu olla. Sest 29 Oktobril m. a. oli Otto Kangur sel ajal kui Traguni warandust keelu alt lahti tehti seda summad temale maksa ja ka üks käekiri selle peale wäljakirjutada lubanud. Peale selle nõudis weel 3 rubla selle eest, mis  iired tema poodis sel ajal kui pood kohtu pitsati all kahju teinud. Summas 350 rubla.
Tunnistajaks nimetas Josep Soiewa Otto Mõisa ja Kustaw Wipper nii kui ka wallawanem.
Ettekutsutud Josep Soiewa ja wastas küsimise pääle et tema seda mitte kuulnud ei ole et Otto Kangro Tragunile 350 rubla maksa lubanud.
Ette kutsutud  Otto Mõisa ja tunnistas niisama kui Josep Soiewa.
Ette kutsutud wallawanem Gustaw Kokka ja wastas küsimise pääle nii kui Otto Mõisa.
Otto Kangro ei olnud tulnud.Otsus: Otto Kangro tulewa kohtupääwaks ette tellida.
                                           Pääkohtumees      J. Hawakiwi.
                                                  Kohtumees     M. Piiri.                                                     
                                           abi  Kohtumees     J. Taggoma.                    
                                                  Kirjutaja     allkiri.
                     
