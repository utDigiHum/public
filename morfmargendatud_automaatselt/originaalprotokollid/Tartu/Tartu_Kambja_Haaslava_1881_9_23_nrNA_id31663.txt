Haslawa kogokonna kohus 23 Sept. 1881.
Juures istus:
peakohtumees  M. Klaos
kohtumees Joh. Link
dito  Rein Anko
Tuli ette Wana Koijola Ruusa talu peremees ja edespidine Haslawa walla Kortsu talu päris omanik Johan Black ja andis protokolli, et tema oma liikuwa ja liikumata, nüüd olewa ja eenpidi saadawa warandusega Kortsu talu magatsi wõlgade eest kuni 1000 rubl. wastutab ja lubab tema nimelt see wõlg kuni 1se Decembrini 1881 walla walitsusele ära maksa. Pääle see andis ka Krimanni walla Roijo weskimaa ja Weski pärisomanik Jüri Klaos protokolli, et ka tema see nimetud magatsi wõla eest (1000 rubl.) oma liikuwa ja liikumata, nüüdse ja eenpidi saadawa waraga wastutada lubab ja lubab, kui Johan Black seda wilja wõlga mitte nimetud terminis walla walitsusele ära ei maksa seda essi wälja maksa, aga jääb tema (Jüri Klaos) siis see Kortsu talu pärisomanikus, misga Johan Black ka rahul oli ja kelle tunnistuses nemad omad nimed alakirjutawad.
Kortsu tallo perris ommanik Johann Black /allkiri/
Kriimanni Roijo pärismõisapidaja Jüri Klaos XXX
Otsus:
Seda niikui sündinud protokolli üles panna.
Peakohtumees Mihkel Klaos  XXX
Kohtumees Joh. Link XXX
dito  Rein Anko XXX
