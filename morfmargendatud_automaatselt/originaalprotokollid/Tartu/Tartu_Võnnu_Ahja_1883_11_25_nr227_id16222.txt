Hindrik Laar palus, et Jaan Saawast karta on, et tema kõik kraami majast wälja laotab, seda Jaan Saawa kraami ja nimelt linad ja hainad kinni panda.
Mõiste: Hainad ja linad saawad Jaan Saawal kinnipantud. Linad aga peawad warju alla weetud saama ja wõib neid ka ära arida.
Pääkohtumees: Peeter Kripson XXX
Kohtumees: Peeter Hindrikson XXX
Kohtumees: Kota Wossman XXX
Kohtumees: Kusta Iwanthal XXX
Kohtumees: Johan Kiljak
Mõiste sai Hindrik Laarile ja Jaan Saawale kuulutatud olid mõlemad rahul.
Kirjutaja: Chr Kapp &lt;allkiri&gt;
19. Oktobril 1885 Tartu-Wõru kreissiskali abile sisse saadetud /ärakiri/
