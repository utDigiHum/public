Awwinorme Wallakohtus sel. 13 Junil 1892.
koos olid
peakohtumees Jaan Kallaus
kohtumees Jaan Paas
kohtumees Josep Saar.
Et siitwalla taluperenaine ärasurnud Tomas Laurissoni lesk Marri Laurisson on omad talumaad pooletera peale annud. siis olid lesk Marri Laurison ja tema Wölmöndrid Jaan Ambos, ja Mihkel Laurison ning pooleteramees Purmanni walla inimene Jaan Männiksaar ette tulnud ja palusid wallakohut nende kaupa ja kontrahti kinnitada nenda kui jarelseiswalt on üles woetud.
Kontrakt.
1. Marri Laurison ja tema Wöölmöndrid Jaan Ambos ja Mihkel Laurison annawad Laekanno Jakobrahwa tallu Puurmanni inimese Jaan Männiksaarele ühe Aasta peale poole tera peale. ja saab keik seeme Majaperenaise poolest antud mis süggise saab jälle peksetud wiljast pealt ära woetud ning siis põllu saak pooleks jautud
2.) Keik põllutöe ja sõnniku weddu on pooletera mehe asi ja teeb tema need töed keik oma töe riistadega ja saab keik põllupõhk poolest woetud, nendasamuti teeb pooleteramees keik hein ära teggema ja saab hein pooleks woetud ja weddab pooleteramees heina koddu, üks tükk heinamaad kümne sado alla jäeb maja perenaisele ja teeb tema isi se hein ära. ja ei tohi põllupõhku egga heinu müidud saada.
3.) Keik wallatööd ja weddu mis selle taupealt teha tuleb peab pooleteramees ära tegema ja ka põlendmajade tarwis materiali weddama. raha maksmine on maja perenaise asi nendasamuti peab pooleteramees teed teggema ja sildu parantama.
4). Keik maksud mis selle talu pealt maksa tuleb Õppetajale köstrile ja krono rent maksab maja perenaine
5). Taluhonetest saab suur laut taluperenaisele kaks weikemaad lauta poole teramehele, pooleteramehe saab üks Ait wilja ja kraami tarwis:
6). Kui tuul peaks kattusid rikkuma siis peab pooleteramees neid parandama. nendasamuti peab pooleteramees aedasid kohendama
7) Keik põletuse puud. ja haggu saab talumetsast ja peab pooleteramees need raijuma ja weddama. rehepuud sawad ostetod ja maksab pooleteramees ühe kuppitsa eest ja taluperenaine teise kopiki sülla eest raha, poole teramees weab need koddu ja raijub
8). Karjapois saab palka 16 rubla. ja maksawad maja perenaine ja pooleteramees palk pooliti kumbki 8 Rubla. et se karja pois agga pooleteramehe poeg. ja tema oma isa riidid keik suwwi kannab siis maksab maja perenaine weel riiete raha 2 Rubla juure ülepea 10 Rubla ja södawad nemad karja poisi teine teine näddal.
9). Üks tükk metsa porri ligude soos kust woib heina suggu teha jaeb pooleteramehele. ja woib tema sedda kellegile heina tegemiseks wälja anda. kes temale aitab heina teha
Selle kontrakti kinnitamiseks on need kontrahti teggijad omad nimed alla kirjutanud. 
Marri Laurison XXX
Jaan Ambos XXX
Mihkel Laurison XXX
Jaan Männiksaar (allkiri)
Otsus
Wallakohus kinnitab sedda kaupa ja kontrahti et se muutmatta jäeb.
Jaan Kalaus. (allkiri)
Jaan Paas. (allkiri)
Joosep Saar (allkiri)
Kirjutaja Schulbach (allkiri)
