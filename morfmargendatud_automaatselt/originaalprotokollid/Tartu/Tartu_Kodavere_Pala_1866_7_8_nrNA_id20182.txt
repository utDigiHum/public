Siin saab se sinnane Kauba Kontrat Palla Mõisa Koggokonna Kohtu poolt Prottukulli tunnistuse alla ülles pantut. Moisa Wallitsuse lubbaga sedda wisi; et seal sammas hingekirjas Seisaw:Pois Josep Rosenpärk saab Koggokonna, Puumeistri:Ammmetmehheks.
Nekruti Losiwõtmise numri alt Lahti ollema. Selle Wisipeäl et temma kui täieste mõistlik ja ka enne sedda kõlplikko,Maja
ehitamise tööd on Koggokonnas teinud. Peab ei mitte Kõrkema inna ehk Kassu Wõtmisepeäle lootma. Waid omma tööd igga kort
nii, sugguse parraja innaga mis Koggokonna wannematte polest arwatakse Wäärt ja õike ollewat ärra teggema ja issi ennast igga
kort nii, sugguses tenistuses piddama et Kui Koggokond tedda tarwitab kohhe sadawal on.
Pea Kohtumees Jaan Welt+++Kohtumees Tõnnis Soiewa+++Koggokonna Wöörmönter Kustaw Kokka+++ Kirjutaja Jaan Kukk.
