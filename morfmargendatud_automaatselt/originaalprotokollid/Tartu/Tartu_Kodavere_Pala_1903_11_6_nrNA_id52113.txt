Batrakski dogovor na mõznuju zemlju.Segodnjašnjago dnja meždu Galliskim mõznõm upravlenijem i krestjaninom Mihkel Mogom blagoobdumano uslovlen i napisan nižesledujuštšiji dogovor:
§1. Gallikskoje mõznoje upravlenije sdajet mõznuju batrakskuju usadbu Juhkami, stoimostju v 6 talerov 16 17/112 grošei, v arendu Mihkel Mogom srokom na 2 goda stšitaja s 23 aprelja 1904 goda  
po 23 aprelja 1906 goda.
§ 2.Mihkel Mogom platit za nazvannuju usadbu denežnuju arendu vsego po- rub.-kop.  v god, a imenno: 1go aprelja- rub.- kop. i 1go oktjabrja-rub-kop. vpered i objazan krome togo ježegodno bezplatno proizvodit sledujuštšija rabotõ, a imenno:
1) žat i klast v skirdõ 6 lofštelei ozimi:
2) žat 8 lofštelei jarovago  hleba ili goroška;
3) kosit 3 lofštelei klevera;
4) kosit 10 lofštelei sena;
5) kosit - lofštelei goroška;
6) sobirat........ lofštelei kartofelja, kotorõi svozit tuda, kuda mõznoje upravlenije prikažet;
7) svozit........- sažen...... 1 aršinõh drov............
8) uravnivat 100-150 saženei dorogi tam, gde mõznoje upravlenije prikažet;
9) 83 peših dnei, iz kotorõh dolžen postavit 45 dnei do Mihailova dnja;  ostalnõje v tetšeniji zima. Za každõi nepostavlennõi rabotšiji den  uplatšivajet on letom 75 kop. i zimoju 50 kopejek.
Ispolnenije oznatšennõh rabot razrešajetsja soglasno § 168 Položenija o krestjanah Lifljandskoi guberniji ot 13 nojabrja 1860 g. Za každõi rabotši den, postavlennoi  boleje uslovenago uplatšivajet letom 40 kop. i zimoju 35 koppek.
§ 3.O brevnah i žerdjah na potšinku ili vozvedenije strojeniji arendator batrakskoi usadbõ dolžen zajavit do Mihailova dnja i objazan on podtšinjatsja vo vseh otnošenijah lesnomu ustavu; v slutšaje narušenija im sego ustava, on budet lišen svojei usadbõ.
§ 4. Arendator batrakskoi usadbõ  objazan vesti hozjaistvo v sdannoi jemu v arendu usadbõ po nadležaštšim zemledeltšeskim pravilam i sdat usadbu imeniju obratno po istetšenija sroka sego dogovora v sledujuštšem porjadke:
              a) vse strojenija i v osobennosti ih krõši dolžnõ bõt horošo potšinenõ;
               b) ržanoje pole dolžno bõt horošo obrabotano i svojevremenno obsemeneno horošim semenem;
               v) zaborõ dolžnõ bõt horošo potšinenõ;
               g) iz rabot k buduštšemu ekonomitšeskomu godu ržanoje požnivo dolžno bõt vspahano i horošo vsboroneno;
              e) On polutšajet 3 saženi 1 aršinnõh drov dlja toplivam i 5 saž. hvorosta. Za rubku uplatšivajet  arendator usadbõ. Krome togo polutšajet 2 lofš. senokosa.
§ 5. V slutšaje jesli batrak po prikazaniju mõznago upravlenija vozvedet novõja strojenija ili potšinit starõja, to eto on proizvodit na svoi stšet, ne imeja prava trebovat kakogo libo za eto voznagraždenija na kakovuju postroiku mõznoje upravlenije dajet bezvozmezdno brevna, kotorõja vse batraki, sorazmerno velitšine ih zemli , dolžnõ dat bezvozmezdno solomõ na krõšu soglasno istšislenija mõznago upravlenija. Za proizvodstvo kamennago fundamenta pod  hlev mõznoje upravlenije uplatšivajet po kvadratnõm  sažnjam.
§ 6. Svoi sobstvennõja polja arendator mõznoi batrakskoi usadbõ  dolžen obrabotõvat v tšetõreh tšastjah, tak tštobõ nahodilis 3 lofšteli pod rožju, 6  lofšteli pod jarovõm hlebom, 3 lofšteli pod parom, pritšem on ne imejet prava zasevat par jarovõm hlebom ili lnom.
§ 7.Solomu, seno i drugoi korm dlja skota arendator sei usadbõ ne imejet prava komu nibud ssužat ili prodavat.
§ 8. Mõznoje upravlenije imejet pravo revizovat hozjaistvo usadbõ vo vsjakoje vremja ili samo ili tšerez svojego upolnomotšennago.
§ 9. Arendator usadbõ imejet pravo zasevat lnom tolko 1½ lofšteli.
§ 10. Jesli mõznoje upravlenije poželajet arendatoru sei usadbõ sdelat otkaz v onoi, to etoi dolžno posledovat 23. oktjabrja do istetšenija sroka sego dogovora, pritšem arendator usadbõ ne imejet prava trebovat voznagraždenija za kakija libo rabotõ ili izderžki.
§ 11. V obezpetšenije totšnago ispolnenija prinjatõh na sebja objazatelstv arendator etoi batrakskoi usadbõ zakladõvajet vse svoje imuštšestvo, a v osobennosti svoi železnõi inventar, kotorõi dolžen sostojat po krainei mere iz  odnoi  lošadi i dvuh golovõ rogatago skota i nahoditsja v horošem sostojaniji.
§ 12. V slutšaje esli arendator sei usadbõ budet obraštšatsja  neostorožno s ognem i tšerez eto proizoidet požar, to on otvetšajet za proisšedšije ubõtki vsem svoim imuštšestvom. Krome togo mõznoje upravlenije s arendatorom onoi usadbõ  ještše v sledujuštšem:
         1) Arendator usadbõ prinimajet na sebja uravnivat.... sažen...dorogi;     
         2) Arendator usadbõ nedolžen bõt zametšen v kraže, a takže, ne dolžen prinimat v svojei usadbõ vorov ili kradennõh veštšei;
         3) Jesli žilaja riga po vethosti prijidet v plohoje sostjanije, to imenije vozvodit takovuju na svoi stšet, pritšem odnako arendatorõ usadeb mõznoi zemli vse vmeste dolžnõ sorazmerno velitšine svojei zemli, bezvozmezdno podvozit trebujemõi material; ravnõm obrazom oni dolžnõ podvozit material takže i na potšinku žiloi rigi.
§ 13. Dlja otbõvanija dnei, arendator usadbõ dolžen javljatsja každõi raz nemedlenno po prikazaniju imenija. Pri molotbõ zimoju s 6 tšasov utra do 7 tšasov vetšera.
§ 14. Arendator usadbõ ne vprav dopuskat, tštobõ sobaki svobodno šljalis. Každaja sobaka, kotoruju vstretjat, vse ravno gde vne granits usadbõ, budet nemedlenno ubita.
§ 15. V slutšaje neispolnenija arendatorom usadbõ kakogo libo iz oznatšennõh v sem  dogovor punktov, on lišitsja svojei usadbõ, ne imeja prava žalovatsja na to v sud ili trebovat voznagraždenija.
§ 16. Sostojanije arendnoi usadbõ  Juhkami. 
1) Žilaja riga novaja.
2) Ambar......................................
3) Hlev starõi.
4) Banja......................................
5) Kuhnja.................................... 
6) Kolodets sredniji.
7) ..............................................
8) Zabor vokurg dvora v horošem sostojaniji. 
 Tšto nastojaštšiji dogovor vpolne protšitan i razjasnen arendatoru Mihkelju  Mogom i tšto pomjanutõi arendator vpolne dovolen so vsemi võšeoznatšennõmi uslovijami, on udostverjajet svojego podpisju.
          Tak soveršeno v 6 nojabrja 1903 goda. 
                                 Arendator batrakskoi usadbõ: Mihkel Moggom. 
                                 Ot imeni mõznago upravlenija: E. Bauman.          
