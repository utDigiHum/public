Kohtumees Josep Hawakiwi kaebas, tema on kewwadel Sarewalla poisiga Kustaw Kirtsi aasta tenistuse kaupa teinud ja se on tema jurde tenima tulnud aga mõnne nädala pärrast kui põllu töö on
alganud läinud tema ööse sallaja ära ja warrastanud 1. tükk nahka ja ühhed uued romad, kes teab mis muud.  Jakup Waddi kaewas. Kustaw Kirtsi wastas tema on kül issi ära läinud ja need nimetud asjad warrastanud aga mitte muud. Joseb Hawakiwi nõuab kaubeltud aasta palk taggasi, ja need roomad mis Kusta Kirtsi ütlep alles ollema ning seaduse järgi trahwi ja nahha eest 80 Cop  h. se palk mis tema piddi sama on rahha järgi 28 Rubl. ja üks paar püksa mis Gusta on ärra winud 1 Rub.
 Kohhus mõistis: Et Kustaw Kirtsi oma perremehhe jurest on ärra kadunud peale tema se tingitud aasta palk se on 28 Rbl. käsi raha 1 Rbl. , 1 p. püksa 1 Rbl., 1 tük nahku 80 Cop. ja romad taggasi. Suma 30 R. 80 Cop. Warguse trahwiks saab Kustaw Kirtsi 15 witsa lööki. Se mõistus sai säduse järgi ette loetud.
                Pea kohtumees Kustaw Kokka +++
                     Kohtumees Kustaw Nukka  +++
                     abbi Tomas Welt                  +++
                     Kog. Kirja. Fucks.
