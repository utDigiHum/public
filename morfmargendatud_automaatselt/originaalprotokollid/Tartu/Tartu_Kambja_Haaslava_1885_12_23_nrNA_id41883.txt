Sel 23 Detsembril 1885
Selle otsuse päle sest 9 Detsembrist s.a Johan Linki kaibuse asjan wastu Jaan Roiland es olle Unikülla Kõrzinaene tullnu ja om Kohtumees Jaan Pruksile kõnelnu, et tema wähamb nännu om kui kõrzimees.
Mõistetu: et Jaan Roiland 5 Rubl walloraha Johan Linkile 8 päiwa sehen selle lögi est massma peab ja päle selle 1 Rubl waeste heas ja Johan Link peab 1 Rubl waeste heas 8 päiwa sehen massma, selle et temma Jaan Roilandi sõimanu om.
Se mõistus sai Johan Linkile kulutedo ja õpetus leht wälja antu. Se mõistus sai 30 Detsembril s.a Jaan Roilandile kulutedo ja õpetus leht wälja antu.
Kohtumees:  Peter Iwan  /allkiri/
"  J. Krosbärk /allkiri/
"  Jaan Pruks /allkiri/
