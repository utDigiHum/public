Mari Michelsoni kaebduses wastu Michel Müller, nõudmise pärast, kaebatud protokoll Nr 323.
Kutsuti ette Karl Müller. Tema ütles, et temale ei olla mitte minewal kewadel posti tee tegemise käsku saanud.
Tema ei teada sellest midagi. Johann Püss ütles, et tema ja Jaan Tiidt olnud ja on Jaan Tiidt Animatsi salul muru pääl
Karl Müllerile tee tegemise käsk kätte andnud.
Otsus: Jaan Tiidt, kes täna puudus on, weel teiseks korraks ette kutsuda.
