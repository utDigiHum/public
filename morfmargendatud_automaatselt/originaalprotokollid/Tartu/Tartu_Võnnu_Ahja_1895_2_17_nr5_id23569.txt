Tuliwad ette Jüri Puusepp, Peeri wallas elamas ja Hindrik Mällo Ahka wallast ja palusiwad järgmist lepingut akti raamatusse kirjutada:
Jüri Puusepp müüb oma maja, mis Ahja wallas, Kõnnu külas Juhkami talu maa pääl on Hendrik Mällole 33 rubla eest ära. Sellest on ära maksetud 13 rubla ja wõlgnew 20 rubla saab maksetud tänasest päewast arwatud 2 aasta jooksul. 10 rubla maksab 10. Nowembril 1896 aastal ja 10 rubla 10. Nowembril 1897.
Jüri Pusep &lt;allkiri&gt;
Hendrik Mällo, aga et esi kirjutada ei oska, siis tema palwel: Hendrik Tork &lt;allkiri&gt;
Et kohus lepingu tegijaid tunneb ja et Jüri Puusepp esi oma käega ala kirjutanud ja Hendrik Mällo eest Hendrik Tork saab sellega tunnistatud.
Eesistuja J Kooskord​​​​ &lt;allkiri&gt;
Kirjutaja Puusepp &lt;allkiri&gt;
1898 a. Detsembrikuu 3 päewal maksis Hendrik Mällo Kümme rubl. 'ra ja jääb weel wiis rubl. Puusepa lese eest: Juhan Zeikar &lt;allkiri&gt;
Eesistuja J Willipson &lt;allkiri&gt;
