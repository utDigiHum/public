Ette tulli Hans Kirs ja kaibas tema olla 2 näddalat tagasi  kodust ära olnud ja oma magamisetoa uks olnud temal nööriga kinni seotu olnud. Kui tema koddo tulnud olnud toa uks lahti ja  tema nöör ära.
Tema tüdruk Lena Tralla ja ka pois Johann Sokk olla  niisama Kustaw Kasik Maddis Waddi  kes koddu on olnud üttelnud, et Gusta Edro olla seal toas käinud ja selle ukse lahti teinud ja ka seal toas käinud, siis peab tema selle nöri ärra wõtnud ollema.
  Gustaw Edro andis wastust Tema olla kül  Kirsi toas käinud seal tükki aega juttu ajanud ja ka perrast küssinud mis kel on ja lükkanud kül ka see ukse lahti aga pole mitte kella näinud ei ka wõtnud ja Kirsi oma pois läinud temaga ühes sinna tuppa aga tema ei tea nööri. 
 Maddis Waddi tunnistas: tema on küll Kirsi köögis olnud ja näinud et Kirsi uks on nööriga kinni olnud ja ka et Gustaw Edro see toas on käinud aga nööri wõtmist tema ei tea.
 Kohtu otsus: Et tunnistajad Kusta Kasik, Leena Tralla ja Johann Sokk siin ei olle saab teine kord eddasi tallitud. 
                     pä Kohtumees: Hindrek Horn
                          Kohtumees: Märt Piri
                          Kohtumees: Josep Soiewa
