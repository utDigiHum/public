Haslawa Kogukonna Kohus sel 23 Detsembril 1886
Man ollid: 
Kohtumees Jürri Kärn
"  Johan Opman
"  Jaan Hansen
"  Johan Raswa
Selle protokolli päle sest 1 Detsembrist ja Quote maa tallude müimise perrast tullit ette
1. Zirgaste tallo rentnik Kristjan Järgewitz massis 580 rubl käeraha ja 30 rubl Kontrakt § 14 perra Kontrakti raha mõisawallitsuse Herra Holzi kätte sisse.
2. Tilba tallo rentnik Jaan Rosenthal massis 320 rubl Käeraha ja 30 rubl Kontrakti raha mõisawallitsuse Herra Holzi kätte sisse.
3. Molliko tallo rentnik Peter Duberg massis 520 rubl ostmise käeraha ja 30 rubl Kontrakti raha mõisawalitsuse Herra Holzi kätte sisse.
4. Krisa tallo rentnik Jaan Link massis 446 rubl 66⅔ kop ostmise käeraha ja 30 rubl Kontrakti raha mõisawallitseja Herra Holzi kätte sisse.
5. Peetso tallo rentnik Johan Soo massis 306 rubl 66⅔ Kop ostmise käeraha ja 30 rubl Kontrakti raha mõisawalitsuse Herra Holzi kätte sisse.
6. Zobbi tallo rentnik Peter Hansen andis ülles, et temal praegu keik täis raha massa ei olle ja pallus kuni 30 Detsembrini s.a kannatada ja lubas sis keik ärra massa ja se tallo osta.
7. Utte Jaan Hansen pallus nendasama kui Peter Hansen.
8. (Labbi Mihkel Klaos pallus nendasama kui Peter Hansen ja Jaan Hansen)
8. Sillaotsa Johan Raswa andis ülles, et tema seda tallo ostma saab, ent tinnawu asta seda raha puduse perrast osta ei jõua ja pallus tullewa astani kannatada, konnas tema sis seda raha massma saab.
9. Wantka tallo perremehe Oscar Posti Wölmünder Johan Post massis Oscar Posti est 693 rubl 33⅓ Kop ostmise käeraha ja 30 Rubl Kontrakti raha mõisawalitsuse Herra Holzi kätte sisse.
11. Musta tallo rentnik Jaan Kiesa ütles, et temmal mitte weel täis rahha massa ei olle ja pallus kuni 30 Detsembrini s.a kannadata, konna temma sis se raha ärra massa lubas.
12. Klima tallo rentnik Jaan Kliim massis 466 rubl 66⅔ Kop ostmise käeraha ja 30 rubl Kontrakti raha mõisawalitsuse Herra Holzi kätte sisse.
Otsus: seda protokolli ülles panda.
Kohtumees: 
Jürri Kärn XXX
Johan Opman XXX
Jaan Hansen /allkiri
Johan Raswa XXX
