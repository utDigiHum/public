sel. 4 Febroaril 1877
koos ollid
teine kohtomes Andres Kiik
kolmas kohtomes Jaan Mänd
neljas kohtomes Josep Welt
Astus ette selle walla innimenne Jürri Raud ja andis koggukonnakohtule teada et temma issa Mart Raud kes Mõisa krundi peal N 7 perremes olli on ärra surnud, ning et temma wannemb wend Mihkel Raud jo 25 Aastad omma issast lahhus ja temmal omma krunt siis pallub temma ennast kui säduslikku issa Tallu asseme parrija, nende Tallumaade peale perremehhest panna ja sedda Protokolli ülles kirjutada
Moistus
Et se õige et Mihkel Raud jo 25 Aastad omma issa juurest ärra ning temmal ommal üks krunt on Jürri Raud agga lapsest sadik omma issa juures olnud ning selle Tallu eest hoolt kannud. siis saab temma koggukonnakohto poolest kui perremees omma issa tallu peale wasto wõetud ning sedda Protokolli ülles kirjotud.
Andres Kiik XXX
Jaan Mänd XXX
Josep Welt. XXX
