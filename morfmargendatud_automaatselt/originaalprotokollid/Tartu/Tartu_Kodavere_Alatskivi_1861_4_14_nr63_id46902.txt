Allnimitud Alatskiwi walla peremehed on kõik Möödaläinud aastal Jakobi ja Märdi pääwal Mõisawalitsusele üles öölnud, mis pärast nende majad äraantakse ja talu majade seisus siia protokolli raamatus saab tähendud:
N 65. Tõnnis Keski maja - kaheks jauks Abram Andersoni ja Jüri Juhkami katte.
Hoonete seisus: Rehi ja rehealune wana, katus halw; kamma uus, katus hea; 
1 ait mäda;
1 laut hea,
1 tall hea,
1 onn hea,
1 koda hea,
1 saun hea,
1 kaiw hea,
1 küün hea,
aiad jääwad parandada,
puid 4 1/2 süld.
N 71. Toomas Tintilt - Laar Laht.
Hoonete seisus: Rehi ja rehealune mäda
katus halw, saab walla poolt katetud,
2 aita head,1 tall,			head		1 onn,		1 koda,		
1 saan, mäda,
aiad keskmised, parandada
3 süld puid
N. 75. Jüri Kalmuse pool maja - Benjamin Liiwa kätte
Hoonete seisus: Rehi hea, kambrel põrand,
4 aita head, ühe katukse all,2 lauta			head,					
1 onn,					1 saun,		1 koda,		1 kaiw		
5 süld puid, aiad parandata.
N. 77 Jaan Aug - Toomas Kalewile
Hoonete seisus: Rehi keskmine, kamber mäda,
1 laut mäda,
1 ait hea,
1 ait keskmine,
1 kaiw hea,
aiad parandada,
1 1/2 sülda puid
N 79. Siim Rääbis - Karl Laksile.
Hoonete seisus: Rehi hea,
Rehealune ja katus keskmine,
1 ait hea,1 laut,			head,		1 onn		1 laut			mädad,		1 onn		
2 küüni mädad
1 koda mäda,
1 saun hea,
1 kaiw mäda,
aiad keskmised,
1/2 süld puid
N 80. Märt Pärn - Abram Lind.
Hoonete seisus: Rehi hea,
2 aita keskmised,1 laut,			 head,		1 onn		
1 laut mäda,
1 küün hea1 saun hea,			ühe katukse all,		1 köök hea,		
2 küüni head,
1 küün mäda,
1 kaiw keskmine,
1 1/2 süld puid.
N 81. Aadam Sep - Mihkel Kallewile.
Hoonete seisus: Rehi wana, katus keskmine, 
2 aita head,
1 köök wana,
1 saun wana,
1 saun wana,1 laut			head,		1 onn		
aiad peawad parandud saama,
1 kaiw hea,
1 wana küün,
3 süld puid.
N 82. Jaan Tang - Mihkel Pärnale.
Hoonete seisus:
Rehi hea, 4 paari hengi, 6 ruuto 2 raamis,
1 kammer puu põrandaga, 8 " 2 ",3 aita			head,		1 laut		1 onn		1 saun		1 koda		
aiad keskmised,
1 kaiw hea,
3 süld puid.
N 83. Jaak Puuka maja - Jaan Ratassepale.
Hoonete seisus: Rehi hea, katus hea,
1 ait hea,
1 ait mäda,
1 laut mäda,
1 saun hea,
1 kaiw keskmine,
Aiad head, aga parandada
2 küüni kesmised,
2 1/2 sülda puid.
N 84. Tõno Koka - Jüri Suurele.
Hoonete seisus: Rehi hea, 1 wärate halb,
kammer puu põrandaga,
1 onn hea,
1 ait keskmine,1 laut			head,		1 onn		
1 laut halw,1 koda,			 head,		1 saun,		1 küün,		1 kaiw		
aiad keskmised,
1/2 süld puid.
N 88. Jüri Kask - Jaan Tangu kätte.
Hoonete seisus: Rehi hea, kammer laua põrandaga,
köök wana,
saun hea2 lauta			head,		2 onni		kaiw		
1 küün,
aiad head, aga parandada,
3 süld puid.
N. 100. Jüri Juhkam - Johann Widrik Andersoni kätte.
Hoonete seisus: Rehi, rehealune, katuksega hea,
1 ait hea,
1 ait wana,
2 lauta,			head,		2 onni,		1 köök		1 saun		1 tall		
1 koda hea, aga katukseta,
aiad parandada,
1 kaiw hea,
2 süld puid.
N 101. Abram Anderson - Karl Andersoni kätte.
Hoonete seisus: Rehi ja rehealune wanad, katus keskmine,
1 ait hea,
1 ait wana,
2 lauta			head,		2 onni,		
1 hobusetall hea
1 koda hea
1 saun hea,
1 kaiw hea,
3 küüni wanad
5 süld puid
aiad parandada.
5 süld puid.
N 102. Karl Metskiwi - Jooseb Kooki kätte.
Hoonete seisus: Elomaja (rehi) wana, Kamber uus, 2 ukse raudhingedega
1 raam 4 rutoga
ja 2 " 1 ",
rehealune wana, 2 wärate ja laduga,
1 uus ait
1 koda hea aga katukseta,
saun hea,
1 laut wana,
1 tall wana,
3 head heina küüni - 3 sülda puid.
N 103. Karl Laks - Hans Käärti kätte.
Hoonete seisus: Rehi uus, katus hea, 5 ukse raud hingedega,
4 aknat iga üks 4 ruutoga (1 ruut katki; rehealuse ladu hea, 1 wärate wana, 1 wärate uus;
kambrel laudpõrand,
koda hea
1 ait uus, töine ait wana, mõlemad head,
1 laut wana,1 heä küün,			head		1 saun,		1 kaiw		
3 süld puid.
N 104. Kusta Kiis - Andres Punga kätte.
Hoonete seisus: Rehi uus, rehealune hea, 2 wäratiga,
1 uus ait,
1 wana ait,
1 koda wana, aga hea,
1 uus laut,
1 tall uus,
3 head heina küüni, hea katustega,
saun hea,
3 süld puid.
N 105. Inn Toomink - Karl Kiis kätte.
Hoonete seisus: Rehi keskmine			halwa katuksega,		rehealune wiltu		1 ait keskmine,		1 laut, 			head,		1 onn,		1 koda,		1 saun,		
aiad parandada,
3 süld puid,
2 küüni head.
Mõisa kõlge said wõetud Weski küla 2 talu, nimelt:
N 34. Benjamin Liiwa talu.
Hoonete seisus: rehi hea, katus wana; 6 raudhinge 14 ruuto 3 raami sees,
3 aita head, 3 lukku, 3 hinge,
1 koda hea,
1 laut hea,
1 onn hea,
saun hea,
2 küüni head,
2 küüni keskmised,
1 kaiw 8 õunapuud.
N 33. Jüri Suure talu
Hoonete seisus: Rehi hea; 6 paari hingi, 21 ruuto 5 raamiga,
2 aita head 1 raudluk, 2 p. hinge, 1 punluk,
2 lauta wanad, 1 paar hingi,1 tall			head,		1 onn		
1 koda hea, 1 paar hingi,
1 saun hea, 1 paar hingi,
1 kaiw hea,
1 kartohwlikelder, 1 paar hingi,
1 sigade laut 1 " hingi,
1 küün hea,
2 küüni wanad.
