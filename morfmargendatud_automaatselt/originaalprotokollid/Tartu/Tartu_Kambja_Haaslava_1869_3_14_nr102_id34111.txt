Se karjus Jaak Piir kaibap, et se perremees Kusta Wassila ei tahtwat karjapalk ärra massa:1.,			1 triik külmit rükki		2.,			1 triik külmit keswi		3.,			1 1/2 nael woid		4.,			7 1/2 toop keetis		5.,			3 1/2 zooga		
Kusta Wassila wastutap, et temma olnut kül ikka perremees majan agga maja sagi ei olle temma käen sanud, sellepärrast ei tahha temma kah karjusse palk massa, enge naudku karjus neide kaest, kis majast saagi saanuwat - palka.
Moistus: Se Kusta Wassila om tallon perremees olnut ning piap temma siis tollepärrast sel karjus Jaan Piiril karja hoitmisse palk mis üllewan nimmitud warsti ärra masma.
Pääkohtomees Johan Toom XXX
Kohtomees Jürri Luhha XXX
Abbi-Kohtomees Jaan Opmann XXX
Moistus sai kuulutud ning suuremba kohtokäimisse õppetusse kirri wälja andtu.
