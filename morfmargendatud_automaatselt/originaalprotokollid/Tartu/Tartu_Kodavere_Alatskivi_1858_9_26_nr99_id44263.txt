Nina küla körtsimees Joosep Jakobson kaibab, Dmitry Trofimow olewa tema emmise, mis 10 rubla wäärt ja kellel 10 põrsast järele jäänud, mahalöönud.
Sellepääle wastab Trofimow, tema olewa Jakobsonile ammuki öölnud - külakohtumees Taaniel Kook olnud juures - : "kui sina oma siga paremalt ei hoia, ma lasen teda püssiga maha! Siga aga olli pääleselle jälle minu kartuhwlis, ja ma wirutasin tema üle aia, aga ei löönud teda mitte.
Tunnistanud on Nina külas peakohtumehe ja Jakobsoni ees Anton Ekimof, Trofimow olewat siga löönud, aga salgab seda Kogokonna kohtu ees ära, ööldes, Trofimow olewat siga koeraga aga wälja hirmutanud.
Mõista ie wõinud midagi, selle et õiget tunnistust ei ole.
