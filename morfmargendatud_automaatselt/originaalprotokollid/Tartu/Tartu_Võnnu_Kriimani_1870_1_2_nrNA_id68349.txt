Krimani Koggokonna kohtus 2sel Januaril 1870
 Mihkel Kärik tulli ette ja ütles: et minna ollin üks wakka rükki külw  Jürri Kitsele enda nurmest annud päiwade eest nenda modi et Temma õlled minno maja jättab, agga süggise üks pühhapäew homiko enne walged olli Jürri Kitse need rükkid keige õlgiga minno nurmest ärra winud enda perremees Kotta Päiwa pole.
 Jürri Kitse tulli ette ja ütles: se olli minno kaup et minna se omma wakka rükki külw keige õlgedega temma nurmest kätte saan 4 jalgsi päewa eest ja ollen ka need päewad temale ärra teinud kolm enne sedda ja 2 peale selle kui ma need rükkid temma nurmest ärra tõin ja kohhus wõib sedda arwata et kui minna ilma Temma lubbata olleksin need rükkid Temma nurmest ärra tonud siis olleks Temma kohhe wõinud kaebata ehk jälle siis üttelda kui minno naist tulli selle rükki ma eest tööse kutsuma peale se kui rükkid ollin ärra tonud. Se ei olle mitte õige et minna ösi ollin need rükkid Temma nurmest tonud agga pühhapäew sömaajast ollin ma  kül need rükkis koddo tonud seperrast et meie Allaso hainale üttest näddalist läksime ja arwsin et külla ellajad sawad ärra söma ollin selleperrast ärra tonud ja Kotta Päiw wõib tunnistata kunnas ma need rükkid koddo wisin.
Juhan Raswa tulli ette ja ütles: meie karri olli weel koddo kui Jürri Kitse rükki koormatega meist möda läks.
Tunnistaja Kotlieb Päiw tulli ette ja ütles: Kui Jürri Kitse rükkikoormatega kotto tulli siis olli päew täieste sömaajas ja sedda ma kulin kül kui Mihkel Kärik peale rükki tomist käis Jürri Kitse naest endale kutsmas selle rükki eest päewi teggema.
Mihkel Kärik ütles minna wõin need päwad rahhaga ärra maksta ja pean siis need õlled ja rükid taggasi sama. Jürri Kitse tulli 15 Januaril ette ja ütles: kui Mihkel Kärik nüid talwel teada olli sanud et Mõisale selle rükki ülle olli kaebatud, siis kutsus mind koli tarre manno ja hakkasiwad koolmeistriga mind mõllemad pallema et minna need õlled pean taggasi andma ja nenda ütlema et ma enda lubbaga ollen need rükkid temma nurmest  winud et siis Temma süidi wähhemaks jäeb.
Mõistus:
Koggokonna kohhus mõistis et  Jürri Kitse peab 40 leisikat õlgi tagasi wima ja 1 Rubla waeste ladiko trahwi maksma sepärrast et Ta pühhapäew olli orjanud ja  Mihkel Kärik maksab 3 Rubla waeste ladiko sepärrast et Ta mitte siis polle kohhe kaebanud kui rükkid nurmest ärra kaddunud ja nüid ilma aigo kaibtust on üllese tõstnud.
Kohto lauan olliwa
Peakohtomees Jürri Bragli XXX
Kohtomees  Ado Lentzius XXX
Abbikohtumees  Jaan Põdderson  XXX
Koggokonna kohto kirjutaja W. Kogi /allkiri/
Jürri Kitsele ja Mihkel Kärikule sai mõistus sel sammal päewal säeduslikul kombel mõllemile ette kulutud.
