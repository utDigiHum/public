Awwinorme Metskoggokonnakohtus sel 15 Augustil 1875
koos ollid
peakohtumees Josep Kask
teine kohtumees Juhhan Koppel.
kolmas kohtumees Jaan Mänd
Tödosaare perremees Mihkel Ambos ja Laekonno Koolmeister Juhhan Schulbach kaebasid et Teadosare külla karri on nende kaera põllud ärra sönud ja tallanud ning nõuab Mihkel Ambos 4 Robla ja Juhhan Schulbach 3 Robl kahju tassumist
Kohtumees Josep Kask tunnistas et temma on sedda pilla watamas käinud ja takserib temma et se nõutud kahju tassuminne keigest agga pool sest pillast.
Moistus
Need kaks karja poisi Tomas Pukk ja Jaan Paas maksawad omma karja palgast se pill ärra kumbgi 3 Robla ja saab Mihkel Ambos ja Juhhan Schulbach kumbgi 3 Rubla kahju tassumist
Juhhan Koppel XXX
Maddis Sildnik XXX
Jaan Mänd XXX
