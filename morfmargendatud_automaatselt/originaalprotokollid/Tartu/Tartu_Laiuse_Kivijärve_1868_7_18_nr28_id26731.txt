Tulli kohtu ette Johann Roos ja kaebas, et Johann Kirss on temma oddrad obbustega ärra söötnud.
Johann Kirss ütleb, et temma ei olle mitte söötnud.
Tunnistussemees Leno Weer tunnistab, et Johann Kirssi obbussed on Johann Roosi oddras ollnud ja on wägga rängaste ärra söötnud.
Peakohtumees Indrik Lepp tunnistab, et sest maast, mis ärra söödetud on, peab Johann Kirss, Johann Roosile 2/3 wakka oddre.
Sai selle peäle mõistetud, et Johnn Kirss peab Johann Roosile 2/3 wakka oddre maksma ni pea kui temma ommad oddrad on ärra peksnud.
Pea kohtumees Hindrik Lepp [allkiri]
Kohtumees Jürrij Tächt XXX
Kohtumees Jürrij Peterson XXX
