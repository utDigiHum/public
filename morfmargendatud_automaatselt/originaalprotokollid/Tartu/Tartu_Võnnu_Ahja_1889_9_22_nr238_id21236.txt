Ahja mõtsa walitsus kaebas, et Peeter Tenno on mõisa mõtsast ühe kuuse warastanud: 9 tolli jäme 60 jalga pikk hind 1 r. 30 kop. ja et puu temale jäänud nõuab 2 kordne hind.
Peeter Tenno ei salanud ja ütles et ta ühe kuuse toonud ja pandnud tare laeks.
Mõiste: Peeter Tenno peab 2 rub 8 päewa sees mõtsawalitsuse hääks ära maksma.
Mõiste kuulutadi sääduse järele ja Tenno maksis ka kohe ära.
