Awwinorme Mets kogokona kohtus sel 2 Augustil 1874.
koos ollid.
pea kohtomees Josep Kask
teine kohtomees Juhhan Koppel.
kolmas kohtomees Maddis Sildnik.
Mart Ambos kaebas et Jaan Pärn on 2 Aasta eest selle aja sees kui tema ei olle koddos olnud tema majase tulnud ja tema tüdruku käest 1 wakk kaero petnud kelle eest tema ei olle tänna päwani ei rahha maksnud, egga kaerad assemele tonud ja pallub temma kohut sedda järrele kulata et miks pärrast Jaan Pern need kaerad pettusega wõtnud ja tema aita omma lubbaga lähnud, ja need kaerad omma käega wõtnud, ja tahab tema kolme kuhjaga küllimitto eest 2 Rubla
Jaan Pärn räkis et temma on Mart Ambso käest kaerad kaubelnud 1 Rubla 60 kop wakk ning on tema siis pühapäwa hommiko Mart Ambose Majase lähnud ja et Mart Ambos ei olle koddo olnud, on siis temma tüdruk Marri Pihlak, need kaerad aidast annud aga ei olle tema neid kaero oma käega wõtnud, ja et tema piddanud need kaerad assemele toma, siis on se assi nenda kaua selletamatta olnud, ja woib Juhhan Leben tunnistust anda et tema need kaerad Mart Ambose käest kaubelnud.  
Marri Pihlak tunnistas et Jaan Pern tulnud nende Majase ja üttelnud anna mulle üks wak kaero, meil on Mardiga ärra rägitud, ja on siis tema Jaan Perna aita lasknud ja se üks wak kaero omma käega ühe küllimittoga mis kuhjadega olnud wälja mõõtnud.
Juhhan Leben tunnistas et temma olnud jures kui Jaan Pärn ja Mart Ambos kaero kaubelnud, ja on Mart Ambos essite 1 Rubla 70 kopp wakk tahtnud pärast aga 1 Rubla 60 kopp. jätnud Jaan Pärn agga on 1 Rubla 50 kop. pakkunud ja üttelnud ma saan Wennewerrest selle 
hinna eest kaero, mis peale siis Mart Ambos üttelnud kui sa neid kaero 1 Rubla 60 kopp.eest wõtta tahad siis tulle homme woi ülle homme pühhapäwa homiko lähen mina kodost ärra ladale.
Moistus
Et kogokona kohus sest tülli asjast selgest näeb et se muud ei olle kui Jaan Pärna kangekaelus, et tema ei olle need kaerad jo ammogi ärra maksnud ja melega Mart Ambost wintsutada ja kohut waewata tahtnud, siis moistab kogokona kohus et Jaan Pärn peab Mart Ambosele kaheksa päwa sees 1 Rubla 80 kop. kaera raha wakka eest ära maksma, ja saab selle melega kiusu eest, ja et tema teise aidast kaero wõtnud kahhe päwa ja öö peale trahwist kinni pantud pealegi et temma ei olle mitto korda kogokona kohto käsko kuulnud.
Jaan Pärn pallus Prottokolli wälja surema kohto minna mis temale agga seaduse § 701 pohja peal keeldud sai agga üks tunnistus lubbatud.
Se lubbatud tunistus sai sel 5 Augustil Jaan Pärnale No 192 all wälja antud.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
