Haslawa Kogukonna Kohus sel 16 Nowembril 1887
Man ollid: 
Kohtumees: Johan Opman
"  Jaan Hansen
"  Johan Kliim
Kaebas  Jaan Kusma Kriimanist, et minewa suwel ollnu tema tüttar 11 astat wana Korzu perremehhe Johan Blacki jures, ent nüid es masswat Johan Black 1 wakk palka rükki mitte ärra ja pallus et Kohus seda massma sunis.
Johan Black wastutas selle päle, et se karja tüdrukenne zigu om likatama pessnu nink nimelt om ütte suure zia likkatama lönu, kes joba 4 nädalit likatab, selleperast ollewat tema 1 wak rükki kinni pidanu. Walla wölmünder Endrik Zobel ollewat neid zigu ülle kaenu.
Jaan Kusma wastutas, et Kõhra perrenaene ollewat omma Emise Kulti jurde aijanu, sis päle selle nakanuwa, tõise zia seda Kulti wihkama, misperra sis kult likatama akkanu. 
Wallawölmünder Endrik Zobel andis ülles, et sel kult zial üts Kram jalla peal om ollnu, kelle peal se ziga likatanu, tõisi zigu ei olle tema nännu.
Mõistetu: et Johan Black se 1 wakk rükki Jaan Kusmale 8 päewa sehen ärra peab massma, selle et se ziga, kel kram jalla pääl ollnu, sis mitte lömise perra enge purremisse perra likkatama om nakkanu om arwata.
Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.
Kohtumees: Johan Opman XXX
Kohtumees: Jaan Hansen /allkiri/
"  Johan Kliim XXX
"  Johan Raswa XXX
