Tuli kohtu ette Jaan Pazi nink kaibas Peter Tarraski karjusse Taniel Paiju päle, et Taniel Pai om Jaan Pazi tatrikku ja kara ärrä lasknu ellajil süwwa.
Kutsuti ette Taniel Pai, nink küssitus, kuis sinna Jaan Pazi tatriku ja kara ärrä ollet söötnu, Taniel Pai es salga nink ütles, sedda olle minna kül tennu mõistke kohhus mes arwate.
Wöölmöndre Jakop Lük kai se Tatrik ja kaar ülle mes kahjo ol sanu üts nelläntik tatrikke ja kats nellantikku karo ärrä södedu.
Otsus.
Taniel Pai peap Jaan Pazile tatrikke ja kara söötmisse eest kats Rubla 70 kopkat masma süggise kui perremees palka massap.
Päkohtumees Jakop Konts &lt;allkiri&gt;
Abbi Jaan Petso XXX
Abbi Jaan Wisk XXX
Kidijerwel sel 9. Aukustil 1868
Koggokonna kirjotaja T. Konts &lt;allkiri&gt;
