№ 129
Sel 29 Oktobril 1890
Kaibas perremees   Mihkel Iwan, et tema karjapois  Kusta Zoo  pidanu kauba perra kuni 10 Nowembrini 1890 karjan käima ent lännu 16 Septembril s.a ilma põhjata tenistusest ja wõtnu püksi ja mütsi ütten.  Mihkel Iwan nõudis kaubeltu asta ja rõiwat tagasi Kusta Zoo käest, suwe est olli 1 wak rükki ja 1 wak Keswi palka kaubeltu ja 1 wak rükki om tema poisile ärra massnu.
Mihkli Iwan /allkiri/
Kusta Zoo, 10 astat wanna, wastutas emma Ann Zoo man ollen selle kaibuse päle, et kaub ei olle suguki 10 Nowembrini s.a ollnu, enegi ollnu käimisse päle kül kuni sügiseni antu kui pois jõuab. Poig astonu 1 nädal enne Jaanipäiwa tenistussi. Palka olli kül 1 wak rüki ja 1 wak keswi kaubeltu kui pois ärra tenida jõuab ja 1 wak rüki om kätte sanu. Watt, püksi ja müts om kül pois ütten ärra tonu ent pois pidanu omma rõiwa karjan ärra ja kui Mihkel Iwan nõuab, sis wõib temma need rõiwat tagasi anda.
Ann Zoo ei mõista kirjutada.
Mõistetu: et Ann Zoo watti, püksi ja mütsi  Mihkel Iwanile tagasi peab andma ja peab muide nõudmistega rahul jäma.
Se mõistus sai kulutedo.
Mihkli Iwan /allkiri/
 Ann Zoo ei mõista kirjutada.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks  /allkiri/
"  Peter Hansen XXX
