Naine Ellis Loiku kaibap Luige Jaan Mutso päle, et timma on üts päw timma jure tulnud nink on timma küsinut mis pärrast Jaan on timma poig kes Jaani mannu karjuses olli enne aiga ärralasnut, et ta olli seda lats ommetige asta pole kauplenud. Kui timma seda ni kõnneles on Jaan wälja karganut ja on teda pesnut luwa ka, nink on si naine Jani käst tahtnud si nugga ärra wõtta mis Jani käw se kõrd olli, ja ni selle tapplemise ka sai si palge külge noa ka Jaani käst hawatud, ja olli palju werd josnut kui ta kohtu mehhe mannu naitamas keis.
Jaan salgab ärra et ei olle lönud ei ka hawatud seda naist, mudku on teda maiast wäljatoukanud.
Moistap kohhus. Sest et kumbaki tunnistajat mannu ei olle ollnud sap hawa pärra moista et Jaan massap trawirahha ladiko 3 Ru ja naisele 2 1/2 Ru.
Kohtu kaijadele sai ütletud et protokoll woip wõtta kui Rahhu ei ja selle moistmise ka.
Jaan pallus Protokoll waljaanda. 
Peter Maddisson XXX
Josep Arik XXX
Josep Tensing XXX
Peter Zirk XXX
