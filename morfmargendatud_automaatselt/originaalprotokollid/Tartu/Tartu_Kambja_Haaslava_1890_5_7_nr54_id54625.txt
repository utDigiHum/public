№ 54
Sel 7 Mail 1890.
Kaibas Jaan Patzmann, kes Tarto linnan Peterburgi ulitzen  № 81 ellab, et 24 Märzil s.a purrenu Otsa perremehe Jürri Luha  koera tema nenda ärra, et tema 6 nädalit haige om ollnu. Jaan Patzmann nõudis põetamise kullu 21 rubl rohu raha 5 rubl ja 2 kohtu päiwa seija ja pallus et kohus seda Jürri Luhale massa pannes. Päle selle nõuab tema ka edesi kohtu käimise kullu kui se kohus tänna lõpetedo ei sa selle et tema ainus warrandus päiwa palk om.
Jaan Patsmann /allkiri/
Otsa tallo perris ostja Jürri Luha wastutas selle päle, et tema seda suguki kuldnu ei olle et tema koera kedagi purrenu olles sis olles kaibaja temale omma hawa näitama pidanu. Selle jures wõis ka üts wõeras koer seda kurja teha, sest tema koera ei olle weel kedagi purrenu ja tema koeradel ollewat päitsed peas.
J. Luha /allkiri/
Jaan Patzmann andis Otsa tallo tüdruku Anno Müller tunistus ülles, kes koero kodo om winu ja tema edesi mina sanu ja Kõrtsimees Kristjan Undritsi naene ollewat ka seda purremist nännu.
Otsus:  et  Anno Müller ja  Kristjan Undritzi naene ette om kutsuda.
Se mõistus sai kulutedo.
Kohtumees:  Jaan Pruks  /allkiri/
" Jakob Saks /allkiri/
"  Jaan Treijal /allkiri/
