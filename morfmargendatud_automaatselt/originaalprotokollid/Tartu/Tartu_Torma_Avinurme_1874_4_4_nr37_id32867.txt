Awwinorme Mets koggokona kohtus sel 4 April 1874.
koos ollid
pea kohtomees Josep Kask
teine kohtomees Juhhan Koppel
kolmas kohtomees Maddis Sildnik
Mart Pihkwa saab selle eest, et temma omma Koddose rahwaga allati tüllitseb ja riidleb 15 witsa lögiga trahwitud, ja kui ta mitte ennast ei parranda ja riidlemist mahha ei jätta, eddespiddi kõwwema trahwi alla woetud.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
