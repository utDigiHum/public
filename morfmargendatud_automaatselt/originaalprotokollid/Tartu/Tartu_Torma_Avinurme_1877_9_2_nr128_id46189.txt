sel. 2 Septembril 1877
koos ollid
peakohtomees Josep Laurisson
teine kohtomees  Andres Kiik
kolmas kohtomes Mihkel Sild
Liisu Paas kaebas Jürri Kasiku juures ollemissel. et Adrako külla perremehhed Josep Kallaus ja Mihkel Pomm ja Jaan Pern. on ommad karja ellajad Andres Habakukke heinamaale ajanud ja on siis Wallawöölmonder Gustaw Hallik temmale ütlenud minne aja need ellajad sealt heinamaalt ärra ja on temma siis selle peale neid ellajaid ärra ajama lähnud. selle peale on agga Josep Kallaus temma kallale joksnud. ning tedda mahha lönod. ja tulnud siis Mihkel Pomm. ja lönod temmale kolm korda puuga nenda kowwast selga lönud et temma poolsurnud mahha jänud ja on siis Ewa Habakuk tedda koddu aitanud, ja et need kaks innimest tedda santist tehnud, siis nõuab temma et need innimessed selle peksu eest trahwitud saawad. ja temmale wallu rahha maksawad.
Mihkel Pomm räkis et Liiso Paas on nende karja ellajaid peksnod. ja on temma siis küll Liiso Paasi keelanod agga tedda mitte putunud. ja tahhab temma sedda teada saada mis põhja peal Liiso Paas on nende karja ellajaid peksnod
Josep Kallaus räkis nenda sammuti et Liiso Paas on nende karja ellajaid tagga ajanud ja temma siis Liiso Paasi küll kelanud agga mitte putunod
Wöölmönder Gusta Hallik tunnistas et temma on küll Liiso Paasile ütlenud et temma peab need karja ellajad Andres Habakukke heinamaalt mis kohto peal olnud ärra ajama, ja on siis Adrako külla perremehhed Josep Kallaus, Mihkel Pomm ja Jaan Pärn ka sinna tulnud, ja nemmad selle heinama pärrast kärretaste waidlenud agga temma ei olle nähnud kas Josep Kallaus ja Mihkel Pommi on sedda Naist lönod, agga se Naine küll pikkali olnud.
Tomas Raja tunnistas et temma on nähnud kui Mihkel Pomm on ükskord Liiso Paasile puuga selga lönud, agga sedda ei olle temma nähnud et Josep Kallaus on lönud.
Josep Kallaus ja Mihkel Pomm räkisid et nemmad Tomas Rajada tunnismehhest wasto ei wõtta sest et se nendele wihhamees on.
Moistus
Et Koggokonnakohtus sedda teada ei olle et Josep Kallaus ja Mihkel Pomm, Tomas Rajaga awwalikkus wihha waenus ellawad. siis wõttab koggukonnakohhus Tomas Raja tunnistust wasto. ja moistab et Mihkel Pomm peab 1 Rubla Liiso Paasile wallo rahha maksma. ühhe löögi eest.
Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastast 1860 §§ 772 ja 773. kuulutud ning nende kohto käijatelle need säduse järrel Appellationi õppetusse lehhed wälja antud. Mihkel Pommile agga Appellation keeldud.
Josep Laurisson XXX
Andres Kiik XXX
Mihkel Sild XXX
