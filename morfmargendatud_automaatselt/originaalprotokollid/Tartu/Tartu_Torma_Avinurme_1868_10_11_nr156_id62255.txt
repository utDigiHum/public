Awwinorme Koggokonna kohtus sel 11mal October 1868.
Kohto juures olliwad
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Astus ette Iwan Kock ja kaebas Mihkel Kangro peale, et se temmale Wõlgo 27 Rubla ja 60 kop., mis temma kauplemisse peal Mihkel Kangro kätte annud. ja pallus koggukonna kohhut sedda wahhet õiendada.
Kutsuti ette Mihkel Kangor kes räkis, et temmal ei olle Iwan Kokkale ei koppikud anda, agga weel 30 Rubla Iwan Kokka käest sada.
Küssis koggukonna kohhus. kas neil kummalgi tunnismeest ette tua on, räkisiwad mollemad, et sedda neil ei olle, ja et nende seggadus kauplemisse peal Peterburgis tulnud
Moistus
Et nendel kummalgi tunnistust ei olle kuddawiisi nende wahhe on, ja et nemmad kokku kaubelnud ollid ei woi koggu konna kohhus selle asjale ühtegi otsust tehha, ja sowis, et nemmad issikeskis ärra leppiksiwad, mis kohto otsusega Iwan Kock ei lubbanud rahhol olla ja pallus Protokolli wälja mis temmale antud sai
Ka sai Iwan Kokkale temma Appellationi wälja andmisse pärrast Tallorahwa sädusest §§ 773. teada antud, mis temmal tarwis tähhele panna, kui da suurema kohto minna tahhab. 
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
