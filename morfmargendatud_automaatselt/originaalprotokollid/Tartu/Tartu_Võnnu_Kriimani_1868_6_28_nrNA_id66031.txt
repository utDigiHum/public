Krimannis 28mal Junil 1868.
Jaan Hint tulli ette ja kaebas et mul on Juhan Kolga käest weel palka sada ½ wakka keswi ½ pearahha tütrile, 1 Pund leiba ja mul olli Temmale enne Jürripäewa 18 jalgsi päewa tetto seapoisile 1 wak rükkid 5 toopi ernid ja 1 adra luits ja adra kastis mis ma nüid Temma käest tahhan kätte sada.
 Juhan Kolga tulli ette ja ütles: ½ wak rükkid ollen minna kül sepärrast kinni piddanud et Ta peale Mihkli päewa minno mant 2 näddalid ärra olli ja ½ pearahha 1 Leisika leiba ja 1 adra luits on temmale anda agga adra kastisid ei olle mitte minno kätte sanud ja minnul ka  Jaan Hinti käest sada 1 leisikas keswa jahho. Temma teggi kül enne Jürripäewa mulle mõnne päewa agga mul ei olle melen kui paljo ta teggi ja se wak rükkid seapoisi anna ma mitte sest meie kaup olli nenda tetto et minno ellaja karjus hoiab Tema ellajad ja Tema sea karjus hoiab omma ja ka minno sead.
Mõistus: 
Koggokonna kohhus mõistis 1 leisikas jahho ja 3 toopi ernid jäewad wastastiko ja Juhan Kolk  maksab ½ wakka keswi pool pearahha ja üks adra 1 Leisik leiba Jaan Hintile 2 nädali aja sissen ärra.
Man olliwa:
Peakohtomees Jüri Bragli XXX
Kohtomees   Juhan Mina  
"   Ado Lentzius XXX
