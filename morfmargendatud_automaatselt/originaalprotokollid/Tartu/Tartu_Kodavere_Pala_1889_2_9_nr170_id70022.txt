Mõisawalitsuse kirja pääle 13. Januarist s. a. N. 2 ollid kohtu ette kutsutud, kaebaja mõisawalitsus ja kaebtuse alused. Mõisawalitsuse saadik Th. Behm kordas mõisa kaebtust seletades: kaebtuse alusel olla Palla mõisaga kaupa teinud tegude eest pääwi tehha. Kaebuse alused
1. Jaan Nõmmm Rannast olla			2 rubla 88 kop.		2. Josep Terras Kokkoralt olla			2 rubla -		3. Juhan Lastik Kokkoralt  olla			2 rubla -		4. Karel Kiili  Kokkoralt			1 rubla		5. Luka Feklistow Kokkora mõisa			1 rubla 75 kop.		6. Nikolai Efimow  Jehuchow Allatskiwwi			2 rubla -		7. August Reinomägi Allatskiwwi			2rubla 63 kop.		8. Arnold Reinomägi Allatskiwwi			1 rubla 75 kop.		9. Karel Wälja Kokkoralt			2 rubla -		10. Jaan Wälja Kokkoralt 			1 rubla 75 kop.		
eest hagu saanud, aga mitte pääwi teinud ja palub mõisawalitsus kogukonna kohut, kaibuse alused süüdlasteks mõista, mõisale hagu hind ülewal nimetud suuruses mõisale wälja maksa.
 Kaebuse alusid ei olnud ükski kohtu ette tulnud ja sai selle pärast tehtud järgmine
         Otsus: tulewaks kohtupääwaks kohtukäijad ettetarwitada ja nimelt kohtu alused kahekordse trahwi juures. 
                                  Peakohtumees: J. Sarwik.
                                  Kohtumees:        W. Oksa.
                                  Kohtumees :       M. Piiri.
                                  Kirjutaja O. Seidenbach. (allkiri) 
