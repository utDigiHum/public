Moistman olliwa keik kohtomehet.
Tulli ette Ottilauri perremiis Karel Soonwald ja kaibas jürripäiwal 1859 kui minna selle maja wõttin massin minna wanna Ottilauri peremehe Johann Kruus sullastele 8 wakka rukki ja nüüd et se walla prak ümber om lodu et wanna perremehe tungile wastne perremiis ei pea palka masma sis ma tahan nüüd seda mis ma masnu olle mõtte sada.
Otsus
Saije leppitetus Karel Soonwald jättab 4 wakka rukki maha ja nõwwab ennegi 4 wakka rukki. Se 4 wakka rukki saije neda ärra jaotetas wanna Ottilauri perremiis Johann Kruus massab 2 wak ja Johann Kruus sullane Hans Joost päle jääb 2 wakka mis däl weel sada om.
