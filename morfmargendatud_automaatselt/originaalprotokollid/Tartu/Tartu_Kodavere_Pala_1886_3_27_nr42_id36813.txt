Tuli ette Krõõt Roots oma eestkostjaga ja kaebas, et Josep Willa teda peksnud ja nimelt temale  lume labidaga ühe küljeluu katki löönud. Tunnistajaks nimetas  Jakob Waddi. Nõudis 50 rubla. 
Ettekutsutud Josep Willa ja wastas küsimise pääle, et tema Krõõt Rootsi küll mitte löönud ei ole. Krõõt Roots on teda küll sõimanud, aga oma kätt ei ole tema külge pandnud.
Tunnistajateks nimetas Jakob Waddi.
Jakob Waddi ei olnud tulnud.
Otsus tullewa kohtupääwaks ette tellida.
                        Pääkohtumees J. Haawakiwi.
                              Kohtumees J. Stamm.
                              Kohtumees M. Piiri.
                             kirjutaja allkiri
