Haslawa Kogukonna Kohus sel 27 Nowembril 1889.
Man ollid: päkohtumees: Jaan Wirro
Kohtumees: Jaan Pruks
"  Jakob Saks
"  Jaan Treial
Selle otsuse päle sest 13 Nowembrist s.a Haslawa mõisawalitsuse kaibuse asjan wasto wakko tallude ostjat kontrakti kullu nõudmise perrast ollid tallu ostjade wollinik Johan Link, Jürri Luha ja Johan Black tullnu ja wastutiwa et nemma seda massu mitte massa ei taha, selle et neide ostmise kontrakti joba 1882 astal kinnituse om sanu ja selle ülle proklam kus joba 1882 astal ärra om josknu ja proklami joskmise aijal ei olle üttegi nõudmist ülles antu.
Mõistetu: et Haslawa mõisawallitsuse kontrakti kullu nõudmine tühjas om mõista, selle et neide wakko tallude ostmise kontrakti ülle joba 1882 astal proklam ärra om josknu ja sel aijal wastokõned ehk nõudmise ülles antu ei olle sanu.
Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.
Päkohtumees: Jaan Wirro XXX
Kohtumees:   Jaan Pruks /allkiri/
"  Jakob Saks /allkiri/
"  Jaan Treijal /allkiri/
