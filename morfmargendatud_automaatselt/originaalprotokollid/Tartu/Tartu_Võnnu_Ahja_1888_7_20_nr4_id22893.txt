Tuli ette Mikk Wilipson ja palus end poja Peeter Wilipsoni asemel protokolli üles panda: Peeter Wilipson lubab enda poja Friedrich Wilipsonile 20 rbl. osajao raha, sest et  tema ise töistkorda abielusse heita soowib.
Peeter Wilipson elab ise Wenemaal ja ei olnud tema endal wõimalik siis ette tulla.
Et see nõnda on tunnistab enda allkirjaga
Peeter Wilipsoni asemel: Mikk Wilipson XXX
Et Mikk Wilipson oma käega ritit alla on tõmmanud saab see läbi kogukonna kohtu poolt tunnistatud
Peakohtumees: Peeter Kripson XXX
Kohtumees P. Peedoson XXX
Kohtumees Kusta Saarwa &lt;allkiri&gt;
Kirjutaja: Chr. Kapp &lt;allkiri&gt;
renidirt sel 2 Decr 88
K.K h. ?? &lt;allkiri&gt;
