Awwinorme Metskoggokonnakohtus sel 24 Octobril 1875.
koos ollid
teine kohtumees Juhhan Koppel
kolmas kohtumees Maddis Sildnik
neljas kohtumees Jaan Mänd
Kai Pern kaebas omma issa Hindrik Perna kostmisse all et Mihkel Tamm on tedda ommale suwwilissest kauplenud, ning lubbanud temmale 15 Robla palka 1 leisik linnu ja nael willu, nüid agga 5 Rubla rahha palka ja nael willu ärra salgab ja pallus kohhut sedda selletada.
Mihkel Tamm räkis, et kaup olnud nenda et temma Kai Pernale 10 Robla rahha palka ja üks leisik linno annab, ning on Kai Pern omma tenistusse ajast 8 näddalad tenimatta jätnud, ja woib selle palga ülle üks Wõttigwerre walla tüdruk nimmega Leno Kaddak tunnistust anda kellele Kai Pern on issi tunnistanud, et temma peab poksa saama 10.Rubla ja leisik linno.
Moistus
Se assi jäeb teise kohtu päwa peale selletada ning saab se Wottigwerre tüdruk Leno Kaddak seie kohto ette kutsutud.
Juhhan Koppel XXX
Maddis Sildnik XXX
Jaan Mänd XXX
