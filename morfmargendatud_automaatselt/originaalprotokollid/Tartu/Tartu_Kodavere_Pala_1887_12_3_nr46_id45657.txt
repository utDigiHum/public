Ettekutsutud Kristjan Kütt ja Karl Wända Kata Kütti Widriku lese ja lapse Liisa wöörmünder ja andsiwad üles et Liisa Kütt oma ema hoole all seisab ja kaswatust saab. Se lapse jagu raha 61 rbl. 74 kop. on intressi peal Tartu pangas seisnud ja 1887 aasta intress wäljawõtmata ja nüüd 64 rbl. 80 kop. suur. Liisa Kütt käib küla koolis.
                                                        Kristjan Kütt
                                                        Karl Wända. (allkirjad)
                                   Peakohtumees J. Sarwik.
                                          Kohtumees J. Stam.
                                          Kohtumees M. Piiri.
                                          Kirjutaja allkiri mitteloetaw.    
