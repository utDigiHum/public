Kohus alustas kel 11. Tulli ette Josep Oksa ja tunnistas et tema ei tea kedagi:
Mõisteti: Et Piiri Kõrtsimees Josep Nõmm tunnistab, et Jürri Welt Jakob Willemson tülli allustaja  olnuwad maksawad kumbgi 2 Rbl trahwi ladiku ja Josep Kubja nink Josep Waddi.
Ka tülli wahel on lähnuwad ja ka tüllitsenuwad maksawad kumbgi 1 Rbl trahwi waeste ladiku.
Se mõistus sai ette kuulutud. Josep Kubja ei olnud rahul Jakob Willemson ei olnud ka rahul.
Sai Protokolli ärra kirja.
                                         Peakohtomees: O.Kangro
                                         Kohtomees:       J. Soiewa
                                         Kohtomees:       O. Mõisa
