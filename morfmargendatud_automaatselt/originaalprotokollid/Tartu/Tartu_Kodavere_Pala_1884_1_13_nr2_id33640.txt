Tulli ette Jaan Wiljus Kokkoralt ja andis üllese et Kadunu Karel Tosso ära on surnud ja enne omma surma Kokkora kogokonna kohtu es sel 22. Februaril 1879 tema poja Pauli enesel kassupojaks wõtnud,  nõudis  et se Paul Wiljus kes Tosso Karla poeg on ka omma kasu asjasd järel jänud warrantuses osa saaks.
Sai Kadri Tosso ette kutsudu omma kõrwal seisja Josep Rosenbergika kes wastas, et temmal ei ole ühtegi kes se poega aga  tema kadunu mees on küll selle perrast Paul Wiljusse ommal kassu pojast wõtnud et  loosi minekust päsep, aga warrantust ei ole tema mees lubanud ja ei luba ka tema kellegil anda, ja tema kadunu mees Karel on küll üttelnud et se Protokoll on wõltsist tehtud.
Otsus: et se  een seisnu protokoll ühhe 1. Tarto Kihelkonna Kohtu ülle kuulamise alla saada, sest et Kokkora kohus selle j. asjas allust on. Se otsus sai ette kuulutud.
                                                              Kohtumees: Josep Soiewa
                                                              Kohtumees: Otto Mõisa.
                                                              Kohtumees: Mihkel Mölder.
