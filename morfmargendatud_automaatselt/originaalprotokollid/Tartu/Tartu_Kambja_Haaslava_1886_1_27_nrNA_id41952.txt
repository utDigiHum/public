Sel 27 Januaril 1886
Kaibas Jaan Unt, kes Peterburgi Kubermangus Gdowi Kreisin ja Remja Kogukonnan elab, et temmal 30 Rubl Johan Raswa käest sada om ja pallus et Kohus seda massma sunis, selle et tema heaga seda massa ei taha. Wõlla tähe näidas ette.
Johan Raswa wastutas selle päle, et tema Kaibaja käest seda raha lainanu ei olle ja seda wõlla tähte ei olle tema konnagi kaibajale andnu. Johan Raswa nõudis 3 Rubl 40 kop Kartofflide raha, mis temmal Jaan Unti käest sada om.
Jaan Unt wastutas, et tema Kartoflist Johan Raswa käest kül sanu om, ent keiki est om tema allati ärra massnu. Jaan Unt andis Peter Raudjalg Tartust tunistajas ülles, kes nännu kui Johan Raswa selle raha om lainanu ja wõlla tähe andno, nisama ütles Johan Raswa, et se Peter Raudjalg ka seda om nännu et tema kartoflist om tonu.
Otsus: et Peter Raudjalg ette om kutsuta.
Päkohtumees: Mihkli Iwan /allkiri/
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
