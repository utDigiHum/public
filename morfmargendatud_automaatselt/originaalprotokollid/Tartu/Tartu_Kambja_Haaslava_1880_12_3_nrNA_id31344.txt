Haslawa kogokonna kohus 3 Decbr. 1880
Juures istus:
Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
dito  Johan Udel
Tänasel pääwal tulewad ette  Hans Nuusa Tartu linnast ja Hans Pranso /kes Walgjerwe rewisionis Kannapäh kihelkonnas/ assemel Gustaw Kõiw ka Tartust ning palusiwad üles kirjutada, et Hans Nuusa wõttab Hans Prans'o poja nimega Johan omale kaswopojas, selle, et temal omal lapsi ei ole, ja lubab pääle oma surma kõik oma waranduse oma kaswupojale, ja peab kaswopoig kui tema jõudma nakkab, tema, kaswuessa kui ka tema naise eest hoolt kandma. Kaswupoig Johan Pranso on sündinud sel 10mal Decembril 1870.
Et see nõnda õige ja sündinud on, tunnistawad oma nime ala kirjutamisega:
Kaswopoja wõtja: Hans Nusa /allkiri/
Kaswopoja andja asemel : Gustaw Kõiw /allkiri/
Otsus.
Seda protokolli üles kirjotada.
Pääkohtomees Mihkel Klaos
Kohtumees   Johan Udel
dito  Joh. Link oli selle wasta.
Täiendatud 17. Junil 1891 aast. ( ? akt № 33)
