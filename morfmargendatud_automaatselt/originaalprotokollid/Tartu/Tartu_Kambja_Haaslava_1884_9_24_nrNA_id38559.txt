Haslawa Kogukonna Kohus sel 24 Septbr 1884
Man ollid: Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
Selle Kogukonna Kohtu mõistuse päle sest 17 Septb 1884 Ann Rosenthali kaibuse asjan wasta omma poiga Jaan Rosenthal tulliwa nemma tänna mõllemba selle kohtu ette ja nimelt Ann Rosenthal omma welle Jürri Klaose man ollen ja pallusiwa protokolli ülles panda et nemma omma wahel nenda lepinu om, et se rahha jagamine jääb nenda kui 17 Septembril s.a sijn mõistetu om, päle selle annab poeg Jaan Rosenthal 1 Lehm, 7 Lamast, 1 Emmis 3 põrssiga ja keik selle Tilba tallu peen kram, mes Essast perra om jänu, Emma Ann Rosenthalile.
Emma Ann Rosenthal jääb kunni 23 Aprilini 1885 Tilba tallu Ellama ja saab Jaan Rosenthali käest 3 wakka Rükki, 3 wakka Keswi ja 6 wakka kartoflid seni kui kewadeni Monas ja poig Jaan Rosenthal peab Emale 1 Lehm, 1 Lammas ja se Emmis tsiga ilma massuta ülle talwe sis annab poeg Jaan Rosenthal omma allaealiste welledele, kes tema jures karjan om käinu egale üttele 2 wakka Rükki 2 wakka Keswi ja 3 wakka Kartoflid talwe moonas Emma kätte, kes Emaga eentullewan talwen ütten sööma sawa ja Wellest lahti jäwa.
Otsus: Seda Protokolli ülle panda.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
" Peter Iwan /allkiri/
