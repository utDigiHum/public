 Sel 28mal Junil 1874. 
Kaebas Wallavannem Willem Perramets, kui temma endise wallavannema Maddis Saare käest Koggukonna Kassa vastu võtnud, siis olla 29 Rubl. 36 pudus olnud ja weel ilma selleta Jaan Sallo võlg mis
7 Rubl 64. Kedda temma ka kui se võlg õige ei peas ollema Maddis Saare käest nõuwab. Kostis Maddis Saar,Wöörmönder Jaan Kubja ja Wöörmönder Gustaw Rosenberg, et nemmat olla Jürri Sallo
pärranduse rahha 19 Rubl 36 Kop- ja Karel Sarvik pärranduse rahha 10 Rubl hõbbedat endise Wallawannema Gustaw Kokka ja Koggukonna Wõõrmöndrile Gustaw Waht  Kässu peal nende eesnimmetud poistele Koggokonna Kassast wälja maksnud.Se rahha olla nende eesnimmetud Gustaw Kokka, Gustaw Waht ammetiaig Koggokona Kassas hoidmas olnud agga kuskil  kirjutetud egga ka rahha Gustaw Kokka ja Gustaw Waht nende kätte andnud. 
 Mõistis Kohhus, et Maddis Saar,Jaan Kubja ja Gustav Rosenberg  peavat sedda rahha käeste Koggukonna Kassas sisse maksma,ja kui naad arwawat Gustaw Kokka, Gustaw Waht kui ka Koggokonna Wöörmönder Gustaw Nukka selle rahha ülle õegust sama, siis neile lubba sedda nõuda.
                                                                                                           Peakohtumees:Paul Willemson +++
                                                                                                                        Abbi:       Tomas Welt       +++
                                                                                                                        Abbi:      Mihkel Lallo       +++                
