Haslawa Kogukonna Kohus sel 19 Märtsil 1884.
Man ollid:
Päkohtumees Jaan Link.
Kohtumees
Johan Klaosen
Peter Iwan
Kaibas Jaan Meijer, et temal 3 Rubl 40 Jaan Posti käest sada om, tema ollewad 8 Wankri tsõri a 120 Cop Jaan Postile tennu, selle päle om 6 Rub 20 Cop kätte sanu ja om weel 3 Rub 40 Cop sada ja pallus et kohhus seda massma sunnis.
Jaan Post wastutas selle päle et Jaan Meijer 7 wankri tsõri temale tennu om ja 80 Cop tük tegemisse est ollnu kaubeltu ligi 60 kõdarat ollon tema käest 6 Rub 20 Cop om Jaan Meijer kätte sanu, mes järgi 60 Cop rohkemb sanu om, 1 tsõri lubas Jaan Post ka Meijeri käest ärra tua ja tulleb temmal 20 cop weel mannu massa.  
Jaan Meijer tõentas et 120 Cop tsõris est kaubeltu ollnu, ent tunistajat es ütle ollewat.
Mõistetu: et Jaan Meijeri nõudmine tühjas mõista om, selle et selle jures tunnistust ei olle ja et Jaan Post 20 Cop tegemise raha Jaan Meijerile ärra masson peab sis saab omma kattesas ratta tsõr kätte.
Se mõistus sai kulutado ja õpetus leht wälja anto.
Päkohtumees J. Link /allkiri/
Kohtumees: Johan Klaosen XXX
 Peter Iwan /allkiri/ 
J. Krosbärk /allkiri/
