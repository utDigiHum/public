1910 goda sentjabrja 9 dnja polutšil ja "Petr Janov Tseigerj" - 25 ljet? - ot naslednikov mojei pokoinoi sestrõ Milii-Rozalija Janovoi Sikk, urožd. Tseiger, svoju nasledstvennuju dolju, kakovuju ja imel polutšitj soglasno nasledorazdeljnago akta ot 21 sent 1906g. §2, vsego šestdesjat dva (62) rublja 50 kop. i protsentov 5 rub. 20 kop. - vsego 68 rubljei - i izjavljaju sim svoju soglasije na unitštoženije v krep. knigah otmetki vnesennoi na osnovanii §4 upomjanutago dogovora ob obremenii usadjbõ Gerrengof. 
Krestjanin: Peeter Ziger Ivan poig [allkiri]
Tõsjatša devjatsot desjatago goda sentjabrja devjatago, podpisj eta sdelana sobstvennorutšno kr-nom Petrom Janovõm Tseigerj.  
Nr 6.
Predsedatelj: K. Pala [allkiri]
Tšlenõ suda: P Keis [allkiri] JaVirmann [allkiri]
Pisarj JWeske [allkiri]
