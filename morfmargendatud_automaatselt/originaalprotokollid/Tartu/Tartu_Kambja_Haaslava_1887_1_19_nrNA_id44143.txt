Sel 19 Januaril 1887
Kaibas Endrik Jaska, et perremees Jaan Puhm, tema karja Moona 1⅔ wakka rükki, 20 toopi kudest ehk surmit, 3 naela raswa ja 5 naela Soola ja 1 paar zuge ärra ei massa ja pallus et kohus seda massma sunis.
Jaan Puhm wastutas, et tema ellajat selle kaibaja een ei olle ollnu ja tema ka kaubelnu ei olle 1 wak kartoflid pandno tema kaibajale maha selle et tema essa lehma karjan om käino.
Kohtumees Jaan Hansen tunistas, et Jaan Puhma ellajat Endrik Jaska een ei olle käinu, Jaan Puhmal ollnu essi karjus.
Mõistetu: et Endrik Jaska nõudmisse kaibus wastu Jaan Puhm tühjas om mõista selle et Jaan Puhma ellajat Endrik Jaska en karjan ei olle käino, enge Jaan Puhmal essi karjus om ollnu.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Kohtumees: Jürri Kärn XXX
Johan Opman XXX
Jaan Hansen /allkiri/
Johan Raswa XXX
