№ 2.
Haaslawa wallakohus, Jurjewi maakonnas,  Haaslawa wallamajas 30 Aprillil  1904 aastal.
Juures: Kohtu esimees:  Johan Otsa 
Kohtu liige:  Johan Kõõra  
 Reinh. Päiw 
Tuliwad ette Haaslawa walla liige Ritsu talu omanik Jaan Johani p. Luha ja Ropka walla liige  Julius Josepi p. Hüsse, elawad mõlemad Haaslawa wallas, paluwad alljärgnewat suusõnal kohtule ette kantud lepingut kinnitada.
1.
Jaan Luha annab oma mõtsa maast Kurepalus kolm wakamaad Julius Hüsse kätte rendile 23 Aprillist 1904 a. kuni 23 Aprillini 1910 a. s.o. kuue järestikku aasta pääle.
2.
Julius Hüsse kohustub selle maa eest iga aasta kaktstõistkümmend /12/ rub. renti maksma. Selle rendi maksab Hüsse iga aasta kahes jaus ära, nimelt 8 rub 13 Aprillil ja 4 rub. 13 Oktobril. Esimese rendi aasta kewadene jagu on juba ära makstud.
 3.
Wiimasel rendi aastal piab rentnik Hüsse sellest renditud maast ühe wakamaa rukki alla külima. Külimiseks annab Luha kolm puuta rukkid juba käesolewal 1904 a. Hüssele kätte. Rukki maa olgu aga Hüsse polt hästi haritud ja sõnnikuga rammutatud.
4.
Kui Hüsse seda § 3 nimetatud maad mitte rukki alla korralikult ei tee, siis piab ta maa omanikule kahjutasuks maksma: neljakordse seemne hinna ja wiiskümmend puuta õlgi.
 Jaan Luha /allkiri/    Julus Hüse /allkiri/
1904 a. Aprilli kuu 30 päewal on ülemal olew leping  Jaan Luha ja Julius Hüse poolt, kes mõlemad kohtule isiklikult tuttawad ja kellel mõlematel seadusline wõim lepingute tegemiseks on suusõnal wallakohtule ette kantud ning mõlemate poolt oma käega alla kirjutatud, mida kohus selle läbi tõeks tunnistab.
Kohtu esimees:  J. Otsa /allkiri/
Kohtu liige:
 J. Kõõra /allkiri/
 R. Päiw /allkiri/
Kirjutaja: J. Wäljaots /allkiri/
