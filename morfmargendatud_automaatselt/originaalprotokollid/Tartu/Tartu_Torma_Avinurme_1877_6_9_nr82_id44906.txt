Protokoll
Awwinorme Metswallakohtus sel 9 Junil 1877
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jürri Tomik
kolmas kohtomees Josep Welt
Et selle walla innimenne Josep Wahher jo mitto Aastad haige ning koggukonnakohtu ette ei woinud tulla olli koggokonna kohhus selle pärrast tännasel pääwal temma Majase Ulwi küllase tulnud temma ja temma Wenna Tomas Wahheri wahhel leppitust teggema tallumaade pärrast, sest et Josep Wahher kes 57 Aastad wanna omma issa wannemb poeg ning selle Ulwikülla Keskkülla N 107 tallu pärrija, nenda ärra surnud issa Jakob Wahher olli agga se tallu 18 Aasta eest Tomas Wahheri nimme peale muuta se tallu kontrakt /:Tomas Wahheri nimme peale :/ kinnitada ja Wakku ramatuse ülles kirjutada lasknud.
Tomas Wahher räkis et se Ulwikülla Keskülla tallu N 107. on 18 Aastate eest temma nimme peale sanud. ning temma selle järrel selle tallu perremees olnud. ja annud temma siis 10 Aasta eest selle tallu küllest, omma noorema wendadele Maddis ja Mihkel Wahherile suggu maad ja heinamaad prukida ning se perris tallu omma wannema wenna Josep Wahheri kätte. sest et temmal ommal Maja Mustwes olnud ja temma Rentkohha ... ning on temma nüid omma wannema Wenna Josep Wahheriga nenda kokko leppinud. et temma omma pärrimisse õiguse selle tallu peale. wannema wenna Josep Wahherile annab sest et temmal palju lapsi agga selle tingimissega, et se aed mis Keskküllas umbes pool wakkamaad suur ning kaks heinamad sitta pütti ja sooheinamad temmale sawad. ja et temma ka need maatükkid ja heinamaad. mis temma omma wenna Maddis Wahherile pruki annud. omma kätte taggasi wõttab. sest et Maddis omma renti ärra ei maksa, ning sealt krundi pealt igga Aasta heino müib, mis seaduse wasto. ja temmal sedda ka nüid ommal tarwis on
Josep Wahher räkis et temma omma Wenna Tomaga nenda wiisi kokko leppinud et se Keskülla tallu N 107 temma kätte ning katemma lastele pärrida jäeb ja sawad selle tallu küllest se Keskkülla Õunapu aed mis umbes pool wakkamad suur. ning kaks heinamad sitta pütti ja soo heinam Tomas Wahherile, ning woib Tomas ka need maa tükkid, ja heinamaad mis Maddis Wahheri käes seie sadik pruki olnud, omma kätte wõtta.
Peale selle pallus Josep Wahher et temma praegust raske haige ja paisetanud ja sedda kardab. et temma pea surra woib siis pallob temma omma wannema poja Gustaw Wahherid kes 26 1/2 Aastad wanna nende tallu maade peale perre mehhest kinnitada lasta. Ühhe Keiserlikku Baltia Domänhowi wallitsuse poolest
Maddis Wahher räkis et temma neid maasid ja heinamid ennamb taggasi ei anna sest et temma wennad Tomas ja Josep Wahher neid temmale prukida annod iggawesse aja peale.
Weel räkis Josep Wahher et temma need maa tükkid ja heinamaad mis temma norema wenna Mihkel Wahheri käes, temma kätte jättab. eddasi prukida
Moistus
Et Tomas Wahher omma wenna Josep Wahheriga selle Ulwikulla Keskkülla N. 107 tallu pärrast nenda wiisi kokko leppinud et se tallu pärrimisse õigus Josep Wahheri kätte jäeb, se agga wägga põddeja ja omma poega Gustawid Wahherid pallub. perremehhest kinnitada, siis saab se koggukonnakohhos Üht Keiserlikku Baltia Domänhowi Wallitsust palluma et Gustaw Wahher selle N 107 tallu peale mis 11 thald. 76 Grossi sured perremehhest kinnitud saab. ning sinna kõrwa kaks weikest krundikest Tomas Wahher ja Mihkel Wahher sest et need maatükkid jo 10 Aastad selle tallu küllest lahhus ning Tomas Wahher neid omma kätte taggasi woib wõtta mis temma agga Maddise kätte pruki annud.
Josep Laurisson XXX
Jürri Tomik XXX
Josep Welt. XXX
Maddis Welt andis sel 16. Junil koggokonnakohto ülles et temma selle moistusega rahhul ei olle. et koggokonna kohhus temma käest need maatükkid ja heinamaad. mis temma wend Tomas Wahher 14 Aasta eest temmale annod. nüid jälle Toma kätte taggasi moistnud. sest et nemmad selle asseme ülle on 14 Aasta eest kihhelkonnakohto juures leppinud, ja pallus Appellationi wälja suurema kohto minna mis temmale ka nendakohhe N 180 all sai wälja antud.  
Josep Laurisson. XXX 
Copia sel. 29. Aktobril 1895 Kusta Wahherile walja antud A Schulbach (allkiri)
Ärakiri 28/V-18 Gust. Waherile
