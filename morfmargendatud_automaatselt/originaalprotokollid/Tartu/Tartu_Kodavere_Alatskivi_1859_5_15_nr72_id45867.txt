Jekim Andrejew kaibab: "Michailo Jefimow olewa tema sulaselle Iwan Kirillowile passi kätte andnud, kellega se ära pagenend ja omas seljas tema /Andrejewi/ riided, wäärt kõigewähem 5 rubla h., araweenud." 
Mõistetud: Et Tartu Maa kohtu kiri Andrejewi kaibuse tõeks tunnistab, maksab Jefimow Andrejevile riiete eest 5 rubla h.
Nüüd hakasid riidlejad tõine tõise käest oma wõlga, mis neil weel muido on, nõudma, ja jääb Jefimow - kui Andrejewi wõlg tema 5. rublast maha arwatakse - keiges wõlgu 3 rubla 25 kopik h.
