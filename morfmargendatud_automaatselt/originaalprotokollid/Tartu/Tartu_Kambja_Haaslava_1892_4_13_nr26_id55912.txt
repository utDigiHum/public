№ 26
Haslawa  wallakohus 13 Aprilil 1892 a
Juures olid: Eesistuja: Jaan Treijal
Kohtomees:  Jaan Pruks
"   Gotfried Meos
Tuli ette  Johan Jüri p. Laar ja  Hindrik Johani p. Tätte ja palusiwad nende allnimitud leping protokolli kinnituseks üles panda.
Kawasto walla liikme Johan Jürri poeg Laar ja Kurista walla liikme Hindrik Johani p Tätte wahel om järelseisaw rentikontrakt tehtud ja allakirjutatud saanud.
§ 1.
 Johan Jürri p Laar  annab oma  Logina talust № 17 Haslawa wallas Tartu Kreisis Kambja kihelkonnas, mis kaheksateistkümmend Thaler, kuuskümmend krossi suur om, pooled talo maad Hindriko Johani p. Tätte´le kuue aasta pääle renti pääle, see om 23 apr. 1892 kuni 23 apr. 1898 aastani, aga Seppa maad ja Uipuaid jääwad sest kaubast wälja ja esimene aasta om proowi aasta.
Rendiraha om üks sada kümme /110/ rub. aastas. Rendiraha maksmine ega 1sel Aprillil wiiskümmend wiis /55/ rub ja iga 1sel Oktobril wiiskümmend wiis /55/ rub.
§ 2.
Rentnik Hendrik Johani p. Tätte maksab keik wallamaksudest pool ja teeb pool teed ja keik walla käskmist pooleks, kattuse parandamine, aid ja heinamaa pooleks ja ka Desätini raha maksmisest pool, see om 2 rub wiiskümmend kop. aastas.
§ 3.
Selle kinnituseks om see kontrakt kahes lehes ära kirjutud ja mõlemadest kontrakti tegijatest allakirjutud saanud.
Rendileandja:  Johan Laar /allkiri/
Rentnik:  Hindrik Tätte /allkiri/
Et selle eenseiswa kontrakti sisu ette om loetud ja ka seletud rentnik  Hindrik Tättele ja et rentnik selles täiliku rahul olemise om awaldanud keige tingimiste kohta, saab seega selle kontrakti kinnitamise juures tunnistatud.
Eestistuja: Jaan Treijal /allkiri/
Kohtumees: J. Pruks /allkiri/
"  Gottfried Meos /allkiri/
Kirjutaja: J. Weber /allkiri/
