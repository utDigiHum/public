Sel 28 Aprilil 1884.
Tännasel päiwal olli Päraha wõlla perrast Oksion kulutedo ja sai  Johan Rosenthali õmmelus kässi massin oksioni wisil müidu keda  Ado Ottas rohkemba hina est ostis ja pakkus 3 Rubl 35 Kop, kelle est müidu sai. Sest rahast om 35 kop oksioni lehte wälja paneki kullus wõetu ja sai 3 Rubl temma Päraha wõlla kistutuses.
Otsus: seda Protokolli ülles panda.
Päkohtumees J. Link /allkiri/
Kohtumees  Johan Klaosen XXX
Kirjutaja: J. Weber /allkiri/
