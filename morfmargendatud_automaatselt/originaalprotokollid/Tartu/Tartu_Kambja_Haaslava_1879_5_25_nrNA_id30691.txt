Haslawa kogokonna kohus 25 Mail 1879.
Juures istus:
Pääkohtumees Mihkel Klaos
kohtumees Johan Link
dito Johan Udel
dito Rein Anko
Tuli ette   Juhan Paeiw Kriimanist ja kaebas, et  Jaan Zobel temale 30 rubla wõlgo ja nõuab seda raha.
 Jaan Zobel ette kutsutud ei salganud agga temmal ei ole raha.
Mõistus:
 Jaan Zobel massab 8 päewa sees 15 rubla ja Mardi päew 1879 jälle 15 rubla  Johan Paewa haeks wälja.
Kohtolauan oliwat
Päkohtomees Mihkel Klaos
Kohtomees Johan Link
dito Johan Udel
dito Rein Anko
Mõistus sai mõlemile kohtu käijatele kuulutud T.S.R pärra a 1860 § 772, 774 kuulutud ja olid mõlemad sellega rahul.
