Ette tulli walla wöörmünder Reinhold Stamm ja kaibas, et Mai kuu 1880 aasta kirriku tee tegemise ajal on Aidamehe tallu poiss Tõldsepp oma tee jagu sandiste teinud ja oma  tee jao truubi peale mulla mättad pannud mis perast tee kassa kaibajat kui tee ära andjad 5 rublaga on trahwinud ja,  et temal omal kui iga teisel tee tegial tee teha ja mitte polle ülle  keegi ja selle perast peab iga üks kes teed teep issi oma trahw kandma.
 Jürri Tõldsepp wastas, miks ametimehed enne silla  saksa sõitmist polle tee jagu ülle kaenud selle perast tema ei wõi tee trahwi maksa. Selle peale andis kaibaja wastuse kaibatu olla weel see hommiko kruusa lahutanud kui tema on selle saksaga sõitma minnud selle järgi polle sedda   enne wõimalus olnud kaeda.
      Kohtu otsus: Jürri Tõldsepp peab see trahw mis tema sandi tee tegemise läbi on tulnud 5 rubl. wälja maksma. Ette loetu kohtu käiattelle.
                            pä Kohtumees: Hindrek Horn
                                 Kohtumees: Märt Piiri
                                  Kohtumees: Josep Soiewa.
