   Weski kogukonna kohus 19 weebruaril 1888.
                                                        Koos oliwad Pääkohtunik: M. Karu.
                                                                              Kohtumees: J. Aleksanderson
                                                                               Kohtumees: J. Kullberg.
Peeter Kullbergi kaebtuses wastu Jüri Saar ja tõised hobusega ülesõitmise perast kutsuti ette tunnisataja Peep Sussep Krüüdnerist ja tema ütles küsimise pääle. - et tema perast sõitnud, tema ja Wollmer Wanak ja Johan Mark tõstnud ree Peeter Kullberg´i päält ära ja säädnud ho-
buse uuesti ette. Tõised ütelnud temale, et Joh. Pehk olla ütelnud: "Kui walla mehe ees peaks olema, neist sõidan ma üle." -
Tema sinnajõudmisel, kus Kullberg ree all olnud, siis olnud Jüri Saar, Peeter Rüga ree nurga pääl rühmi kalla
