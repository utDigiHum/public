Sel 27 Webruaril 1889
Selle otsuse päle sest 23 Januarist s.a Kusta Janseni kaibuse asjan wasto Jaan Roiland olli kutsumise päle ette tullnu Jürri Schwalbe ja Leo Schwalbe ja tunistas et rükki põimo aigo olli Kusta Jansen ärra lännu ja ollnu 3 päiwa ärra, sis wõtnu perrenaene teda tagasi ja et se pois ütte rehhe aigo pudus olli ja pühapäiwal ollnu se pois allati kodust ärra, poolpäiwa õdagu lännu ärra ja tullnu pühapäiwa õdangu tagasi nink nimelt üttel pühapäiwal käsknu perremees sel poisil kodun olla, ent pois lännu ärra, heina aigo wõtnu Karl Roiland päiwilist poisi assemele ja massnu 50 kop päiwa est.
Johan Pruks tunistas, et seda om tema nännu et pois rükki põimo aigo pudus ollnu ja muidu ollnu se pois allati pühapäiwite kodust ärra.
Karl Roiland Meksist andis ülles, et rükki põimo ja heina aigo ollnu se pois 4 päiwa pudus, tema wõtnu päiwilise ja massnu 50 kop päiwa est ja selle et Kusta Jansen tagasi tahtnu tulla jätnu tema 3 rubl kaubeltut palkast maha. Tema ollewat 1 rubl est selle poisile Krami tonu.
Mõistetu: et Jaan Roiland 4 rubl 50 kop Kusta Jansenile 8 päiwa sehen perra massma peab, sest pool astat om Kusta Jansen teninu, mis 18 rubl 50 kop wälja teeb, sest om kätte sanu 			Rubl			Kop		Jaan Roilandi käest			11					Karl Roilandi käest			1					puduwade päiwade est 2			14					mis perra sis massmada om			4			50		
Selle et Kusta Jansen tenistusen wastaline olli, olli Jaan Roilandil õigus teda tenistusest lahti laske.
Se mõistus sai § 773 ja 774 perra Kusta Jansenile kulutedo ja õpetus leht wälja antu.
Sel 3 Märzil sai se mõistus Jaan Roilandile § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks XXX
"   Jaan Toom  XXX
