Se kooliwannemb Mihkel Hansen kaibap, et Johan Papp ei saatwat latsi kooli ning ollewat jubba 1 rub. 80 kop. wõlgo kooli trahwi. Michel Hansen nowwap se koolitrahw 1 r. 80 k. mis temma jubba kirriku man ärra massnut, kätte.
Johan Papp wastutap, et temmal ei olle mitte woimalik latsi kooli saata ja ei jowwa temma se trahw kah massa.
Moistus: Johan Papp piap warsti se koolitrahwi rahha 1 rub. 80 kop. hõbdt. ärra massma ning piap latset koolin saatma.
Kohtomees Jürri Luhha XXX
Abbi-Kohtomees Jaan Opmann XXX
Abbi-Kohtomees Märt Labber XXX
Mositus sai kuulutud ning suuremba kohtokäimisse õppetusse kirri wälja andtu.
