Krimani koggokonna kohtus 20 Septbr. 1868.
Kawwastust  Peter Sikka tulli koggokonna kohto ette ja kaebas et  Roijo karjamõisa perrispiddaja Jürri Klaos on minnewa kewwade kui Temma mant ärra läksin minno käest ärra risunud 5 lammast ja üks wanker mis ma minnewa kewwade weel lasksin tetta kui Roijole tullin.
 Jürri Klaos tulli 4mal Octobril ette ja ütles se on tõssi kül et ma ollen 5 lammast ja se wanker tema käest risunud ja minnewa näddali olli ta ütte lamba minno karja mant ärra winud warguse wisil ja se wanker mis ma Temma käest wõtsin olli minno wanker muud kui 2 sõri olli Temma jaggo ja Temma Krami ollen ma omma wõlla eest kinni pannud enda kätte sest ta jättis minnewa asta mulle 30 hobbuse päewa teggemata ja muud krami olli ta minno käest wõtnud mis tal praegust maksmata on nimelt			Rubla			Kop		12 wakka Kartoflid			9			40		1 wak kaero			2					1 wak rukkid seemnest			4					ja puhhast rahha			2			50		Summa			17			90		Ta olli minno käest wõtno sest rahhast on Ta linna kakkumise ja rükki leikusega tassa teinud			4					Ning on weel Temma käest sada Summa			13			90		
 Peter Sikka tulli 4 Octobril ette ja ütles: minna ollen rukki leikusega ja linna kakkumisega need wankri tassa teinud se wak rukkid mis Klaos andis ollen ma mahha külwanud.
J. Klaos ütles: minna ollen se wak rükkid sul lainanud et piddid jälle taggasi maksma kui omma rukkid olled peksnud.
Mõistus: 4 Octobril 1868.
Koggokonna kohhus mõistis et Peter Sikka peab Jürri Klaosele 13 Rubla 90 kop ärra maksma ja  Klaos  peab siis  Sikka risutud kraam kätte andma ja arwab koggokonna kohhus et  Klaosel ei olle õigust neid hobbesega teo orjuse päewi nõuda sepärrast et  Klaos siis kohhe ei olle nõudnud kui  Sikka  Päiwiga Temmale wõlgu jänud.
Man  olliwa:
Peakohtomees Jürri Bragli XXX
Kohtomees Juhan Mina XXX
Kohtomees  Ado Lentzius XXX
 Peter Sikka ei olnud selle mõistmisega rahhul ning selsammal päewal protokol.
Kirjutaja assemel:  M. Baumann  /allkiri/ 
