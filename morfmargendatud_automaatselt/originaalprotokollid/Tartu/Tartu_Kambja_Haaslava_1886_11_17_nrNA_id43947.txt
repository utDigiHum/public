Haslawa Kogukonna Kohus sel 17 Nowembril 1886
Man ollid: 
Kohtumees Jürri Kärn.
Tännasel päiwal andis Wallawanemb wallawalitsuse nimmel Magatsi wilja wõlla lehe Kogukonna Kohtu kätte ja pallus neid wõlgo kui tarwis risumisse teel sisse wõtta, selle et wõlgniku seni aijani seda wõlga massnu ei olle.
Otsus: seda protokolli ülles panda.
Kohtumees: Jürri Kärn XXX
Kirjutaja: J. Weber /allkiri/
