 Sellsamal päewal 26 augustil 1888.
                                              Wallawanem: J. Hargi. -
Selles kaebtuses kutsuti ette kaebtuse alune Hindrik Pirsku ja ütles küsimise pääle, et tema küll mitte löönud ei olla, waid tema olnud nõnda wäsinud (purjus) et ta midagi ei mäleta, waid teda kiskunud Johann Tross ja Jüri Jaska kõrtsist wälja, siis näinud tema, et Joh. Tross kõrtsmik Hans Kõdarale löönud. - Jüri Jaska löömist kõrtsmikule ei ole tema mitte näinud. Ka olla tema näinud, kui Johan Tross Margus Kõdara kallale karanud, tema kuue lõhkunud, teda rinnust kinni pidanud ja rusikuga löönud. Jüri Jaska löömist ei ole tema näinud ehk ei mäleta, sest et ta wäega wäsinud olnud. - Et äramineku juures mingisugust kaitseriista oleks olnud, sellest ei tea ehk ei maleta tema mitte midagi. -
                                              Otsus: Jüri Jaska, Johan Tross ja Jaan Moluk ette kutsuda. -
                                                                                  Wallawanem: J. Hargi
                                                                                     Kirjutaja: P. Eisenschmidt
