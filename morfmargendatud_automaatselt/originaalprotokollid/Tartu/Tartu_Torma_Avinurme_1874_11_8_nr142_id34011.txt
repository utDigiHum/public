Awwinorme Mets kogokona kohtus sel 8 Nowembril 1874.
koos ollid.
pea kohtomees Josep Kask.
teine kohtomees Juhan Koppel.
kolmas kohtomees Maddis Sildnik
Krono metsawaht Andres Raja astus ette ja pallus kogokona kohut et kui kogokona kohus on temma Maja warrandust sel 5 Oktobril ülles kirjotanud siis on temma naese koddost todud ülles woetud, ja on 1 lehm 1 lamas ja Naise kirst, ning peale selle weel üks weike laste wodi, mis wõeras olnud, ja pallub temma selle pärrast, et need Naise kramid ei sa oksjoni peal ärra müüdud.
Mart Sildnik tunnistas et Andres Raja on Tomas Tomikulle koddo wäimehhest lähnud ja on siis Tomas Tomik omma tütrele kaasa annud 1 lehm 3 lamast ja prudi kirst, ning on siis se naine ommale üks uus wodi teha lasknud, ning selle eest üks lammas annud.
Moistus
Koggokonna kohus moistab Liihwlandi tallo rahwa seaduse pohja peal Aastal 1860 §§ 946 all et Andres Raja Naine, Kai Raja, ei prugi oma kodust todud warraga omma mehhe trahwi maksa, ja saab se ülles kirjotud warrandus oksjoni kraamist mahha arwatud, ja nimmelt se lehm 1 lammas, Naise kirst ja woodi, ning üks weikene laste wodi, mis laenatud ja wõeras on.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
