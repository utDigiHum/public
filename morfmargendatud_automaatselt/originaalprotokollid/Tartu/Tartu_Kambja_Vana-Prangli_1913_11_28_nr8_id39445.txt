Rospiska
28 nojabrja 1913 g. polutšili mõ Jan Manglusov Vint, Julia Manglusovna Tross, urožden. Vint i Anna Manglusovna Karjusse, urožden. Vint ot Petra Joganova Mets svoi nasledstvennõja doli v summe tridtsati (30) rublei každõi, kakovõja dengi mõ imeli polutšit ot Petra Metsa, soglasno duhovnomu zaveštšanija umeršago Jogana Metsa ot 12 Dek. 1906. odnaradovannago i priznannago zakonnõh v Vrangelskom vol. sude 8 Dek. 1907 goda Delo N 51 - 1907 goda
Krestjanin I Wint [allkiri]
Žena krestjanina: J Tros [allkiri]
Jeja muž i sovetnik: Jaan Tros [allkiri]
Žena krestjanina: Anna Karjuse [allkiri]
Jeja muž i sovetn.: Kusta Karjuse [allkiri]
Dalše zajavljajet žena krestjanina Milija Joganovna Russak, uroždennaja Mets, što ona polutšila ot svojego brata Petra Joganova Mets, v set nasledstvennoi doli po duhovnomu zaveštšaniju otsa: nalitšnõmi dengami pjatdesjat (50) rublei; odnu korovu stoimostju v sto rublei i odnu svinju stoimostju v semdesjat rublei i tri ovtsõ stoimostju tritsat rublei, vsego ona polutšila nalitšnõmi dengami dvesti pjatdesjat (250) rublei - v tšem i rospisujetsja
Žena krestjanina: Miili Russak [allkiri]
Jeja muž i sovetnik: Petr Russak [allkiri]
Tõsjatša devjatsot trinadtsatago goda Nojabrja 28 dnja rospiska eta javlena k zasvidetelstvovaniju Vrangelskomu vol. sudu, Jurjevskago ujezda, krestjaninom Janom Manglusovõm Vint, ženoju krestjan Julijeju Manglusovoi Tross, uroždennoi Vint, Annoju Manglusovoi Karjuse, urožden. Vint i Milijeju Joganovoi Russak, uroždennoi Mets, pri mužah-sovetnikah: Jan Tross, Kustoju Karjuse i Petrom Russak, živuštšimi v StaroVrangelskoi volosti i imejuštšimi zakonnuju pravosposobnost k soveršeniju aktov; pri etom vol. sud udostoverjajet, što rospiska eta sobstvennorutšno podpisana Janom Vint, suprugami Tross, Karjuse i Russak. 
Po reestru N 8.
Predsedatel suda: K. Pala [allkiri]
Tšlenõ suda: A. Potahov [allkiri] K Eli [allkiri] 
Pisar suda: J Weske [allkiri]
