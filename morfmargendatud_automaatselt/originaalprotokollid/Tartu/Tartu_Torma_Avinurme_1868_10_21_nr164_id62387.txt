Awwinorme Koggo konna kohtus sel. 21el October 1868
Kohto juures olliwad
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Astus ette Jürri Ilwes ja kaebas Kärrasi külla perre meiste wasto, et temma seal küllas 6 Aastad seppaks olnud ja siis sandiks jänud ning et temma ellanud külla seppa majas ja sinna külge üks weikene kamber tehnud, mis agga perremehhed ärra lahhutanud, ja tedda wasto talwe majast wälja ajawad, ja pallus kuhho temma talwest korterid sab.
Kärrasi külla perremehhed astusid ette, ja räkisiwad, et nemmad ommale teise Seppa wõtnud, ning et selle pärrast, seal külla seppa majas ennamb ruumi ei olle ellada, ja et sest kambrist ei olle suurt asju olnud mis nemmad ärra lahhutanud.
Moistus
Kärrasi külla perremehhed peawad Jürri Ilweselle korteri murretsema, ja kui nemmad sedda issi ei te, lähheb peakohtomees sinna sedda asja õiendama. 
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
