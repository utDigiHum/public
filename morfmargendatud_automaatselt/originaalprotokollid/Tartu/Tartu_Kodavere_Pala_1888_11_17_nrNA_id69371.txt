Madis Wadi kaebus wastu Willem Wadi. Kusta Nukka tunnistab et tema seda selgelt mäletab et Willem Waddi isa Annus Wadi umbes 20 aasta eest Madis Wadi käest peale 400 rbl. aseme ostmise raha tagasi maksetud saanud ja lubanud Annus Wadi selle rahaga omale kohta osta.
 Kusta Wahi tunnistab et umbes 20 aasta eest Annus Wadi ja Madis Wadi koha riiu pärast kohtu ees olnud. Annus Wadi tulnud kohtu toast wälja ja olnud raha käes kui palju ei tea tema mitte ja lubanud selle rahaga Punnikweres omale kohta osta.
 Jüri Priks tunnistas niisama kui Kusta Nukka muud kui lisas juure et see asi seeaegse protokolli raamatus seisma peab.
 Otsus see protokolli raamat mis praegu Tartus Landgerihti kohtus seisab sisse nõuda.
                                                        /: allkirjad:/  
 Johannes Peramets tunnistas et temal küll kedagi Liisa Kasikuga tegemist olnud ei ole aga Paul Rätsep olla kuulu järele Liisa Kasiku peigmees olnud.
Otsus Liisa Kasik ja Paul Rätsep tunnistuste kuulutuseks ettetarwitada.
                                     /:allkirjad:/
