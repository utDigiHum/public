Sell 16 Aprilil 1884.
Möda lännu kewade käinuwa kohtumehe Sae tallun ja keldnuwa selle Tallu Perremehe Jaan Wirro kätte Peter Labani palk kinni; Jaan Wirro ja ka Peter Laban üttelnuwa sis et Peter Laban om omma palka jobba wälja wõtan ja et midagi sada ei olle, ent nüid tunistab Jaan Wirro ja ka Peter Laban ärra et Peter Labanil praegu weel möda länu suwe palka sada om, mis läbbi wõls awalikus sai.
Selle päle sai Mõistetu: et Jaan Wirro 3 Rubl ja Peter Laban 1 Rubl 50 kop 8 päiwa sehen waeste heas trahwi massma peawa, me neile kulutado sai.
Päkohtumees Jaan Link /allkiri/
Kohtumehe  Johan Klaosen XXX
Peter Iwan /allkiri/
J. Krosbärk /allkiri/
