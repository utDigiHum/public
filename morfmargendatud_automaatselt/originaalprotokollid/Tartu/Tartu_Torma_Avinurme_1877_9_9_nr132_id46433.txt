sel. 9. September 1877.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees  Andres Kiik
kolmas kohtomes Josep Welt
Waddi Mölder Tõnnis Treial kaebas et temma tüdruk Marri Lodes on temmale tunnistanud, et temma peab Jürri Waino tütrega Leno Waino temma Aita lukk ühhe Naelaga lahti tehnud ollema ning sealt jahhu. tango, ubbe, nissujahhu, wõid ja lihha warrastanud ollema. ja pallus koggokonnakohhut sedda asja järrel kuulata et need wargad trahwitud sawad.
Marri Lodes räkis et Leno Waino on tedda pallunud temmale middagi anda ja on temma siis neljal pühhapäwal kui möldrid koddus ei olle olnud. Aidast jahhu lihha, tango ja muud asja annud. ning on Leno Waino aida lukku naelaga lahti tehnud.
Leno Waino räkis omma issa Jürri Waino juures ollemissel. et se kaeptus temma peale hopis walle et temma wargust wasto wõtnud, ja ei olle se tüdruk temmale ei ühtegi annud, ja wõtnud se tüdruk temma käest ühhed pooled sapad mis temma katki piddand ja nõuab temma nende poole sabaste eest 3 Rubla.
Moistus
Et Marri Lodes issi tunnistab et temma omma perremehhe warrandust warrastanud ning Leno Wainole annud se agga sedda ärra salgab. ja selle ülle ka tunnistust ei olle, siis moistab koggukonnakohhus et Marri Lodes selle omma tunnistud warguse eest 3 pääwa ja öe peale trahwist kinni pantud saab. ja peab Marri Lodes need pooled sapad jo Leno Wainole kätte andma. ja Jürri Waino jälle Marri Lodesele temma riided ja ei tohhi Jürri Waino Marri Lodest. ei millagi omma juures sallida
Josep Laurisson XXX
Andres Kiik XXX
Josep Welt. XXX
