Tullu Kohdu ette Liis Konnu nink kaiwas et Peep Tens neid kats wakka kartohwli mes Liis Konnul Peep Tensi ma pääl mahha olli pantu Kätte ei anna ja kelle eest Liis Konnu kats päiwa mes peep tens selle ma eest olli tahtnu ärra olli tennu ja selle ma päle sitta olli pandnu.
sai Kohtu ette Kutsutus Peep Tens nink Liso Konnu kaibus ette ütteldus Peep Tens wastas minna olle Lillo Konnuga kes minno kolm ajastaiga om teenu kaup tennu minnol ei olle Liis Konnuga middagi teggemist.
sai Lillo Konnu ette kutsutus nink kussitus kas emmal se kartohwli seme ol wäe peal. Lillo Konnu wastas se seme olli emmal ja aemma om Peep Tensile päiwa tennu ma harrimisse eest se ei puttu minnole middagi selle kaipusse päle
moistse Kohhus Liis Konnul ne kartohwli kätte et ne kartohwli olliwa Liis Konnul es olle mitte Lillol Konnul temma pojal sest et Liis Konnu pand male sitta ja teggi ma eest päiwa.
Pakohtumees P. Tarrask &lt;allkiri&gt;
Abbi Jaan Lük &lt;allkiri&gt;
Abbi Jaan Petso XXX
Kirjotaja wöölmöndre Taniel Konts &lt;allkiri&gt;
