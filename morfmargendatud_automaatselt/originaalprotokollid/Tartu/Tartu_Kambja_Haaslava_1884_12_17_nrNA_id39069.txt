Sel 17 Detsembril 1884.
Kaibas  Karl Toom, kes  Tarto Linan Aleksandri ulitsen N 48 ellab, et temmal 8 Rubl Johan Klaoseni käest tehtu töö est sada om ja pallus et Kohus seda keige protsentiga massma sunis ja protsenti 1875 astast sadik.
Johan Klaosen wastutas selle kaibuse päle et temal mitte midagi Karl Toomile massa ei olle, ega kõrd om tema töö est ärra massnu. Selle töö est om tema kohtu laua peal ärra massnu, mis ka 1875 asta Protokoli ramaton seisab, keda sekõrdne kohtumees Enno Jaan Soo ja Loko Jaan Grossberg teadma sawa.
Otsus: et tunistaja ette kutsuta om. 
Päkohtumees: J. Link /allkiri/
Kohtumees: Johan Klaosen XXX
Peter Iwan /allkiri/
