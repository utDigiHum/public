Kadri Sepp wasto Karel Olla. 
Karel Olla ja Kadri Sepp eestkostja Karel Kooli kostjad ollit siin. Kusta Aunapuu, tunnistaja, ütles. Temma muud selle asjas ei tea, et agga Tõnnis Rosenberg temmale minnewa talwel üttelnu ma käisin korra Kadri Seppa jures ja  pannin temmale /: Wälja:/ Karel Olla pallus weel Jakob Waddy üllekulata, mis wasto Kadri Seppal middagi räkimist es olle./:läksit wälja:/ Jakob Waddy tunnistas: Temma selle asja sees middagi ei tea, ei olle ka issi Kadri Seppa prukinu./:wälja:/ Karel Olla andis weel tunnistajaks Michel  Kirtsi Palla, Karel Kurs Hallikult. Kadri Seppal es olle ka neite tunnistajade wasto middagi räkimist, sai mõistetud. Tunnistajat ka teises kohtupäewas ette kutsu.
                                    Peakohtumees:Paul Willemson
                                           Kohtumees:Jos.Hawakiwwi
                                           Kohtumees: Josep Rosenberg
                                           Kirj, allkiri   
