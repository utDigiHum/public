Protokolli peale 25 Juunist s. a. N: 122 sai ettekutsutud Jürri Welt ja wastas küsimise peale et Karl Sallo tema wastu ütelnud et Karl Alliku isa see raha kolm rubla ja kiri kätte saanud olla, aga tema, Karl Sallo tahta jonni perast Alexander Rosenbergiga kohut käia, sest et Rosenberg teda ükskord sõimanud olla. See olnud selajal kui see kirja üle kaebus Kihelkonna kohtus olnud.
 Toomas Welt ettekutsutud andis üles, et Karl Sallo seda ütelnud, et tema teab küll et see kiri Karl Allika isale nii kui ka raha 3 rbl. tulnud mitte temale muud kui adress olla tema nime peal.
 Karl Annuk ettekutsutud andis protokolli et Karl Sallo olla mitu kord ütelnud et see kolm rubla raha ja kiri olla Jürri Allikule Karli isale tulnud muud kui Karl Sallo adressi peal, raha ollu niisama on
kiri Jürri Allik käte saanud aga jonni perast, et Alexander Rosenberg teda, Karl Sallod, sõimanud, tahta tema kohut käia. Aleksander Rosenbergile saiwad tunnistused mis kohtu juures olnud awaltud ei olnud temal kedagi juure anda.
 Otsus Karl Sallo tunnistuse kuulutuseks ette tarwitada.
              Peakohtumees J. Sarwik.
              Kohtumees       J. Stamm.
              Kohtumees      M. Piiri .
              Kirjutaja GPalm. 
