Awwinorme Koggo konna kohtus sel. 29mal Julil 1868
Koos olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Jaan Saar.
Astus ette Jürri Lepp Waddi külla N 26 tallu perremees ja temma emma Kai Lepp ning selle poeg Mihkel, ja pallusid koggukonna kohhut nende leppimist kinnitada, et Jürri Lepp omma tallo maade küllest mis 8 thaldrid suured kolmas jaggo maasid omma poole wenna Mihklile annab, mis eest Mihkel Lepp siis kolmas jaggo renti ja keik teised Walla maksud maksab, ning jäeb se issa pärrandus Liiso Leppa jaggo 24 Rubla ja 10 kop. ka Jürri Leppa kätte nikauaks kui temma Jürri juures ellab. mis ta ärra lahkumisse ajal ärra nõuda woib Jürri käest.
Moistus
Siis on koggo konna kohhus nende enneste leppimist kinnitanud ja sedda Prottokolli ülles tähhendanud.
Mis tunnistawad kohtomehhed
Mart Jaggo. XXX
Mart Liis XXX
Jaan Saar XXX
