Leena Toots Johannes Kükkita juuresollemisel 14. aastad wanna, annab üllesse, et Johannes Poolakese polle temma külge millalgi puutunud, agga Johannes Kükkit annab üllesse, et temma olla öösel jaole saanud, kui temma Johannes Polakese tüdrukukese juures olnud, ja püksid olnud jubba lahti.
Johannes Poolakese 17. a. wanna salgab.
Ewa Moor ütleb, et kui temma üllesse aetud olnud Johannes Polakese omma asseme peal.
Kohhus moistis:
Niisugguse innetuma töö eest saab Johannes Poolakese kohtu poolt 15 witsalöögiga karristud, ja maksab sõidu ja kohtu kullu 3. rublaga Johannes Kükkitajale.
/: Otsus kulutud sel 8. Januaril 1882 a, Tallorahwa seaduse juhhatusel ja rahhul oldut:/
Maddis Erm olli Johannes Polakesel kõrwal seisja:/
A. Krimm [allkiri], pea
Maddis Erm, kohtum.
Tawet Kook, -"-
Josep Maddi, -"-
