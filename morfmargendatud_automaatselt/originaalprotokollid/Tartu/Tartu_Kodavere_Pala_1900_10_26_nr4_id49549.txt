Batrakski dogovor na mõznuju zemlju. Segodnjašnjago dnja meždu Gallikskim mõznõm     upravlenijem  krestjaninom Janom Kask blagoobdumano uslovlen i napisan nižesledujuštši dogovor:
§ 1. Gallikskoje mõznoje upravlenije sdajet mõznuju batrakskuju usadbu Puseppa, stoimostju v 6 talerov 1 68/112 grošei v arendu: srokom na tri goda, stšitaja s 23 aprelja 1901 goga po 23 aprelja 1904. goda.
§ 2. Jan Kask platit za nazvannuju usadbu denežnuju arendu vsego po- rub,- kop. v god, a imenno 1go aprelja-rub - kop- i 1go oktjabrja-rub -kop vpered i objazan krome togo ježegodno bezplatno proizvodit sledujuštšija rabotõ, a imenno:
1) žat i klast v skirdõ 6 lofštelei ozimi;
2) žat 10 lofštelei jarovago hleba;
3) kosit 3 lofštelei klevera;
4) kosit 10 lofštelei sena;
5) kosit-   lofštelei goroška;
6) sobirat-lofštelei kartofelja, kotorõi svozit tuda, kuda mõznoje upravlenije prikažet;
7) svozit- sažen- 1 aršinnõh drov;
8) uravnivat 150-200 saženei dorogi tam, gde mõznoje upravlenije prikažet;
9) 138 peših dnei, iz kotorõh dolžen postavit 93 dnei do Mihailova dnja i ostalnõle v tetšeniju  zimoje. Za každõi nepostavlennõi rabotši deneg on uplatšivajet  letom 75 kop. i zimoju 50 kop.
Ispolnenije oznatšennõh rabot razrešajetsja soglasno § 168 Položenija o krestjanah Lifljandskoi guberniji ot 13 nojabra 1860 g. Za každõi rabotšiji den postavlennõi bolše uslovlennogo uplatšivajutsja  letom mesjatsju 40 kop l zimoju 30 kop.
O brevnah i žerdjah na potšinku ili vozdenije strojeni arendator batrakskoi usadbõ dolžen zajavit do Mihailova dnja  i objazn on podtšinjatsja vo vseh otnošenijah lesnomu ustvu; v slutšaje narušenija im sego ustava, on budet lišen svojei usadbõ.
§ 4. Arendator batrakskoi usadbõ objazan vesti hozjaistvo v sdannoi jemu v arendu usadbe po nadležaštšim zemledeltšeskim pravilam i sdat usadbu imeniju obratno po istetšeniji sroka sego dogovora b sledujuštšem porjadok:
a) vse strojenija i v osobennosti ih krõši dolžnõ bõt horošo potšinenõ;
b) ržanoje pole dolžno bõt horošo obrabotano i svojevremenno obsemeneno horošim semenem;
v) zaborõ dolžnõ bõt horošo potšinenõ;
g) senokosõ dolžnõ bõt otšištšenõ ot hvorosta i vse kanavõ otšištšenõ;
d) iz rabot k buduštšemu ekonomitšeskomu godu pžanoje požnivo dolžno bõt vspahano i horošo vsboroneno;
e) On polutšajet 4 saženi 1 aršinnõh drov dlja toplivami 5 saž hvorosta. Za rubku uplatšivajet arendator usadbõ.
§ 5. V slutšaje jesli batrak po prikazaniju mõznago upravlenija vozvedet novõja strojenija ili potšinit starõja, to eto on proizvodit na svoi stšjet, ne imeja prava trebovat kakogo libo za eto voznagraždenija na kakovuju postroiku mõznoje upravlenije dajet bezvozmezdno brevna, kotorõja vse batraki sorazmerno velitšine ih zemli, dolžnõ  bezvozmezdno privezti k mestu postroiki; ravnõm obrazom vse batraki vmeste sorazmerno velitšine ih zemli, dolžnõ dat bezvozmezdno solomõ na krõšu soglasno istšislenija mõznago upravlenija. Za proizvodstvo kamennago fundamenta pod  hlev mõznoje upravlenije uplatšivajet po kvadratnõm sažnjam. Za tšistku  trubõ on  dilalivajet 60 kop. v god.
§ 6. Svoi sobstvennõja polja arendator mõznoi batrakskoi usadbõ dolžen obrabotõvat v tšetõreh tšastjah, tak, tšobõ nahodilis 5 lofštelei pod rožju, 16 lofštelei pod jarovõm hlebom, 5  lofštelei pod parom, pritšem on ne imejet prava zasevat par jarovõm hlebom ili  lnom.
§ 7. Solomu, seno i drugoi korm dlja skota arendator sei usadbõ ne imejet prava komu nibut ssužat ili prodavat.
§ 8. Mõznoje upravlenije imeejet pravo revizovat hozjaistvo usadbõ vo vsjakoje vremja ili samo ili tšerez svojego upolnomotšennago.
§  9. Arendator usadbõ imejet pravo zasevat  lnom tolko 1½ lofšteli
§ 10. Jesli mõznoje upravlenije poželajet arendatoru sei usadbõ sdelat otkaz v onoi, to etoi dolžno posledovat 23. oktjabrja do istetšenija sroka sego dogovora, pritšem arendator usadbõ ne imejet prava trebovat voznagraždenija za kakija libo rabotõ ili izderžki.
§ 11. V obezpetšenije totšnago ispolnenija prinjatõh na sebja objazatelstv arendator etoi batrakskoi usadbõ zakladõvajet vse svoje imuštšestvo, a v osobennosti svoi železnõi inventar, kotorõi dolžen  sostojat po krainei mere iz odnoi lošadi i trih golova rogatago skota i nahoditsja v horošem sostojaniji.
§ 12. V slutšaje esli arendator sei usadbõ budet obraštšatsja neostorožno s ognem i tšerez eto proizoidet požar, to on otvetšajet za proisšedšije ubõtki vsem svoim imuštšestvom. Krome togo mõznoje upravlenije uslovlivajetsja s arendatorom onoi usadbõ, ještšo v sledujuštšem:
1) Arendator usadbõ prinimajet na sebja uravnivat................ sažen.................. dorogi;
2) Arendator usadbõ nedolžen bõt zametšen v kraže, a takže, ne dolžen prinimat v svojei usadbe vorov ili kradennõh veštšei;
3) Jesli žilaja riga po vethosti prijidet v plohoje sostojanije, to imenije vozvodit takovuju na svoi stšet, pritšem odnako arendatorõ usadeb mõznoi zemli vse vmeste dolžnõ, sorazmerno velitšine svojei zemli, bezvozmezdno podvozit trebujemõi material; ravnõm obrazom oni dolžnõ podvozit material takže i na potšinku žiloi rigi.
§ 13. Dlja otbõvanija dnei, arendator usadbõ dolžen javljatsja každõi raz nemedlenno po prikazaniju imenija. Pri molodba zimoju s 6 tšasov utro do 7 tšasov vetšera.
§ 14.  Arendator usadbõ ne vprav dopuskat, tštobõ ego sobaki svobodno šljalis. Každaja sobaka, kotoruju vstretjat, vse ravno gde, vne granits usadbõ, budet nemedlenno ubita.
§ 15. V slutšaje neispolnenija arendatorom usadbõ kakogo libo iz oznatšennõh v sem dogovor punktov, on lišitsja svojei usadbõ, ne imeja prava žalovatsja na to v sud ili trebovat voznagraždenija.
§ 16. Sostojanije strojeniji arendnoi usadbõ  Puseppa
1) Žilaja riga horošaja;
2) Ambar i sarai srednije;
3) Hlev  sredni;
4) Banja srednaja;
5) Kuhnja........................................................
6) Kolodets sredniji;
7) pogreb sredniji;
8) Zabor vokurg dvora v horošem sostojanije;
Tšto nastojaštšiji dogovor vpolne protšitan i razjasnen arendatoru Janu Kask i tšto pomjanutõi arendator vpolne dovolen so vsemi oznatšennõmi uslovijami, on udostverjajet svoego podpisju.
Tak soveršeno v Pallaskom Vol. dom 26 oktjabrja 1900 goda.
Arendator batrakskoi usadbõ: Jaan Kask (allkiri)
   Ot imeni mõznago upravlenija: Eduard Bauman.
1900 aasta oktoobri kuu 26 päewal Palla walla Kohus tõendab, et ees olew teenistuse leping mõlemate lepingu tegijatele ette luetud ja nemad selle lepingu kinnitamiseks alla on kirjutanud, mis pärast Walla Kohtu Seaduse II jau § 278 ja Poslini Seaduse 1893 a.  § § 62 ja 81 p. 4 põhjusel see leping akti raamatusse Nr.17. all wõib sisse kirjutada.
                  Kohtueesistuja: J. Soiewa
                  Kohtumõistjad M. Saan.
                                              K. Hawakiwi
                          Kirjutaja Sepp.                
