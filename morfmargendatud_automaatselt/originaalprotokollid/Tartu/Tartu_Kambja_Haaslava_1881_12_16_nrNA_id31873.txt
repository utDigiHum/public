Haslawa kogokonna kohus 16 Decbr. 1881.
Juures istusid: Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
"   Johan Udel
Tuli ette  Peter Laban ja kaebas: temal on 25 rubl. minewast aastast palka peremehe  Johan Koorti  käest saada; aasta palk olnud 35 rubl., kellest rahast  tema 10 rubl. kätte saanud, niida, et weel 25 rubl. saada on, mis tema kätte nõuab.
 Johan Koort wastutas kaebtuse pääle, et aasta palk on kül nõnda olnud, aga kaibaja on 1sel Nowembril 1880 teenistusest ära läinud, kuna teenistuse aig kuni Jüripääwani 1881 kestnud; palka on tema Peter Labanile ära maksnud: ühe korraga 10 rubl., kaibaja wõlga Uniküla kõrtsi 3 rubl., Roijo kõrtsin annud 5 rubl. ja üks kord weel 2 rubl. ülepia 20 rubl. ja ei taha tema enam midagi kaibajale maksa.
Peter Laban ütles selle pääle wälja, et tema ennegi 3 nädalid enne teenistuse aea lõpmist ära läinud ja ei ole enam palka kätte saanud, kui 13 rbl. Uniküla kõrtsi wõlaga kokko arwatud.
Uniküla kõrtsimees Josep Tenno ütles wälja, et tema seda mitte ei mäleta, kas Joh. Koort on Peter Labani eest maksnud wõi mitte.
Tunnistaja Johan Linde Roijo kõrtsimees Kriimannist ei olnud mitte sija kohtu ette tulnud.
Mõistus: 
Sest 35 rubl. palga rahast saab maha arwatud: mis  Peter Laban kätte saanud 13 rubl. enne aego teenistusest ära mineki eest 5 rubl. 40 kop., Roijo kõrtsin antud 5 rubl. Summa 23 rubl. 40 kop. nõnda et weel  Johan Koort 11 rubl 60 kop. 8 pääwa sees Peter Labani hääks wälja peab maksma.
Pääkohtumees Mihkel Klaos  
Kohtumees Johan Link 
" Johan Udel
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi kõrgemat kohut tahab käija ja ei olnud Johan Koort mitte sellega rahul. 
