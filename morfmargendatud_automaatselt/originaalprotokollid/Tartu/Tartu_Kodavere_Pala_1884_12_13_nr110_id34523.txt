Tulli ette Tawet Willemson ja tunnistab siin kohtu ees, et Jakob Wända olla temale ütelnud, et tema maja  ei saa Josep Willale anda.
See tunnistus sai ette loetud.
Mõisteti, et tunnistus selge et Jakob Wända maja ei ole Josep Willale annud ja et see maja Jakob Wända oma on, siis maksab Paul Wända see
raha 3 rubl  Josep Willale 2 nädale sees wälja.
See otsus sai ette loetud ja Paul Wända ei ole rahul sai Apelation wälja antud.
                         peakohtum    O. Kangro
                         kohtum           J. Soiewa
                          kohtum           O. Mõisa
