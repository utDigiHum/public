Akt o razdele nasledstva.
Meždu naslednikami pokoinago krestjanina Jogana Ganzena, a imenno: vdovoju jego Mai Ganzen urožd. Ljude i detmi jego a) Jurijem. b) Janom, v) Joganom, g) Kustoju, Annoju v zamužestve Kromana, e) Minoju i ž) Lizoju Joganovõm Ganzen s odnoi, i sõna naslednikom jego Petrom Ganzen s drugoi storonõ, pri sodeistvii 14 Gaslavskago Volostnogo Suda, zakljutšen i napissan nižesledujuštšii, objazatelnõi dlja naslednikov i pravoprejemnnikov dogovarujstsihsja storon akta o razdelenii nasledstva.
§ 1.
Võšeupomjanutõje litsa, kak jedinstvennõje nasledniki umeršago Jogana Ganzena, utverždenõ v pravah nasledstva opredelenijem 14 volostnogo suda okruga 2 Jurjevskago Verhnjago Krestjanskago Suda ot 17 fevr. 1895 g. kak vladeltsev usadbõ Simo № 55 imenija Gazelau Jurjevskago ujezda, soderžaštšei v sebe 28,64 desjatinõ, zemelnoju stoimostju v 16 talerov 60 grošei, ne želaja ostatsja v obštšem vladenii etogo usadboju, položili prekratit eto obštšeje vladenije putjom dobrovolnago razdela, takim obrazom, tšto vdova Mai Ganzen ur. Ljude, Juri, Jan, Jogan, Kusta, Mina i Liza Ganzen i Anna Kroman ur. Ganzen, prinadležaštšeje každoje iz nih prava ha umstvennuju devjatuju tšast usadbu Simo № 55 sem peredajut svojemu sonasledniku Petru Joganovu Ganzen v polnuju jego sobstvennost s svojego storonõ Petr Ganzen objazujetsja vzamen pereustuplennõh jemu prav 
1) Bezplatno i kak nadležajetsja horošemu sõnu soderžatt u sebja svoju matt, vdovu Mai Ganzen, do jeja smerti, datt jei odeždu i kvartiru v ussadbe i.t.d. a takže soderžatt jei odnu korovu i ovtsu,
2) predostavitt svojemu bratu Janu Ganzen, rovno kak i materi svojei bezplatnoje u sebja soderžanije do jego smerti.
3) Võplatitt ostalnõm sonaslednikam, imenno: Juriju, Joganu, Kuste, Miine i Lize Joganovõm Ganzen i Anne Kroman ur. Ganzen  každomu svoju dolju v nalitšnõh dengah t. e. dvesti rublei.
Iz võšepoimenovannõh pod p 3 [ei loe välja] Kusta Ganzen uže polutšil svoju dolju i udostoverjajet v etom svojeju podpisju pod sim aktom, ostalnõm svoim sonaslednikam Petr Ganzen objazan uplatšivatt pritšitajuštsijasja im doli
a) Juriju Ganzen v tetšenii janvarja 1898 g.
b) Joganu Ganzen "  "  1901 g.
v) Anne Kroman  "  " 1904 g.
g) Mine Ganzen "  "  1907 g.
d) Lize Ganzen "  "  1910 g.
Sim razdelom vseh võšeupomjanutõje nasledniki vpolne dovolnõ i o peredele voprossov vozbuždatt ne v prave.
§ 2.
Stoimost ussadbõ Simo s prinadležnostjami dogovarivajuštsijesja litsa opredeljajut po sovesti tšetõre tõsjatsi sto vossemdesjat /4180/ rub., kakovõm summa prevõšajetsa kazjonnuju otsenku, iz toi summõ podlezonnõh odnako iskljutšeniju ukrepljonnõi na ussadbu Simo dolg po zakladnõm listam Lifl. Dvorjanskago Kreditnogo Obštšestva v razmere odnoi tõsjatsi trjoh sot rub /1300 rub./ i ukrepljonnaja v polzu grafa Šeremetjeva 12 nojabrja 1885 g. zakladnaja v odnu tõsjatsu vosemdesjat rub /1080 r./ tak tsto tsennost predmeta razdela sostovljajetsja summu v odnu tõsjatšu vossem sot /1800/ rublei.
§ 3.
Petr Ganzen sim objazujetsja prinjatt na sebja oznatšennõja v § 2 ukreplennõje na ussadbu Simo № 55 dolgi, v ossobennosti že dolg po zakladnõm listam v 1300 rub. Lifljanskomu Dvorjanskomu Obstšestvu; osvoboždaja soveršenno i navsegda protšihh svoihh sonaslednikov ot uplatõ takovogo i vmeste sim zajavljajet tšto  etot dolg v 1300 rublei i nõne i v buduštšem [ei loe välja] i ostanetsja, soglasno Ustavu obštšestva pervoju ipotekoju na krestjanskoi ussadbe Simo № 55 s inventarom i vsemi jeja prinadležnostjami, tsto on bezotgovorenno podtšinjajetsja i budet podtšinjatsja pod strahom vostrebovanija upomjanutogo dolga zakladnõmi mestami vsem nastojaštsim i buduštsim na zakonnom osnovanii sostojavsimsja postanovlenijam i rasporjaženijam Lifljandskago Dvorjanskago Kreditnogo Obštšestva i tsto on objazujetsja v totšnosti takovõhh ispolnjatt. Sverh sego Petr Ganzen sim bezuslovno objazujetsja nikogda ne predprinimatt nikakihh deistvii ili zakljutšatt nikakihh dogovorov, vsledstvije koihh po mneniju direktsii võšeoznatšennago Kreditnogo obštšestva, stoimost i dohodõ skazannoi spetsialno založennoi ussadbõ mogli bõ bõtt uhhudšenõ i umenšenõ i sim predostavljajet bezuslovno v protivnom slutšaje, neispravnosti jego v plateže protsentov i v drugihh vznossah võšeoznatšennomu Kreditnomu Obštšestvu pravo, nemedlenno vsei zakladnoi ssudõ ili naložitt na priobretjennuju im po semu dogovoru ussadbu Simo № 55 sekversta, a na vse jego ostalnoje imuštšestvo pohadataistvovatt položenije aresta.
§ 4.
Petr Ganzen sim objazujetsja,vpred do ispolnenija im vseh teh objazannostei po otnošeniju k svoim sonaslednikam kotorõhh izloženõ v paragrafe pervom sego dogovora, ni prodavat ni ottšuždatt us. Simo № 55 ni obremenjatt jejo dolgami i izjavljajetsja soglassije na vnessenije o seti nadležaštšei zapissi v podležaštšii otdel krepostnago rejestra.
§ 5.
Peredatša ussadbõ Simo v sobstvennosti Petru Ganzen uže posledovalo do podpissanija nastojaštšogo dogovora. Nakopivšiisja do peredatsi v Lifljandskom Dvorjanskom Kreditnom Obštšestve pogassitelnõi fond ha razremennoi ussadbe Simo ssude zakladnõmi lastami etogo obštšestva, postupajet takže v sobstvennost Petra Ganzen.
§ 6 
Vse rashodõ po soveršeniju i ukrepleniju nastojaštšago akta o razdele nasledstva nesjot Petr Ganzen odin.
 V udostoverenijem vsego võšeizložennago akt etot sobstvennorutsno podpisan vsemi dogovarivajustšimisja litsami.
Tak soveršeno v Gaslava 17 fevralja 1895 goda.
Vdova Mai Ganzen ur. Ljude negramotna, za nejo po litšnoi jeja o tom prosbe rospissalsja: Jaan Meier /allkiri/
V katšestve sovetnika vdovõ Mai Ganzen rospissalsja: Petr Mikelev Ganzen, no po negramotnosti Petra Mikeleva Ganzen po litšnoi jego etom prosbe rospissalsja Johan Link /allkiri/
Jürri Ansen /allkiri/
Po negramotnosti Jana Joganova Ganzen po litšnoi jego o tom prosbe raspissalsja Jürri Ansen /allkiri/
Peter Ansen /allkiri  Jugan Anzen /allkiri/  Kusta Anzen /allkiri  Anna Kroman Sünd. Ansen /allkiri/  Jürri Kroman /allkiri/ Miina Hansen /allkiri/
Liisa Ansen /allkiri/ Lena Aansen /allkiri/
Po negramotnosti Petra Mikeleva Ganzen po litšnoi jego o tom prosbe rospissalsja Johan Link /allkiri/
Sovetnik Minõ i Lizõ Ganzen: Jürri Ansen /allkiri/
Estonskija podpisi: Johan Link, Jüri Ansen, Peter Ansen, Anna Kroman sünd. Ansen, Jüri Kroman, Mina Hansen, Liisa Ansen, Lena Ansen, Johan Link oznatšajutsja po russki: Jogan Link, Jurii Anzen, Petr Anzen, Anna Kromana ur. Anzen; Juri Kroman, Mina Ganzena, Liza Anzena, Lena Anzen, Jogan Link, Kusta Anzen.
Tõsjatša vossemsot devjanosto pjatogo goda Fevralja 17 dnja nastojaštsii razdelnõi akt javlen v 14 Volostnoi Sude okruga 2 Jurjewskogo Verhnjago Krestjanskago Suda vdovoju Mai Ganzena ur. Ljude, i priglašennõm jeja assistentom Petra Mikeleva Ganzena, Annoju Joganovoju Kroman ur. Ganzen pri assistentsii jeja muža Jurija Kromana, Lenoju Ganzen pri assistentsii jeja muža Petra Miheleva Ganzen, Minoju i Lizoju Joganovõmi Ganzen pri assistentsii ih priglašennõh Jurija Ganzen, Jurijem Janom, Joganom, Kustoju i Petrom Joganovõm Ganzen, litšno izvestnõmi volostnomu Sudu i imejuštšimi zakonnuju k soveršeniju aktov pravospossobnosti.
Pri etom vlostnoi Sud udostoverjajet, tšto Jurii Ganzen, Jogan Ganzen, Anna Kromana, Juri Kromana, Mina Ganzen, Liza Ganzen, Lena Ganzen, Petr Joganov Ganzen podpissami sobstvennorutsno, i sa negramotnõje Mai Ganzen, Jan Ganzen i Petra Mikeleva Ganzen po ih o tom prosbe rospissalis: Jan Meier i Jogan Link, Jurii Anzen 
Predsedatel vol.Suda : J. Hansen /allkiri/
Pissar K. Laar /allkiri//XIV vallakohtu pitser/
