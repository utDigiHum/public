Tulli ette Komödiant H.Gonsboloff Rijast nink kaebas, et tema rentinud Aadu Tiirmanni käest üks tuba ära oma Komödie tegemiseks sest 4 Octoobrist kell 9 kunni 5 Octoobrini kello 7 ajani õhtu. Andnud 2 Rubla käeraha sisse, 8 Rubla jäänud wõlgu. Aga 4. Octoobril pääle lõunat tulnud kello pool wiis ajal Aadu Tiirmann ühe teisega raha perima ja kui tema seda puuduwat üüri raha tingitud aja lõpetusel wälja maksa lubanud, on Aadu Tiirmann ütelnud, et tema temal mitte enam edasi mängida ei lase. Käsknud ühel laada kubjal temale järele passida, et tema ära ei jookseks. Selle tüli läbi olla kõik komödiest osawõtjad ärapeletatud saanud nink tema nõuda Aadu Tiirmann'i käest kahju tasumist 20 Rubla. Aga  puuduwat üüri raha, 8 Rubla, lubap ika äramaksa.
Kutsuti ette Aadu Tiirmann. Tema ütles, et temal olnud küll Komödiantiga nenda leptid, aga Komödiant on tema poolt kõige oma kraamiga salaja äratulla tahtnud.
Laada kubjas Andres Treial ütles, et Aadu Tiirmann  on Komödianti käest raha küsinud, aga pahandust ei olewat mitte olnud.
Moisa rendi herra O. Zastrow ütles, et kauba tegemise mann olla tema Aadu Tiirmann'ile üttenud, et tema puuduwa üüri eest wastutap.
Mõistus: Komödiant H. Gonsboloff Riijast  peap lepingu järele puuduw üüri raha 8 Rubla Aadu Tiirmann'ile äramasma.
Komödianti kahju tasumise nõudmine saab tühjaks aewatud, et kahju suurus mitte awalik ei ole ja raha küsimise läbi mängu rikumist ei woinud ette tulla.
Et Komödiant Tiirmann'i korterist äratulnu, siis peap tema Tiirmannile see 8 Rubla kohe äramaksma. 
Kohtumõistus kuulutadi kohtumkäijile §773 nink 774 seletamisega.
Nemad leppinud kokku nink Komödiant maksis Aadu Tiirmann'ile 3 Rubla weel juure, summa 5 Rubla nink jäid sellega rahule.
Pääkohtomees Jaan Rebane (allkiri)
Kohtomees Jakob Kukrus (allkiri)
abikohtomees Kusta Jürjenson (XXX)
Kirjutaja J.Mäkssi
