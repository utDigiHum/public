Weskimõisa walitsus kaebab, et Jüri Märs olla mõisa metsast 1 pettai 9 tolli jäme 48 jalga pitk saega lõigatud päewa wõetud. - Kaebaja nõuab seadusliku trahwimist.
Jüri Märs ütles küsimise pääle, et tema olla seljaga küll seda nimetatud pedajat, kodu toonud, millest 2  pakku juba ära lõigatud tüwest olnud. -
Metsawaht Andres Sootla ütles küsimise pääle, et tema ise Jüri Märs kannu juurest katte saanud. -
                                            Mõistus: 4 r. 40 kop.
