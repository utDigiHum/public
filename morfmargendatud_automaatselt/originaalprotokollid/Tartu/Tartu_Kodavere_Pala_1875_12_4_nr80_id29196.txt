Palla Perremees Jakob Tuhha kaebas: Thomas Welt üttelnu 4 nädala eest temma naesele, et temma, kaebaja ahhila warras olla. Tunnistajat olla Jürri Tross ja Sohwi Peterson.
Tunnistaja Jürri Tross, et räkis: Thomas Welt üttelnu 4 näddala eest taggasi Jakob Tuhha naisele, et Suitso Jürri ahhilat mõisast warrastanu ja Jakob Tuhhale andnu, ja mõllemat ühhet wargat.
Tunnistaja Sohwi Peterson ütles: Thomas Welt üttelnu Jakob Tuhha naisele, teil on warrastud ahhilat, Saksa Jürri warrastab ja annab teile, mis peale Jakob Tuhha naine Thomas Weltile üttelnu
,, sa kurrati raisk.    /:wälja:/  Tunnistusest said kohtokäijatele etteloetu, mis järrel kaebaja weel Palla mõisa wallitseja ja kubjas tunnistajaks andis.
Tunnistajat  teises kohtopäewas ka weel ettekutsu.
                                Peamees:P.Willemsonn
                                Kohtumees: J. Hawakiwwi
                            Abbikohtomees: M.Lallo
                                             Kirj. allkiri                 
