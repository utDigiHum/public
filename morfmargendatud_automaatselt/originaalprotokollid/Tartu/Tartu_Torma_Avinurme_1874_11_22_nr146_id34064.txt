Awwinorme Mets kogokona kohtus sel 22 Nowembril 1874.
koos ollid.
pea kohtomees Josep Kask.
teine kohtomees Juhhan Koppel
kolmas kohtomees Maddis Sildnik
Wennewerre walla mees Tõnno Alterman kaebas et temmal Josep Karro käest 2 Rubla 40 kopp jo mitto Aastat saada, ja pallus koggo konna kohut, et temma omma kätte tahhab saada.
Josep Karro tunnistas et se tõssi on et temma Tõnno Altermanile 2 Rubla 40 kop wõlgo, temma agga waesuse pärrast ei jõua ärra maksa, ja pallus selle wõllaga kannatada.
Moistus
Josep Karro peab sedda wõlgo 1 Rubla 20 kop küündlapäwast, ja 1 Rubla 20 kop. Jürri pääwast 1875. Tõnno Altermanile ärra maksma.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
