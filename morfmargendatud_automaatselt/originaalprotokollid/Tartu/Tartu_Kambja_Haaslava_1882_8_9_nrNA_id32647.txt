Sel 9 Augustil 1882 
Tulli ette  Peter Tõlk, kes Maidlas ellab ja andis ülles et temal 48 Rbl 17 Cop Päraha wõlgo om ent kui temma Emma Liis Tõlk 15 asta est ärra surno siis om temma ema wäikene Ellomaja Kogukonna walitsuse polt  Jaan Willemsonile ärra müüdu, kost temma Pärahha massetu pidi sama, ent nüüd ei olle selle maja rahaga temma Päraha mitte massetu selle maja est om  Jaan Willemson 27 Rbl 80 cop massa lubanu 2 Rbl om temma selle maja rahhast kättesanu  ja pallus et kohus sedda wõlgo rahha  Jaan Willemsoni käest temma Pärahha kistutamisses sisse nõuda.
Sekõrdne wallawanemb  Lillo Hansen andis ülles et Jaan Willemson kül sele Liis Tõlki maja om ostnu ja ni kui tema mälletab 25 Rbl est. rahha massmist ei tea tema mitte 2 Rbl om temma nähten Peter Tõlkile masson.
Thomas Kahma tunistas et Jaan Willemson om temale ütelnu et Willemson tema kätte ütte asta Peter Tõlki Pärahha om masson, ent temma sest midagi ei mäleta.
Jaan Willemson wastutas 23 Augustil selle päle et tema selle maja 25 Rubl. est ostis ja om selle maja est se rahha joba amuki ärr massnu, tema om seda Peter Tõlki sötnu mes keik tassa om Jaan Kahmas om keik ülles pandno ja wõib selleperrast selgust anda. Jaan Kahmas ellab Tarto Linan.
Sel 23 Augustil 1882 sai Mõistetu:
et Jaan Willemson 23 Rubl. selle Tarre est Peter Tõlkile weel perra massma peab, selle et temma tunistuse perra 25 Rubl eest selle tarre oston om ja 2 Rbl selle päle ärra massnu om. 
Se mõistus sai kulutud ja õpetus leht wälja antu.
Päkohtumees: Mihkel Klaos  XXX
Kohtumees:
Johan Link XXX
Johan Udel  XXX
