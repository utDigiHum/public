Krimanni koggokonna kohtun 14mal  Febr. 
 Jürri Klaos  tulli  ette ja kaebas et üks õhto kui minna mõisast koddo läksin ollen minna kella 9 wõi 10ne aego  Mihkel Mahli enda karja aijast kätte sanud ning olli ka 1 üssa täis minno ellaja põhko Temma üssan mis Juhan Klaos ja Jaan Põdderson wõib tunnistata.
 Roijo  Möldri sullane  Mihkel Mahl  tulli ette ja ütles: se on tõssi kül, et  Klaos  on mind omma karja ajast kinni wõtnud ning pallen et ma sedda asja selletan se on se: meie talli sain mis karja aija sissen on, on rodlitest tetto aid ja sittaga toppitud ja õhto enne maggama minnekit läksin hobbest kaema ning leidsin et kange tuul olli selle põhho wõi sitta aija wahhelt kaarde alt ärra ajanud ja et kange tuul hobbosele peale puhhus läksin minna karja aija sisse riibsin wedike põhko wõi sitta kätte wahhele ja tahtsin karja allutse seest toppima hakkata ja  Klaos tulli sinna ja ütles: mis sinna ösi minno karja aijast otsid ja mõtles et minna Tema ellaja põhko warrastan agga sedda mõttet mul mitte ei olleks olnud ja kui meie Talli aed polleks mitte Temma karja aija sissen ollema siis polleks mul mitte senna asja ollema minna agga nüid ollinjust hädda perrast läinud aida toppima.
 Jaan Põdderson tulli ette ja ütles: minna näggin kül et 1 üssa täis põhko karja aijan maas olli mis  Mihkel Mahli käest ütle Jürri Klaos  wõtnud.
 Jürri Klaos ütles: süggise ükskord tulli Mihkel Mahl minno rehhe mant mulle wasto ja olli üks minno perro pu pak seljas ja teine kord olled weski poisiga minno õlgi rehhe mant 3 kubbo enda kartoflite peale winud agga sis ollin minna weel Möldriga hea ja ei tahtnud kaebada ent nüid olleme wihhalaseks sanud ning nõuan nüid ülle kige nende asjo 25 Rubla.
Mõistus: 14. Febr 1869.
Koggokonna kohhus mõistis et  Mihkel Mahl maksab Jürri Klaosele õlle leisika pealt 10 Cop põhho üssa täie pealt 15 Cop ja pirropu pakku pealt 15 Cop. Summa 60 Cop ja selle eest trahwist et omma lubbaga on wõtnud maksab waeste ladiko 150 cop.
Peakohtomees Jürri Bragli XXX
Kohtomees  Juhan Mina 
" "      Ado Lentzius 
Kirjotaja assemel  M. Baumann /allkiri/
