Endine Walawanem Willem Peramets kaebas, et Josep Sojewa weel 5 rubla pearaha wõlgu on ja  nõutis seda ka Pearaha sist wälja. Josep Soiewa ütles, et temma keik ära maksnu, mis tema weike pearaha raamat näitama. Weike Pearaha raamat, mis Sojewa käes  seisnu, näitas aga wälja, et se weel 5 rubla wõlgo on.
Mõisteti: Joosep Sojewa peab 8 päewa sees 5 rubla wälja maksma. Se mõistus sai seaduse wiisil eteloetu.
                   Peakohtomees: Kusta Kokka xxx
                   Koht.                   Kusta Nukka xxx
                   Abikoht.              Tomas Welt xxx     
