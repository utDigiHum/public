Ettekutsutud kadunud Kusta Kasiku waeste laste wöörmünder Willem Perramets ja andis aru, et  Kasiku tüttar Lisa olla arwata 26 a. ja poeg Willem toita ennast käsitööst, olla 20 a. wana ja tüttar Mina olla sant ja saada walla poolt ülespidamist. Warrandust  neil ei ole ja elawad tema, Perrametsa saunas.
Otsus: Kui sündinud ülestähjentada.
                        Peakohtumees J. Sarwik.
                              Kohtumees W. Oksa
                              Kohtumees  M. Piiri. 
                              Kirjutaja O. Seidenbach.
