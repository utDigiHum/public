Tuli ette Josep Nõmm ära surnud Jürri Möldri poja Kustawi wöörmünder ja wastas küsimise peale et se isast pojale järele jäänud raha nelja aasta intressidega kokku on 32 rbl. 98 kop. Tartu pangas seisab, Kustaw Mölder saab Josep Nõmme poolt ülespidamist nii kui  wiimase asjani.
                                                                                         Josep Nõmme
                                                                      Pääkohtumees O. Kangro
                                                                      Kohtumees J. Soiewa.
                                                                      Kohtumees O. Moisa.
                                                                      Kirjutaja allkiri mitteloetaw.     
