Krimannis 22 Januaril 1871.
Wastsest-Kustest  Juhan Kissel  tulli ette ja kaebas et  Juhan Krünberg olli minnewa kewwade minno käest 25 Rb hõb. rahha lainanud selle peale et lubbas Talwistest pühhist ärra maksta agga praegast on weel maksmata ning pallun et minna omma wõlg Temma käest kätte saan.
 Juhan Krünberg tulli ette ja ütles: minnul on kül Juhan Kisselile 25 Rublad maksta ja lubbasin Talwistest pühhist ärra maksta agga et mul rahha ei olnud ei sanud mitte maksta ning pallun et Juhan Kissel se rahhaga tullewa süggiseni kannataks.
Juhan Kissel leppis koggokonna kohto een Juhan Krünbergiga nenda et Juhan Krünberg maksab se nimmetud wõlg 25 Rublad tullewa süggise Juhan Kisselile ärra agga kui neist peaks kumpki enne soldatiks ärra antud sama siis peab Juhan Krünberg õkwa se rahha 25 Rublad Juhan Kisselile ärra maksma.
Kohto lauan olliwa:
Peakohtomees Juhan Mina XXX
Kohtomees   Karel Põdderson  XXX
Kohtomees   Juhan Raswa  XXX
