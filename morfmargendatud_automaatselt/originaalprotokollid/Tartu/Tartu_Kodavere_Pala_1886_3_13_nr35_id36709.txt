Tuli ette Pääkohtumees Josep Hawakiwi ja kaebas et Jürri Tõltsep teda, 30 Januaril s.a. kui Hans Kirschi konkursi asi  ees oli, mitte lauas kohtumeheks ei ole wastu wõtta tahtnud nõudis et kohus seda põhjust nõuab mis pääle Jürri Tõltsep seda  ärateinud on.
Ette kutsutud Jürri Tõltsep ja wastas küsimise pääle et tema küll soowinud et Josep Hawakiwi mitte temale kohtumeheks ei ole ja nimelt selle pärast et  Josep Hawakiwi teda, Tõltsepad wargaks teha tahtnud, nii kui tema kaebtus 14 Märtsist m. a. N: 35 wäljanäitab, ja palub ka edespidi Hawakiwid; kui tema, Jürri Tõltsepa asjad kohtu ees on, mitte lauas istuda.
Otsus: Seda protokolli Tartu I kihelkonna kohtu ühes Jürri Tõltsepa kaebusega  minewast aastast wastu Haawakiwi ülekuulamiseks ja otsusetegemisest, selle üle kas Jürri Tõltseppal põhjust on, Josep Hawakiwi Kohtumõistjaks ärapõlata saata.
See otsus sai kuulutud.
                                Kohtumees J. Stamm.
                                Kohtumees M. Piiri
                                Kohtumees J. Tagoma
                                Kirjutaja allkiri.  
