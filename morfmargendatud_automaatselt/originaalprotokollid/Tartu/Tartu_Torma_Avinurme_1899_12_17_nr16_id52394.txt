Protokoll
Awwinorme Wallakohtus sel. 17. Detsembril 1899. 
juures olid
Eesistuja Mihkel Pihlak
kohtumees Andres Kallaus
kohtumees Josep Ambos
Tuli ette Awwinorme Walla ja Adrako küla taluperemees Josep Sobber Madise poeg ja palus Wallakohut et tema oma testamenti kedda tema varapärimise pärast sel 20 Oktobril 1895 siin Wallakohtu juures Akti ramatuse on üles kerjutada lasknud muuta tahab. ja andis üks kirjalik testament Wallakohtu kätte kedda tema palub Wallakohtu Aktiramatuse üles kirjutada ja kinnitada ja on tema kirjalik testament nenda viisi sana sanalt; Mina allnimetud Josep Madise poeg Sõbber awaldan selle läbi oma järgmist wiimast tahtmist. Kõik testamentid ja wiimsed tahtmised mis mull enne tänase tehtud on niikaugel kui nemad minult ostetud Adraku N 265 talukoha, mis Awwinorme mõisa all Jürjewi kreisis on. wai tüttre Mari Tomikule lubatud jaose puutunud, muudan mina tänasel pääval ära sest et se talukoht minult oma armsa tüttre Kai Lillele sündinud Sõbrale on kingitud mis üle kreposti kohus on.  
Selle kontrakti järele peab Kai Lille sõsaralle Marri Tomikule nelisadda wiiskümmend rubla wäljamaksma, Selle testamentiga palun mina keiki sugulaisi rahul olla, Mari Tomik pärib peale minu surma pool minu liikuvast warantusest. Josep Sõbber (allkiri)
Kai Lille ei moista kirjutada tema palve peale kirjutas Jakob Tam (allkiri)
Jurjewo Werro Kreposti jauskonnas wenekeele peale ümber pantud
(...)
 1899 Aastal Detsembrikuu 17 pääwal on se eesseisav pärantuselepping kirjalikkult Awwinorme Wallakohtule I Jurjevi Ulema talurahvakohtu ringkonnas Awwinorme Wallamajas kohtu aktiramatuse üleskerjutamiseks ja kinnitamiseks ette pantud Awwinorme Walla ja Adrako küla taluperemehe Josep Sõbber Maddise poja ja tema lihase tüttre Kai Lille sündinud Sobberi poolest Awwinorme wallas ja Adrako külas olewa N 265 talu parimise pärast ja on need pärantuse leppingu tegijad Wallakohtule tuntud ja teatud ning nende lepping tänasel pääval kohtu akti ramatuse N 16 all üleskirjutud ja kinnitud sanud ja on Josep Sobber oma nime oma käega allakirjutanud, Kai Lille eest agga kes ei moista kirjutada on tema eest ja tema palve peale Awwinorme inimene Jakob Tamm Maddise poeg allakirjutanud.
M. Pihlak. (allkiri)
A. Kalaus. (allkiri)
J. Ambus. (allkiri)
Kirjutaja Schulbach (allkiri)
