Protokolli päält sest 20 Oktobri k.p. No 213 tul ette Johan Märten nink üttel et temma suggulane Johan Papp ei ollewat mit üks palk enda poolest tolle kambre seen pandnut, üllesraiumisse eest on temma Johan Pappil 1 wak rükki ärra massnut.
Koggokonna wannemb üttlep et temma keelnut Johan Pappi ja üttelnut, et temma Johan Papp ei tohhi enne koggokonna kohtu selletamist mitte tarre ärra weddata.
Moistus: Tarre om Johan Papp ommetige keelu wasto ärra wedanut, temma saab tolle eest 20 Witsa löökiga trahwitus. Päle tolle Johan Papp piab Johan Pedu tarre jälle nendawiisi taggasi weddama ja ülles pandma naggo ta ennegi olnut kiik enda kulluga, ja kui wast munni palk saab puudus ollema, ehk kaotsel läinut siis piap Johan Papp kiik enda kulluga ostma, nink tarre sial samma paika pääle, kos ta esti ki saisnud ülles pannema. Kui Johan Pappil keddagi noudmist om Johan Märteni wasto, siis woip Johan Papp selle Johan Märteni ülle koggokonna kohto man kaibust tösta.
Pääkohtomees: Johan Hansen XXX
Kohtomees Ado Ottas XXX
Kohtomees: Jakob Jansen XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kiri wälja antdu.
