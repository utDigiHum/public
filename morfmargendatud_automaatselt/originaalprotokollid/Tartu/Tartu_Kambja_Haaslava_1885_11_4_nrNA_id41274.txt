Haslawa Kogukonna Kohus sel 4 Nowembril 1885
Man ollid: päkohtumees Jaan Link
Kohtumees: Johan Klaosen
"  Johan Grossberg
Kaibas Lokko tallo ostja Jaan Grossberg, et Jaan Tenn temma mõtsast 15 kuuse puud 3 ja 4 tolli jämeda ja 8-9 jalga pikka ja üts koorma kadajat warrastanu om. Kaebaja nõudis 15 Rubl. Uelle om kaenu Kohtumees Johan Grossberg.
Jaan Tenn wastutas selle päle, et tema neid puid warrastanu ei olle, ja tema käest ei olle midagi kätte sadu tema keldre pält, mis tema kodust kaugel om, ollewat kaibaja mõnne kuuse leudnu.
Kohtumees Johan Grauberg andis ülles, et Jaan Tenni keldre pält om 3 Kuuse puud ega üts 3 tolli jämme 7 ehk 8 jalga pikku leudnu, need kuuset om kännu päle sündinu ja kadajat om üts koorma arwata seal keldre peal ollnu. Liis Tenn om sel kõrral üttelnu, et se Jaan Tenni kelder ja ka haot om.
Mõistetu: et Jaan Tenn neide temma keldre pält leutu 3 kuuse est säduselik kolmekirdne trahw 1 Rubl 8 kop 8 päiwa sehen Jaan Grossbergile ärra peab massma, selle et tema need puud warrastanu arwata om.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees  Johan Klaosen XXX
 Peter Iwan /allkiri/
 J. Krosbärk /allkiri/
