Tuli ette Mõisa walitseja Christiani ja kaibas temal on siit walla mehe Kristjan Kütti wiinaköögi kaup kuu peale olnud ja se on tema jurde sel wiimasel päewal kuu lõppetusel tulnud ja keik kuu raha  wälja palunud  se mõdo peal et tema edasi tenib on õhta kel 8 üttelnud, et tema ära  lähheb ja on ka ära läinud ja tema  wina kögi töö maha jätnud mis läbbi mõis on kahju saanud ning nõuab oma  kahjo tassumise 10 Rubla ühhe kuu palk. Kristjan Kütt wastas tema on se pärast ärra läinud et raske töö ja palka olla tema se pärast wälja wõtnud, et muido kui tema oleks ilma ülles ütlematta ära
läinud  olleski mõis tema palka kinni piddanud.
                      Kohhus Mõistis:
Et Kristjan Kütt  säduse wastu ja ülle kelu on tenistusest ära tulnud peab tema et ta mitte ½ kuud aega enne  polle ülles üttelnud Mõisale 8 päewa sees 5 Rubl. kahju tasumiseks wälja maksma.
See Otsus sai kohtu käiattele appellationi sädusega ette loetud misga keik rahhul ollid.
                    Pä kohtumees Kustaw Kokka +++
                    Kohtumees      Kustaw Nukka +++
                    teine                 Karel Waddy   +++
                    Kog. kirj. Fucks.
