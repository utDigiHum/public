Sel 4 Mär c sub N 36 pooleni jäänud asjus andis Toomas Linde naine Maret Linde üles, et Toomas Lind haige on ja tema seda asja oma eesseisja Peter Kogeri kõrwal tahab õientada.
Tunnistus sai awaltud kohtu käijattele.
Otsus: Et Toomas Lindel mingisugust tunnistust ei ole et Mihkel Lallo koer on nende sia ära kisnud ja selle pääle siga ära on lõpnud siis saab tema kaibdus tühjaks mõistetud.
See kohtu otsus sai Mihkel Lallole ja Maret  Lindel Peter Kogeril ette loetud, palub aga et kui Mihkel Lallo tüttar lugenud saab et siis saab ette wõetud.
                                                    Peakohtum Hindrek Horn
                                                    Kohtum       Otto Mõisa
                                                    Kohtum       Karel Olla.
