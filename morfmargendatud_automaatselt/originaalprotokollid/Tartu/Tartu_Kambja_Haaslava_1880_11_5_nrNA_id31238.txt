Haslawa kogokonna kohus 5 Nowembril 1880
Juures istus:
Pääkohtumees  Mihkel Klaos
kohtumees Johan Udel
dito  Rein Anko
Tuli ette  Jaan Põdder oma wenna Dawit Põddraga ja kaebas:  Mihkel Kokka olewat suure tee pääl Roijo kõrtsi ees tema hobuse suu weerest kinni wõtnud ja ei ole temal teed mööda käija lasknud ja kui tema on  Mihkel Kokka käest küsinud: kellega sul tegemist! olewat Kokka temale wastanud: "Neide kattega!" sellepääle on Kokka temale päha löönud ja tema wenna Dawit Põddra pääst kübara ära wõtnud, mis 1 rubl. 50 kop. wäärt.
Nüüd palub tema, et  Mihkel Kokka trahwitud saaks.
 Mihkel Kokka ei salga, et tema Roijo kõrtsi juures olnud, aga  Jaan Põddra ei ole tema mitte löönud, ei ka  Dawit Põddra pääst kübarad wõtnud.
Tunnistaja Jaan Link ütles wälja: tema on näinud kui Krimanni mehed selkorral Roijo kõrtsi juures omawad kiwa korjanuwad ja tahtnuwad nendele, kes  Jaan Põddraga  ühes on olnud, wissata ja siis on ka üks nendest meestest  Dawit Põddra pääst kübara ära wõtnud.
 Jaan Jantzen tunnistas nõndasama.
Tunnistaja Jüri Opmann ütles wälja: tema on näinud, kui üks nendest meestest Jaan Põddra hobusel päitsed pääst ära tõmbanud ja pärast Dawit Põddra pääst kübara ära wõtnud, aga siis ei ole tema seda meest nimepidi mitte tundnud, nüüd tunneb tema kül, et see Mihkel Kokka on olnud; löömist ei ole tema mitte näinud. 
Tunnistaja Jüri Jantzen ütles wälja: nemad on kõik Roijo kõrtsist wälja tulnuwad ja kui Jaan Põdder oma hobuse man tee pääl on olnud siis on tema enne Mihkel Kokkad löönud, mispääle Kokka teda ka tagasi löönud ja siis Dawit Põddra pääst kübara ära wõtnud. 
Tunnistaja Johan Woltri ütles wälja: neid on sääl palju mehi sel korral olnud ja kiwa korjanuwad ja on Mihkel Kokka sääl Jaan Põddra wankre ümber käinud, aga kübara wõtmist ning löömist ei ole tema mitte näinud.
Johan Kitsing tunnistas nõnda sama kui Johan Woltri.
Mõistus:
Kohto käijad peawad rahule jääma ja ei ole neil midagi nõudmist tõine tõise wastu
Mihkel Klaos
Johan Udel
 Rein Anko
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ning õppus antud mis tähele tuleb panna kui keegi kõrgemat kohut tahab käija ning ei olnud Jaan ja Dawit Põdder mitte  sellega rahul.
