Protokolli peale 11 Webruarist s.a. N: 24 tuli ette Palla mõisa kirjutaja  Julius Waldmann ja andis üles et kubjas Karl Märtin mitte tänaseks pääwaks ettetarwitud ei ole sest et ühe wäga tarwilise asja pärast mõisa walitsus teda kolme päwa peale kodust ärasaatnud on.
 Ettekutsutud Jakob Puusep Ranna mõisa moonamees ja wastas küsimise peale et 6tal Detsembril m. a. olla tema kartohwlidega Palla wiinakoja juures olnud kui Palla mõisa kubjas Karl Märtin, Madis Abramsoni hobuse pealt ohjad ärawõtnud ja ütelnud et need ohjad mõisa omad olnud, selle peale wõtnud Karl Märtin ühe kepi hao kubu seest umbes kolm jalga pitk ja löönud sellega kaks korda Madis Abramsonile õla peale, mispeale mõlemad  riidlejad koost äraläinud. Madis Abramson ei ole Karl Märtinid mitte puutunud.
 Josep Willa Ranna mõisa moonamees andis niisama protokolli kui Jakob Puusep ilma et midagi juurde lisada oli.
  Madis Abramson andis üles et tema ennast mitte tohtri poolt ülewaadata lasknud ei ole mis järele kohus nenda hoopide raskusest arusaada oleks wõinud.
 Madis Abramsonil saiwad tunnistused awaltud.
  Otsus: Karl Märtin tunnistuste awalduseks ettetarwitada.
                     Peakohtumees J. Sarwik 
                      Kohtumees      J. Stamm
                      Kohtumees      M. Piiri
                      Kirjutaja  Carl Palm.(allkiri)
