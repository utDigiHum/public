Haslawa kogokonna kohus 2 Decembr 1881.
Juures istus: Pääkohtumees  Mihkel Klaos
kohtumees Johan Udel
"   Johan Link
"  Rein Anko
Tänasel pääwal sai Haslawa  mõisa walitsuse poolt kaks kirja Peterburgi pääülewalitsuse poolt kogokonna kohtu ees walla walitsuse kätte antud, üks 6 Nowbr. s.a. № 550, kelle sees kuue Mõra küla ostetud maapidajatele nende palwe peale 28 Aug.c. kuulutud saab et nemad Hainsoo karjamaad aga maksu eest mis ülewalitseja härra Alabuschew määrab ja kelle üle kui kontraht tehtud saab oma pruukimiseks saada wõiwad - ja tõine kiri 6 Nowbr. s.a. № 551 kelle sees Haslawa walla walitsusele kuulutud saab, et nende palwe 28 Aug. d. Sillaotsa ja Zirgasse talu pärisrentimise pärast Haslawa wallale mitte kuulda ei ole wõetud.
Otsus:
Seda protokolli üles wõtta ja need nimetud 2 kirja 6 Nowbr. s.a. № 550 ja 551 mõisa walitsusele nende nõudmise pääle kuue nädala pärast tagasi anda.
Peakohtumees Mihkel Klaos  XXX
Kohtumees Joh. Udel XXX
"   Joh. Link  XXX
"  Rein Anko XXX
