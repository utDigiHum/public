Protokoll № 17.
1900 goda Avgusta 4 dnja v Gaslavskom volostnom sude.
V sostave predsedatelja Jugana Koort
Tšlenov: Petra Reino i Jana Meos
i pri pissare Jan Veljaots.
Javilis nasledniki umeršago Karla Pederson, imenno Anna Juganova Vilup s mužem Janom Vilup, a za nesoveršennoletnego Leongarda Jugannesa Pederson, a ravno i po doverennosti Annõ Marii Lell i jeja muža Gansa Gustavova Lell - opekun pervago i poverennõi poslednih - Jugan Jakov Link, i Oskar Eduard Pederson i prossili zapissat v aktovuju knigu sledujuštšeje:
V dopolnenije k § 4 i 6 zakljutšjonnago 10 Janvarja s.g. za № 1 v Gaslavskom volostnom sude razdelnago ussadbõ Naritse № 10, Kolga № 16 i Sapi № 15 imenija Brinkengof oni zajavljajut, tšto pri zakljutšenii võšeoznatšennago dogovora, oni uslovilis v tom, tšto Anna Vilup, za prinjatoje jeju na sebja objazatelstva võplatõ Leongardu Pederson 700 rub. i Anne Lell 231 rub. otvetstvujet ona liš odna priobretjonnoju jeju ussadboju Naritse i Oskar Pederson za 4300 rub. sledujemõje Leongardu Pederson - svoimi ussadbami Kolga i Sapi, i tšto oni ne dolžnõ bõli otvetstvovat solidarno za eti objazatelstva, tak tšto v slutšaje ispolnenija odnim iz priobretatelei prinjatõh na sebja objazatelsv, vnessennaja na jego nedvižimost otmetka možet bõtt pogašena.
Johan Jago poig Link /allkiri/
Anna Juganova Vilup, a za negramotnostju i po litšnoi o tom prosbe jeja i litsno za sebja v katšestve muža i sovetnika rospissalsja J. Wilup /allkiri/
Oskar Eduard Karlov Pederson /allkiri/
Podlinnost predstojastših podpissei Jugana Link, Annõ i Jana Vilupov i Oskara Pederson, a ravno i tšto Link imejet pravo na soveršenije predstojaštšago zajavlenija, kak poverennõi Annõ i Gansa Lell tak i v katšestve opekuna Leongarda Pederson, Gaslavskim volostnõm sudom udostoverjajetsja.
Predsedatel suda: J. Koort /allkiri/
Tšlen suda: P. Reino /allkiri/
"  J. Meos /allkiri/
Pissar Ja. Veljaots /allkiri/
Spravka: kopija nastojaštšago zajavlenija võdana Janu Vilup 4 Avg 1900 za № 271.
Pissar Ja. Veljaots /allkiri/
