Sel 10 Webruaril 1886
Kaibas Ado Kogolt, kes Tarto Linan Riia ulitzen № 5 ellamas, et temal 725 Rub ja selle raha intressi 4 Webruarist s.a sadik 10 prozenti asta pält  Jürri Klaose käest sada om, selle wasto om temal 7 Torti Tallo päle om kinnitedo Obligatsioni ja kulutab nüid need Obligatsionit tänna  Jürri Klaosele ülles et poole asta perrast seda raha kätte sada tahab.
Jürri Klaos  ütles selle wasto, et need Obligatsioni Tarto Linan Herra Albaumi käen om ollnu, neide Obligatsionide päle ollewat tema 450 Rubl Albaumile ärra massnu ja ei wõtta kulutust selleperrast mitte wastu.
Mõistetu: et Jürri Klaos kuni 10 Augustis se 725 Rubl ja selle est 5 prozenti intr. 4 Webruarist s.a sadik Ado Kogoltile ärra peab massma ja kui sis massetu ei olle saab se Torti tallo selle est Oksioni alla pantu. Jürri Klaose wasto nõue et 480 Rubl ärra massnu om, ei sa õiges wõetu, selle et neide Obligatsioni peal midagi sest ei seisa ja kui ka massnu peas ollema, wõib tema selle käest nõuda, kellele tema massnu om.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees: Mihkli Iwan /allkiri/
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
Kirjutaja: J. Weber /allkiri/
