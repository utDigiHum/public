1900 aastal Webruari kuu 17 päeval sai järgmine suusõnaline lepping aktiraamatusse sisse kirjutatud. Hendrik Petrepg Kilk annab oma lehma mis väärt 15. Rbl. tütre Marile kingituseks selle tingimisega, et tütar Mari nimetataud lehma jätab isa poole wanemate toitmiseks ja seda säält kuhugi ära ei wõi wiia.
Hendrik Kilk, aga et esi kirjutada ei oska, siis kirjutas tema palvel alla: Peeter Willak
Et Peter Willak Hendrik Kilgi eest on alla kirjutanud, kes kirjutada ei oska ja et wallakohtule kinkimise lubaja Hendrik Kilk isiklikult tuntud on, jääb tõeks tunnistatuks.
