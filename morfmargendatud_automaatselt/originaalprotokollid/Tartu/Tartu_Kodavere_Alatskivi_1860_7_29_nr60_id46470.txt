Walla Magasi wöörmünder Jüri Bringwelt kaibab: "Karl Mesipuu olewa teda hulga rahwa kuuldes mitmat wiisi sõimanud, isiäranis nimitanud teda wargaks, kes walla rahaga rikkaks saanud; tema niisuguse teotamise ja aurikkumisega ei wõiwa rahul olla ja paluwa Kogokonna kohut, sõimajat trahwitada."
Tunnistawad seda kaibust sõnast sõnna tõeks: Kusta Aru ja Jooseb Timusk.
Et Karl Mesipuu 5 kõrd on Kogokonna kohtu ette kutsutud, kül kohtumehe, kül kohtuteendre läbi; tema ga mitte pole sõna kuulnud, saab selle wastopanemise eest Keisrilik Kihelkonna kohus palutud, seda asja oma kuulamise alla wõtta.
