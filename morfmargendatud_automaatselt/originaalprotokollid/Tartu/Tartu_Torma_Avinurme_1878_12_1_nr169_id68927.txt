Awwinorme Metskoggokonnakohtus sel 1 Dcmbril 1878
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomees Mihkel Sild
Selle walla innimenne Jürri Lambasaar kaebas et Soldat Wassili Kiwwimeister on temma hobbose Rangid Schleid ja Waljad ärra warrastanod ning need Metsa ärra peitnod ja neid siis wälja tunnistanod kui kohtomees tedda kinni sidduda tahtnud, ning pallos kohhut selle ülle moistost tehha.
Wassili Kiwwimeister tunnistas et se õige et temma on küll need hobbose ristad ärra wõtnod ja ärra peitnod, sest et temma Jürri Lambasaare on nattokenne kiusata tahtnod, ja pallob temma sedda asja andest anda.
Moistos
Et Wassili Kiwwimeister allati warrastab ning jubba mitto korda temma peale kaeptosed olnod. siis moistab koggokonnakohhos et temma saab selle Warguse eest 2 öe ja pääwa peale trahwist kinni pantod ja et kui temma peale selle weel warrastab siis saab temma raudos Tartu Maakohto kätte ärra sadetud suurema kohto trahwi alla.
Josep Laurisson XXX
Andres Kiik XXX
Mihkel Sild XXX
