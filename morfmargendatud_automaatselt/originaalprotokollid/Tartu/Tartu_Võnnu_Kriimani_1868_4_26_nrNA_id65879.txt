Krimanni koggokonna kohtus 26mal April 1868.
 Juhan Kink tulli 26mal Aprilil koggokonna kohto ette ja kaebas et  Jürri Klaos on mind 12mal Aprilil koggokonna kohto een sõimanud et minna ollen temma jahho warrastanud kui minnew aasta Temma man poisist ollin ja jahho linnas müimas käisin.
 Jürri Klaos  tulli ette ja ütles ma ollin kül nenda üttelnud et sa warras olled sest üks kord saatsin ma linna Pekker  Wittele 4 Puda nisso püidli jahho ja sinna olled 5 Puda eest Wittele  ärra müinud ja ka 5 Puda eest rahha wõtnud. 
Juhan Kink tulli ette ja ütles: Jürri Klaos on igga kord mulle koddust jahhud kätte kalunud ja ollen ka temmale jahhode eest täielik rahha maksnud, mis ta mulle kätte on kalunud ja mis ma jahhutest ollen sanud ja se on tõssi kül et Temma mind käskis 4 Puda jahho Wittele wia agga mul olli turro müimisest jahho ülle jänud ja pannin neist jahhodest 1 Pud manno ja wisin siis Wittele 5 Puda ja wõtsin ka Witte käest 5 Puda rahha sest egga ma omma käest wõinud temmale 1 Pud jahho kinkida ja se olli üks keik kellele ma jahhod müisin ja sulle rahha koddo tõin ja kewwade kui sinno mant ärra tullin siis teggid mulle selge rehnung ja minul jäi weel sulle nende jahhode eest mis linna wõlgo andsin 60 Rubla maksta ja kuis minna siis pean warga nimme kandma. kui sinno rehnungi perra keik täieste ollen ärra maksnud.
Kohto tarrest wälja minnes ütles Juhan Kink sa ise olledki warras ja tahhad weel mind ilma aigo wargaks tetta se pääle sai Juhan Kink ette kutsutud ja küssitud mis Jürri Klaos on warrastanud. Siis ütles Juhan Kink Temma wiis omma poistega üks kord õllepruli Abelile keswi ja temma kottid olliwad õlle pruli läks Lõunat söma ja wõtti ja Klaose kotti pealt pud
Mõistus:
Krimani Koggokonna kohtus sel 3 Maij 1868.
Kohhus mõistis ni kui se maa pool wiljan ja pool söötis olnu nink  Kotta 6 aijastad sedda maad pruginud mes Kaarti perra 2¼ wakka alla om, peab  Kotta Päeiw egga wakka maa eest aijasa pealt 2 rubla maksma ja 2¼ wakkama teeb 4 Rubla 50 Copka aijasta mes summa 27 rubla wälja teeb, om Kotta Päewal Jaan Wisserile ärra maksa.
Man olliwa:
Peamees Jüri Pragli XXX
Abbi "  Ado Lenzius   XXX
Abbi "  Jaan Poeddersohn XXX
 Kotta Päiw ei ollud selle mõistmissega rahho nink nõudis suuremad kohhud.
