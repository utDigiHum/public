Tänawu 1859 sai Lahe küla Mustsaari talu /N 72/ Peeter Tõrowere kätte antud, kes oma wenna Adami selle maja selle maja pere meheks nimetas.
Maja seisus oli: Elomaja ilma haganikuta ja ilma rehe aluse laduta, ait wana ja halwa katuksega, laut uus ja ilma katukseta, koda ei ole, saun ilma katukseta, aiad hoopis otsas ja lagedad.
Peeter Tõrowere maksis selle eest, et ta selle maja sai, wana peremehe Mustsaari magasi wõlg: 8 1/2 wakka ruki, 5 1/2 wakka odre ja 14 wakka kaeru.
