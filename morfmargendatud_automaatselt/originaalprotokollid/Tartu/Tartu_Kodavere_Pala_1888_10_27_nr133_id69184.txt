Tuli ette  Werbo N:1 talu peremees Märt Piiri ja kaebas, et Willem Kangro Pille Edro N:2 talu peremees ühte karja teed mis kardi järele nende kahe talu wahel seisma peab üleskündnud on misläbi kari ei ka hobused seda
teed mööda enam käia ei wõi. Andis üks tunnistus herra von Stryki käest 11 Aug. s.a. N. 50 ette kelle järele see karja tee 21 jalga lai ja 480 jalga pitk olema peab. Seda teed oli wallawalitsus 12 Augustil selle tunnistuse põhjusel üleswõetud ja märk sisse  löönud. Nüüd aga olla Willem Kangro need märgid mahakiskunud ja see tee uuest üleskündnud, palus kohut asja ülekuulata ja süüdlast sääduse järele trahwida.
      Ettekutsutud wallawöörmünder Kustaw Nukka  ja andis küsimise peale üles et tema olla 12 Augustil seda karjateed ülesmõetnud ja märk tikud sisselöönud. See tee olnud täiesti nagu wäli pruugitud. Tema käsust wagu sisseajama tulla, ei ole Willem Kangro tähele pannud ning teise käsu  peale wastanud et tema ei taha tulla. Willem Kangro ettekutsutud wastas küsimise peale et tema sellepärast seda teed üleskündnud et seda teed mööda keegi muud ei käi kui tema ise.
 See tee olla praegu 14 jalga lai ja wõib seal peal küll läbi saada, tahab aga Märt Piiri see tee nii laiaks saada siis nõuab tema et Piiri omalt poolt ka see tee edasi nii laiaks kuni Kokkora rajani teeb. Wallawöörmünder tunnistas seda õigeks et see tee küll 14 jalga lai on.
  Otsus. Palla mõisaherra poolt kaard paluda et õiget seisust järelewaadata.
                                        Peakohtumees   J. Sarwik.
                                        Kohtumees         J. Stamm.
                                        abi Kohtumees  W. Oksa
                                        Kirjutaja C Palm. (allkiri)
 
