Protokolli peale 5 Webruarist s.a. N: 29 saiwad ettekutsutud Märt Piiri ja Jürri Tralla ning  nendele Mart Terase tunnistus awaltud. Märt Piiri wabandas ennast mitte seda selle mehele rääkinud ei ole.
 Muud tunnistust ei olnud mõlematel pooltel enam ülesanda.
 Otsus et selle asja juures üks ainukene tunnistaja, ja selle järele wähe tunnistust on, jääb Märt Piiri selle laimamise süült wastu Jürri Tralla sellega õige rahul ning palus järele mõtlemise aega.
                           Peakohtumees J. Sarwik        
                             Kohtumees    J Stamm
                             Kohtumees    M. Piiri
