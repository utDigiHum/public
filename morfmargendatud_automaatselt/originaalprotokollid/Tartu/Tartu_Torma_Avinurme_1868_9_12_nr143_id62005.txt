Awwinorme Koggo konna kohtus sel. 12mal Septembr 1868
Kohto juures olliwad
pea kohtomees Mart Jaggo.
teine kohtomees Tomas Pärn
kolmas kohtomees Jaan Saar
Kaebas Mihkel Rosenthal, et temma lähnud omma wäljale ja nähnud et Karel Ambos omma põllo pealt kiwwisid temma põllo peale lopinud ja kutsunud tedda omma juure, temma lähnud siis Karel Ambose juure ja üttelnud, nabrimees egga sinna üksi ei joua neid kiwwisid omma põllo pealt ärra lopita, minne koddo ja to omma perre appi, selle peale on agga Karel Ambos temmale möda kõrwo annud, nenda et temma werrine mahha kukkunod.
 Kutsuti ette Karel Ambos, kes räkis et Mihkel Rosenthal olnud lopinud keik kiwwid omma põllo pealt temma põllo peale, mis temma agga taggasi lopinud, tulnud siis Mihkel Rosenthal ja lönud temmale möda pead. temma lönud agga kohhe taggasi kaks korda õige mõnnusast.
Moistus
Koggukonna kohhus moistab, et Mihkel Rosenthali kiwwi lopimist ja löömist ei olle keski näinud Karel Ambos agga issi tunnistab et temma need mollemad tükkid täielikkult ärra tehnud, et Karel Ambos 4 Robla ja Mihkel Rosenthal 1 Robla tapluse eest Waestelaeka trahwi maksawad.
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Tomas Pärn XXX
Jaan Saar. XXX
