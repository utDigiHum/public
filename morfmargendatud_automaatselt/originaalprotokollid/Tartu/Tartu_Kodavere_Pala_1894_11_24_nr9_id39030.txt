Kohtu ette tuliwad Otto Leonhard Reinmann Jüri pg. oma poja Kaarliga, ning palusiwad et ende wahel maha tehtud leping saaks kohtu poolt kinnitatud: Otto Reinmann annab oma õigused talu wäliste asjade tallituses oma poja Kaarel Reinmanni hooleks, nõnda et Kaarlil woli on kõigis asjas mis wana isa Otto Reinmanni maja pidamisesse puudub tema asemel wälja astuda, talu sisse tulekute ning wäljaminekute asjad õiendada, Kohtus igal korral ning kõigis kohtades isa asemel olla koha kinnitamise asju toimetada, wõlgasi tasuda ja ülepea nagu peremees toimetada.
Peale selle tõutab Otto Reinman talu oma poja Kaarlile anda pärast oma surma, aga liikuw warandus läheb õe Miina ja Kaarli wahel jagamise alla ühesuguses osas.
                                Otto Reinman ei oska kirja, tema palwe peale.
                                Kirjutas alla: Jaan Piibar
                                                       Karel Reinman
  Kohtumäärus: Otto Reinmanni ja poja Kaarli wahel tehtud lepingut Walla Kohtu Seaduse II jau § 278 põhjusel ustawaks tunnistada.
                            Kohtu eesistuja: Abram Saar
                            Kohtumõistjad Peeter Holst
                                                       Gustav Uuk
                                       Kirjutaja KSepp.            
