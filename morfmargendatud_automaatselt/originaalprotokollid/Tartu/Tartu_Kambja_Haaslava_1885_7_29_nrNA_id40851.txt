Sel 29 Julil 1885
Tulliwa ette Johan Widik, Johan Udel, Johan Berkman ja Peter Märtin ja pallusiwa kinituses protokolli panda, et nemma nida lepinu om, et selle Johan Widike wilja sömisse kahjutasumisse Johan Udel 1 päiw, Johan Berkman 2 päiwa ja Peter Märtin 2 päiwa hobusega Johan Widikele sitta wedada awitawa se om sel tullewal sügissel peawa need päiwat tehtu ollema kui Möldre Johan Widik prugib. Nisammu lubas Jürri Tenn 2 päiwa teha, Jaan Puhm lubas nendasammu kui tõise.
Otsus: seda kinnituses protokolli ülles panda.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
"  Peter Iwan /allkiri/
