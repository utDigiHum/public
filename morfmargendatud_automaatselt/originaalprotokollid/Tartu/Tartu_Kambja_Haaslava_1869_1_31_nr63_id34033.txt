Mai Turba kaibap Jaan Müüri eestkostmissen, et se tütruk Lies Niklus riisunut temma rõiwat ärra.
Lies Niklus wastutap Jürri Labberi eestkostmisen, et se Mai Turba warrastanut temma kaest ütte rätti, selle temma wotnut Mai Turba kaest rõiwat ärra.
Moistus: Lies Niklus ei woi selgest tehha, et se Maij Turba rätti temma kaest warastanut, enge piap warsti Maij Turbale röiwat kätte andma.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
