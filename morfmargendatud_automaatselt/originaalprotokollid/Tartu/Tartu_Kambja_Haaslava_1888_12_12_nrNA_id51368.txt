Haslawa Kogukonna Kohus sel 12 Detsembril 1888.
Man ollid:
Kohtumees: Jürri Kärn
"  Johan Opman
"  Johan Raswa
Selle otsuse päle sest 28 Nowembrist Kusta Michelsoni kaibuse asjan wasto Johan Zobel olli Kusta Rõuge Surest Kambjast kutsumise päle tullnu ja tunistas, et tema ollnu jures kui Kusta Michelson ja Johan Zobel selle wõlla perrast selletanu, sis pallunu Johan Zobel selle rahaga 48 rubl kannatada ja lubas ärra massa ja Michelson üttelnu kui sinna intressi massat sis minna kannata. Kusta Rauge arwas 5 ehk 6 astat aega ollewat.
Kusta Toom Haslawast tunistas, et 1885 astal tahtnu Kusta Michelson Johan Zobeli päle kaibata sis üttelnu Johan Zobel, et ega minna ütte kõrraga seda 48 rubl ärra massa ei jõua ja pallus kannatada. 
Mõistetu: et Gustaw Michelson nõudminne wasto Johan Zobel tühjas om mõista, selle et kaibajal wõlla tähte ei olle ja se wõlg kui se peas ollema kaibaja essa käest wõetu om ja mitte poja käest.
Se mõistus sai § 773 ja 774 perra mõllematele kulutedo ja õpetus leht wälja antu.
Kohtumees: Jürri Kärn XXX
"  Johan OpmanXXX
"  Johan Raswa XXX
