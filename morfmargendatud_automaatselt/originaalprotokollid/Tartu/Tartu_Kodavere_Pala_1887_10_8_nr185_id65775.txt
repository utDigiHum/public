Protokolli peale 17 Septembrist s.a. N: 178 ei olnud Liisa Tamm ja Jakob Kallaste kohtu ette tulnud ja palus Karl Annuk neid tunnistusi  maha jäta ja asi kriminal kohto kätte anda.
 Otsus seda asja protokolli ärakirjades Tartu Sillakohtu suurema ülekuulamiseks saata.
                                   Peakohtumees J. Sarwik.
                                    Kohtumees      J. Stamm
                                    Kohtumees      M. Piiri.
                                    Kirjutaja G. Palm.
