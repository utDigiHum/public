Sel 10 Nowembril 1886
Kaibas Dawit Kork Krimanist, et kauba perra pidanu perremees Johan Kord temma 1885/86 asta päraha 5 rubl ja 2 rubl 8 kop wanna wõlga massma ent ei olle senni aijani massnu ja pallus et kohus seda massma sunis,
Johan Kort ütles et enegi se 2 rubl 7 kop massu wõlga om tema massa lubanu ja mitte se 5 rubl.
Dawit Kork andis tunistajas ülles Kusta Adamson, Wenne Jaan Klaosen ja Jaan Tani.
Otsus: et tunistaja ette om kutsuta.
Kohtumees:  Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen  /allkiri/
"  Johan Kliim XXX
