Haslawa kogokonna kohus 4 Aprillil 
Juures istus:
peakohtumees M. Klaos
kohtumees Joh. Udel
dito  Rein Anko
Tuli ette  Johan Soo  ja kaebas, et tema kodapoolne   An Toom peab warastama, sellepärast ei wõi tema teda mitte oma maa peal enam pidada ja tahab, et tema oma tare sealt maa pealt ära wiib.
 An Toomi käemees Jaan Toom  ette kutsutud, ütles kaebtuse peale wälja, et  Joh. Soo tüdruk on tema poole keswi toonud, aga tema ei wõi taret mitte ära wija.
Mõistus:
Et see tare juba ammust aeast seal Joh. Soo maa peal on, ei wõi tema mitte maha kistud saada.
Peakohtumees  M. Klaos
kohtumees  Joh. Udel
dito  Rein Anko
Mõistus sai kohtukäijate T.S.R. pärra a. 1860 § 772-774 kuulutud ning õppus antud ning ei olnud  Joh. Soo selle mõistusega mitte rahul.
