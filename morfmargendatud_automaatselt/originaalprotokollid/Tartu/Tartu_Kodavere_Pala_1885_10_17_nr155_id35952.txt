Protokolli pääle 5 Septembrist s. a. N:126 sai ette kutsutud Kustas Rosner Kadrinalt ja wastas küsimise peale et tema kül Jaani õhtul Piiri kõrtsis teiste poistega olnud aga ühtegi tüli näinud ei ole.
Karel Ilwes ei olnud tulnud.
Otsus: Karel Ilwes tulewast kohtu pääwast ette tellida.
                                Pääkohtumees O. Kangro
                                       Kohtumees O. Mõisa
                                       Kohtumees M. Mölder
                                       Kirjutaja allkiri.
