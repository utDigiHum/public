 Krimanni koggokonna kohtus 23 Octobril 1870.
 Jakob Sugasep  tulli  ette ja kaebas et Roijo Tammi teggia  Allexei Spekow ei olle mitte meie leppingo aega täitnud, ni kui meie 11 Septembril siin kohto een Age weski Tammi perrast leppisime, et Temma lubas Age weski Tammi 3 näddali sissen illusti ärra parrandata ja minna lubbasin 2 näddalit enda kulluga temma jures tööd tetta agga Temma ei tulnud mitte sedda Tammi teggema. Nink pallun et minna nüid se rahha mis  Juhhan Klaose käest Tammi teggemise eest weel sada on 94 rublad 84 kop kätte saan.
Juhan Klaos tulli ette ja ütles enne ei wõin minna mitte se rahha wälja maksta kui minno Tamm on walmis teinud ja kõlbolikus korras mulle ärra annud ni kui meie kaup tettu olli siis wõib igga kord omma rahha kätte  sada mis temmal minno käest sada on.
Roijo Tammi tegia  Allexei Spekkow tulli ette ja ütles: minna ei mõista middagi üttelda ja kul ei olle Age Tammi egga ka  Suggasepaga middagi teggemist ja minna sedda Age Tammi mitte parrandama ei hakkan.
Mõistus: 23mal Octobril 1870.
Koggokonna kohhus mõistis et  Alleksei Jawan Spekkow peab 4 näddali perrast se nimmetud rahha 94 Rubla 84 kop. Jakob Suggasepale ärra maksma seperrast et Temma mitte omma leppingud polle täitnud ni kui 11mal Septembril siin koggokonna kohto een on leppinuwad ja Spekkow peab se Age Tamm 4 näddali sissen ärra parrandama siis saab se nimmetud rahha 94 Rubla 84 kop Juhan Klaose käest õkwa kätte ja kui Spekkow 4 näddali sissen polle mitte se Tamm ärra parrandanud egga kõlbolikus korras kätte annud Klaosele siis ei olle Temmal ennam õigust sedda rahha Klaose käest nõuda ja Klaos wõib teisel meistril selle rahha eest Tamm laske ärra parrandata.
Kohto lauan olliwa:
Peakohtomees Jürri Bragli XXX
Kohtomees   Juhan Mina XXX
"   Ado Lentzius XXX
Abbi " " Hindrik Sulbi XXX
" Jaan Põdderson XXX
Kirjutaja assemel: M. Baumann /allkiri/
 Allexei Spekkow ja Suggasep ei olnud koggokonna mõistusega rahhul ning on sel sammal päewal se lubba täht koggokonnast wälja et wõib suremad kohhut nõuda.
Jakob Suggasep on Protocolli koggokonnast wälja wõtnud 26mal Octobril 1870.
Kirjutaja assemel: M. Baumann /allkiri/
