Tuli kohtu ette isiklikult tuntud Pala walla liige Josep Jakobi p. Nõmm ja palub oma wiimast suusõna testamenti ülesse kirjutada, mis järgmine on:
§ 1. Pääle oma surma jätan kõik enese liikuwa ja liikumata waranduse, selle hulgas 1/4 osa Nõmme N 16 talust Halliku endises wallas, oma tütre Miili Wernomasingile, sünd Nõmm.
§ 2. Tütar Miili Wernomasing peab teiste kaaspärijatele wälja maksma:
a) oma sõsara Juuli Mihkelsonile sellkorral, kui talu maa minu ehk minu pärijate kätte jäeb, seitsekümmend wiis rubla; kui maa meile ei jäe, siis ei pruugi maksa. Muu päranduse osa on Juuli Mihkelson kätte saanud.
b) Oma sõsara Alwine Odele, sünd. Nõmm, sellkorral kui Nõmme talu maaosa minu perekonna kätte jäeb, kakssada rubla ja annab üks lehm. Ei jäe see maa mitte meie perekonnale siis ei pruugi raha maksa, aga lehma annab,
c) oma ema Miina Nõmmele kakssada /: 200:/ rubla ja temale enese juures talus prii ülespidamise kuni surmani andma. Joosep Nõmm (allkir)
Pala wallakohus tõendab et isiklikult tuntud Josep  Jakobi p. Nõmm eesolewa testamendi on teinud, see tema etteütlemise järele üles on kirjutatud, temale etteloetud ja Nõmme poolt isiklikult allakirjutatud.
Esimees A Priks Liikmed J Wadi K Elken
Kirjutaja Brükkel.
