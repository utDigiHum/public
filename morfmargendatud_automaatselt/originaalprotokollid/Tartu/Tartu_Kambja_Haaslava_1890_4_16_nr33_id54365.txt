№ 33
Sel 16 Aprilil 1890.
Kaibas  Märt Laber et tema päle wastse  asta 3 wakka keswi Haslawa weski peale om wienu ja need Möldre poisi Gustaw Saare kätte andno, ent nüüd ollewat se kott keswadega ärra kadunu ja nõudis keswade est 6 rubl ja 70 kop selle kotti est. 
 Märt Laber ei mõista kirjutada.
Gustaw Saare wastutas, et tema selle keswa kotti Märt Laber käest wasto om wõtnu, sel kottil ollnu Kriska tallo nimi peal ja Kriska tallo sullase Johan ja Mihkel Tamman om oma wilja jahwatanu ja ollewat sis ka selle Märt Laberi kotti ärra jahwatanu, selleperrast ei olle tema selle jures süidi ja kotti sisse andmise juures ei ollewat Märt Laber mitte ülles andno, et tema kottil Kriska tallo nimi peal olli.
K. Saare /allkiri/
Märt Laber ütles, et tema kottil kül Kriska tallo nimi peal ollnu, sest tema sanu Kriska tallost selle kottiga haganit sis selle jures jänu se kott tema kätte ja temma pandno omma keswa sisse. Kotti sisse andmise jures ei olle kotti märkist kõnnet ollnu.
Mõistetu: et Gustaw Saare selle 3 wakka keswi est 6 rubl ja 50 kop kotti est Märt Laberile 8 päiwa sehen ärra peab massma, selle et tema need Keswat kottiga wastoom wõtnu ja Märt Laber neist ilma om jänu. 
Gustaw Saarel jääb õigus neid keswi ja seda kotti Kriska tallo sullaste Johan ja Mihkel Tammani käest nõuda.
Se mõistus sai kulutedo.
K. Saare /allkiri/   Märt Laber /allkiri/
Päkohtumees Jaan Wirro XXX
Kohtumees: Jakob Saks  /allkiri/
" Peter Hansen XXX
