Kaibas wöörmünder J. Rosenberg et tema olla käsknud kasaka Palla kooli laiali ajamise juurde käskinud 10 meest telli: tulnud aga 5 meest, need 5 ei ole jõudnud maia laiali ajada ja meister on  tähnud ära minna sest et ta ei ole tööle saanud  hakkada, sest et wana olnud laiali ajamatta tema ometi maksnud selle eest meistrile 2 pange õlut see 180 kop ja nõuab nüüd kos kasak Toomas Welt on inimestele käsku annud, ja nõuab nüüd see 180 kop kätte saada.
Toomas Welt ütleb et tema olla käsku annud Annus Wadile ja Jaan Kubjale, Kusta Wadile ka et pidanud tulema aga ei ole tulnud. Jaan Kubja olla aga ära keelnud sinna minna.
Jaan Kubja ütleb et tema ei ole  keelnud teisi ära jäämast, tema issi ole ka käsku saanud.
Otsus: Tulewaks kohtu pääwaks Annus Wadi, Kusta Wadi ette kutsuda ja otsus teha.
                               peakohtum:  H. Horn
                               Kohtum:         Josep Soiewa
                                Kohtumees:  Otto Mõisa    
