Haslawa Kogukonna Kohus sel 27 Webruaril 1889
Man ollid: päkohtumees Jaan Wirro
Kohtumees: Jaan Pruks
"  Jakob Saks
"  Jaan Toom
Kaibas Möldre Johan Widik, et temal Jaan Kroman käest 9 rubl 38 kop jahwatsmise raha sada om ja pallus et kohus seda massma sunnis.
Jaan Kroman wastutas, et se wõlg wõib õige olla tema naene ei olle temale raha jahwatuse tarwis andno.
Mõistetu: et Jaan Kroman se 9 rubl 38 kop 8 päiwa sehen Johan Widikele ärra peab massma, selle et tema kui perremees seda jahwatust om jahwatanu.
Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Toom XXX
"   Jakob Saks /allkiri/
