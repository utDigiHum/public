Harjaklane Feodor Sorokin nõuab Dementi Isakow Borodkina käest wõlg rahha 22 rubla
Dementi Isakow Borodkin kostab kaebduse peale, et temmal olla Feodor Sorokinile 20 rubla maksa, kelle summaga ka kaibaja rahhule jääb.
Otsus.
Dementi Isakow Borodkin peab sest wõllast 10 rubla 15 Märtsiks 1878 ja 10 rubla Suwwiste pühhadeks 1878 kaebajale wäljamaksma.
/: Otsus kulutud sel 17 Webruaril . c. Tallorahwa seaduseramato §§ 772 ja 773 juhhatusele kellega kohtukäijad rahhule:/
