Awwinorme Koggo konna kohtus sel. 6mel Decembr 1868
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Jaan Saar
Kaebas Aidamees Andres Raja et Liiso Paas sedda jutto tehnod, et temma sell ajal kui Hopman koddust ärra olnud, weskile saatnud üks kolme wakkane Rukki kott ja teine kolme wakkane Odra kott, moona wilja, kus temmal agga muido õigus kaks wakka lubba wõtta, ja pallus kohhot sedda järrel kulata.
Kutsuti ette Liiso Paas, kes räkis et temma küll sedda ütlenud, et need wilja kottid peaaegust 3 Wakkased olnud.
Astus ette Juhhan Särg ja tunnistas et temma issi aitanud Aidamehhel nende kottide sisse wilja mõõta ja et nende kummagi kotti sees agga kaks wakka olnud.
Moistus
Liiso Paas saab selle kiusu juttu eest kahhe öe ja päwa peale kinni pantud trahwiks
Mart Jaggo XXX
Mart Liis XXX
Jaan Saar. XXX
