Sel 12 Märtsil 1884.
Kaibas  Johan Klaosen et tema ollon 1880/81 astal Weriko Jüri Luha man sullane ja pidanu Perremees temma Päraha massma, ent nüid nõuetu tema käest seda raha 5 Rubl 20 Cop tema pallus et Jürri Luha seda massma sunnitas.
Jürri Luha wastutas selle päle, et tema selle raha ärra massnu om, ent tähte wastu sanu ei olle.
Päraha Ramaton om se raha wõlgu.
Mõistetu: et Weriko  Jürri Luha se 5 Rubl 20 Cop selle  Johan Klaoseni 1880/81 asta Päraha Wallawanemba kätte ärra massma peab, selle et tema seda raha massma pidas ent tunistust ei olle et seda ärra masson olles.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Kohtumees: Johan Klaosen XXX
 Peter Iwan /allkiri/ 
Johan Krosbärk /allkiri/
