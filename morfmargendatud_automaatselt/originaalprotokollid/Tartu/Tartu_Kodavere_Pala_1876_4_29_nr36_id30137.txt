Saare Perremees Willem Waddy kaebas: Otto Otsa winu minnewa talwel 1 palk temma  õuest ilmlubata ära, mis 5 sülda 2 jalga pikk ja  tüwwikust 17 tolli paks olnu. Kaebaja nõudis nisuggune palk assemele ehk 5 rubla rahha.
Otto Otsa wastas selle peale Kaebaja issa wahetanu temmaga üks pailk ja winu tema se ka Willem Waddy hõue. Se pailk mis temma wastoandnu, olnu  süld lühem ja ka peenem. Pealeselle pidanu 
Ristikheina Heinnega se wahhe õientud sama. Annus Waddy ütles: Nemmad wahetanu nida palkit Otto Otsa pidanu 3 palki selle ühe wastu andma.Tunnistust es olle kummagil.
Kohus mõistis:
Otto Otsa peab Willem Waddile et selle pailk pikkem ja jämmedam olnu 1 rubla 25 kop. juurde maksma selle ühe pailkile, mis juba sinna winu, ja wõib  Otsa muud nõudmiset, nimelt ristikheinu perast ommas paigas nõuda. Se otsus sai kohtokäijatele seaduse wiisil etteloetu, mis järrel mõllemat rahul ollit.
                                        Peakohtomees P.Willemson
                                           Kohtumees: J.Hawakiwi
                                     Abi Kohtumees: Th. Welt.
                                                                 Kirj. allkiri.             
