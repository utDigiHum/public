Warwara Tschernowa ja Irinja Mironowna Persitzki kaebavad omma eestkostja Iwan Persitzki peale, se olla neid peksnud, ja löönud essimessele 4. korda russikaga ja pea palgi peale kelle eest temma 200. rubla nõuab, ja teist 3. korda russikaga, kelle eest temma 200. rubla nõuab.
Dmitri Lodkin kostab, et temma aeanud Warwara Tschernowa omma waabrikust wälja, ja polle löönud, niisamma polle ka teist kaebajad löönud egga peksnud
Iwan Iwanow Saposchnikow tunnistab, et temma nähnud, kudda Dmitri Lodkin tee peale karranud ja Warwara Tschernowada 2. korda russikaga ja 2. korda wasta palki, nii, et maeasse kantud. Irinja Mironownad löönud Dmitri Lodeikin niisamma 2. korda russikaga. Enni sedda sõimelnuwad.
Kinksepp Juhann Peterson tunnistab, et temma nähnud kudda need Warwara Tschernowa ja Irinja Mironowna wabriku ees olnuwad, ja sõimelnuwad wastastikku, siis nähnud, kudda Dmitri Lodkin wälja karranud ja Irinja Mironownad 2. korda löönud ja Warwara Tschernowad 2. korda russikaga ja 2. korda wasta palki, selja piddi ja olnud minnestanud nii, et koddu kantud.
Kohtumees Maddis Erm tunnistab, et tedda kutsutud peksetud Warwara Tschernowad waatama, ta olnud woodis, polle paistetanud.
Tunnistus sai awwaldut
Kohhus moistis:
Tunnistuste wäljaütlemise järrele on Dmitri Lodkin Warwara Tschernowad ja Irinja Mironowad peksnud, siis maksab temma Warwara Tschernowale 20. rubla ja Irinja Mironowale 10. rubla wallorahhaks 8. paewa aea sees ja saab niisugguse teo eest 2x24 tunni peale koggukonna wangihoone kinni pandut.
/: Otsus kuulutud sel 11. Nowembril c Tallorahwa seaduse raamato §§ 772 ja 773 juhhatusel ja oppuse kirjad wäljaantud :/
Dmitri Lodkin kuulutas sel 18. Nowembril, et temma otsusega rahhule ei olle, mis peale temmale kohhe appellatsioni kirri wäljaantud sai.
A. Krimm [allkiri], peaxxx Maddis Erm, kohtumeesA. Pärn [allkiri], "xxx Josep Maddi, "
