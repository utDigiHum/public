Ette kutsutud Kustaw Lass Kokkoralt  seni Pallal teenistuses seisew kes protokolli järele 18 Septembrist s.a. N. 85 ööse teiste poistega tüdrukude juures wõera talus ööse ümberhulkunud,  eestkostja ja
tahtis oma ütlemised minewast korrast ära segada.
 Ettekutsutud Jaan Tuhha Tõntsu talus teenimas ja wastas küsimise peale et tema mitte näiinud ei ole kuna Kustaw Lassi õhtu ehk ööse Tõntsu talusse tulnud, nüüd kui homiku weikse walguse ajal oli näinud kui Kusta Lass äraläinud. Jakob Waddi ja Kustaw Linde oliwad küll sel õhtul ühe asja perast ka seal talus olnud aga warsi peale selle äraläinud.
 Jakob Waddi andis küsimise peale protokolli et tema oma sulase Kustaw Lindega Tõntsu talusse sel õhtul ühte kirja wiima läinud umbes ühe poole tunni perast aga tagasi kodu läinud Kustaw Lassi oli tema tüdruku kõrwal onnis näinud.
 Kustaw Linde tunnistas niisama kui Jakob Waddi.
 Otsus: Et Kustaw Lassi tunnituste nii kui ka ise oma tunnistuse järele ööse wõeras talus asjata ümber hulkunud saab tema selle  kogukonna kohtu poolt 20 witsa löögiga trahwitud.
 Se otsus sai kuulutud ja täidetud.
                Peakohtumees J. Hawakiwi.
                      Kohtumees J. Stam.
                      Kohtumees  J. Tagoma.
                      kirjutaja Carl Palm (allkiri)
