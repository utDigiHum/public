Tulli ette Lena Janask ja kaebas; et Josep Wända on teda pitsawarrega ja saaba kontsaga peksnud nõudis 50 Rbl.
Sai ette kutsudu Josep Wända kes wastas; et tema ei ole peksnud aga ema Ehwersina Wända ja Lena Janask on tappelnuwa.
Tulli ette Jürri Sarwik ja tunnistas et Lena Janask on kül Josep Wändat sõimanud aga peksmist ei ole näinud.
Mõisteti: et tunnistusd ei ole selle peksmise ülle siis jääb Lena Janaski kaebus tühjast wasta Josep Wändat.
Se mõistus sai ette kuulutud Lena Janask ei olnud rahul sai Appelationi.
                                       Peakohtumees: O.Kangro
                                       Kohtumees:       J. Soiewa
                                       Kohtumees:       O. Mõisa
