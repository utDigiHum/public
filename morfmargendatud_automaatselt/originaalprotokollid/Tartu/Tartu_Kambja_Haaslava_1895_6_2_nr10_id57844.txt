№ 10.
2 Jurjewi Ülema Talurahwa Kohtu ringkonna 14 Haslawa Wallakohus 2 Junil 1895 a.
Juures oli Eesistuja Johan Hansen.
 Kawastu walla  liige,  Haslawa walla Logina talu pidaja  Johan Laar ja Wana Kuuste walla liige Jaan Albert tulid ette ja palusid neide lepingut protokolli panda.
 Johan Laar  annab üles, et tema  Jaan Albert wõlgneb kakskümmend rubla kuuskümmend kop. /20 rbl 60 kop/ ja selle wõla kinnituseks palub enese päralt olewad üks lehm sinine 6 a wana wäärt 15 rbl 60 kop ja üks raudtelgiga tööwanker wäärt 5 rbl.  Jaan Albert omanduseks kirjutada.
 Jaan Albert tunnistab, et Johan Laarist ülesantud wõlg õige on ja lubab neide lehma ja wankri selle wõla ette wõtta omanduseks.
 Johhan Saar /allkiri/  Jaan Albert /allkiri/
Johan Laar on selle kohtule kui tundud palelikult inimene ja Jaan Albert on uskuda kui Wana Kuuste walla liige ja on selle kohtu ees see kakskümmend rubla 60 kop. Johan Laarile wõlgu laenanud, selle pärast on neide leping kindlaks tunnistada ja ühtlasi saab tunnistatud, et mõlemad lepingu tegijad oma käega lepingule alla kirjutasiwad.
Kohtu Eesistuja: J. Hansen /allkiri/
Kirjutaja: K. Laar /allkiri/
/Pitser/
