Ettekutsutud Ranna Raatwere ja Halliku Assikwere koolimaade rentnikud Karel Weskimets ja Jakob Annuk ja nendele Ranna ja Hallilku mõisa pärishärra woliniku härra Mühlenthali kiri 12. Oktobrist 1892 a. N: 43 etteloetud, mis pääle mõlemad rääkisid, et nemad neid kohtasid üleskuulutud tingimistega mitte enam rendi pääle ei ole wõinud wõtta ja nemad ka sel põhjusel Liiwimaa Talurahwa sääduse  § 116 on tähele panemata jätnud.
      Kaarel Weskimets
      Jakub Annuk
 Kohtu otsus: et koolmeistrid Kaarel Weskimets ja Jakob Annuk mitte ei ole sääduse sees § 116 (Liiwimaa Talurahwa asjus.)  määratud terminis oma kohade üle uut kaupa teinud, siis ei ole neil enam nende kohtate üle Jürripääwast 1893 a. saadik rendiõigust ja peawad omad kohad Jürripääwal 1893 a. käest ära andma, koolmeister Johanes Suits, Sassokwere kooli maade rentnik on ära  surnud ja selle kontraht selle järel Jürripääwal 1893 a. lõpnud ja Halliku koolimaade rentnik Jakob Karelson ei ole mitte enam Halliku koolikoha pääl ja tema elukoht teadmata ja ei ole siis kohtul wõimalik teda ette kutsuda ja koha üle otsustada. 
                               Kohtueesistuja Abram Saar.
                               Kohtumees    Joosep Hanst
                               Kohtumees    J Rosenberg
                               Kirjutaja         O. Seidenbach.
                   
