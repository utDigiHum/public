Sel 27 Märzil 1889.
Kaibas Unikülla möldre  Johan Kalpus, et tema poisi käest 16 Septembril 1888 Krimani Roiu kõrzi jures 51 küinart willast ried ärra om warrastedu sanu. Seda ried tonu pois Tarto linast wärwja käest wanudada ja ollnu Radi walla mehe Thomsoni omandus, kes 104 rubl selle riede est nõudnu, sest se ollnu 3 küinart lai, tema massnu selle riede ärra. Nüid om Soowere tallo sullase selle riede omma heina kuhja sehest leudnu ja om selle riede  ärra sallanu ja nõudis neide inemiste käest seda ried kes selle leudnu ja ärra om sallanu.
Sullane  Mihkel Woltri tunistas, et tema ütte kanga leudnu heina kuhja sehest ja andno selle riede perremees Endrik Woltri kätte, kel se rie praegu alles peab ollema.
Perremehe wend Mihkel Woltri 18 astat wanna tunistas et nemma selle riede trulli heina kuhja sehest leudnu ja ollewat praegu perremehe käen.
Otsus: et perremees Endrik Woltri ette om kutsuta.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks /allkiri/
"  Jaan Toom XXX
