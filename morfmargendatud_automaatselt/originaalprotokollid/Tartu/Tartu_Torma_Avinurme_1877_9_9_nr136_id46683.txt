sel. 9. Septembril 1877.
peakohtomees Josep Laurisson
teine kohtomees  Andres Kiik
kolmas kohtomes Jürri Tomik
Et Jaan Pärn ei olle omma Naist kes olli kahto poolest 24 tunni peale kinni moisted tapluse eest seie tonud, ning nüid weel kohto ees torre ja ei olle üht kella mis temma olli Jaan Kimmli käest ärra riisund. ei olle sellele kätte annud. siis moistab koggokonnakohhus et Jaan Pärn peab omma kanguse eest se Naisele moistetud trahw issi ärra istuma ja weel 24 tundi juure ja selle järrele 2 pääwa ja öed kinni istuma
Josep Laurisson XXX
Andres Kiik XXX
Jürri Tomik XXX
