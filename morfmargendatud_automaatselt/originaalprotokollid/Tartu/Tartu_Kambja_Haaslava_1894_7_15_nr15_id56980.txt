№ 15
14  Haaslawa  Wallakohus 15 Julil 1894
Juures oli  Eesistuja: Johan Hansen
Juuresistuja: Jaan Tekkel 
Johan Klaos 
 Johan Pilberg 
 Kriimani mõisa  perisprou  von Brusch wolinik mõisa walitseja herra O. Gläser palub Matsi talu rentnikule  Mihel Klaosele, kelle kondrahi aeg 23 Apr lõpeb kuulutada, et mõisa walitsus tema käest 23 Aprillil 1895 aastal see Matsi talu ära wõtab.
O. Glaezer /allkiri/
See ülesütlemine sai  Mihel Klaosenile Walla Seaduse § 215  põhjusel sest 1860 a. kuulutatud, kelle juures tema ühtegi wastu ütlemist ei awaldanud.
 Mihkel Klaosen  /allkiri/
Kohtu Eesistuja: J. Hansen /allkiri/
Juuresistujad: J. Pilberg /allkiri/
J. Tekkel /allkiri/
J. Klaos /allkiri/
Kirjutaja: K. Laar /allkiri/
