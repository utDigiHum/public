Ettekutsutud kadunud Jakob Waddi lese ja laste wöörmünder Kusta Rosenberg ja andis üles, et lesk ennast käsitööst toita, poeg Willem teeninud suwiliseks, olla 19 a. wana, ja Lena olla ka lugenud ja suwel teenistuses olnud, nüüd aga kodu ema juures. Warandust neil ei ole.
                                    Peakohtumees J. Sarwik.
                                          Kohtumees J. Stamm
                                          Kohtumees M. Piiri.
                                          Kirjutaja O. Leidenbach. 
