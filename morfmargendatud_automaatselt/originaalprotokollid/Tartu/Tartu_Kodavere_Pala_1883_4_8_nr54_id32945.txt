Kaibas wöörmünder Jaan Kubja, et tema käsknud  Josep Nõmme hobust andma Tee reviderija härra sõitu aga Josep Nõmm ei  ole hobust andnud, tema wõtnud siis Märt Piiri käest hobuse ja makssa lubanud selle eest 5 rubla. Josep Nõmm wastas temal olnud küll käsk hobust anda, aga temal ei ole olnud wõimalik hobust anda ja palunud Karel Olla et see oma hobuse anda, Olla lubanud
ka hobuse anda.
Otsus: Tulewaks kohtupääwaks Karel Olla ja Märt Piiri ette telli ja saab siis õientud.
                                                       Kohtumees: J.Soiew.
                                                       Kohtumees O. Mõisa.
                                                       Kohtumees: M.Mölder.
