Sel 2 Oktobril 1889
Kaibas Jürri Luha, et tema Kristjan Toomi naesele  Anna Toomile üts kõrd 15 rubl ja tõine kõrd 5 rubl  mattuse ehk ristimisse tarwis om andno ja nõuab nüid seda sest rahast tagasi, mis tema  Kristjan Toomi perrandajatele kohtu mõistuse perra peab massma.
Lesk  Anna Toom, kes Wanna Kusten ellab wastutas Jaan Hageli man ollen selle päle, et tema seda raha konnagi Jürri Luha käest ei olle sanu.
Johan Toom tunnistas, et üts kõrd ei olle  Ann Toom  last matta sanu sis om tema käsknu Jürri Luha käest küssida ja tõine kõrd küssinu tema naese käest kas raha sanu, sis üttelnu Anna Toom et tema ennegi 5 rubl om sanu.
Anna Toom wastutas weel Jaan Hageli man ollen, et se assi joba suremba kohtu läbbi om käinu sis ei wõis seda asja mitte enamb Kogukonna kohtu selletuse alla wõtta.
Jürri Luha andis weel ülles, et tema üts kõrd 30 rubl Maakohtu Kristjan Toomi est om massnu sis ollewat tema 100 rubl Jaan Wirrole Kristjan Toomi wõlga massnu ja Kristjan Toomi Magatsi wõlla est ollewat tema 120 rubl massma pidanu, keda Weriko Jürri Luha ja Kristjan Kapp Ahjalt teadwat, sis ollewat tema Kambja Kerriko ehituse raha ja tee tegemise raha Kristjan Toomi est massma pidanu.
Otsus: et Weriko Jürri Luha ja Kristjan Kapp ette om kutsuta ja Karl Toom Kooli Raatuse ulitze Kohi majan.
Kohtumees: Jaan Pruks /allkiri/
"  Jakob Saks /allkiri/
"  Jaan Treijal /allkiri/
