                 Sellsamal paewal 18 oktobril 1888. -
Ette tuli Peeter Tani Tartust ja kaebas, et eile tema poeletti lõhkumise perast on trahwitud saanud, aga Jaan Peebu Wõnnu kihelk. Pokka wallast on need lauad, mis letti laudadeks on arwatud, esiteks temale pakunud, tema seda aga mitte wastu wõtnud, mida Jaan Peebu siis sinna maha wisanud, mida ta siis perast oma kaupluse ees kõnniteeks tarwitanud. Puud olla seesama mees küll poole naela napsi eest temale toonud. - 
Jaan Peebu ütles küsimise pääle, et tema mingi kraami temale wiinud ei ole. -
Turukubijas N 1. L. Eller ütleb, et Jaan Peebu temale on ütelnud, et tema need letti lauad on Peeter Tanile toonud. -
Leena Tammberg, Jaani naine Haaslawast  ütles küsimise pääle, et Jaan Peebu on need lauad sinna toonud. -
Ette toodi kompweki kaupmees Fadei Drafimow ja tema ütles küsimise pääle, et tema lõhutud letti laudu juba pühapäewal hunikust wõtnu, lett on mõisa walitsuse ülewaatamise perast aga eesmaspäewa hommiku lõhutud. -
               Mõistus: Et Fadei Drafimow kohtu kohtumisele wastu pannud ja ainult "urjadniku" käsul kohtu ette sai toodud,
                               selleperast peab tema 2 rubla trahwi maksma, et tema kinnise poodi letti lõhkunud on, selle perast peab tema 1 rubla 
                               trahwi maksma. - Et Jaan Peebu wiina eest lauad Peeter Tanile on toonud, peab 1 rubla trahwi maksma. Peeter Tani
                               peab 1 rubla trahwi selle eest, et tema letti tarwitanud on, ja seda tarwitamist mitte teada annud ei ole. -
                               Jaan Toomberg saab priiks mõistetud. -
                               Mõistus kuulutati Fadei Drofimowile, Peeter Tanile, Jaan Peebule, Jaan Toombergile. -
                               T. r. s. r. § 773 ja 774 seletamisega. -
