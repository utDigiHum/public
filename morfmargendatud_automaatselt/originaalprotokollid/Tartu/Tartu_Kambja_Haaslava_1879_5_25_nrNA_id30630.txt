Haslawa kogokonna kohus 25 Mail 1879.
Juures istus:
Pääkohtumees Mihkel Klaos
kohtumees Johan Link
dito Johan Udel
Tuli ette Jaan Kromann ja kaebas, et Peter Kiter tema maa pääle kodapoolseks ilma tema lubata elama on tulnud ja kui tema sisse tuleku aeg Peter Kitert kogokonna kohtu abiga keelama on läinud, siis on Peter Kiter ja tema küidumees Mäl Silm kes mõlemad soldatid on, teda weel päälegi peksnud. 
 Peter Kiter selle kaebtuse üle küsitud, ütles wälja, et tema selle tare Johan Jägeri käest ostnud ja arwab, et temal selle maa peremehe Jaan Kromanniga midagi tegemist ei ole - löönud ei ole tema Jaan Kromanni mitte.
Kohtumees Johan Udel ütles wälja, et tema Peter Kitert ja Mäl Silmi on keelnud wöörmündrid Jaan Kromanni löömast aga nemad ei ole sellest mitte hoolinud, waid enne on Mäll Silm Kromani löönud ja pärast on Peter Kiter Jaab Kromanni rinnust kinni wõtnud ja maha löönud,
Johan Jäger tunnistas, et tema selle tare kül Peter Kiterile on müinud, aga selle maa eest, kelle pääl se tare seisab, on pidanud tema peremehe Jaan Kromaniga isi leppima.
Mäl Silm kui ka Peter Kiter on mõlemad Jaan Kromanni löönud.
Mäl Silm ei olnud mitte kolme kõrra kutsmise pääle tulnud.
Mõistus:
Peter Kiter peab oma tare Jaan Kromanni maa päält ära wima ja selle löömise asja pärast saab asi kohe keiserliku Tartu Silla kohtu üle kuulamise ja mõistmise alla antud.
Kohto lauan olliwa
Pääkohtumees Mihkel Klaos.
Kohtomees Johan Link
dito Johan Udel
Mõistus sai kohtu käijale T.S.R pärra aastast 1860 § 772 ja 773 kuulutud ning õppus antud mis tähele tuleb panna  kui keegi suuremad kohut tahab käija.
