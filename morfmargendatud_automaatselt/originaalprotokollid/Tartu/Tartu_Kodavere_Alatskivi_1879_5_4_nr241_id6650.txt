Poolele jänud Karel Läst kaebduses tullid tänna kaohtu ette Karel Läst, Kusta Päll, Jaan Wipper, Paul Päll omma issa Maddisegatul***
Karel Läst pallub kohhut, ses asjas otsus tehha, ilma, et Josep Pealkiwwi, Allatskiwwilt ja wöörmönder Karel PawelKuddinalt ärra kulatud saaks.
Maddis Päll kaest sai küssitud, et kas temma sel ööl koddu olnud, kui teised warrastud kraami jagganuwad, selle peale ütleb temma, et temma sest ei tea, agga temma poeg Paul ütleb kõrwal, et issa on sel ööl nimmelt ka koddu olnud.
Kohhus mõistis
Karel Läst kaebduse tühjaks, sest et ta omma kaebdust põhjendada pulle joudnud,
/: Otsus kulutud sel 4. Mail 1879:/
A. Krimm [allkiri], peaxxx Maddis Erm, kohtumeesA. Pärn [allkiri], "xxx Tawet Kook, "
