Awwinorme Koggokonna kohtus sel. 30mal Mail 1868
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Tännasel pääwal astus ette Mihkel Kütt Adrako karja mõisa järrel N 44. krundi mees, ja pallus koggokonna kohhut sedda tülli asja selletada, mis temmal teiste Adraku krundimeistega kopli pärrast on, ja et need sammad temmale, temma kitsa ruumi pärrast wie Aasta eest, külla karjama küllest, wasto temma piiri üks koppel annud, kuhhu nemmad issi ollid piiri ette tehnud, kust sadik se koppel sab, ja pealegi weel haugost ette lasknud, ja et temma sedda koplid selle aja sees hästi metsast puhhastanud, ja seal palio tööd tehnud, tahhawad agga nüid teised krundi mehhed sedda temma käest ärra wõtta, ja karja maaks tehha,.
Astusiwad ette teised Adraku külla krundimehhed, ja räkisiwad, et nemmad Mihkel Küttale agga nattukenne ollid lubbanud kopliks wõtta, kui agga Mihkel Kütt igga Aasta sedda koplid laiemaks ajab külla karja maa sisse, ja et nemmad selle pärrast nüid tahhawad arra wõtta.
Siis watas koggokonna kohhus sedda asja külla kaarti pealt järrele, ja leidis et se koppel mis Mihkel Kütta krundi külles on mitte suur ei olle, ja et Mihkel Kütt ilma selleta läbbi ei sa ning 
Moistis
Et Adrako krundi mehhed isse wie Aasta eest Mihkel Küttale sedda koplid annud ollid kelle juures temma paljo tööd tehnud ja waewa nähnud, jäeb selle pärrast se koppel Mihkel Kütta kätte ka eddispiddi prukida
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
