Palla mõisa herra ja selle walla liige Tõnnis Oksa wahel sai tännasel päewal allpisaw lepping erratehtud.
                                                                                        § 1.
Palla mõisa herra annab Tõnnis Oksale 23 Aprillist 1892 allustates kolme aasta peal, se on kuini 23 Aprili 1895 se Piiri N: XXIV tallo metsmaa tükk N: 39.i suur 2 wakkamaa 9 kapp, omale pruukida ja maksab Tõnnis Oksa selle eest igga aasta pealmaksetawal kõrral 29 Septembril (Mihkli päewal) 6 (kuus) rubla rent.
                                                                                         § 2.
Rentnik pruugib sedda maatükki kolmest wäljas, ja peab igga aasta kolmanda jaole sõnnikut anda kõige wähemb 20(kakskümmet) ühhe hobuse koormat.
                                                                                         § 3.
Kui rentnik peaks selle kontrahi aega wahhel surema, siis saab see maatükk jälle mõisale tagasi, Jüripäewal peale rentniko surma ilma et tema perrijat mingisugust kahjutasumist wõiksit nõuda. Mis rentnik ennast selle maatükkile peaks kulluma  tedda parrandada, selle eest ei wõi tema midagi mõisa poolt nõuda.
                                                                                         § 4.
Kui rentnik peaks mõisa ihk muu hinnimese warandus warrastama, siis on see kaup  katski, ja on mõisa herrale ehk tema asemikul õigus rentnikul üles üttelda ja peab see tulewa Jüripäewal see maa tükk tagasi ja käest erra anda.
                                                                                          § 5.
Kui se kontraht  kumbgi poolest kuus kuud enne lõppmist (23 Oktobril 1894) ülles ütteldut sai, siis keib temma weel üks aasta eddesi.
                                Tõnnis Oksa  ristid.
 Kohtu attestat niisama kui Protokoll N: 7.      
   
