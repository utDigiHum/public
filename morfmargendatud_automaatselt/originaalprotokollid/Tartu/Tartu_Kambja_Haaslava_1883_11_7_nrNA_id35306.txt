Haslawa Kogukonna Kohus sel 7 Nowember 1883
Man ollid:
Päkohtumees: Jaan Link
Kohtumees: 
Peter Iwan
Johan Grossberg 
Kaibas Liesu Werno Jaan Sawiko man ollen, et üttel ööl tullon neide tallo sullane Jaan Särg kodu kutsunu teda wälja ja üttelnu, et tsiga om wäljan wällan ollnu tõised mehed, kes Jaan Särg ütten kõrtsist tullnuwa, ja Jaan Särg tahtnu teda küüni wija ent tema om ärra sanu pageda, selle jures om üts tundmada mees tema säljast Perremehe kassuka ärra ja kassuk jänu ärra, tema pallub et kohus Jaan Särge sunis seda kassukat massma, nipalju kui Perremees selle est nõuab, tunistajas andis ülles Karl Lätti ja Perremees Johan Madison.
Johani Perremees Johan Madison tunistas, et tema kuldnu kui Jaan Särg tüdruk Lies Werno wasta üttelnu: Lies tulle wälja tsiad karjuwa, selle päle lännu tüdruk wälja ja perrast ösel lännu tüdruk wälja ja üttelnu et kassuk wõeti ärra. Johan Madison nõudis 10 Rubl selle kassuka est.
Jaan Särg wastutas selle kaibuse päle, et tema Lies Wernot wälja kutsunu ei olle ja keigest sest kaibusest midagi ei tea.
Otsus: et  Karl Lätti tõises kohtupäiwas seija kohtu ette kutsutu om.
Päkohtomees: Jaan Link /allkiri/
Kohtomees:
Johan Klaosen XXX
Peter Iwan /allkiri/
Johan Krosbärk /allkiri/
