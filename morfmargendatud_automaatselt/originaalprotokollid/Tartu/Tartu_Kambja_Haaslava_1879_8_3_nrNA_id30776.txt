Haslawa kogokonnakohus sel 3 Augustil 1879.
Juures istus:
Pääkohtomees: Mihkel Klaos
Kohtumees Johan Link
dito Johan Udel
Tuli ette  An Ehrmann ja kaebas  Johan Klaossen'i pääle, kes temaga lihalikult elanud ja on temale selle elu läbi üks tüttar laps 10 pääwa enne Jõulu 1878 sündinud.  Joh. Klaossen  on lubanud teda omale naiseks wõtta, aga et tema seda nüüd mitte ei tegewad, nõudis tema selle lapse ülespidamiseks 120 rubl.
 Johan Klaossen sai selle kaebtuse üle küsitud ja salgas sellest tüdrukust osa wõtnud olewad.
Selle pääle saiwad  An Ehrmannist üles antud tunnistajad  An Pallo Tartu linnast, Lena Kitsing ja Mari Grünthal üle kuulatud.
An Pallo tunnistas, et tema on näinud, kui Johan Klaossen Tartu linnas An Ehkmanni taga käinud. 
Lena Kitzing ei teadnud midagi selle asja sees tunnistust anda.
Marri Grünthal ütles wälja, et Mihkel Klaossen olewad rehe peksmise aeg An Ehrmanni man käinud.
Mõistus: 
Et selget tunnistust ei ole jääb Joh. Klaossen  An Ehrmanni lapse ülalpidamisest priiks.
Peakohtumees M. Klaos
Kohtumees Joh. Link
dito Joh. Udel
Mõistus sai kohtukäijatele T.S.R. pärra aast. 1860 § 772-774 kuulutud ning õppus antud, mis tähele tuleb panna, kui keegi suuremad kohut tahab nõuda - ja ei olnud  An Ehrmann  selle otsusega rahul. 
