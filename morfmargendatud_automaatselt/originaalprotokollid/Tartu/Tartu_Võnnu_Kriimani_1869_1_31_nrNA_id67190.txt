Krimani Koggokonna kohtus 31 Januar 1869.
  Mai Laetti tulli koggokonna kohto ette ja kaebas et Temma perremees  Jaan Wisser  Temmale ennam süia ei anna ja on mind poliko aastaga tenistusest wallale teinud ja ei lubbab mitte minno palka maksta mis sada on 6 Rubla 2 leisikat linno, 1⅓ ernid 40 naela leiba seperrast et Temmale Oksion on tehtud ja et temmal ennam süia anda ei ka palka mulle maksta kustki ei olle.
 Jaan Wisser tulli ette ja ütles se on tõssi kül et ma tüdruko ollen ärra ajanud sest minno naesel ja lastel ei olle middagi süia ja mis siis weel tüdrukule pean andma ja palka ei olle mul ka mitte praegust kustki Temmale maksta.
Mõistus 31 Januaril 1869: 
Koggokonna kohhus mõistis et  Mai Laett  peab senni kaua omma palgaga kannatama kui  Jaan Wisser  saab sedda palka tenima pantud ja  Jaan Wisser peab se tüdruk senni kaua omma toidun piddma kunni tenistuse wõi kaubeldud aasta aig ümber saab.
Kohto lauan olliwa:
Peakohtomees Jürri Bragli XXX
Kohtomees  Johan Mina XXX
" "      Ado Lenzius XXX
Mai Läett ei olnud se mõistusega rahhul ning wõttis selsama päewal Protokol wälja.
Mai Lätt on 21 Febr 6 Rubla palka kätte sanud 1 leisikas linno eest 2 Rubla 50 kop.
M. Baumann /allkiri/
