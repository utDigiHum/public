Se nekrut Peter Luhha nowwap selle Johan Birkenbaumi kaest wõlg rahha 1 rub. 70 kop. hbdt. warsti kätte.
Johan Birkenbaum wastutap selle Peter Luhha naudmisse pääl, et temma ei ollewat kedddagi Peter Luhhale wõlgo.
Peter Jägger tulli ette ja tunnist: se Johan Birkenbaum lubbanut selle Peter Luhha wõlg 1 r. 70 kop. Peter Luhha naudmisse pääl ärra massa.
Moistus: Johan Birkenbaum piap warsti 1 rub. 70 kop. hõbdt. Peter Luhhale ärra massma.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohto käimisse õppetusse kirri wälja andtu.
