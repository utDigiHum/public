Pallamõisawalitseja J.Christiani kaebas: Minewa pühapäew on  8 noort saart 6 tüki 1 tolli jämme ja 2 tüki 2 tolli jämetat esimeset 1 süld ja teiset pikemat. Laupäewa õhto olnu Kusta Nukka ja Karel Soewa Mõisaheinama peal hõitsis.Teisel hommingul kui metsawaht kaema läinu olnu need saaret ära põlenut, aga se kahjo ola Madis Saarest tulnu.
Madis Saar wastas selepeale: Tema teinu kül heina aeg ükskõrd moddu perast tulli sina lähidele, kus need saaret ära põlenu, kanna sisse tulli, tema kustutanu aga seda warsti ära. Wanutud Metsawaht Karel Waht andis protokoli: Madis Saar ütelnu temale wimne kõrd, et Jürri Kukk se tulli teinu. Maddis Saar nüüd küsitu, ütlep tema ütelnu seda nalja perast.
Kusta Nukka tunistas: Maddis Saar teinu sele Mõisamaatüki peale, kus need Saaret ärapõlenu tulle ülles, ja ola se sama tulli minewa pühapäew neide saarte juurde läinut olnu.
Metsawaht Waht andis weel üles, et 6 saart 1 Toll jämme ja 1 süld pik ja 2 saart 2 tolli jäme ja rohkem kui sülapikaliset olnu. Mõisawalitsus nõudis 15 rubla kahjotasumist.
 Kohus                                                                                          Mõistis:
         Maddis Saar peab iga põlenut saare eest 50 kop. Se on 8 saare eest 4 rubla 8 päewa sees wälja maksma.
Se mõistus sai kohtokäijatele seaduse wiisil eteloetu ja Maddis Saarele oppuse leht  antu.
                                              Kohtomees: Josep Hawakiwi +++
                                              Kohtomees:Josep Rosenberg +++
                                              Abikohtomees: Tomas Welt     +++
                                                                          Kirj. allkiri.                     
