Lesknaine Natalia Uljanowa kaibab: "Tema müünud Lahe kõrtsi juures korwiga õunu; seal tulnud 3 Kamarohwi töölist, üks neist, Maxim Semenow, wõtnud murdnud korwi puruks ja tõised kaks, Pawel Wassiljew ja Iwan Filatow, hakanud teda peksma, et kül esimene /Maxim/ neid keelnud."
Selle kaibuse tõeks tunnistajad on Lahe sep, Tõno Kook, ja wana köstre emand.
Mõistetud: Uljanowale maksab Maxim 50 kopik, Pawel 75 kopik ja Iwan 75 kop. ühtekokko 2 r. h.
