Sel 13nel webr 1887.
                        Man oliwa Päkohtumees M. Karru
                                           Kohtumees J. Aleksanderson
                                                 -"-        J. Kulberg
Jaan Numa kaebap et kasak Mihkel Tam om tema päle wallawanemale kaewanud, et tema pois Taniel Hõim prantsuse aigusen om, mina olen nüid wallawanema käsu päle poisiga Tartun kreistohtri een ärä käinu, ja pois om terwe leitu, mina nõwwa selle Tartun poisiga käigi eest 3. rbla, ja ei ole ka sellega rahul, et minu maja üle särane laim ehk teotus om wälja tettu.
Kasak M. Tam üttel, et tütruk Liis Warn om temale seda kõnelnu, et pois saap perremehe poolt joo ütsikult erält erält söök ja jook ja pel-
gawa poisi aigust, ja käsk pallel et mina wallawanemale seda pidi üles andma, et kui mina ei anna üles, sis lubas ta essi minnä. - Selle juttu päle anni mina wallawanemale seda juttu üles.
Liis Warn üttel, et perremees oli essi see een koneleja ja anti joo poisile erält söök, mes pois mulle essi kõnnel, ja juwwa es lubatu ka täl ka perre üttest see oli se põhhi mes minu pelgama pand. -
Pois Taniel Hõim ja kõrwal saisja Joh Ellen, üttel et temale om kül 2. päiwa eralt söök antu, ja et muu rahwas seda kõik kõnelewa, selle oli mina ka kahtlane
Kohus mõistis et pois Taniel Hõim peap peremehele J. Numale Tartu käigi eest 1 1/2 rbl omast palgast masma
Naad olid mõlemat sellega rahul
