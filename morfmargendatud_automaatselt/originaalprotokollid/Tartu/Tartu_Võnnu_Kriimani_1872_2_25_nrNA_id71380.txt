Krimannis 25 Februaril 1872.
 Peter Tedder  tulli ette ja kaebas et minnul on  Juhan Semeni käest 4 Rublad sada mis Temma jo minnew süggise mulle lubbas ärra maksta ning pallun et ma kohto abbi läbbi Temma käest se rahha kätte saan.
Juhan Semen tulli ette ja ütles: se on õige kül et ma lubbasin se rahha 4 Rubla Tedrele ärra maksta agga mul ei olnud mitte rahha.
Mõistus: Koggokonna Kohhus mõistis et Juhan Semen peab se nimmetud wõlg 4 Rubla 2 näddali sissen Peter Teddrele ärra maksma.
Kohto lauan olliwa
Peakohtomees Juhan Mina XXX
Kohtomees Juhan Raswa 
Wöörmünder Mihkel Kärik XXX
