Ette tulli ja kaiwas Mihkel Lestman et Jürri Ehrlich on tema Poissi Karel Lestman hirmsaste aija saibaga peksnud, nenda et pois nüüd kohhalt sandist on peksetud ja tõi 3 saiwast seija kohto ette, ja tunnistab et neidega on peksetud. Nenda on se kohhus need saibad kokko sädind, agga ei olle mitte ütski sündind kokko.
Jürri Ehrlich wastab, et pois on koera talle päle aijanud ja hirmsast sõimanud, selle on tema tedda leppa witsaga mõnni kõrd lönud.
Johan Märtinson tunnistab et leppa witsaga 5 wai 6 kõrd lönud.
Mõistus.
Siis on kohhus mõistnud et Mihkel Lestman peab 1 rbl wõltsi eest trahwi masma, ja se witsaga peks jääb sõimo ja koera pääle aijamisse perrast tassa. Kohtootsus on kulutud õppus leht wälja anto.
Pä kohtomees Märt Pok
Juresistoja Mihkel Männ
        "           Peter Lesthal
