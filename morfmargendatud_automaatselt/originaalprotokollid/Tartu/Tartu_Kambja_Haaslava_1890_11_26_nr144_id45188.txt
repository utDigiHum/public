№ 144
Sel 26 Nowembril 1890
Kaibas Ann Konno, kes Tarto linnan Wastsen ulitzen Nr 27 ellab, et minewa suwel ollnu tema poeg Haslawa Tilba perremehe Jaan Rosenthali jures karjan ja kauba perra pidanu Jaan Rosenthal 10 naela zia raswa ja 20 naela zia liha palga jurde andma, ent Jaan Rosenthal andno 10 naela lamba raswa ja 20 naela lamba liha. Kaibaja nõuab kauba perra seda raswa ja Liha ja pallus et Kohus seda ärra massma sunis.
Ann Konno ei mõista kirjutada.
Jaan Rosenthal wastutas, et tema seda kaubelnu ei olle kas lamba ehk zia raswa ja liha, enegi 10 naela raswa ja 20 naela liha ja tema massnu se kaubeltu rassew ja liha ärra ja nimelt lamba liha ja rassew.
Jaan Rosenthal /allkiri/
Anno Konno andis tunistajas ülles Peter Saks, kes Krimanin ellab ja kauba tegemise jures ollnu.
Ann Konno ei mõista kirjutada.
Peter Saks Krimanist tunistas, et tema om kuldno 10 naela raswa ja 20 naela liha, mis liha ei wõi tema üttelda.
Peter Saks /allkiri/
Mõistetu: et Ann Konno kaibus tühjas om mõista selle et tema seda nõudmist selges ei jõua teha.
Se mõistus sai kulutedo.
Jaan Rosenthal /allkiri/   Ann Konno ei mõista kirjutada.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks /allkiri/
"  Peter Hansen XXX
