Haslawa kogokonna kohus 9 Julil  1880
Juures istus:
Peakohtumees  M. Klaos
kohtumees Joh. Udel
dito  Rein Anko
Tuli ette  Jaan Klaossen  ja kaebas:  Peter Laban  olewat tema tsea rewolwriga maha lasknud, mis 15 rubla wäärt ja nõuab, et see temale tasutud ja pois üleanetu töö eest trahwitud saaks. 
 Peter Laban salgab seda teinud olewad.
 Karl Ango  tunnistas, et  Peter Laban olla tol pääwal ühe rewolwriga Klaose usse aea peal lasknud ja pääle tolle üle nurme läinud ja et pääkohtumees seda tsiga on kaenud kust tema lastud oli.
Peakohtumees tunnistas, et tema on pääle tolle, kui tsiga lastud sanud, teda ülekaenud ja leidnud, et tema ühe kuuliga lastud saanud. 
Mõistus:
 Peter Laban  peab 15 rubl. tsea eest  Jaan Klaossenile masma. 
Mõistus sai kohtu käijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud mis tähele tuleb panna, kui keegi suuremat kohut tahab käija ja ei olnud Peter Laban selega mitte rahul.
