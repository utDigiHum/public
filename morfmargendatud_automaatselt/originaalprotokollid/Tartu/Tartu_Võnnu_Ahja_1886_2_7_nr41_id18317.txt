Jaan Kulp kaibas, et temal 33 rubla Mihkel Wossmani käest saada, mis see ei salga ja 1. Mail ära maksa lubab.
Leping: 1. Mail maksab ära.
Pääkohtumees: P. Kripson XXX
Kohtumees: P. Pedoson XXX
Kohtumees: J Kiljak &lt;allkiri&gt;
Kohtumees: K Piir &lt;allkiri&gt;
Mõiste sai kohtukäijatele kuulutatud T.S.R. §772-774 järele aastast 1860 ja õpus antud mis tähele panda tuleb kui keegi kõrgemat kohut nõuab ning ei olnud wallawanem rahul.
Kirjutaja: Chr Kapp &lt;allkiri&gt;
