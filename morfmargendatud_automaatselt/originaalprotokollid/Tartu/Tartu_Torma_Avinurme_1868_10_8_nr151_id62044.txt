Awwinorme Koggu konna kohtus  sel 8mal Octobril 1868
Kohto juures olliwad.
pea kohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Jürri Mäggi astus ette ja kaebas et temma Wend Josep Mäggi, kes temmast lahhus ellab temma hobbose omma lubbaga ärra wõtnud ja selle põhja peal ennast toetab, et issa tedda käskinud ärra wõtta, et se issa hobbone peab ollema.
Astus ette Mihkel Mäggi, ja räkis et temma annud omma tallu poja Jürri kätte ja siis jänud ka se hobbone Jürri kätte, kedda temma agga nüid ärra wõttab
Kutsuti ette üks neist poegatest Jakob Mäggi kes tunnistas, et se kord kui issa omma tallo Jürri kätte annud, ei olle muud Maja Kramist olnud, kui kaks Lehma, ja pärrast sedda kui temma monne Aasta pärrast ärra lahkunud annud Jürri temmale üks lehm, ja kui Josep ja issa ärra lahkunud Jürrist on need Jürri käest kahhepeale üks Lehm sanud.
Moistus
Et nooremad Wennad Jakob ja Josep Mäggi Jürrida mitto Aastad teeninud ollid ilma palgata peab Jürri nendele kummagille üks Lehm andma. ja et issa ka weel Jürri käest ühtegi Maja ossa polle sanud, peab Jürri issale ka üks Lehm andma, agga se hobbone sab Jürrile taggasi antud.
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
