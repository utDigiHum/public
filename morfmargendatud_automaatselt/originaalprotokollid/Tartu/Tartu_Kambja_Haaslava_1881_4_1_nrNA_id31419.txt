Haslawa kogokonna kohus 1 April 1881.
Juures istus:
Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
dito Johan Udel
dito Johan Jantzen
Tuli ette  Mari Luha käemees Gottlieb Päiw ja kaebas: temal on 4 aasta 3 kuu teenitud palka à 3 rubl. kuu see teeb wälja 153 rubl.  Joachim Rosenthali käest saada, sest rahast tuleb maha arwata 16 wakka kartohwlid à wakk 50 kop. teeb wälja 8 rubl. ja 20 rubl. puhast raha, ülepia 28 rubl. mis tema kätte on saanud, tema on kaebatawa käest teenistusse aego mitto korda oma palka küsinud, aga see on enda iga kõrd wabandanud, et temal lahtist raha ei ole ja tõine kõrd jälle üttelnud, ega sinul sest rahast hädda ei ole tema wõib sissen seista ja sina saad ühe kõrraga kätte, mis aga saamata on jäänud ning palub seda nüüd kätte saada.
 Joachim Rosenthal kaebtuse üle küsitud ütles wälja:  Mari Luha on teda kül nõnda kaua teeninud ja on kuu palk 2½ rubl. olnud, rehnungi pidamise juures on temal Mari Luhale 125 rubl. maksa tulnud ja et kaibaja teda wäega palju on warastanud ja temale kahju teinud, siis on tema sest rahast 105 rubl. maha arwanud nõnda et temal 20 rubl. Mari Luhale maksa jäänud, mis tema ka kätte on saanud. Sest wargusest ja kahjust ei ole tema enne tääda saanud kui pärametse aasta.
 Mari Luha wastutas selle pääle, et tema midagi warastanud ei ole, selle et  Rosenthal teda siis mitte nõnda kaua oma juures ei oleks pidanud.
Tunnistaja  An Jürgenson  Tartust ja  Peter Alla  Keeri wallast ei olnud mitte tulnud.
Mõistus.
Selle et   Joachim Rosenthal kui  Mari Luha teda warastas mitte kohtu kätte ei andnud ja nüüd kuna tema mant ära on ja oma palka nõuab seda awaldab, siis peab  Joachim Rosenthal 125 rubl. 8 pääwa sees Mari Luha  hääks wälja maksma. 
Mihkel Klaos
Johan Link 
Johan Udel
Johan Jantzen
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud mis tähele tuleb panna kui keegi kõrgemat kohut tahab käija ja ei olnud kohtukäijad mitte sellega rahul.
