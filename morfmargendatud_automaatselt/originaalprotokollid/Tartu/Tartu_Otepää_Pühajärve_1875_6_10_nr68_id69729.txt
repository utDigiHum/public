Nustako Apoteki E. Jürwetson kaebas, et sel 29 Mai'il s.a. enne lounat kello 10. aigo tungis joobnul kombel Kastokatzi Tamme Tallo perremees Jaan Tamm minnu apteki sissen ja nõudis särast rohto, mis ma mitte es woi anda, selle et önnetus woi neide rohto läbbi sunnita, sis palsui ma et ta mitte ei lärmitsa. Kui ta selle pääle kuulnud, sis teggin ma usse lahti ja saatsin tedda walja, selle et ta liig joobnu näitas ollewad.
Warsti päälesedda tahtis temma wägusi jälle apteki sisse tungida, agga mul olli us lukku pantu ja sis hakkas temma mind häbbematal wiisil usse takkan soimata, mes Liis Tamm ja Liis Koiw Kastolatzi Elwa tallost ja ka keik törahwas Nusatko platsi pääle kuulsin.
Sellepärast pallun ma ütte aulikko koggokonna kohto allandlikkult minno kaubus wasto wõtta ja sedda üllewal nimmetedu Jaan Tamm säeduse perra trahwi ja sunnita, et kohto kaemisse ja aija ärrawimisse kullu ärra tassotab.
Tunnistaja om Liis Tamm ja Liis Koiw.
Otsus: Töisel kohto päewal ette tallitada tunnistaja ja kaebaja nink kaewatab. 
Päekohtomeed: Johhan Kõiw (XXX)
Kohtomees: J. Kuusk (allkiri)
Kohtomees Hans Task (XXX)
