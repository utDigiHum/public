Alatskiwi Mõisawalitsuse ja Kogokonna kohtu poolest saawad allseiswad tunnistused siia protokolli ülestähendud:
1. Juhan Jõgi ei leidnud, kui tema mõisa sawikoda wahiks tuli, mitte rukid maas külwetud;
2. Mõisapobulid Siim Siim, 3. Juhan Bringfeldt ja 4. Peeter Moor on omad elomajad isi üles raiunud ja omad õled katukseks pannud.
