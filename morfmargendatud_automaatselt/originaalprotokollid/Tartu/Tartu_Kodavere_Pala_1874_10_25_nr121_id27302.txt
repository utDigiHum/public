 Pallal sel 25 Oktobril 1874.
                        Kohtomees:Josep Avakiwwi.
                        Kohtomees: Josep Rosenberg.
                        Kohtomees: Tomas Welt. 
Kustaw Kuil Kokoralt ütles wälja,et nende hobbosed on all pool Kilgi Kõrtsi Kaewoliggi seisnud,siis on Michel Oebius tulnud  sõitmisega,ja Michel Õunapu wankri Keige meestega ümberaianud ja tee olnud lahti, et enneon rahvas seält möda läinud ja pärrast ka.Karel Kuil ütles wälja, et nemmad on Krawi äres Kilgi Kõrtsi kaewo liggi hobbosega seisnud ja siis on Palla Kärner sõites tulnud ja hiohel Õunapu wankri keige naesega küllite aianud agga tee olnud lahti. Karel Peetso ütles wälja,et nemmad on all pool Kilgi kõrtsi kaevo liggi Krawi ääres seisnud ja tee olnud lahti ja siis on tulnud Palla Kärner hobbosega käies taggast möda ja Michel Õunapu naest keige wankriga ümber aianud. Peale selle tulli ette Kallastelt Sohpie Ehrlich omma Eestseisjaga Karel Kuil ja kaebas,et nemmad on seisnud Kilgi Kõrtsi Kaewo jures hobbestega, ja siis on Michel Oeibius seält sõites möda aianud ja neid  Õunapu naesega ühhes ümber aianud.Keige wankriga ühhes ja temma  on palju haiget saanud ja temma nõuab selle eest 25 Rubla hõb. Kohhus arwas,sedda asja essite Tartu SillaKohto kätte anda,sest et se ümberaiaminne olli Postmaantee peäl sündinud;agga pärrast teggi otsuse seddawisi.
                                                                              Otsus:
 Et Kärner Oebius maksab kummalegi naesele Kedda temma ümber aias 2 Rubla h. seonühte kokko 4 Rubla h. ja ühhe rubla maksab ta trahwilaeka se ette waatamisse puudusse pärrast.Kohtuotsus sai Appelationi sädusse järrel  772 ja 773 Kohtu käiatele kuulutud,ja nendele Appellationi wälljaantud. Sohwie Ehrlich ei olnud selle otsusega mitte rahhul ja pallus Kärner Oebius Protokoll ka wälja, mis neile 8 päewa pärrast sai lubbatud.
Protolkoll wälja antud sel 1. November 1874.
                                               Kohtomees:Josep Awakiwi      +++
                                               Kohtomees:Josep Rosenberg +++
                                               Kohtomees: Tomas Welt          +++
                                               Kirjutaja assemel J.Saul.                      
