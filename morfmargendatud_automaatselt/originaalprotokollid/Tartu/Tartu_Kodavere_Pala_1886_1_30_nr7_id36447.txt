Ette kutsutud Kustaw Blum Tarakwerest ja sai temale selle  kogukonna kohtu otsus 31 Januarist 1885 N: 14 Talurahwa sääduse § 772,773 ja 774 põhjusse peale kõige tarwiliku õppustega ette kuulutud.
Kustaw Blum ei olnud selle otsusega rahul ja anti temale selsamal korral appellationi kiri kätte.
                                   Pääkohtumees J. Hawakiwi.
                                         Kohtumees M. Piiri.
                                         Kohtumees J. Taggoma
                                         Kirjutaja allkiri.
