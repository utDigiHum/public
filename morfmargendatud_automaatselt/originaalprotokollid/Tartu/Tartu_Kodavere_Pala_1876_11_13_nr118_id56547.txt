Pala Mõisawalitsuse saadik  Michel Oebius kaebas: Äraläinud suwel on Josep Wända  1 kuusk 5, tolli paks 3 sülda s.o. 18 jalga pikk, 1 kask 11 sülda paks 4½ sülda s.o.27 jalga pikk, 1 kask 4 sülda  paks 3 sülda s. o. 18 jalga pikk ärapõletanu, mis eest kaebaja 5 rubla nõudis.
  Metsawaht Kusta Eber oli tunistaja. Metsawaht Kusta Eber ütles, et Josep Wända on kül ni paljo puid ära põletanu, kuidas kaebduses üles antu. Josep Wända oma isa Jaan Wända kõrwal ütles, et Josep Wända külma perast se tulli üllesteinu ja siis se kahjo juhtunu. 
                                             Kohus mõistis:
 Jaan Wända maksab oma poja süü eest iga puu eest 50 kop. se on, 1 rubla 50 kop. ja saab Joosep Wända 10 witsa lööki.
 Se mõistus sai apelatsioni seadusega eteloetu.
            Kohtumees: Josep Hawakiwi    xxx
            Kohtumees: Josep Rosenberg xxx 
            Abi kohtumees: Tomas Welt    xxx  
             
