 Tuli ette Karl Annuk ja kaebas et Jakob Wända temale kaks aastat 14 rubla wõlga on ja palus seda raha  sisse nõuda. Ettekutsutud Jakob Wända ja tunnistas seda õigeks et tema 14 rbl Karl Annukale wõlga on lubas Mihkli pääwal seda raha äramaksa. Karl Annuk nõudis kuni Mihkli pääwani   wõib ootama peab 70 kop. protsenti.
 Otsus et Jakob Wända ise tunnistab Karl Annukale 14 rbl. wõlgu on maksab need 14 rbl. 70 kop. Mihklipääwal 1887 aastal Karl Annukale ära. See otsus sai kuulutud.
                                Kohtumees J. Stamm.
                                 Kohtumees M. Piiri.
                                 abi Kohtumees J. Tagoma.
                                 Kirjutaja G. Palm.
