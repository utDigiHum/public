Tuli ette Kustaw Nautras ja kaebas et temal Kustaw Nukka käest 1 wakk rukkist ja 1 wakk odre palka nõuda on.
Kustaw Nukka wastas küsimise pääle, et tema 1 wakk rukkid ja 1/3 wakka odre küll kinni peab selle eest et Kusta Nautras sel aja metsast witsa toonud kelle eest 85 kop trahwi maksnud ja on peremehe kasuk hooletuse läbi ühe ao koorma all äramädaneta lasknud niisamuti üks sädelga rihm.
Kasuka ja rihma eest hoiab see wakk rukkid nii kui pool kahju ja metsa trahwi eest see kolmandik odre kinni. Nautras tunnistas seda õigeks et kasuka hooletuse läbi aokoorma all äramädaneta lasknud. Aga Peremehe käsu peale oli mõisametsast witsu tooma läinud, keda ka Kusta Nukka õigeks tunnistas.
Otsus: Sellepärast et Kusta Nukka se poissi metsa witsu tooma saatnud jääb Nautras sellest süüst lahti ja annab Nukka need kinni peetud odrad temale kätte. Selle eest aga , et Nautras tunnistas kasuka hooletuse läbi äramädaneta lasknud jääb see wakk rukkit Kusta Nukkale kahjutasumiseks.
See otsus sai ettekuulutud ja ei olnud Kustaw Nautras sellega rahul.
                                   Pääkohtumees     J. Hawakiwi.
                                          Kohtumeees M. Piiri
                                   abi kohtumees    J. Taggoma
                                         kirjutaja allkiri.
