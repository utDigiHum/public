Kaibas Josep Lätti naine Miina eeskostja Josep Kukk kõrwal et olla Mihkel Lall 26 Mai tema tuppa ja sõimanud teda wargaks ja  wiimati löönud teda kiwiga 1 kord wasto rindo ja teine kord wasto pääd nii et ta maha kukkunud oma lapse pääle ja sel tulnud ka sust ja ninast weri wälja ja nõuab nüüd nende 2 löögi päält 30 rubl waluraha.
Wastas Mihkel Lall  tema lähnud nende tuppa ja küsinud näita oma nuga, sest  minu oma on ära kadunud, kas on see mino oma, tema wõtnud (Lätti naine) siis pange ja tahtnud teda pangega lüia ja lähnud ka löövatale külge, tema käsknud ära taganeda, tema jooksnud siis majast wälja aga löönud ei ole teda.
Otsus: Tulewaks  kohtu pääwaks tunnistajad Lena Hallik ja Lena Suits ette kutsu.
                                                             Kohtum J. Soiewa.
                                                            Kohtum  O. Mõisa.
                                                             Kohtum  M. Mölder.
