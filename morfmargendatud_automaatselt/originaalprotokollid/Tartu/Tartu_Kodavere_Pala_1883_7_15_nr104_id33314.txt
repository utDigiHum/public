Silla kohtu kätte antud Protokoll sest 9 julist N 7654. sai ette loetud ja otsus tehtud:
Otsus: Et Miina Puusep ja Joosep Welt wastastiko on tülitsenud siis maksab Josep Welt 1 rubl trahwi laega 8 pääwa sees, Miina Puusep aga peab 24. tundi Palla walla wangihoones kinni istma, selle pärast et naad wastastiku tülitsenut.
See otsus sai ette loetut. Miina Puuseppale wöörmünder  Jüri Tralla kõrwal rahu ei ole sai oppuse leht wälja antud.
                                 peakohtumees Hindrek Horn
                                 Kohtum             Josep Soiewa
                                 Kohtumees       Otto Mõisa.  
