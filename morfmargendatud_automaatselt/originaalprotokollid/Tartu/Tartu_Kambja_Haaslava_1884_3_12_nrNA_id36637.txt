Sel 12 Märtsil 1884.
Kaibas  Peter Grosberg, et tema andnu  Daniel Sakna kätte 8 lamba nahka pessa ja nüüd olewat  Daniel Saknal need nahad ära warastedu ent tema nõuab aga neide nahku eest a 85 Kop see teeb wälja 680 Kop. ja palus, et kohus seda masma sunnis.
Daniel Sakna wastutas selle kaibuse pääle ja ütles et see nõudmine õige on, aga temal ei ole wõimalik seda praegu massa.
Mõistetu. et Daniel Sakna peab see raha pool Jürripäewal 1884 ja tõine pool Mihkli päewal s.a. ära masma.
See mõistus sai kuulutedu.
Kohtumees: Johan Klaosen XXX
 Peter Iwan /allkiri/ 
