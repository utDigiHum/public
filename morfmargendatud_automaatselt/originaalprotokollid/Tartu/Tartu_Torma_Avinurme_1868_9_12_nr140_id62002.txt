Awwinorme Koggukonna kohtus sel. 12mal Septembr 1868.
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis 
kolmas kohtomees Tomas Pärn
Jürri Traks tulli ette ja tõstis kaebtust omma sullase Maddis Pajo peale, et temma sedda kewwade sullaseks kaubelnud, ja lubbanud temmale palka 20 Robla rahha, ja tubbakad nipaljo, kui temma agga prugib, tahtnud agga temma omma sullast nüid tökkutimetsa sata, üttelnud Maddis Pajo, minna muido tökkutimetsa ei lähhe kui sa minnole 5 Robla palka juure ei panne ja et temma sedda ei olle lubbanud, lähnud Maddis Pajo omma lubbaga temma juurest ärra. 
Kutsuti ette Maddis Pajo, kes räkis et temmal olnud kaup Jürri Traksiga nenda tehtud, et kui temma tökkutimetsa ei lähhe, siis Jürri temmale 20 Rubla, agga kui Metsa lähheb siis 25 Robla palka Aastas annab, ja et temma piddanud tökkuti metsa minnema, küssinud temma Jürri Traksi käest kas sa annad minnole 25 Robla palka, ja Jürri ei olle lubbanud, ja üttelnud, sa woid minno juurest ärra minna, ja temma tulnud siis ärra
Moistus
Maddis Pajo peab Jürri Traksi juure taggasi minnema teenima, ja keik teggema mis perre mees temmale töeks juhhadab ja sab omma tingitud palk 20 Robla Jürri käest, ja sab Jürri Traksi kaeptuse peale kui ta holeto on omma tenistusses koggukonnakohtu poolest trahwitud.
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
