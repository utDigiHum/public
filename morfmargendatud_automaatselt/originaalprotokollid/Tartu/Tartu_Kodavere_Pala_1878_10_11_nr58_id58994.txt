Ette tulli Karel Rättsep ja kaibas temal olla  ait Redi öösi sel 6 Octobril ärra lõhhutud ja säl 3 lukku rikkutu ning temal 11 Rubla 55 Kop raha üks lännik wõid 4 hõbbe sõrmust 1 nael suhkrut ja üks mark mis kanga wärwi ja linnast ride wastu wõtmise pärrast on  Kundra ride wasto annud. Wargad on aga ühhe wõtme kellega need kes se on tahtnud lahti tehha senna kes ta ette jätnud ja järgi otsimisse jures on wälja tulnud et se wõtti Bernart Adelsoni lesse Maali toa wõtti on olnud, sest nad on senna läinud Kohtumees Kusta Nukka  kaasas ollu ja seal on naine üttelnud, et temma wõtti olla ärra ja wiisit tema poisid Dawet Kukk ja Eduart Pruel olla need wargad.
 Ka olla perrenaine üttelnud et tema poisid on teine hommiku liikwa järel haisenud, ja Karel Rättsep ütlep et temal olla ½ klaasi liikwad ärra wõetud.
 Dawet Kukk wastas: tema ei olla sel nimetud öösel kuskil käinud waid koddo maganud ja ei tea ka wõtmeist middagi. 
    Eduard Pruel ütles wälja tema on sel öösel koddo maganud ja ka Dawet Kukk tema kõrwas aga muud nad ei tea aga need wõtmed olla kõik koddo.
 Amalie Adelson tunnistas, et tema wõtmed kõik käes olla ja kui kohtu mees küssimas on käinud, olla temal üks wõtti teises majas olnud ja olla paljalt mõtelnud, et tema wõtti ärra on aga nüd on tema omad  kätte suand ja leiab, et tema omadel on  keigel aukud  sees aga se mis Karl Rättseppa juurest on  leitu on ilma auguta ja ei olla mitte tema wõtti.
 Kohtu otsus: et ühtegi tunistust ei olle ja ka need wõtmed mitte ühte ei sünni siis wõib kahju   saja selgemad tunnistust otsi.
                               Pä Kohtumees: Gustaw Kokka
                                    Kohtumees: Gustaw Nukka
                                    Kohtumees: Karel Waddi.
