Prottokolli päält s.s. 28 Juni k.p. s.a No 148 alt tulli ette Johan Thom nink andis koggokonna kohtole ülles, et temma karja laps Natalie Alte paggenut temma mant ärra nink piawat parhilla Tarto linnan enda essa man, kes soldat om, ellama. Agga kelle maja seen, ei teadwat temma.
Otsus: Ütte Keiserliku 4. Kihhelkonna kohhut saab eige alandlikult pallelda sedda Natalie Altet Tarto linnast wälja koggokonna kohto ette saata.
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Abbi-Kohtomees Jaan Opmann XXX
