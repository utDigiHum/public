Jaan Kärro kaibap, et se perremees Jaan Taar tahtwat temmal ärra massa 3 rubla palka.
Jaan Taar wstp. selle kaibusse pääl, temma piddanut selle palkast 3 rubla kinni, et Jaan Kärro			wiitnut temma tenistussest ärra 11 päiwi			nowwap 2 rubla		2.,			masnut temma Jaan Kärro eest nekrutirahha			33 K.		3.,			jänut Jaan Kärro kaen üts liiwa kast, tolle eest nowwap temma			67 K.		Summa			3 rubla		
Jaan Kärro wastutap selle pääl, et 6 päiwi olnut temma aige ja 3 päiwi olnut temma perremehhe enda ajamisse pääl töö mant ärra, üllepäh 9 päiwi.
Nekrutirahha piddanut perremees essi massma. Liwa kastil saatnut perremees sedda Jaan Klaussenil perra, ning andnut temma se kast Jaan Klausseni kätte.
Moistus: Jaan Taar piap neid 3 rubla palk sel Jaan Kärrole ärra massma, agga wiidetu 9 päiwite eest piap Jaan Kärro 81 kop. Jaan Taril taggasi massma.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
