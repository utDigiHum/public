Jakob Uuk, Halliku Peedu N: 42 talu omanik on kohut palunud, oma wenna Karel Uukile ülesütelda, et peab 23, Aprillil 1893 a. tema käes rendi pääl seiswa kolmas osa talu maadest ja talumajadest
käest ära andma Jakob Uukile. Selle palwe täitmiseks sai ettekutsutud Karel Uuk ja temale kuulutud, et peab 23. Aprillil 1893 a. see tema käes olew Peedu talu N: 42 maadest kolmas osa Jakob Uukile ära andma.
            Karel Uuk
Otsus: seda, kui sündinud, ülestähentada.
             Kohtu eesistuja A Saar
             Kohtumees        J Rosenberg
             Kohtumees         J Hanst
             Kohtumees         J Reinman
             Kirjutaja              O. Seidenbach.  
                           
