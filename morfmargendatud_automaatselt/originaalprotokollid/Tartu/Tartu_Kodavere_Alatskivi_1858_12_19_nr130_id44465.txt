Toomas Tint kaibab: "Toomas Kalew olewa tema heina maa ära niitnud ilma lubata."
Toomas Kalew wastas: "ta sellepärast niitnud, et selle heina maaküla kaarti pääl oma talu numriga leidnud tähendud.
Sai kohtumeestest küla kaarti läbiwaadatud, leiti Kalewil õigus olewa ja mõisteti: Kalew wedagu niidetud heinad riio heina maa päält ära, andku aga se heina kuhi, mis sealsamas Tindi ema päralt, tagasi.
