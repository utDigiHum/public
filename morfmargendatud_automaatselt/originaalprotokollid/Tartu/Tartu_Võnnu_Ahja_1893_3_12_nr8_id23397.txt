Tuli ette Hendrik Sikkut Loorismetskülast Ahjalt Sikkuti talu omanik ning palus oma poja Kustale Jüripäewaks 1893 üles ütelda s.o. et Kusta Sikkuti talu mis praegu tema käes on, tema omanikule Hendrik Sikkutile Jüripäew 1893 a. kätte annab kõige kraamiga mis ona kätte saanud.
Hendrik Sikkut &lt;allkiri&gt;
Sellesamal korral sai ülesütlemine Kusta Sikkutile kuulutud.
Et Hendrik ja Jaan Sikkut selle kohtule tutawad on saab sellega tunnistud.
Eesistuja Jaan Lokko &lt;allkiri&gt;
kirjutaja J Puusepp &lt;allkiri&gt;
