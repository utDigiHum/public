Haslawa kogokonna kohus 21 Octobril 1881.
Juures istus:
Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
"   Johan Jantzen
Tuli ette Johan Seppa ja kaebas: Kui tema  Alexander Nõmitze käest Unnikülla weski maa on rentinud, siis ei ole see temale mitte kõiki hoonid, nimelt: Küün, laut ja tall kohe kätte andnud, waid 3 aastad pääle seda ning nõudis selle eest iga aasta 250 rubl. kahjutasumiseks ülepia 750 rubl.
2) on tema sinna oma kulloga üks lauust ait ehitanud, mis eest 17 rubl. 50 kop. nõuab;
3) on tema sinna roowidest ühe haganiko teinud, selle et õlgi ei ole kohegile panda olnud, nõuab 25 rubl.
4) on tema ühe lauust koa ehitanud nõuab 35 rubl.
5) on rendile andja temale iga aasta 100 wakka wilja ilma maksuta jahwatada lubanud, aga seda täitmata jätnud ning nõudis selle eest 150 rubl. ülepia 977 rubl. 50 kop. kaebatawa käest kahjutasumiseks.
 Alexander Nõmmitz ütles kaebtuse pääle wälja, et tema kõik hooned kontrahi pärra rendi aea alustusel rentnikule kätte on andnud; 2, 3 ja 4 punkt on kül õiged, aga tema ei ole neid hoonid sinna ehitada käsknud, ei ole ka rentnik seda temale üttelnud, et sinna midagi ehitada tahab, selle ei taha tema ka midagi kaibajale see eest maksa; 5 punkt ei sisa mitte kontrahis ja ei ole tema seda mitte Johan Seppale lubanud. Pääle see on rentnik wiimase poole aasta rendi 75 rubl. maksmata jätnud, mis tema on pidanud lainama ja mõisale ära saatma, mis tema protsendiga kaibaja käest kätte nõuab.
Mõistus:
Johan Seppa peab oma maksmata jäetud rent 75 rubl. 8 pääwa sees  Alexander Nõmmitze hääks ära maksma ja saab selle käest oma materialist üles ehitud hoonete eest 40 rubl. wälja maksetud, mis siis Nõmmitzela jääwad ja ei wõi Seppa enam midagi Alex. Nõmitze käest nõuda.
Pääkohtumees Mihkel Klaos  
Kohtumees Johan Link 
"   Johan Jantzen  
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi kõrgemat kohut tahab käija ja ei olnud mõlemad mitte sellega rahul.
