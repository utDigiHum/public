Kohus alustas kel  11. Tulli ette Piiri Kõrtsimees Josep Nõmm ja kaebas et Jüri Suits Kadrinalt on tema lauwa süil kaks auku korkinseriga puurinud ja siis weel tema naisel alwa sõimu  sõnad üttelnud nõudis 5 Rbl lauwa eest ja säduselik trahw sõimamise eest.
Sai  ette Kutsudu Jüri Suits kes wastas, et tema on küll sõimanud ja on ka selle lauwa sisse puurinud aga Juli Nõmm on teda peksnud.
Otsus: et tunnistajad Karel Sep ette tellida. Se otsus sai ette kuulutud.
                                        Kohtumees: Josep. Soiewa
                                        Kohtumees: Otto Mõisa
                                        Kohtumees: Mihkel Mölder
