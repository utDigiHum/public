№ 62
Haslawa Kogukonna Kohus sel 28 Mail 1890.
Man ollid: 
Kohtumees Jakob Saks
"  Peter Hansen
Kredit kassa kirja päle sest 26 Aprilist s.a № 392 said tännasel päiwal järrelseiswa tallo ärra kirjutedo ja sekwestri alla pantu Kreditkassa prozenti wõlla perrast: 
Leppiko tallo, kelle omanik Märt Toom om said ülles kirjutedo ja tema wallitsuse alla antu: 2 hobust, 7 karja ellajat, 10 lamast ja 18 ziga.
Märt Toom ei mõista kirjutada.
Karro tallo, kelle omanik Jaan Silm om, said ülles kirjutedo ja tema wallitsuse alla antu: 3 hobust, 10 karja ellajad, 20 lamast ja 15 ziga.
Jaan Silm /allkiri/
Otsus: Seda protokolli ülles panda.
Kohtumees:  Jakob Saks /allkiri/
"  Peter Hansen XXX
