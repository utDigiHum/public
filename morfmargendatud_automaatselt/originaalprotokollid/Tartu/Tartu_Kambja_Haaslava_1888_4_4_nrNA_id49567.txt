Sel 4 Aprilil 1888.
Kaibas  Peter Saag, kes   Tarto linan Anhofi ulitzen N 12 ellab, et  Jürri Janzen ollewat temale kolm kõrd päha lönu ja nõudis selle est 75 rubl selle et tema 2 nädalit päle selle haige ollnu.
 Jürri Janzen wastutas selle päle, et tema kaibajat lönu ei olle.
 Peter Saag andis tunistajas ülles  Jaan Kärik,  Jürri Kolk, Dawit Rüitli ja  Samo Buck, keik Wastsest Kustest, kes nännu kui tema lödu sanu.
Jaan Kärik Wastsest Kustest tunistas, et tema nännu kui Jürri Janzen 2 kõrd Peter Saagele Unikülla Kõrzin pähha lönu.
Jürri Janzen andis weel ülles, et Wastse Kuste mees Jaan Ollesk ollewat enne Peter Saage tõuganu selle et Saag õlle mannu tükkinu ja sis tõuganu kül tema ka Peter Saage; sel kõrral ollnu kõrzin Ado Kalpus, Jaan Tarrik ja Jaan Uibo Haslawast, kes nännu et tema lönu ei olle.
Ado Kalpus tunistas, et tema nännu ei olle kui Jürri Janzen seda Peter Saage lönu om, tema ollnu sel aijal kõrzin.
Otsus: et tunistaja ette on kutsuta.
Päkohtumees: Johan Link XXX
Kohtumees:  Jürri Kärn XXX
"  Jaan Hansen /allkiri/
