Sel 25 Julil 1883
Wassula kogukonna Liege Mihkel Akki om 2 Märtsi kuust sadik kassaka nink kohtumehe Johan Grossbergi läbbi mitto käsku sanu, et tema Päraha ärra massma ja sia passi toma peab ent Mihkel Akki om nüid ilda seda täitnu, selleperrast om tõine kirri Uellewatja kohtu poolt tullnu, mesläbi 2 Rubl tulleb massa.
Mõistetu: et Mihkel Akki se 2 Rubl Kihelkonna kohtu trahwi raha ärra massma peab ja om 15 witsa lögiga selle wastapanemise est trahwitu ehk massab 3 Rubl selle est waeste laeka heas trahwi.
Se mõistus sai kulutado ja õpetus leht wälja antu, tema olli rahul ja lubas 8 Augustil s.a. se raha ärra tua.
Päkohtumees: Jaan Link /allkiri/
Kohtumees:  Jaan Klaosen XXX
Peter Iwan /allkiri/
J. Krosbärk  /allkiri/
5 Septembril massis Jaan Silm 2 Rubl palkast wälja ja Mihkel Akki sai 15 witsa lööki selle et tema raha ei massa.
