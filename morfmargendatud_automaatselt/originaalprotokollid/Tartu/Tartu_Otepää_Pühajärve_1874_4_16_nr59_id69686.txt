Walla wannemb andi keikil perremehhel teada, et poisi ei töhi mitte ösel talloda mööda ümbre hulgata ja ei töhi pühapaiwal mitte ilma perremehhe lubbaga koddust wälja minna. Poisi, mis selle wasta teeb, massab 3 Rubl. trahwi ja perremees mes selle pääle perra ei waata - massab 6 Rubla trahwi.
Talgo teggeminne pühhapaiwal om keelatud.
Walla Wannemb M. Müller (allkiri)
Päkohtomees Johan Koiw (XXX)
Kohtomees Hans Willemson (XXX)
