Mötsawalitsus kaibap et se Johan Saggara warrastanud moisa mötsast 5 aawa puud, eggaüts olnut 4 tolli jämme, se teeb üllepäh 20 tolli lehhe puu wälja nink nowwap mõtsawallitsus egga ütte tolli eest 8 kop., ehk üllepäh 1 rubla 60 kop. hbd. trahwi mõtsa kassa hääs.
Johan Saggara wastutap, et se mõtsawalitsusse kaebus om eige nink pallub et mõttsawalitsus annnaks temmal se körd andis ja pakkub 1 rubla mõtsawalitsussel.
Moistus: Johan Saggara piap 8 päiwa seen warrastedu 20 tolli lehhe puu eest 1 rubla 60 kop. hõbd. mõtsa kassa hääs sisse massma.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud j. n. e.
