sel 18. Junil 1876.
koos ollid. 
peakohtomees Josep Kask
teine kohtomees Josep Laurisson
kolmas kohtomees Maddis Sildnik
Astus ette Jakob Raja ja andis koggokonna kohtule ülles et temma tahhab teistkorda abbielluse minna ning pallus omma essimesse Naise järrele jänod warrandust Protokolli ülles wõtta mis essimesse Naise lastele pärrida jäeb.
1 lehm wärt 20 Robl.
emma Kerst - " -
1 lammas - 2 -
riet ei olle ühtegi järrele jänod. sest et temma Naine kuus Aastad põddenud.
Andres Habakuk selle essimesse Naise wend. tunnistas et temma sõssaral ei olle muud kraami järrele jänud.
Moistus
Jakob Rajale saab üks tunnistus antud et siitpolt wasto ühtegi keelmist ei olle. et temma teist korda abbielluse lähheb.
Josep Kask XXX
Josep Laurisson
Maddis Sildnik
