Awwinorme Metswallakohtus sel 20 Novembril 1875.
koos ollid
peakohtumees Josep Kask
teine kohtumees Juhhan Koppel
kolmas kohtumees Jaan Mänd
Kadri Silberbachi kaebtuse peale et Gustaw Mattisen ei pea temmale se Kihhelkonna kohtu poolest moisted moona andma, sai Gustaw Mattisen ette kutsutud ning temmale teada antud, kui temma mitte se Aasta moon kahheksa pääwa sees Kadri Silberbachile wälja ei maksa, siis saab temma oksjon peetud ning temma warrandussest nenda palju müidud kui se moon rahha peale walja teeb.
Gustaw Mattisen wastas selle peale et temma mitte keige wähhemad Kadri Silberbachile ei anna sest et se ei olle temma kaup olnud; ning woib Koggokonna kohhus temmale küll oksjon tehha, mis ülle temma agga suurema kohtu kaebada tahhab.
Moistus
Koggokonnakohhus panneb kerriko samba üks kuulutus et sel 12 Decembril Gustaw Mattisenil oksjon saab peatud, ja saab temma warrandust nenda palju müidud, kui palju se Kadri Silberbachi moon ühhe Aasta jaggu rahha peale maksab.
Gustaw Mattisen pallus Protokolli wälja suurema kohtu minna, sest et temma sellega rahhul ei olle et temma peab Kadri Silberbachile maksma, ning temmale selle ülle Aksjon kuulutud.
Gustaw Mattisenile sai se pallutud Protokoll selle Liwlandi Tallurahwa säduse põhja peal Aastal 1860 §  keeldud. sest et se assi jo ükskord kohtu poolest moistetud, ja sai temmale agga üks Formalientäht selle kässuga antud. et temma peab Kihhelkonnakohhot palluma et nimmetud Keiserlik kohhus se kuulutud Oksjon kinni panneb muido saab agga se Oksjon ärra peatud.
Se lubbatud tunnistus sai Gustaw Mattisenile nenda kohhe N 291 all wälja antud.
Josep Kask XXX
Juhhan Koppel XXX
Jaan Mänd XXX
