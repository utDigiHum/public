Tawet Willemson andis üles et tema enam pitkemalt mitte Tomas Lind lese ja laste wöörmündri  ametid pidada ei wõi sest et  tema Jakob Willemsoni perijate juures selles ametis seisab ja edaspidi ka oma isawenna poja Willem Willemsoni wöörmündriks walitud saama saab, sest et keegi  muud selle tummaga asju toimetada ei oska. Otsus Tawet Willemsoni asemele ühte teist wöörmündrid Tomas Lind lese ja lastele walida. See otsus sai kuulutud.
                                                                               Kohtumees     J. Stamm.
                                                                              abikohtumees J. Tagoma.
                                                                              abikohtumees G  Stamm
                                                                              Kirjutaja mitteloetaw. 
