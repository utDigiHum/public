Sel 9 Detsembril 1885
Kaibas Jaan Klaos, et temal 45 Rub Jaan Hageli käest sada om ja pallus et Kohus seda massma sunis.
Jaan Hagel wastutas selle päle, et tema se 45 Rubl wõlgu om ent praegu massa ei jõua ja pallus temaga kannatada ja lubas 29 Septembril 1886 seda raha ärra massa.
Jaan Klaos lubas sis selle rahaga ni kaua kannatada.
Mõistetu: et Jaan Hagel sel 29 Septembril 1886 se 45 Rubl Jaan Klaosele ärra peab massma ni kui nemma lepinu om.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
J. Krosbärk /allkiri/
