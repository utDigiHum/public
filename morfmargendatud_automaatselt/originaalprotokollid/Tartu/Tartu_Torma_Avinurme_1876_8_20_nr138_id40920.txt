sel 20 Augustil 1876
koos ollid
peakohtomes Josep Kask
teine kohtomes Josep Laurisson
kolmas kohtomes Jaan Mänd
Jürri Raud kaebas et Marri Pern on ülle temma keelo temma kaewost wett wenod. kos hopis wähhe wett olnud ja kui temma kelamalähnd on Marri Perna tüttar Liiso Pern temma silmad lõhki tõmband ning Marri Pern temma padda ja Towri ärra wed.
Marri Pern räkis omma mehhe Hindrik Perna juures ollemissel et nendel kibbedast wett waja olnud ja kui temma lähnud Jürri Raua kawust wett toma on Jürri Raud temma pange katki lönod mis peale temma siis Jürri Raua padda ärra wõtnud ning ärra wija tahtnd. ja temma tüttar Liiso Pern appi tulnod. Jürri Raud on agga ühhe puuga temma tüttrele käe peale lönod nenda et selle kässi paisetand.
Liiso Pern räkis et Jürri Raud on temmale puga käe peale lönd nenda et se nenda haige et temma selle käega toed ei woi tehha. ja on temma siis Jürri Rauale silmad lõhki tombanud.
Jürri Raud kaebas weel et Marri Pern tedda hoora pälikust sõimanud.
Marri Pern tunnistas et se õige et temma küll wihhaga sedda wiisi sõimanod.
Moistus
Et se kaew Jürri Raua tallu kaew ja Hindrik Pernal ja temma perrel mingi suggust õigust ei olle sealt wäggise wett tuwa kui seal ... wed ei olle, Marri Pern agga issi tunnisatb et temma Jürri Raua padda ja towwri ärra pantinod ning tedda sõimanud ollema ja Liiso Pern tunnistab et temma Jürri Raua selmad lõhki tombanud siis moistab koggukonna kohhus et nemmad mollemad omma waggiwaltse toode eest kahhe pääwa ja öe peale trahwist sawad kinni pantud. pealeegi weel et Liiso Pern on kohto ees utlenud et kohto mehhed joomisse eest kohhut moistawad. ja peab Jürri Raud se katki lödud pang Hindrik Pernale ärra maksma.
Se moistus sai nende kohto käijatelle selle Liwlandi Tallu rahwa saduse pohja peal Aastal 1860 §§ 772 ja 773 kulud ning nendele Appellation keladod agga ühtlaise teada antud et nemmad selle moistuse ülle 8 pääwa sees woiwad Kihhelkonnakohto juures kaebtost tõsta.
Josep Kask XXX
Josep Laurisson XXX
Jaan Mand XXX
