Palla Mõisa wallitsus wasto Willem Peets: Et Willem Peets sel 16 Septembril mitte siin ei olnud sai tema kohtu otsus mõtsa warguse asjus  tema sel päwal tehtud ja ni kui al seisab ette loetud. Ka on Willem Peets mitme kordse kohtu kutsomise wastu pannud. Willem Peets wastas tema polle mitte ennam kui 3 kässu wasta pannud tema polle oma teenistuse päwi mitte wiita tahtnud.
Kohus Mõistis:
Willem Peets peab se säduselik trahw mis 1873 wäljaantud metsa tag nõuab se on 18 Rub 84 Kop ja kolme kordse kohtu kässu wasta pannek 2 Rubl. waeste ladiko heaks 8 pääwa sees wälja maksma. See mõistu sai kohtu käiattele appellationi seadusega ette loetud.
                            Peakohtumees Kustaw Kokka +++
                             Kohtumees      Kustaw Nukka +++
                              abi                    Thomas Welt  +++
                             Kog. Kirj.           Fucks.
