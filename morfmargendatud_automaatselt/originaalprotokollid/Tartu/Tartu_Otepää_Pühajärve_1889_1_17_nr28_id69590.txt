Peeter Kääriku asjas Karl Mõtuse wastu, kahju tasumise pärast, protokoll N141, olid Karl Mõtus ja Peeter Käärik tulnud. Neile said tunnistused ette leotud.
Mõistus: Et mitte awalikuks ei saa, et Peeter Kääriku õmblemise massin oleks Karl Mõtuse läbi katki tehtud, siis saab Peeter Kääriku kaebdus Karl Mõtuse wastu tühjaks mõistetud.
Mõistus kuulutati § 773 seletamisega. 
