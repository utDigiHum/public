Protokoll N: 206 pooleli jäänud asjus tulid ette Marret Lind Prits Karro eestkostmisel ja Hans Kogger Kawastust. Marret Lind kaebas et Hans Koggeri sulane wõtnud maja pääle
           1. 2½ wakka odre
           2. 1 wakk nisu
           3. 7 wakka kartolid
           4. 10 puuda heinu, mis Kogeri sulane Johan Plotnik hobustega ära söötnud.
           5. 4 rubla 60 kop. kiwide wedu raha, mis Johan Plotnik wallawalitsuse poolt wälja wõtnud.
           6. ja olla Johan Plotnik tema wankriga 7 korda käinud, kelle eest 5 rubla 10 kop. nõuab.
           7. ja olla Hans Kogger linad ära wiinud mis pidanud pooleks wõetud saama. 
 Hans Kogger saanud 5 kaalu ja jäänud temale keigest 1½ kaalu, nõuab siis 1 kael 15 leesikad linu. Palub Hans Kogri käest pkt. 1-4 sissenõuda kraam, kiwiwedu raha, wankrilõhkumise raha ja linade raha kohtu poolt.
Hans Kogger wastutas:
          ad 1., ei tea tema mitte, kas on sinna odre wõetud, palub Johan Plotnik üle kuulata.
          ad.2., niisama kui pkt. 1. 
          ad. 3., niisama
          ad. 4., niisama
          ad. 5.,  niisama
          ad.6., ei tea muud, et tema sulase käes olnud 2 kord wanker Kawastus käija, palub kohut Johan Plotnik üle kuulata.
          ad. 7., olla linad Kawastus 2½ kaalu haritud saanud, aga mitte 5 kaalu. Lina harijad olnud Otto Pärtin Kawastust ja Tawet Martinson Kawastust, kes tunnistada teadwad kui palju linu saanud. Marret Lind olla aga  wist rohkem linu saanud kui 1½ kaalu ja palub, et see tunnistuste läbi seda selgeks teeb, kui palju temal linu saanud. Marret Lind rääkis, et temal olnud linna harijad Peter Laasik Kawastust ja Kusta Lassi Kokkoralt, kes tunnistada wõiwad, et tema keigest 1½ kaalu linu saanud. Palub neid üle kuulata. Mõlemitel kohtukäijatel ei olnud tunnistuste ülekuulamise wasta kedagi rääkimist...
 Otsus: tulewaks kohtupääwaks Johan Plotnik,  Otto Pärtin, Tawet Martinson ja Peter Laasik Kawastust ja Kusta Lassi Kokkoralt ette kutsuda.
                     Peakohtumees: J. Sarwik.
                    Kohtumees:       W. Oksa.
                    Kohtumees:       K. Hawakiwi.
                    Kirjutaja O. Seidenbach. (allkiri)       
