Moisawalitsuse kaibuse pääl, et Ado Ottas ei massawat ütte ostedu rehhe eest 100 Rubla hbd. ärra tul ette Ado Ottas nink wastutas, et temmal parhilla mitte ei olle woimalik neit 50 Rubla höbd. , sis 50 Rubla om Ado Ottas jubba mötsawalitsusele ärra massnut, massa, agga nelja näddali seen lubbap Ado Ottas neit 50 Rubla ärra tassuda.
Moistus: Koggokonna kohhus annap Ado Ottassil, tämbatsest päiwast arwatu, weel 3 näddalit aigu mötsawalitsusel wõlg rahha 50 Rub. ärra massa, kui siis Ado Ottas ei olle massnut, saab temmale oksion tehtu!
Pähkohtomees: Johan Hansen XXX
Kohtomees: Jaan Purrik XXX
Abbi - Kohtomees: Jaan Wirro XXX
Moistus sai kuulutut nink suuremba kohto käimisse õppetusse kirri wälja andtu.
