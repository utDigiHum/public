1898 goda janvarja 29 dnja Pallaskoi Volostnoi Sud v sostav Predsedatelja Jakoba Reingolda i tšlenov Suda Gustawa Soo i Tenisa Oksa javilis jedinstvennõje zakonnõje naslednika umeršago Jakoba Turk, Mina
Martinson urožd. Turk, muž jeja Jozep Martinson, i Anna Reinok po pervojemu mužu Turk i zajavili, tšto poslutšaju  pereustupki ih sonasledništšeju Lizoju Tuxa prisuždennoi jei oprelenijem sego Suda ot 18 dekabrja 1897 goda polovinõ usadbõ Kauri N 2 imenija Gallik svojemu mužu Karlu Jurjevu Tuha oni otkazõvajutsja ot predostavlennogo im prava preimuštšestvennoi pokupki etoi nedvižmosti i sim izjavljajut položitelnoje soglasije na ukreplenije Jurjevskim Krepostnõm Otdelenijem za mužem Lizi Tuha- Karla Tuha usadbõ Kauri N 2 imenija Gallik.
Miina Martisoni eest: Josep Martinson Anna ja Mihkel Reinoki eest. Liza Tuha
Volostnoi Sud ubedivilis na to tšto,tšto dogovora vajuštšiesja litsa imejut zakonnuju pravosposobnosti k soveršeniju aktov, udostovrjajet podpisi Jozepa Martinsona i Mihelja Reinoka, za nego Lizi Tuha sobstvennorutšno podpisannõmi.
              Predsedatel Suda J Reingold
              Vol. Sudi: G. Soo
                                T. Oksa
               Pisar         KSepp.       
