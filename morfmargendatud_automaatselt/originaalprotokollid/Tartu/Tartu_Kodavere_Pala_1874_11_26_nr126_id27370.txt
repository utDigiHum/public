Pallal sel 26.Novembril 1874.
                Peakohtumeres:Paul Willemson.
                       Kohtumees:Josep Rosenberg.
            
                       Kohtumees:Michel Lallo;
Nikolai Efremow  Kallastel andis Palla peakohtomehhe läbbi se , et temmal ollewa Johan Awakiwwi käest 7 Rubla 40 Kop hõb. saada, Johan Awakiwwi tunnistus sedda õige ollewad ja lubbas sedda rahha Nikolail ärramaksa. Jaan Peterov Starikow kaebas;et temmal ollewa Johan Awakiwwi käest 8 Rubla h. saada, Johan Awakiwwi ütles wälja, et temma ollewa 4 wakka rukki jahho Iwan Starikowi Puudi mahha pannud ja Starikow piddanud  sedda jahho ärra müima, ehk ennesele wõtma, agga wakka hind lubbanud temma 2 rubla 75 Kop. Kui sedda jahhu müia saaks  summa tulleks rahha 11 Rubla ja temma wõtnud seäl poodist 2 paari weikesed saapad,hind olnud kokko 4 Rubla.ja weel wõtnud üks rangide täis wilti ja kaks seddolga rihma, nende rehnuti temma polle teädnud ja keik jäänud ni kaugeks õiendada, kunni se willi saab ärra müidud ja siis rehknut tehtud ja arwab weel, et temmal Starikowi käest:saada rahha on.
                                             Peakohtomees:Paul Willemson      +++
                                                    Kohtomees: Josep Rosenberg +++
                                                    Kohtomees: Michel Lallo           +++            
