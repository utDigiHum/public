Awwinorme Metswallakohtus sel 1° Novembril 1874.
koos ollid
peakohtumees Josep Kask
teine kohtumees Juhhan Koppel
kolmas kohtumees Maddis Sildnik
Sai ette kutsutud Kristjan Pajo ja temmale ette loetud et krono Metsa härra Gentz on koggokonnakohtule ülles annod, et temma peab Awwinorme Mõisametsast warrastanud ollema järrel nimmetud puud, mis trahwisäduse järrel peawad maksma.5 kuuse puud			3 sülla pealt			4 versok			3 Rubla			" kp		3			2			4			"			99		2			2			5			1			2		13 latti			3			3			3			51		15 latti			2			3			2			25		12 latti			1			2			"			36		Summa			11 Rubl.			13 kop		
Kristjan Pajo räkis, et tuul on temma heinamaa pealt puud maha murdnud ja murdu täis ajanod, ning on temma siis omma heinamaad puhhastanudning penikeist puid korjanud, kellest temma heina küini tehnud, ja on Mõisa wallitsus temmale ütlenud, et temma issi peab omma honete eest hoolt kandma, ja omma maasid ja heinamaid rookima.
Kohtumehhed ollid sedda heina küine watamas käinod. ja leidnud, et need palgid kellest Kristjan Pajo ommale heina kõini tehnud keik wannad ja mäddad puud. ja agga nelli 2 wersukast latti mis wärskemad puud on.
Moistus
Et keik need palgid ennamast mäddad ja wannad puud on ja sedda asja eimingil teel Metsawarguse nimme alla ei woi panna sest et Kristjan Pajo neid omma heinamaad puhhastadis on maast korjanud. Metsahärra agga neid ühhe selle koggokonnakohtule tundmatta põhja peal Metsawarguse nimme alla ülles wõtnud, siis moistab koggokonnakohhus, et Kristjan Pajo neist mäddapuudest mis muidogi olleksid maas ärra mäddanenud ja temma heinamaa peal risust olnud. on heina küinist tehnud, siis ei olle temmal selle ülle ühtegi süid egga trahwi maksa.
Se moistus sai selle Tallurahwa säduse põhja peal Aastal 1860. §§ 772 ja 773 nende kohtu käijatelle kuulutud, ning nendele se säduse järrel Appellation lubbatud.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
Se eesseisaw Protokoll sai sel 20 Junil 1875 ühhe Appellationtähhega N 167 all Metsahärra Gentzi kätte sadetud appellerimisse tarwis, kui Awwinorme Mõisawallitsus neid selle tarwis wollildab. muido ei olle nendel selle asjaga teggemist
Josep Kask
