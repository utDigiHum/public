Peeter Markus rätsepp Kawastult kaebab Mart Markus peale, ja nõuab selle kaest wõlg rahha 10. rubla
Mart Markus kostab selle peale, et temma olla Peeter Markuse kaest keigest 5. rubla laenanud ja maksnud temmale selle peale ühhel korral Tartu linnas 4. rubla ärra, kus juures Juhhan Saks Kastrest olnud, eddasi maksnud Kawasto Koosa kõrtsis 60. koppikad ärra ja olla weel 40. kopp maksa
Juhhan Saks, Kastrelt tunnistab, et temma on rahha maksmist Tartus nähnud, agga ei tea, kui palju nimmelt on maksnud.
Kawasto Koosa kõrtsis ärramakstud 60. koppikad wõttab Peter Markus wasto
Koggukonna kohtu liikmed ühhendasid endid selle 
Moistusele:
et kui Peeter Markus sedda tõeks ei joua tehha et temma Mart Markusele 10. rubla on laenanud, siis woib kohhus keigest se 5. rubla arwata, mis Mart Markus isse ütleb laenanud ollema, kellest 60. kopp mahha lähheb, ja peab siis Mart Markus 4. rubla 40. kp 8. paewa aea sees Peeter Markusele wäljamaksma
/: Otsus kulutud seaduse raamato §§ 772 ja 773 juhhatusel ja oppuse kirjad wäljaantud :/
Peeter Markus kuulutas sel 26. juunil c et temma otsusega rahhule ei olle, mis peale temmale kohhe appellatsioni kirri wäljaantud sai
A. Krimm [allkiri], pea
Maddis Erm, khtm
A. Pärn [allkiri], "Tawet Kook, "
