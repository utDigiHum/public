Awwinorme Metswallawallitsusse juures sel. 15 Junil 1878
koos ollid
Wallawannemb Juhhan Koppel
kohtomees Jaan Pukk
Astusid ette Padenorme külla perremehhed ja anniwad Wallawallitsussele ülles et nemmad on kronomaamõtja Härrada pallunud nendele krundid tehha, ja on siis kronomaamõtja Härra Telizin nendele krundid tehnud ja ka nendele uwwed piirid ärra näitanud mis nemmad tundwad
1). Josep Errapart N krundi perremees tunnistas et temma omma krundiga rahhul on. ja sedda uwwe piiride järrel omma kätte prukimisseks pallub sada.
Josep Errapart XXX
2). Jürri Errapart N krundi perremees tunnistas et temma omma krundiga rahhul on ja sedda uwwe piiride järrel omma kätte prukimisseks pallub sada.
Jürri Errapart XXX
3). Mart Laud N krundi perremees tunnistas et temma omma krundiga rahhul on. ja sedda uwwe piiride järrel omma kätte prukimisseks pallub sada.
Mart Laud XXX
4). Jürri Paas N krundi perremees tunnistas et temma omma krundiga rahhul on. ja sedda uwwe piiride järrel omma kätte prukimisseks pallub sada.
Jürri Paas XXX
5) Maddis Tamm N krundi perremees tunnistas et temma omma krundiga rahhul on ja pallub sedda uwwe piiride järrel omma kätte prukimisseks sada.
Maddis Tamm XXX
6). Jaan Kiwwi N krundi perremees tunnistas et temma omma krundiga rahhul on. ja pallub sedda uwwe piiride järrel omma kätte prukimisseks sada.
Jaan Kiwwi XXX
7). Mihkel Laud. N krundi perremees tunnistas et temma omma krundiga rahhul on. ja pallub sedda uwwe piiride jarrel omma kätte prukimisseks sada.
Mihkel Laud XXX
Et need eesnimmetud perremehhed omma käega kolm risti omma nimme juure on tehnud. saab Wallawallitsusse poolest tunnistud
Juhhan Koppel XXX
Jaan Pukk XXX
