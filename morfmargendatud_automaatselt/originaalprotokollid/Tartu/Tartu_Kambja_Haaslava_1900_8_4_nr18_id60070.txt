Protokoll № 18
Haaslawa wallakohus, Haaslawa wallamajas 4 Augustil 1900 aastal.
Juures: eesistuja Johan Koort.
Tuli ette Margus Laurits Mihkli poeg, Wana-Kuuste walla liige, elab praegu Haaslawa wallas Roio karja mõisas. Annab üles, et tema naene Leena Aprilli kuul sellel aastal ära on surnud ja kaks last maha jätnud, nimelt poeg Peeter 10 aast. ja tüt. Amalie 2 aastat wana.
Et temal nüüd nõuu on uuesti abielusse heita, sellepärast palub siin kohtu poolt ustawaks tunnistada, et ta oma lastele Peetrile ja Amaliele kummalegi 5 rbl päranduseks jätta lubab sellest warandusest, mis ta oma naesega kogunud.
Tunnistajana ettetulnud Hans Mugu Unipiha walla liige, elamas Haaslawa wallas, tunnistab, et Margus Lauritsel tõeste wähe warandust on ja rohkem lubada ei wõi.
M. Laurits /allkiri/
Ands Muggu /allkiri/
1900 aasta Augusti kuu 4 päewal on ülemal olew ülles andmine Haaslawa walla kohtu juures Margus Lauritse ja Hans Mugu poolt, kes mõlemad Kohtule isiklikult tuttawad on, oma käega alla kirjutatud, mida walla kohus selle läbi ustawaks tunnistab.
Eesistuja J. Koort /allkiri/
Kirjutaja J. Wäljaots /allkiri/
