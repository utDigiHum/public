Awwinorme Koggokonna kohtus sel. 13mal April 1868.
Kohto juures olliwad.
Wallawannemb Maddis Tamm.
peakohtomees Jakob Liis
teine kohtomees Juhhan Hallik
kolmas kohtomees Karel Raja
Selle Walla perremees Josep Kasik astus koggukonna kohto ette selle palwega et temma ei jõua sedda perret toita mis temma jaust nende tallo maade küllest mis N 11. järrel ja 4 Thaldrid ja 44 Grossi suured ülles pidda tulleb, ja et temma jo wõlgade sisse langenud. ning pallus et kui koggokonna kohhus sedda temma noorema wenna Jaani kätte annaks, kes sedda koormad lubbab omma peale wõtta, ja muist temma wõlgatest tassuda. 
Astus ette Jaan Kasik ja räkis et temma tahhab need tallo maasid omma wenna Josepi käest wasto wõtta, keige perre ja Maja kramiga ja ka muist temma wõlgasid tassuda aidata, ning peale kauba iggalt wäljalt temmale ühhe wakka alla põllu maad, ja kaks heinamaad anda ni kaua kui temma kui perre mees nende maade peal woib ellada. 
Josep Kasik lubbas omma wennale selle eest Aastas 5 Rubla renti ja pool teeteggo aidata tehha. 
Moistis
Koggukonna kohhus et Josep Kasik peab omma wenna Jani kätte keik Maja kraam ning nende noorema wendade ossa, miska temma sedda tallo omma kätte olli sanud terwelt taggasi andma ning et Josep Kasik muist Maggasi wõlga peab issi tassuma, mis ta sell ajal olli teinud kui temma perremees olli 
Mis tunnistawad kohtomehhed.
Jakob Liis XXX
Juhhan Hallik XXX
Karel Raja XXX
