Kaebas mõisahärra von Stryk, et mõisa kärner Karel Wuks on mõisast warratanud 11 tawlid klasi, 1 kot ernid. arwata 46 1 kot sibbulid, 1 suur tük kitti arwata 2 ja on ounu warrastanud ning on moonameestele kapsid annud ja pallubet se kärner warsti ammetist lahti sab ja et keik need assjad, miss siin on leitud mõisale jäwad.
Kärner Karel Wuks ütleb, et sedda klaasi keik temma Põltsamaald on tonud, need erned, sibbulad ja kitti on temma Põltsamaald tonud. Need erned on wa astased, need sibbulad on tännawa astasd se kitt on temma minnewalsüggisel teinud. Ning andis weel rehnungi üllesse, koss temma klaasi on kullutanud, agga temma üllesse andmine ei lähhe suggugi härra üllesse andmissega kokko ja sest klaasist pudub weel 11 taflid.
Wallawannem Adam Pukk tunnistab, et Karel Wuks on temma kuuldes rehnungid härrale üllesse andnud, agga se klaasi rehnung suggugi selle tännase rehnungiga kokko ei käi, se kitt ei olle mitte minnewal aastal tehtud, waid suutumast wärske kitt on ollnud, neederned arwab temma 1/3 wakk ollema.
Karel Wuks tunnistab isse, et temma need õunad, miss härra on leidnud, kül siit warrastanud on ja moonamehhe naisele on temma ka kapsid annud. 
Sai selle peäle mõistetud, et Karel Wuks selle wargusse perrast jallamaid mõisa tenistussest lahti sab, ja härra maksab kunni seie aijani temma palga wälja, need erned, sibbuladi jäeb mõisale ja selle wargusse est maksab Karel Wuks 6 rubl. hõb. waeste laeka trahwi. 
Peakohtumees Hindrik Lepp [allkiri]
Kohtumees Jürry Täht XXX
Kohtumees Rein Pukk XXX
