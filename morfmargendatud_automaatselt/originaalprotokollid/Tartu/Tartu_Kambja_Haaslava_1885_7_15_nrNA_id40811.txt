Haslawa Kogokonna Kohus sel 15 Julil 1885
Man ollid: päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
"  Peeter Iwan
Kaibas Jaan Kork, et Tanni perremees Johan Toom teda sel 10 Junil s.a tenistusest ilma aigu ärra om aijanu. Jaan Kork nõudis 14 Rubl ja 1 paar sabit waiwa palka ja selle asta tenitu aija est palka. Temma pidanu 45 Rubl kuni Märtipäiw s.a sama se om 7 kuu est, mis sis 64 kop kuu est wälja teeb ja pallus et kohus seda massma sunis.
Johan Toom wastutas, et tema Jaan Korkile keik mullu asta palk ärra om massnu ni kui tähe ette näidas, ja et Jaan Kork kauba perra need sabat tsuge assemel tenistusse ärra pidama pidas sis ei olles tsuge kullunu, ent Jaan Kork om tsugega läbbi sanu, sis ei olle temal ka sabit õigus nõuda.
Jaan Kork om essi tenistusest ärra lännu, tema ei olle seda poissi aijanu, selleperrast ei taha tema midagi selle tenitu aija est massa, neide kaub ollnu kuni Kadernapäiwani.
Jaan Kork andis ülles, et temmal tunistust wanna palka samise perrast ei olle.
Mõistetu: et Johan Toom minewa asta palkast 1 paar sabite est 2 Rubl ja selle käen ollewa asta ärra tenitu 1½ kuu aija est 963 Kop palka 8 päiwa sehen Korkile ärra massma peab, minewa asta palka nõudmine om tühjas mõista, selle et Jaan Korkil selle nõudmisse jures tunistust ei olle.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
Peter Iwan /allkiri/
