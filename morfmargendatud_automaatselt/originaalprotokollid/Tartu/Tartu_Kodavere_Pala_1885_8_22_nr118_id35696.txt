Protokolli peale 18 Juulist s. a. N: 86 . Ette kutsutud Ranna wallawanem Jaan Weskimets ja kohtumees Jakob Weskimets ja wastas küsimise peale et Jaan Weskimets haiguse perast wälja jäänud ja kohtumees tema asemel magasi mõetmise juures olnud.
Edasi küsitud ütlesiwad mõlemad et nemad seda puuwedamist Pala metsast Karl Reinomägi ja Johannes Punga läbi Märtsi kuul s. a. enam ei mäleta.
Josep Nõmm andis weel tunnistajaks Carl  Tuhling Ellistverest ülesse.
Otsus: Karl Reinomägi, Johannes Punga ja Carl Tuhling tulewa kohtupäääwaks 19 Septbrist ettetellida.
                    Pääkohtumees O. Kangro
                          Kohtumees J. Soiewa
                          Kohtumees O. Mõisa
                           Kirjutaja allkiri.
                                   
