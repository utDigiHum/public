Sel 13 Oktobril 1886
Kaibas Roijo Jürri Klaos, et tema pimarentnik Johan Pock tema jurest kesk ösel ärra om pagenu ja om sis temale 32 Rubl 80 Kop pima raha wõlgu jänu, ja om 20 pima kaussi ka ütten ärra wienu. Jürri Klaos nõudis tagasi 			Rubl			Kop		se pima raha			32			80		20 pima kauside est			4			40		kohtu kullo, sest tema om asta aigo sijn kohtu käinu			12					Summa			49 Rubl			20 Kop		
ja pallus et Kohus seda selle inemisele massa panes.
Se Johan Pock om mitto kõrd joba seja ette kutsutu ja nimelt tännases päiwas sele ähwartusega et kui temma ei tulle saab kohus ärra mõistedu, ent siski ei õlle se Juhan Pöck tänna Kirrepi mõisast tullnu ei ka hennast wabandanu, selleperrast sai
Mõistetu: et Johan Pock 			Rubl			Kop		pimaraha			32			80		kause est			4			40		kohtu käimisse kullu			7			-		Summa			44 Rubl			20 Kop		
8 päiwa sehen Jürri Klaosele massma peab.
Se mõistus sai Jürri Klaosele kulutedo ja olli sellega rahul.
Päkohtumees: Mihkli Iwan
Kohtumees:  Jürri Kärn XXX
"  Johan Kliim XXX
Kirja läbbi 13 Oktobris 1886 № 396 all 17 Nowember 1886
"  " 22 Detsember 1886 № 525 all 19 Januaris 1887
"  " 23 Webruar 1887 № 74 all 13 Aprilis 1887 
Kui ei tulle jääb mõistus kindmas ja es tulle Johan Pock mitte.
