Haslawa Kogukonna Kohus sel 25 Webruaril 1885.
Man olliwa: Päkohtumees: Jaan Link
Kohtumees: Johan Klausen
"  Johan Grossberg
Kaibas Jaan Sawik, et Lena Karjus, kes praegu Kagweren Jago tallun tenib, 2 nädalit päle Jürripäiwa 1884 astal hennast temmale tüdrukus kaubelnu ja teninu enegi üts nädal, lännu sis tema hobusega kastile perra sis ei olle kasti tonu enge satnu hobuse ülle wärsi sisse ja lännu essiki ärra. Kaibaja nõudis asta palk selle tüdruku käest. Asta est oli palka lubatu 26 Rubl, 1 Rubl käeraha, 2 punda linno, 3 nakla willo, 1 wakk Kartoflid maha panda ja tema rõiwas.
Lena Karjus Kagwerest wastutas Peter Klaose man ollen selle kaibuse päle, et temma asta päle hennast tenistusse  lönu ei olle, temma teninu üts nädal ja lännu hobusega omma Emma kaema sis ei olle tema enamb tenita sanu ja lännu ärra. 1 Rubl üttel kül sanu käerahas.
Päle selle lepisiwa nemma nenda, et Lena Karjus lubas 11 Märtsil se 5 Rubl Jaan Sawiko heas seija ärra massa.
Mõistetu: et Lena Karjus omma lubamisse perra se 5 Rubl 11 Märtsis s.a. seija sisse massma peab.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Peter Iwan /allkiri/
J. Krosbärk /allkiri/
Sel 18 Märtsil 1885 massis Lena Karjus se 5 Rubl Jaan Sawikule ärra.
