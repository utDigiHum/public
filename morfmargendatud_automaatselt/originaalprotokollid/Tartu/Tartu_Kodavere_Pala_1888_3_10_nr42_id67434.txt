Protokolli peale 25 Webruarist s.a. sai ettekutsutud Kusta Welt ja tunnistas et 26 Detsembri m. a. õhtu olla Jakob ja Karl Sallo Tuha õllepoodi ees Josep Tagoma kallale tunginud ja nimelt olla nemad mõlemad ütelnud sa kuradi tädiõhva poeg   saia kohtumees ma tahan sulle näidata, wiimati oli siiski Karl Sallo teiste keelu peale kuuldes tüli mahajätnud aga ähwardades nimelt Josep Tagoma peale, ärasõitnud.
 Ettekutsutud Jakob Welt ja andis küsimise peale üles niisama kui Kusta Welt muud kui lisas juure et ärasõites olla Karl ja Jakob Sallo weel ütelnud meie saame üks teine kord kokku see olnud Josep Tagoma kohta.
       Otsus tulewa korraks Karl ja Jakob Sallo ja Josep Tagoma tunnistuste kuulutuseks ettetarwitada.
                                                     Kohtumees J. Stamm
                                                     Kohtumees M. Piiri
                                                     Kirjutaja Carl Palm ( allkiri).
                                                
                                                   
