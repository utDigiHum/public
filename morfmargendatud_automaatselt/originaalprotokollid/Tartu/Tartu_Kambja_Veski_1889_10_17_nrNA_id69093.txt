    Sellsamal päewal 17 oktobril 1889.
Tuli ette Jakob Napp, Tähtwere wallamees, Saberna wallas elukoht, kaebab, et tema olnud ei ole Saberna kortsis turukubijas. Sääl saanud kõrtsimehe elutoast tema palito ärawarastatud ja nüüd saanud tema seda palitud ühe tundmata mehe käest, kes ennast Kusta Lina ütleb olewat. -
Kusta Lina kutsuti ette, aga tema oli nii täis joonud, et teda ülekuulata ei wõinud. -
                   Otsus: Kusta Lina targaks saades ülekuulata. -
