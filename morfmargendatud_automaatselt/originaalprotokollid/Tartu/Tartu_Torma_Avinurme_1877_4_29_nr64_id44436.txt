sel. 29. Aprilil 1877
koos ollid
peakohtomees Josep Laurisson
teine kohtomes Jaan Mänd
kolmas kohtomes Jürri Tomik
Tännasel pääwal leppis Ulwi külla perremees Maddis Kukk omma wõera emma Marri Kukkega nenda kokko.et temma omma woera emmale maad ja heinamaad annab ning nimmelt. eessarest kaks põldu taggast otsast krusaaugu juurest üks põld, teiselt wäljalt weike liwakolt kolm tükki ja keskmisselt teelt üks kitsas põld, kolmandamalt wäljalt sawwiku alluse  tükk kõrge lõik ja wõttikwerre poolt nurk. ja Pärtlimaa heinam. ja et siis Emma temma käest mitte moona ei nõua. Marri Kukk tunnistas omma Wöölmöndri Maddis Paasi juures ollemissel. et nemmad nenda kokkuleppinud. ja et se Ait ka temmale saab.
Moistus
Nende leppiminne on Protokolli ülles woetud et se kindlast jäeb.
Josep Laurisson XXX
Jaan Mänd XXX
Jürri Tomik XXX
