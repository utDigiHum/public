Sel 21 Augustil 1889.
Kaibas Jaag Paul, et Johan Laar wõtnu tema käest 2 wakka maad kartoflide tarwis, tema nõuab 15 rubl wakka maa pält, päle selle om se Johan Laar 34 päiwa terwe maja krami ja ellajadega tema jures ellanu ja karri käinu tema maa päl nink nimelt 7 Ellajat 6 lamast, 2 hobust ja 3 ziga, tema nõuab ega ellaja pält 25 kop ega 24 tunni est mis kokku 144 rubl 50 kop wälja teeb.
Johan Laar, kes Sarrakusten ellab, wastutas selle päle, et tema 2 wakka alla maad kartoflide tarwis om wõtnu ja nemma kaubelnu 375 Kop wakkama est, mis Jaag Paul essi massab ja et tema Ellajat käino 5 ehk 6 päiwa mõtsan hobustele sötnu tema heino nisama ka Ellajatele Kartofli maa kauba tegemise jures ei olle kedagi ollnu.
Jaag Paul ütles, et tema kartofli maa anmise jures midagi hinda märanu ei olle.
Karja mõtsan käimise perrast teadwat tunistada Daniel Põder ja Jaan Tors ja Edi Pruks.
Otsus: et Datwi Põder, Jaan Tors ja Edi Pruks ette om kutsuta.
Päkohtumees: Jaan Wirro XXX
Kohtumees:   Jaan Pruks  /allkiri/
"  Jakob Saks /allkiri/
