Kohtu ette tuliwad Kohtule isiliselt tuntud Ranna walla liikmed Jaan Mihkli poeg Nõmm oma naise Leenaga ning kolme tütrega Sohwie, Liisa, Anna selle palwega et kohus nende suusõnalist lepingut wõtaks kinnitada järgmiselt:
Wanem tütar Sohwie on oma teenitud rahaga ühe uue elu maja ehitanud, weikse aida ja sauna, ostnud, mis tema omanduseks tuleb mõista. Maja on niikaugel walmis et weel 60 rbl tarwis lähewad et päris walmis saaks. Seda lubawad kõik kolm tütart ühiselt kokku panna ja maja elamiseks walmis teha, ning ühes koos wanematega seal elada. Peaks aga kaks  nooremat õde tahtma ära minna, ehk wanem õde neid kord ära ajama, siis on wanem õde Sohwie Nõmm kohustatud kumbagi ära minijale 20 rbl maksma, nõnda et kokku nelikümmend rubla noorematele saab. Wanemate elu ajal peawad kõik seal majas elama, kellel wäljas teenistust ei ole. Selle maja ehituseks ja ostmiseks on Sohwie rahast 150 rbl ära tarwitatud.
See leping on ette luetud ja kirjutawad alla:
                                 Sohwi Nõmm
                                 Anna   Nõmm
Liisa Nõmm, Leena Nõmm Jaan Nõmm ei oska kirja nende palwe peale kirjutas alla:
                                                                                                                        Gustaw Martinson
Kohtumäärus: Sohwie Anna ja Liisa Jaani tütarde Nõmme lepingut ühetõise wahel 60 rbl ehituse raha üle Walla Kohtu Seaduse II jau § 278 põhjusel ustawaks tunnistada
             Kohtumõistjad: Peeter Holst
                                          Juhan Märtinson
                                          Gustaw Uuk
              Kirjutaja KSepp. 
