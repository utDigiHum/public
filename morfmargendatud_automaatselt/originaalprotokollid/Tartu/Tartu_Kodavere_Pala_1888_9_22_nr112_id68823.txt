Et wallakasak Toomas Waltmann wiimse aja sees mitte enam kogukonna kohtu käsusid inimestele andnud ei olnud ilma et temal mingisugust wabandust oleks siis sai Wallawanem palutud uut kasakad ametisse saada.
 Wallawanem Josep Soiewa leidis seda  õige küll olewat ja lubas uue kasaka ametisse sääda.
                                            Peakohtumees J. Sarwik
                                            Kohtumees      J. Stamm
                                           abi Kohtumees J. Tagoma
                                           Kirjutaja Carl Palm. (allkiri)
