1898 aasta septembrikuu 24 päewal kinnitatud Palla mõisa herra A. von Stryki ja Jakob Tagoma kirjalikult sisse antud järgmise sisuga mõisa maa meeste kontrakt.
                                                                                                                           1.
,, Palla mõisa walitsus annab Tosso N: III mõisa maa koha Jakob Tagoma kätte, Jüri päewast 1898 kuni Jüripäewani 1901, mille eest Jakob Tagoma mõisale 4 mehe jala päewa oma töönõudega igal nädalal teeb see on iga: Esmaspäew, teisipäew, kolmapäew, neljapäew see on aastas 208 päewa.
                                                                                                                            2.
Kõik hooned, kaewud, aijad, üks külwatud rükipõld ja üks künnetud ja äestatud rukki kõrre põld, igaüks  5 wakaalla suur ja 13 2/25  wakaalla hagudest puhastatud heinamaad peab Jakob Tagoma terwelt wastu wõtma ja niisamuti ka ära andma. Edasi kui protokoll N: 12.
                                                                                                                             3. ja 4.
                                                           
Niisama kui protokoll N:12.
                                                                                                                              5.
Jakob Tagoma saab aastas (2) kaks  6 jalalise sülda 3/4 arssina pikused põletamise puid ja 6 sülda walmis hagu, mis tema mõisa kasu pääle kas mõisa ehk wõerast metsast ise peab raiuma ja wedama.
                                                                                                                               6.
Niisama kui leping N: 12.
                                                                                                                                7. on maha kustutatud.
                                                                                                                                 8, 9, 10, 11, 12,13,14.
Need paragrahwid niisama kui leping N: 12. sisse kirjutus.
                                                                                                                                  Hoonete seisus:
1) Elumaja korstnaga õlest katuse all hea.
2) Laut ja õlestik õlest katuse all hea.
3) Ait õlest katuse all hea.
Aiad õue ümber ja kaew on korras. Kõik inged ja rauakraam on mõisa oma. Nii ka pliit ja ahju raud. Aida ukse raud ei ole mõisa oma.
Allkirjad.                                                                       Kohtu eesistuja: J Reinhold
                                                                                                   Kirjutaja: KSepp.                                   
