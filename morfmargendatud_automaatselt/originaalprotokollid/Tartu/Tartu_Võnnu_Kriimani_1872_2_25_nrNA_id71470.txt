Krimanni koggokonna kohtus 
Mõisawallitsus tulli ette ja kaebas et mõisa kantnik  Jaan Kutsar  on mõisale suwwel päewa wõlga jänud ja nüid on sel näddalil 3 päewa wõlga jänud ja ütleb Temma suggugi mõisa päewi teggema ei tulleb.
Jaan Kutsar  tulli ette ja ütles mind on ärra oksitud Magatsini wõlla perrast ja mul ei olle middagi süia ütten wõtta ja minna ei wõin mitte neid päiwi tühja kõttuga ärra tetta.
Sepeale ütles koggokonna kohhus et sul on lehm ja hobbune ärra oksitud agga mitte toido krami. 
Mõistus: Koggokonna Kohhus mõistis Tema kauba Contrakti perra ni kui Protocolli ramatusse on sisse kirjutud et Jaan Kutsar  peab igga päewa eest mõisale 50 kop hõb. maksma.
Kohto lauan olliwa
Peakohtomees Juh. Mina 
Kohtomees  Karel Põdderson 
"  Juhan Raswa 
