Haslawa kogokonna kohus 16 Decbr. 1881.
Juures istusid: Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
"   Johan Udel
Tuli ette  Peter Duberg ja andis üles, et tema poig nimega  Mihkel Duberg, kes tinawu aasta soldatis on antud, on 5 aasta eest oma hää jala päält põlwe ära murdnud ning palus, et tunnistajad Toomas Kahna, Jaan Wirro ja Joachim Rosenthal, kes seda toll kõrral on näinuwad üle kuulatud saaksid.
 Toomas Kahna  tunnistas: tema käinud Mihkel Dubergi kaemas ja olnud selle jalg päält põlwe ära murdunud olnud.
 Jaan Wirro ja Joachim Rosenthal tunnistasid nõndasama kui  Toomas Kahna.
Otsus:
Seda protokolli  Uehe Keiserliko Tartu Kreis Wäe teenistuse Kommissioni edespidiseks toimenduseks sisse saata.
Pääkohtumees Mihkel Klaos  XXX
Kohtumees Johan Link XXX
" Johan Udel  XXX
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi kõrgemat kohut tahab käija ja ei olnud Johan Prikko mitte sellega rahul. 
