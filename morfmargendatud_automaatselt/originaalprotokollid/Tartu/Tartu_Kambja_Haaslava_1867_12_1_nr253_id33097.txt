Kristjan Toom kaibap, et Johan Pappi hobbene söönut ja sökkunut kiik temma rüa orrasse ärra, temma wotnut hobbesse kinni. Kristjan Toom nowwap kahju tassomist ja pandistamisse rahha Johan Pappi käest.
Johan Papp wastutap, et temma hobbene olnut kül kinni Kristjan Toomi man, agga kost kurja pält olnut hobbene kinni woetu, ei tia temma.
Moistus: Kristjan Toom ei olle lasknut koggokonna kohtule kahju, mis temmal Johan Pappi hobbesse läbbi ollewat sündinut, ülekaia, ei woi ka kahjo tassomist nauda. Pandirahha piap Johan Papp 8 päiwa seen Kristjan Toomil ärra massma.
Pääkohtomees: Johan Hansen XXX
Kohtomees Ado Ottas XXX
Kohtomees: Jakob Jansen XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kiri wälja antdu.
