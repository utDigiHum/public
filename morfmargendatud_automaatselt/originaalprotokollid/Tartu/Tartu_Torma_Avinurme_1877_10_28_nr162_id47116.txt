Awwinorme Metswallakohtus sel. 28 Aktobril 1877.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jürri Tomik
kolmas kohtomes Josep Welt.
Selle walla innimenne Jaan Laurisson astus ette ja pallus koggokonnakohhut temmale selle ülle tunnistust anda. et temma poeg Josep Laurisson kes tännawo Aasta Negruti loosi all seisab jubba lapsest sadik ränga kuulmissega on. ning igga wannakuul temma kõrwad rängaste mädda jookswad. peale selle on temma poeg siis kui se 8 Aastad wanna olnud. hobbose seljast mahha kukkunud. ning omma pahhema käe liikmest ärra wännud. nenda et se kässi nüid kange ja liikmest liikuda ei anna. ja pallub temma selle pärrast koggokonnakohhut temmale tunnistust anda mis temma Ühhe Keiserlikku Tartu Negruti kommissionile woib ette näita et temma poeg neid wiggasid mitte issi ommale ei olle tehnud. ja woiwad sedda tunnistada Mihkel Mäggi, Mart Laurisson ja koolmeister Juhhan Schulbach et temma poeg jo weikesest sadik raske kuulmissega.
Mihkel Mäggi tunnistas et se õige et Josep Laurisson Jaani poeg rängaste kuuleb weikesest sadik
Mart Laurisson tunnistas ka et se õige et Josep Laurisson Jaani poeg heaste ei kuule ja et temma wahhel nenda ränga kuulmissega et peab kõwwaste räkima, ning siis temma kui loll on.
Koolmeister Juhhan Schulbach tunnistas et se pois on temma juures koolis kainud ja sellel jo lapsest sadik ränk kuulminne on.
Moistus
Koggokonnakohhus saab sedda Protokolli Josep Laurissonile selle juhhatamissega wälja andma et temma sedda Ühhe Keiserlikku Tartu Kreis Negruti Kommissionile woib ette naitata et temma need wiggad mitte issi ommale ei olle tehnod wait temmal need jo lapsest sadik ja temma santi terwisega on
Josep Laurisson XXX
Jürri Tomik XXX
Josep Welt. XXX
