Haslawa Kogukonna Kohus sel 16 Aprilil 1883
Man olid:
Päkohtumees: Jaan Link
Kohtumees:
Johan Klaosen
Peter Iwan
Selle  Haslawa Marguse Tallu Perrisostja Johan Henno  läsk  Mina Henn andis  Jaan Antiko man ollen ülles et temma seda Marguse maja enamb essi kui läsk edesi pidada ei jõua ja pallus et Kogukonna Kohus selle Maja renti päle annas ja seda modu et tema ka seal kõrwal ärra elleada saab. Selleperrast olli tänases päiwas  Mina Henn ja selle wölmünder  Jürri Woltri seija kutsutu et üts Rentikontraht saas tehtut. Jürri Woltri es olle tullnu.
 Jürri Peda  Wannast Kustest om selle Tallu läbbi käinu ja tahab sedasama renti, selleperrast sai järrelseisew Rendi Kontraht Kogukonna Kohtu ja Jürri Peda wahel tehtut.
1.
Jürri Peda Wanast Kustest wõttab selle Haslawa Marguse Tallu suur 20 Thaler kue asta päle renti päle se om 23 Aprilist 1883 kuni 20 Aprilini 1889 astani.
2.
Jürri Peda massab selle Tallu est 243 Rubl astas renti ja nimelt 170 Rubl ega asta 25 Märtsil ja 73 Rubl 25 Septembril ette. Päle selle massab Rentnik keik Walla, Kerriko ja Krono massu täitab tetegemise ja ki...(ei loe välja) käimisse, Tullekassa raha, Adra rahha ja Desatini raha massab Rentile andja ommalt poolt sest sisse wõetu renti rahast.
3.
Se läsk Mina Henn jääb sinna Tallu ellama, ja Rentnik anab selle pole renti selle läsjale ega asta 9 wakka Rükki, 6 wakka Keswi ja paneb temale 8 wakka kartoflid ega asta Nurme ja 1 wakk kartoflid aida sinna kos omma Kartofli panneb ja anab ka arida 4 pennart kapstamaad, peab lesjale suwel omma karjase ja karja seen üts lehm, 1 Lamas poigega ja 1 tsiga nisama toidab Rentnik selle lesja Lehma ja ütte Lamba omma põhhu ja heina peal üllen talwe sedawisi ni kui Rentnik omma Ellajäid ja lambit södab. Mina Henn jodab essi omma Lehma ja lamba.
4.
Ellokammer selle lesjale saab sellesama külle päl Rehhetarre kõrwale ehitado ja muidu Krami paneki tarwis jääb lesjale se edimine ait. Kütmisse nõu peab Mina Henn essi murretsema.
5.
Rentnik Jürri Peda peab selle tallu nurme 5 jaon pidama ja sis kui maja käst ärra annab peab nisama 14 wakka Rükki külwatu ollema kui praegu om. Wastsent maad wõib rentnik Wore mäe polt jure teha, kos mitte sured puud päl ei kaswa.
[6.]
Kui tullekahju juhtub peab Rentnik ehituse Material kas ommast pierist ehk pierist wäljast ilma massuta manu wedama, ent se põllenu hone ei tohi mitte suremb ehitadu sama kui wana olli ja Materiali wedamine ei tohi mitte kaugembalt kui 3 wersta tagant wedada ollema. Tullekassa rahhaga saab ehitus Material ostetu ja hone ehitadu ja kui sest rahast midagi ülle peas jäma saab rentnik se ülle jänu raha Materiali wedamise est hendale. Kui Hädalise parrandust ette tulleb peab Rentnik sedda omma kullu päl ärra tegema ent Material selle tarwis annab Rentileandja.
7.
Rentnik peab 7 Karja ellajat ja 2 hobust ülletalwe pidama ja ei tohhi õlgi heino ei ka põhku sest majast wälja müija ei ka lainata.
8.
Puid ja haggo peab Rentnik omma tarwituses essi ostma ja ei sa selle koha pält midagi. Heinama pält ja sealt kos hago põllule kahju teeb wõib Rentnik hagu raguda.
9. 
Kui Rentnik arwab et tema selle kohhaga läbbi ei tulle wõib tema 25 Septembril 1883 ülles üttelda ja sis Jürripäewal 1884 astal se koht ärra anda.
Selle kinnituses massis Jürri Peda 140 Rubl renti sisse ja lubas Jürripäiwal kui maja wastu om wõetu 30 Rubl massa.
Rentnik
Jürri Peda /allkiri/
Päkohtumees Jaan Link /allkiri/
Kohtumees: Johan Klaosen XXX
Peter Iwan /allkiri/
