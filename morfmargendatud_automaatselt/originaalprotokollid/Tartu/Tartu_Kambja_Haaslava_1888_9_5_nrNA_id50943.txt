Sel 5 Septembril 1888.
Selle otsuse päle sest 20 Junist s.a  Johan Toomi kaibuse asjan wasto Peter Utter olli kutsumise päle ette tullnu  Karl Luha ja tunistas et  Peter Utter ollewat 4 päiwa rohkemb tennu kui temal õigus tetta olli ja mõnne rehe pessmise jures jänu Peter Utter pudu ja mitte ni paljo kui  Johan Toom om kaibanu. 
Peter Madisson tunistas nisamma kui Karl Luha ja et tema Peter Utteri tarren keik suwi omma perrega om ellanu. 
Mõistetu: et Johan Toome nõudmisse kaibus wasto Peter Utter tühjas om mõista, selle et ni kui tunistaja wälja ütlesiwa,  Peter Utter  keik tingitud päiwat ärra om tennu ja selle et  Johan Toom tõise sullase Peter Utteri tuppa ellama pandno, ei wõi tema ka mitte keiki rehhe päiwi nõuda.
Päkohtumees: Johan Link XXX
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan  Hansen /allkiri/
