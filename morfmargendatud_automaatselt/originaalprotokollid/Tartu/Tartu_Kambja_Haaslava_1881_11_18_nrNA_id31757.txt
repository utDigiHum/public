Haslawa kogokonna kohus 18 Nowembril 1881.
Juures istus: Pääkohtumees  Mihkel Klaos
kohtumees Johan Udel
"   Johan Jantzen
" Rein Anko
Tuli ette  Karl Miina ja kaebas: Karjus  Johan Werno lasknud tsigadel tema kartohwlid ära tsongi, ning nõudis 5 rubl. kahjutasumiseks kaebatawa käest.
Johan Werno 16 aastad wana kaebtuse üle küsitud, salgab seda.
Hendrik Jaska tunnistas: Johan Werno tsigadest on paar tükki Lilo maa pääl Karl Miina kartohwlis olnud.
Kohtumees Johan Udel andis protokolli: Karl Miina kutsund teda kahju üle kaema, et temal Utte maa pääl tsiad kartohwlid ära on söönuwad ja on sääl arwata see kahju üks wakk kartohwlid suur olnud, aga seda ei ole Miina tiadnud temale üttelda, kelle tsiad sääl kartohwlis on käinuwad.
Mõistus:
Et Karl Miinal ühtegi tunnistust ei ole, et Johan Werno tsiad Utte maa pääl tema kartohwlis on käinuwad, siis ei wõi tema midagi Joh. Werno käest nõuda ei ka Lilo maa pääl sündinud kahju eest, selle, et tema seda mitte kohtumehel üle kaeda ei ole lasknud.
Pääkohtumees Mihkel Klaos  
Kohtumees Johan Udel 
"   Johan Jantzen  
" Rein Anko 
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna, kui keegi kõrgemat kohut tahab käija ja ei olnud Karl Miina mitte sellega rahul.
