Haslawa kogokonna kohus 12 Nowbr. 1880
Juures istus:
Pääkohtumees  Mihkel Klaos
kohtumees Johan Udel
dito  Rein Anko
Tuli ette  Jaan Post ja kaebas, et walla kooli ja kohtumaja 4 aastad ilma maksota tema maa pääl olnud ning nõudis selle eest 40 Rbl.
Kohtumees  Johan Link walla wanema Jüri Luha asemel ütles wälja, et see õige on ning lepsiwad sellega, et wald maksab 30 Rbl. selle ma eest Jaan Posti hääks.
Mihkel Klaos
Johan Udel
 Rein Anko
