Kohtu ette tuliwad Johan Lea oma naisega Leenaga sünd. Tamm Gustawi t. palwega, et neile tunnistus saaks antud selle tõenduseks et ostu kontrahti kinnituse juures kellegil tõisel õigust ei ole, kui aga tema pojal , kelle nimel Kontraht tehakse, ja palusiwad Elisabeth Kukke üle kuulata, kes teab kuidas kadunud ema tahtmine oli.
 Elisabeth Kukk teab. Kui Gustaw Tamme lesk Mai Tamm haige oli, ütles ta et wanema tütrele Liisale midagi päranduseks ei jäta sest et Liisa teda waatamagi ei tule. Ometi matuse päewal jagasiwad Liisa Tagoma ja Leena Lea- õeksed eneste wahel kõik ema riided ja kangad ära, tema, tunnistaja oli jagamise juures abiks. Elisapet Kukk.
  Kohus tõendab, et El. Kukk tunnistusel Liisa Taguma sünd. Tamm Gustawi tütar tema  ema wiimase seadluse järele Tagoma kohast mitte osanik ei ole, mis pärast Juhan Leale selle protokollist ära kiri wälja antakse, ühtlasi ka oma lubamise protokollist 22. Märtsist 1902. N.14. Kontrahti kinnitamise tarbeks Taguma N.5 poole koha jauks.
                      Esimees K. Reinhold
                     Kohtumõistjad  K Holst P. Juur
                                                  W. Tamming
                            Kirjutaja Sepp.
