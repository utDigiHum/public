Sel 3tal oktobril 1886.
                           Man oliwa. Päkohtomees M. Karru
                                           Kohtomees   J Aleksanderson
                                               -  -             J. Kulberg.
Paina Johan Tomeri, kongursi asja üle om sel tänatsel päiwal selle kohtu Otsus tettu, üle selle, sisse tulnud 160. rbl 31. kop. sel wiisil
Talurahwa säduse § 904. põhjuse järrele.
Waranduse wahide palk 20. päiwa eest a´50. kop.                               Suma,   10. rbl
Kohtu kullu,     "         "             "         "         "                                                "         3 - 68
Wimatse aasta sullase palk, 8. kuu eest                  -                                           30 -  -
Wimatse aasta seppa töö                                            -                                           4 -  -
Wimatse aasta kewäde pool renti, ilm samise õiguseta mõistetud, 
selle et omanik seda omal ajal ei ole mitte nõudnu, nink selle läbi
suure kahju ka kongkursi sisse tulekile om tennu, ja om kindla rendi
nõudmine 29tal nowembril                  1885.
weel essimist kõrda kohtule sisse antud., - " -
Sügise pool 1885.a renti omanikule                        -                             -               55-  63 kop.
Wiimatse aasta magasi wõlga tasumise                        -                                       50 - -
Johan Tomeri päraha wõlg                                 -                                -                   7 -     -
                                                                                                                       ----------------------------
                                                                                                             Suma             160. - 31.
                                               Päkohtomees Märt Karro  XXX
                                                Kohtomees  Jaan Aleksandersohn.  XXX
                                                Kohtomees Jakob Kulberg.   XXX
Jürri Anni ei ole selle otsusega rahul. Nimelt et kewade pool renti ei ole kätte mõistetud, palus prottokol wälja 14. päiwani edesi kaebus om tälle ka nimetetud
Päle selle weel wõlla kaotajide alla kirjotus, kos raha kätte sajad oma wõlla õigeste ülles andmise perrast, sawat wannute-
tus wai mitte.
Wana Pranglist Jaan Tomer 20. rbl sada, ei sowi wanutamist J. Tomer  XXX
Krotusest Mihkli Laine 2 1/2 rbl.  Sowip wannutamist.    M. Laine
Peter Steinbergil saada, 7. rbl raha 2. wak lina semend, 1. wak hernid, 3, rbl, 1. wak tatrik 2 1/2 ,10. rbl 1. wak keswi 2 1/2 rbl  Mihkel Numal                                                                      
