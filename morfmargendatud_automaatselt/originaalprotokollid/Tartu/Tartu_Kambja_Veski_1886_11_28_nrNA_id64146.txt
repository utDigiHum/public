Sel 28 Nowembril 1886
                        Man oliwa Päkohtomees M. Karru
                                          Kohtomees J. Aleksandersohn
                                                    -" -     J. Kulberg
Weskimõisa walast Peep Hall Johan Trooni rentnik kaebap et temal om nimetud pudusest mis üle tema oma kaubeldu wakkama hind Johan Trooni nõudmise perrä ei wõi wälja massa, ei ka üles üttelus 1887.a kewadel wälja minekin wastu wõtta
10. wakka maad nurme om ta kinni kelnu, selle eest ma nõwwan, a´ 7 1/2. rbl suma 75. rbl.
Tõiselt ma nõwan oma 6. aasta kontrati aeg weel 5. a edesi. Ja see mäggi mis arwata 6. wakkamaad  ei ole terwelt jänu een pral aastatel, ja üts tük soud om ta oma krundi külgi ajanu mes minu krundi külgi pidi jääma, arwata mõtsaga kokku
6. wakkamaad,
Tunistaja Johan Russak üttel, et Johan Troon käsk Peep Hallal see ma künda, (mes ta kinni keeletu üttel olewat) sel kõrral kui ta minu käest, raha laenas seda päiwa näitap wõlla täht, 
Tunistaja Jürri Märs üttel, et 2. nädalit enne Jaani käsk ta kül ühe tõise kohha päle mäele otsa poole aga sinnä nurme es luba ta suggugi künda.
