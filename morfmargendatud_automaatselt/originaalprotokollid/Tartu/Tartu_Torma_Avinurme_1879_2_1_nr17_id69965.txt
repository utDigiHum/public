Awwinorme Metskoggokonnakohtus sel 1 Febroaril 1879.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jürri Tomik
kolmas kohtomees Josep Welt
Kaebas Renthärra Kymenthal et nemmad on minnewa süggise Mõisa pobbolile Mihkel Aab kuus wakka maad kaero leigata annod; mis Mihkel Aab leikama piddanod, ning rõuku pannema, ja sanod temma selle leikuse eest 18 pääwa ja latti weddamisse eest 2 pääwa ühte kokko 20 pääwa, ning olnud se temma wastamisse peal et need rõugud peawad head saama. nüid talwel agga kui Mõisa sullased on neid kaero weddanod. on kaks üks pool rõuku nenda ärra mäddanenod ja läbbi külmanod olnod et need kaerad on piddanod kerwega lahti raijutod saama et nemmad selle alwa töe ja holetosse läbbi wägga suure kahju on sanod. siis nõuawad nemmad Mihkel Aabi käest 50 Rubla kahju tassu tassumist
Mihkel Aab tunnistas et se õige et temma on härrale 20 pääwa eest kuus wakkamaad kaero leikand ja rõuku pannud, ja on temma need rõugud küll omma teadmisse järrel korra parrast tehnod agga pikk wihmane süggise se willi ärra maddandanud.
Leppisid
Mihkel Aab teeb Härrale selle kahju eest 20 pääwa tööd, omma leiwaga.    
Josep Laurisson XXX
Jürri Tomik XXX
Josep Welt. XXX
