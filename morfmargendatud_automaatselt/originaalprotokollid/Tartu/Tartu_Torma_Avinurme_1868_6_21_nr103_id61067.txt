Awwinorme Koggo konna kohtus sel. 21el Junil 1868
Kohtu juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Jaan Pärn sai koggokonna kohto ette kutsutud ja küssitud, kuddas temma tohtis wimati ärrajoosta kui kohhus temmale 10 hopi witsu trahwiks moistnud olli, moistab koggokonna kohhus temmale selle eest 10 hopi witsu juure. ja sab 20 witsa lögiga trahwitud.
Mis tunnistawad kohtomehhed
Mart Jaggo. XXX
Mart Liis XXX
Mihkel Jallak XXX
