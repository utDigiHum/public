Protokoll N 5.
Jaan Andrese p. Orrina wolinik Karla Jaagu p. Särg müib ära Jüri Jaani p. Siimensonile ehi tare ja lauda, mis on WanaPrangli wallas Kitse talu maa peale eraldi ehitatud ja mis praegu on Märt Nussbergi käes tarwitada, summa kahesada (200) Rubla eest peris omaks.
Hooned saab Jüri Siimenson kätte 23 Aprillil 1914a. omale. Maa kohta jääb endine lepping makswaks, mis Jaan Orrina ja Ann Siimenson selle wallakohtu juures on teinud. 8 oktobr. 1909 aastal N8 all.
Raha saab ära maksetud kõik tänasel päewal Karl Särg on seda wastu wõtnud. 
17 April 1914a. 
Karl Jaagu poeg Särg [allkiri]
J Simenson [allkiri]
Need allkirjad saawad kinnitatud Prangli wallakohtu poolt. Mõlemad pooled on tuttawad selle walla kohtule
Sell 17 April 1914a.
Kohtu eesistuja: K. Pala [allkiri]
Kohtu liikmed: AKaru [allkiri] A. Potahow [allkiri]
