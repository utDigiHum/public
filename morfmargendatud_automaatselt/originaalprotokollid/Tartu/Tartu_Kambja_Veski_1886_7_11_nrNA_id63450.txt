Sel 11 juli 1886
    Man olid. Päkohtomees M Karru
                      Kohtom.     J Aleksanderson
                      -  "  -          J Kulberg
Pokka mölder Peter Eller kaebap, et Wastse Otepalt Adu Sisask, om mul 1/2 wak lina seemnid, 4. rbl wäärt, selle kot. 45. ja
1. tühi kot 50. kop. Suma 495. kop ärrä warastanu, kui ta weskele tulli oli linaseemne alale, ja kui ta ärrä oli lännu leidsi ma et seemne ärä oli, tõisi weskilisi üttegi temaga ühel ajal es ole kuni linaseemne pudus leidmisini, ja kui ma temale poole otsma lätsin, üttel ta edimalt et ta oli sell ööl päle weskil käiki Tannil käinut, perast kui ma selle koha järrele küssisin, siis üttel, et ta sääl ei ole kül käinud, üttel hendal tõiste kohadel käiwat, tema om minu seemne tõeste warastanud. -
I  Adu Sisask üttel, mina käise sel ööl Indu talul assi oli talitada Jaan Kuusega linaseemnede asja perast, kutsi teda otsma kui pudus om. Siin nägi ka Jaan Kuuse naine Mai, ma ütli et 6 wai 7. toopi üle jänu
II  Oru talul sõsare Leena Sisaski pool, särgi rõiwast otsman, seda ka es saa, se oli kesk öö aeg seda näiwa, Peter Ilwes, ja tema naine Mai Ilwes  Päle selle tullin kodu, seda juttu kuli nee molema
Tannil ei ole ma mitte käinu, seda wõlsi ütli ma et ma es ole perra mõttelnu, ma kül mõtli ka sinnä minekit.
