 Keisri Majesteedi käsu peale.                                                                                                                                          
1908 aastal januari  kuu 24 päewal waatas Pala wallakohus Jurjewi kreisis Liiwi kubermangus Pala wallamajas  awalikul koosistumisel, eesistuja K Reinholdi ja kohtu liikmete P. Juur, W. Tammingi,
K Holsti osawõtmisel ning kirjutaja E Brükkeli juuresolekul Kustav Elkeni ja tema poegade Josep ja Kaarel Elkeni palwe  Jiro talu pidamise lepingu ustavaks tunnistamise pärast läbi ja leidis, et need isikud on wallakohtule isiklikult tuttawad
2) Lepingu tegijatel on täieline seaduslik õiguse wõimus.
3) Nende poolt ettepandud akt on stempel- maksu seaduse põhjusel kirjutatud, ja nimelt 1 rub 50 kop. Stempel markidega maksetud ning ei tõuse oma hinna poolest mitte üle 300 rubla.
4) Pooled on akti sisuga täieste rahul ning on teda allakirjutanud.
Sellepärast sai 1889 a. Wallakohtu sead. II jau § 278 järele
                                    määratud:
poolt ettepandud kirjalik talupruugi leping ustawaks tunnistada ja wallakohtu akti raamatusse N: all 1....a. sissekanda, algupäraline akt aga tarwiliku päälkirjaga pooltele wälja anda.
                  Eesistuja: K. Reinhold
                   Kohtuliikmed: K Holst P Juur W. Tamming (allkirjad)
                   Kirjutaja: Brükkel.
