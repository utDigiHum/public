Krimanis sel 17mal Januaril 1869.
 Krimani  perrismaa pidaja  Juhhan Raswa tulli sel 17mal koggokonna kohto ette ja kaebas  Haslawa mehhe   Juhhan Lalli peale et temma minno pärris ostetud metsast 48 noort kõiwo puud warrastanud, mis Juhhan Lalli naene essi ülles antis kos kohtomees Jürri Bragli  ja  Haslawa kohtomees Jürri Käern kuuliwad ja näiwa er J. Lalli ree kesse jälle lumme peal olliwa.
Se peale sai sel üllemal nimmetud päewal  Juhhan Lall K. Koggokonna kohto ette kutsutud ja ta ütles: minna ei olle mitte Juhhan Raswa metsast need hakko tonu waid Haslawa wõssust, kui minna warrastanu ei olle siis ei olle mul ka middagi leppi.
Siis sai Peakohtumehhe  Jürri Bragli ette kutsotud ja ta tunnistas: kui meije need kõiwo tagga otsime siis üttel Johhan Lalli naene jätke nenda sammote kül me essige ärra ilma kohtota leppime, ja ne  Lalli reekesse jallase olliwa ni sammo laija kui lumme pealt mõõt wõttsime.
Mõistus Krimani koggokonna kohhus mõistis nenda kui kohtomees Jürri Bragli tunnistas et Johhan Bragli tunnistas et Johhan Lalli reekesse jälle nenda samma laija olliwa kelle ka ärra weetud need kõiwad ollid
II. nenda kuis Johhan Lalli naene essi ülles tunnistas et Joh. Raswa metsast need kõiwod warrasted ollid ning ilma kohtota ärra leppi tahtnuwad, peab Johhan Lall kahhe näddala perrast igga kõiwo tükki pealt 10 kopka maksma mis summa 4 rubla 80 kopka Johan Raswale ärra maksma.
Man olliwa:
Abbikohtomees Johan Mina XXX
"  "  A. Lenzius XXX
"  " Jaan Poedderson XXX
Kirjutaja W. Kogi /allkiri/
