Krimani Koggokonna kohtus sel 23. August 1868.
 Markusse tallo perremees  Mihkel Käerik tulli koggokonna kohto ette ja kaebas et Mango Käerik om Allasoos temma heina maa pealt wiis koormad aino tennu ning pealegi Markusse heina maa peale kuhja ülles tennu ja hainamaa puhtas niitnu ni et minno 4 innimest heina teggema saatsin, agga muud kui paijo puhma weel niita sanud sepärrast et Mango se parramb ja eddimesne hain endale ärra tennu.
Sepeale sai Mango Käerik ette kutsotud ja ta ütles: minna ollen heina tennu agga minna ollen Roijo Kotta heina maa pealt ja nemnda samma kost minna igga nukka pealt korjasin sedda wiisi ollen ma se kuhhi ülles pannud ja ei teanud et se Mihkel Käeriko ma peale se kuhhi saanu.
Mõistus:
Koggokonna kohhus mõistis et Mihkel Käerik ja Mango Kärik peawa need ärra tehtud heinad ehk se kuhhi haino pooles wõtma, sepärrast et Mango piddi enne ütlema ehk luppa küssima kui heino teggema läks kas kuhhi Mihkli maa peale tehja wõis
2) ei tea kust Mango need heinad ka weel endale korjanu olli.
Man olliwa:
Peakohtomees Jürri Bragli XXX
Kohtomees  Ado Lenzius XXX
Kohtomees  Jaan Poedderson XXX
Kirjutaja W. Kogi /allkiri/
