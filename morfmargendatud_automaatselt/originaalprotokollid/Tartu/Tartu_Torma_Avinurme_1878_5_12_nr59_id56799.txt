Awwinorme Metskoggokonnakohtos sel 12 Mail 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomes Jaan Mänd
Astus ette Jaan Särk ja pallus koggokonna kohhut et temma tahhab teistkorda abbi elluse minna ja on temma essimesse Naisel warrantust järrele jänud mis temma pojale Mart 3 1/2 Aastad wanna pärrida jäeb, ja on
			
Linnast riet						71 Arsinod		takkust kangast			33		1 kuwwe täis wattalt					2 Naisterahwa kube					3 willast selikod					4 linnast selikod					3 sitsi selikod					1 kävarini Selik					1 kallewine kuhwda					1 kawarini kuhwda					2 poomwillast kuhwd					3 paari kindaid					2 padja pööri					3 pallakad.					ja emma kerst					3 kätterättikud					2 tekki					
Selle ärra surnud Naise Anne Särki wõeras issa Mart Pomm tunnis et temma ärra surnud tüttrel need esnimmetod asjad on järrele jänud, mis temma pojale pärrida jäwad
Moistus
Et Jaan Särk on omma ärra surnud Naise järrele jänud warrandus Protokolli ülles kirjutanud mis temma essimesse Naise pojale pärrida jäeb. siis peawad Jaan Särk ja Mart Pomm selle eest holt kandma et need riided ärra ei mäddane ja woiwad need riided mis seista ei woi ärra müidud saada ning se rahha Wallakassa intressi peale pantud saama. ja saab Jaan Särkile tunnistus antud et temma woib teistkorda abbielluse minna
Josep Laurisson XXX
Andres Kiik XXX
Jaan Mänd XXX
