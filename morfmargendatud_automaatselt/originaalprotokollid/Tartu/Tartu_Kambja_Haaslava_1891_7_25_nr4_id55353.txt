Se protokoll om Haslawa wallakohtu aktist 1891 astast № 45 ärra kirjutedo.
№ 4.
14 Haslawa  kogokonna kohus sel 25 Juulil 1891.
Juures olid: Eesistuja Jaan Treijal
Kohtumees  Jaan Pruks
"  Peter Iwan
Logina talo omanik Johan Prikko palus selle praegutse  Logina talo  pidajale Johan Laar'ile kuulutada, et tema 23 apr. 1892 a. see Logina talo oma kätte tahab saada ja Johan Laar peab wälja minema, selle, et Johan Laar kauba perra ei massa, mis läbi ostmise kontrakt kinnitatud ei saa ja tema sele läbi kahju wõib saada.
Johan Prikko /allkiri/
See eenseiswa ülles ütlemine sai Johan Laar´ile kuulutatud.
Johan Laar /allkiri/
Otsus: Seda protokolli üles panda.
Eesistuja: Jaan Treijal /allkiri/
Kohtumees: Peter Iwan /allkiri/
"  Karli Anngo /allkiri/
Kirjutaja:   J. Weber /allkiri/
