Tänasel päewal tulid ette Jakob Pabor ja Kusta Pabor ja tegid oma wahel järgmise leppingu: poeg Kusta Pabor läheb Jüripäewal 1892 a. oma isamajast wälja ja saab sellsamal ajal 50 rbl. isa käest. peale selle aga kui isa sureb saab Kusta Pabor tema warandusest niisama osa nagu töisedgi pojad, kes majasse jääwad.
Sellega on mõlemad rahul.
Kusta Pabor &lt;allkirI
Jakob Paabor, aga et kirjutada ei mõista kirjutas alla: Mihkel Pärn &lt;allkiri&gt;
Et ülemalnimetud isikud selle kohtule tuntud on ja et need allkirjad oiges saab Wallakohtu poolt sellega tunnistud
Eesistuja: Jaan Lokko &lt;allkiri&gt;
Kirjutaja: J Puusepp &lt;allkiri&gt;
