Endrik Iwan kaibap, et perremees Johan Post ajanut nelja näddali eest teenistusest ärra, wasto talwe ei saawat temma Endrik Iwan ännamb töist teenistust. Endrik Iwan nowwap tollepärrast terwe aasta palk kätte, söök nink korter ja rõiwas perremehhe Johan Posti käest. Märt Üitsi om ka kuulnut kui perremees ärra ajanut.
Johan Post wastutap, et temma ei olle mitte Endrik Iwani teenistusest aijanut, Endrik Iwan om essi ärra paggenut. Johan Post nowwap ärra wiidetu päiwi eest kahju tassomist nink pallub et Koggokonna Kohhus Endrik Iwanit tolle est trahwis nink sunnis taggasi tullemas teenistusen, siis temmal om häddaste sullaset tarwis. Märt Üitsit ei wotta temma tunnistajast, siis Märt Üitsi om essi mittu körda temma teenistusest ärra paggenut ja temma ülle kaibanut, Märt Üitsi ollewat temma wihhamees.
Moistus: Koggokonna kohhus arwap ka et Märt Üitsi om Johan Posti wihhamees, siis Märt Üitsi om mittu korda Johan Posti pääl kaibanut. Endrik Iwan ei woi selgest tehha, et pm äraajanut piap warsti taggasi minnema teenima nink saab Jürri päiwan kiik enda palk kätte.
Peakohtomees: Johan Hansen XXX
Kohtomees Ado Ottas XXX
Kohtomees: Jaan Purrik XXX
Moistus sai kuulutut nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
