Se mõtsawaht Michel Klaus kaibap, et selle perremehhe Johann Thomil 12 suuret ellajat olnuwat noore kassu mõtsa pääl, temma ei olle jaudnut neid kinni wotta ja se karja tütrik ajanut neid kaira pääl ja ei olle lasknut kinni wõtta, temma om agga karja tütriku kaest rätt ärra wotnut ja nowwap egga ütte ellaja päält 1 rubla kahjotassomist.
Johan Thom wastutap tolle kaibusse pääl, kui se karja tüttrik om temma ellajat lasknut railme pääl minna siis wastutago temma essi tolle kurja eest, Temma ei tiadwat mitte keddagi tost.
Karjatütrik Ann Astawerre wastutap Johan Thomi eestkostmisen, et mitte üks lehm ei olle railme pääl olnut, temma saisnut tee pääl railme ette ja sial wotnut Mihel Klaus temma rätti ärra.
Moistus: Michel Klaus ei woi selgest tehha, et selle Johan Thomi karri keeletu noore kassu mõtsa pääl olnud, ei woi ka sellepärrast kahjotassomiseks keddagi nauda nink piap rahhule jääma ja sedda rätti mis temma tüttriku käest tee pääl ärra wotnut, warsti kätte andma.
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
