№ 1
Gaslavski volostnoi sud Jurjevskago ujezda Lifljandskoi gub. v Gaslavskom volostnom dome 9 Ijulja 1910 goda.
V sostave: predsedatelja V. Pederson
Tšlenov suda: Jana Rozental i Karla Ratasep pri pissare Jan Veljaots.
Javilis: krestjanin Gaslavskoi volosti Jan Juganov Genn, proživajuštši v Gaslavskoi volosti, s odnoi storonõ i krestjanka Staro-Kustskoi volosti Mina Juganova Tehvand pri muže i sovetnike svoim Jak Peepov Tehvand, proživajuštšije v Staro-Kustskoi volosti, s drugoi storonõ, i prosjat zapissat sledujuštšeje dopolnenije k razdelnomu aktu, zakljutšjonnomu v sim sude 1 Marta 1902 g. za N 2.
 Mina Tehvand s mužem-sovetnikom svoim Jakom Tehvand zajavljaju:
" Mõ polutšili uže polnostju vsju summu v razmere tšetõrjoh sot /400/ rub; kotoruju Jan Juganov Genn bõl objazan platit Mine Tehvand soglasno § III nasledorazdelnago akta ot 1 Marta 1902 g. za № 2, a potomu ja, Mina Tehvand, nikakih pretenzi k Janu Genn bolše ne imeju."
Mina Tehvand, a za negramotnosti i po litšnoi o tom prosbe jeja rospissalsja jeja muž i sovetnik:
Jaan Peep P Tehwand /allkiri/
Jaan Enn Juhan Poe /allkiri/
Znatšit po russki: "Jak Pepov Tehvand" i "Jan Juganov Genn"
Perevodil: pissar J, Veljaots /allkiri/
1910 goda Ijulja 9 dnja Gaslavski vol. sud udostoverjajet, tšto nastojaštši akt slovesno zajavlen semu sudu litšno izvestnõmi semu sudu krestjaninami Janom Juganovõm Genn, Minoi Pepovaju Tehvand i mužem-sovetnikom jeja Jakom Peepovõm Tehvand, imejuštšimi na to zakonnuju pravospossobnost, i podpissan temi že litsami, za negramotnostju Minõ Tehvand rospissalsja muž-sovetnik jeja.
Gaslava, 9 Ijulja 1910 g.
Predsedatel Suda: V. Pederson /allkiri/
Tšlenõ Suda:
J. Rosenthal /allkiri/
K, Rattasepp /allkiri/
Pissar J. Veljaots /allkiri/
