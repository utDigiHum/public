Tämbatsel päiwal sai selle mõtsawalitsusse kaibusse pääl kangeste sel Kondi tallo perremehhe Kusta Wassila ütteldu, et temma piap illuste enda tallo hoonet parrandama ja kiik joonen säädma, ent kui temma sedda ei tee, saab temma säädusse perra trahwitus.
Otsus: Saap prottokolli üllespantu nink Kusta Wassilale ette loetu.
Pääkohtomees Johan Toom XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Kärn XXX
