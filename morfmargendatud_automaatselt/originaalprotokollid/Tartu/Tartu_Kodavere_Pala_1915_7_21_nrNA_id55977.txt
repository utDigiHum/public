1915 aastal Juuli kuu 21 päewal waatas Pala wallakohus I Jurjewi Ülema Talurahwakohtu ringkonnas Pala wallamajas awalikul koosistumisel, eesistuja as.  J Wadi ja kohtu liikmete Elken, Kirik osawõtmisel ning kirjutaja Brükkeli juures olekul ettepandud päranduse jagamise lepingu ustawaks tunnistamise pärast läbi ja leidis, et lepingu tegijad on wallakohtule isiklikult tuttawad.
              2) Lepingu tegijatel on täieline seaduslik õiguse wõimus.
              3) Nende poolt ette pandud akt on stempelmaksu seaduse põhjusel kirjutatud, ja nimelt 9 rub. 40 kop. stempelpaberi pääl.
              4) Pooled on akti sisuga täieste rahul ning on teda allakirjutanud. Sellepärast sai 1889 a. wallakohtu sead. § 278 jao järele määratud:
                   Jakob Uuki pärijate poolt ettepandud kirjalik päranduse jagamise leping ustawaks tunnistada ja wallakohtu akti raamatusse N: 5  all 1915 a. sissekanda, algusakt aga tarwiliku pealkirjaga                       pooltele wälja anda.
                   Eesistuja J Wadi.
                   Kohtu liikmed K Elken W Kirik. 
                   Kirjutaja Brjukkel.
