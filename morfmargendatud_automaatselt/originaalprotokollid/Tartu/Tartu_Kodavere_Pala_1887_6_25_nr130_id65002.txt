Protokolli peale 19 Webruarist s. a. N:45 sai Josep Hawakiwi tunnistus et tema kedagi sellest asjast ei tea kohtukäiatele awaltud ei olnud Karl Kubjal enam kedagi tunnistust juure anda.
     Otsus: Karl Kubja maksab oma käekirja põhjusel Willem Perametsale 6 wakka 9 karn. odre 1  Oktobrini 1887. ära. See otsus sai kuulutud ei olnud kellegil selle wastu midagi ütelda. 
                                        Peakohtumees J. Sarwik
                                        Kohtumees       J. Stamm
                                         Kohtumees      J. Tagoma
                                         Kirjutaja G Palm.   
