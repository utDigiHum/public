Kohus oli tänasel pääwal Lallo b N. 14 taluase läinud praeguse peremehe kadunud Toomas Lind lese Maret Lindu käest  wanu walla  ja peremeeste maksu wõlgasid sissenõudma. Maret Lind oma wöörmündri Tawet Willemsoni eest kostmisel andis üles, et temal mitte raha ei olla ja pani kohus selle pealle 30 rbl. 98 kop. eest keelu alla.
                                1 siga- 8 rbl.
                               1 hall lehm
                               1 punane päitspea lehm
                               1 must päitspea lehm.
Otsus: Kuulutused   kiriku sambasse wälja sääda.
                                 Kohtumees           J. Stamm.
                                 Kohtumees           M. Piiri.
                                 abi Kohtumees    J. Tagoma
                                Kirjutaja G Palm. (allkiri)
