Haslawa Kogukonna Kohus sel 5 Augustil 1885.
Man ollid: päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
"  Johan Grossberg
"  Jaan Pruks
"  Peter Luha
Selle otsuse päle sest 29 Julist s.a Ropka Kogukonna Saadikude kaibuse asjan wastu Johan Jännes es olle Johan Jännes kutsumisse päle tullnu enge om sõnna satnu, et tema tulla ei sa selle et temma lapsed haige peawa ollema ja temma Tarto Linna tohtre jurde sõitma peab. Seda wabandust ei wõtta kohus mitte kui säduse peraline wastu, selle et tema kaits kõrd enne seda ilma wabandamada tullemada om jänu ja Kogukonna tendrile J. Sawikole üttelnu ollewat: " Lass nemma kolm kõrd ärra käiwa sis minna hennast ennegi üts kõrd näita", kost nätta om, et Johan Jännes melega aigu wieta püijab, selle perrast sai tännasel päiwal 
Mõistetu: et Johan Jännes 4 nädali sehen se kaibajadest nõutaw summa 1626 Rubl 3 Kop ja selle raha est 5 prozenti asta pält intressi ennegi kapitali est kaibajadest nimitedo Patenti põhja pääl 9 Aprilist 1879 astast sadik, konnas se kaibus Ropka Kogokonna Kohtu ees om tõstetu, ni kui Ropka kogukonna kohtu protokolli ramat wälja näitab, kunni selle summa wälja massmisse päiwani Ropka Kogukonnale wälja massma peab, selle et:
edimalt, need kaibuse punkti 1, 2, 3, 4, 5 ja 6 all seiswat Uellembin Kohtin ärra selletado ja need pudused leutu om ja ka sien läbbi kaetu Ropka Kogokonna nöri ramatude perra need puduset õige om leutu,
tõiselt kaibuse punkti 7, 8 ja 9 all nõutawa summat om ette todu kirrjutado päraha listi, läbbi kaetu Ropka päraha nöri ramatu, perra arwamisse ja ramatu läbbi kaemisse perra need puduset ja nõudmiset õige leutu,
kolmandamalt om Johan Jännes kui sekõrdne Ropka Wallawanemb keik sisse tulleki wastu wõtnu ja wälja mineki massnu sis ka need Kogukonna kahju ja Johan Jännese kassus wälja kirjutusse läbbi tõusnu nõudmised ütsinda Johan Jännese kassus om sanu ja nellantamalt Johan Jännes essi Keiserliko 2se Tarto Kihelkonna Kohtu een, ni kui sellen aktin seisab, Ropka Kogukonnale keik wälja massa lubanu kui temale ärra om selletado, et tema ütte ehk tõise wisi peal kogukonnale selle wallawanemba ameti pidamisse aijal kahju om tennu, konnas temmale nüid mitme kohtu läbbi se kahjutegemine ärra selletado om sanu ja kost temma essi, kui üts rehnungi tundja inemine, arwu sanu saab ollema, ent seda kahju just melega Ropka Kogukonnale ärra massa ei taha enge kaibust tõsta lasseb ja sellega aigo woeta püijab. Akti wäljastamise kullu ja käimise kullu nõudmine om tühjas mõista.
Se mõistus sai sadikule Anus Lukase kulutado ja õpetus leht wälja antu . Sel 12 Augustil 1885 sai se mõistus Johan Jännesele kulutado ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
"  Peter Iwan /allkiri/
"  Johan Krosbärk /allkiri/
"  Peter Luhha /allkiri/
"  Jaan Pruks /allkiri/
