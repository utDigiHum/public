№ 7.
2 Jurjewi Ülema Talurahwa Kohtu ringkonna 14 Wallakohus  Haslawa wallamajas 14 Junil 1896.
Juures oli Kohtu Eesistuja: Johan Hansen.
 Haslawa  walla mees  Kristjan Järkewits ja  Wana Prangli  walla liige lesk  Lena Hesse, kes Haslawal elab tulid ette ja palusid järgmest protokolli panda.
 Kristjan Järkewits annab üles et tema Lena Hesse wõlgneb üks sada seitsekümmend rubla /170 rbl/ millest Detsembri kuul 1896 a. seisekümmend rbl /70/ ja Detsembri kuul 1896 a. seisekümmend rbl /70/ ja Detsembri kuul 1897 a. üks sada /100/ rbl. lubab ära maksa.
 Lena Hesse on sellega rahul.
 Lena Esse /allkiri/
 K. Järgewits /allkiri/
Eelseiswad lepingu tegijad on selle wallakohtul palelikult tuntud inimesed, kellel seaduse järele lepingu tegemise õigus on ja et mõlemad oma käega on alla kirjutanud, saab selle wallakohto poolt tõeks tunnistatud.
Kohtu Eesistuja: J. Hansen /allkiri/
Kirjutaja: K. Laar /allkiri/
/Pitser/
