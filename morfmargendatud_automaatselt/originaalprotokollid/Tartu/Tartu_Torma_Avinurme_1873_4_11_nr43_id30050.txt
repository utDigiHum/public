Awwinorme Mets Koggokonna kohtus sel 11 Aprillil 1873.
Koos ollid pea kohtomees Maddis Tamm.
teine kohtomees Josep Reisenpuk
walla wöölmönder Mart Tomik.
Astus ette pilleti peal soltat Karel Kakkason ja pallus koggokonna kohhut, et temmal ommal lihhalikku lapsi ei olle, siis on temma Josep Pukki poega nimmega Juhhan Pukk kes nüid 14 Aastat wanna ja jo 3 aastat temma jures olnud ommale kasso pojast wõtnud, ja pallub temma sedda asja Protokolli ülles wõtta, et se Juhhan Pukk temma warra pärriast ja üksikust pojast jäeb.
Josep Pukk selle poisi Issa tunnistas et temma ommalt poolt se lubba annab et temma poeg Karel Kakkasonile kassu pojast jäeb
Moistus
Et Karel Kakkason on Juhhan Pukki ommale kasso pojast wõtnud ning selle poisi issa Josep Pukk ka sedda lubba annab, ning ka wollimehhed ommalt poolt sedda lubba annud, siis on koggokonna kohhus selle seaduse §§ 952 pohja peal Karel Kakkasoni palwet kuulda wõtnud,ning se assi Protokolli pannud, et se assi kindlast jäeb.
Maddis Tamm XXX
Josep Reisenpuk XXX
Mart Tomik XXX
