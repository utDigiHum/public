Tulli ette se Endrik Schnikker ja kaibas et ne sullase Hans Märten ja Johan Saggara söötnuwat hobbestega temma nissu akki ärra. Endrik Schnikker nowwap kahjotassomist 2 wak nissu ehk 8 rubla hbdt.
Siis olnut poistel käsk tee pääl kruusa weddata, nemma läinuwat agga ilma lubbata tüü mant ärra ning weddanuwat tolle Lillo Udelil linna temma hobbessega, tolle eest nowwap Endrik Schnikker 1 rubla.
Johan Saggara wastutap selle kaibusse pääl, et temma pandnut perremehhe enda kässu pääl hobbesse halwa kablaga kinni, hobbene tõmbanut sedda kapla katski ja läinut siis wast nissu akki man. Se perremees Endrik Schnikker üttelnut weel:"Kui Kabbel ei pia, egga si siis sinno süüd ei olle!"
Siis olnut neide perremees se Lillo Udelil päiwi wõlgo ja nemma teinuwat linna weddamissega päiwi tassa.
Tulli ette se Lillo Udel ja üttel, et temmal olnut kül 2 päiwi Endrik Schnikkeri kaest sada ning piddanut se Endrik Schnikker temma linnat katte hobbestega likku weddama ei olle agga sedda täitnut. Tollepääl lasknut siis temma pääle kruusa weddamist õddaku 2 koorma linnah Schnikkeri hobbestega koddu weddata.
Otsus: Saab prottokolli ülles pantu. Hans Märten saab agga koggokonna kohto ette tallitedus.
Kohtomees Jürri Luhha XXX
Abbi-Kohtomees Jaan Opmann XXX
Abbi-Kohtomees Märt Labber XXX
