Tulli ette Miina Puusep eestkostjaga ja kaebas et 11 juunil  olla Toomas Welti koer tema poega Willem kiskunud ja nimelt oli tema parem käsi kõwaste werd jooksnud. Tunnistajaks andis Liisa Lall Johani naine. Nõudis 25 rbl. walu ja rohitsemise kulu. Peale seda andis Miina Puusep  üles kihelkonna tohtri tunnistuse selle kohtu kätte 12 Juunist s.a. kos tunnistud oli et sellel Willem Puusepal  kuus haawa käe sees oliwad. Liisa Lall ei olnud käsku saanud.
      Otsus: Liisa Lall tulewaks korraks ettetarwitada.
                             Peakohtumees J. Sarwik
                             Kohtumees       J. Stamm
                              Kohtumees      M. Piiri
                               Kirjutaja Carl Palm. (allkiri)
