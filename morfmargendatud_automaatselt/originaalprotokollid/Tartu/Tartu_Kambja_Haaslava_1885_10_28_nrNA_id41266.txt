Haslawa Kogukonna Kohus sel 28 Oktobril 1885
Man ollid: päkohtumees Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
"  Johan Grossberg
Tännasel päiwal massis Johan Palla selle Jaan Kessale tema Essast jänu perranduse raha 61 Rubl ja surno sõssara Marri perranduse raha 18 Rubl koko 79 Rubl sijn kohtu een wälja.
Otsus: Seda protokolli ülles panda.
Päkohtumees: J. Link /allkiri/
Kohtumees  Johan Klaosen XXX
 Peter Iwan /allkiri/
 J. Krosbärk /allkiri/
Ollen 79 Rubl kätte sanu.
Jaan Kessa XXX
