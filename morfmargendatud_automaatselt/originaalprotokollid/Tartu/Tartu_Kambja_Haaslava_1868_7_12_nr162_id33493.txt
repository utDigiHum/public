Se mõtsawaht Alexander Labber kaibap, et se Ann Wuks põimanut kats körda railme päält hainat, mullemba körra wotnut temma tedda kinni nink nowwap se mõtsawaht tolle eest 1 rubla hõbbedat.
Ann Wuks wastutap Jaan Goldi eestkostmisen, et temma ei olle mitte rohkemn kui 1 körd railmel olnut, töine körd põimanut temma rüa weeren ja se mõtsawaht Alexander Labber wotnut tedda tee päält kinni.
Moistus: Se mõtsawaht Alesander Labber ei woi selgest tehha et se Ann Wuks 2 körda railme päält hainat põimanut, nink piap se Ann Wuks 50 kop. tolle ütte körra põimamisse eest 14 päiwa seen ärra masma.
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Abbi-Kohtomees Jaan Opmann XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu. Se mõtsawaht Alexander Labber es olle kohto moistmissega rahhul, prootokol es lubbata agga wälja andta selle et se assi mitte ülle 5 rubla wert ei olle.
bezaslt ?? 16 August 1868
