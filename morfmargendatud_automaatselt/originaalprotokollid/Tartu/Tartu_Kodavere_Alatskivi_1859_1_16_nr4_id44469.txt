Tartu linna mees Jüri Back kaibab: "Michailo Astafjew olewa Kolkja külas tema saani päält 30 ihesset, 2 haugi, 1 tek ja 2 rogost wägisi ärariisunud, ja teda 3 kõrd tõukanud, sellepärast, miks tema pole tema /Mihailo/ käest ka mitte kalu ostnud."
Seda tunnistab ka Makei Fedorow tõeks.
Kogokonna kohus pandis riidlejad lepima ja maksis wiimaks Astafjew Bachile 5 rubla trahwi.
