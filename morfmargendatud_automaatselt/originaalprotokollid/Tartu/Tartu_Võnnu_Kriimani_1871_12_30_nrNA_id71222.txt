Krimanni Koggokonna kohtus 30mal Decembr. 1871.
Roijo karjamõisa perrismapiddaja  Jürri Klaos tulli ette ja kaebas omma ööwaht Soldat  Juhan Wuksi peale se on minnul warrastati Jõulo keskmise pühha ösi tallist hobbune ärra ja nõuan et Öwaht peab selle ärra warrastatud hobbuse eest mulle 90 Rubla maksma.
Soldat  Juhan Wuks  tulli ette ja ütles: se hobbune warrastati õhto kella 6 aego ärra ja minnul olli nenda kaup et ma siis wälja wahtima lähhen kui keik rahwas maggama lähwad ja se ei olle minno süidi et se hobbune õhto olli ärra warrasstud kui ma mitte weel wahhi peal ei olnud.
Jürri Klaos tulli ette ja ütles: minnul olli nenda kaup et kui pimmedast lähheb et siis õkwa wahtima lähheb ja selle tarwis ollin ma ka temmale kassuka  annud et wõib külmaga wahtida ja minno perre wõib keik tunnistata kuis meie kaup olli tehtud.
Soldat Juhan Wuks  tulli ette ja ütles: Klaosel on wägga laggunud ja ilma uksedeta honed ja ütlen koggokonna kohto een et minna ennamb Klaose tenistusse ei jäen ja pallun et ma Temast wallale saan.
Jürri Klaos tulli ette ja ütles: kui Ta mind ennamb tenida ei tahhab siis wõib Ta ka minno polest se lubba sada et wõib ärra minda agga enda hobbuse nõuan ma ikka Temma käest.
Soldat Juhan Wuks tulli 7 Januaril ette ja ütles et minna ei ollen mitte Klaosele Öwahhist kaubelnud ent ollin Weski poisist kaubelnud ja minnole ei putunud se Öwahtiminee middagi ja kaeban weel et Klaos olli mind Rehhe jures peksnud kus Kusta Mina Jaan Mina man olliwad.
Jürri Klaos tulli 7mal Januaril ette ja ütles se ei olle mitte tõssi et ollen Tedda lönud ent minna ollin agga omma Kassuka Temma pea alt ärra wõtnud kui Ta minno rehhes maggas nink ütlesin kui sa mind tenida ei tahha siis ei pea sinna ka mitte siin ennamb asset piddama.
Tunnistaja Jaan Tabbun tulli ette ja ütles et Juhan Wuks olli kül Klaosega nenda kaup teinud et Ööd wahhib ja keik teeb mis Klaos kässib ehk mis ammet ette tulleb ja se õhto kui hobbune ärra warrastati siis ütlesin minna Juhan Wuksile et Klaos käskis sind tänna ösi illusti wäljas perra kaeda sest et Ta isse koddun ei olle.
Tunnistaja Jaan Mina tulli ette ja ütles et Juhan Wuks ütles isse minnule et temma on Öwahhist kaubelnud ja Klaos ei olle Tedda mitte rehhe man lönud ent Ta wõttis kassuka Juhan Wuksi käest ärra mis Ta Öwahtimisse perrast temmale selga olli andnud ja Klaos ütles egga ma omma kassukad ei wõi sinno kätte jätta kui sa minno mant ärra lähhed.
Kusta Mina tulli ette 12 Januaril ja tunnistas just nenda sammoti kui Jaan Mina.
Marri Utte tulli ette ja ütles: nende kaupa ei olnud ma mitte kuulnud agga sedda ma kül näggin et Juhan Wuks iks öse wäljas wahhi peal käis.
Mõistus: 12 Januaril 1872.
Koggokonna kohhus arwas se hobbune wäert ollewad 60 Rubla nink mõistis siis se kahjo polest nenda et Juhan Wuks peab siis 30 Rubla Klaosele maksma seperrast et Ta se öwahhi amet olli enda peale wõtnud nink omma holetusse läbbi se hobbune ärra warrastata lasknud.
Kohto lauan olliwad:
Peakohtomees Juhan Mina 
Kohtomees:  Karel Põdderson 
Abbi "  Jaan Põdderson 
Jürri Klaos ja Juhan Wuks  ei olnud kumpki selle mõistusega rahhul nink Juhan Wuks olli selsammal päewal se lubba täht sanud et wõib suremat kohhut nõuda.
