Haslawa Kogukonna Kohus sel 18 Junil 1884.
Man ollid:
Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
Peter Iwan
Kaibas Johan Wuks, et Molliko maja Perremees Peter Duberg, kelle maa pääl temal üts majakenne ollon, ei olle teda seal majan ellada lasknu, om keik maa ligi seda maja ülles kündnu, maja taguse wee augu kinni aijanu, ei olle teed wälja käija andno, päle selle warsti om se maja ösel põllema pantu, ni et tema omma naesega palja amega wälja sanu. Selle et Peter Duberg teda jobba minewa sügisse ähwartanu:" mina sõkku selle maja sisse ehk põlleta ärra" arwab tema, et Duberg essi selle maja põllema om pandno ehk panda om lasknu. 100 Rubl est ollewat temmal Krami sisse põllenu ja maja om 20 Rubl masson, seda kahju nõuab tema Peter Dubergi käest.
Peter Duberg wastutas selle päle, et tema Johan Wuksi sääl majan ellada ei olle keldnu, om kül maa ja krawi ümbre selle maja ülles kündno, om jalla tee wälja jätnu ja et tema Johan Wuksi ähwartanu ei olle, ja ei tea sest maja põllemisest midagi.
Johan Wuks  andis tunistajas ülles Johan Opman ja Ado Einman, kes teadwad, et Peter Duberg teda ähwartanu om.
Otsus: et Johan Opman ja Ado Einman ette kutsuta om.
Päkohtumees J. Link /allkiri/
Kohtumees  Johan Klaosen XXX
"   Peter Iwan /allkiri/
Kirjutaja: J. Weber /allkiri/
