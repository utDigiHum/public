                                            Sel 14 webr. 1886.
                                            Man oliwa Päkohtomees M. Karru
                                                               Kohtomees J. Aleksanderson
                                                                  -  "  -        J. Kulberg.
Weskimõisa walitseja Jakob Inti kaebap et Wahi kantnik, Jürri Hall, ja Jaak Kuiw om üttelnuwat et mina olen nende orjatud teo päiwat ärä wõtnu, ja neid Johan Rehale andnud, keda tema Tartun käimisen prukinud. Mina ei ole seda mitte teinud waid oma raha eest olen ma Johan Rehat Tartus käimisen prukinud, ja nende päewad olen ma ka omma teada kõik kätte andnud.
Jürri Hall üttel et tema ei ole seda mitte üttelnud üttelegi inemisele, et need 7. päiwa mes mul pudus orjusest om Johan Rehale antu
Jaak Kuiw üttel, et 4. päiwa om mul kül walitsaja ärä wõtnu, ag seda ma ei ole kellekile üttelnu et need Rehale om antu.
Johan Reha üttel, et walitseja om kül minu Tartu käimise tarwis pruukinu, ja selle eest ollen ma sanud, selge raha mitte orjuse teo päiwi 
1 Tunistaja Tanni Aak üttel et mina olen seda kül kuulut et Jürri Hallal ja Jaak Kuiwal om walitseja neil teo päiwi ärä wõetu, Aga et tema neid Rehale om andnu ei tea mina, ja ei ole ka kellegi käest seda juttu kuulu. -
2. Tunistaja Peter Jantson üttel et seda ta om kül kulut et walitseja om Jürri Hallal 7. päiwa ja Jaak Kuiwal 3 1/2 päiwa ärä wõetut, ja Jaak Kuiwa naine Anna üttel, et need sawat siis Johan Rehale antus  Ja walitsejale olen ma ka kül seda teadus-
tanud, et tema üle särane jutt om wälja tettu. -
Sel 2. mail 86. een olnu.
Jaak Sootla kaebusen wastu Peter Waherit tunistaja Kaarli Jõgi üttel, et Jaak Sootla ei ole mitte rohgemb kui arwata 10. samu, Peter Waherile perrä astnu üttelden, mine kodu, kas täl midagi käen oli, es pane ma sugugi tagasi, selle kõrraltsel koon olekin üttel ta et kül Keisu poisi kül sure ommawa, aga ma pane neid maha, 6. tükki wõin ma maha panna, ja 7tamaga lähän weel kokku, Albust ei ole sel kõrral sääl kumaltgi poolt olnu. -
