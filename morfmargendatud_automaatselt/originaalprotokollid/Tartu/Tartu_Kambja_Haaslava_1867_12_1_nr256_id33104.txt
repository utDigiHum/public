Jaan Sawwik tul ette nink andis koggokonna kohtule ülles, et temma mötsast om warrastedu:1,			3 peddajat			5 tolli jämme			se teeb wälja 15 tolli		2,			1 peddaja			6 tolli jämme			se teeb wälja 6 tolli		3,			2 peddajat			7 tolli jämme			se teeb wälja 14 tolli		4,			1 peddaja			8 tolli jämme			se teeb wälja 8 tolli		5,			1 peddaja			9 tolli jämme			se teeb wälja 9 tolli		Summa			52 tolli		
Nuit saanud temma selle Wanna Kuuste Reddeli tallo sullasse Jaan Kisteri käest kuulda, et se Reddeli tallo perremees Johan Kord ollewat si warras, temma läint kül Reddeli tallon enda puud otsima ei olle agga keddagi löidnut. Jaan Sawwik nowwap säädusse perra kahju tassomist.
Se Wanna Kuuste perremees Johan Kord tul ette ja wastutas tolle kaibusse pääl, et temma ei olle kunnagi warastanud ei olle ka nuit Jaan Sawwiku mõtsast puud warrastanud. Johan Kord pallub et koggokonna kohhus tolle teotamisse eest Jaan Sawwiku trahwis.
Otsus: Koggokonna kohhus lähhap kandusit mõtsan ülle mõõtma. Jaan Kister Wannast Kuustest saab koggokonna kohto ette tallitedus. Michel Klaus ja Suhki Jaan, kes ka kuulnuwat kui Jaan Kister tolle wargussest Jaan Sawwikul kõnnelenut, saawat ka koggokonna kohto ette tallitedus.
Pääkohtomees: Johan Hansen XXX
Kohtomees Ado Ottas XXX
Kohtomees: Jakob Jansen XXX
