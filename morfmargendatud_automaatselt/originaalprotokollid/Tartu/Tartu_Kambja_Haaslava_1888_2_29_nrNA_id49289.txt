Sel 29 Webruaril 1888.
Man ollid: päkohtumees Johan Link
Kohtumees: Jaan Opman
"  Johan Kliim
Kaibas Antsi tallo ostja Johan Hansen, et Johan Kessa tema heina maad mööda  Järwe käib ja nõuab selle maa est kos Johan Kessa majakenne seisab ja aijamaad saanu 18 rubl asta pält renti ehk 5 päiwa tarre asseme est.
Johan Kessa es lubba ni paljo selle tee est ja maja platzi est massa enge lubas 10 rubl selle est ega asta massa.
Mõistetu: et Johan Kessa selle aija platzi est, kus tema saun seisabja selle aija maa est, mis tema käen om, 15 rubl ega asta  Johan Hansenile peab massma nink nimelt ega asta se rahha  Jürripäiwal ette, sis jääb Johan Kessal tee ütten Jürri Salloga järwe ja ka wälja käija. Kui Johan Kessa seda teed järwe ei prugi nisama ka aijamaad ei prugi sis om temal 5 päiwa ega asta heina aigo Johan Hansenile sauna platzi est tetta.
Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja anto.
Päkohtumees Johan Link XXX
Kohtumees: Johan Opman XXX
"  Johan Kliim XXX
