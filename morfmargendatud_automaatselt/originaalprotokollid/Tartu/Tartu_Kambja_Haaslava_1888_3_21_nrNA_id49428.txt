Sel 21 Märzil 1888.
Kaibas  Peter Hansen, et minewa kewade wõttnu  Anna Kroman 3 wakka seemne kaero ja lubanu neide est 180 Kp wakk ja pallus, et kohus seda massma sunis.
 Anna Kroman wastutas mehe Jaan Kromani man ollen selle päle, et tema lubbanu kaero tagasi massa ja mitte 180 Cop wakkast.
Küssimise päle andis Peter Hansen ülles, et selle Kaera lainamise jures keda ei olle ollnu.
Anna Kroman lubas ½ wakka neide kaerade est odust massa.
Mõistetu: et  Anna Kroman neid 3½ wakka kaeraseemed 8 päiwa sehen Peter Hansenile ärra peab massma.
Se mõistus sai § 773 ja 774 perra kulutedo ja õpetusleht wälja antu.
Kohtumees:  Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
