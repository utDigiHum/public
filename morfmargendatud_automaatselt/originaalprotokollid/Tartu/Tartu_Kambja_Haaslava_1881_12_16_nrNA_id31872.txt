Haslawa kogokonna kohus 16 Decbr. 1881.
Juures istusid: Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
"   Johan Udel
Tuli ette  Kristo Grünberg ja kaebas: Haslawa mõisa kõrtsimees  Mihkel Rattassep on temal kõrtsis kasuka seljast ära riisunud ning nõuab tema käest seda kätte saada.
Kõrtsimees  Mihkel Rattassep kaebtuse üle küsitud, ütles wälja: kaibaja tulnud kõrtsi ja wõtnud ½ toopi wiina, aga ei ole raha maksa olnud, ning andnud siis kaibaja ise oma kasuka temale pandiks; pääle see ei ole kaibaja mitte raha ära toonud ei ka kasukale pärra tulnud.
Mõistus:
 Kristo Grünberg peab see ½ toobi wiina raha kõrtsimehe  Mihkel Rattassepale  ära maksma ja saab siis selle käest oma kasuka kätte.
Pääkohtumees Mihkel Klaos  
Kohtumees Johan Link 
" Johan Udel
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja olid kohtukäijad sellega rahul. 
