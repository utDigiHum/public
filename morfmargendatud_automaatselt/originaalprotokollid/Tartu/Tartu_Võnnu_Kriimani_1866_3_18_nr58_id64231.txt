Krimanni koggokonna kohtus 18 Märzil 1866.
 Jaan Müirsep  kaebas 11mal Märzil Krimanni koggokonna kohto et Haaslawa koggokonna mees Juhan Ers on temma haino kuhjast 5mal Märzil poolpäew ösi 2 koormad haino warrastanud.
Siis tulli 18mal Märzil Juhan Ers ette ja tunnistas et temma ei olle mitte sel ösil ommast majast wäljas kuskilgi käinud egga ka Jaan Müirsepa haino warrastanud siis tulliwad tunnistajad Haaslawa kohtomees Jaan Wirro ja kohtumehhe sullane Juhan Wirro kes sel korral neid jälgi Jaan Müirsepaga ollid kaemas käinuwad tunnistasiwad et need Juhan Erso reed on kül nende reejälgiga kokko passinuwad kellega hainad on warrastud, sest Juhan Erso 1sel reel olnud teise jallakse al wanna ratta rehw ja teise jallakse al kitsas kandiline raud ja 2 reggi olnud hopis laiem kui muud tallomeeste reed olnud ja nenda olnud ka jäljed maas teisel reel laiemad jäljed mis koggonist Juhan Erso reega on kokko passinuwad.
Krimanni kohtomees Jaan Aberg ja kohtomehhe sullane Jaan Mällo on ka käinud neid jälgi kaemas ja tunnistasiwad et Juhan Erso rega on need hainad warrastud sest need jäljed on tema reggedega üts olnud.
Siis tulli Jaan Müirsep ette ja nõudis omma hainte eest mis ärra warrasti 20 Rubla hõbbedat.
Mõistus: 18mal Märzil 1866.
Koggokonna kohhus mõistis nende üllemal seiswa nelja tunnistusmeeste läbbi et Juhan Ers hainawargas jäeb ja peab Jaan Müirsepale nende 2 koorma hainte eest 10 Rubla maksma.
Koggokonna Peakohtomees Josep Kerge XXX
"  "  Abbi "  "  Jaan Aberg 
Wöörmünder  Jaan  Põdderson 
Koggokonna kohto mõistmisega ei olnud mitte Jaan Müirsep egga Juhhan Ers rahhul ja nõudwad mõllemad suremad kohhut.
Juhhan Ers on protokol wälja wõtnud 20 Märzil 1866 a
M. Baumann /allkiri/
