Puurmannis sel 29 Nobr. 1874.
Koos ollid: Peakohtumees Hans Perre
kõrwalistja Tõnnu Bauer
teine Jurri Luhha.
Tulli ette wanna Põltsa ma mees Jaan Leppik ja kaibas siit walla mees Jaan Kipos olla suwwel tema käest hannisid ostnud ja jätnud 4 Rubl. 80 Kop. maks matta ning polle sedda mitme kordse pärrimise peale mitte ärra annud.
Jaan Kipus wastas, temma tahhab sedda rahha keige hiljem esmaspääw ärra maksa.
Kohtu otsus:
Jaan Kipus peab sedda rahha 4 Rub. 80 kop sia kohtu jure ehk kwitungi wasta se innimese kätte ärra maksma.
