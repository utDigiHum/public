  Palla Mõisamaamees Jakob Wända kaebas: Palla Pobboli Michel Konsa käest on temma se  saag, mis kahhe aasta eest temmal ärrawarrastatut kätte sanu.Ommanduse tunnistajaks pallus temma üllekuulata Otto Müürsepp ja Karel Waht. Michel Konsa ütles, et se temma saag on, mis tunnistada wõiwa Josep Tralla, Karel Rättsepp ja Jaan Sallo. Temma ostnu se saag  minnewa süggise 2 aasta
eest Kallaste podist.
Kae baja ütles, et temma saag  tullewa kewade kahhe aasta eest ärrawarrastud. Se saag olla 5 aastat wanna. Tunnistaja  Otto Müürsepp ütles, et se saag, mis temmal  siin kohto ees ette näitatu mitte se ei olle, kellega temma 4 aasta eest Jakob Wända jures tööd teinu, et sel sagil kirwe märk sälja peal süggwambale sisseläinu. /:wälja:/
Tunnistaja Karel Rättsepp räkis se saag, mis temmal kohhus siin näitab olla Michel Konsa omma, et sedda kahhe aasta Seppa metsas  Konsa käes näinu ja sekõrd koggonisti uus olnu. /: wälja:/
Tunnistaja Jaan Lallo räkis: Temma ostnu minnewa süggise 2 aasta eest taggasi Michel Konsaga se saag, mis temmal nüüd ettenäitatud sai Kallaste Konusetka poest ja maksnu nemma ka politi rahha selle eest wälja.
Karel Waht ja Josep Tralla ollit pudus. Jäi teises kõrras. Kaebaja wõttis omma kaebdus taggasi ja jäi saag wiimselt Konsa ommaks.
                            Peakohtomees:P. Willemson
                            Kohtomees: J. Rosenberg
                            Abbi Kohtomees: M. Lallo
                                                           Kirj. allkiri
