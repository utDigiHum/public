Tulli ette Margus Pourson ja ennine Niggula perremees Jaan Anderson ja selletiwa ende maja ärra andmise luggo Jaan Anderson massis Margus Poursonile riia kõrre kündmise eest 4 rubla hõbetad ja kou teggemise eest om da tälla päiwi tennu, sis ei olle ennamb nauda Jaan Andersoni käist roa kõrre künnise ega koo teggemise eest, enge moisal om weel näuda aida eest 10 rubla ja aiga eest 2 rubla. 
Ja Margus Poursonil om sis kos temma maja käest ärra annab üts kõrreline koda ja ülles künnedu roakõrs ärra anda ja weel om Jaan Andersonil metsa. 
Peter Kellamow 1 wak rukki
Jaan Laurberg 1 wak rukki külwi palka
