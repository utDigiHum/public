Kusta Raudsepp müib oma hooned mis temal Loomakese ja Kusma talu maade pääl on Jaan Lükkile 40 rbl. eest ära; sellest maksab Jaan Lükk kohe 5 rubl. ära mille wastu wõtmist Raudsepp selle leppingu lõppul oma nime alla kirjutamisega tunnistab. Praegu jääb aga wõlgu 35 rubl. ja saab Jaan Lükk sellest 17 rubl. 50 kop. 10 Januaril 1900 a ja töine 17 rbl. 50 kop. 10 Januaril 1901 aastal ära maksma ühes wiis protsendiga.
K Rautsepp &lt;allkiri&gt; Jaan Lükk &lt;allkiri&gt;
Et Kohus kokkulepijaid pooli selgeste tunneb ja et need mõlemad oma käega on alla kirjutanud saab selleläbi tõeks tunnistatud.
Eesistuja J Willipson &lt;allkiri&gt;
Kirjutaja: Puusepp &lt;allkiri&gt;
