Tulid ette kad Kaarel Kubja pärijad: lesk Mari Kubja, poeg Josep Kubja, tema sõsar Sohwi Kokka sünd Kubja, wiimane oma mehe ja nõuandja Jakob Kokka juuresolekul, ja panewad Kaarel Kubja päranduse jagamise akti ette ning paluwad seda ustawaks tunnistada. Akt on sääduslise stempelpaberile kirjutud ja pooltel on säädusline lepingu tegemise wõimus. Elawad kõik Pala wallas. 
S.Kokka J. Kokka.
Mari Kokka ei oska kirja tema palwel Karl Welt J Kubja.
Kohus otsustab: Ettepandud päranduse jagamise leping ustawaks tunnistada ja aktiraamatu sisse kanda. Pooled on kõik wallakohtule tuttawad ja aktis nimetud pärandus on Pala wallas.
 Esimees K.Wahi.
liikmed J Otter K Pender
aj. Kirjutaja J Priimägi.
