Sellsamal päewal 18 oktobril 1888. -
Ette kutsuti Adam Tiganik, tema ütles küsimise pääle, et tundmatud inimesed ühe pudeli on üle letti wisanud. -
Alexander Tiganik tunnistab, et tema Juhan Trossile 3 pudelit õlut müünud, säält ühe tühja pudeli tõuganud üle letti teisele poole ja loonud 
4 tühja pudelit katki. - neid pudeli tükka olla Ad. Tiganik küll üle letti wisanud, aga need tükkid mitte Hindrik Parwesse puutunud. -
Ka Reinhold Arsus tunnistas sedasama. -
Juhan Rodes tunnistas sedasama. -
Mõistus: Kaebaja Hindrik Parw saab oma kombeta üles pidamise perast kohtu ees ja kõrtsi rentnikule Aadam Tiganikule kätte külge pane-
mise eest 5 rublaga trahwitud. - Jüri Jaska, Andres Steinbach ja Jaan Steinberg peawad wale tunnistuse perast kumbgi 3 rubla maksma. Et Juhan Tross tüli alustaja kõrtsi muujatega ja kohtu ees mitmetpidi toorelt ja kropilt üles naidanud peab 4 rubla trahwi ehk 20 witsa lööki saama. -
Mõistus kuulutati § 773 ja 774 seletamisega Hindrik Parw´ele, Aadam Tiganik´le, Jüri Jaska´le, Andres Steinbac´le  Jaan Steinberg´le ja Juhan Tross´ile.
