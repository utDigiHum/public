Haslawa Kogukonna Kohus sel 19 Januaril 1887
Man ollid:
Kohtumees Jürri Kärn
"  Johan Opman
"  Jaan Hansen
"  Johan Raswa
Selle otsuse päle sest 12 Januarist s.a Kristjan Undritzi kaibuse asjan wastu Karl Lesta olli Jürri Laban tullnu ja tunistas et Karl Lesta tullnu Kõrzi jurest kodu ja kõnelnu et tema kõrzimehe küssimise päle 10 rubl est kuu tenial Petril wõlgu anda om käsknu.
Mõistetu: et Karl Lesta se 6 rubl 63 kop Kristjan Undritzele 8 päiwa sehen ärra peab massma, selle et tema, ni kui kaits tunistajat wälja ütlewa kõrzimees Kristjan Undritzele luba om andno 10 rubl est selle inemisel wõlgo anda ja selle est wastutada om lubanu.
Se mõistus sai kulutedo Karl Lestale ja õpetus leht wälja antu. Sel 23 Webruaril s.a Kristjan Undritzele kulutedo. 
Kohtumees: Jürri Kärn XXX
Johan Opman XXX
Jaan Hansen /allkiri/
Johan Raswa XXX
