Adu Luik kaebas, et Ado Lill temale 2 rubla wõlgu mitte ära ei makswat. Kaebaja nõuab seda kätte saada.
Ado Lill ütles, et tema on küll Adu Luiga käest 2 rubla laenanud, aga Adu Luiga käest temal 3 kuu üüri a 1 rubla = 3 rubla saada.
Adu Luik ütles, et kolme kuu üür a 25 kopkat = 75 kopkat maksa olla.
Mõistus: Üüri raha saab kuu ees 50 kopkat arwatud, summa 1 rubla 50 kopkat, siis on Ado Lill weel 50 kopkat maksa ja peab tema see 50 kopkat Adu Luigale 14 päewa sees äramaksma.
Mõistus kuulutati § 773 seletamisega.
