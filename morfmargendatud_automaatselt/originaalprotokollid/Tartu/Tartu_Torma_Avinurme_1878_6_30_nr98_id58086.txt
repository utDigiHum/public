Awwinorme Metskoggokonnakohtus sel. 30 Junil 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jürri Tomik
kolmas kohtomes Josep Welt.
Kaebas Maetsma külla koolmeister August Schulbach et Jaan Unt on koolimajase tulnud jobnud peaga ja seal kissendanud ja wandunud. ning omma tüttart seal suure puuga peksma hakkanud. ja kui temma Jaan Unti mitto korda kelanud ja Jaan Unt ei olle kulanud on temma tedda kinni sidduda lasknud. mis peale agga Jaan Unt häbbematta sannadega tedda soimama hakkanud. kurradi puts, lehmaputš ja sa horad minno Naisega ja pärrast ölnud, noh ma saan küll omma sõimamisse eest trahwitud agga külma ma siis tahhan sind häwwitada et sinno ennamb ei olle, ja pallus koggukonnakohhut et temma kartab et kas Jaan Unt tedda sallamahti ärra tappa tahhab ehk Maja põllema panneb. ning et Jaan Unti käest küssitud saab mis asjaga temma tedda kautada ähwartanud.
Jaan Unt räkis et temma olnud suggu wiina sanud. ja temma küll wihhaga koolmeistrid sõimanud. ja ka ähwartanud agga kurja ei olle temma ommas süddames mõtlenud
Moistus
Et Jaan Unt üks häbbematta innimenne ning igga innimest roppuste sõimab, kui temma on wiina sanud. ja sedda süid temmale mitto korda andest antud. temma agga pääw pääwalt häbbemattust lähheb siis sab temma selle eest 25 witsa lögiga trahwitud. Josep Laurisson XXX
Jürri Tomik XXX
Josep Welt XXX
