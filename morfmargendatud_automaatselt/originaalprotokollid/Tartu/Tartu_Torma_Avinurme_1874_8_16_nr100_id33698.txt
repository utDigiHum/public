Awwinorme Mets kogokona kohtus sel 16 Augustil 1874
koos ollid
pea kohtomees Josep Kask.
teine kohtomees Juhhan Koppel.
kolmas kohtomees Maddis Sildnik.
Andres Pomm kaebas et temma on külla ma pealt 2 saato heino maha ajanud, mis aga tema wend Maddis Pom ärra tonud, ja tahab tema need heinad kätte sada.
Maddis Pomm räkis et tema on seda hainamad jo mitto aastat tehnud mis külla perremehed on temale annud ja teha lasknud, Andres Pomm on aga seda heina maha ajanud ja on temma siis need heinad ärra tonud.
Külla perremehed räkisid et se heinam külla karjama on ja ei tea nemad kes sedda heinamad iga aasta tehnud
Mart Wälli tunistas et Andres Pomm on tema käest lubba küssinud sealt heina teha, koolmeister Josep Sammelson tunnistas et Maddis Pomm mitto Aastad sealt heina tehnud mis temma põllo ma otsas.
Moistus
Et selle heinama külles Maddis Pommil ei ka Andres Pommil kummagil rohkem õigust ei olle, Andres Pomm olli agga sealt heina maha ajanud, Maddis Pomm agga need heinad jälle ärra tonud siis moistab kogokona kohus et nendele need heinad polest sawad se aasta, ja Maddis Pomm peab Andreselle 1 Rubla wälja maksma, ja jääb se assi külla peremeeste tahtmisse järrele, kellele nemad edespidi seda heinamad pruki anda tahawad.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
