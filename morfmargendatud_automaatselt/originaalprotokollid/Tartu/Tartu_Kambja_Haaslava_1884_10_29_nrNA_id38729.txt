Sel 29 Oktober 1884
Kaibas tüdruk Ida Martinson, Jaan Sawiko man ollen, et ösel 16/17 Oktober päle Kangro tallo aid ärra lõhutu sanu sis om se warras tema kasti lukki katski murdnu ja om kastist wälja warrastanu: 			Wärt		1 kask			8 Rbl		1 palito			12 Rbl		10 nakla willast langu			10 Rbl		3 sängi linna			3 Rbl		1 nael willast podilangi			2 Rbl 56 cop		
tema arwab seda wargust Haslawa walla soldat Kusta Toomi päle, selle et se warras kui ärra lännu omma kaela salli sinna aita maha om jätnu ja se om just Kusta Toome salli ja pallus et kohus seda asja selletuse alla wõttas.
Kusta Toom wastutas selle päle et temal sel ösel kui se wargus sündinu kello 3 homiku Ignatse kõrtsin jonu ja sis om Krimani Jaan Tohwri jurde lännu ja seda salli es tunnistanu Kusta Toom mitte ommaks, mis aidast leutu. wait üttelnu et temma salli tema kaelan om.
Kangro tallo sullane Karl Laberile said mõllemba sallit se om se mis aidast leutu ja se mis Kusta Toomi kaelast sai wõetu näidatu. Sesama es tune midagi üttelda. Kangro tallu sullane Jaan Luha ja Kangro tallu rentnik Peter Leming tunistasiwa et nemma neid sallisit õigede es tune kumb Kusta Toome kaelan enne seda wargust om ollnu.
Jaan Erman tunistas, et se salli, mis aidast om leutu enne wargust Kusta Toomi kaelan om ollnu ja tundis ka sijn selle salli ärra kui mõllemba ette said näidatu.
Kangro tallu ommanik Karl Angu tunistas et teama ei wõi just mitte üttelda kumb salli Kusta Toomi kaelan om ollnu, tõine neist om kül enne seda lõhkumist Toomi kaelan ollnu.
Kohtumees Johan Klaosen andis Protokolli, et tema tõisel päiwal päle seda lõhkumist ülle kaenu om ja leudnu et warras läbbi aida kattuse sisse om lännu, aidan om tema selle tüdruku kasti lukku lahki kangutanu.
Otsus: et se wargusse assi Keiserliko Tarto Sillakohtu kätte anda om, selle et se wargus ülle 10 Rubla suur om
Päkohtumees: J. Link /allkiri/
Kohtumees: 
"   Johan Klaosen XXX
"  J. Krosbärk /allkiri/
