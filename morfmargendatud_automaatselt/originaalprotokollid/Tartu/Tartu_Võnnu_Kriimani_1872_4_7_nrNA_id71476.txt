Krimannis 7 Aprilil 1872.
Marri  Warrik tulli omma eest kostja Tõnnis Warrikuga ette ja kaebas et mul on  Juhan Semeni käest palka sada ½ leisikat linno ja 2 Rubla 50 kop rahha ja linnade eest nõuan minna 1 Rubla 50 kop rahha siis on mul ütte summa Tema käest sada 4 Rublad.
 Juhan Semen  tulli ette ja ütles: ma ollin Temmale ½ leisikat linno pakkunud agga Ta ei wõtnud wasto nink käskis ärra müia ja ma olli enda linnade seas ärra müinud ja sain 1 Rubla 60 kop Leisikas ja palka on mul Temale kül weel 2 Rubla 50 kop maksta agga selle assemel on Temmal minnule maksta 3 näddali päewad mis Ta minno mant ärra olli à 25 kop päew ja 1 paar sogi wõttis ka peale se kui minno mant ärra läks se on 50 kop. se teeb ütte summa 5 Rubla 75 kop.
Marri Warrik tulli ette ja ütles: et mul olli Tema käest need päewad kaubeldud se peale ütles Juhan Semen: näita omma kauba leht mis ma sulle andsin.
Mõistus 7 Aprilil 1872:
Koggokonna Kohhus mõistis nende kauba lehhe peale et  Marri Warrik peab Juhan Semenile 1 Rubla 70 kop. maksma kus Tema wõlgo jänud palk ka juurde arwatud on.
Kohto lauan olliwa
Peakohtomees Juhan Mina 
Kohtomees Karel Põdderson 
" Juhan Raswa 
