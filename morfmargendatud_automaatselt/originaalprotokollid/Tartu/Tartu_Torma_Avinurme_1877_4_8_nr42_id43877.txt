sel. 8 Aprilil 1877
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jürri Tomik
kolmas kohtomees Jaan Pukk
Jaan Kiik kaebas weel omma ärrasurnud wõera emma Leeno Kiike warrandusse pärrast, et kui temma issa on ärra surnud siis on se tallurahha keik wõera emma kätte sanud. ning olnud sedda rahha 87 Robl. mis issa enne omma surma ommale jaust wõtnud ning wõera emma kätte annud, ja wenud siis se wõeras emma sedda rahha omma wenna Josep Kallause kätte hoida, mis nemmad agga taggasi nõuawad
Josep Kallaus räkis et se hopis walle et temma ärra surnud Sõssar Leno on temma kätte rahha hoida annod. ning on temma nüid agga Kiigest omma surnud sõssara järrele janud warrandussest sanod. 1 kuub, 1 rättik 1 selik ja 5 küinart riet.
Moistus
Et Marri Rajada ei olle seie kohto ette tulnud sest et temma Majas just tännasel pääwal pulmad, siis jäeb se assi tullewase kohto pääwa peale selletada ja maksab Josep Kallaus minnewase korra ärra jäämisse eest 30 Cop trahwi walla laeka.
Josep Laurisson XXX
Jürri Tomik XXX
Jaan Pukk XXX
