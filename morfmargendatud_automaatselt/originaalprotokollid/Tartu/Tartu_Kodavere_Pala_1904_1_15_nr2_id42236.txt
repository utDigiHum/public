1904 aasta jaanuari kuu15 päewal kinnitatud Samuel Willa ja Aleksander von  Stryki maja müümise leping:
Mina Samuel Willa tunnistan see läbi, et mina olen oma maja, mis Kadrina mõisa maa pääl Punikwere kõrtsi ligidal ja selle kõrtsi krundi pääl seisab Kadrina mõisa perishärrale Alexander von Strykile ära müünud 70(seitsekümne) rubla eest. Kuni see kaup kogukonna Kohtu läbi kinnitatud saab wastan mina weel kõige kahjo eest mis sel majal peaks tänasest päiwast kuni kauba kinnitamise päiwani,
sellel majal juhtuma. See üüri raha, mis see mees, kes praegu sääl sees elab, weel maksma peab, ei saa mina enam pärida, waid jäeb see raha Kadrina mõisa perishärrale. 5(wiis) rubla olen mina käsi rahast sanud, ja jäeb mulle siis kauba kinnitamise päewal weel 65 (kuuskümmend wiis) rubla saada. Kuuskümmmend wiis rubla makseti kinnitamise päewal ära.
                               Esimees: J. Soiewa
                               Kirjutaja: Sepp.
