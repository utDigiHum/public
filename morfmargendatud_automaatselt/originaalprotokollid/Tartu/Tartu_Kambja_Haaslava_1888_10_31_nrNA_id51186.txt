Sel  31 Oktobril 1888.
Kaibas Pappa tallo ostja  Johan Link, et  Ärso tallo ostja  Mihkel Ärs, Treiali tallo ostja Jaan Treial ja Linki tallo ostja Johan Link omma karja arwata 30 Ellajat, lamba ja ziad tema karja maast ega päiw läbbi karja maa pääle aijanu ja selle läbbi tema karja maast ja heina maast 60 sammu mõllembile poole selle maa nenda ärra sõkkunu, et seal midagi enamb peal ei kaswa ja tema omma karja koheki panda ei wõi, neil nimitedo perremestele om kül kardi perra üts wäikene tee ütte rattaste täis tema piirist läbbi antu, ent perremehed ei pea mitte piiri selleperrast pallub tema et kohus neid perremehi keelas ülle piiri karja aijada. Seda kahju om Kohtumees Johan Kliim ülle kaenu. 
Kohtumees  Johan Kliim  andis ülles, et se Johan Linki kaibus õige ja põhjandedu om.
Ärso tallo ostja  Mihkel Ärs ja Treiali tallo ostja Jaan Treial ollit tännases päiwas selle asja perrast selle kohtu ette kutsutu ja nimmelt selle ähwartusega et kui nemma ei tulle saab kohus ärra mõistma. Selle et nemma jobba nellas kõrd kutsumise päle ette tulnud ei ka wabbandanu ei olle, nemma ei olle ka tänna ette tullnu ei ka hennast wabandanu,
Otsus: et Linki Johan Link ette om kutsuta ja peab Pappa tallo ostja Johan Link ütte Maamõtja poolt se tunistus ette toma kui lai se tee läbbi tema karja maa (ei loe välja) om jäetu.
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
