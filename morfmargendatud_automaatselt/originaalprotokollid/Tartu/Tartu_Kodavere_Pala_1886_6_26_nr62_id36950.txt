Metsawaht Willem Kukk oli selle kohtu ette metsa asjus kaebust tõstma tulnud, ilma et temale oma ameti märki kuskil näha oli, ja kohus teda sellepärast  mitte ameti poolest tunda ei wõinud.
Järeleküsimise peale tõmbas Willem Kukk oma metsawahi märgi taskust wälja, näitas teda kohtule ja pani tasku tagasi.
Otsus Willem Kukk maksab üks rubla trahwi waeste laeku 8 pääwa sees sellepärast et ilma ameti märgita kohtu ette ametlikul wiisil tulnud oli.
See otsus sai kuulutud.
                                       Pääkohtumees J. Haawakiwwi.
                                             Kohtumees J.Stamm.
                                             Kohtumees M. Piiri.
                                             Kirjutaja      allkiri.
