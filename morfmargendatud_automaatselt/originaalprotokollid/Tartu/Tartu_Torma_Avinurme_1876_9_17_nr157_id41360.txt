sel. 17 Septembril 1876. 
koos ollid
peakohtomees Josep Kask
teine kohtomes Josep Laurisson
kolmas kohtomes Jaan Mänd
Tomas Sild kaebas et temma Lell Maddis Sild on ärra surnud ning keik Majakraam ja Maja keige krundiga temmale jätnud, nüid on agga Koddasaare Metsa waht Jakob Traks keik ajad ümber selle maja ärra põlletanud ning õue ülles künnud ja pallos kohhot sedda selletada et mis õigusega Metsawaht Jakob Traks sedda teeb.
Metsawaht Jakob Traks kostis selle kaeptose peale et se pobbolimaja on temma krundi peal ja on temma se maatükkikenne Maddis Silla käes rendi peal hoidnod. et agga Maddis Sild on nüid ärra surnud. ning temmal lapsi ei olle jarrele janud siis on temma se haugis selle maja ümber ärra põlletanud ning ei lubba temma et keski ennamb sinna Maja sisse ellama lähheb ja on Maddis Sild temmale 2 Robl. renti wõlgu jänud mis temma Tomas Silla käest nõuab sest et se keik Maddis Silla warrandus pärrinud.
Tomas Sild räkis et temma Lell Maddis Sild on omma asjad enne surma selletanud ja ei olle se siis ütlenud et temma Metsawaht Jakob Traksile renti wõlgu mis pärrast temma ka siis sedda renti 2 Rubl. maksa ei rahha ja olnud kohtomees Jaan Mänd ka temm Lelle surma wodi juures
Kohtomees Jaan Mänd tunnistas et Maddis Sild ei olle enne omma surma ütlenud, et temma Metsawaht Jakob Traksile 2 Rubla wõlgu
Moistus
Et se wanna pobbolimaja Metsawahhi krundi peal selle maja perremees Maddis Sild agga ärra surnod. ning temmale lapsi ja Naist ei olle järrele jänud, kes seal sees ellada woiksid ning se surnud. sedda Maja omma Wenna poja Tomas Sillale lubbanod; siis moistab koggukonna kohhus et Metsawaht woib need maad küll ärra wõtta aga maja on Tomas Silla jaggo ja woib temma seal sees ellada kui temma tahhab mis Metsawaht Jakob Traks keelata ei woi.
Ka ei prugi Tomas Sild se nõutud 2 Robl maksa sest et Maddis Sild ei olle sedda omma surma woodi peal ülles annod. et temma Jakob Traksile 2 Robla wõlgu
Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastal 1860 §§ 772 ja 773 kuulutud ning nendele kohto kaijaatelle need sadose jarrel Appellationi õppetusse kirjad walja antud. 
Josep Kask XXX
Josep Laurisson XXX
Jaan Mänd XXX
Tomas Sild andis sel. 23 Septembril ülles et temma selle moistosega rahhul ei olle, et Metsawaht Jakob Traks need maatükkid mis temma kaddund Lell temmale testamenti järrel lubbanud, woib arra wõtta ja pallus Appellationi wälja suurema kohto minna, mis temmale ka nendakohhe N 255 all sai wälja antud. 
