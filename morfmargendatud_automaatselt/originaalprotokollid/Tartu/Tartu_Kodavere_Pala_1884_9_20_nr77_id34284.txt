Kaebas Palla mõisa walitsus kirjalikult mõisa walitsuse  asemiku  Kühni juures olemisel et Kristjan Rätsep olla wana proua heringid 5 tk warastanud ja nõuab et se asi saab siin kohtu juures läbi otsitud kui se asi õige on.
Kristjan Rätsep wastas oma isa Karel Rätseppa kõrwal et tema ei ole mitte neid heringid wana prouast warrastanud waid temal olnud küll 3 naela heringid Piiri kõrtsist ja Willem Anask olla 2 naela heringid kaupmehe Karel Trangonilt toonud ja panud ühte ja söönud ühes.
Otsus: Tulewaks kohtu pääwaks tunnistajad: Josep Nõmm ja Karel Tragon ja Juhan  Somri seie kohtu ette telli:
