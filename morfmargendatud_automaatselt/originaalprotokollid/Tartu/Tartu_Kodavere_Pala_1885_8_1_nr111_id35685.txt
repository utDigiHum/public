Tuli ette August Mäggi oma isa eeskostmise all et Willem Tõnts teda Piiri kõrtsis ähwardanud nende sõnadega ,, sul olgu räbalad taga `` ja tunginud tema August Mägi pääle. Peale selle olla Willem Tõnts üttelnud et August Mäggi tema seakarjus olla. Peale selle tahtnud Willem Tõnts, August Mäggi Kodawere poistest peksa laska ja  olnu üks pois kelle nime tema ei tea kiwiga temale pähe wiskanud ja puuga pihta löönud.Tunnistajaks nimetas Jürri Sallo, Märt Wilka, Karl Ilwes. Karel Mägi nõudis selle kõige eest  wiissada rubla.
Ette kutsutud Willem Tõnts ja wastas küsimise pääle, et tema ei ole kunagi August Mäggid ähwardanud ega tema peale tunginud, ka mitte Piiri kõrtsis. August Mägi üttelnud temale, et tema Tõnts, 
olla seakarjus ja ütelnud Tõnts wastu et tema ise seapois olla. Tema Tõnts ei olla mitte Kodawere poissisid tema wastu ülesõssitanud aga temal Mäggil olnu nende poistega üks wana  wiha asi ees ja olla se küll wõimalik et need poisid Mäggid peksa tahtnud.
Ette kutsutud Jürri Sallo ja tunnistas et Piiri kõrtsi August Mäggiga sõitnud ja ise hobuse juure wälja jäänud August Mäggi lähnud kõrtsi warsi sellepääle tõusnud kära kõrtsis ja August tulnud kõrtsist wälja ja Koddawere poisid temale järele kes teda peksa tahtnud kui wankri peale saanud siis löönud üks pois pitka puuga August Mäggile ja pillutud kiwwa järele wankrist. Willem Tõnts oli esimene olnud kes Mäggi järel kõrtsist wälja tuli aga warsi tagasi läks.
Ette kutsutud Piiri  kõrtsinaine Juli Nõmm ja wastas küsimise peale et tema sellest tülist kedagi ei tea.
Jürri Sallo nimetas Koddawere poistest Widrik Jannast, Karel Soomus, Josep Rosenberg, Kusta Rossner, Märt Wilka ja Karl Ilwes kes kohtu ette ei olnud tulnud.
Otsus tulewa kohtu pääwaks tunnistused ette tellida.
                                       Kohtumees J. Soiewa
                                       Kohtumees O. Mõisa
                                       Kohtumees K. Olla.
                                    Kirjutaja  allkiri.
