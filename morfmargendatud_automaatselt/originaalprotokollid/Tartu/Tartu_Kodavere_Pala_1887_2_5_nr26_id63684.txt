Tuli ette Mihkel Wilk ja kaebas, et Leen Sallo olla teda laimanud ja nimelt ütelnud et tema mõisa heinaküini ärapõletanud on . Tunnistajaks nimetas Mihkel Tedder ja Kusta Tagoma Jakobi poeg. Nõudis et kohus ise oma arwamise järele  süüdlased trahwiks. Ettekutsutud Leen Sallo oma mehe eest kostmise all ja wastas  küsimise peale, et tema seda mitte ütelnud ei ole et Mihkel Wilk mõisa heinaküini põletaja on ei ülepea teda laimanud ei ole.
 Ettekutsutud Mihkel Tedder ja andis küsimise peale üles et se küll õige on et Leen Sallo Mihkel Wilkad mõisa heinaküini põletajaks sõimanud seal juures olnud ka Kusta Tagoma.
 Ettekutsutud Kusta Tagoma Jakobi poeg 16 aastat wana ja wastas küsimise peale et tema seda kuulnud kui Leen Sallo ütelnud et Mihkel Wilk Palla mõisa heinaküini ärapõletanud.
 Leen Sallo tunnistustega wastastiku ettekutsutud ja selle juure et tema seda mitte ütelnud ei ole. Tunnistajad jäiwad oma üteluste juure.
 Otsus et Mihkel Tedder Leen Salloga kohtukäiad on see tunnistus mitte wastuwõtta aga Kusta tunnistus selge on et Leen Sallo Mihkel Wilkad heinaküini põletajaks sõimanud, läheb 24 tunni peale wangi. See otsus sai kuulutud Leen Sallo mees ei olnud sellega rahul ja lubas suuremat kohut käia.
                           Kohtumees            J. Stamm. 
                            abi Kohtumees    J.Tagoma
                           abi Kohtumees     G. Stamm
                           Kirjutaja C Palm.(allkiri)
