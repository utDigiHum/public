Tulli ette Peter Pärli ja ütlep et Adam Juksaar on teda teotanud ja on timma õlle läppi 10 Rbl Rahha omma mützist wälja kautanud.
Adam ütlep et Peter on körzis tüllinut kella 8adakas kella 8ni hommiko. Timma olli üttepuhki kõrzis tapplenud ja ei olle mitte kõrzi mehhe kärä päle tülli mahhajadnut.
Grünwald ette kutsutud ütlep et timma on kohtumehhe Adam perra leind ja on tedda pallund tüllisi lachti tetta, Adam on Peter kindi wõtnud ja on tedda ärra winud körzis, moisa jure. Lömist ei olle middaki neind, ja Rahhast ka ei olle middaki neind.
Kohhus moistap sedda astja tühjas ja ei olle wara Adam sedda 10 R. Rahha massa mis Timma nõuwap.
Pämees Peter Maddisson XXX
Abimes Peter Zirk XXX
" Josep Tensing XXX
