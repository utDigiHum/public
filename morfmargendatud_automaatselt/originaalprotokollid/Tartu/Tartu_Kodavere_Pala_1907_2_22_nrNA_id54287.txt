August Kaarli p. Rätsepp palus kohut oma wiimast seadust üles kirjutada järgmiselt:
Olen Halliku Nõmmeveski talu N:6 oma naise Liisa Rätsepa, sünd. Kiisel Jaani t. käest omanduseks saanud. Leping on kinnitatud Jurjewi Kreposti jauskonnas 22. Märtsil 1906. a. Selle lepingu seise jäit üles panemata ja saab siin tõendatud, et minu surma korral kui naene Liisa minust järele jäeb, siis jäeb kõik talu ühes kõigega mis talus on ja minu päralt  seaduse järele on, minu naisele Liisa Rätsepale sünd. Kiisel, päriseks, ilma et kellegil minu sugulastest õigus oleks temalt midagi nõuda.
                                  August Rätsep
 Pala wallakohus tunnistab, et eesolew testament isiklikult tuttawa August Kaarli p. Rätsepa etteütlemise järele kirja on pandud ja tema poolt allakirjutud, kus juures see testament kohtu akti raamatu sisse tuleb kirjutada ja käesolew algustestament alal hoida.
Esimees: K.Reinhold
liikmed K Holst P Juur WTamming
 Aj. Kirjutaja Brükkel.
