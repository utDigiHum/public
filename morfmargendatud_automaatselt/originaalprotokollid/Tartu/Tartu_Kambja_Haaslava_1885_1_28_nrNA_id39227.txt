Haslawa Kogukonna Kohus sel 28Januaril 1885
Man Ollid: Päkohtumees Jaan Link
Kohtumees Peter Iwan
"  Johan Grossberg
Kaibas Johani Tallu ostja Johan Klaosen, et tema rentnik Johan Madison terminil renti ei olle massnu ja nimelt pidanu rentnik pool renti 110 Rubl enne 1 Aprilli ja tõine pool renti 110 Rubl enne 1 Oktobrit massma, ent nüid om tema selle raha 7 Januaril 1885 Kogukonna kohtust kätte sanu ja nõudis 10 protsenti selle raha est.
Rentnik Johan Madison wastutas selle päle, et ega temma raha kaibajale wõlgu ei olle olnu ja tema olles renti ega kõrd terminil massnu kui kontrakt käen olles ollnu ehk Johan Klaosen renti wastu olles wõtnu.
Mõistetu: et  Johan Madisson seda renti raha est, mis tema kaibajale wõlgu olli jänu ja nimelt 110 Rubl est 9 kuu pält ja 110 Rubl est 3 kuu pält 5%, mis 5 Rubl 50 Cop intressi wälja teeb 8 päiwa sehen Johan Klaosenile massma peab, selle, et temma terminil renti massnu ei olle ja kaibaja ka mõisa massu est intressit om massma pidanu.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees:
  Peter Iwan /allkiri/
"  J. Krosbärk /allkiri/
