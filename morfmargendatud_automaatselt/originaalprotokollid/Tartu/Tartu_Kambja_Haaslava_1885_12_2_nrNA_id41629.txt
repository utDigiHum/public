Haslawa Kogukonna Kohus sel 2 Detsembril 1885
Man ollit päkohtumees Jaan Link
Kohtumees: Johan Grossberg
"   Jaan Pruks
Selle otsuse päle sest 14 Nowembrist s.a Hans Kötzi kaibuse asjan wasto Jaan Kroman olli kutsumise päle tullnu Jaan Wirro ja tunistas et Hans Kötzil 5 Wakka Keswi palkas olli kaubeltu Jaan Kromani käest.
Daniel Luha tunistas nisama.
Mõistetu: et Jaan Kroman se 5 Wakka palka Keswi 8 päiwa sehen Hans Kötzile ärra massma peab.
Se mõistus sai Hans Kötzile kulutedo.
Sel 3 Webruaril 1886 sai se mõistus Jaan Kromanile kulutedo ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees J. Krosbärk /allkiri/
"  Jaan Pruks /allkiri/
