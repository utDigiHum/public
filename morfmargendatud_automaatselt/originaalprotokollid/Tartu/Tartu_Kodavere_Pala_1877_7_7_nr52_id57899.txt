Perremees Willem Perramets kaibas tema saatnud oma sullasega Jaan Waddi oma poega  wanna mõisa kooli ja see lasknud tema nore hobbuse mis 120 rubl. wäärt lahti ja se hobbune jooksnud metsa jänud puie wahhele kinni ja surnud ärra. Tema olla teisel hommikul kohhe teise mehe kasa annud ja käsknud otsi agga pois läind Halliko kõrtsi ja wiitnud aja ärra kuni tuiskama hakkanud ja
enam jälgi leida ei wõinud.
 Jaan Waddi wastas hobbune olla tema aisaga mahha lönud ja ärra jooksnud ning olla se õnnetus teadmatta tulnud. Willem Perramets wastas kui see õnnetus on olnud ja  on ette holetuise pärrast tulnud nõuab tema pool kahju se on 30 Rubl. , teada andjale 1 Rubl. 15 kop. üks kassuk 4 Rubl. Summa 35 Rb. 15 Cop.
 Kohhus mõistis: Leppisid nenda, et Jaan Waddi jättab oma tenitud palgast mis weel Perrametsa jures sees on 8 Rubl. ja maksab puhhast raha 20 Rubl. Se on kokku 28 Rubl. lubbab sedda  suma nenda maksa, et sel 1877 aastal 5 Rubl. ja sel 1878 aastal 15 Rubl.
            Pä kohtumees Kustaw Kokka xxx
                 Kohtumees Kustaw Nukka   xxx
                 abbi             Tomas Welt     xxx     
