Krimanni koggokonna kohtus  
 An Tedder tulli  ette ja kaebas, et Weski pois Jaan Wilson on mind russikatega peksnud ja pibo suitso mulle ninnase lasknud ja kiskus ka minno rided üllese sepeale wiskasin minna temma pibo suust alla kotta ja lõin temmale wasto ninna nenda et ninnast werri wälja tulli, siis wõttis mind kinni ja rudjus wasto poolt lagge ja lükkas mind wasto müüri nenda et werri peast wälja tulli.
Peter Freyman tulli ette ja tunnistas: minna ja Juhan Kink hullasime selle tüdruko An Tedrega pole lae peal ja Jaan Wilson tulli ka senna ja hakkas hullama ning laskis An Tedrele pibo suitso agga An Tedder sai wihhatsest ning lõi Wilsonil nõnnast werri wälja tulli siis kerotas Jaan Wilson tüdruko mahha agga seal ei olnud lömist ei ka mingi suggust tappelust.
Juhan Kink tulli ette ja tunnistas ka nenda samoti agga ütles: et Jaan Wilson on tüdroko paljast kiskunud ning ka mitto korda lönud ning wimaks wasto müri tõukanud nenda et peast werri wälja tulnud.
Jaan Wilson tulli ette ja ütles: An Tedder Peter Aberg ja Juhan Kink hullasiwad pole lae peal ni kaua kui ma 9½ wakko nisso ärra nisutasin ja kui ned nissud ollin ärra nisutanud siis läksin minna ka sinna ning lükkasin An Tedre nalja perrast kotti peale mahha agga temma lõi mul suust pibo mahha ja ka ninnast werre wälja ja hakkas hirmsaste sõimama agga sest lükkamisest ei wõinud ta mind haiged sada sest et ma mitte wihhaga pole lükkanud.
Juhhan Klaos (Perremees) ütles Juhan Kink ei olle mitte tunnistaja sepärrast et ta An Tedrega ütten ellab wõi sõbrad on. Sepeale ütles An Tedder kas sellepärrast ta mulle sõbber on et ta mõnni öö on minno man magganud.
Mõistus:
Koggokonna kohhus mõistis 9 Januar et  Jahn Wilson maksab 1 Rubla walla ladiko sepärrast et tüdrukuse on putunud ja  An Teder maksab 1 Rubla et ta ütles: endal õigus ollewad  Juhan Kinki man maggada mis koggonist seaduse wasto on.
Man olliwa:
Peakohtomees Josep Kerge 
Abbi "  Peter Püür 
"  "    Mihkel Läett 
Wallawannem Juhan Klaos XXX
Kirjotaja assemel: M. Baumann /allkiri/
