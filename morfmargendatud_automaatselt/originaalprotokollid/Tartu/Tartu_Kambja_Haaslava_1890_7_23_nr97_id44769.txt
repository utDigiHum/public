№ 97
Sel 23 Julil 1890
Selle otsuse päle sest 9 Julist s.a № 89 olli Johan Udel tullnu ja tunistas, et Mihkel Ers om 16 rubl Kusta Hanseni käest lainanu 23 Aprilil, lubanu seda raha Mihkli päiwal ärra massa ja 5 wakka Kartoflit prozentis maha panda. Johan Udel nõudis päiwa palka.
Johan Udel /allkiri/
Jaan Annok tunistas et tema ei tea kui palju Kartoflit Kusta Hansenil maha pantu ollnu.
Jaan Anuk /allkiri/
Mõistetu: et Mihkel Ers se 16 rubl 14 päiwa sehen Kusta Hansenile ärra peab massma, selle et need Kartoflid tunistajate wälja ütlemise perra prozente est maha pantu ollid ja mitte kapitali kistutuses.
Tunistaja Jaan Kromann ja Johan Udel sawa kumbgi 50 Kop päiwa palka, keda Mihkel Ers massma peab.
Se mõistus sai kulutedo Kusta Hansenile.
Kusta Hansen /allkiri/
Sel 30 Julil s.a sai se mõistus Mihkel Ersole kulutedo.
Mihkel Ärs /allkiri/
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks /allkiri/
"  Jakob Saks /allkiri/
"  Jaan Treijal /allkiri/
