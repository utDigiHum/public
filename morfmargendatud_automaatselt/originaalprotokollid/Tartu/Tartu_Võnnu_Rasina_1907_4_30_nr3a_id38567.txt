Ette ilmusiwad Rasina walla liikmed: Keego talu omanik Joosep Jaani poeg Wanemb ja Johan Jaani poeg Nutt ja palusiwad nende wahel seiswat rendi leppingut Kohtu raamatusse kirjutada, mis järgmiselt käib:
§ 1. Joosep Wanemb annab oma päralt olewast Keegu N22 talu maast Johan Nuttile kolmas osa põllu, heina ja ka karja maad rendile, ühe aasta peale, s.o. 23 Aprillist 1907a. kuni 23ma Aprillini 1908 aastani.
§ 2. Paragrahv 1ses tähendatud 3osa hulka ei käi mitte Keegu talu maa peal olewad koda poolsete majad, maa ega ka nende käest saadaw rent, mis omanik Wanemb omale saab ja nimelt: koda poolse Kutsari käes olew pruukida 2 wakamaad ja - Anijärwe käes olew 4 wakamaa maad.
§ 3. Rentnik Nutt maksab selle 3da osa tema käes olewa talu eest renti aastas kaheksakümmend /80/ rubla ja nimelt: esimene pool Aprillil ja tõine pool 1sel Nowembril 1907 aastal ette ära.
§ 4. Rentnik Nutt peab tema käes olewast põllu maast sügisel nii sama palju üles kündma, kui enne künnetud oli, aga wastasel korral maksab kündmata jäetud maa eest kahju tasu wälja.
§ 5. Et Keegu talu rehe tänawasel rendi aastal ehituse all on ja kui peaks juhtuma, et omanik teda just sugisel aegsaste walmis ei jõuua teha, - siis selle eest ei ole rentnikul mingi sugust kahju tasu õigust nõuda ega ka nõudmist üles tõsta.
§ 6. Rentnik peab rukki nurm õigel ajal ja nimelt 18 Augustiks heaste ja hea seemnega maha külima ja ei tohi ka talust heinu, ristik heinu ega mingi sugust põllu põhku talust wälja laenata ega müüa ja ei tohi lina ka rohkem külida kui kolm wakamaad.
Rendile andja Josep Wanamb (allkiri)
Rendile wõtja Johan Nut (allkiri)
Et selle leppingu tegijad walla kohtule tuttawad on ja summa üle 300 rubla ei käi, saab Rasina walla Kohtu poolt tõeks tunnistatud ja lepping kinnitatud.
Esimees K. Schmaltz (allkiri)Liikmed D. Klaos (allkiri) J Kollamets (allkiri) P. Kiiple (allkiri)
Kirjutaja (allkiri)
