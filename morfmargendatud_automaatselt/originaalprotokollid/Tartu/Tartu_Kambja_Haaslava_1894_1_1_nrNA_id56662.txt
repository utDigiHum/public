1894 aastal.
Tuli ette Jaan Silm Haaslawa walla liige ja Jaan Mark Raadi walla liige ja palusiwad nende wahel tehtud järgmest lepingut protokolli üles panda.
Kondrakt.
Selle allkirjaga tunnistan mina Jaan Silm, Karu talu omanik ja perisostja, et oma Karu talu Jaan Morgile olen wälja rentinud, 70 waka maad põldu, kolm rubla ja wiiskümmend kop. wakamaa ja 13 wakamaad heinamaad kuue aasta pääle ja rentnik peab põldu järgmeste punkti pääl pidama.
1. Rentnik peab iga aasta poole põldu, see on kuue wakamaa ristikheinas pidama, aga nimelt üks suwi, sügisel wõib üles künda.
2. Rentnik wõib iga aasta wiie wakamaa lina all pidada.
3. Ristikheina seemneid annab peremees sada naela rentnikule, mis rentnik siis, kui aeg täis saab, tagasi peab maksma.
  
Talu hooned.
Kõik muud hooned saawad rentnikule nii kui on. Rehetuba kaha kambriga, kuho ka peremees ise Mihkli päewani elama tahab jääda, kuni ta omale elu korterid tahab ehitada; ja pääle selle laut, tall, ait ja keldri, mis Rentnikule saawad. peremees wõtab siis küüni, kelle otsas wäike laut on ja suure aida kõrwal wäike ait, aga küünile peab tõise külje pääle uks sisse lõikama nõnda, et peremehel karja aia pääle asja ei ole. Kõik muud tallitused, mis wallawalitsuse nimel tulewad tallitada, peab rentnik täitma, nii kui on: küidi korrad, õpetaja ja koolmeistri puu toomine ja muud.
Teetegemine langeb kahte jakku. Posti teed teeb rentnik kaks aastat ja peremees üks aasta ja kiriku tee pääle wiib rentnik kaks koormat kruusa ja peremees ühe, tegema läheb rentnik hobusega ja peremees jalksi.
Peremees peab sea karjuse ja rentnik karja karjose pidama ja kõik ehitused, mis uuest tulewad ehitada, teeb peremees, aga katuse õled saawad majast ja wana paranduste tarwis annab peremees materijali, ja rentnik teeb, nii kui hooned, katuksed ja õue aijad.
Sõnnikut saab peremees talu laudast esimese kuude 15 koormat rukimaa wõi kesa pääle.
Põhku annab peremees kewade nii palju kui küünib ja paar koormat heinu, mis rentnik siis, kui kondrakti aeg täis saab ja kui üle on jäänud, tagasi peab andma.
Rendi maksmise aeg on kaks kord aastas: 1sel Aprill kewade 145 rbl. sügise 10 Oktobril 100 rbl. ja iga kord peaks peremees rendi maksmise juures täielne kwiitung wastu andma.
Jaama heinad peab peremees maksma iga aasta ja ka kõik muud maksud, rentnik maksab ainult rendi ja kaks jagu magatsi sisse makso wilja.
Ei tohi ka peremees millalgi wiina saanud pääga rääkima tulle, pealegi naestega; kui tarwis rääki, peab ainult rendi maksjaga kaine pääga rääkima.
Selle allkirjaga tunistawad talu omanik ja rentija pärisomanik Jaan Silm ja rentnik Jaan Mork, et nemad omad lubad täieste täidawad.
Tähendus: Rentnik peab  8 karja elajad olema ja ei tohi tema, rentnik, mingisuguseid kiwi Karu maa päält ära wedada müimiseks.
Et selle enseiswa kontrakti sisu Talurahwa Sääduse § 718 järgi täielikult on ette loetud ja ka selles täielikku rahul olemist on wawaldanud kõige tingimiste kohta, saab selle kondrakti kinnitamise juures kinnitatud.
