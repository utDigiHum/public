Sel 10. Martsil c. subNr 1019. poolele jänud asjus tulli ette Abram Willik ja ütleb, et temma pahhanduse juures polle olnud. Pärrast räkinud Josep Kusik, et Jaan Peetso aanud tedda toast wälja ja öölnud: "kui ma talle hoobi andsin, siis ollid suust ja silmast werri wäljas.
Tunnistus sai awwaldut 
Kohhus moistis: Jaan Peetso ja Josep Kusik maksawad wastastikku tapluse eest 1. rubla kumbgi waeste ladiko trahwiks.
/: Otsus kuulutud sel 28. aprillil 1883. a. Tallorahwa seaduse raamato §§ 772 ja 773 juhhatusel ja oppuse kirjad wäljaantud :/
J. Pringweld [allkiri]
J. Kokka [allkiri]
Karel Kaddak [allkiri]
Karrel Tõrrowerre [allkiri]
Josep Kusik kuulutas 28. aprillil c et temma otsusega rahhule ei olle mis peale temmale kohhe appellatsioni kirri wäljaantud sai.
