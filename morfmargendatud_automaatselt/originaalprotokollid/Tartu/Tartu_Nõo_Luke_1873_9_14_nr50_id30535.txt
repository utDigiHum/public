Lukke mõisa kõrtsimees Jaak Põdder wõttis Padratsiko wennelassel 30 kop. eest kue seljast ärra, ja kui se mõisa Polizeile kaiwas, sis laskis tedda mõisa polizei enda ette kutso, agga kõrtsimees ei tulnud, siis läks renti herra Johanson kui mõisa Polizei essi kõrtsi jurde ja kässib sedda kube kätte anda, et tema kui mõisa Polizei wastotab sedda 30 kop.
Sis tulnud Pilleti Soldat Hans Põdder ja kissub wenelasse käest kube, kui mõisa Polizei tälle ütlep, et peab lahti laskma siis saab Soldat hirmsast wihhatsest ja nakkab moisa Polizeid sõimama, ja nõnda tulleb tema. Kuna tal suggugi asja ei olle mõisa Polizei ameti Tallitusse wahhele, ja rikkub sedda ärra. Ja pallub et nisuggune wasta nakkaja mõisa pääld peab ärra sama aetu.
Ette sai kutsutud se Pilleti Soldat Hans Põdder ja tema püiab henda wabbantada et tema ei olle teadnud et on lubbanud maksta, ja et tema ei ole sõimanud.
Tunistajast sai ka weel kutsutud Palmi naine ja tema ütlep, et kahhe wahhel on kuube kisknuwad ja soldat üttelnud ikka sinna ja sigga.
Mõistus.
Et Pilleti Soldat Hans Põdder henda sinna on wahhele palkanud, kos tedda suggoki waja ei olnud, ja suurt tülli ja mõisa Polizei tallitust on rikkunud, on kohhus mõistnud et 3 päiwa perrast peab mõisast ärra minnema, se on een tullewal tõisipäiw, ja kes tedda peab massab 3 Rbl öö päiwa pääld trahwi, seda on sanud ka kõrtsimees Jaak Põddrale siin kohto ees kulutud. Kohtu otsus on kulutud.
Pä kohtomees Karel Lahman
Juresistoja Peter Lestman
           "        Jaan Hagel
