Tulli ette wanna Palloperra wallawanemb ja kaibas, et temma omma ammeti ajal mitma asjade ja nõudmiste pääle rahha walla Kassat wälja masnu, mis temma Käsknu selleaigsel kirjutajal P.Paulid Kassa raatude kui wäljaminnekit ülles tähhendata ja mis kirjotaja mitte ei ollewat tennu.
1. Rõngu Kerriko mannu masnu temma ehhitusse rahha 8 Rubel 
2. Jaakob Nahel Laatsaretti rahha masnu temma 8 Rubel 70 Kopk
3. Rõngu Kerriko moiste masnu temma 6 Rubel 57 Kopk. maggasi wilja eest
4. Kohto mannu ostnu temma 4 tooli ja masnu neide eest ülle kige 5 Rubel 60 Kopk.
5. Nekruti rahha msnu temma 2 Rubl. 10 Kopk.
6. Kooli trahwi masnu temma ommast käest Pühha jerwe 5 Rubl.
Se teep utte summa wälja 35 rubl. 97 Kopk.
Otsus: Et Peter Paul essi ollewat üttel nii, et temma sija Kohto mannu ei tulle ja et temmal sii middagi tegemist ei olle, sest et tema jobba mitme aasta eest oma ammetid äräandnu, ehk olgu sis, kui kihelkonna Kohhus tedda nõwwap, sis saap tedda öijendamist allandlikult Keisreliku IV Kihelkonna Kohto hoolde pallutud. 
Kohto eenistuja Hans Murro (XXX)
Kohtomees Hans Uibo (XXX)
Kohtomees Willem Pern (XXX)
