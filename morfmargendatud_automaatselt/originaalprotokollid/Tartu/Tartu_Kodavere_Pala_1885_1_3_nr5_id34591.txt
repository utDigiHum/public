Kaebas Josep Willa et Kusta Nukka olla teda Piiri Kõrtsis 3 korda rusikuga löönud nõuab 15 rubl walo raha. 
Kusta Nukka wastab et tema ei olla löönud waid aga lükkanud teda käega.
Mihailo Wolosow Kockoralt tunnistab siin kohtu ees; et Kusta Nukka olla Josep Willad 3 korda löönud Piiri kõrtsis.
Jakob Wadi Pallalt tunnistab, et Kusta Nukka Josep Willat löönud üks kord. Tunnistus sai awaltud.
Otsus: et tunnistus selge on et Kusta Nukka Josep Willat löönud on, siis maksab tema Josep Willale 2 rubla walo raha
pääle selle  1 rubl trahwi laeka, seda raha peab 8 pääwa  sees wälja maksma.See otsus sai ette loetud ja rahul oldut.
                                     Kohtumees:   Josep Soiewa +
                                     Kohtumees:   Otto Mõisa     ++
                                     Kohtumees:   Mih.  Mölder   +++
