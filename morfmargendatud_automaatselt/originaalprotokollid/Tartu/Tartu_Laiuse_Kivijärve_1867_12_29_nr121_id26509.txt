Tulli kohtu ette Laiuse kaupmees A. Rauch ja kaebas, et se mõisa moonamees Christian Reinkubjas on temma podist üks tük rauda warrastanud, miss 30 naela kalub.
Kristian Reinkubjas ütleb, et temma ei olle mitte warrastanud, waid on sealt podist sedda rauda ostnud ja on 4 1/2 kop hõb. selle raua naela est ärra kaubelnud. Kui palju temma agga kalus, sedda temma ei tea.
Tunnistussemees Jürrij Palm tunnistustab, et ühhel pühhapäiwal on nemmad mõllemad sinna podi läinud ja Jürrij on seält rauda ostnud ja on sedda raua podist wälja tonud, selle peäle on se Kristian Reinkubjas taggumissest uksest ühhe raua kangiga wälja joksnud ja on temma reepeäle wissanud, ja on temmale agga üttelnud, sõida õite kermast, minna sedda rauda warrastasin.
Selle peäle sai mõistetud, et Kristian Reinkubjas selle wargusse est  peab temma 1 Rbl. 50 kop maksma ning se raud saab kaupmehhele taggasi. 
Pea kohtumees Märt Niggul XXX
Kohtumees Jürrij Tächt XXX
Kohtumees Jürrij Usstaw XXX
