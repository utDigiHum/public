Protokoll № 1.
Haaslawa wallakohus   Haaslawa  wallamajas 9 Webr. 1907 a.
Juures: esimees:  W. Põderson 
Kohtu liige
J. Rosenthal
P. Reino
Tuliwad ette  Johan Jaani p. Põderson, elab Haaslawa wallas  Roio karja mõisas ja tema wäimees  August Jaagu p. Kukk, wiimane wolikirja põhjal oma abikaasa Elfriede Kukke, sünd. Põdersoni asemel, elab   Alatskiwi wallas, annawad üles, et Johan Põderson täna selle lepingu põhjal, mis 3 Nowembril 1900 a. siin wallakohtu juures tehtud, oma õele Elfriede päranduse osa jau wiimse üks tuhat üks sada /1100/ ära maksa tahab kuna üks sada rubla juba ennem seda on ära makstud. Selle pääle maksis Johan Põderson kohtu kätte üks tuhat üks sada rubla /1100/. Selle summa andis kohus August Kukke kätte. Kukk seda summa wastu wõttes tunnistab, et tema abikaas Elfriede nüüd kõik oma osajau kätte on saanud ja enamasti kõikidest õigustest, mis tal siia maale Roio karja mõisa kohta oli, täitsa ja jäädawalt lahti ütleb.
 August Kukk  /allkiri/ 
Johan Põderson /allkiri/
1907 a. Webruari kuu 9 päewal on ülemal olew leping Haaslawa wallakohtule walla liikme Johan Jaani p. Põdersoni ning Worbuse walla liikme August Jaagu p. Kukke poolt wiimne woli kirja põhjal oma abikaasa eest, suusõnal ette kantud, nende mõlemate poolt oma käega alla kirjutatud mida kohus selle läbi siin tõeks tunnistab, juurde lisades, et mõlemad   kohtule isiklikult tuttawad on.
Kohtu esimees:  W. Põderson /allkiri/
Kohtu liige: J. Rosenthal /allkiri/
P. Reino /allkiri/
Kirjutaja: J. Wäljaots /allkiri/
