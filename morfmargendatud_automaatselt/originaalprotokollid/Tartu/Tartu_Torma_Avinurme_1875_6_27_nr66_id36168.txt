Awwinorme Metskoggokonnakohtus sel 27 Junil 1875
koos ollid
peakohtumees Josep Kask
teine kohtomees Maddis Sildnik
kohtumees Jaan Mänd
Kaebas Kronometsawaht Josep Kallaus et Kirbo külla perremehhed Mihkel Rosenthal ja Josep ja Karel Ambos on temma wälja alluse ärra wõtnud mis temma issi on rokinud ja nenda wiisi temma karjamaa omma wälja sisse lasknud. ja pallus koggokonnakohhot sedda selletada.
Kirbo külla perremehhed Mihkel Rosenthal ja Josep ja Karel Ambos räkisid, et se mitte Metsa wahhi wait nende karjama peab ollema, ja on Metsawaht omma wolliga nende karjamad rokinud kust temmal suggugi õigus rokida ei olnud, sest et nendel karjamaa suggugi ühhes ei olle
Moistus
Et Kirbo külla ja Metsa wahhi asseme wahhel ühtegi piiri egga kuppitsad ei olle, wait nende karjamaad ühhes on siis moistab koggokonnakohhus et Metsa waht Josep Kallaus nendasammuti omma rogitud karjamaad prugib kui Kirbo külla perremehhed ommad sel ajal kui se karjamaa wilja wälja allune on, se on et siis iggaüks sealt omma jaupealt heina woib tehha. kui agga se wälli kessas on, siis södawad nemmad loomadega ühhes ärra, et agga Josep Kallaus üks jaggu karjamaad omma wälja sisse tahhab lasta, ei sa se temmale lubbatud, ning jäeb se uus aed mis temma sowimisse peale teha sai nenda kaua seisma kui uus Maaselletus tulleb.
Se moistus sai selle säduse põhja peal §§ 772 ja 773 sest Tallurahwa sädusest 1860 kuulutud ning nendele need säduse järrel Appellation õppetusse kirja wälja antud.
Josep Kallaus pallus sel 4 Julil 1875 et temma selle moistosega rahhul ei olle. et temmale mitte lubba ei anta omma jaggu karjamaad aja sisse lasta. mis temma tahhab lahhus prukida, ja et temma selle asja pärrast suremad kohhut nõuab
Se lubbatud Appellation sai sel 4 Julil N 181 all Josep Kallausele wälja antud. 
Josep Kask XXX
Maddis Sildnik XXX
Jaan Mänd XXX
