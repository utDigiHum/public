Haslawa Kogukonna Kohus sel 19 Detsembril 1883
Man ollid:
Päkohtumehe asemel: Johan Klaosen
Kohtumees: 
 Peter Iwan
Johan Grossberg 
Jaan Pruks 
Kaibas  Jaan Särre, et temal Perremehe  Johan Madison käest 34 Rubl palka sada om ja pallus et kohus seda massma sunis selle et Perremees essi ei massa.
 Johan Madison  wastutas et tema 28 Rubl 66 Cop ärra massnu om ja 47 Rubl olla kaubeltu palk, selle et  Jaan Särre keik aido ärra teninu ei olle ei olle tema palka massnu. Suwel rüki põimo aigo om 9 päiwa pudu jänu päle tolle 7 päiwa ja ütte tsirpi om  Jaan Särre  ärra kaotanu, mes 60 Cop massnu. Neide päiwi est om tema sell kõrral 50 Cop päiwa est massnu.  Jaan Särre om sedulka rihma ärra kaotanu mis 60 Cop massab. Kaub ollnu seni kui Kadernapäiwani ent Jaan Särre länu Märtipäiwal ärra ja 2 Wakka kartoflid sanu  Jaan Särre, keda tagasi nõuab, tema om kauba leht  Jaan Särrele andno, kost keik nätta om.
 Jaan Särre andis selle ülles, et tema enegi 3 Rubl palka wälja sanu om 47 Rubl 50 Cop ollnu kaubeltu ollnu kaubeltu. Rükki põimo aigo ollon tema 8 päiwa haige, 7 päiwa pudu ei olle ollnu, tsirpi ja sedulka rihma ei olle tema ärra kaotanu. Kaub ollnu Märtipäiwani, kauba lehte ei olle sanu.
 Jaan Särre andis Jaan Otsa Haslawast tunnistajas ülles, kes kaubategemise jures om ollun. 
Otsus: et ülles antu tunistaja  Jaan Ots 2 Januaris 1884 ette kutsu om, ent  Johan Madison peab 5 Rubl (ei loe välja)  Jaan Särrele wälja massma, tõise raha ülle saab kohus mõistus tegema kui tunistaja ärra käinu om.
2 Januaril 1884 massis  Johan Madison 5 Rubl Jaan Särrele wälja.
Päkohtumees Jaan Link /allkiri/ 
Kohtumees 
Johan Klaosen XXX
Johan Krosbärk /allkiri/
