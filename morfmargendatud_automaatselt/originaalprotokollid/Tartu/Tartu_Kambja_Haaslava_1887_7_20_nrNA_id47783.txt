Sel 20 Julil 1887.
Tännasel päiwal sai Jürri Kitzing palk perremehe Johan Walge kätte kinni pantu, kes 8 rubl 73 kop päraha ja 15 rubl Jaan Sawikule kohtu mõistuse perra wõlgu om.
Johan Walge ütles, et temal joba 28 rubl palka wälja om antu Jürri Kitzingele ja 30 rubl sawat se inemine suwe est palka.
Selle päle tegi kohus otsuse, et Johan Walge 4 kuu palk, mis aeg weel tenimada om, 17 rubl kui tenimise aeg otsan om, selle kohtu kätte wälja peab massma neide nimitedo wõlgu kistutuses, mis Johan Walgele kulutedo sai ja õpetus leht wälja antu.
Päkohtumees Johan Link XXX
Kohtumees Jürri Kärn XXX
"  Jaan Hansen /allkiri/
