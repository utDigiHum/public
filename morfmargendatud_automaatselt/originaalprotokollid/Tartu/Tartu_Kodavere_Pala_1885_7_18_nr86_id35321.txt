Protokolli pääle 11 Julist s. a. N: 83 sai ettekutsutud Kadrina metsawaht Willem Tõnts ja wastas küsimise peale, et tema mitte seda puud mis Piiri kõrtsimehe käest leitud ei tunne ja ka mitte ütelnud ei ole, et se puu Kadrina metsast warastud on; üksi seda on tema ütelnud et se puu niisama pitkaks lõikatud on, kui Kadrina mõisa sanu. Neid kuus kasepakku ei tunne tema mitte sest omast jaust metsast ei ole temal mitte puid warastud.
Piiri kõrtsinaine oli haiguse perast wälja jäänud ja astus tema mees Josep Nõmm ette. Küsitud mis põhjuse peale tema naine metsawaht Karel Mäggi üles üttelnud, et tema  wõera inimestele metsast puid annab, wastas tema, et Märtsi kuus s.a. olnu ühel öösel Karel Reinomäggi Mardi poeg kes Kallaste külas Allatzkiwi jaus elab Palla metsast üks koorem puid  31 arsinad pitkad toonud ja Piiri kõrtsi ees seisatanud. Karel Reinomägiga
olnud ühes Johannes Punga kes Kallastel Kokkora all elab. Tunnistajaks nimetas Josep Nõmm, Ranna wallawanemad Jaan Weskimets ja kohtumeest Jakob Weskimets.Otsus tulewa kohtupääewaks metsawaht Karel Mägi, Karel Reinomägi, Johannes Punga ja Ranna wallawallawanem ja kohtumees ettetellida.
                                                        Kohtumees J. Soiewa.
                                                        Kohtumees    O. Mõisa.
                                                        Kohtumees    M. Mölder.
             
                                          Kirjutaja   allkiri.
