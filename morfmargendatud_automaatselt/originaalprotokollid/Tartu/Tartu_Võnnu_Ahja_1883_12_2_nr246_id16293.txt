Ahjamõisa mõtsawalitsus kaibas Johan Mägi olewad mõisa metsa 2 wakka alla üles kündnud ja nõudis säädusliku mõistmist selle üle.
Johan Mägi salgab.
Mõtsawaht Jakop Hindrikson teab et mõtsa üles künnetud on ei tea aga kes seda teinud.
Mõiste: Asi jääb seisma seni kui paremat tunnistust tuuaks.
Kohtumees: Peeter Hindrikson XXX
Kohtumees: Kota Wossman XXX
Kohtumees: Johan Klaosoon
Kohtumees: Johan Kiljak
Mõiste sai kuulutatud.
Kirjutaja: Chr Kapp &lt;allkiri&gt;
