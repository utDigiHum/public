Juli Adder omma issa Johani juuresollemisel kaebab Adam Tõrrowerre peale, temma olnud sellel teenistuses suwwe peal 28. rubla hinna al, saanud 20. rubla kätte ja olla 8. rubla saada
Adam Tõrrowerre kostab, et kaup olnud 20. rubla peale ja piddanud temma peale selle tüdruku leerima, agga issa weenud 5. näddalad tüdruku ennemalt ärra ja olli 4. näddalad teenimatta. Rahha olla temma 20. rubla keik ärramaksnud.
Johann Adder ütleb, et temma küssinud suwwel õppetaja kaest, ka wõttab temma tüttart Koddawerre leeri, se polle agga mitte lubbanud.
Kummagilgi pool ei olle tunnistust üllesse anda.
Kohhus moistis:
kauba suuruse peale ei olle kummagilgi tunnistust, perremees annab üllesse, et temma keik palk 20. rublaga ärra on maksnud, mis ka tüdruk wasta wõttab, pealegi on tüdruk 4. näddalad puudunud, mis peale kohhus Juli Adderi kaebduse tühjaks moistis.
/: Otsus kulutud sel 4. Martsil 1882. a. Tallorahwa seaduse raamato §§ 772 ja 773 juhhatusel ja oppuse kirjad wäljaantud :/
A. Krimm [allkiri], pea
Maddis Erm, khtm
A. Pärn [allkiri], -"-
