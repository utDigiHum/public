Rospiska N3.
Ja nižepodpisavšajasja žena krestjanina Liine Karlovna Koppel, urožden. Rozental, daju siju rospisku v tom, što polutšila ot brata svojego Petra Karlova Rozental svoju nasledstvennuju dolju dvesti pjatdesjat (250) rublei, s protsentami, kakovõja dengi ja imela polutšit soglasno nasledorazdelnago akta ot 4 Dek. 1908g. za N:8 po §3 - ot Petra Rozentalja. 
Dalše ja izjavljaju sim svoje soglasije na unitštoženije otmetki vnesennoi v otdel usadbõ Kulla, za krep. N 1388, im. St. Vrangelsgof, Jurjevskago ujezda, soglasno tomuže razdelnomu aktu §3 - javlennago u Vrangelsk. vol. suda.
Žena krestjanina: Liine Koppel sünd. Rosental [allkiri]
Jeja muž-sovetnik: Ludwig Koppel Jakobi poeg [allkiri]
Sobstvennorutšnost podpisi Liinõ Karlovoi Koppel i jeja muža Ludviga Jakobova Koppel sim udostoverjajetsja Vrangelskim volostn. sudom. Jurjevskago ujezda, s podpisjam i 27 fevralja 1914 goda 
Predsedatel suda: K. Pala [allkiri]
A. Potahov [allkiri] K Eli [allkiri]
Pisar suda: J Weske [allkiri]
