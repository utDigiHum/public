Leping № 8.
Haaslawa wallakohus Jurjewi maakonnas 23 Augustil 1913 a.
Koos on: wallakohtu esimees Jaan Koort,
kohtu liikmed: Kusta Kolt ja  Leo Klaos
 Tuliwad ette  Haaslawa walla  liige Kirbu talu omanik  Jaan Jaani poeg Rosenthal ühelt ja Raadi walla liige  Kristjan Kusta p. Laas  tõiselt poolt ja paluwad alljärgnewat suusõnalist lepingut kohtu lepingute raamatusse sisse kirjutada ja ustawaks tunnistada. leping on nimelt nii:
Jaan Rosenthal müüb selle maja oma päralt olewa Kirbu talu heinamaa pääl Ardla külas, nii sama ka selle majaga ühe katuse all ja sääl juures olewa sauna, mis tema Rosenthal Kusta Märtina käest ostnud, kokku lepitud hinna ühe saja neljakümne (140) rubla ees, mis hinna ostja Laas terwena Rosenthalile siin wälja maksis. 
Hooned saab ostja aga kätte alles 23 Aprillil 1914 a. Ostjal on õigus seda maja parandada ja ümber ehitada ja edasi müüa selle sama koha pääle jätmiseks ainult niisugustele inimestele, kes kohtulikult ilma trahwimata on. Kui hoone enne 23 Aprilli 1014 a. wigastatud ehk tule läbi häwitatud piaks saama, siis wastutab Rosenthal kahjude eest.
Jaan Rosenthal /allkiri/
 Kristjan Kusta p. Laas /allkiri/
1913 a. Augusti kuu 23 päewal Haaslawa wallakohus selle läbi tunnistab, et ülemal olew leping Jaan Rosenthali ja Kusta Laasi poolt suusõnal selle wallakohtule on ette kantud täna ülemal nimetatud päewal selle wallakohtule ette on kantud ja nende sõnade järele üleskirjutatud ja et leping mõlemate poolt oma käega alla on kirjutatud. Rosenthal on kohtule isiklikult tuttaw ja elab siin wallas, Laas elab praegu Tartu linnas Meltsiweski uul. N 29a, näitab oma isiku tõenduseks ette Raadi wallawalitsuse poolt 21 Augustil 1913 a. N 369 all wälja antud passi.
Wallakohtu esimees: J. Koort /allkiri/
Kohtu liikmed:
 K. Kolt /allkiri/
 L. Klaos /allkiri/
Kirjutaja:  Wäljaots /allkiri/
