№ 4.
2 Jurjewi Ülema Talurahwa Kohtu ringkonna 14 Wallakohus  Haslawa wallamajas 27 Webruaril 1897 aastal.
Juures olid Kohtueesistuja: Jaan Rosenthal 
Tuli ette  Haslawa  walla Zirma talu pärisomanik  Jaan Meier, elab Haslawa wallas ja Mäksa walla hingekirjaline  Jaan Petri p. Kiisla, elab Mäksa wallas, paludes järgmest nende walla tehtud lepingut protokolli panda.
 Jaan Lilo p. Meier annab üles, et tema õemehele  Jaan Petri p. Kiisla'le om oma warandusest järgmesed asjad ja waranduse tükid pruukida annud ja osalt nii kui rätsepa tööriista enese raha eest Jaan Kiislale pruukimiseks annud; nii kuiAsjad ja waranduse tükid			Rbl		1 hobune, regi, wanker ja hobuse riistad			70		2 lehma paar wäärt arwatud			60		2 siga paar wäärt arwatud à 10 rbl			20		1 riide kast täis riidedega			50		1 kapp wäärt			10		1 Söögi laud "			4		2 tooli ´a 2 rbl wäärt			4		1 rätsepa õmblus masin kelle juures kast täis rätsepa tööriistadega			45		Summa			263 Rbl		
Summa kakssada kuuskümmend kolm rubla.
Nendest eelpool nimetatud waranduse tükidest on minul Jaan Meier igal ajal enesele tagasi pärida mis minul tarwis on, sell põhjusel et asjad on minu omandus, mis õemehele Jaan Kiisla laenanud olen.
Jaan Petri poeg Kiisla tunistab eelpool üles antud lepingut õigeks.
J. Meier /allkiri/
Jaan Kisla /allkiri/
1897 aastal 26 Januaril on eelpool seisaw leping 14 wallakohtule suusõnalisel ülesandmisel ustawaks tunnistuseks sisse antud Haaslawa walla liikme  Jaan Lilo p. Meier ja Mäksa walla liikme  Jaan Petri p. Kiisla  poolt, kelle elu ase on Jaan Meier Haslawa wallas ja Jaan Kiisla Mäksa wallas, mõlemad lepingu tegijad on selle walla kohtule palelikult tundud inimesed, kellel mõlemidel seaduslik õigus lepingu  tegemiseks on ja et Jaan Lilo p. Meier ja Jaan Petri p. Kiisla  sellele lepingule oma käega alla on kirjutanud, saab selle walla kohtu poolt õigeks tunnistatud.
Kohtueesistuha: J. Rosenthal /allkiri/
Kirjutaja: K. Laar /allkiri/
/Pitser/
