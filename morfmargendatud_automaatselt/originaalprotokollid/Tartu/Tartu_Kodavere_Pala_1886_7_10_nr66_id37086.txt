Protokolli peale 17 Januarist m.a. N: 12 oli kogukonna kohus seda müüri mis Michail Wolosow Annus Waddile teinud ülewõetnud ja leidnud et seal 15 ruutsülda müüri tehtud oli.
Seda tunnistas Wolosow õigeks. Annus Waddi ütles Wolosowil andnud 3 wakka rukist ja üks kott 11 rbl. ja 1 rubla raha kokku 12 rbl sellega oli tema tasa.
Tunnistused mis selle asja sees kohtu ees käinud olid, saiwad kohtukäijatele ettekantud ja ei olnud mõlematel enam tunnistuse juure anda.
Kohus tegi otsus et Michail  Wolosowil kedagi tunnistust selle peale ei ole, et tema kaup müüri töö eest pääwiti olnud, Annus Waddi tunnistaja Miili Mihkeli aga tunnistab, et kaup sülla wiisi olnud 70 kop. a` süld teeb 15 sülla pealt 10 rbl. 50 kop. Annus Waddi aga ülepea 12 rbl maksnud on, jääb Michail Wolosowi nõudmine tühjaks.
See otsus sai kuulutud ei olnud Mihail Wolosow sellega rahul ja anti temal Tal. Säd. Raamat §§ 772, 773 ja 774 põhjusel õppuse kiri kõigi tarwiliste juhatustega suurema kohtu käimiseks selsamal pääwal kätte.
                                  Pääkohtumees  J. Haawakiwwi.
                                        Kohtumees  J.  Stamm.
                                        Kohtumees  J. Tagoma.
                                         Kirjutaja allkiri.
                                  
