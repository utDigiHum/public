Awwinorme Metskoggokonnakohtus sel 24 Novembril 1878.
koos ollid
teine kohtomees Andres Kiik
kolmas kohtomees Jaan Mänd
neljas kohtomes Mihkel Sild
Astus ette Maddis Tamm ja kaebas et Josep Errapart on tedda kõrtsis hulga innimeste wasto teotanud et temma peab omma krunti eest minnewa Aasta Regulerimisse härra Krügerille Rijalinnas 60 Rubla maksnod ollema ja pallus kohhut sedda järrele kuulata se et temma selle teutussega rahhul ei olle.
Josep Errapart räkis et nemmad on omma kruntide pärrast waidlema lähnud ja on siis Maddis Tamm temmale ütlenod, et Maamõtjahärra on temma Josep Errapartile maksu eest suurema krunti annod. kui õigus ja on siis temma ütlenud et rahwas rägiwad et sinna issi olled minnewa Aasta Krügerihärrale 60 Robla Rijalinnas maksnd. 
Maddis Kask ja Jaan Laurisson tunnistasid et nemmad on kuulnud kui Josep Errapart on Maddis Tammele ütlenud. sa maksid Krügerihärrale 60 Robla omma krunti eest agga minna ei olle kellegille koppikod annod.
Josep Errapart räkis weel et Jaan Kiwwi on sedda juttu keige essiti räkinod. et Maddis Tamm on Krügerihärrale 60 Robla maksnod.
Moistos
Seassi jäeb teise kohto pääwa peale selletada ja peab Jaan Kiwwi seie kohto ette tullema.
Andres Kiik XXX
Jaan Mänd XXX
Mihkel Sild XXX
