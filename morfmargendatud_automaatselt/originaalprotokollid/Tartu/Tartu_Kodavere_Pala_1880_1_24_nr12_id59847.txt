 Makohtust sadut kässo peale mis kogokonna kohto  kõrwalistuja siin ülles andis, et kinni wõetu warras Jaan Männi olla seal tunnistanud, et Maddis Abramson olla temale mõisa hobuse riistad kätte annud sai siin järgmine ülle kuulamine tehtud:
I Ette kutsuti Maddis Abramson ja sai küssitud tedda see andmise perrast ja see wastus: tema ei tea sest kaibusest midagi ei tea ja olla see selge et ka tema mitte sel ööl polle wäljas  käinud.
Kui need riistad on  warastud sanud pealegi olla  Jaan Männi teda ähwardand ja lubanud teda 10 aasta perrast lõiwu wõtta ja tema arwab, et kurja tegia  kiusu perrast tema peale kaibab.
II See peale andis tunnistust Johannes Somus et tema muud ei tea kui et Maddis Abramson õhtu kel kuus (wai rohkem) koddu on olnud kui nad magama on läinud ja homiku kel 4 kui nad ülles on tõusnud. Muud tema ei tea.
 Tunnistaja Jürri Soiewa andis protokolli tema ei tea sest  asjast ei ka Abramsonist mitte midagi, ei olle ka rehe jurest tulles Nimetud Abramsoni mitte tähele panud.
 Abram Stamm: andis protokolli: et tema  nähes on Maddis Abramson koddu läinud aga muud tema ei tea peale selle ütles tema kuulnud  olla et Jaan Männi olla Maddis Abramsoni ähwardanud, kas 10 aasta perrast kätte maksa. See kuulmine olla  pütsepp Desseni läbi olnud.
Josep Willik: tunnistas, et Abramson olla  heina weddu peal nalja perrast üttelnud juttu tehes, eks se rumal olla kui kätte andja saab warga poolt üles tunnistud.
 Kohto otsus: Ei olle midagi  Abramsoni  süüst tunnistuste läbbi wälja tulnud.
                            Hindrik Horn    +++
                             Mart Piiri         +++
                             Josep Soiewa +++
