Awwinorme Metskoggokonnakohtos sel. 11 Mail 1879.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jaan Mänd 
kolmas kohtomes Jürri Tomik
Tännasel kohto pääwal leppis Awwinorme Ranna walla innimenne Mart Liiw, Willandi innimesse Jaak Traksiga nendawiisi kokko et Jaak Traks selle hobbose eest mis temma poeg Jaan Traks olli Mart Liiwa käest palkanud Tartu sõita, ja Liiwa kõrtsmik Jaak Leppik sedda olli ärra pantinud; Mart Liiwale 52. maksab., 10 Rubl on jo käes, hobbose wahhetusse läbbi, nüid kohhe maksab Jaak Traks 10 Rubl. Jaani pääwal 10 Rubl ja Mihklipääwal 1879. 22 Robla.Josep Laurisson XXX
Jaan Mänd XXX
Jürri Tomik XXX
