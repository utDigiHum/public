№ 83
Sel 9 Julil 1890.
Kaibas perremees  Mihkel Rattasep, et tema kaubelnu Jaan Wirro ommale sullases 23 Aprilist 1890 kuni 23 Aprilini 1891 selle aija est ollnu 55 rubl raha palka lubatu ja päle selle 2 paari saabit, keda Wirro temma tenistusen ärra pidama pidanu ja 2 wakka Kartofli tema, Rattasepa, seemnega maha panda ja perremehe rõiwas pidada. Jaan Wirro ollewat palka wälja sanu 3 rubl 15 kop ja 1 rubl käeraha. Jaan Wirro lännu 1 Julil s.a tenistusest ilma põhja ärra ja nõudis Mihkel Rattasep säduselik kaubeltu palk ja käeraha Jaan Wirro käest.
Mihkel Rattassep /allkiri/
Jaan Wirro, kes praegu Haslawa wallan ellab, wastutas selle kaibuse päle et perremehe ülles andmine kauba ja palka perrast õige om, tema ei ollewat mitte tenistusest ärra lännu, enge perremees aijanu tema tenistusest ärra ja es luba enemb tenistussi tagasi minna. Perremees ollewat teda sõimanu ja ei olle tööga rahul ollnu.
Jaan Wirro ei mõista kirjotada.
Mihkel Rattasep ütles selle wastu, et tema Jaan Wirrot tenistusest ärra aijanu ei olle, üts kõrd käsknu tema heinamaad parrembest niet, selle päle wastanu Jaan Wirro et "saatan talle nida essi".
Mihkel Rattassep /allkiri/
Jaan Wirro andis küssimise päle ülles, et tema omma kaibust tunistuste läbbi selges teha ei jõua, sest kedagi ei olle jures ollnu.
Jaan Wirro /allkiri/
Päle selle said nemma ärra lepima sunnitu ent lepitust neide wahel es sa.
Mõistetu: et Jaan Wirro tallurahwa säduse ramatu 1860 astast § 383 perra kaubeltu asta palk 55 rubl raha, kartoflide mahapaneki est 5 rubl ja rõiwe est 15 rubl koko 75 rubl ja 1 rubl käeraha 14 päiwa sehen Mihkel Rattasepale peab massma, selle et tema ilma põhjata tenistusest ärra om lännu ja seda selges teha ei jõua, et perremees teda tenistusest ärra olles aijanu.
Se mõistus sai kulutedo.
Mihkel Rattassep /allkiri/  Jaan Wirro ei mõista kirjutada.
Päkohtumees:  Jaan Wirro XXX
Kohtumees:   Jaan Pruks /allkiri/
"  Jakob Saks /allkiri/
