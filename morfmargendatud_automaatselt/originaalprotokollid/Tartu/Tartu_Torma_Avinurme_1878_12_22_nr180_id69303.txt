Sel 22 Decembril 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jaan Mänd
kolmas kohtomees Jürri Tomik
Astus ette Tõnnusare N. 98 tallu perremees Juhhan Aun ja pallus Koggokonnakohhut sedda Protokolli ülles kirjutada et temma omma tallumaade küllest mis 11 Thaldrid 76 Grossi suured on neljas jaggu omma Wenna Jaanile annab. Maadest heinamadest ja karjamaast, ja et se jaggu maad temma Tallumaade küllest regulerimisse ajal saab ärra jautud issi kruntist.
Juhhan Aun XXX
Et Juhhan Aun omma käega kolm risti omma nimme juure on tehnud saab Koggokonnakohto polest tunnistud
Josep Laurisson XXX
Jaan Mänd XXX
Jurri Tomik XXX
