Krimannis 24 Maertzil 1872.
Mõisa wallitsus kaebab et  Willem Laett  on 220 leisikat õlgi  Jaan Raswale müinud 12 rubla eest ja 3 wakka maa on jätnud rukkid teggemata kelle eest mõisa 5 semet nõuab ja 5 sülda risa puid 10 rublad ja üks kodda 4 rublad ja puduwate aijade eest mis Ta Mihkel Läettile on ärra müinud. Se on ütte summa 71 rubla ja aijad 8 Rbl se on summarum 79 Rublad.
Willem Laetti tulli ette ja ütles olleks mulle seaduse perra Jacob Paeiwal üllese antud siis olleksin minna ka se rükki põld mahha teinud agga et mul weel 4 aijastad Contract olli olla ja tal wel mulle üllese anti ei wõinud ma talwel ennam sedda sitta maa rükkid mahha tetta pallutuse puid ollin Mõisa wallitsuse käest nõudnud agga mulle es anto mitte ja selle koddale olli kül koski pudus ja aeda ei olle mul paljo pudus puije eest on kartoffli ma sit ärra wetud.
Lena Pihlapuu tulli 20mal Aprillil ette ja tunnistas: et Naritse ja Orraka wahhe Aed on sel sammal kewwadel ärra weetud ja Oelgi Narritselt Arrakale wiitud.
Marri Jaerw tulli 26 mail ette ja ütles: sael olli kül üks laggunud aid wahhel kus se aid jänud ei tean ma middagi üttelda.
Mõisa wallitsus tulli 26mal Mail ette ja ütles: Willem Laett on essi omma renti Contract katki murtnud selle läbbi et Ta omma renti ei olle jõudnud ärra maksa.
Willem Laetti on tännasel paewal 26 Mai omma kaebus Keiserliko Kihhelkonna kohtust taggasi wõtnud ja saab Mõisa käest 6 rubla 50 kop.
Kohto lauan olli:
Peakohtomees Johan Mina XXX
Kohtomees Karel Poedderson XXX
 "  Johan Raswa XXX
Kirjutaja: W. Kogi /allkiri/
