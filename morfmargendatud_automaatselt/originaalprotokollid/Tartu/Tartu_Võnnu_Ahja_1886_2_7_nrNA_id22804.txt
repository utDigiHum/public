Tulid ette Jaan Kusta ning Peeter Padar ja palusiwad protokolli üleswõtta:
et nemad Emeljan Rõibakowi käest üks elumaja kõige kõrwaliste hoonetega 250 rbl. eest ostnud ja jääwad need hooned selle kolme wenna ja ema Ane Padari omanduseks. Kui üks neist omanikudest wälja tahab minna, siis ei saa tema oma jagu mitte enne wälja maksetud, kui selle pääle wõetud wõlg tasa on.
Otsus: Seda protokolli ülespanda.
Pääkohtumees: P. Kripson
Kohtumees:
Kirjutaja:
