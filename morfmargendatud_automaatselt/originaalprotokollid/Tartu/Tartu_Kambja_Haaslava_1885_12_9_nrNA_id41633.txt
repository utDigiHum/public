Haslawa Kogukonna Kohus sel 9 Detsembril 1885
Man ollit päkohtumees Jaan Link
Kohtumees: Johan Klausen
"   Johan Grossberg
Kaibas Sepp Woldemar Bergman, et temal töö raha 10 Rubl 66 Kop Josep Willemsoni käest sada om, temal om jälle 55 Kop tagasi massa ni et temal sis 10 Rubl 11 Kop ärra massma sunis.
Johan Willemson om 18 Nowembril s.a. 2 Detsembris ja tännases päiwas selleperrast ette kutsutu ja nimelt tännases päiwas selle ähwartusega et kui tima ei tulle mõistab kohus ärra. Josep Willemson ei olle konagi ei ka tänna tulnu ei ka hennast wabandanu selleperrast sai
Mõistetu: et Josep Willemson 8 päiwa sehen se 10 Rubl 11 Kop Woldemar Bergmanile ärra massma peab.
Se mõistus sai Woldemar Bergmanile kulutedo ja olli sellega rahul. Sel 30 Detsembril s.a sai se mõistus Josep Willemsonile kulutedo ja õpetus leht wälja antu.
Päkohtumees J. Link /allkiri/
Kohtumees Johan Klaosen XXX
J. Krosbärk /allkiri/
