Haslawa kogokonna kohus 7 Januar 1881.
Juures istus:
Pääkohtumees  Mihkel Klaos
kohtumees Johan Link
dito  Johan Udel
dito Johan Jantzen
Tuli ette Haslawa mõisa kõrtsimees   Mihkel Rattassep ja  kaebas: tema olewat minewa aasta Märt Wirrole 12 rubl. raha wõlgo andnud, mis temale seni aeani mitte tagsi ei ole maksetud ning palub seda nüüd kätte saada.
 Märt Wirro kaebtuse üle küsitud ütles wälja, et tema Mihkel Rattassepa käest mitte üks kopik ei ole laenanud. Pärast aga ütles: tema on kül Rattassepa käest laenanud, on aga temale jahu wiinud ning temaga tasa teinud.
 Mihkel Rattassep  wastutas selle pääle: tema on kül jahu saanud, on aga  Wirrole jahuraha kohe wälja maksnud.
Mõistus.
Et  Märt Wirro essiteks ütles, et tema midagi  Mihkel Rattassepa käest laenanud ei ole aga pärast seda isse wälja ütles, siis peab tema see 12 rubl.  Mihkel Rattassepa hääks wälja maksma.
Mihkel Klaos
Joh. Link
 Johan Udel
Joh. Jantzen
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud, mis tähele tuleb panna kui keegi kõrgemat kohut tahab käija ja ei olnud  Märt Wirro sellega rahul.
