Awwinorme Metskoggokonna kohtus sel 27 Januaril 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Jaan Mänd
kolmas kohtomees Mihkel Sild
Kronometsawallitsusse ülles andmisse peale sel 19 Nowembril 1877 N 402. sai Mihkel Tamm ette kutsutud ning temmale teada antud et temma peale kaeptus et temma peab krono metsast warrastanud ollema 1 kuuse puu 70 jalga pik 15 tolli kannu pealt mis kronotaksi järrel 64 Rbl 40 koppikud maksab. ja peab temma se trahw nenda kohhe ärra maksma.
Mihkel Tamm räkis et temma ei olle Kronometsas käinud, egga warrastanud, ja ei tahha temma sedda trahwi maksa.
Metsawaht Jakub Wirgi räkis et temma metsast üks puu arrawarrastud, ja on temma siis jalgi möda kunni Mihkel Tamme õue lähnud ja se palk Reia all pukkide peal leidnud. kellel jo nöörid peale lastud, et laudatest leigata. ja on sees Mihkel Tamm tunnistanud et nemad se puu Krono metsast tonud. 
Mihkel Tamm räkis et temma ei olle se puu metsast tonud wait temma wend. Josep Tamm kes Soldat
Moistus
Et Mihkel Tamm seal majas peremees ning temma lubbaga se warrastud palk temma reia alla weedud. ning temma ka omma wennaga ühhes leiwas, siis mõistab Koggokonnakohhus et Mihkel Tamm se puu wastab, ja peab temma se seaduslik trahw 6 Rubla 40 koppikud wälja maksma.
Se moistus sai selle Liwlandi Tallurahwa sadusse põhja peal Aastast 1860 §§ 772 ja 773 kuulutud ning nende kohto käijatelle need seadusse järrel Appellationi õppetusse lehhed wälja antud
Josep Laurisson XXX
Jaan Mänd XXX
Mihkel Sild XXX
