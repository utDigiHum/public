Haslawa Kogukonna Kohus sel 19 Septembril 1883
Man olliwe:
Päkohtumees Jaan Link
Kohtumees Johan Klaosen
Johan Grossberg
Kaibas  Johan Silm, et tema peab Keiserliko 4ta Tartu Kihelkonna Kohtu mõistuse perra Jürri Tamanile 11 Rubl 40 Cop ütte jahu kotti est massma, Kihelkonna Kohus om ütteln et tema seda Haslawa Möldre Mihkel Soo käest nõudma peab, kes praegu Ropkan ellab. Johan Silm pallus seda rahha Mihkel Soo käest sisse nõuda.
Mihkel Soo Ropkast wastutas, et sekõrdse weski poisi Jaag Reimundi läbbi se tullnu om, kes praegu Unipiha Arike Kambja weski pääl tenib ja temal om 30 Rubl selle poisi palka sehen, ja pallus et kohus seda raha Jaag Reimundile massa panna.
Jaag Reimund Unipihast wastutas sel 28 Nowembril et Möldri Mihkel Soo essi om Johan Silma kui ka Jürri Tamani kotti wastu wõtnu sis kui need kottit janwatedu sanu sis om tema Mihkel Soo wastamisse ja Johan Silma ütlemisse perra selle Johan Silmale 2 kotti kätte andno nisama om tema Tamanile 6 kotti kätte andno ja Mihkel Soo om ütte kotti andno ni et Taman 7 kotti kätte sanu om, keda tema ka Weski jurde tonu ollewat,
Mihkel Soo wastutas et tema Johan Silma käest 2 kotti wastu wõtnu ja jahwatanu om, tema ei olle Jaag Reimundile neid kotte näidanu ei olle ka käsknu neid kotte kätte anda, Reimund om omma lubaga need kottit ärra andno.
Sel 28 Nowembril 1883 sai
Mõistetu: et Johan Silm 11 Rubl 40 Cop selle pudu jänu jahhu kotti est massetu peab sama ja peab Möldre  Mihkel Soo 5 Rubl 70 Cop ja weskipois Jaag Reimund 5 Rubl 70 Cop selle Johan Silm heaks 8 päiwa sehen seija sisse massma, selle et selges ei olle wõimalik tetta just kuma süü läbbi se jahu kott hukka om sanu.
Se mõistus sai kulutado ja õpetus leht wälja antu.
Päkohtumees Jaan Link /allkiri/
Kohtumees Johan Klaosen XXX
J. Krosbärk /allkiri/
Mõllembat jäiwat sellega rahul se mass 11 Rubl 40 Cop jäi Mihkel Soo päle, selle et 10 Rubl Jaag Reimund palka Mihkel Soo käen om.
Sel 10 Detsemb. 1884 sai Jürri Taman se 11 Rubl 40 Cop wälja massetu. 
Jüri Taman XXX
