I. Protokoll N: 108 19. Aprillist s.a. astusid ette Gustaw Aunapu ja Marri Saar Josep  Lauk eestkostmisel.
        Gustaw Aunapu näitas Kodawere Kihelkonna wöörmündri härra täht ette, et temal Kihelkonna käest 37 Rubla raha saada olla ja tahtis weel 3 rubla juure maksa, nii et wõlg tasa saab.
         Marri Saar, kad. Tõnnis Saare lesk, nõuab aga ka raha intressi, sest et  Gustav Aunapu olla 10 protsenti raha eest lubanud ja selle hea teo eest, et raha annud, weel pääwi teha. Nõuab 5 protsenti Gustaw Aunapu käest raha eest, mis 70 rubla 2 aastad, 50 rubla üks aasta ja 40 rubla 3 aastad, teeb intressid kokko 2x 3.50= 7 rubla 2.50+ 6 = 15 rubla 50 kop. Gustaw Aunapu teinud tema kad: mehele 6 aasta sees, mis raha tema käes on, 5 pääwa heina ajal, mis eest tema kad. mees 50 kop. pääw arwanud.= 2 rubla 50 kop., mis tema nõudmine Gustaw Aunapuu käest weel 13 rubla.
       Gustaw Aunapuu wastas küsimise pääle, et kad. Tõnnis Saar ei ole tema käest intressi nõudnud, wait nõudnud enne, et  tema Gustaw, pidi igal ajal, kui sõna saab, pääwi tegema, ja et teda ei ole rohkem tööle kutsutud, kui keigest 5 pääwa, siis ei ole temal ka kedagi maksa.
       Tunnistust kummalgi pool üles anda ei ole.
       Kohtu liikmed ühendasid ennast selle mõistmisele:
       et Gustav Aunapu ütlemise järel intressid kaubeltud ei olnud, Aunapu ka raha eest pääwi teinud on, Marri Saar ka intressi nõudmist tõeks teha ei saa, mõistab kogukonna kohus, et Gustaw Aunapu maksab see wõlg raha 40 rubla ja 5 protsenti intressi 40 rubla eest 4. Aprillist s.a. arwatud 4. Detsembril s.a. kad. Tõnnis Saare wöörmündritele wälja.
See mõistus sai  Tall. säd. raamatu § § 773 ja 774 Marri Saar, tema wöörmündri Josep Lauk ja Gustav Aunapuule kuulutud ja maksis Gustav Aunapu 4 rubla 32 kop.wälja ja wõttis kogukonna kohus wöörmündri härra tunnistus raha asemel wasta.
     Otsus: raha 37 rubla wöörmündri härra juures kinni panna.
II. Ettekutsutud Karel Soiewa ja maksis kad. Tõnnis Saare wõlga 20 rubla ära.
     Otsus: rahad omal ajal intressi pääle panna.
                           Peakohtumees J Sarwik
                                 Kohtumees  W Oksa
                                 Kohtumees   K. Hawakiwi
                           Kirjutaja  O Seidenbach.     
