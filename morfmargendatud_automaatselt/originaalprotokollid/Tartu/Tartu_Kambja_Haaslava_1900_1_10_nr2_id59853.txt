Protokoll № 2*
Haaslawa wallakohus 10 Januaril 1900 aastal Haaslawa wallamajas.
Juures:
eesistuja Jaan Rosenthal
juuresistujad:
Märt Käärik
Jaan Grünthal
Kirjutaja: Gustaw Laar
Tuliwad ette kadunud Karl Põdersoni pärijatest Oskar Eduard Põderson, Anna Marie Lell, sündinud Põderson, oma mehe Hans Lell'aga ning alaealine Leonhard Johannes Põderson oma eestkostja Johan Linkiga, ning palusiwad alljärgnewat suusõnalist lepingut kohtu lepingu raamatusse maha kirjutada.
§ 1.
Kadunud Karl Põdersoni pärija Anna Marie Lell, sünd. Põderson, annab sellest rahasummast, mis temal selsamal päewal siin walla kohtus kinnitatud lepingu № 1 paragrahwide 3 ja 4 järele oma isa pärandusest saada on, üks tuhat /1000/ rubla oma kaaspärijale Oskar Eduard Põdersonile üheks aastaks ilma protsendita wõlatähe wasta laenuks.
§ 2.
Lepinguga oliwad rahul ja kirjutasiwad alla lepingu osalised, ja nimelt:
Anna Maria Lell sündinut Põdderson /allkiri/
Hans Kusta poig Lell /allkiri/
Oskard Eduard Põderson /allkiri/
Johan Link Jaagu poig /allkiri/
Leongard Pederson /allkiri/
1900 aasta Januari 10 päewal on ülemal olew leping Oskar Eduard Põdersoni, Anna Marie Lella, sünd Põdersoni, tema mehe Hans Lella ning alaealise Leonhard Johannes Põdersoni ja tema eestkostja Johan Linki poolt, kes kõik walla kohtule tuttawad suusõnaliselt ette kantud ja kõikide nende poolt oma käega alla kirjutatud, mida walla kohus oma allkirjadega tõeks tunnistab.
Kohtu eesistuja: J. Rosenthal /allkiri/
Kohtu liikmed:
M. Kärik /allkiri/
J. Grünthal /allkiri/
Kirjutaja:
Waata protokoll № 3 21 Januarist 1900 aastal!
