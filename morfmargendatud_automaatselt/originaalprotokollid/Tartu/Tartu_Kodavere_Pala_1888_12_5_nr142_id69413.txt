Protokolli N: 132 pooleli jäänud asjus tullid ette Anne Kubja oma isa eestkostmisel ja Kusta Olla. Anne Kubja kordas oma kaebust. Kusta Olla salgas, et see mitte tema laps ei olema, sest tema ei olla kunagi Anne Kubjaga lihaliku elu pidanud.  Anne Kubja rääkinud ise temale, et Karel Annuk selle lapse isa olema. Tema olema kül wahel öösete Jaan Kubja majas käinud, aga kunagi üksipäinis, ikka olnud tõisi poissa ühes ja läinuwad nemad ka jälle ühes ära. Lihaliku elu aga ei olla tema kunagi Ann Kubjaga pidanud.
 Anne Kubja rääkis, et Josep Taggoma näinud, et Kusta Olla tema juures aidas olnud, Karel Annuk, Jürri Welt, Kusta Lillo ja Sohwi Kukk teadwad nendasama, et Kusta Olla tema juures käinud.
 Kusta Olla rääkis, et tema pidada Jürri Welti ja Karel Annuka tunnistuse wasta seisma, sest et need isi Anne Kubja juures käinud. Tema palub Karel Tralla ülekuulata, kes ka Anne Kubjaga lihalikult ühte elanud, kelle ülekuulamise wasta Anne Kubjal ütlemist ei olnud.
         Kohtu otsus: Tunnistajad tõiseks kohtupääwaks ettekutsuda.
                 Peakohtumees J.Sarwik.
                 Kohtumees       M. Piiri.
                 Kohtumees       J. Taguma.
                 Kirjutaja  O Seidenbach    (allkiri)  
