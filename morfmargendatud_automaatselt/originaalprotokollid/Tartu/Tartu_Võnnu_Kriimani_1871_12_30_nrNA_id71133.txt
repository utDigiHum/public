Krimanni Koggokonna kohtus 30 Dec 1871.
 Jürri Klaos tulli koggokonna kohto ette ja kaebas et Minno seapois  Peter Tolk  on tullega wäega ette kaemata ja käib aga lauan ja ka rehhe man nink pallun et Temma trahwitus saaks seperrast et selle läbbi suur õnnetus wõib sündida.
 Peter Tolk  tulli ette ja ütles: laudas ollen ma kül mõnni kord piboga käinud ja põlletanud agga rehhe man ei olle ma mitte käinud ja mul on kõwwa kaas pibul pääl kust mitte tulli wälja ei tulle.
Mõistus: 7 Januaril 1872
Kohhus mõistis: et  Peter Tolk  saab 15 witsa lööki et Ta kui poisikene pibo tullega ülle annetusel wisil ümber käib.
Kohto lauan olliwa:
Peakohtomees Juhan Mina 
Kohtomees:  Karel Põdderson 
"  Juhan Raswa 
