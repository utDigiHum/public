№ 15.
2 Jurjewi Ülema Talurahwa Kohturingkonna 14 Wallakohus Haslawa wallamajas 3 Nowembril 1895.
Juures oli Kohtu Eesistuja: Johan Hansen.
 Haslawa walla liikmed  Jaan Jaani p. Klaos ja Anna Jaani t Klaos abielus Luha tulid ette ja palusid protokolli panda järgmest lepingut:
Jaan Klaos annab üles, et tema surnud isa Jaan Klaos testamenti järele sest 1 Webruarist 1893 a. oma õele Anna Klaos abielus Luha tänasel päewal üks sada rubla osjagu wälja on maksnud.
 Jaan Klaos /allkiri/ 
Anna Klaos abielus Luha tunnistab tõeks et ülewel nimetatud üks sada rubla oma osa jagu Jaan Klaos käest wastu on wõtnud ja lubab testamentiga rahul olla, kelle järele weel kolmsada /300/ rubla saada jäeb.
Anna Klaos abielus Luha ei mõista kirjutada tema pallwe peale kirjutas tema eest alla siin juures oleja mees Juhan Luha /allkiri/ 
Eesseiswad lepingu tegijad on selle walla Kohtule kui tuntud palelikult inimesed ja et  Jaan Klaos selle lepingule oma käega alla kirjutas ja Anna Klaos eest abielus Luha tema pallwe peale tema mees Juhan Luha allakirjutas tunistab sellega 14 Wallakohtu
Eesistuja: J. Hansen /allkiri/
Kirjotaja: K. Laar /allkiri/
/Pitser/
