Kaibas wallawanem Kusta Kokka et minewa aasta kewade on tee rewideerija härra v. Schulz Palla peremehe Josep Soiewa tee jao pääle 2 rubl trahwi pannud ja nõuab nüüd see 2 rubl Josep Soiewa käest kätte saada walla  laegase.
 Josep Soiewa wastas et tema ei ole käsk saanud et see tema tee jago ka tuleb  uuest teha.
Jaan Kubja II wöörmünder ütleb et tema ei ole saanud käsko anda, selle pärast on tema  I wöörmündrit Josep Rosenbergi palunud neid käskusid anda ja Rosenberg on lubanud  käsk anda.
I Wöörmünder Josep Rosenberg  ütleb Jaan Kubja on temale ütelnud, et käsusid anda, Josep Soiewa hobone olnud rewideriga sõitmas selle pärast arwanud ta et Jaan Kubja on juba Josep Soiewa 
poisile ütelnud et see tee tuleb uuest teha. Kui see poisi on ära weenud.
Elias Kubja wastas Jaan Kubja ei ole temale käsko annud et teet teha Jaan Kubja on aga teda kraami kutsunud wedama.
Otsus: Wöörmünder Jaan Kubja oleks pidanud Josep Soiewa poisile, kui see hobust ära wiis kohe ütlema et Josep Soiewa tee jago uuest tuleb teha siis maksab ta selle eest 1 rubla et ta seda ei ole täitnud, Josep Rosenberg oleks pidanud Josep Soiewale käsku andma et tema tee jago tuleb uuest teha ei ole ta seda aga täitnud siis maksab ta selle eest 1 rubl.
Jaan Kubja ei ole rahul.
                             peakohtum Hindrek Horn
                             Kohtum        Otto Mõisa
                             Kohtum        Mihkel Mölder 
 
