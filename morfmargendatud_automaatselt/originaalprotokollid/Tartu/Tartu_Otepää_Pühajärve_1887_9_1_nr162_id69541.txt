Tulliwad kogukonna kohtu ette Juuli Pettai eestkostjaga oma isaga Peeter Pettai'ga ja Kusta Pettermann ja palusiwad kohtu protokolli üleskirjutada, et nemad omas kaebduse asjas, protololl tänasest päewast N157, nende ära lepinud on, et Kusta Pettermann maksab Juuli Pettajale lapse kaswatamiseks 70 (seitsekümmend) rubla. Seda raha maksab Kusta Petermann 5 (wiis) aasta sees ära. Kui ka las peaks enne seda aega ärasurema, siis ika maksab Petermann kõik see raha Juuli Pettajale ära ja ei jää sellest raha summast midagi maha. Raha maksmine sünnib igal aastal wiies jagu.
Kusta Petermann (allkiri)
Juuli Pettai (allkiri)
eestkostja Peeter Pettai (XXX)
