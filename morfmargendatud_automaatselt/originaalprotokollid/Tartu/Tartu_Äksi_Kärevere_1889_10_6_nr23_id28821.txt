Protokolli pääle 6. Oktobrist s. a. No 22 all, Jaan Truusi kohtu wasto kropp olemise pärast.
Mõisteti: Et Jaan Truus peab selle eest, et ta kohtu wasto kõrget kõnet tõstis ja krop oli, 2 rbl. trahwi waeste laadiku maksma.
See mõistus sai sääduslikult kuulutud.
Pääkohtumees: J. Kaal [allkiri]
Kohtumees: J. Kruus XXX
ja J. Truus XXX
Ära kiri I T. Ülema Talurahwakohtusse saadetud 24. Jan. 1890. No 23
