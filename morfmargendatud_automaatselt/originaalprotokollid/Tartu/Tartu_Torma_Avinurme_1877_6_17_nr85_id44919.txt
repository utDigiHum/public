Awwinorme Metskoggokonnakohtos sel 17 Junil 1877
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Mihkel Sild
kolmas kohtomees Jaan Pukk
Kajusaare perremehhe Jaan Lebeni kaeptuse asjas omma wenna Jakob Lebeni wasto et se karjamaast üks suur tük rookinud. ja ommale heinamaad tehha tahhab. ollid kohtomehhed Josep Laurisson, Jürri Tomik, Jaan Pukk ja Walla wannemb Juhhan Koppel seal käinud wahhet teggema ja neile wahhet näitanud. kui paljo kubki selle karjama küllest ommale saab ja sai selle pärrast tännasel kohto pääwal Jaan ja Jakob Lebenile öldod et. kui kumbki selle Walla ammeti meiste poolest tehtud piirist ülle lähheb 10 Rubla wallakassa trahwi maksab.
Josep Laurisson XXX
Mihkel Sild XXX
Jaan Pukk XXX
