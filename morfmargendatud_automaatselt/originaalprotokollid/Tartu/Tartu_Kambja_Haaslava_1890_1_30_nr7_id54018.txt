№ 7
Haslawa sel 30 Januaril 1890.
Selle Kogukonna Kohtu kindmas jänu mõistuse perra sest 14 Maist 1884 om Jaan Märtin perrandusest Jürri Luha heas sisse nõuda 60 rubl ja päle selle selle raha 5 asta intressi 18 rubl kuni Detsember 1889 ja se jagu pääle , selle et Jaan Martini perrandajat seda wõlga seni aijani massa ei olle püidnu ja Jürri Luha seda raha sisse nõuda pallunu om sai tännasel päiwal Jaan Martini perrandusest kohtu keelu alla pantu:			Värt		Rubl			Kop		1 weo wanker			3					1 ...ellu (ei loe välja)								7 lamast								8 zigga			50					1 kuhi kartoflit								
Selle jures sai Jaan Märtini laste wölmündrele Jaan Opmanile ja lesjale ütteldu, et kui se wõlg kuni 12 Webruaris s.a ärra massetu ei olle need asjat oksioni wisil ärra müidut sawa.
Otsus: Seda protokolli ülles panda.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Pruks /allkiri/
"  Jakob Saks /allkiri/
"  Jaan Treial /allkiri/
