Tulli kohtu ette Widrik Johannson ja kaebas, et Mart Lepp on tedda ja temma naist wargas teotanud ja öelnud, et temma naine on kartoli kobas käinud kartulid warrastamas ja nõuab, et Märt Lepp selle est trahvwitud sab.
Mart Lepp ütleb, et lapsed kolis räkinud, et Widrik Johansoni lapsed on öelnud, et nende emma ja emma wend on käinud kartolid tomas.
Michkli Elleri tüttar Ello Eller ütles, et temma olli kuulnud, kui Widrik Johansoni tüttar olli temma poega küssinud, kosst need kartolid on seie tulnud meil nisuggusid suggugi ei ollnud, temma olli üttelnud, et temma onno ja emma olliwad tonud.
Kai Raija tunnistab, et temma olli kige essite leidnud, et Märt Leppa kartoli koop olli ärra kisstud ning olli sedda assja kohhe Märt Leppale teada andnud. 
Sai selle peäle mõistetud, et Märt Lepp keddagi Widrik Johansoni käest nõuda ei wõi middagi nõuda, selle perrast, et sellged tunnistust ei olle.
