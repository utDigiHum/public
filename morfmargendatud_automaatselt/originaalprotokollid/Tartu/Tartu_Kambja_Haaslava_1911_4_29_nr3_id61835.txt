№ 3.
1911 aasta Aprillikuu 29 päewal ilmusiwad Haaslawa wallakohtusse  Jurjewi maakonnas Liiwi kubermangus, Haaslawa walla liige 
Jaan Rosenthal, elab Haaslawa wallas Kirbu talus ja  Kodijärwe walla liige Johan Tõnise p. Kõõrna, elab Haaslawa wallas Kirbu talu maa pääl paluwad alljärgnewat suusõnalist rendilepingut kohtu lepingute raamatusse sisse kirjutada ja ustawaks tunnistada:
§ 1.
Jaan Rosenthal nagu  Kirbu talu  omanik annab oma heinamaa ülemast otsast selle osa maad, umbes 1½ wakamaad, mis enne  Jaan Läätsa käes tarwitada oli, Johan Kõõrna kätte tarwitada kolmekümne järjestikku aasta pääle s.o. 23 Aprillist 1911 a. kuni 23 Aprillini 1941 a.
§ 2.
Kõõrna wõib selle maa pääle elumaja ja tõisi maja pidamiseks tarwilisi hooneid üles ehitada ning maad nii tawitada nagu ta ise hääks arwab, kuid siiski mitte nii, et maa selle tarwitamise läbi oma wäärtusest langeda wõiks.
§ 3.
Kõõrna piab iga aasta 1 Aprilliks terwe rendi aasta ette rendi kümme (10) rubla ära maksma. Selle rendi wõib Kõõrna ka tööga tasa teha.
§ 4.
Pääle iga aastase rendi, mis § 3es nimetatud, wõtab Kõõrna weel oma pääle kohustuse Kirbu talu heinamaad selles talust lahus olewas tükis, kui ka § 1-ses tähendatud maa tükk seisab, wahtida ja juhtuwate kahjude üle talu omanikule wiibimata teatama ja kurjategijate kinniwõtmise ning tagaajamisel abiks olema.
§ 5.
Kõõrnal ei ole mitte õigust ilma talu omaniku lubata oma hooneid kellegile edasi müüa selleks et uus omanik nad selle koha pääle jätab. Ka ei tohi Kõõrna enda juures kellegile kuritegudes kahtlustatud isikutele öömaja ega korterit anda.
§ 6.
Omanikul on õigus Kõõrna käest maad tagasi wõtta ja Kõõrnat oma hoonetega säält ära minema sundida, kui Kõõrna renti õigel ajal ära ei maksa ehk muid oma pääle wõetud kohustusi ei täida. ja kui tema warguse asjadega tegemist hakkab tegema.
Jaan Rosenthal /allkiri/ 
Johan Kõõrna  /allkiri/ 
1911 a. aprillikuu 29 päewal Haaslawa wallakohus selle läbi tunnistab, et ülemal olew rendileping suusõnal selle kohtule awaldatud on, pärast raamatusse sisse kirjutamist pooltele etteluetud ning poolte poolt alla kirjutatud sai, ja mõlemad lepingu tegijad s.o. Jaan Rosenthal ja Johan Kõõrna  kohtule isiklikult nagu seaduseliselt lepinguwõimulised inimesed tuttawad on ja selle wallakohtu piirkonnas elawad.
Kohtu esimees:  W. Põderson /allkiri/
Kohtu liikmed: 
 J. Laber /allkiri/
 P. Luha /allkiri/
