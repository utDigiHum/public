Sel 24 Nowembril 1886
Kaibas Ann Nigol  Jaan Linki man ollen, kes Kagwere mõisan ellab, et temal suwe tenistus 39 töö päiwa ja 10 rehe päiwa est a 30 kop Johan Zobeli käest sada om. Kätte om tema sanu 4 wakka Kartoflit, mis 2 Rubl 60 Kop massab ja päle selle ollewat temal 1 pund linno sada ja pallus et kohus seda massma sunis.
Johan Zobel wastutas selle päle, et Kaibaja kül päiwi tennu, selle est ollnu kaibajal 5 wakka kartoflit edimane asta ja tõine asta 1 wakk kartofli man ollnu, raha massa es olle kaubeltu, päle selle andno tema 4 wakka kartoflit ja 10 toopi Ernit. Linno ei olle kaubeltu. 2 Lamast ollnu suwwi otsa tema karjan.
Ann Nigul andis tunistajas ülles Jaan Klaos Haslawast Utte tallost ja Perto Marri Krimanist kes teadwat .
Otsus: et tunistaja ette kutsuta om.
Kohtumees: Jürri Kärn XXX
"  Johan Opman XXX
"  Jaan Hansen /allkiri/
"  Johan Raswa XXX
