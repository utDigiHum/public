Krimani koggokonna kohtus sel 14 November 1869.
Mäksa  koggokonna revisionist Endrik Nielus nüid Tartus ellamas tulli sel üllemal nimmetud päewal koggokonna kohto ette ja kaebas  Kultna  rentniko  H. Freyfeldti peale et minnewal  kewwadel jäi minno lehm koggokonna kohto mõistmise perra et minno lehm 4 rubla 60 kopka eest senna pantis jäi ja minna käisin Petri päewa aego ja tahtsin omma lehma kätte sada agga  Freyfeldt  wiskas mind uksest wälja nink ei annud mulle mitte minno lehma kätte, minna tahtsen se lehm ärra müija ja siis olles  Freyfeldti wõlg ja  Ado Lenziuse rahha ka ärra maksnud sest minnol enne mitte rahha maksa ei olnud.
Nüid nõuan minna omma lehma piima antmist arwata sest koggokonna kohhus teab et suwwel lämmi piima toob 5 kopka h. maksab ja igga päeiw se minno lehm 4 toopi antis piima, mis kohhus se eest arwab minnol  Freyfeldi käest sada ja tahhan omma lehm kätte saata.
Siis sai  H. Freyfeldt ette sel 14mal Novembril kutsotud ja ta ütles: kui  Endrik Nilus mulle minno mõistetud wõlg 5 rubla 90 kopka ärra maksnud olleks, olles  minna temma lehm jo ammo kätte annud, sest et Nilusel minnule maksa on ei wõinud minna mitte enne lehm kätte anda kui rahha käes.
Se peale ütles Koggokonna kohhus: Endrik Nilusel om Aprili kuu mõistmisse perra 5 rubla 90 kopka Freyfeldile maksa ja piddi enne wõlg kätte maksma siis lehma otsima minnema.
Mõistus: Krimanni  kohtus 14 November 1869.
Koggokonna kohhus mõistis: se eest 20 Rbl 50 kop sest wõlla rahhast mahha mis  Nilus sel Freyfeldile maksa olli, ning  Nilus peab  H. Freyfeldile nüid tänna sest päewast 3 rubla 40 kopka ärra maksma et siis lehma kätte saata wõib agga kui mitte maksetud ei olle wai  Freyfeldtile nõutud  19 rubla eest müitud siis maksab egga päewa eest ellaja söögi rahha, sest se 2 rubla 50 kopka on suwwe piima andmisse eest arwatud.
Kohto lauan olliwa:
Koggokonna Peakohtomees Jürri Bragli XXX
"  "  Abbi "   Ado Lenzius XXX
"  "  Abbi "   Endrik Sulbi XXX
Mõllemad jäid selle mõistmissega rahho.
Kirjutaja W. Kogi /allkiri/
