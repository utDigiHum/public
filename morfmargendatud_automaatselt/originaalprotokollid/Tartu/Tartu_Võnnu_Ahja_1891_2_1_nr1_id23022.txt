Anna Sulp, Jakobi lesk tulli ette ning lubas oma lastele järgmist pärandusest:tütar Liisale			jääb tare ning üks lehm		tütar Mariel'e			saab 10 rbl. raha		poig Josep'ile			saab 20 rbl. raha		
aga nimitud lapsed rõiwad kõik ses tares elada ja ei wõi neid keegi sealt ära ajada kui nemad minna ei taha seeni kui täisealiseks saawad. Ema Anna Sulp jääb ka sinna elama nii kaua kui elab ning maksab tütar Liisa mees, Johan Korrok see lubatud raha poea ning tütrele wälja kui nemad täisealisus saawad.
Et tema sellega rahul om tunistab allkirjaga: Johan Korrok &lt;allkiri&gt;
Anna Sulp ei oska kirjutada.
Otsus: Seda protokolli ülespanda.
Piakohtumees Kotta Piir &lt;allkiri&gt;
Kohtumees: Kusta Saarwa &lt;allkiri&gt;
Kohtumees: Juhan Rootslane &lt;allkiri&gt;
Kohtumees: Hendrik Nagel &lt;allkiri&gt;
Kohtumees: Kusta Nagla &lt;allkiri&gt;
tütar Marie surnud. Joosep 10 rbl. juba kättesaanud nii et weel saada jääb 10 rbl. Ahjal 12 Now. 1898.a
Joosep Sulbi &lt;allkiri&gt;
Kohtuliige: K. Mertens &lt;allkiri&gt;
