Lena Kübbar omma eestseisja Josep Kook juuresollemisel kaebab omma wenna Karel Kübbar peale se olla juttu sees puuga 3. korda löönud ja wokki purrustanud ärra, kelle eest temma 20. rubla nõuab. ja peab wokki ärraparrandama.
Karel Kübbar kostab kaebduse peale, et Leena Kübbar olla temma warras, tüdruk lorranud temmale ette et temma lubbanud Tikko Adami hobbusele liiwa kõrwa aada, ja et seppa lehm ärralõppeb, wõtnud puu ja wissanud Leena Kübbarad, agga hoop polle tüdruku külge lähnud waid wokki külge.
Ann Silm tunnistab, et temma muud pahhandust polle nähnud, kui sedda kudda Karel Kübbar Leena Kübbara wokki ratta Katki löönud.
Tunnistus sai awwaldut.
Kohhus moistis:
Karel Kübbar peab ärra lõhhutud wokki ratta ärra parrandada laskma, ja saab selle trahwiks 24. tunni peale koggukonna wangihoonesse kinnipandut saama
/: Otsus kuulutud sel 27. Mail 1883. a, Tallorahwa seaduse raamato §§ 772 ja 773 juhhatusel ja oppuse kirjad wäljaantud:
J. Pringweld [allkiri]
J. Kokka  [allkiri]
Karel Kaddak  [allkiri]
Karrel Torrowerre [allkiri]
