1909 goda janvarja 15 dnja daju siju rospisku ja, krestjanin StaroVrangeljskoi volosti Jogan Karlov Rozental, v tom, što polutšil ot brata mojego Petra Karlova Rozental summu trista (300) rubljei, kakovõm dengi ja imeju polutšitj ot Petra Rozental soglasno §3 nasledorazdeljnago dogovora naslednikov pokoinago otsa našego Karla Romanova Rozental ot 4 dekabrja 1908 a. za N 8 utveržden. Vrangeljskim Volostnõm Sudom. Pri etom izljavljaju soglasije na pogašenije sih trjehsot (300) rubljei po krepostnõm knigam JurjevoVerroskago krepostn. otdelenija.
Krestjanin: im Juhan Rosentaal [allkiri]
Tõsjatša devjatsot devjatogo goda janvarja pjatnadtsatago dnja, akt sei javlenj k zasvideteljstvovaniju v Vrangeljskomu Vol. Sudu, v pomeštšenii suda, nahodjaštšemusja v St.Vrangeljskom volostnom dome, krestjaninom StaroVrangeljskoi volosti, Jurjevskago ujezda, litšno volostnomu sudu izvestnõm i imejuštšim zakonnuju pravosposobnostj k soveršeniju aktov. pri etom vol. sudj udostoverjajet, što akt sei protšitan Rozentalju i sobstvennorutšno podpisanj im. 
Po aktovoi knigi No 1.
Predsedatelj suda: K. Pala [allkiri]
Tšlenõ suda: J Virman? [allkiri] D. Gart? [allkiri]
Pisarj JWeske [allkiri]
