Tännasel päewal tulli ette Jaan Rattasepp Maddis Andressoni asjas ja tunnistas, et temma selgeste ei tea kui paljo Maddise Andressoni rahha on Kirjotaja kätte jänud, agga temma arvab, et on sisse jänud,
Kohto moistus, et Maddis Andresson maksab 2 Rubla 30 kop Peter *** wõlga ärra 8 päewa pärrast
J. Pringweld [allkiri]
J. Kokka  [allkiri]
Karel Kaddak  [allkiri]
Karrel Torrowerre [allkiri]
Kirjutaja Braunbrück wõttis appellatsioni kirja 3. Juunil c. wälja.
