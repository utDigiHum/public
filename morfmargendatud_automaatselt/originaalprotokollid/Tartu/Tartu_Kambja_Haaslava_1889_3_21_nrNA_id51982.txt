Haslawa sel 21 Märzil 1889
Keiserliko 4 Tarto Kihelkonna Kohtu kässu päle sest 14 Märzist s.a Nr 1904 sai tänna Johan Walge warrandusest kohtu keelu alla pantu:
1 must emis, 3 musta orrikut, 1 walge orrik, 2 rabbamis penki, 2 maa atra, 1 lina ürs, 2 moldi, 1 puutelgiga wankrit, 1 kõrb ruun hobune, 2 raud rege, 1 pull, 1 õhwakene, 4 lehma, 5 lamast ja 2 wastsed wokki.
Otsus: Seda protokolli ülles panda.
Päkohtumees: Jaan Wirro XXX
Kohtumees: Jaan Toom  XXX
