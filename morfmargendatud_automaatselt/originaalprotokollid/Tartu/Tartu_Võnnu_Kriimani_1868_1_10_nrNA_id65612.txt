Krimannis  sel 10 Januaril 1868.
 Juhan Kink tulli koggokonna kohto ette ja kaebas et  Jaak Turba käest temmal 9 Rubla 5 kop sada on mis ta nüid kätte tahhab sada.
Jaak Turba  tulli ette ja tunnistas et temmal on 9 Rubla 5 kop Juhan Kinkile maksa ja pallus et Juhan Kink temmaga eddesi piddi kannataks ning ütles kui kewwade pole rahha tenin siis maksan kohhe ärra se peale lubbas Juhan Kink Jaak Turbaga kannatada kunni Jürripäewani 1869.
Man olliwa:
Peakohtomees Josep Kerge  
Abbi "  Peter Püür 
Wöörmünder    Peter Krüünthal 
Kirjutaja: W. Rauping /allkiri/
