Tänasel pääwal  oliwad kohtumehed Jürri Tõltsepa majasse läinud tema warandust neli rubla sisse riisuma, nii kui protokoll 19 Webruarist s.a. N. 49 seda nõuab aga Tõltsep oli kohtumeeste eest ukse lukku pannud ja  mitte sisselasknud.
 Otsus niisuguse wastupanemise perast maksab  Jürri Tõltsep üks rubla waeste laeka 8 pääwa sees ära. Se otsus sai Jürri Tõltsepale kuulutud.
                           Peakohtumees   J. Sarwik.
                             Kohtumees:      J. Stamm.
                             abi Kohtumees J. Tagoma.
                              Kirjutaja G. Palm.
