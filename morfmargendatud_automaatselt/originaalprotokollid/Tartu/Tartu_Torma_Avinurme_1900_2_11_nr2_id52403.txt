Protokoll
Awwinorme Wallakohtus sel. 11 Vebroaril 1900.
juures olid
Eesistuja Jürri Reisenpukk
kohtumees Jaan Jaakson
kohtumes Mihkel Tomik
Tulid ette Awwinorme Walla ja Adrako küla taluperemehed Jaan Lambasaar Jakobi poeg ja Jaan Kukk Hanso p. ja annid Wallakohtule teada et nemad on kahekeisti oma talukohad ära vahetanud nenda viisi et nemad nüid kohe teine teise taluse elama lähevad; järgmiste tingimistega:
Mina Jaan Kukk Hanso poeg, Awwinorme wallas ja Adrako külas. Hanso talu N 284 omanik mis talu 16 75/100 dessatini suur wahetan Awwinorme walla talu peremehe Jaan Lambasaare Johan pojaga ära wastakutte keigi selle krundi peal olevate honete, heina küinide ja aedadega. ja jäeb agga selle kruntiküllest üks karja maa tukk nimega linaalekere karjamaa kedda mina olen oma wenna Josep Kukke Hanso pojale maja asemeks ja elukohaks temale ja tema lastele igavese aja peale annud. Josep Kukke kätte igawese aja peale. mis ette Josep Kukk Jaan Lambasaarele igga Aasta 4 Rubl renti maksab ja ei ole selle külles ei mingisugust muud teist kõrwalist maksu; ja annan mina Jaan Lambasarele 20 kolme ja kolme ühe poole wersukad palki küini ehitamise tarvis ilma maksuta ja ei ole nendel teineteisele ei ühtegi pealis raha maksa; Ka on tema naine Lenu Kukk ja tema lapsed selle vahetamisega rahul. mis pära tema Wallakohut palub nende kaupa Lambasarega kinnitada; Jaan Kukk ei moista kirjutada 
Mina Jaan Lambasar Juhani p. Awwinorme wallas ja Adrako  külas N 268 talukoha omanik wahetan oma talu krunt soosaare mis 19 16/100 dessatini suur Awwinorme valla taluperemehe Jaan Kukkega wastakutte ära ilma pealis rahata. ja luban et se maatük kedda Jaan Kukk oli oma wenna Josep Kukkele iggavese aja peale maja platsiks ja elukohaks annud; ka igaveseks ajaks Josep Kukke ja tema pärijatte kätte pruki jaeb selle renti ette neli rubla Aastas mis vahetusega tema naine Anna Lambasaar ja poeg Johannes kes 16 Aastad vana rahul on ja palub tema sellepärast Walla kohut nende kaupa Jaan Kukkega kinnitada, neli noort Õunapud wõttab tema oma wana koha pealt kaasa uwe koha peale.
Jaan Lambasaar ei moista kirjutad
Wiimaks leppisid kauba tegijad weel kokku et kui se peaks olema et üks nendest peaks tahtma oma kaubast ja sellest kohade vahetamisest taganeta siis maksab kontrakti murdja teisele üks sadda rubla kahju tasumist ja peale selle veel muud teised kulud mis teisel on olnud ja peab se wahetamine nende ja nende parijatte kohta igavese aja peale maksma
Lena Kukk Jaani naine andis kohtule üles et temal selle wastu ei ühtegi räkimist ei ole. et tema mees oma talukoha Jaan Lambasaarega ära vahetab ja on tema sellega rahul.
Lena Kukk ei moista kirjutada 
Anna Lambasaar Jaani naine andis kohtule üles et temal selle wastu ei ühtegi räkimist ei ole et tema mees oma Soosaare talukoha Jaan Kukkega ära vahetab ja on tema sellega rahul.
Anna Lambasaar ei moista kirjutada
Selle kauba kinnitamiseks on kaubategijad omad nimed selle protokollile alla kirjutanud.
Jaan Kukk ei moista kirjutada tema palwe pele kirjutas alla Joanes Raja (allkiri)
Jaan Lambasaar ei moista kirjutada tema palwe peale kirjutas Josep Kuk (allkiri)
1900 Aastal Webroarikuu 11 pääval on see eesseisaw suusana lepping ja kaup Awwinorme Wallakohtule Awwinorme Walla majas kohtu protokolli üles kirjutamiseks ja kinnitamiseks ette kantud Awwinorme Walla inimeste taluperemeiste Jaan Kukk Hanso poja N 284 Hanso talu omanikku ja Jaan Lambasaare Juhani poja Soosaare N 268 talu omanikku poolt kes Awwinorme wallas oma talukohtade peal elawad, ja on need kaubategijad Wallakohtule tuntud ja teatud ja nemad õiguse võimelised kaupasid ja leppinguid teha. ning selle pärast nende kaup ja lepping tanasel pääwal kohtu Akti ramatuse N 2 all üles kirjutud ja kohtu poolest kinnitud sanud. ja peawad need kruntide wahetajad oma wahetamist Jürjevi ​​​​​​Werr​e Rahu kohtu krepposti jauskonnas kinnitada laskma ja on nende kaubategijatte eest kes kirjutada ei moista nende palve peale Jaan Kukke eest Awwinorme inimene Johannes Raja ja Jaan Lambasaare eest Awwinorme Walla inimene Josep Kukk allakirjutanud 
J. Reisenpuk (allkiri)
Jaan Jaakson (allkiri)
M. Toming (allkiri)
Kirjutaja Schulbach (allkiri)
Kopia sel 10 Septembril 1903 Josep Kukkele wälja antud ja Stembel margid 15 kp väärtes pealepantud
Kirjutaja (allkiri)
ära kiri ühes wene keele tõlkega 9 Aprilil 1907 a. Jaan Lambasarele wälja antud.
Kirjutaja Tõldsepp (allkiri)
Ära kiri ühes wene keele tõlkega 22 aug. 1908 a Jaan Kukkele wälja antud.
