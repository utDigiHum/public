Protokolli peale 20 Augustist s. a. N.164 sai ettekutsutud Johannes Peramets ja wastas küsimise peale et tema mitte Josep Kokkad omale woorimeheks palunud ei ole. Josep Kokka tahtnud ise tema käest õlut juua ja sõidutanud teda  sellepärast. Palla mõisast kutsunud Josep Kokka weel teda wägise  Tuhal õllepoodi, seal juures oli Karl Annuk tunnismees olnud.
 Ettekutsutud Karl Annuk ja wastas küsimise peale et mitte Kokka waid Peramets Tuhale sõita tahtnud sest Josep Kokka oli kodu sõita tahtnud ja Perametsa palunud teda lahti laska aga Peramets ütelnud, kui sina minu woorimees oled pead sa sõitma . Sellepeale oli tema ise  Kokka käest ohjad äratõmbanud ja ise Kokka hobuse Tuha poole ajanud kes mitu kord weel ka selle tee peal  ümberkäinata tahtnud. Karl Annuk oli Perametsa soowi peale nendega ühes Tuhale sõitnud, sealt oli Peramets üks korw õlut peale pannud ja wägise Josep Kokkad sundinud Soiewa talusse ühes sõitma. Tunnistajad kes selle kohtu poolt ülekuulatud saanud  saiwad Johannes  Perametsale wastu. Josep Kokka oli wälja jäänud. 
 Otsus: Josep Kokka  tunnistuste awalduseks ettetarwitada.
                     Peakohtumees J. Sarwik.
                     Kohtumees       J. Stamm.
                     Kohtumees       M. Piiri.
                     Kirjutaja GPalm. 
    
