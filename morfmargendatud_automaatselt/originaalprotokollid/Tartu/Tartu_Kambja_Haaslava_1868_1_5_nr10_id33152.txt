Koolmeister Peter Maddisse kaibap et Johan Tenn warastanut kolimaja aggu 5 koormat ärra. Peter Maddisse nowwap akku taggasi.
Johan Tenn wastutap, et temma ei olle mitte koolimaja akku warastanud, nink ei lubba assemel ka wija.
Mõtsawaht Jaak Reimann tunnist, et Johan Tenn warastanud kooli maja akku.
Kohtomees Jaan Purrik üttel ka et neid kolimaja akku om temma löidnut Johan Tenni mant.
Johan Tenn üttel nuit nende tunnistusse pääl et temma man ellawat üks soldat, se warastanud neid akku.
Mõistus: Johan Tenn on mi hää kui warras, siis temma om essi neit akku pölletanud ja paltanud, temma piap warsti 5 koormat akku assemel wiima.
Pääkohtomees Johan Hansen XXX
Kohtomees Ado Ottas XXX
Kohtomees Jakob Jansen XXX
Moistus sai kuulutud nink suuremba kohtokäimisse öppetusse kirri wälja andtu.
