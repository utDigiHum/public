 Krimanni koggokonna kohtus 23 Octobril 1870.
 Jaan Podderson  tulli sel ette ja kaebas, et minnul olli minnewa aasta kui Adam Lip minno jures sullasest olli minno majast ärra kaddunud 2 suitse 2 nahk ohjad 2 seddulka rihma ütte taose ja schlensi wastne nukka Sag 1 adder ja 1 seddulkas ja nüid ollin minna se seddulkas  Willem Lätti käest kätte sanud mis 
 Adam Lip  olli  Willem Lattileannud ja et Temma se seddulkas on warrastanud nõuan minna nüid keik omma riistad temma käest mis mul olli ärra kaddunud ning nõuan Temma käest nende riisto eest 20 rublad.
 Adam Lip tulli ette ja ütles: kui minnewa kewade omma last läksin matma siis läksin Willem Latti pole ja Laett wõttis siis sedulka hobbuse pealt ärra nink ütles se on minno seddulkas ja kül ma isse Põddersonile ütlen et ollen seddulka ärra wõtnud.
Willem Lätt tulli ette ja ütles se olli minno enda seddulkas sest minna andsin Põddersonile sedda seddulkas käia ja ollin kül Adam Lippi käest Temma hobbuse pealt ärra wõtnud ja käskisin üttelda et ollen ärra wõtnud.
Jaan Põdderson tulli ette ja ütles, siis olli Temma minno seddulkas kui Temma minno käes olli et minna weel mitte sulle ärra annud ei olnud ja kui Lätt olleks sedda seddulkad perrast nõudma hakkanud siis olleksin minna wistist maksma piddanud. Ja minna küssisin mitto korda Lippi käest et kus se seddulkas on jänud agga Lip wastas et Temma ei teab mitte. 
Adam Lip tulli ette ja tunnitas sedda kül enda süiks et Ta mitte polle perremehhe Põddersonile üttelnud et Latt olli seddulka ärra wõtnud.
Mõistus: 
Koggokonna kohhus mõistis, et  Adam Lip peab 1 Rubla waeste ladiko maksma seperrast et ta perremehhe wasto ärra salgas ja mitte ei olnud üttelnud kus seddulkas olli jäänud agga teisi risto ülle ei olle  Põdderson õigus nõuda seperrast et Temmal ühtige tunnistust ei olle et Lip on Temma riistad warrastanud.
Kohto lauan olliwa:
Peakohtomees Jürri Bragli XXX
Kohtomees   Juhan Mina XXX
"   Ado Lentzius XXX
