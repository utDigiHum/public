Krimani koggokonna kohtus sel 3mal Julil 1870.
 Jaan Luhha   Roijo körtsimees tulli sel 19mal Junil kohto  ette ja kaebas  Jaan Otti peale et temmal minnole kõrtsi wõlga kui ka nenda sammo mis antus sai 11 rubla 63 kopka wõlga om, mis minna nüüd kohto läbbi kätte sata tahhan.
 Jaan Otti tulli ka sel 19mal Junil ette ja ütles: minna ei olle ennamb Jaan Luhhale kui 4 rubla 40 kopka 2 wakka kaerade eest maksa agga muud wõlga ei tea minna middagi maksa ollema.
See peale tulli  Jaan Luhha  ette ja ütles: se om kül õige mis minna ülles annud ollen, sest minnol on jo tunnistaja  Josep Kerge, Andres Ossep, Jaan Raswa, kes tunnistata wõiwad.
Siis sai tunnistaja Jaan raswa sel 3mal Julil ette ja ütles: Jaan Luhha ütles kül et temmal Jaan Otti käest 11 rubla saija om ja se ütleminne olli Suwwike tallo ussaija peal, kui ma Jaan Otti trossi wima läitsi siis kuuli, agga Jaan Otti es ütle selle peale middagi, muud kui kül ma sulle ärra maksa, agga kui paljo sedda Jaan Otti mitte es nimmeta.
Tunnistaja Andres Ossep tulli 3mal Julil ette ja ütles: Jaan Luhha tulli ja tahtis Jaan Otti käest rüa kott wõlla eest ärra wõtta, agga Jaan Otti ütles Luhhale egga ma ommetegi ilma otsa ei lä, kui ma taggasi tulle siis saad jo kätte, sest kui ma Willem Laetti käest kätte omma 4 külmiddi rükki saan, kui ka ketisse moona ja lammaste rahha, mis 5 kopka lamba hoidmisse eest iks mastas mis mul sulle siis weel peale maksa tulleb siis maksan.
Wanna Kustest tunnistaja Josep Kerge tulli ette ja ütles: Jaan Luhha tulli Suwwike Maijasse kui Jaan Otti omma trossiga sealt miinnema hakkas ja tahtis 4 wakkalist rükki kotti ärra wõtta agga Jaan Otti ei annud mitte ning kutsusiwad siis mind sinna manno ja Jaan Otti ütles siis et minnul on Jaan Luhhale 11 Rubla 60 kop maksta ja kui ma enne Jürri päiwa tullen seie maja kattusid parrandama siis töön ja maksan se rahha sule illusti ärra ning selle peale jäi siis nende lepping et enne Jürripäiwa Luhha se rahha ärra maksab.
Tunnistaja Willem Laett tulli ette ja ütles: enne Jürri päewa teggiwad Jaan Luhha ja Jaan Otti enda rechnung ja siis jäi Jaan Ottil Jaan Luhhale maksta 11 Rubla kus ma man ollin ja sedda asja selgelt tean ja wõib olla et peale se olli weel siis se 60 kop wõlgo teinud. Jaan Otti on 3, 17 ja 31sel Julil seis kohto mitte tulnud.
Mõistus 31sel Julil 1870.
Koggokonna kohhus mõistis tunnistus meeste tunnistust peale et Jaan Otti peab 2 näddali perast se nimmetud 11 Rubla 60 kop Jaan Luhhale ärra maksma.
Peakohtomees Jürri Bragli 
Kohtomees  Juhan Mina 
Abbi  "  Jaan Poedderson 
 Jaan Ottile sai omma kohto otsus kulutud 23 Octobril 1870
