Haslawa Kogukonna Kohus sel 1 Junil 1887.
Man ollid: 
Kohtumees: Jürri Kärn
"  Johan Opman
"  Johan Raswa
Keiserliko 4 Tarto Kihelkonna Kohtu kässu päle sest 16 Märzist s.a № 1504 ja 4 Maist 1887 № 2620 sai tännasel päiwal perremees Johan Janseni kätte tema poja Eduart Jansen palk 20 rubl Marri Semeli heas kinni pantu, selle et Eduart Jansenil midagi warrandust ei olle, kost se raha sisse olles risoda wõinu. Johan Jansen lubas 29 Septembril s.a se raha ärra massa mis sis Marri Semelile sis ärra saab massetu.
Otsus: seda protokolli ülles panda.
Kohtumees  Jürri Kärn XXX
" Johan Opman XXX
" Johan Raswa XXX
