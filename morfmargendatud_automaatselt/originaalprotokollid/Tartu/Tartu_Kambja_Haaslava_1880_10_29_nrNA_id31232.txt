Haslawa kogokonna kohus 29 Octobr. 1880
Juures istus:
Pääkohtumees  Mihk. Klaos
kohtumees Johan Jantzen
dito  Johan Udel
Tuli ette  Mihkel Klaos ja kaebas, et  Joachim Rosenthal, kui kohus koos oli mõistuse tegemisel rietega kohtu tuppa tuli ja ajamise pääle mitte wälja ei läinud, waid wasto rääkis ning palub, et tema trahwitud saaks.
Joachim Rosenthal ei wõi seda mitte salgada.
Mõistus:
 Joachim Rosenthal peab 6 rubla 8 pääwa sees walla laadiku trahwi maksma.
Mihkel Klaos
Johan Jantzen
 Johan Udel 
Mõistus sai kohtukäijatele T.S.R. pärra a. 1860 § 772-774 kuulutud ja õppus antud mis tähele tuleb panna kui keegi kõrgemat kohut tahab käija ja ei olnud Joachim Rosenthal  mitte sellega rahul.
