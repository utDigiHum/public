Batratski dogovor na mõznuju zemlju. Segodnjašnjago dnja meždu Gallikskim mõznom upravlenijem i krestjaninom Gustav Laumets   blago obdumano usloven i napisan nižesledujuštšiji dogovor:
§ 1. Gallikskije mõznoje upravlenije sdajet mõznuju batrakskuju usadbu Kurzi, stoimostju v 7 talerov  29 106/ 112 grošel, v arendu  Gustavu Laumetsu srokom na 2 goda stšitaja s 23 aprelja 1904 goda po 23 aprelja 1906 goda.
§ 2. Gustav Laumets platit za nazvannuju usadbu deneznuju arendu vsego po -rub. - kop.  v god, a imenno:1go aprelja- rub- kop. i 1go oktjabrja- rub.-kop. vpered i objazan krome togo ježegodno bezplatno proizvodit sledujuštšija rabotõ, a imenno:
1) žat i klast v skirdõ 6 lofštelei ozimi;
2) žat 8 lofštelei jarovago hleba ili goroška:
3) kosit 3 lofštelei klevera:
4) kosit-10 lofštelei sena;
5) kosit - lofštelei goroška;
6) sobirat ........ lofštelei kartofelja, kotorõi svozit tuda, kuda mõznoje upravlenije prikažet;
7) svozit- sažen 1 aršinnõh drov...........................................................................................
8) Uravnivat 100-150 saženei dorogi tam, gde mõznoje upravlenije prikažet;
9) 130 peših dnei, iz  kotorõi dolžen postavit 90 dnei do Mihailova dnja, ostalnõje v tetšenije zimõ. Za každõi nepostavlennõi rabotši den uplatšivajet on  letom 75 kop. i zimoju 50 kop.
Ispolnenije  oznatšennõh rabot razrešajetsja soglasno  § 168 Položenija o krestjanah Lifljandskoi guberniji ot 13 nojabrja 1860 g. Za každõi rabotšiji den, postavlennoi  bolše uslovlen,  nego uplatšivajetsja litsoju letom 40 kop i zimoju 35 kop.
§ 3.O brevnah i žerdjah na potšinku ili vozvedenije strojeniji arendator batrakskoi usadbõ dolžen zajavit do Mihailova dnja i objazan on podtšinjajetsja vo vseh otnošenijah lesnomu ustavu; v slutšai narušenija im sego ustava , on budet lišen svojei usadbõ.
§ 4. Arendator batrakskoi usadbõ objazan vesti hozjaistvo v sdannoi jemu v arendu usadbe po nadležaštšim zemledeltšeskim pravilam i sdat usadbu imeniju obratno po istetšeniji sroka sego dogovora v sledujuštšem porjadke:
      a) Vse strojenija i v osobennosti ih krõši dolžnõ bõt horošo potšinenõ;
      b) ržanoje pole dolžno bõt horošo obrabotano i svojevremenno obsemenno horošim semenem;
      v) zaborõ dolžnõ bõt horošo potšinenõ;
      g) senokosõ dolžnõ bõt otšištšenõ;
     d) iz rabot k buduštšemu ekonomitšeskomu godu ržanoje požnivo dolžno bõt vspahano i horošo vsboroneno;
     e) On polutšajet 4 saženi 1 aršinnõh drov glja toplivam 5 saž, hvorost. Za rubku uplatšivajet arendator usadbõ.
§ 5.V sljutšaje jesli batrak po prikazaniju mõznago upravlenija vozvedet novõja strojenija ili potšinit starõja, to eto on proizvodit na svoi stšet ,ne imeja prava trebovat kakogo libo za eto voznagraždenija, na kakovuju postroiku mõznoje upravlenije dajet bezvozmezdno privesti k mestu postroiki; ravnõm obrazom vse batraki vmeste, sorazmerno velitšin ih zemli, dolžnõ dat bezvozmezdno solomõ na krõšu soglasno istšislenija mõznago upravlenija. Za proizvodstvo kamennago fundamenta pod hlev mõznoje upravlenije uplatšivajet po kvadratnõm sažnjam.
§ 6.  Svoi sobstvennõja polja arendator mõznoi batrakskoi usadbõ dolžen obrabotõvat v tšetõreh tšastjah, tak, tšobõ nahodilis 3 lofšteli pod rožju, 6 lofšteli pod jarovõm hlebom, 3 lofšteli pod parom, pritšem on ne imejet prava zasevat par jarovõm hlebom ili lnom.
§ 7. Solomu, seno i drugoi korm dlja skota arendator sei usadbõ ne imejet prava komu nibud ssužat ili prodavat.
§ 8. Mõznoje upravlenije imejet pravo revizovat hozjaistvo usadbõ vo vsjakoje vremja ili samo ili libo tšerez svojego upolnomotšennago.
§ 9. Arendator usadbõ imejet  pravo zasevat lnom tolko 1 lofštel.
§ 10. Jesli mõznoje upravlenije poželajet arendatoru sei usadbõ sdelat otkaz v onoi, to etoi dolžno posledovat 23. oktjabrja do istetšenija sroka sego dogovora, pritšem arendator usadbõ ne imet prava trebovat voznagraždenija za kakija libo rabotõ ili naderžki.
§ 11. V obezpetšenije totšnago ispolnenija prinjatõh na sebja objazatelstv arendator etoi batrakskoi usadbõ zakladõvajet vse svoje imuštšestvo, a v osobennosti svoi železnõi inventar, kotorõi dolžen sostojat po krainei mere iz odnoi lošadi i treh golovõ rogatago skota i nahoditsja v horošem sostojaniji.
§ 12. V slutšaje jesli arendator sei usadbõ budet obraštšatsja neostorožno s ognem i tšerez eto proizoidet požar, to on otvetšajet za prosšedšije ubõtki vsem svoim imuštšestvom. Krome togo mõznoje upravlenije uslovlivajetsja s arendatorom onoi usadbõ ještše v sledujuštšem:
     1) Arendator usadbõ prinimajet na sebja uravnivat........sažen.....dorogi;
     2) Arendator usadbõ nedolžen bõt zametšen v kraže, a takže, ne dolžen  prinimat v svojei usadbõ vorov ili kradennõh veštei;
     3) Jesli žilaja riga po vethosti priidet v plohoje sostojanije, to imenije vozvodit takovuju na svoi sšet, pritšem odnako arendator usadeb mõznoi zemli vse meste dolžnõ, sorazmerno velitšin svojei zemli, bezvozmezdno podvozit trebujemõi material; ravnõm obrazom oni dolžnõ podvozit material takze i na potšinku žiloi rigi.
§ 13. Dlja otbõvanija dnei, arendator usadbõ dolžen javljatsja každõi raz nemedlenno po prikazaniju imenija. Pri molotbõ zimoju s 6 tšasov utra do 7 tšasov vetšera.
§ 14.  Arendator usadbõ ne vprav dopuskat, tštobõ  ego sobaki svobodno šljalis. Každaja sobaka, kotoruju vstretit vse ravno gde vne granits usadbõ, budet nemedlenno ubita.
§  15. V slutšaje ispolnenija arendatorom usadbõ kakogo libo iz oznatšennõh v sem dogovor punktov, on lišitsja svolei usadbõ, ne imeja prava žalovatsja na to v sud ili trebovat voznaraždenija.
§  16. Sostojanije strojeniji arendnoi usadbõ Kursi.
     1) Žilaja riga novaja.
     2) Ambar starõi.
     3) Hlev starõi.
     4) Banja  starõi
     5) Kuhnja.................................................
     6) Kolodets  hudoi.
     7)............................................................
     8) Zabor vokurg dvora v horošem sostajaniji. 
Tšto nastojaštšiji dogovor vpolne protšitan i razjasnen arendatoru Gustavu Laumets i tšto pomjanutõi arendator vpolne dogovor so vsemi võšeoznatšennõmi uslovijami, on udostverjajet svoego podpisju.
          Tak soveršeno v 6 nojabrja 1903 goda.
           Arendator  batrakskoi usadbõ: Kustaw Laumets 
           Ot imeni mõznago upravlenija: E Bauman.  
  
