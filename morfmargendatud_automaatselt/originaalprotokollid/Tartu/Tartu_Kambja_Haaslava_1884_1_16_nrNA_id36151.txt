Haslawa Kogukonna Kohus sel 16 Januaril 1884
Man ollid:
Päkohtumees: Jaan Link
Kohtumees:
Peter Iwan
Johan Grossberg
Kaibas  Leno Reinbaum Johan Palla  man ollen, kes Haslawa mõisan ellab, et Jõulu ösel kel 12 om Haslawa mõisa Tallmeister  Märt Wirro, kes naese mees om, kui tema omma sängi pääl man ollon, tema jurde tullon om teda kiskunu ja krapinu ja om tema rõiwat ülles kiskuna tahtnu, päle selle om se  Märt Wirro tema jalgust kinni wõttan, teda ülles tõstan ni et tema paljast lännu, tema keldnu ja aijanu seda mest henda jurest ärra, sis om se mees temale enne jallaga möda jalgu lönu ja sis lönu temale kõrwa päle ja sis tõisel päiwal om se mees ähwärtanu, et tema weel parremba sanna peab sama.  Leno Reinbaum nõudis selle teotuse ja lömisse est 50 Rubl, selle jures om Johan Esse ja Kristjan Kõiw ollnu, kes Haslawa mõisan teniwa. 
 Märt Wirro Haslawa mõisast wastutas selle kaibusse päle, et tema pandno omma mütsi selle tüdruku sängi pääle ja kui keik möda ollon länu tema omma mütsi otsima sis om  Leno Reinbaum temale jallaga munite päle lönu ja sis wõtton tema abimist kinni wõtnu ja kiskunu sis om tema kül selle tüdruki sängi litsunu seni kuii abena lahti lasknu, ja  Leno Reinbaum om teda wargas sõimanu seda tüdrukut tema lönu ei ka puton ei olle.  Johan Esse ei taha tema tunistajas wõtta, selle et  Johan Esse selle tüdrukuga sõbra ja temaga wihamees om.
Kristjan Kõiw tunistas et tema nänu, kui  Lena Reinbaum selle  Märt Wirro habemest kinni hoitnon ja üttelnu, kas mina sinno litz ollen, kellega sinna tänna õhtu ullat ja kui tüdruk abbenist lahti lasknu om  Märt Wirro selle tüdrukule kämblaga põsse päle lönu mes päle tüdruk wargas ütlen.
 Johan Esse  Haslawa mõisast tunistas, et se tüdruk om omma sängi päl ollon ja kõnelnu ütte tõisega sis om Märt Wirro sinna jurde lännu ja tõstan selle tüdruku jallat sängi mes päle tüdruk üttelnu sinna äbemada sinna wõit omma naese jallat sängi tõsta, selle päle om Märt Wirro selle tüdruku päle lamanu ja om teda rammino, päle selle om tüdruk üttelnu sinna raibe. Päle selle om Märt Wirro selle tüdruku jalgust kinni wõton ja ülles tõston ni et tüdruk paljas lännu, sis om tüdruk abemist sel mehel kinni wõtan ja kuio tema abimist lahti olli lasknu lönu Märt Wirro selle tüdrukule põsse päle, selle päle om tüdruk kül Märt Wirrole warras üttelnu ja Märt Wirro üttelnu, sinna ollet karmantsiku lits.
Mõistetu: et  Märt Wirro 5 Rubl selle lögi est  Lena Reinbaumile ja 3 Rubl trahwi waiste ladiku heas 8 päiwa sehen massma peab, selle et Märt Wirro üttel Kallil õhtul nisugust näotut tööd tennu om.
Se mõistus sai kulutedo ja õpetus leht wälja anto.
Päkohtumees : Jaan Link /allkiri/
Kohtumees 
Johan Klaosen XXX
 Peter Iwan /allkiri/
Johan Krosbärk /allkiri/
Ollen seitse Rubl kätte sanu sel 4 Märtsil 1885.
Leno Reinbaum praegune Lattik XXX
