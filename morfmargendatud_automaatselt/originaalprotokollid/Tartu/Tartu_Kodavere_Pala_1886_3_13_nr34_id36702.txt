Protokolli pääle 13 Webruarist s. a. sub. N:15 sai ette kutsutud endine Pääkohtu mees Otto Kangro ja wastas küsimise pääle et tema kunagi Karl Tragunile seda kahju tasumist lubanud ei ole sest et Traguni warandus Tartu Maakohtu käsu pääle keelu alla pantud sai; nendasama  ka lahti lastud.
Tunnistused said Karel Tragunile awaltud, ei olnud temal enam  nõudmist kui et Kusta Wipper weel tunnistajad wõtta.
Kohus tegi otsus. Sellepärast et wallapolitsei nii kui ka kogukonna kohus Tartu Maakohtu käsku, Karel Traguni warandust keelu alla panna, täitma pidi, ilma et kudasgil wiisil selle juures üks wastutus walitsuse wõi valitu liikmete pääle langes; Kustaw Wipperi tunnistust üleswõtmata ja Karel Traguni kaebtust nii kui ilma põhjuseta kõrwale  jätta s. a. tühjaks arwata.
See otsus sai Talurahwa sääduse § 772, 773 ja 774 põhjusel kohtukäijatele awaltud ja ei olnud Karl Tragun sellega rahul mis pääle temale appellationi täht kõige tarwiliste õppustega selsamal korral kätte anti. 
                                                        Pääkohtumees J. Hawakiwi.
                                                              Kohtumees M. Piiri.
                                                              Kohtumees J. Tamm.
                                                              kirjutaja allkiri.
