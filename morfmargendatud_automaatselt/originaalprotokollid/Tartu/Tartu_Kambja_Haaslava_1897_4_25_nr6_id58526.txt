№ 6
2 Jurjewi Ülema Talurahwa Kohturingkonna 14 Wallakohus  Haslawa wallamajas 25 Aprillil 1897 a.
Juures oli: Eesistuja: Jaan Rosenthal 
Ilmusid  Jaan Jaani p. Klaos ja  Johan Otsa ja palusid  järgmest protokolli kirjutada.
 Jaan Klaos annab üles, et tema isa testamenti järele sest 1 Webruarist 1893 a oma õele ja kaaspärijale  Lena Klaos abielus Otsa kakssada /200/ rubla äramaksis.
Lena Klaos mees Johan Otsa,  kui oma naese wolinik tunistab  õigeks, et eelnimetatud kakssada rubla /200/ on wasta wõnud.
Jaan Klaos /allkiri/
Johan Otsa /allkiri/
1897 aastal Aprilli 25 kp. on eelseisaw leping  14 wallakohtule suusõna pallwel ustawaks tunistuseks sisse antud  Haaslawa walla liikmede  Jaan Jaani p. Klaos ja  Johan Jaani p. Otsa poolt, kes Haaslawa walla liikmed on ja selles samas wallas selle wallakohtu ringkonnas elawad, mõlemad lepingu tegijad on wallakohtule palelikult tuntud isekud on, kellel seaduslik õigus lepingu tegemiseks on ja et Lena Klaos mees Jaan Otsa tõeste oma naese wolinik on ja et mõlemad lepingu tegijad selle lepingule  oma käega alla on kirjutanud, tunnistab wallakohus tõeks.
Kohtueesistuja: J. Rosenthal /allkiri/
Kirjutaja: K. Laar /allkiri/
/Pitser/
