sel. 6. Mail 1877
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomes Jaan Mänd
Mihkel Habakukk kaebas et temmal Kai Pukki käest 45 Rubla saada ja tahhab temma se rahha kätte saada
Kai Pukk tunnistas omma Wöölmondrite Maddis Paasi ja Mihkel Pukki juures ollemissel. et se õige et temma kaddund mees Jürri Pukk Mihkel Habakukkele 45 Rubla wõlgo jänud ja et temmal nüid woimalik ei olle sedda wõlga ärra maksa.
Moistus
Kai Pukk peab sedda wõlga Mihkel Habakukkele nenda wiisi ärra maksma, Jaanipääwast 1877. - 20 Rubl. Martipääwast 1877 - 15 Rubla ja Jürripääwast 1878. 10 Robla ning jäeb se 5 saado alla heinam selle rahha intressi ette weel tännawo Aasta Mihkel Habakukke kätte eddasi
Josep Laurisson
Andres Kiik
Jaan Mänd.
