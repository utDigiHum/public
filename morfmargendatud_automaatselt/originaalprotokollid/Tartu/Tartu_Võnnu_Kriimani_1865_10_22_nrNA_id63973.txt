Krimanni koggokonna kohtus 22 Octobr 1865.
Liis Järw tulli koggokonna kohto ette ja kaebas et Juhan Klaos temma tütri An Järwe enda mant ärra ajanud ja ei olle mitte palka kätte maksnud, sai Juhan Klaos ette kutsutud ja ta tunnistas et temma ei olle mitte An Järwe enda mant ärra ajanud waid An Järw on isse temma mant ärra läinud poliko aastaga ja ütles: kui ta minno mannu taggasi tenima tulleb, siis wõttan ma tedda hea melega wasto ja maksan ka temmale täie palga kui ta illusti om aasta täis teninud ni kui kaubelnud ollen agga sepärrast et ta poliko aastaga om minno mant ärra läinud ei wõin ma mitte täie aasta palka maksta.
Siis sai Liis Järw ette kutsutud ja taütles: et minna ei panne mitte omma tütart ennam Juhan Klaose juurde taggasi tenima.
Mõistus: Koggukonna Kohhus mõistis et Juhan Klaos peab An Järwele pole aasta palga ärra maksma mis ta teninud on, ja An Järwel ei olle se läbbi mitte täie aasta palka õigus nõuda, sepärrast et ta isse on enda tenistusest wallale teinud.
Peakohtomees Josep Kerge 
Abbi "  "  Jaan Aberg 
"  "  "  "  Jaan  Müirsep 
