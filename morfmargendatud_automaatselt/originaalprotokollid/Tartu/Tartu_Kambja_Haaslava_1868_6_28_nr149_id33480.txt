Se Koggokonna wannemb kaibap, et se perremees Jaan Zobbel saanud käsk hobbust wälja saata kiudu kõrrast, ei olle agga saatnut nink om tolle eest trahwi massetu 2 rubla hõbbedat. Koggokonna wannemb nowwap se trahw 2 rubla Jaan Zobeli käest kätte ja pallub, et kohhus sedda Jaan Zobeli tolle wasto pannemisse eest trahwis.
Jaan Zobel wastutap tolle kaibusse pääl, temma ei olle käsk saanud nink ei massa temma ka sellepärrast mitte trahwi.
See kassak Jaan Müür tul ette ja üttel, et Jaan Zobel om käsku saanud kiudu hobbest wälja saata.
Se kassaku naine üttel nendasamoti.
Jaan Klaussen ja Endrik Prikko tunnistawa ka et se kassaku naine üttelnut neil tol körral et Jaan Zobel saanud käsk kiudu hobbes wälja saata.
Moistus: Jaan Zobel piap 14 päiwa seen se trahw 2 rubla ärra masma.
Pääkohtomees Jaan Purrik XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
Kellepääl Jaan Zobel weiga krop oll nink weiga suust lärmi koggokonna kohto een teggi nink es lubba mitte se trahw ärra massa siis sai tollepääl Jaan Zobbel 3 ööst ja päiwast kinni moisteto.
Pääkohtomees Jaan Purrik XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
