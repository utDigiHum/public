25a Adam Anniotti ja Hindrik Riika wõttis se "Muga" Jaani tallo Rasina küllast 1 aastas orjuse päle ja orjab kõik nenda kui wannast agga obbuse päiwi teeb ta egga Tadre päld üts päiw rohkemb sis om tal ütte kokku 156 obbuse ja 169 jalla paiwi aastas orjata se orjus allustab Jurri p. 1861 seni kui Jürri p. 1862ni. 
Rasin d. 29. August 1860
(Allkiri)
Hindrik Rikka XXX
Adam Anniotti XXX
21b Johaan Janson wöttab se "Jaani" maja mis Nitalanes om 1 aasta päle nenda samma kui wannast orjuse päle ja teeb se est 78 jalla päwa ja 2 sülda puid moisa. Aasta allustab 1861 seni kui 1862ni Jurri pawani.
Rasin d. 29l. August 1860
(Allkiri) Moisawall(itsus)
Johann Jaanson XXX
22b. Johann Anni wõttab se "Zäro Pebo Jaani" maja mis Krusamael om 1861 seni kui 1862ni Jürri päiwani 104 jalla paiwa est teoorjuse päle ja tob wel 2 sülda puid mõisa.
Rasin d. 29. August 186
Johann Anni XXX
