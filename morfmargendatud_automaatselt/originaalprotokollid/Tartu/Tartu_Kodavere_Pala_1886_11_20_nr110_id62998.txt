Kustaw Waht nii kui wallawalitsus kaebas, et Jürri Tõltsep tõekatsumise ajal mitte hobust sõidu tarwis andnud ei ole ja Willem Perametsa käest sellepärast üks hobune 3 rubla eest palgatud sai, nõudis seda raha sisse ajada. Jürri Tõltsep wastas küsimise peale et temal mitte ennem kui õhtu enne seda käsk sanud, tema hobune aga mitte rauas olnud ei ole ja sellepärast mitte sõitu minna ei wõinud. Tema naine olla kasakale kül ütelnud, et wõimata on küüti minna.
 Kasak Toomas Welt andis üles et tema andnud päew enne seda Tõltsepale käsk aga seda ei ole Tõltsepa naine mitte ütelnud et küüti minna wõimatu on. 
Tõldsep andis tunnistajaks et tema naine kasakale ütelnud et küüti minna wõimata on. Taawet Aia oma  sulane üles.
 Otsus: Jürri Tõltsep maksab need 3 rbl. wallawalitsusele 3 päewa sees ära, tunnistus Tawet Aia aga kuulamata jätta sest et kasak käsu andmiste juures mitte tunnistust ühes wõta ei wõi. Se otsus sai kuulutud ei olnud aga Jürri Tõltsep  sellega rahul. 
                                       Peakohtumees   J. Hawakiwi
                                       Kohtumees         J. Stamm
                                       abi kohtumees  J. Tagoma
                                       kirjutaja C Palm. (allkiri)
