sel 22al Januaril 1873. 
koos ollid
peakohtumees Maddis Tamm
teine kohtumees Josep Reisenpuk
kolmas kohtumees Josep Kallaus 
Tännasel pääwal said selle walla liisu mehhed ette kutsutud ning nende käest järrel küssitud kas nendel kellegi käest sada on.
N 26. Liisumees Jakob Messi tunnistas et temmal kellegi käest sada ei olle.
N 27. Liisumees Jaan Pajo tunnistas et temmal kellegi käest sada ei olle.
N 28. Liisumees Mihkel Pernitson ei olnud Koggokonnakohtu ette tulnud.
N 29. Liisumees Jaan Klepanit ei olnud Koggukonnakohtu ette tulnud.
N 30. Liisumees Tomas Reisenpukk ei olnud Koggokonnakohtu ette tulnud.
N 31. Liisumees Jaan Ambos ei olnud Koggokonnakohtu ette tulnud.
N 32. Liisumees Mart Kallaus kaebas, et temma omma issa käest omma emma pärrandust tahhab saada 1 kuub.
Moistus
Koggokonna kohhus saab selle eest muretsema et temma se kätte saab.
N 33. Liisumees Jakob Kallaus ei olnud Koggokonnakohtu ette tulnud.
N 34. Jaan Murd kes Tallinamaal passi peal tenimas ei olnud Koggokonnakohtu ette tulnud.
N 35. Tomas Aun ei olnud Koggokonnakohtu ette tulnud.
N 36. Liisu mees Tomas Waht, kes Wirrumaal passi peal tenimas ei olnud Koggukonnakohtu ette tulnud.
N 37 Liisumees Tomas Adamson astus ette ja tunnistas etvtemmal kellegi käest sada ei olle.
N 38. Liisumees Maddis Pern astus ette ja tunnistas, et temmal kellegi käest sada ei olle. 
N 39. Liisu mees Gustaw Pukk astus ette ja tunnistas ja tunnistas, et temmal Mihkel Ambose käest 5 Rubla ja Josep Tomiku käest 1 Rubla saada.
Moistus
Et Mihkel Ambos ja Josep Tomik ei olle Koggokonnakohtu ette tulnud, siis saab Koggokonnakohhus selle eest murretsema et temma omma rahha kätte saab.
