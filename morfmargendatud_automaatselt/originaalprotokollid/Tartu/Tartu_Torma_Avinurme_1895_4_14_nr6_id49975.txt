Testamenti lisa sest. 13 Oktobrist 1889 N. 188.
Sel. 14 Aprilil 1895
koos olid
peakohtumees Jaan Kallaus
kohtumees Jüri Reisenpuk
kohtumees Jaan Paas
Siit walla inimene pobbol Josep Kukk Piilsist astus ette ja palus Wallakohut et tema tahab oma testamenti kedda tema sel 13 Aktobril 1889 Protokoll N 188 all siin Wallakohtu juures üles kirjutada lasknud nattukene muuta nenda wiisi et sedda wana rehemaja mis tema testamenti punkt 5 sees nimetud pärib tema poeg Maddis Kukk omale selle parast et tema neljas poeg Mart on ära surnud siis on tema se mis maja ära müinud; nendasamuti on tema tüttred Mai ja Kadri mehele lähnud ja need oma jaud kätte sanud mis järel siis se rehemaja keige kambriga ja üks wana Ait Maddiselle pärida jaeb ja woiwad teised wennad. kui nendel rehte rehe peksmiseks tarwis on, selle asja pärast kokku leppida oma wenna Maddisega 
Josep Kukk ei moista kirjutada tema palwe peale kirjutas alla Juhanes Karu (allkiri)
Otsus
Wallakohus saab se lisa Josep Kukke testamentile sest 13 Aktobrist 1889. juure lisama. kus se peale tema surma saab tema pärijattele ette loetud ja tunnistab Johannese Karro allkirja tõeks
Jaan Kalaus. (allkiri)
Jürri Reisenpuk (allkiri)
Jaan Paas (allkiri)
kirjutaja Schulbach (allkiri)
17 Nowembril 1900 a. Jakob Kukele ärakiri wälja antud 15 kop margiga.
kirjutaja Tõldsepp (allkiri) 
