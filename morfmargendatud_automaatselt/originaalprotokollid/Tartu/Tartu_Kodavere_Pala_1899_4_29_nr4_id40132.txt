1899 aasta aprilli kuu 29 päewal kinnitatud Jaan Kiiseli ja Johan Sikkeli suusõnaline leping:
Et Gustaw Reinmann oma maja on Sikkelile ära müünud, selle sama maja, mis Jaan Kiiseli maa peale ehitatud, mis maa tükki rendi üle Reinmanni ja Kiiseli wahel 22 webruaril 1896 a. N:4 siin walla kohtus on kinnitatud, siis lubab Jaan Kiisel omalt poolt Johan Sikkeli endiste tingimistega wastu wõtta nagu Reinmannigi ja Johan Sikkel omalt poolt tõutab kõik seda sama täita ja aastase rendi 15 rbla kahes tärminis 25 aprillil ja 1 oktobril iga aasta ära maksta.
Lepingule kirjutasid mõlemad lepingu tegijad alla.
                            Kohtueesistuja: J Reinhold
                                    Kirjutaja:     KSepp.
