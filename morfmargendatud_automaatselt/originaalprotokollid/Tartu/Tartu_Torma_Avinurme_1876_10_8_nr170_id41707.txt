sel. 8 Actobril 1876 
koos ollid
peakohtomees Josep Kask
teine kohtomes Josep Laurisson
kolmas kohtomes Jaan Mänd
Awwinorme Ranna walla innimenne Juhhan Klaup kaebas et temma wõtnud Waddikülla perremehhe Jakob Tamme käest Maja ehhitada 69 Rubla eest ning on temma siis se maja ülles ehhitanud ja Jakob Tamme käest 50 Rubla 20 kop kätte sanud 19 Rubla agga weel sada mis Jakob Tamm temmale maksa ei tahha ja pallus kohhut nende asja selletada. ja olnud nendel kauba juures Maddis Klaup..
Jakob Tamm räkis et nende kaup olnud 67 Rubla. ja on Juhhan Klaup temma Maja hopis ärra rikkunud. ning mitmed töed teggematta jätnud, ja nimmelt, üks tagguminne sein ja üks lae walts, ning on temma Juhhan Klaupile 55 Rubla ja 20 koppikud kätte annud. ja 11 Rub 80 Copikud weel anda mis agga temma maksa ei lubba enne kui need töed on tehtud.
Maddis Klaup tunnistas et temma kuulnud pealt. kui Jakob Tamm ja Juhhan Klaup kaupa tehnud Maja ehhitusse pärrast, ning piddanud Juhhan Klaup agga majaseinad ülles raijuma. ja selle eest 67 Robla saama.
Mihkel Soo tunnistas et temma olnud juures kui Jakob Tamm wiimastkorda Juhhan Klaupile rahha annud. 25 Rubla ja ütlenud enne olli sul 25 Rob käes ja nüid said 25 Robl. ühtekokko 50 Rubla ning ei olle neil ehhitusse ajal juttu olnud kulma kambri pärrast
Josep Karro tunnistas et temma olnud selle ehhitusse juures töemees ning ei olle just seal juures olnud kui Jakob Tamm Juhhan Klaupile rahha annud. agga pärrast on Jakob Tamm temmale ütlenud ma annin 30 Robl. Juhhan Klaupile. kaup olnud neil agga essiti 67 Rob. ja on pärrast Jakob Tamm 2 Rubla juure lubbanud. Kambri ümber teggemisse eest agga külm kamber ei nende kauba sees olnd.
Tomas Reisenpuk tunnistas et temma kulnud pealt kui Jakob Tamm on lubbanud kambriümber teggemisse eest 2 Rubla juure anda Juhhan Klaupile ja Juhhan Klaup selle külma kambri eest 4 Rubla nõudnud Jakob Tamm agga ei olle lubbanud ja nenda wiisi nende kaup katki jänud.
Wallawöölmönder Mart Reisenpuk tunnistas et temma on se Maja ülle watanod. ning ei olle temma selle juures ühtegi wigga leidnod.
Moistus
Sellejärrel Et Josep Karro ja Tomas Reisenpuk tunnistawad et Jakob Tamm on 2 Rubla juure lubbanud. kambri suuremast teggemisse pärrast, Mihkel Soo agga tunnistab et kui Jakob Tamm on wiimastkorda Juhhan Klaupile rahha annud issi ütlenud et nüid 50 Rubla rahha kätte sanud; ja Wöölmönder Mart Reisenpuk tunnistab et sel majal ühtegi wigga ei olle., siis moistab koggukonnakohhus et Juhhan Klaup selle maja ehhitusse eest se tingitud 69 Rubla saab. ning et temma jo 50 Rubl 20 koppikud kättesanud siis peab Jakob Tamm Juhhan Klaupile weel 18 Rubl. 80 Cop wälja maksma. ning ei prugi Juhhan Klaup sedda seina ülles tehha mis temma kauba sees ei olle olnud.
Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastal 1860 §§ 772 ja 773. kuulutud ning nende kohto käijatelle need säduse jarrel Appellationi õppetusse kirjad wälja antud.
Jakob Tamm tunnistas et temma selle mõistusega rahhul ei olle. sest et temma on Juhhan Klaupile 55 Rubl wälja maksnud. ning se temma töe ärra rikkund ja muist weel teggematta jätnud ja pallus Protokolli wälja suurema kohto minna
Josep Kask XXX
Josep Laurisson XXX
Jaan Mänd XXX
se lubbatud Appellation sai sel. 15 Oktobri N 279 all. Jakob Tammele wälja antud.
