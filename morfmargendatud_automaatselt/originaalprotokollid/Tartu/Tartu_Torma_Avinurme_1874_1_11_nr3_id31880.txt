Awwinorme Mets Koggokonna Kohtus sel 11 Januaril 1874.
koos ollid.
pea kohtomees Josep Kask
teine kohtomees Juhhan Koppel
kolmas kohtomees Maddis Sildnik
Mart Errapard kaebas et temma tehnud minnewa kewwdw poolteist wakka kartolid mahha kedda agga Jaan Rosentali sead hopis ärra sönud ja ümber tuhninud ning olnud Mihkel Rosental neid siggu omma jure karja wõtnud, ja nõuab temma selle kahjo eest 6 wakka kartolid.
Mihkel Rosental räkis et need sead temma wenna ommad olnud, kedda temma omma jure karja wõtnud, ja et need sead on need kartolid ülles tuhninud siis on temma neid nenda kohhe ärra saatnud, ja peab selle pärrast Jaan Rosental sedda kahjo õiendama.
Jaan Rosental räkis et temma wend Mihkel temma sead omma jure karja wõtnud, kolme näddala pärrast agga temmale teadus satetud, et sead pilla tehnud, mis peale temma siis sinna lähnud, ja sedda pilla järrele watanud, ning ka kohhe Mart Errapartile kartuli seemne maksta lubbanud, agga rohkem kahjo ei woi temma omma peale wõtta.  
Moistus.
Koggokonna kohhus on moistnud et Mihkel Rosental olli neid siggu omma holest wõtnud kes pilla tegid, siis kannab temma kaks jaggo kahjo, ja Jaan Rosental et temma sead sedda pilla teggid, üks jaggo kahjo maksab, ja maksab Mihkel Rosental 4 wakka ja Jaan Rosental 2 wakka kartulid Mart Errapartile kahjo tassumist
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
