Johann Wadi Saare wallast oma wenna Jaani kõrwas, kes mõtsa warguse perast sia kohtu ette olli kutsutud.Oli aga iljaks tulema jänud ja kui seda temale kohus ette luges sai  see roppuks ja hakkas waljo sõnadega kohtu uksest wälja minnes kohut ähwardama, et tema sedda ei tunne ega seie ette tulema ei saa.
Kohto otsus: Et Johann kohut  wälja minnes ähwardab mitte selle käsku kuulda ei luba jääb tema, 2 Rublaga ja et mõlemad oma wennaga ilja on tulnud kumgi 50 Kop Waeste Laegas ka kaks trahwitud.
                                                                                                                                       Märt Piiri
                                                                                                                                       Josep Soiewa
                                                                                                                                        D. Willemson                                     
