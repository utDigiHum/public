Sel 18 Oktobril 1882 
Kaebas  Johan Lock  Tarto Linas Mäe ulitsas N 11  ellab, et temma 300 Rubl Johani Johan Klaosenilt maja ostmise rahha sisse om massnu ja pallus tunistust et tema sedda rahha selle maja eenõiguse päle sisse om massnu, tema massnu 400 Rubl. sisse ja 100 Rubl olla  Johan Klaoseni omma rahha.
 Johan Klaosen  wastutas et tema selle Johan Locki käest rahha laenanu ei olle, se  Johan Lock om tema est rahha kül Mõisa herra kätte andno selle et tema essi ei olle näinut.
Kohtunik Johan Udel tunistas et temma nännu kui Johan Lock kül 400 Rubl Mõisa herra kätte massnu ja Johan Klaosen om nime kontrahti sisse kirjotanu kõnet ei olle ollan midagi.
Wallawanemb tunistas nisama ja Mõisa Herra rahha wastuwõtja om üttelnu mes sinna Johan Klaosen rahha essi ei massa, sis om Johan Klaosen wastotanu "mina ei näe".
Johan Lock  andis ülles et se rahha keik Johan Luikenbergi käest Tarto Linast om laenato ent temma om selle jures käemees ollnu.
Johan Luikenberg Tarto Linnast andis 8 Nowembril Protokoli et tema selle rahha kolmsada Rubl Johan Locki läbbi Johan Klausonile om andno ja ninda se wõlg ütsinda Johan Klausoni päle jääb ja Johan Lock jääb sält wahelt wälja. Temma Johan Luikenberg om weel 50 Rubl perra andno ni et praega 350  Rbl Johan Klaosoni käes om. Johan Klaosen lubab seda rahha massa
1 Mail 1883 massab ütssada Rubl
1 Mail 1884 jälle üts sada Rubl
1 Mail 1885 jälle üts sada Rubl
ja 1 Mail 1886 jälle wiis kümmend Rubl.
Johan Klaosen andis ülles et temma se raha 350 Rubl wõlgo om ja neil kirjutud tärminidel seda rahha ärra massa lubbab.
Otsus: seda Protokolli ülles panda.
Päkohtumees: Mihkel Klaos 
Kohtumees: Johan Udel 
