1912 17 märtsil tuli Pala wallakohtu esimees Karl Wahi ühes kirjutaja  Brükkeliga Pala walda Wadi Ado talusse kad. Gustaw Kokka lese Miina Kokka, sünd. Rosneri wiimast testamenti üles kirjutama. Tunnistajana on juures Pala walla liige Karel Josepi p. Salu ja Saare walla liige Willem Kaarli p Mihkelson. Miina Kokka on päält näha terwe mõistuse juures ja awaldab selgelt oma wiimast tahtmist, mis on järgmine:
1) Pääle oma surma luban oma poja Jakob Kokkale ainu-omanduseks:
a) Kõik oma sula raha, mis minult peaks järele jäema,b) Kõik need nõudmise õigused, mis minul oma noorema poja Kaarel Kokka ehk ükskõik kelle käest minu surmani saada jäeb. Sissenõutud raha saab  Jakob Kokka omale.
c) minu kadunud mehe Gustaw Kokkast järele jäenud 100 rub osa, suur 266 rubla, mis tema , Jakob, ka kohtu teel nõuda ja omale jätta wõib. d) kaks kappi, üks neist Kaarlist  poja kambris, tõine aidas, minu riided ja kaks lehma.
2) Matuksed peab minu noorem p Kaarel sellekohase kreposti akti järele minule tegema, aga et tema mind halwasti on ülewal pidanud ja minule sellesama akti järele saadawad taskuraha heaga ei maksa, siis peab nüüd Jakobist poeg minule niisamasugused matuksed pidama, nagu minu kadunud mehele tehti;
3) Teiste oma lastele mina oma warandust midagi ei luba, sellepärast, et nad mind wanal päiwil ja praegugi  sugugi talitanud ei ole.
4) Minu  endised testamendid on tühised.
Miina Kokka ei oska kirja, tema isiklikul palwel kirjutab alla üks tunnistajatest: Karel Salu
Tunnistame, et see testament Miina Kokka etterääkimise järele kirja on pandud, see temale sõnasõnalt etteloetud ja Miina Kokka seda täieste oma wiimaseks tahtmiseks tunnistanud. Karel Salu Willem Mihkelsoon.
Pala wallakohtu esimees tõenab, et see testament Miina Kokka poolt on tehtud, tema isiklikul palwel Karel Josepi p Salu poolt allakirjutud ja tunnistajatena, kes testamendi tegemise juures olid, sellesama Salu ja Willem Kaarli p Mihkelsoni poolt isiklikult  allakirjutud.
Kohtu Esimees K. Wahi.
Kirjutaja Brükkel.
