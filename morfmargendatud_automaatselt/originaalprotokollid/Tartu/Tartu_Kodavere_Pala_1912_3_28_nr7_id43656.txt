1912 a 28 märtsil Pala wallakohtu koosolek end Jõe wallas Toma N 14 talus. Esimees Karl Wahi liikmed: Jakob Uusen, candidat Karel Sarwik.
Tooma N 14 talu omanik Jakob Tooma p. Ernits on kopsunaha põletikus haige ja on ülalnimetatud kohtu liikmed oma juure kutsuda lasknud wiimast tahtmist testamendina ülesse kirjutama. Ernits
on täiesti mõistuse juures, räägib selgesti ja arusaadawalt, sängi pääl istudes järgmiselt: 
1) Pääle oma  surman luban oma Tooma N 14 talu koha endise Jõe wallas, mis minu arwamise järele 6000 rub wäärt, aga kus pääl 2000 rub mõisa ja kreditkassa wõlga, oma nelja sõsarale järgmise osade suuruses:
Sohwi Wene ja Anna Wunkile, sündinud Ernitsad, kummagile üks tuhat 1000 rub suuruses. Leena Punder, Mihkli lesele, kellel suurem pere ja kes siis rohkem tarwitab tuhatwiiesaja-1500 rub. suuruses. Miina Sepp, sündinud Ernitsale selle osa, mis weel talu wäärtusest üle jäeb.
Kui minu sõsarad talu peaksiwad äramüüma, siis saawad igaüks neist ülemalnimetud summad talu hinnast.
2) Liikuw warandus jäeb pääle minu surma minu noorema sõsarate Sohwi Wene ja Anna Wunkile ühetasa pooleks, sest et nemad minuga kaua aega ühes siin talus tööd teinud ja warandust koguda aitanud, liikuwast warandusest wõiwad nemad teiste sõsaratele ka anda, niipalju kui ise hääks arwawad.
3) Minu sula rahas, mis praegu kodus, luban pääle surma: matukse kuludeks sada 100 rub. oma risti laste Poline Uusenile 25 rub. August Jaani p Ernitsale 25 rub, Johannes Madise p Leppikule
25 rub, Johannes Madise p Leppikule 25 rub, Nõwa Tuletõrjujate Seltsile 25 rub. Mis weel üle jäeb, saagu maja talituste pääle ära kulutud. Jakob Ernits (allkiri)
Pala wallakohus tõendab, et Jakob Tooma poeg Ernitsa wiimane tahtmine tema etteütlemise järele ülesse on kirjutud, see temale etteloetud ja tema enese poolt õigesti ülessepanduks tunnistud ja isiklikult allakirjutud. Testamendi tegijal on sääduslik lepingu tegemise wõimus. Wallakohtule on tema isiklikult tuttaw.
Esimees K.Wahi
Juuresistujad J Uusen Karel Sarwik
Kirjutaja Brükkel.
