Sel 18 Julil 1888.
Selle otsuse päle sest 1 Junist 1887  Lena Birkenbaumi kaibuse asjan wasto Jaan Kahma nõudmise perrast olli  Daniel Säkna tullnu ja tunistas, et üts talw ollnu  Lena Birkenbaumi lehm Jaan Kahma jures söda peal ja  Birkenbaum käinu essi wahel lehma sötma selle massust selle est es tee tema midagi nisama kartofflist.
 Liis Säkna  tunistas et  Lena Birkenbaumi lehm ollnu 6 nädalit Jaan Kahma jures söda peal, massust selle est ei tee tema midagi nisama ka kartoflist.
Kohtumees  Jaan Hansen andis ülles et üts kõrd ollnu kül Birkenbaumi kartofli Jaan Kahma maa peal maan.
Mõistetu: et Jaan Kahma 8 rubl Lena Birkenbaumile 8 päiwa sehen peab massma, selle et tema seda selges teha ei jõua et Lena Birkenbaumi lehm tema jures 2 astat södal olles ollnu, 6 rubl om 6 nädali lehma sötmise est ja 2 rubl kartofli maas olleku est tassa arwatu.
23 Julil s.a sai se mõistus Lena Birkenbaumi mehele Johan Birkenbaumile sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.
Päkohtumees: Johan Link XXX
Kohtumees: Jürri Kärn XXX
"  Jaan Hansen /allkiri/
"  Johan Raswa XXX
