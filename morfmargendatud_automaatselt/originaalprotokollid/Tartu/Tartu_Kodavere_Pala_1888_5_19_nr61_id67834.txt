Protokolli peale 25 Webruarist s.a. N:30 oli August Pruel mitmekordse kutsumise peale wäljajäänud ja tegi kohus.
Otsus: seda August Prueli kaebuse asja wastu Jakob Sallo peksmise pärast sellepärast lõpetada, et kaebaja ise enam kohtu ette tulla ei taha. See otsus sai Karl Sallole ja Jakob Sallole kuulutud. August Pruel oli ise wälja jäänud.
                                                      Peakohtumees  J. Sarwik
                                                      Kohtumees        J. Stamm
                                                       abi Kohtumees W. Oksa
                                                       Kirjutaja Carl Palm. (allkiri)
