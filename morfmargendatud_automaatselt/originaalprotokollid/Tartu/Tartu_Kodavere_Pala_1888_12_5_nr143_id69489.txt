Tulli kohtu ette Josep Nõmm ja kaebas, et endine kirjutaja Palm olema temale 26 rubla 50 kop. wõlgu. Palm olema tema käest ühe wankre 20 rubla eest ostnud ja 11 rubla puhast raha laenanud. Sellest on Palm temale 3 rubla 50 kop äramaksnud, nii et praegune wõlg weel 26 rubla 50 kop. suur olla. Tema palub kohut seda raha Palmi käest sisse nõuda.
 Endine kirjutaja Carl Palm rääkis kaebuse pääle, et tema rehnung tema arwamise järel Josep Nõmmega 7 rubla 50 kop. olla. Tema ostnud Josep Nõmme käest üks wanker 20 rubla eest, olla aga see raha ära maksnud. Siis
olema tema Nõmme käest 1 rubla puhast raha laenanud, sellest aga 3 rubla 50 kop. ära maksnud, nii et tema Nõmmele praegu weel 7 rubla 50 kop. wõlgu on.
 Josep Nõmm rääkis, et tema kül Palmi käest ükskord 20 rubla saanud, seda aga  Palmi lubaga tema wana kõrtsi wõla pääle ära rehkendanud. Kõrtsi rehnung olla 28 rubla 16 kop. suur olnud. Sellest olla 20 rubla äramaksetud ja olla tema, Nõmm, wiimane osa osa 8 rubla 16 kop. Palmile kinkinud. Josep Nõmm näitas üks rehnung ette, kelle järel tema Palmile 60 kop. tempelwärwi ostnud ja üks täht Palmi poolt, kelle sees see palub, temale,, ½ kr. Otsehisehins ?ja 1 weike Rumsiden " saata. Palm tunnistas õigeks olema see 60 kop. ei wõtta aga seda tähte mitte wasta, et tema sellega wiina wõlga tahtnud. Kõrtsiwõlga 28 rubla 16 kop. ei tea tema mitte Nõmmele wõlgu olewad ja ei wõtta seda wõlga mitte wastu.
             Josep Nõmm palub Jakob Weskimets Rannast ülekuulata, kelle wasta Palm ütelnud, et tema seda kõrtsiwõlga ära salata tahta. Niisama Willem Tõnts, kes kõrtsist wõla pääle jookisid toonud.
  Palmil ei ole nende wasta rääkimist, lubab keigiga rahul olla, mis tunnistused ütlewad ja palub ilma temata otsus teha. 
  Willem Tõnts tunnistas, et kirjutaja Palm olla kahe ehk kolme aasta eest, Joosep Nõmme käest 10 pudelid wist õlut wõlga wõtnud mis tema nähes kül maksmata jäänud.
 Otsus: tulewaks kohtupääwaks Jakob Weskimets ettekutsutud.
                      Peakohtumees J. Sarwik
                       Kohtumees      M. Piiri
                        Kohtumees     J. Tagoma.
                        Kirjutaja O Seidenbach.(allkiri)
