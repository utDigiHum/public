1897 aasta Mai kuu 17 päewal on kohtu poolt üles kirjutatud Palla wallas Soiewa N. 38 omaniku Karel Jani pg Rosenbergi suusõnaline testament järgmiselt:
                                                                                             Testament
,, 1897 aasta Mai kuu 17 päewal luban mina Karel Jaani pg Rosenberg Palla walla Soiewa N: 38 talu omanik oma abikaasale ja lastele järgmist warandust ja õigusi, mis minu päralt seni ajani on olnud:
1) Minu abikaasa Euphrosine Rosenberg sünd. Horn jäeb kõige minu sularaha ja bank billetite walitsejaks kunni tema elab (Jurjewi banga pilet 18 Okt. 1895 a. Lif E N. 59/57 ja Hoiu Kassa raamat N. 14945) Pärast tema surma langeb see pangas olew raha minu mõlemate lastele Aleksander ja Alinele ühe wõrra pooleks.
2) Minu poeg Alexander Gustaw Rosenberg pärib minu päris koha, ,,Soiewa 38 talu``, mis 13 taadrit 58 grossi= 30 dessatiini suur on kõige liikuwa warandusega ja juurde ostetud metsaga, kõigega, mis sell korral talus on järgmiste tingimistega:
a) maksab iga aastal oma emale, minu abikaasale ükssada rubla ülespidamise raha ja annab sellesama tua kus praegu sees elame, elu Korteriks, ning puud kütteks. Selle juures jäeb minu abikaasal waba woli seda ülespidamise raha pojale siis tagasi anda kui ta seda ei tarwita, ehk hästi läbi saawad. Ka wõib minu abikaasa niikaua lehma pidada kui tema soovib.
b) maksab minu tütrele, tema  sõsarale Aline Emilie Põdderile sünd. Rosenberg pärast minu ja minu abikaasa surma ja pärast seda kui kõik Kredit kassa wõlg tasutud ja maha kustutatud on saanud,
wiissada rubla wiie aasta jooksul iga aasta 100 rbl ilma protsendita wälja.
3) Kui minu tütar Aline peaks warakult leseks jäema, siis jäeb temale õigus tema enese soowi järele selles tuas elada, kus mina oma abikaasaga praegu elan.
4) Poeg kannab minu ja minu abi kaasa matuse kulud.``
                                        K. Rosenberg /Allkiri/
                                        Kohtu eesistuja: J Reinhold
                                                   Kirjutaja    KSepp.
