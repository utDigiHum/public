Awwinorme Koggo konna kohtus sel. 30mal Decembr 1868.
Kohto juures olliwad.
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Jaan Hunt astus koggokonna kohto ette selle palwega, et temma assi selle eksind tüdruku Anna Ambri pärrast weel ükskord ette woetud saks, sest et temma nüid tunnis mehhi sanud; et Anna Amer tõiste külla poistega liderlikko ellu ellanud, agga temma sest asjast lahti on. ja et need tunnismehhed Jürri Waino ja Mai Amer on.
Jürri Waino sai ette kutsutud kes räkis et Anno Amer tulnud üks õhto temma Majase ja ütlenud, oleks Jaan Hunt minnole wiis Rubla annud, siis olleks minna rahhul olnud, agga temma hakkas minno sõimama ja litsitama, selle pärrast ma sedda ei jätta
Mai Amer astus ette ja tunnistas et temma nähnud Mihkel Kiige Majas kui Anno Amber issi Juhhan Kiige juure tükkinud maggama, ja on temma siis Annole ütlenud, kas on nüid poistel süid kui tüdrukud litsest lähhewad, ütlenud Anno Amber, ei ütlegi et poiste süi ongi omma süi, ja on siis Liiso Kiik ütlenud Mai Ambrile, et Anno maggab meie Juhhaniga kui mees ja naine kolm näddalad.
Liiso Kiik sai kohto ette kutsutud, kes räkis et se keik walle on. mis Mai Amber temma peale tunnistanud, ja et temma keige wähhemad Anno Ambrist üttelda ei woi, weel wähhem et se Juhhan Kiigega ühhes magganud,
Moistus
Et neist tunnistustest ühtegi õiget otsust ei sa et Anno Amber teistega kokko ellanud. ja pealegi Mai Amber, Anno Ambri wenna Naine on ei woi kohhus tedda tunnistajaks wasto wõtta, ja jäeb koggokonna kohto Moistus, N. 78. Protokolli põhja peale, ja et Anno Amber omma sõrmusid Jaan Hundi käest nõuab, moistab kohhus et Jaan Hunt kahhe sõrmuse eest Anno Ambrile 40 kop. wälja maksma peab. mis ka Jaan Hunt kohto laua peale wälja maksis, ühtlaisa agga tunnistas Jaan Hunt et temma selle kohto otsusega rahhul ei olle, ja pallus Protokolli wälja suurema kohto minna,
Jaan Hundile sai temma Appellationi wälja küssimisse peale. säduse ramatust §§ 773. teada antud, mis temmal tarwis tähhele panna, kui ta suurema kohto minna tahhab
Mart Jaggo.
Mart Liis 
Tomas Pärn
