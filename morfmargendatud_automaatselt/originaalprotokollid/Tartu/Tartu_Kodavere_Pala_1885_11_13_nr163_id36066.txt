Tuli ettte Maddis Hawakiwi ja palus kohut  Annus Waddi käest 10 rubla sisse nõuda mis Waddi temale wõlgu on.
Annus Waddi ütles seda õigeks ja andis üles et see 10 rubla wallawalitsusese juures sees seisab ja säält Maddis Haawakiwile wälja makstud saada wõib.
Otsus: Seda raha 10 rubla wallawalitsuse käest sissenõuda ja Maddis Hawakiwile wälja maksa.
                                           Pääkohtumees O. Kangro
                                                 Kohtumees J. Soiewa
                                                 Kohtumees O. Mõisa
                                                  Kirjutaja allkiri.
