Haslawa kogukonna kohhus sel 26 Januaril 1882.
Man ollid: Päkohtumees  Mihkel Klaos
kohtumees Johan Link
"   Johan Udel
Kaebas  Jaan Taari, et temmal 6 Rubl. Sapaste est  Johan Klaoseni käest sada om ja pallus et kohhus sunnis sedda massma.
Johan Klaosen wastutas et tema sapa wahhetamise päle 6 Rubl piddanu päle massma, ent se wahhetus ei olle täide läinut selle et Jaan Taari neid sapit mitte ei olle andno mes neide kaub ollnu wõttnu ka temma sapa ärra selleperrast temmal middagi maksa ei olle.
Jaan Taari andis Jaan Konks tunnistajas ülles, kes tunistas et temma sapa wahhetust nännu ei olle ja et tõine sullane Jaan Montag temmale kõnelnu om et nemma, Jaan Taari ja Jaan Montag, sedda Johan Klaoseni heaste tõmata om sanu sapa wahhetamise jures et Johan Klaosen wõlgu jänu ei olle middagi kõnelta.
Mõistetu: et  Jaan Taari  omma kaebusega rahhul peab jäma selle et tunnistust ei olle, et  Johan Klaosen  wõlgu olles jänu ja se kaebus ka enembe olles ette toma piddanu kui midagi nõuda olles ollnu. 
Se mõistus sai kulutus ja õppetus leht wälja antus.
Päkohtumees Mihkel Klaos  XXX
Kohtumees Johan Link XXX
" Johan Udel  XXX
