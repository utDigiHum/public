Krimanis sel 20 Mai 1869.
Mõisa Wallitsus  M. Baumann tulli sel üllemal nimmetud päewal koggokonna kohto ette ja kaebas et  Selli tallo  wanna perremees  Mihkel Laett  on omma tallus talwel ärra põlletanud nimmelt:			Summa		23 palgi. Oelle küini põrmando palgi à 40 kop			9.20		80 sülda Õue aeda, 5 süld koorma 16 koormad			16.00		Pallutamise puud 4½ süld à 160 Cop			7.20		Summa			32 Rbl 40 kop		
 
Sepeale sai  Mihkel Laett ette kutsotud ja ta ütles, minna ollen need puud ja Aijad ärra põlletanud, kost ma piddin siis weel tooma kui mul jo Süggisel Oksjon tehti.
Siis tulli se uus tallo perremees Thomas Laett ette ja ütles: minna ei wõib mitte sedda wiisi se maija piddata ehk
2) kui Mõisa minno käest jälle se maija nõnda wiisi wasto wõttab kui minno aeg täis saab.
Mõistus: Krimani s sel 20mal Mai 69.
Koggokonna kohhus mõistis Mõisa wallitsuse nõudmisse perra agga wähhem nimmelt:			Rbl			Kop		23 lauda küini põrranda palgi eest			4			60		80 süld Aija puid			8			-		4½ süld pallutamisse puid			5			40		Summa			18 Rubla					
peab Mihkel Laett Mõisale ärra maksma.
Man olliwa:
Peakohtomees Jürri Bragli XXX
Abbi "  "  Johan Mina XXX
"  "  Ado Lenzius XXX
Kirjutaja W. Kogi /allkiri/
