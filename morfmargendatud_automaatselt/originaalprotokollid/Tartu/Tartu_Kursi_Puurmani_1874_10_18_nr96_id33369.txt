Sesamma kord
N 80. polele jänud.
Ettekutsutud tunnistaja Mihkel Willem ja wastas tema ei tea sest tüdrukust middagi, waid olla 1. kord teistega seal lakkas käinud.
Jürri Kipus tunnistab nende tütruku jures olla kül wahhest poissa kulda olnud ning Mihkel Willem olla kül Mihklipawa öösel lakkas käinud
Tõnnu Tuppits ütlep tema ei tea sest asjast midagi
Jaan Kikas ütlep: se tütruk issi poiste jure tükkid tema poegi hakkanud armastama ja tükkinud poegade jurde maggama ning tema keelnud poisid ärra agga tütruk läinud läbbi akna kambri poiste jurde.
Kohtuotsos.
Et need poisid keik oma tunnituse järgi on sel lakkas käinud ning öösel ümperhulkunud sawad Need Mihkel Willem, Tõnnu Tuppits ja Mihkel Koppel igga üks 15 loki witsa Kadri Weltman otse wälja näitab, et mitmed tema jures on käinud ning ka õiged aega ei tea ei woi middagi nouda.
Hans Perre XXX
Tonnu Bauer XXX
Karel Adolf XXX
Surema kohtu käimist sai kulutud
Kadri Weltmann polle kohtuotsusega rahhu.
