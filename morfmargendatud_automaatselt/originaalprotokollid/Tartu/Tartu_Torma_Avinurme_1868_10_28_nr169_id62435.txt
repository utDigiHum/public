Awwinorme Koggokonnakohtus sel. 28mal October 1868
Kohto juures olliwad
peakohtomees Mart Jaggo.
teine kohtomees Mart Liis
kolmas kohtomees Tomas Pärn
Ühhe Keiserlikku Kreiskommissairi kohto kässu peale sai tännasel pääwal Tomas Pihlak Tomas Habakuk ja Maddis Paas koggu konna kohto ette kutsutud, ning nendele teada antud et Tomas Pihlak selle Protokolli põhja peal mis siit poolt Ühhe Keiserlikko Kreiskommissairi kohtole ette pantud olli sel 20mal Julil N: 116. perremehheks kinnitud saanud, omma Wenna Jaani Pihlaka N 49 tallo peale, ja et nüid Tomas Habakuk kui hakkanud käemees, keik need koggukonna ja teised Wõllad peab sisse maksma, mis 100 Rubla peale wälja teeb,   
Astus ette Tomas Habakuk ja räkis et temma sedda asja nüid pikkemalt ja täiemalt järrele mõtlenud, ning ärra tundnud, et temmal sedda joudu ei olle, nii suurt wõlga sell waesel Aastal omma peale wõtta, ilma et temmal ühtegi tassumist selle eest nähtawal olleks, ja et temma selle lootusega käe mehheks hakkanud, et se tallo pärrast temma kätte sab, pallub agga temma nüid omma lubbadost koggokonna kohto käest taggasi, sest et temma sedda ilma järrele mõtlematta teinud.
Astus ette Tomas Pihlak, ja räkis et temma tahhab ommale üks teine käemees katsu, sest et Tomas Habakuk ennast taggasi wõtnud käemehhest.
Moistus
Ehk Tomas Habakuk essite küll Tomas Pihlakalle käemehheks lubbanud hakkada nüid agga selgeste ärra tunneb, et temmal sedda joudu ei olle sell waesel Aastal niisured Wõllad omma omma peale wõtta, ja Tomas Pihlakal ennestel ei koppikud selle Tallo wõlla peale sisse maksta ei olle, ehk küll temma jo Keiserlikko Kreiskommissairi kohto poolest perremehheks kinnitud sanud, ei woi agga koggokonna kohhus neid wõlgasid ennamb kauemaks wälja jätta, kellest ossalt jo Tomas Paas rahhaga ärra maksnud, moistab koggokonna kohhus, et Tomas Paas se tallo Maggasi wõlg mis 3 Tschetwerti Rukkid 1 Tschetwert Odre, ja 1 Tschetwert 24 Garnitz Kaero nikohhe peab Maggasi sissemaksma, ja et Wallawallitsus ja koggukonna kohhus sedda ärra tunneb, et Tomas Pihlakast ka eddespiddigi selle tallo piddajad ei sa - sab se Protokoll Ühhe Keiserlikko Kreis Kommissairi kohtole ette pantud suurema kohto otsuseks.
Mis tunnistawad kohtomehhed.
Mart Jaggo. XXX
Mart Liis XXX
Tomas Pärn XXX
