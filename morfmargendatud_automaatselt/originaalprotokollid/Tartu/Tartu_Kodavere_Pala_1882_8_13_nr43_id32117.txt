Tulli kohtu ette Anno Tragon otstarski soldati Karel Tragoni naene annab üllesse, et  kui Märt Piiri kirjutaja Fuchsiga Kihhelkonna Kohtus käinud, ja sealt tagasi tulnud, tulnud kirjutaja Fuchs nende maiasse, joonuwad Karel Tragoniga õllut, ja seal öelnud Fuchs: siis ma ei olle mees kui Wärwo Mardil ei olle Kepp ja Kott ja näidanud käega kõrri alla muud temma ei mälleta: Eddasi öelnud Fuchs 
Weldi Tomas, Wärwo Mart ja Palla  Stryk kisuwad üksteist hammastega. Temma tunnistada sellepärrast, et Fuchs temma mehhe Karel Tragoni wiimsel aial omma  kilda alla wõtnud, ja temmaga õlu kulista. Ta ei teada selle peale tunnistust üllesse anda. 
Kirjutaja Fuchs ei olle praegu kohtus.
Otsus: lähhemal kohtupäewal Fuchs ettekutsuda.
                                   Peakohtumees: H.Horn
                                         Kohtumees: Märt Piiri
                                         Kohtumees: Josep Soiewa.
