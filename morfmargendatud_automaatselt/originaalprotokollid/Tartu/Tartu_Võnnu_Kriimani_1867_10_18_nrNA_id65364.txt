Krimannis sel 18mal Octobril 1867. 
Krimanni koggokonna kohtu tulli kaebaja Jaak Turba ja kaebas, et temma perremees Jahn Poeddersohn kolme näddala eest tedda enda tenistussest ärra aijanud; sepeale sai Jahn Poeddersohn  ette kutsutud ja ta ütles: se om õige, kui mul üks sullane kes mo sõnna ei kuule ja nenda teeb nink lähhab kus ta meel mõtleb ja se olli nenda üttel Redetsel päewal kui mul keige kalgemb töö aeg ja suur Linna kakkominne olli, kui sömast ärra läitsime nurme peale Linno kakkoma kui ma ümber ringi kus Jaak mõtli et kül ta tulleb, ehk om mõtsa lännu, uutse ja kai ei keddagi senni kui Esmaspäew hommiko kui koijo tulli sepeale sai Jaak Turba ette kutsotud ja ta wastas ma läitsi reedi lõuna aijal Haslawa kohto, kos üks wannamees tedda kutsonud ja sealt tulno õhto omma Emma poole, nink emma üttelnu poig ma lei hommen Liina Jaak üttelnu temma ka lätt, et ma olle Pihlapu Jahni üttelnu et to Saabid meil om eespä luhta hainale minnek kus süggaw wessi om emma anna rahha sapid tua siis ei olle enne eespa hommokut koijo lännu sepärrast et mis perremees mõtleb kui tööpäiw ärra olnu om ja pühhapas koddo sööma lähha, kui eespa koddo saanu istnu pengi peale mahha ja tõmbanu piibu, perremees ei olle temmale middagi üttelnu; kui tõsse rahwas sööma lännu om Jaak ka söma hakkata sis ollewad Jahn Põddersohn temma mannu karranu ja söögi länniko eest ärra tõmmanu selle ütlemissega mis olled tennu ma so ei prugi minne kos kurrat.
Mõistus:  
Krimanni Koggokonna kohtus sel 18. Octobril 1867.
Koggokonna kohhus mõistis Jahn Poeddersohn peab  Jaak Turba enda mannu taggasi wõtma nink Jaak Turba peab nende 1½ töö päewa eest 70 Copka massma sepärrast et temma ilm ütlematta ja lubbata äkkitselt ärra katte,
II. peab Jaak Turba omma suure julguse ja wasto aijamiosse perrast, mis  ütski sullane tehja ei tohhi 30 witsa lööki saama.
Man olliwa:
Abbikohtomees Peter Püür XXX
"  Mihkel Laett XXX
Walla Wöörmünder   Peter Grünthal XXX
Koggokonna kohto kirjutaja W. Rauping /allkiri/
Jahn Poeddersohn nink Jaak Turba es olle mõllemad selle kohtuga rahho ja nõuawad mõllemad suuremad kohhut.
