Krimani koggokonna kohtus 12mal Julil 1868.
 Jürri Klaos  tulli koggokonna kohto ette ja kaebas et Koolmeistri tüdruk  Mina Herm  on minno ristik haina nurmest ½ wakka alla ristik haina ärra põimanud mis Juhan Rosenthal ja Jaan Otti tunnistawad et se tüdruk on minno ristik haina põimanud.
Koolmeistri tüdruk  Mina Herm tulli selsama päewal ette ja ütles: minno hingi ei tea sest ja kes sedda wõib üttelda et minna ollen  Jürri Klaose ristik haina põimamas käinud sest egga mul endal ellajad ei olle ja egga minna ommetige perremehhe ellajale ei lähhe haino warrastama.
Tunnistajad  Juhan Rosenthal ja  Jaan Otti tulliwad ette ja tunnistawad mõllemad et meie näggime sitta weddamise aego üks kord et Kolmeistri tüdruk Sappi aja kohhal tulli ja heina kott olli seljas agga sedda meie mitte ei tea üttelda mis heinad tal seal kottis olliwad kas ma wõi ristik heinad ja sedda meie ka mitte ei olle näinud kas ta Jürri Klaose ristik hainast wõi muijalt tõi. 
 Mina Herm sai jälle ette kutsutud ja ta ütles: se saab kül tõssi ollema et nemmad näggiwad et mul kot seljas olli ja Sappi aja werest tullin agga minna ei olle mitte Klaose ristik hainast tulnud ent minna tõin need hainad omma ristik haina ma werest ja olli ka omma ristik hainast mõnni peotäis kotti põhja pantud sest Koolmeistril olli kõwwaste ärra keeldud et ma mitte ristik heina ma pealt ei piddanud heino toma ning tullin selle haina kottiga möda krawi Sappi aja weerde wälja ja läksin õkwa omma wärratist sisse sest sedda teab jo egga üks et meie õue wärraw Sappi aja weren on.
Koggokonna kohhus ütles 12mal Julil et Jürri Klaos peab 4 näddali perrast selgemad tunnistust toma et Mina Herm on temma ristik haino warrastanud.
 Jürri Klaos  andis selsammal päewal tunnistaja üllesse et Kolga Tallo perrenaene on näinud kui  Mina Herm on minno ristikhaina põimanud.
Kolga Perrenaene tulli 26mal s.k. ette ja ütles minna ei olle Mina Hermi ei ka keddagi muud näinud seal ristik hainas käiwad egga tea ka sest middagi kõnnelda mis minno silm ei olle näinud.
Mõistus: 23mal Augustil 1868.
Koggokonna kohhus mõistis  Mina Hermi sest ristik haina wargusest priiks seperrast et   Jürri Klaosel ühtegi selged tunnistust selle peale ei olle et se tüdruk on temma ristik haino warrastanud ja
2) arwab koggokonna kohhus et se mitte koggonist wõimalik ei saab ollema et ükski päewal ajal senna wargile saab minnema sepärrast et se ristik haina nurm wasto Kolga tallo usse aeda on ja koggonist sure te weren kust allati innimeisi möda käib ja ka ülle jõe Päkste küllase selgeste ärra nätta on kui kegi senna ristik haina nurme peale peaks minema haina kakkuma.
Man olliwa:
Peakohtomees Jürri Bragli 
Abbi kohtomees  Ado Lenzius 
Abbi " Jaan Põddersohn 
Koggokonna kohhus on Jürri Klaosele keelnud suremad kohhut nõudmast seperrast et selle kaebtuse asjan Mina Hermi wasto ühtige selged tunnistust ei olle leitud - agga Keiserliko kihhelkonna kohto kässo peale sadab koggokonna kohhus se Jürri Klaose ja Mina Hermi Protokol № 101 s.a. 31 Aug Keiserliko kihhelkonna kohto 
Kirjutaja assemel  M. Baumann /allkiri/
