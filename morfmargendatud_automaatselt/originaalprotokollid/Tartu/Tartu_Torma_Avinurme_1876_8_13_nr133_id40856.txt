sel 13. Augustil 1876
koos ollid
teine kohtomes Maddis Sildnik
kolmas kohtomes Jaan Mänd
Abbi kohtomees Mihkel Jallak
Tõnnusaare külla perremees Jaan Pern kaebas et minnewa Aasta on koggukonnakohto mehhed Tonnusares käinud ning nende heinamaadele wahhet tehnod, tännawo Aasta on agga Jürri Unt jälle ülle piiri temma heinama seest heina wanna moodi ajanod. ja pallus koggukonnakohhut sedda selletada.
Teine perremees Jürri Unt räkis et temma ei olle suggugi kaugemalle heina ajanud kui temmale minnewa Aasta on näidatud. ja on koggokonnakohhos minnewa Aasta moistnod et keski ei tohhi karjamaa metsa mahha raijoda. Jaan Pärn agga tännawo Aasta jälle hulk metsa mahha raijunud. ning nenda wiisi jälle suur tükk külla karjamaad omma heinam külge wõtnud.
Tõnnusare perremes Jakob Klepanit tunnistas et se õige et Jürri Hunt on heina ülle selle piiri ajanud. mis nendele minnewa Aasta näid ja Jaan Pärn jälle üks tükk karjama metsa mahha raijunud.
Juhhan Aun tunnistas nenda sammuti et Jürri Hunt ülle piiri heino ajanud. ja Jaan Pärn jälle hulk metsa mahha raijunud.
Wallawannemb Juhhan Koppel ja kohtomes Jaan Mänd tunnistasid nenda sammuti et Jürri Unt ja Jaan Pärn mollemad ülle keelo lähnud. ja et se nende kohto käiminne ennamb üks kiusu kui hädda assi
Moistus.
Et se assi mis ülle Jaan Pärn ja Jürri Unt kohhut käiwad. muud ei olle kui kisu assi ning nemmad mollemad, ülle mõistose lähnod, mis nedele sel 4. Junil sel. 1876 Protokoll N 90. all moistetud sai siis on koggukonna kohhos moistnud et nemmad selle kiuslemisse eest ja ülle moistose teggemisse eest kumbki 6 Rubla walla laeka trahwi maksawad ja saab nendele ütteldud et kui nemmad omma kiuslemist mahha ei jätta ja mitte selle järrel ei tee kudda wiisi nendele on wahhe tehtud siis sawad nemmad ühhe ... trahwiga trahwitud.
Se moistus sai selle Liwlandi Tallorahwa säduse põhja peal Aastal 1860 §§ 772 ja 773 kuulutud ning nendele need säduse järrel Appellationi õppetusse kirjad wälja antud.
Jaan Pärn tunnistas et temma selle moistusega rahhul ei olle et temma selle eest peab trahwi maksma et temma omma heinama serwa on puhhastanud ja pallus Appellationi wälja suurema kohto minna 
Jürri Unt tunnistas nenda sammoti et temma selle moistusega rahhul ei olle et temma peab selle heinama eest. trahwi maksma mis temma issa nenda kaugele on ... kui temmale näidatud
Maddis Sildnik XXX
Jaan Mänd XXX
Mihkel Jallak XXX
Need lubbatud Appellationid said sel 14 Augustil Jaan Parnale ja Jürri Untile N 211. ja 212. all walja antud
