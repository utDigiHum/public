Protokolli päält s.s. No tulli ette Mai Tirik ja tunnist et öddaku tulnud Peter Kiter sial küünin kos temma ja töisetki magganut, temma hakkanut tõrrelema siis üttelnut Peter Kiter "wait, wait mis sa tännitat kurrat" nink läinut Jürri Wuksi poole ja kõnnelenut Jürri Wuksiga, agga mis nemma kõnnelenut, ei tia temma.
Moistus: Tunnistajate läbbi om selgest tehtu, et Peter Kiter ja Johan Purrik mullembat süallutset ommawa. Peter Kiter saab tolle eest et temma öösi töise honete seen tükkip 30 Witza löökiga teahwitus ja Johan Purrik sab taplemisse eest 20 Witza löökiga trahwitus.
Pähkohtomees: Johan Hansen XXX
Kohtomees: Ado Ottas XXX
Kohtomees: Jakob Jansen XXX
Moistus sai kuulutut nink suremba kohto käimisse õppetusse kirri wälja andtu.
