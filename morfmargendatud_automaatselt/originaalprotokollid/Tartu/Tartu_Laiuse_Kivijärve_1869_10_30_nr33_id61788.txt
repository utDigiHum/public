Tulli kohtu ette Otti Joh. Blauwet ja kaebas, et se pois Johann Jürgenson on temmale 2 wakka oddre müinud, 1 rubl. 50 kop. est wak ja on 2 rbl. 30 kop. kätte sanud ning nüid ei tahha sedda kätte anda.
Joh. Jürgenson ütleb, et temma on kül need oddrad müinud, piddi agga Joh. Blauweti käest 2 wakka oddre sama ja on selle rahha selle est wõtnud. Sai selle peäle mõistetud, et Joh. Jürgenson peab se rahha 2 rubl. 30 kop. ärra maksma ja wõib need oddrad nõuda Joh. Blauweti käest kui temmal tunnistust on.
Peakohtumees Hindrik Lepp [allkiri]
Kohtumees Peter Palm XXX
