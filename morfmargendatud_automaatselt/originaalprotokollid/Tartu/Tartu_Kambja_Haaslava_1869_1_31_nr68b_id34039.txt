Ütte Keiserlikku IVma Tarto Kihelkonna kohto kässu pääl s.s. 22 Tetsembri k.p. 1868 No 3969 sao Aslawa koggokonna kohto polest sel 23 Januaril 1869 No 45 selle ärra koolu perremehhe Jürri Wassila warrandus oksjoni wiisil ärra müüdu ning tulli üllepäh 54 rub. 17 kop. hbdt. sisse.
Kusta Wassila om tost warandusest mis prottokolli s.s. 25. Webr. 1866 No 20 olli ülles woetu, endale ärra prukinut, kai prottokoll s.s. 12 April 1868 No 83 - 64 r.
Nuit om agga se Kusta Wassila renti tarbis rahha enda poolest masnut - 17 rubla.
Kusta Wassila enda ossa jaggu - 27 rubla 10 K.
Summa 44 r. 10 K.
Siis tulleb temmal mannu massa 19 r. 90 K.
Summa 64 r.
Ning lubbas Kusta Wassila se wõlg rahha 19 rub. 90 kop hõbdt. kats näddalit päle Orriku lade 1869 koggokonna kohto man sisse massa; siis parhilla, selle halwa ajan ei ollewat temmal mingi modu pääl woimalik se rahha ärra massa.
Otsus: Saab prottokolli üllespantu ning piap K. W. se rahha mis temma selle ossa jaggule wölgo om kats näddalit päle Orriku lade 1869 ärra masma.
Pääkohtomees Johan Toom XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
