Haslawa kogokonna kohus 16 Mail  1880
Juures istus:
Peakohtumees Mihkel Klaos
kohtumees Johan Udel
kohtumees  Rein Anko
abikohtumees Joh. Jantzen
Tuli ette  Johan Klaossen  ja kaebas, et tema  Jaan Tenn teda hirmsal wiisil sõimanud wargaks ja hoorapäälikuks ja nõudis, et tema sellepärast trahwitud saab. 
 Jaan Tenn ette kutsutud, ütles kaebtuse pääle wälja, et tema on joobnud olnud ja ei mäleta mitte, kas tema on sõimanud wõi mitte.
 Joh. Luha tunnistas, et  Jaan Tenn on  Joh. Klaosseni kül sõimanud sandinussijaks ja hoorapäälikuks u.n.e.
Mõistus.
Jaan Tenn maksab 1 rubla hõb. Wallawaeste laadiku trahwi 8 pääwa sees. 
Peakohtumees  Mihkel Klaos
kohtumees Joh. Udel
dito Joh. Jantzen
Mõistus sai kohtukäijatele kuulutud. 
