Sel 16 Detsembril 1885
Kaibas  Kirbu Jaan Rosenthal, et tema rentnik Jaan Saag 24 Rubl renti wõlgu om ja pallus et kohus seda massma sunis.
Jaan Saag wastutas, et tema 22 Rubl renti wõlga om sest poole wakama om rahu jäetu ja lubas seda renti massa.
Mõistetu: et Jaan Saag 8 päiwa sehen se 22 Rubl renti Jaan Rosenthalile ärra massma peab.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Kohtumees: Johan Klaosen XXX
"  Peter Iwan  /allkiri/
"  J. Krosbärk /allkiri/
