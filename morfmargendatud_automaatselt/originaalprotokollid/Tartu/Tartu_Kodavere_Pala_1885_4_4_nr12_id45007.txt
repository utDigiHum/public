Tänasel pääwal olid Kogukonna kohtu liikmed ärasurnud Bernhard Adelsoni lese  nüdse Tõltsepa, Aidamehe talusse läinud ja lese ja waeste laste järele jäänud kraam ülewaatanud ja leidsiwad se kraam täielik kõigi pidi. Sellepääle wõttis uus wöörmünder Mihkel Lallo ameti oma kätte ja sai Gustaw Nukka lahti.
                                                                    Pääkohtumees O. Kangro
                                                                    Kohtumees      J. Soiewa.
                                                                    Kohtumees     O. Moisa.
Märkus. Ersehen, Ludenhof 21 Mai 1885 Kirchspielsrechtes allkiri mitteloetaw.
