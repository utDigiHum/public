Awwinorme Koggokonna Kohtus sel 28tal Mail 1876
koos ollid 
pea kohtomees Josep Kask.
teine kohtomees Maddis Sildnik
kolmas kohtomees Jaan Mänd.
Et Maddis Traks Walla wannemat ja koggokonna kohhut tühjaks teinud, ja üttelnud Kohtomehhe Jaan Mändi ja Walla wöölmöndri Mart Reisenpukki wasta, ma ei tunnegi neid, peale selle mittu kord se kohto kässo wasto pannud, ja ei olle kohto ette tulnud kui tedda kutsuti, miska ta omma ütlemist näitab tõe ollewad, moistab koggokonna kohhus Maddis Traksi selle eest 2 päwa ja öö peale trahwist kinni.
Josep Kask.
Maddis Sildnik.
Jaan Mänd. 
