Ette tulli Karl Koinurm ja kaibas temal olla Willem Perametsa käest seppatöö rahha saada mis see mitte heaga maksa ei tahta:
Minnewa aasta raha			5 rubl.   -		1. wanker rauta. raha			1 rubl.    -		sängi rauad			-            50 kop.		1 pikk raud			-            50 kop.		hobuse pääw			1 rubl.   -		jalla pääw			 -           50 kop.  		                      Summa			8 rubl.   50 kop.		minewa aasta wakka maa heina tegu			1 rubl.   50 kop.		se aasta maa heina tegu			1 rubl.   50 kop.		weski jurde seppa töö			16 rubl.   -		selle aasta tingi töö			 5 rubl. -		                         Suma			32 rubl. 50 kop.		
 Willem Peramets wastas tema ei tea mud kaibajalle maksa olla kui minewa aasta tingi töö eest 5 rubl. ja wankri eest 1. rubl ja sängi raude eest 50 kop see on kokku 6 rubl 50 koppik seega olleks kül pidanud selle aasta eest ka 5 rubla edasi saama Jürripäwal wälja maksta aga polle temale  midagi teinud  peale selle olla sepp tema weski pilli otsa ära rikkunud ja kripm. katki teinud selle eest nõuab tema 5 rubla. Peale selle olla temal weel nõuda 1 kerwes ja nõuab selle eest 5 rubla nii sama magasi wilja ½ wakka rukkid 1/3 wakka odre 1/3 wakka karo.
               Kohto otsus: weski töö saab kohtu poolt tagastud ja peab Koinurm tunnistaja andma selle ülle, et tema ülle kontrati heinu on teinud.
                             pä Kohtumees: Hindrek Horn
                             Kohtumees:       Märt Piiri
                             Kohtumees:       Josep Soiewa.
   
