Keiserliku Tartu I Kihelkonna kohtu poolt oli N: 8091 17 Septembril üle Tartu Sillakohtu akt Mart Piiri kaebuse asjus wastu Jaan Kubja selle kogukonna kohtu seletuse ja mõistmise alla saadetud sest et see asi üks liht teotus talupojade wahel on.
 Ettekutsutud Märt Piiri ja Jaan Kubja see otsus neile awaltud Märt Piiri palus süüdlast Liiwimaa Tal. Sääd. Raamatu järele  trahwida. Jaan Kubjal ei olnud kedagi  süüe juure ütelda.
 Otsus Et Jaan Kubja Märt Piirid awalikult teotanud on nagu tunnistused seda Tartu Sillakohtu juures õigeks teinud läheb Jaan Kubja T. S. R. § 1097 põhjusel ühe päewa ja öö peale wangi.
 See otsus sai Märt Piiri  ja Jaan Kubjale kuulutud ja täidetud.  Jaan Kubja lubas aga edasi kaewatada.
                                         Kohtumees J. Stamm
                                         abi Kohtumees W. Oksa.
                                         abi Kohtumees J. Tagoma.
                                          Kirjutaja C Palm. (allkiri)  
                                    
