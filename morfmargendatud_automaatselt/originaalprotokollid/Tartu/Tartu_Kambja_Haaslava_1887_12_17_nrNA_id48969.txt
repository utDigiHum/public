Haaslawal, sel 17 December 1887.
Tikku  Johan Udel ja tema poeg Lillu Udel tegiwa täämbatsel päiwal see lepping ära:
Johan Udel annab oma pojale Lillu Udel kätte oma Tikku talu eesõiguse, niida wiisi et Lillu Udel wõib mõisaga warsti ostmise kontrakti äratetta ja temal ei ole selle wastu mitte midagi ütlemist enamb, annab aga see talukoht see Jüripäew 1888 Lillu kätte niida kui talu praegu saisab. Lillu lubab need 16 palki, mis tema käen ommawa ja niidasama need palgid, mis Tikku usaija pääl saisawad, esale kätte ja need weikesed 2 wastset kolmesainaga urtsikut, mis tare küllen ommawa, pääle see 3 wakamaad nurmest risti otsast Kesa piiri wastu 1 wakamaa luhast Sawipajo wasta, see maa jääb 10 aastat kui esa om enne seda aega ära surnu, kui aga esa weel rohkemb aega elab, siis jääb see 4 wakamaad maad seni esa kätte kui temal ellu om, pääle see lubab Lillu oma esale aga paljas sest aastast 1888, see rükkitük, mis kesknurmen om, rüad ühes õlgiga omale wõtta, pääle selle aga Johanil ei olle enamb õigust selle rüa pääle nõudmist. Lillule ei ole welle Johani urtsiku poolest selle aja perrast mitte midagi nõudmist, aga peawat Johani naene ja latset selle urtsiku perrast wastset leppingut tegema. Orjust seni kui Johan elab ei ole temal mitte tetta. Selle tarre ja kambre klaasid ühes raamidega wõtab Johan ära.
Selle lepinguga on mõlemad rahul olnud.
Lepijad: Johan Udel XXX ja Lillu Udel XXX
Tunnistajad: Johan Link XXX, Jaan Hansen /allkiri/, Peter Märtin /allkiri/
Otsus: Seda protokolli maha kirjutada.
Pääkohtumees Johan Link XXX
Kohtumees Jürri Kärn XXX
"  Jaan Hansen  /allkiri/
