Kasak Toomas Welt oli juba 12 Juunist saadik s.a. käsk saanud Liisa  Lall Johani  naine kogukonna kohtu ette tarwitada aga ei olnud ka mitte  seniajani käsku saanud. Sellepeale andis kohus temale käsk Liisa Lallile  järele minna. Tagasi tulles ütles kasak ,, kui teie Märt Lalli naist tahate saata siis wõite teda saata aga Johani naist ei leia mina mitte" .
  Otsus: et üks kohtumees puudub jääb otsus  teise korra peale.
                                       Peakohtumees J. Sarwik
                                       Kohtumees       J. Stamm
                                       Kirjutaja Carl Palm, (allkiri)
