1905 aasta jaanuari kuu 27 päewal kinnitadi järgmine suusõnaline leping:
1. Aleksander Korknobel müüb oma Laewa wallas Mägra talus ja Makkari talus Kudina wallas olewat sindli lõikuse kuurid kui ka sindli lõikuse saeraamid saagidega Tõnu Korknobelile ükssada kaheksakümmend wiis (185) rubla eest ära, mis raha T. Korknobel kolme aasta jooksul wäljamaksma peab.
2) T. Korknobel peab sindli lõikust omal wastutusel ja arwel edasi ostab materjali ja maksab ja palkab töölisid, ja kõik muud kulud kannab ise.
3). On T. Korknobel kohustatud Aleksander Korknobelile, kui tema Alexander Korknobeli aurukattalt sindli lõikuse tarwis pruugib, üht iseäralikku maksu iga töö päewa eest maksma, selle tööpäewa maks on mõlemate kokku lepimise järele.
4) Alexander Korknobel kohustab T. Korknobelile tema lõikuse juures olewate tööde juures abiks olla, nagu metsa ostmise ja tööde järele waatamise juures, tema wõib T. Korknobeli heaks sindlite eest mis müitud saawad raha wastu wõtta ja ka kui tarwis T. Korknobeli töötegijatele palka maksa, nõnda sama ka metsa materjali eest maksusid toimetada. Tasuks selle toimetuste eest olen mina
T. Korknobel kohustud Alex. Korknobeli rehe peksu masina juures järelewaatamise sügisel oma peale wõtma, kus mina ka A. Korknobeli heaks töörahad wastu wõtma walmis olen.
   Lepingu tegijate allkirjad
      Kohtuesimees J. Soiewa
      Kirjut. 
