Krimanni koggokonna kohtus 11mal Webruaril 1866.
 Jürri Klaos  kaebas koggokonna kohto ees et  Hindrik Päiw on minnew aasta temma ommandusest wargust wasto wõtnud nimmelt rehhe mant rükkid ja weski mant püidli jahho ja tunnistas et minnew aasta on temma rehhe mant rukkitee läinud senni kunni Hindrik Päiwa majale rükki kotti kõrwale ja mis ka Jürri Klaos kohtomehhe Jaan Iknatiga ollid ülle sedda rükki teed käinud.
Sai tunnistaja  Jaan Iknat ette kutsutud ja ta ütles: kaemas käinud ja tunnistas et rukki tee on kül  Jürri Klaose rehhe mant  Hindrik Päiwa majase rükki kotti juurde läinud agga sedda ei wõin ma mitte tunnistada kes sedda rükki teed on senna teinud.
 Hindrik Päiw tulli ette ja ütles need olliwad minno omma rukkid mis kotti sissen ollid agga  Jürri Klaose rukkitest ei olle ma mitte üks terra omma silmaga näinud egga wasto wõtnud ja ütles, et se ehk wõib olla et mõnni ehk on kiusta perrast sedda rükki teed minno pole teinud.
Siis tulli weski pois Jakob Adamson ja tunnistas et  Hindrik Päiw on üks kord temma juurde weskile tulnud ning küssinud kas kogi jahho saab mulle ka, siis kostnud Jakob Adamson ei sa neist need jahhud on wägga mustad kogi tarwis, siis läinud Hindrik Päiw ärra ja tulnud tük aja perrast taggasi ja küssinud kas nüid annad mulle kogi jahho ja üttelnud enne ikka teised poisid mulle püidli jahho antsiwad, kas sinna siis nüid teisite ellama hakkad kui meie endised weski poisid et sinna mulle ei tahha anda ja üttelnud nüid on jo kül se kord waggane aeg et wõib ärra wia kui sa agga walmis panned ja mulle annad, siis kostnud Jakob Adamson ei sedda ma kül mitte ei teen et ma sulle perremehhe jahho hakkan andma.
Hindrik Päiw tulli ette ja tunnistas et temma ei olle mitte weski poisi Jakob Adamsoni käest jahho küssinud egga polle temma ka jahhudest mingisuggust jutto kõnnelnud ja Hindrik Päiw ütles: ma ei olle Jürri Klaose jahho warguse wisil mitte üks nael omma majase wasto wõtnud.
Mõistus: Koggokonna kohhus 11 Febr 1866.
Koggokonna kohhus mõistis  Hindrik Päiwa priiks sepärrast et  Jürri Klaosel  selged tunnistust ei olle et  Hindrik Päiw on wargust wasto wõtnud.
Koggokonna peakohtomees Josep Kerge XXX
"  "  Abbi "  "  Jaan Aberg 
"  "  Abbi "  "  Jaan  Müirsep 
Jürri Klaos ei olnud selle mõistmise rahhul ja wõttis Protokoli № 55 Koggokonnast wälja 14mal Febr. 1866.
Kirjutaja assemel W. Baumann /allkiri/
