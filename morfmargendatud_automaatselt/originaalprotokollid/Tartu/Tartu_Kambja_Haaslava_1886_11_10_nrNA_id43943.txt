Sel 10 Nowembril 1886
Selle otsuse päle sest 27 Oktobrist s.a Johan Pikka kaibuse asjan wastu Jaan Roiland tunistas Marri Must, et Jaan Roiland ostnu 12 rubl est kuhjast heino Johan Pikka käest, need heinad awitanu Jaan Roiland essi omma sullasele päle teha ja satnu sullase heinaga koddu ja sõitnud essi linna tagasi tulleki jures massnu Jaan Roiland 7 rubl ärra ja jänu 5 rubl wõlgu.
Mõistetu: et Jaan Roiland 8 päiwa sehen se 5 rubl Johan Pikkale ärra peab massma.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Kohtumees:  Jürri Kärn XXX
"  Jaan Hansen /allkiri/
"  Johan Opman XXX
"  Johan Raswa XXX
