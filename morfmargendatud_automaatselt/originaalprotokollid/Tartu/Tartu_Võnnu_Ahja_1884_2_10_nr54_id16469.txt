Hindrik Luude kaibas Peeter Rüütli käest teenistuse ajal minewal aastal 5 rubla pääraha kaubeldud olewat, mis see aga maksmata jätnud nõuab, et maksetud saaks.
Peeter Rüütli ei salga.
Mõiste: Peeter Rüütli maksab 8 päiwa sees Hindrik Luude pääraha 5 rubla wälja.
Kohtumees: P. Hindrikson XXX
Kohtumees: Kotta Wossman XXX
Kohtumees: Hansing
Mõiste sai kuulutatud ja rahul.
Kirjutaja: Chr Kapp &lt;allkiri&gt;
