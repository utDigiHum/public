Protokolli pääle 5. Maist 1888. N: 72 astus ette Mihkel Tedder ja andis üles, et tema olla kad. esimese naese kraam äramüünud aga keigest 8 rubla saanud, kohe juure tema omalt poolt 2 rubla paneb, nii et 10 rubla on. Tema lubab see raha ära maksa ja maksis ka oma poja Johannes Tedre heaks 10 rubla sisse.
          Otsus: raha intressi pääle panna.
                  Peakohtumees M. Piiri
                        Kohtumees W. Oksa
                        Kohtumees K. Hawakiwi
                 Kirjutaja O. Seidenbach.
                
