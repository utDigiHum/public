№ 19
14 Haslawa  wallakohus sel 16 Detsembril 1891.
Jures ollid: Eestistuja Jaan Treijal
Kohtomees:  Gotfried Meos
"  Peter Iwan
"  Karl Ango
Tulli ette  Kessa tallo ostja  Johan Zobel ja Haslawa Kogukonna liige  Märt Lääts ja pallusit neide leping ülles kirjutada.
Kessa tallo ostja  Johan Zobel tunistab, et se majakene, mis  Märt Lääts Kessa tallo heinamaa otsa peale om ehitanu, selle  Märt Läätsi omandus om, kes seda essi oma kulluga om ehitanu.
Lepingu tegija ei mõista kirjutada ja neide est kirjutas Johan Laber /allkiri/
Otsus: Seda protokolli ülles panda.
Eestistuja: Jaan Treijal /allkiri/
Kohtumees: Peter Iwan /allkiri/
"   Gottfried Meos /allkiri/
"  Peter Iwan /allkiri/
"  Karli Anngo /allkiri/
Kirjutaja: J. Weber /allkiri/
