Haslawa Kogukonna Kohus sel 11 Märtsil 1885.
Man ollid: Päkohtumees: Jaan Link
Kohtumees: 
 Peter Iwan
"  Johan Grossberg
Kaibas Roijo  Jürri Klaos, et temal 3 Rubl Sola Jaan Rosenthali käest sada om, mis jobba 6 asta est sanu  ja pallus et kohus seda massma sunnis.
 Jaan Rosenthal wastutas, et tema se 3 Rubl kartoflide ostmise käeraha sanu om, ent tema om 3 hobusega Kawastu kartoflid wiinu, seal ei olle neid wasto wõetu ja tema om kartofli tagasi tonu selleperrast ei taha tema seda raha enamb tagasi massa.
Jürri Klaos ütles, et tema nõuab Jaan Rosenthali käest kahjutasumist ja selle 3 Rubl 6 asta intressi.
Mõistetu: et Jaan Rosenthal 8 päiwa sehen se 3 Rubl Jürri Klaosele ärra massma peab.
Se mõistus sai kulutedo ja õpetus leht wälja antu.
Päkohtumees: J. Link /allkiri/
Kohtumees:
 Peter Iwan /allkiri/
J. Krosbärk /allkiri/
