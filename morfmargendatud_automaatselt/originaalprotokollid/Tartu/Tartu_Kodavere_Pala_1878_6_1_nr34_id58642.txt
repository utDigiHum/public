Ette tuli Palla mõisa walitseja ja andis ülles Mõtsawaht Josep Eber olla rohhu wargid norest raisist kinni wõtnud ja ka nende  kottid käest ärra wõtnud, need olnud Liisu Soo, Josep Soo naine ja An Tubbin kes keik Lillu tallos ellanud.
  Ette tulli perremees Gustaw  ja  andis ülles, et tema innimesed on tema oma heinama pealt rohtu kiskunud aga mitte  Mõisa räisist ja temale  on omad rohtu küll pealegi ei olle Mõtsa waht tema rahwast mitte Mõisa mõtsast kinni wõtnud waid  ennegi tema Majast on mõtsa waht oma wõimuga need rohhu kottid tema Majast ärrra wiinud ilma et olleks tedda ehk  amet meest kutsunud kui olleks rohhu  ülle watanud olla tema onni läinud ja onni ukse all temal 3 rubla eest kapsa taimi ärra tallanud. Mõtsa waht andis wastuse tema olla kaugelt näinud et naised on Mõtsast tulnud Tuhha heina ma peal istunud ja tema on järgi läinud ja kodust kül kottid ärra tonud.
 Lena Soo Josepi naine andis wastust, et  kost nemad on järwe otsast Loddo pealt  mõtsa rohtu kiskunud ja koddu tonud on kül Mõtsa wahti näinud aga mitte warjanud ega kartnud ja on perremehhe maa olnud.
 Tema on oma peremehhe heinama pealt rohtu kiskunud aga mitte Mõisa metsast.
 Mõisa nõuab 50 Kop. kotti pealt  3 kotti on  1 R. 50 Kop. 
     Kohtu otsus  tene kord saab eddasi tallitud se pärrast, et  Päkohtunik kaebatu suggulane on.
                                             Kohtumees Gustaw Nukka
                                                  teine        Thomas Welt
                                               Kirj:              Fucks.         
