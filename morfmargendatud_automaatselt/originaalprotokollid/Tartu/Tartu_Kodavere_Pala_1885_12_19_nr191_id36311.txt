Tuli ette Jürri Tõltsep ja kaebas et temal weel wallast kattusse ehituse õlgi saada on:
Lauka Liisu sauna eest 			90 leisikat		Thomas Linnu käest			20 leisikat		Kusta Soo käest			  4 leisikat		Josep Nõmm käest			  8 leisikat		Jaan Kokka käest			16  leisikat		Mihkel Mõisa käest			16 leisikat		                                      Summas			154 leisikat õlgi		
Selle Lauka Liisu sauna juure oliwad õlgi wiinuwad:
Jaan Welt			25 leisikat		Karel Olla			25 leisikat		Kustaw Beifeld			25 leisikat		Märt Piiri			20 leisikat		Jakob Tuhha			20 leisikat		Kustaw Rosenberg			25 leisikat		Maddis Waddi			25 leisikat		Reinhold Tamm			25 leisikat					190		
Kohus tegi otsus: wallawalitsususe ülesanda nõukogule ette panna kudas wiisi need 90 leisikat õlgi Tõldsepale äratasutud saama peawad. Teised  oled aga nimetud inimeste käest sissenõuda.
See otsus sai Tõldsepa ja wallawalitsusele kuulutud.
                                      Pääkohtumees O. Kangro
                                      Kohtumees       J. Soiewa
                                      Kohtumees       M. Mölder
                                      Kirjutaja             allkiri.      
