Tulid kohtu ette Joosep Kerge ja Jaan Loko ning andis üles, et Joosep Kerge, et tal omal poegi ei ole Jaan Loko poja Kristjan Loko, sündinud 25 Oktobril 1878, omale pojaks wõtab kõiki neide sääduste ja õigustega mis T.S.R. §952 põhjusel aastast 1860 ihulikul lapsel on ning oli Jaan Loko sellega rahul.
Seda tunnistawad nemad oma allkirjaga
Josep Kerge XXX Jaan Lokko &lt;allkiri&gt;
Otsus: Seda protokolli ülespanda
Kohtumees: P. Hindrikson
Kohtumees: K. Wossman
Kirjutaja
