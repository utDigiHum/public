Haslawa Kogukonna Kohus sel 12 Januaril 1887
Man ollid:
Kohtumees: Jürri Kärn
"  Johan Opman
"  Jaan Hansen
Selle protokolli täienduses sest 29 Detsembrist 1886 pallus Jaan Kliim weel protokolli panda, et temal Haslawa mõisa perrisherra ja mõisawalitsuse käest mitte midagi asja pält enamb nõudmist ei olle selle Klima tallo perast.
Otsus: Seda protokolli ülles panda.
Kohtumees: Jürri Kärn XXX
Johan Opman XXX
Jaan Hansen /allkiri/
