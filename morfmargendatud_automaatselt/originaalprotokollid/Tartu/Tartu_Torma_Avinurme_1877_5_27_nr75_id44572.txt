sel. 27 Mail 1877.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomes Jürri Tomik
Josep Krup kaebas et Jakob Hallik on juttu tehnud temma Naise peale et temma on siis kui se tüdruk olnud. liderlikku ellu ellanud. ja temmaga kurja töed tehnud ja pallus koggokonnakohhut et temma sellega rahhul ei olle et Jakob Hallik temma Naise peale öht nisuggust häbbematta juttu tehnud ja temma abbiellu rikku püiab.
Jakob Hallik kostis selle kaepamisse peale et se jutt hopis walle ja olle temma Josep Kropi naesega üht nisuggust häbbematta töed tehnud egga ka sedda juttu kellegille räkinud.
Mart Hallik tunnistas et Jakob Hallik on temmale räkinud et temma Josep Krupi Naise kui se tüdruk olnud. ärra raiskanud. ja woib temma selle ülle wanduda et Jakob Hallik temmale sedda räkinud.
Moistus
Et Jakob Hallik nisuggust häbbematta juttu jo ennegi ajanud ning selle eest trahwitud sanud temma agga jälle uwweste nisuggust juttu räkinud ja abbiellurahhusid rikkuda püiab, siis saab temma selle eest 15 witsa lögiga trahwitud.
Josep Laurisson XXX
Andres Kiik XXX
Jürri Tomik XXX
Jakob Hallik pallus Protokolli walja suurema kohto minna mis temmale agga keldud sai ja agga üks Formoleeri taht wälja antud.
