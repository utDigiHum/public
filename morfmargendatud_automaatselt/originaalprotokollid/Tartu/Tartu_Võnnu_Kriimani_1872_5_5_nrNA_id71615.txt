Krimanni koggokonna kohtus 5mal Mail 1872.
Korristalt Marri Mutso  tulli sel 7mal sija kohto ette ja kaebas et minno poeg Karel olli Johan Semenil minnew suwwel karjusest kaubelnud et piddin sama 1⅓ wak rükid 1⅓ wak keswi ja 10 naela linno 1 nael willo ja sullaste naeste 2 lehma eest 2 Rubla. Wilja palk ollen kätte sanud. 1 sullase lehma eest ollen 1 Rubla ka sanud ja on weel sada 1 Rubla rahha 10 naela linno 1 nael willo.
 Juhan Semen tulli ette ja ütles: minno naene on selle sea poisiga kaup teinud 21 April.
Johan Semeni naene ei olle sel 28mal Mail mitte haige jalla perrast kohto ette tulla sanud.
Lena Seemen tulli sel 5mal Mail kohto ette ja ütles: minna ollin sedda karjust kül endale kaubelnud se on üks keig kas mo karjas 1 wai 9 ellajad on sepärrast olli mul Karjus kes mino ja minno sullaste ellajaid hoidis, mis mul talle maksa olli ollen keig kätte maksnud.
Mõistus.
Krimanni koggokonna kohtus 5 Mail 1872.
Kohhus mõistis:  et  Marri Mutsol mingisuggust tunnistajad kohto küssimise peale ette anda ei olnud wait enne weel kotto ärra tahtis käija ja siis tunnistajad anda, sepeale on se koggokonna kohhus Lena Semeni maksust prii mõistnud, et kumb kummale egga üksteisega middagi teggemist ei pea ollema.
Kohto lauan olliwa
Peamees Johan Mina XXX
Kohtomees Karel Põdderson XXX
Abbi "  Johan Raswa XXX
