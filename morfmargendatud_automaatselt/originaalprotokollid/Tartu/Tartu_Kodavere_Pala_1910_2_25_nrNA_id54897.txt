Keisri Majesteedi käsu peale. 1910 aastal webruari kuu 25 päewal waatas Pala wallakohus Jurjewi kreisis Liiwi kubermangus Pala wallamajas awalikul koosistumisel, eesistuja K. Wahi ja kohtu liikmete J.Otteri, J. Uuseni, P. Erma osawõtmisel ning kirjutaja Brükkeli juuresolekul  Pala mõisaomaniku Ar. Stryki woliniku Nemwalzi ja Konguta walla liikme Jaan Marmoni palwe mõisa maa kirjaliku kontrahi ustawaks tunnistamise pärast läbi ja leidis, et Nemwalz on wallakohtule isiklikult tuttawad, Marmon oma isiku tõenduseks Kongota wallawalitsuse passi ette paneb.
          2) Lepingu tegijatel on täieline seaduslik õiguse wõimus.
          3) Nende poolt ettepandud akt on tempelmaksu seaduse põhjusel kirjutatud, ja nimelt .......ning ei tõuse oma hinna poolest mitte üle 300 rubla.
          4) Pooled on akti sisuga täieste rahul ning on teda allakirjutanud.
 Sellepärast sai 1889. a. Wallakohtu sead. II jau § 278........ järele 
                                                          määratud:
 Palujate poolt ettepandud kirjalik maakoha kontraht ustawaks tunnistada ja wallakohtu akti raamatusse N..... all 1910 a. sissekanda, algupäraline akt aga tarwiliku päälkirjaga pooltele wälja anda.
                     Eesistuja: K. Wahi
                     Kohtuliikmed: J. Otter J Uusen P. Härma.
                     Kirjutaja Brükkel.
