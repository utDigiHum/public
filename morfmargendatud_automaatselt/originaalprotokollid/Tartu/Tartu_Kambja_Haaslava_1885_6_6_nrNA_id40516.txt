Haslawa Kogukonna Kohus Sel 6 Junil 1885
Man ollid: päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
"  Peter Iwan
Tulliwa ette Jaan Põdra laste Wölmündre  Mihkel Rattasep ja Johan Luha ja andsiwa ülles, et kui Jaan Põder naise wõttis 1870 astal om tema naisega ütten 255 Rubl selged raha ja päle selle wel järrelseiswa muu warrandusse sanu:			Wärt Rubl		Selged raha			255		1 hobune 			60		1 raudtelgiga wanker			30		2 Lehma			70		5 Lamast			15		4 Wakka Rüki			12		10 wakka kartoflit			8		1 Kuhwri rõiwaste			20		2 wakka tatriko semned			4		1 sögi kap			5		Summa			499		
Selle et nüid Jaan Põdra naene surno om ja lapsi sest abiellust perra om jänu ja se Jaan Põder essi Kohtu mõistuse perra keik inemiseliko ja essi erraliko õiguse kaotanu ja nüid wangin om, pallusiwa Wölmündre et se sisse todu warrandus neide abbiellust sündinu lastele selle Jutra tallo päle kinnitedo saas, mis Jaan Põder Haslawa ostnu om.
Otsus: et Jaan Põder selle kaibuse perrast  Keiserliko Tarto Polizei kohtu läbbi küssida om.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
 Peter Iwan /allkiri/
