Ette tulid isiklikult tuntud Eduard Mihkli p. Põder Pala wallast ja Madis Jaani p. Ertis Ranna wallast pärit ja  palusid järgmist lepingut protokolli wõtta, mis nemad oma wahel läbirääkinud ja maha teinud.
I. Madis Ertis wõttab Kadrina walla Otsa N: 11 talu pooled maad rendile ühe aasta peale 23 apr 1907 kuni 23 apr 1908 talu terwe suurus on 15 tald. 30 grossi. Põldudest saab rentnik pooled, aga karjamaa on ühine. Rendi summa on 105 üks sada wiis rub. aastas.
II: Madis Ertis maksab 50 wiiskümmend rub. kautsjoniks sisse, mis kontrahi lõpuni ilma % Eduard Põderi käes seisab. Kauba kinnituse juures maksab rentnik poole renti ette, mille suurus 52 rub. 50 kop. on.
III: Rentnik peab suured künnid talu tõise poole pidaja Josep Rosenbergiga ühes koos kündma, ja nõnda, et tõine tõise hobuse annab.  Adra on Rosenbergi oma. Kündjad isi on ka wastastiku pooliti.
IV. Rentnik peab taliwilja põllu suuruse järele maha külima õigel ajal, aga peremehel on õigus järele waadata, et seeme puhas on. On põllult  saadud seeme karukaerane, siis peab rentnik oma kulul uue mutersema ehk wahetama.
 V.Rentnik  peab 4 karielajat üle talwe pidama, aga mitte wähem. Küll wõib ta aga neid rohkem pidada. 
 Tuntud Palla walla lliikmete Eduard Mihkli p Põder ja Josep Jüri p Rosenbergi ja Ranna walla liikme Madis Jaani p. Ertise poolt alla on kirjutatud  mikspärast otsustati lepingut kinnitada.
    Esimees: K Reinhold
    liikmed W Tamming
aj. Kirjutaja Brükkel.
