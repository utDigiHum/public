Protokolli peale tänasest päewast N. 132 saiwad Karl Sallole ülekuulatud tunnistused awaltud ei olnud temal kedagi tunnist. juure anda.
 Otsus: et kolm tunnistust selgest teinud on, et Karl Sallo ise ütelnud et see raha 3 rbl. ja kiri Jürri Allikale kätte saanud kelle kätte tema ka saama pidi, muud kui jonni perast  tahta tema kohut käia, jääb Karl Sallo  nõudmine wastu Alexander Rosenberg tühja. See otsus sai kuulutud ei olnud Karl Sallu sellega rahul ja anti temale Talurahwa Sääduse Raamat § 772, 773 ja 774 põhjuseks tarwiliste 
 õppustega appellation kätte. Alexander Rosenberg aga jäi mõtlemise peale.
                      Peakohtumees J.Sarwik.
                      Kohtumees       J. Stamm.
                      Kohtumees      M. Piiri.
                       Kirjutaja G.Palm. 
