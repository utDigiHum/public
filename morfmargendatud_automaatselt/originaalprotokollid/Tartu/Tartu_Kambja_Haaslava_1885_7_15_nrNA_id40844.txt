Sel 15 Julil 1885
Tulli ette  Soola tallo perris ostja Jaan Rosenthal ja pallus kinnituses protokolli panda, et tema selle maa, mis Soola tallo Ihaste luha heinama sehen künda kõlbab omma wennale Karl Rosenthali 16 asta päle renti päle annab se om Jürripäiwast 1886 astast sadik 16 astat edesi; Karl Rosenthal massab selle maa est selle aija pält üts sada rubl [ei loe välja] nüid ütte kõrraga ja wahib selle Soola tallo luha heinama ja heina kuhja. Karl Rosenthal ehitab essi honet selle koha päle ülles, need honed jäwa ka perrast seda rendi aiga Karl Rosenthali omanduses.
Otsus: Seda kinnituses protokolli ülles panda.
Päkohtumees: J. Link /allkiri/
Kohtumees:
Johan Klaosen XXX
Peter Iwan /allkiri/
