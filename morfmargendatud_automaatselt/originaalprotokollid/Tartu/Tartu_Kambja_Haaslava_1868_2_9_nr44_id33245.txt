Jürri Klaus, Krimanni wallast tul ette ja nowwap Jaan Opmanni käest 6 rubla hõbbedat wõlg rahha kätte.
Jaan Opmann wastutap, et temma saanud kül neit 6 rubla Jürri Klausi kaest, temma andnut agga warrsti se rahha Jaan Pruksal renti massmisse tarwis ja kui temma se rahha Jaan Pruksi käest kätte saab, siis ta massap Jürri Klaussil ärra.
Moistus: Jaan Opmann piap 2 näddali seen se wõlg rahha 6 rubla hõbbedat Jürri Klaussil ärra massma nink om temmal lubba antu Jaan Pruks pääl kaibust tõsta.
Pääkohtomees Johan Hansen XXX
Kohtomees Ado Ottas XXX
Abbi-Kohtomees Jaan Wirro XXX
Moistus sai kuulutud nink suurremba kohtokäimisse õppetusse kirri wälja andta.
