Mõisa kandikoha orjuse kontraht. 
 Tänasel päewal on Mekkaste mõisawalitsuse ja talumehe Karel Tompson wahel järelseisaw kontraht täieste ära mõteltud, wälja märgitud ja kirja pantud:
§ 1. Mekkaste mõisawalitsus annab seda Songri mõisamaa kandikohta, kelle wäärtus on 18 tadrit 2 krossi 1 aasta peale, see on 23. Aprillist 1905 kuni 23. Aprillini 1906  a. Karel Tompsoni kätte orjuse rendi peale.
§ 2. Nimetatud koha eest maksab Karel Tompson aasta ülepea 26 rubl kop. raharenti, ja nimelt; 1sel Aprillil 13 rubl. .... kop.. ja 1sel Oktobril 13 rubl...,.kop. ette ära, ja peale selle on tema kohus weel järgmised osatükkid teha: tema peab
     1)   8 wakkamaad talwewilja lõikama ja kuhja panema.
     2) 14  wakkamaad suwewilja lõikama.
     3)  4 wakkamaad ristikheina tegema.
     4)  26 wakamaad maaheina tegema.
     5) - wakamaad wikkid niitma.
     6)  4 wakka alla kartohwlid wõtma, ja peab neid ärawedama kuhu mõisawalitsus käseb.
     7) 15 sülda puud 1 arsin pikkad.........wedada.
     8) 164,5 sülda teed teha, kus mõisawalitsus käseb, 1 süld, 7 jalga.
     9) 130 päewa tegema.
     10. ..............................................................................................
     11. ...............................................................................................
         Nende osa tükkide kokku panemine saab 13. Nowembril 1860 wäljaantud Liiwimaa talurahwa seaduse § 168 järele nõuetud.
§ 3. Hoonete parandamise wõi ehitamise palgid ja lattid peab rendikoha pidaja kuni Mihklipäewani üles andma, ning peab kandikoha pidaja ennast kõige asja poolest metsa seadmiste alla heitma; kui ta nende seadmiste wastu eksib, kaotab tema oma koha käest.
§ 4. Selle kandikoha pidaja peab seda tema käes olewat kohta kõige sündsa põlluharimise seaduste järele tallitama, ja kui selle kontrahti aeg mööda on, peab tema seda kohta järelseiswal wiisil jälle mõisale tagasi andma:
      a) kõik hooned ja nimelt nende katuksed olgu heaste parandatud;
      b) rukki wäli heaste haritud ja õigel ajjal hea seemnega külwatud;
      c) aiad heaste parandatud;
      d) heinamaad hagudest puhastud ja kõik kraawid roogitud;
      e) tööde poolest eesseiswa töö aasta peale peab rukki kõrs üles küntud ja ära äestud olema;
      f) põletus puud.... sülda 1 arsina pikkused koha peale jätma.
§ 5. Peaks kandimehel mõisawalitsuse käsu peale uueste hoonid ehitada, ehk ka parandada olema, siis peab kandimees seda oma kuluga ilma kahjutasumise nõudmiseta tegema. Niisuguse ehituse juure annab mõisawalitsus ilma maksuta palgid, aga keik kandimehed peawad neid oma maa ja suuruse järele ilma maksuta ehituse platsile wedama, ja niisama ka hulga kaupa ilma maksuta oma maa suuruse järele, mõisawalitsusse wäljaarwamise järele, hoonete katmise õlgi andma. Karjalauda kiwi wundamendi tegemine saab mõisawalitsuse poolt ruutsülla wiisi maksetud. 
§ 6. Omad põllud peab mõisamaa kandikoha pidaja neljas jaos tarwitama, nii et:
                                8 wakka alla rukki all seisab,
                               16 wakka alla suwewilja all seisab,
                                 8 wakaalla kesanurmen seisab 
                              
     ja ei ole temal õigust ühtegi suwewilja kesanurmesse külwata.
§ 7. Õlgi, heinu ega muud loojuste toitu ei tohi selle koha pidaja ei kellegile wälja laenata ei ka ära müüa.
§ 8. Mõisawalitsusel on õigus selle koha majapidamist igal ajal läbi waadata, ehk omast wolinikust läbi waadata lasta.
§ 9. Lina wõib selle koha pidaja üksi 3 wakka alla maha külwata.
§ 10. Kui mõisawalitsus selle koha pidajale üles tahab ütelda, siis peab see 23. Oktobri kuu päewal enne selle kontrahti lõpetust sündima, ja selle koha pidajal ei ole õigust ei ühtegi töö wõi kulu eest tasumist nõuda. niisamma peab ka kandimees teggema. 
§ 11. Selle kandikoha pidaja paneb kautsioniks nende enese pääle wõetud sundmuste kõik oma warandust, ja nimelt oma jäädawad wara ehk inwentariumi, kelle sees kõige wähem 3 hobust ja 6 suurt weiste pead peawad olema, kõik heas korras üles peetud.
§ 12. Kui selle koha pidaja tulega hooletul kombel ümber peaks käima ja seeläbi mingisugust kahju sünnib, wastutab tema kõige oma waraga sündinud kahju eest.
      Peale selle lepib mõisawalitsus selle koha pidajaga weel järelseiswa peale:
      1) Koha pidaja wõtab oma peale.... sülda teed teha.
      2) Kohapidaja ei tohi wargile minna, wargid ega warastatud asju omas majas wastu wõtta.
      3) Kui elurehi halb ja wana on, ehitab mõisa teda oma kuluga jälle üles, aga mõisamaa kohapidajad peawad hulga kaupa, oma maa suuruse järele, ilma maksuta tarwilikku materjali juure wedama, niisama peawad nemad  ka elurehe parandamise materjali juure wedama.
§ 13. Koha pidaja peab iga kord, kui mõisa käsku annab, wälja tulema päiwi tegema.
§ 14.  Koha pidaja ei tohi omad koerad lahtiselt ümber hulkuda lasta. Iggaüht koera, keda kus tahes lahtiselt wäljaspool koha pidaja krunti leitakse, lastakse maha.
§ 15. Jätab koha pidaja mõned selle kontrahti sees nimetud punktid täitmata, siis kaotab tema oma koha ära, ilma et selle üle kohut käia ehk tasumist nõuda wõib.
§ 16. Songri rendikoha hoonete seisus  :
       1) Elurehi hea.
       2) Ait hea.
       3) 2 Karjalauta ja 2 hõlistiku keskmised.
       4) Saun.................................................
       5) Koda kambriga keskmised.
       6) 2 kaewu üks keskmine, teine wana..
       7) 2 heinaküni keskmised.
       8) Õuue aed korral. 
       9) Ait ja  hobustetall keskmised.
      10)..........................................................
§ 17. Wilja redelid weab kantnik mõisa lisa tööl ilma maksuta kuhu tarwis on. 
§ 18. Koha pidaja  peab tegema iga nädala (3) kolm mehe päewa nelja päew, redi ja laupäew iga päewa eest mis nädalas tegemata jääb maksab tema mõisale seitsekümmend kop. Kõige mõisa tööde juures peawad kantnikul olema omad tööriistad, nimelt: sirbid, wikatid, rehad, hangud, kirwed ja labidad.
§ 19. Rukki wäli peab mitte hiljem 20 augusti kuu päewa, hästi haritud ja hea seemnega mahakülwatud olema.
§ 20. Karel Tompson peab kartohwli rõugud õlgede ja mullaga mõisa walitsuse käsu ja tahtmise järele kinni matma..
§ 21. Kantnikul ei ole luba seda kohta ilma mõisa walitsuse lubata tõise kätte anda.
§ 22. Kantnik peab ........wakamaad uut maad, kandudest ja juurtest puhtaks tegema, mõisa walitsuse tahtmise järgi, siis wõib tema see aasta oma wilja all pruukida, aga kui kohapidaja seda mitte ei tee, siis maksab ta mõisale kahju tasumist, niipalju kui mõisa walitsus nõuab ilma kohtuta.
 Et see sinanekontraht rentnikule Karel Tomson täiesti ette loetud ning seletatud on saanud, ja et seesama rentnik kõige nende ülemal nimetatud tingimustega täiesti rahul on, seda tunnistab tema oma käekirjaga:
         Nõnda sündinud Mekkaste sel  1905 a.
          Kandikoha rentnik: Karel Tomson.
           Mõisawalitsuse nimel: G. Lipp.
            Tunnistaja:
Märkus:Kaadrid 230-232 kordub sama  Karel Tomsoni mõisa kandikoha kontraht.
