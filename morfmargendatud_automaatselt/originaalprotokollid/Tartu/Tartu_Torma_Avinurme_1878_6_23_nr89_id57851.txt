Protokoll
Awwinorme koggokonnakohtus sel. 23. Junil 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomes Jürri Tomik
Astus ette Enniksare külla perremees Josep Kiik, ja pallus koggokonnakohhut sedda Protokolli ülles kirjutada et temma omma Wenna Maddis Kiigele omma tallumaade küllest neljas jaggu maad; karjamaad ja heinamad on annud. ja pallub temma need maad temma tallumaade küllest wälja mõõta.
Josep Kiik XXX
Et Josep Kiik omma käega kolm risti omma nimme juure on tehnod saab Koggokonnakohto poolest tunnistud
Josep Laurisson XXX
Andres Kiik XXX
Jürri Tomik XXX
Protokoll Maamõtjale wälja antud.
