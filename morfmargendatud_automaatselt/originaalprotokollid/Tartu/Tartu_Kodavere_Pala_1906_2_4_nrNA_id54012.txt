               Wiimane seadlus.
Mina Jaan Tõnise poeg Welt Pala mõisa järel olewa ,, Enno N: 8" talu omanik teen täna oma waranduse ja pärijate kohta oma wiimase seadluse järgmiselt:
     ,, Enno N: 8" talu mis 38 taaldrit suur jätan oma kolme pojale Kaarlile, Gustawile ja Aleksandrile ühesuuruses osas pärast oma surma päranduseks. Liikuw warandus on neile juba kewadel 1905 aastal ära jagatud. Aleksandri osa jäeb Gustawiga ühte kuni nemad ise teda ära lahutawad. Paulist pojale antakse talust weel üks lehm, siis on tema jagu käes, teised lapsed on igaüks oma jau kätte saanud, nendel ei ole midagi enam nõuda."
        Pala wallamajas
        4. Webruaril 1906 aastal. Jaan Welt (allkiri)
          tunnistajad:  J. Kitsnik G.Olew Kustaw Tõrwa.
 Tõendan sellega, et Jaan Tõnise pg. Welti suusõnal üteldud Wiimane seadlus on siin tema ütluse järele üles kirjutatud, temale ette luetud ja temalt õigeks tunnistatud ja oma käega alla kirjutanud 
tunnistajate Johan Kitsniku, Gustaw  Olewi ja Gustaw Tõrwa juures  olemisel, kes ka ise oma käega allakirjutasiwad.
  Palal, 4 Web. 1906.
                 Wallawanem: P.Willemson. 
                  Kirjutaja Sepp.
 Pala wallawalitsuse pitsati jäljend.      
