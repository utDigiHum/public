18 Mail 1887 aastal.
Johan Einmann, keda kooli trahwi perast kohtu ette oli aetud ent neli korda wabandamata tulemata oli jäänud, tuli täna ette ja ütles et temal raha ei ole oma sõsara eest kooli trahwi 48 kop ära maksa. Ka welle Jaani eest on tal 1 rbl 38 kop. pääraha maksa.
Kohus Mõistis: Johan Einman peab sõsara Liisa eest kooli trahwi 48 kop Kambja õpetaja kätte, wenna Jaani pääraha 1 rbl 38 kop. wallawalitsuse kätte ja nelja korra kohtust wälja jäämise eest iga korra eest 30 kop kokku 120 kop. waeste kassa hääs 8 päewa sees, sest päewast arwatud ära maksma.
Otsus sai sääduse järgi kuulutud.
Päkohtumees: Johan Link XXX
Kohtumees: Jürri Kärn XXX
Kohtumees  Johan Opman XXX
Kohtumees: Johan Raswa XXX
