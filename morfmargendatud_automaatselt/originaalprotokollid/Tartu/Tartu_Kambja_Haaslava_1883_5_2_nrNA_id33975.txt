Haslawa  Kogukonna Kohus sel 2 Mail 1883
Man ollid:
Päkohtumees: Jaan Link
Kohtumees: Johan Klaosen
Johan Grossberg
Kaibas  Johani Tallu ostja Johan Klaosen, et selle Pallo heinamaa sehen se pops Jürri Tenn  ei olle temale minewa asta renti 2 Rubl 50 Cop ärra massnu ja pallus et Kohus seda massma sunis.
Jürri Tenn wastutas et tema selle Maa tükki est, mis tema käes om, selle Tallu rentimehe Johan Madissonil selle nõudmise päle se tassumine ärra tennu om, keda  Johan Madisson õiges tunistab sest temal ollnu õigus seda wõtta selle et tema keik heinama ütten maaga rentinu om.
Johan Klausen wastutas et tema seda popsi maad ütten ärra rentnu ei olle.
Mõistetu: et se Popsi rent Rentnik  Johan Madisoni heas peab jäma, selle et se maa tük ütten heinamaaga mõedetu om ja heinama keik Johan Madisoni rentitu om. 
Se mõistus sai kulutado ja ollid sellega rahul. 
Päkohtumees Jaan Link /allkiri/
Kohtumees: Johan Klaosen XXX
Johan Krosbärk /allkiri/
