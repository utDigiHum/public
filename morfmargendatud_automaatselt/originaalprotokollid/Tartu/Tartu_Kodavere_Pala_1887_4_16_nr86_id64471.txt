Tänasel pääwal anti Leen Sallole appellation Mihkel Wilka kaebuse asjus wastu teda 5 Web. s.a. N. 26 tarwiliste õpustega Talutahwa Seaduse Raamat § 772, 773 ja 774 põhjusel kätte.
                                               Peakohtumees J. Sarwik.
                                                Kohtumees       J. Stamm.
                                                Kohtumees      M. Piiri.
                                                Kirjutaja  G. Palm.
