Sel 14 Mail 1884.
Man ollid: Päkohtumees: Jaan Link
Kohtumees: Johan Grossberg
Kaibas Jürri Luha, et temal  Jaan Martini käest 100 Rubl ja selle raha 2 asta intress 12 Rubl sada om. 
Jaan Märtin lubas Kadernapäiwal se raha intressiga ärra massa.
Mõistetu: et  Jaan Märtin kuni 25 Nowembris 1884 se 112 Rubl Jürri Luhale ärra massma peab.
Päkohtumees J. Link /allkiri/
Kohtumees  Johan Klaosen  XXX
J. Krosbärk /allkiri/
Sel 17 Detsember 1884 massis Jaan Märtin 58 Rub Jürri Luhale ärra ja lepisiwa, et Jaan Martin se 60 Rubl kuni 20 Nowember 1885 keige selle intressiga Jürri Luhale ärra massab.
J. Luha /allkiri/
