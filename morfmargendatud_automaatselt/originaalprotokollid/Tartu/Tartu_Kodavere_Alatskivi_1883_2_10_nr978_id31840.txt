Poolele jäänud Jürri Bringfeldi kaebduses Fadei Saposchnikow peale, ollid kaebaja, kaebatu kohtu ees ja teggi kohhus peale tunnistuse kuulutuse
Otsuseks:
et jürri Bringfeldi kaebdus tühjaks moista tulleb, sest et ta seda kuidagi tõendada ei jaksa.
/: Otsus kuulutud sel 10. Weebruaril c Tallorahwa seaduse raamato §§ 772 ja 773 juhhatusel ja oppuse kirjad wäljaantud :/
Jürri Bringfeld kuulutas sel 12. Webruaril c. et temma otsusega rahhule ei olle, mis peale temmale kohhe appellatsioni kirri wälja antud sai.
J. Kokka [allkiri] kohtumees
Karel Tõrruwerre [allkiri] -"-
Wasili Muchin -"-
