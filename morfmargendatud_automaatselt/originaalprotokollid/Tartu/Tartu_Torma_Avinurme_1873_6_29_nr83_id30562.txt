Awwinorme koggokonna kohtus sel 29 Junil 1873.
koos ollid
pea kohtomees Maddis Tamm
teine Josep Reisenpuk
kohtomees Josep Kallaus
walla wanem Maddis Tamm
Tännasel päwal sai Anno Tebbele se rahha mis temma poeg Woldemar olli Maddis Pihlaka kätte hoida annud 100 Rubla, ja Maddis Pihlaka käest Tarto maa kohto sisse woetud, ja se rahha jälle sealt pea kohtomehhe Maddis Tamme kätte antud, Anno Tebbele wälja maksetud, ja olli Maddis Pihlak juures, ning sai selle sadda Rubla jure 5 Rubla intressi pantud et se rahha 2 Aastat Maddis Pihlaka käes on seisnud.
Anno Tebbe wõttis se rahha wasto ja teggi omma käega seia prottokolli alla kolm risti tunnistusseks et temma se rahha lätte sanud.
Anno Tebbe.
XXX
