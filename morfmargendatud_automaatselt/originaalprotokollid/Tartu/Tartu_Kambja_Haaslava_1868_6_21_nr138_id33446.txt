Se perremees Michel Tennok töstis enda essa Peter Tennoki pääl kaibust selle et temma teeninut pääle oppusse käimist enda essa 4 aastat ja essa ei ollewat temmale tolle eest keddagi palka masnut. Michel Tennok nowwap egga ütte teenitu aasta eest 50 rubla. Siis nowwap Michel Tennok enda ärra koolu emma warrandus 50 rubla essa Peter Tennoki käest kätte.
Peter Tennok wastutap tolle kaibusse pääl: temma kaswatanut ja koolitanut enda poja mehheks, kui poig teenistusse palka nowwap siis akkap temma kaswatamisse hinda poja eest naudma egga ütte päiwa eest 10 koppikat. Siis om temma pojale sajät piddanut nink ei tahtwat temma sellepärrast pojal se warrandus mis temma töise naise wõtmisse aigu iga üttel latsel lubbanut, wälja massa.
Moistus: Michel Tennok ei woi mitte enda essa kaest palka nauda siis essa om tedda kaswatanut ja mehheks teinut nink sajat ka enda kullu päle piddanut ja wiimatte weel maja pojale andnut. Agga emma warrandus piap Peter Tennok enda pojal Mihkel Tennokil wälja massma 50 rubla hõbbedat.
Pääkohtomees Jaan Purrik XXX
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirri wälja andtu.
