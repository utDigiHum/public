Wallawanem kaebas et 17 Sept. s.a. olla Hindrik Tamm olla tema käsu, oma poja tohtri poolt ülewaatamise tarwis hobust anda, wastu pannud, mikspärast see ülewaatamine mitte toimetud saada ei wõinud. Hindrik Tamm ettekutsutud ei wõinud ennast mitte kellegi asjaga selle kohta ennast wabandada.
Otsus: Hindrik Tamm maksab selle wastupanemise eest wallawanema käsu wastu kolm rubla trahwi waeste laeka 8 pääwa sees selle kohtu juures ära. See otsus sai Hindrik Tamme ja wallawanematele kuulutud.
                             Peakohumees   J. Sarwik.
                             Kohtumees        M. Piri.
                             abi Kohtumees J. Taguma.
                            Kirjutaja Carl Palm.(allkiri).
