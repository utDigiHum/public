№ 10.
Haaslawa wallakohus Haaslawa wallamajas Jurjewi maakonnas Liiwi kubermangus 29 Mail 1909 a.
Juures: kohtu esimehe as.  Jaan Rosenthal 
Kohtu liikmed:  Karl Ratassepp
Peeter Luha
ja kohtu kirjutaja J. Wäljaots.
Tuliwad ette:  Haaslawa walla talupoeg  Peeter Jaani p. Raswa ühelt poolt, kui rendile andja,  ja Arthur Karli p. von Wahl kui rendilewõtja tõiselt poolt ja palusiwad nende mõlemate  wahel kokku lepitud suusõnalist rendilepingut kohtu lepingute raamatusse sisse kirjutada ja ustawaks tunnistada alljärgnewal kujul: 
§ 1. Meie,  Peeter Raswa ja Arthur von Wahl oleme täie meele ja mõistuse juures alljärgnewatel rendilepingu tingimistel kokku leppinud, mis ka mõlemate poolte pärijate ja selle, Peeter Raswa päralt olewa Präksi N 9 talu (Kriimani mõisa järele Jurjewi maakonnas) talu ostjate niisama ka rentijate kohta terwe allpool kokku lepitud rendi aja pääle ilma muutmata kohustaw ja maksew on.
§ 2. Peeter Raswa annab oma Präksi N 9 talust (Kriimani mõisa järele Jurjewi kreisis) ühe wakamaa suuruse maatüki (10000 ruutküünart), mille laius kaheksa seitsmejalalist sülda ja kuus jalga on ja mis Kure talu piiri ääres on, ja kus praegu Kusta Wohla maja pääl on, Arthur von Wahlile kolmekümne aasta pääle rendile, mille eest wiimane iga aasta kümme (10) rubla renti maksma on kohustatud.
§ 3. Selles rendi lepingus tehtud tingimisi wõib ainult mõlemate poolte wabatahtlisel kokkuleppimisel muuta § 2ses nimetatud kolmekümne aasta jooksul.
§ 4. Parakrahw 2ses nimetatud iga aastase rendi kümme rubla piab Arthur von Wahl iga aasta kuni 15 Aprillini terwe aasta kohta ette ära maksma.
§ 5. Iga sugustest kohustustest, mis Präksi N 9 talu pääl on, on Arthur von Wahl parakrahw 2ses nimetatud aja jooksul kohustatud.
§ 6. Parakrahw 2ses nimetatud maatükki wõib von Wahl oma hääks arwamise järele tarwitada, wõib teda edasi rentida, sinna uusi majasid pääle ehitada lasta, seda aia ehk põllu näul tarwitada ehk hoopis harimata jätta, wõib sawi ja liiwa oma ehituste tarwis nimetatud maa tükist wõtta, kuid mulda ja liiwa ei tohi tõistele müüa.
§ 7. Kõik selle rendi lepingu tegemise ja kinnitamise kulud kannab Arthur von Wahl.
§ 8. Nimetatud maa on Arthur von Wahl oma tarwitada saanud 13 Mail s.a., millest ka rendiaja algus tuleb arwata.
Rendile andja:  Peeter Raswa  /allkiri/
Rendile Wõtja: Arthur von Wahl/allkiri/
1909 a. Maikuu 29 päewal Haaslawa wallakohus selle läbi tunnistab, et ülemal olew rendileping tänasel kohtu istumisel mõlemate lepingu tegijate, talupoja Peeter Jaani poeg Raswa ja Arthur Karli p. von Wahli poolt suusõnal kohtule awaldatud, pärast lepingu raamatusse sisse kirjutamist pooltele ette luetud ja nende mõlemate poolt oma käega alla kirjutatud ja et mõlemad kohtule isiklikult tuttawad ja lepingu tegemise wõimelised on.
Kohtu esimehe as.  J. Rosenthal /allkiri/
Kohtu liige: C. Rattassepp /allkiri/
P. Luha /allkiri/
Kirjutaja: J. Wäljaots /allkiri/
