Jakob Kuutsel kaebab Feodor Nemetz peale, temmal olla krundi teggemise rahha 7. rubla saada
ennine wõlg 2. puuda heinu			1. -			25. kp		wenne weddamise rahha			1.-			-		3. korda Pallal käinud			3.-			-		tango eest			1.-			-		heinu weddanud 1. paew			1.-			-		rehhe peksmise eest			3.-			25-		Kallaste käimise eest			-						
50-					
Selle peale olla temma 9. rubla kätte saanud, ja olla weel 9. rubla saada.
Feodor nemez kostab, et nõudminne olla õige, muudkui 2. korda käinud Pallal a' 60. kp., tango olnud 10. toopi, kelle eest 80. koppikad maksnud., ja pallub aega, et tullewa kohtupaewaks rehknungi toob.
/: jääb poolele :/
J. Pringweld [allkiri]
