Tämbatsel päiwal sai selle mõtsawalitsusse kaebdusse asjun selle Joachim Rosenthali wasto moisteto.
Moistus: Johan Jäger, Ann Saks ja Ann Jännesse tunnistusse perra om se mõtsawaht Alexander Labber lehma Rosenthali enda ristikhaina päält kinni wotnut nink ei woi mõtsawalitsus keddagi sellepärrast kahjotassomist nauda.
Jaan Käkki tunnistusse perra annawa agga Endrik Saks ja Juhku Eltzer wäggisse lehma Alexander Labberi käest ärra wotnuwat, nink piawat nemma sellepärrast kumbki 1 rubla walla waiste ladikun trahwi massma.
Kohtomees Jaan Wirro XXX
Kohtomees Jürri Luhha XXX
Kohtomees Jürri Kärn XXX
Moistus sai kuulutud nink suuremba kohtokäimisse õppetusse kirja wälja andtu.
