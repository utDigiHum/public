Kohtu ette tuliwad Lombi talu omanik Karl Wisser Jaanuse pg oma poja Johaniga, ja palusiwad eneste wahel olewat lepingut kinnitada, ühtlasi seda kui Karel Wisseri wiimast tahtmist tähele panna järgmiselt:
1. Karel Wisser jätab oma päralt olewa poole Lombi talu N:35 Jõe mõisa järel, mis tema oma wenna Gustawiga ühes koos ostnud, aga eneste wahel maa mõetja abiga pooleks jaganud ja kõik talu hooned lahku ehitanud, pärast oma surma. Kõige liikuwa warandusega ja kõigega mis talus on, poja Johanile  ainu üksi päranduseks, ja annab ka kohe sellesama kewadel 1901 aastal talu pidamise poja Juhani kätte, kõigega,mis talus on, järgmiste tingimistega:
a) Johann peab isa ja ema elu otsani oma juures üles, annab neile talus korteri ja tarwilise üles pidamise. Kui aga ühes koos hästi läbi ei saa, siis annab isa-ema jauks iga aasta 12 wakka rukkid, 10 wakka odre, 10 wakka kartulid, 2 leesikat linu, peab 1 lehma 1 sea ja 2 lammast talu toidu peal üles ja annab 1 wakamaa põldu, mille harimiseks hobuse lubab.  Isa-ema korter jäeb ikka talusse.
b) maksab noorema wenna Eduardile 6 aasta jooksul koha kätte saamisest arwates kakssada rubla, ja selle sama aja jooksul wanema wenna Gustawile 20 rbl, kellele isa Kohaliste Sead. Kogu 3 jan § 2015 p. 1 põhjusel rohkem ei luba.
2. Kui tarwis tuleb, siis teeb Johann oma õe Amalie Mariele pulmad ja annab temale 2 lehma ja 1 lammast kaasa, mis loomad juba temale ära näidatud.
e. Tütar Pauline on oma jau kätte saanud, ja Liisa on kasulapseks antud.
g) annab isale ja emale nimetatud 6 aasta jooksul 150 rubla, mis aga siis weel pikemale wõib jäeda kui talu pidamine heas korras on.
2. Johann Wisser wõtab tänuga wasta mis isa lubab ja tõutab kõik siin nimetatud tingimisi täita. 
           Juhan Wisser. Kaarel Wisser ei oska kirja, tema palwe peale kirjutas alla. Mart Kitsnik (allkiri)
Palla walla Kohus tõendab, et Kaarel Wisseri suusõnaline wiimase tahtmise awaldamine ja leping pojaga on kohtuprotokolli üles wõetud, protokoll neile mõlemile ette luetud ja nende mõlemate poolt alla kirjutatud. Kirja oskamata Kaarel Wisseri eest tema palwe peale Kohtule tuntud Mart  Kitsingult, mille järel see leping walla Kohtu aktiraamatusse N: 4 all wõib sisse kirjutada.
                   Esimees: J. Soiewa
            Kohtumõistjad M. Saar
                                       J. Kitsnik
                                       K. Haawakiwwi.
             Kirjutaja Sepp.
