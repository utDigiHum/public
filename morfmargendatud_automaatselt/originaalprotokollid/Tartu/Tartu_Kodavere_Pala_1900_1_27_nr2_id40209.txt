1900 aasta Januari k. 27 p. kinnitatud Jaagu Reinu N: 13 talu omanik Kaarel Jaani pg Wadi wiimane  tahtmine.
Mina jätan oma talu oma poegadele Jakob ja Paulile ja wäimehele Karl Reimannile nõnda, et Reinmanni osa neljas jagu kõigest talust jäeb ja poegadele 3/4 jagu terwest talust kumbagile.
Mõisa wõlad ja kredit kassa wõlad peawad kõik talupidajad oma maa suuruse järele maksma, niisama ka kõik maksud, aga üks obligation, mis terwe koha pääle kinnitatud, 600 rbla suur ühes tema intressidega jäeb Jakobi tasuda, mille wasta Jakob  kiwise tuule weski omale saab, sest see wõlg oli Jakobi tahtmisel tehtud. Poole selle peab Jakob wenna Paulile 200 rbla weski raha maksma, aga mitte enne kui 1912 aastal. Talu olewat juba krepostis kõige kolme pärija lubatud kinnitada, aga et seal kõik ühiselt peremehed  on siis jautab nüüd kohtu ees kui palju kellegi osaks saab.
                                                Allkirjad.
                                                Kohtu eesistuja J. Soiewa 
                                                Kirjutaja                  Sepp.         
