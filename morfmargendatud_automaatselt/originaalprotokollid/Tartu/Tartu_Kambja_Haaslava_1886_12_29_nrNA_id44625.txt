Sel 29 Detsembril 1886
Tulli ette Ado Ottas Surest Kambjast ja pallus neide kauba asja raha massmise perrast selletada sest tema om nüid keik selle Johani tallo ostmise raha, ni kui se kaub 28 Mail 1884 selle kogukonna kohtu protokollin seisab, Johan Klaosenile ärra massnu.
Johan Klaosen, kes praegu Ranitsen ellab, olli kutsumise päle seija kohtu ette tullnu ja andis ülles, et tema selle Johani tallo müimise raha selle kauba perra sest 28 Maist 1884 keik Ado Ottase käest kätte om sanu.
Otsus: seda kinnituses ülles panda.
Kohtumees: 
Jürri Kärn XXX
Johan Opman XXX
Johan Raswa XXX
Johan Klaosen XXX
Ato Ottas /allkiri/
Kirjutaja: J. Weber /allkiri/
