№ 31
Sel 16 Aprilil 1890.
Korzu tallo omanik  Johan Black pallus Ue säduse § 40 ja 41 põhjusel et tema piemarentnik Kusta Sieman warrandusest 43 rub 51 kop est Kohtu kelu alla saas pantu, mis Kogukonna Kohus 9 Aprilil s.a temale massa om mõistnu senis kuni kohus käib, selle et tema nõudmine selge om ja peljata om et Kusta Sieman omma warranduse ärra häwitab.
Johann Black /allkiri/
Selle päle sai kohtu poolt Kusta Siemani hobune 7 astane mustjas kõrb ruun ja raudtelgiga piema wetus wankri kinni pantu ja kostja Kusta Siemani hennese kätte hoijo alla antu seni kui se assi selletedu om.
Kusta Siman /allkiri/
Otsus: Seda protokolli ülles panda.
Päkohtumees Jaan Wirro XXX
Kohtumees: Jakob Saks  /allkiri/
" Peter Hansen XXX
