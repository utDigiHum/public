Protokolli peale 29 Oktobrist s.a. N: 192 saawad tunnistajad kes selle kohtu juures ülekuulatud saawad Karl Wahhi ja  Mats Kronkile awaltud ja ei olnud kellegil senna enam juure anda. 
 Otsus et Mats Kronkil kedagi tunnistust selle juures ei ole et tema lehm Karl Wahi süü läbi wigastud saanud jääb tema kaebus wastu Karl Waht tühja.
 See otsus sai Karl Wahi ja Mats Kronkile awaltud ei olnud Mats Kronk sellega rahul ja anti Talurahwa Sääduse Raamatu § 772, 773 ja 774 põhjusel temale tarwiliste õpustega kaebuse kiri kätte.
Mats Kronk palus weel mõtlemise aega.
                         Peakohtumees J. Sarwik.
                         Kohtumees         J. Stamm.
                         Kohtumees         M. Piiri .
                         Kirjutaja G. Palm.   
