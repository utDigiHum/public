Awwinorme Metskoggokonnakohtus sel 29 Decembril 1878.
koos ollid
peakohtomees Josep Laurisson
teine kohtomees Andres Kiik
kolmas kohtomees Jaan Mänd
Astus ette ärrasurnud Sälliksaare talluperremehhe lesk Leno Adamson ja andis Koggokonnakohtole ülles et temma kaddund mees on Testamenti omma warra ülle mahha jätnud, ning pallus kohhut selle asja ülle selletust tehha
Selle peale sai se Testament kohto poolest awwalikkult ette loetud kus sees punk 1. sees Jakob Adamson se Tallu omma wannema poja Mihkli kätte lubbab ning omma Naise ja lapsele Wöölmondrist Wallawannemad Juhhan Kopplid ja talluperremest Mihkel Paljakod.
Selle peale on Jakob Adamson ommad Wõlgnikkod ülles annud. kellede käest temmal saada.Sohwi Unti käest						100 Rubl			" kp					Andres Unt						1								Jakob ja Abram Raja						68								Maddis Adamson			4 küllimitto rukid.											Jürri Errapart						3						maksetud		Mihkel Laud			2 küllimitto jahhu									maksetud.		Josep Errapart			4 wakka kaero									maksetud		Mart Errapart			1 wakk rukkid									maksetud		"			1 wakk odre.									maksetud		Tomas Errapart			4 küllimito Odre									maksetud		"			4 - kaero								"			3 wakk kartulid								"			1 wakk rukkid								Jaan Lepp			4 wakka rukkid									maksetud.		"			2 kullimetto kaero.									maksetud		"			rahha wõlgu			4 Robl 			88 kop			maksetud		Jaan Haaw.			rahha			2						maksetud		Jaan Loddo			1 wakk rukkid									maksetud		Tomas Prowel.			3 wakka 2 küllimit rukkid									maksetud		"			rahha			3						maksetud		rahha wõlg Summa			181 Robl			88 kop.					
Arro oma jaggu kätte sanud. waeste laste protokoll 1887 N. 2.
Puhhast rahha on järrele jänod 447 Rob " kop.
Maja warrandus on rahha peale takseritud sanod. ja on.1 Täkk hobbone wäärt			60 Robl			" kp.		1 Ruun hobbone			30					1 Walge härg			50					1 punnane härg			20					1 pull			20					5 Lehma a 15 Rubl			75					1 Õhw			10					1 wassikas			5					3 Sigga			14					6 lammast			12					1 Raudwanker			35					1 Wanker wanna			8					1 wanna Wanker			4					4 rautamatta puurattast			4					1 Saan			8					2 Regge			3			50		kahhed hobbose riistad			20					1 Linnamassin			5					2 maa atra			2					1 Saksamaa adder			6					1 kangas jallad wäärt			4					puuristad			2			50		1 wanna kerst						50		1 Seina kell			5					1 kapp			10					1 kerst			2					1 Laud			1					5 wikkatid			2					4 Sirpi			1					tõenõud			3					linnaharri			1					Padda pannid			2			50		2 Sarja			1					2 Sitta ango						30		1 labbidas						20		Summa			427 Rubl			50 kp		
Selle järel on siis Jakob Adamsoni järrele jänud warrandus keik kokko
Wõllad mis sisse nõuda saab sellest Abram Raja wõlga 68 Rubl mahha arwatud sest et Abram Raja on Krono tenistusse lähnud ja teadmatta kas kätte saab. ja jäeb weel järrele 113 Rubl 88 kop.
puhhast rahha - 477 "
Maja Kraam rahha peale - 427.50
Summa 1080.38 kop
Sellest Summast 1018 Robl 38 koppikod lähheb Jakob Adamsoni testamenti punkt 3. järrele mahha ja wannema poja Mihkli ning wöölmöndrite lubbamisse järrel et Emma Leno üks Lehm saab mahha rehhendud.Wannema poja Mihkel Adamsonile.			100 Rob					teise poja Josep Adamsonile			100					Mattuse ja tohtri rahha			20					Süggisene Rent ja pearahha			16			70 kp		Seltsi Maja ehhitusse rahha			3			50		Emma Lenole Lehm			15					Summa.			255 R			20 kp		
Selle järrele jäeb 831 Robla 18 koppikod järrele mis pärrijatte peale ärra jautud saab ühtewiisi, ning on pärrijad lesk Leno Adamson, tüttar Marri 22 Aastad tüttar Anno, 19 Aastad poeg Mihkel 17 Aastad, tüttar Anna 13 Aastad, poeg Josep 12 Aastad poeg Tomas 5 Aastad ja tüttar Liisa 1/4 Aastad wanna, nenda kui Jakob Adamson on testamenti 3 punkti sees sowinod. ja saab igga ühhele 103 Rubl 89 1/4 koppikod pärrida,
Moistus
Et Jakob Adamson issi on omma Lesse ja waeste lastele Wöölmondritest sowinud ja pallunud Wallawnnemad Juhhan Kopplid ja talluperremeest Mihkel Paljakud ja need ka selle sowimissega rahhul on. siis jäeb se tallu nende Wöölmondrite watamisse all. Mihkel Adamsoni kätte wallitseda, ja peab seal se lesk omma lastega ellama ning lapsi kaswatama. ning saab koggokonnakohhus neid wõlgasid sisse nõudma. ja se puhhas rahha norema laste heast intressi peale pannema,
Josep Laurisson XXX
Andres Kiik XXX
Jaan Mänd XXX
