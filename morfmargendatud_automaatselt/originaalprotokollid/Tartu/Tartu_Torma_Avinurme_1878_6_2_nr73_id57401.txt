Awwinorme Metskoggokonnakohtus sel. 2 Junil 1878
koos ollid
teine kohtomees Andres Kiik
kolmas kohtomees Mihkel Sild
neljas kohtomees Jaan Pukk
Astus ette Kirbo tallu perremes Josep Ambos ning pallus koggokonna kohhut sedda Protokolli ülles kirjutada et temma omma Wenna Maddis Ambosele üks wijes jaggu maad ja heinamaad omma tallu küllest annab. ja jäeb se jaggu maad. mis temma Lelle Maddis Ambose käes ning maamõtmisse ajal temma nimme peale mõdetud ka temma kätte.
Josep Ambos. XXX
Et Josep Ambos omma käega kolm risti omma Nimme juure on tehnud, saab Koggokonna kohto poolest tunnistud
Andres Kiik XXX
Mihkel Sild XXX
Jaan Pukk XXX
