Sel sammal 22al Nowembri ku päewal 1880 aastal.
Ülemal nimetud päewal tõi kogukonna Kohtu ette, Laimjala wallast Iwan Mägi, oma 12me aastase poia, nimega Mihail, kellel aigus pea ja kõrwad on ära rikkunud: nõnda et temal lapsest saadik. iga sui kõrwad hirmsast mäda jookswad, ning se läbi on tema kõrwa kuulmine tünts; ja mõnikord oopis tema mõistus segane: ja et tema isi on juba wana, ja elatand, wõib olla et surma tund ligi on; sepärast palub tema kogukonna kohut oma tõbise poja Mihailli pärast, kes tööle kõlban ei ole, et teda ilma armuta ei peaks jäätama.
Kogukonna kohtu pea Wassili Tiits XXX
Kingli Kohtumees Iwan Rüütel XXX
kirjutaja Jakow Suster &lt;allkiri&gt;
