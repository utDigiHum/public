Kaehtla koggokonna kohhus olli koos siis tullid Rösa walla mehhed Kisti Kusto Ranno Kusto Koggokonna kohto ette, ja rääksid, et Kaehtla walla mees Mihail Kerr on nende Morrad ärra kiskunud, ja nemmad on temma Käest kallad kätte sanud mis ta nende Mõrrast olli ärra wõtnud siis kohto mehhed mõistsid sedda trahwi wiisteistkümmend witsa.
Kaehtla Möisa Koggokonna kohtos kirjotud Jurri ku 27mal päwal 1868 aasta
Tallitaja Jwand Kerst XXX
Kohtomees Mihail Pihhil XXX
