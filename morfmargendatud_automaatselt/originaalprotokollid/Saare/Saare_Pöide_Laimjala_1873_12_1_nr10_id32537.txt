Sel 1sel December ku päwal 1873 aastal panni Laimjala Mõisa Herra Koggokonna Kohtu ette, et temma selle Pajokurla Mõisa monaka ma jäus ollewad tallo kohta, Toma Matsi, mis tullewa 1sel Aprili ku päwast seks saab ärralahhutud, selle siit walla mehhele, Martin Tännaw, ühhe aasta peäle, se on 1sest Aprili ku päwast 1874 kunni 1875 aastani kuekümne (69) Rubla eest on prugitawaks annud, ning peab se maksu Summa keigehiljemalt 1seks Oktobri ku päwaks 1874 selletud ollema. Peäle sedda jääb Mõisa wallitsusse holeks 3/4 wakkamaad aiamaast, sega omma tahtmist möda tehha.
Keik awwalikkud maksud ning toimetamissed, mis selle perre kohha peäle kandwad on ..... ennese peäle wõtnud. Keik hein peab sama tehtud, ning pöllud wisipärrast harritud, sõnnik peäle pandud, ning külwatud, ning keik põhk seälsammas kohha peäle ärra södetud sama.
Se peäle tunnistas ...... ennast sedda üllemal seiswad kauba teggemist, täwelikkult ennese peäle wõtta ning täita.
Koggokonna Kohto peamees Wassili Marripu
Koggokonna Kohtomees Iwan Kaddarik
Koggokonna Eestseisja Iwan Pöldes
