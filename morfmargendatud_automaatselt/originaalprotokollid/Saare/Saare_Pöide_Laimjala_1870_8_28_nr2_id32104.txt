1870 aastal Leikusse ku 28 päwal olli Kaehtla koggokonna kohhus koos ja Wera Kir astus ette ja kaebas, et temma poeg Mihail Kir allati temmaga riidleb ja saggedaste ka peksa annab. Siis koggokonna kohhus möistis, et nemmad peawad üksteisest ärra lahkuma ja et Mihail Kir peab isse ennast toitma, agga temma emma Wera Kir jaäuks peab plats jäma, et ta ommale sealt toidust ja muud üllespiddamist saab.
Agga sel tännasel päwal astus Wera Kir jälle ueste koggokonna kohtu ette ja kaebas, et temma poeg sedda mõistmist mikski ei panne, waid mitto korda temma käest küllimelt awalt wilja ja ka muid asjo warrastanud ja wäggise wõtnud, ja senna perrese winud, kus temma poeg ellab ja kust ta emma ommad asjad olla mitto korda kätte sanud. Need asjad, mis ta omma emma järrelt wõtnud on: 1 wak 1 gr. Ruggid ja 1 wak odre ja 37 kop. rahha ja 2 naela linno. Ja et se pois tõeste nisuggune kelm on, sedda tunnistassid koggokonna kohtu ees need mehhed: Mart Pöld, Jwan Kok, Jakow Kir ja Mihail Pöld.
Sellepärrast sesinnane koggokonna kohhus pallub Aulist kihhelkonna kohhut allandlikkult selle emma ning poja wahhele ühhe kindla wahhe tehha, et nemmad teineteisega rahhule jääksid.
Kaehtla koggokonna kohtus, sel 29 Januar 1871 aastal.
Tallitaja Rodion Aun
Kohtomees Mihail Pihel
Kohtomees Jakow Kiwwi
