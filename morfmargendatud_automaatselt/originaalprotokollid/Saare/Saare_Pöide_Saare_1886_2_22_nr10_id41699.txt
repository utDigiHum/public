Koggokonna kohtu polest kange käsk selle Saare walla mehe Mihkel Madal. Et sinna pead s.a. sel 8mal Pastu ku päwal koggokonna kohtus ollema Pöitse Jõe Juhani kaibduse peäle. Et sina ole 3 rubla temaga wõlga. Ehk kui sinnul ei olle wõimalik tulla mitte siis katsu se raha (3 rup) seksnimetud päwaks Saare walla koggokonna kohtu sada. Kui mitte polle se raha ära satedud siis lubbawad kohtumehed isse teiseks laubäwaks sinnu järge tulla.
Peakohtumees Aleksei Põlluär
II kohtumees Wladimer Koppel
III kohtumees Nigolai Rehhi
