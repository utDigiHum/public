Sel 6mal Nowembri ku päwal 1870 aastal olli Kaehtla koggokonna kohhus koos ja Rodion Aun astus ette ja räkis, et temma omma wäimeest Jakow Runs, kes Koikuste wallast pärralt on, tahhan ommale abbiks wötta ja pallus, et temma Kaehtla koggokonna hinge kirja saaks ülles wõetud.
Koggokonna kohhus nähhes, et temmale sedda tarwis on, lubbas tedda wötta Kaehtla koggokonna liikmeks. Sedda on ka keigile koggokonna liikmettele teada antud siis, kui Made järrele wataja Herra Kaehtla möisas olli 9mal Augusti ku päwal 1870 aastal. Ja nenda sest 6mast Nowembri ku päwast on Jakow Runs Kaehtla koggokonna liikmette hulka ülles wöetud.
Tallitaja Rodion Aun XXX
Kohtomees Mihail Pihel XXX
Koehtomees Jakow Kiwwi XXX
