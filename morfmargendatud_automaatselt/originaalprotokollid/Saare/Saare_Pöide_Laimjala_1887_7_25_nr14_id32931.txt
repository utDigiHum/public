Selsammal ülemal nimetud päewal, andis Laimjala mõisa walitsus, kogukonna kohtu läbi siit walla liikmele Iwan Kapp, teada et se maa tükk mis praegu tema käes rendi peal on, saab 1888 aastal Juripaewast tema käest ära wõetud. Ja kui ta oma maja seal platsi peal tahab pidada maksab 10 rubla aastas renti.
Kogukonna kohtu pea Alekseij Silla &lt;allkiri&gt;
Kirjutaja Jakow Suster &lt;allkiri&gt;
