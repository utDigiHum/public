Koggokonna kohhus tulli koggo ja kutsus need wannad Renti wõllalised kokko, ja küssis nende käest kuidas teitega luggu on, nemmad ütlesid, meie ei olle mitte weel joudnud rahha tenida, kui meie same, siis meie tahhame maksta.
Agga Pea kohtomees ütles, et siin on teite seas sures wõllalised, mis meie nendega teggema, siis lasksid kohtomehhed Martin Pärti ja Aleksei Wiitsari ette tulla ja ütlesid neile teil on suur Rent wõlg, misga tahhate teie sedda weel maksta, meie anname hirmo, ja Martin Pärt Aleksei Wiitsar said mõllemad kolmkümmend napso keppi. SIis toti Mihail Toombu kohto ette et temma olli Anna Lesk käest üks terwe leib ärra warrastanud siis ütlesid kohtomehhed Mihail sinna pead se lewa taggasi maksma, ja sepärast et sa sedda ollid warrastanud saad sinna 15 napso keppi ja temma sai.
Koggokonna kohto Pea Georgii Ruustallo XXX
Kohtomees Timofei Kirst XXX
Kohtomees Aleksei Reis XXX
