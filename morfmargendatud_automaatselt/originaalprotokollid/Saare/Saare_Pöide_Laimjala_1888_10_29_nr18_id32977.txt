Kogukonna Kohtu ette astus Laimjala Wallast Aleksei Silla, ning kaebas et karjatsed olla temale heinamaades suure kahju teinud, arwata 30 kimpu lehe agu, olla naad, temal küünist ära poletand; ja üle heina maade 77 noort puud, mis läbi arwata 3 1/2 tolli kändu pealt jämed on, ära rajunud. Selle peale kutsuti ette kõik karjapoisid:
Matsi Anton Luukas
Lapi Mardi Mihail Kaarid
Mustla Jaagu Juri Kuusk
Mikku Mihkli Aleksander Mikku
Pisikse Mikku Wassili Tiits
Pärsa Aleksander Room
Lülle Mihail Mägi
Kohu Wassili Treiman
Wana Olli Aleksander Piipu
Olli Jaagu Aleksander Mägi
Wanado Aleksander Tenaw
Podipa Aleksander Wessik
Siis noomis Kohus neid kõwaste, niisuguse kolwatuma tegude pärast; aga et nõnde hulkas ka juba täieealisi mehi olnud, kes oleksid pidanud wähemid keelma sesuguste paha tegude eest, aga et nemad wähemattele weel nõu annud; sepärast möistis kohus 5 koppikud lehe oksa limpu pealt, ja 10 koppikud iga kolme tollise puu kandu pealt trahwi maksma; se on kokku 9 rubla 20 koppikud. Aga Silla töotas se kord weel selle trahwi leppida, aga edaspidised aastad, kes esimese kolme tollise pu ära rajub, ehk kokku pandud lehe oksa kimbu ära toob, se maksab kõik selle mõistetud trahwi, seega jäid kõik rahule, ja töotasid seda meeles pidada.
Kogukonna Kohtu pea Wassili Kadarik XXX
abimees Aleksander Mägi XXX
Kingli Kohtumees Jegor Mölder XXX
kirjutaja Jakow Suster &lt;allkiri&gt;
