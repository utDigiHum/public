Olli koggokonna kohus koos. Siis tulli Adu Ansber ning rägis koggokonna kohtu ees et ta tahhab selle Protokooli peale mis 1883 Mart 5mal Nr 2 all on nend wahele tehtud Willem Ansberiga. Et se Uus maja mis Willem Ansberi piddi oma wenna Juhan Ansber teggema nende kauba wahhel. piddi muidu tegema. Aga nüid on selle Juhani poeg Adu selle Willemile selle maja seinde eest maksand 80 rubla. ning kui nüid selle eesnimetud Protokooli järge nende kaup läheb peab siis se Willem selle rahha Juhani poja Adule maksma.
Pea kohtumees Aleksei Põlluär
II          "          Nigolai Rehhi
III         "          Wladimer Koppel
