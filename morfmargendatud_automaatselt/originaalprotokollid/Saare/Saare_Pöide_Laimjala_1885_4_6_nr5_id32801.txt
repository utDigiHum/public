Essimese Keiserliku Kihhelkonna Kohtu kässu peale s.a. No 225 olid Tallitaja Martin Tumma kutsumise järele peremehhed ja saadikud kokku tulnud, sellepärast et peab peremeiste hulgast ni samma palju wolimehi walitsema kui wallali rahwa hulgast, ning siis sai seaduse §9a põhjusel wolimeheks walitsettud  talu pere rentniku Matweij Mustel wene usku 43 ast wana, ja selle järele wäljakirjutus Prottokollist I-se Keiser Kihhelkonna Kohtu saadetud.
Kogukonna Walitsuse nimel Tallitaja Martin Tumma
Eestseisja Wassilij Mustel XXX
Eestseisja Wassilij Maripu XXX
