Kaehtla koggokonna kohhus tulli koggo siis Laimjalla wallast Jrina Kap kohto ette ja räkis et Kaehtla walla mees Jwan Kiip on tedda kolm napso lönud. siis läsksid kohto mehhed Jwan Kiipo ette tulla ja küssisid mikspärrast sa Jrinad löid Jwan aias essite kül taggasi et ta pole mite tedda lönud, agga wimaks ütles, et ta jomase peaga sedda on teinud, siis kohtomehhed pannid neid ärra leppima. Jirina küssis kolm kümmend koppikad höbbe, aga Jwan ei tahtnud sedda mitte maksta, kui kohtomehhed näggid et nemmad ei leppinud teine teisega siis sai Jwan kolmkümmend napso keppi.
Kaehtla Möisa Koggokonna kohtos kirjotud Mihkli ku 12mal päwal 1859 aasta
Pea kohtomees Radjon Aun XXX
Kohtomees Mihail Reis XXX
Kohtomees Aleksei Wiitsar XXX
