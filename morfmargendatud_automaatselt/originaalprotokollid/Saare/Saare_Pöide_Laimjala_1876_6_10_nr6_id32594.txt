Sel 10 Juni 1876 andis Laimjala Koggokonna Kohtu ees endine Kogg. Kohtomees Wassili Marripu tunnistust et sel aial kui Keiserlik Kihhelkonna Kohhus lesknaese Helena Kiiw järrel jänud warrandusse pärrast seädust teinud, siis issierranis mõistetud sanud, et se wanna ellu maja, nimmetud "Kiwe Aad" selle Rekrutiks läinud Wassili Pöldes pärrandusseks peab jäma, sellepärrast et ta selle ärrasurnud Aleksei Kiiw liggew suggulanne on.
Nisammoti tunnistas ka tännine Tallitaja ennast ükskord selle lessenaese Helena Kiiw suust kuulnud ollewad, et nimmetud Wassili Pöldes se parriaks piddi jäma.
Et agga nüüd selle Wassil Pöldes õdde (Mina) Melania Pöldes ennast ütleb ka pärria ollewa, ning mitte ükspäinis, temma, Wassili, ello seäl maia sees rahhutumaks teeb, waid peälegi maia uksed eest olle ärralöhkunud, siis otsib, se, nüüd Billeti peäle lastud Wassili Pöldes surematte Kohtude jures omma pärrimisse õigusse kinnitamist.
Laimjal sel 10 Juni 1876
Peakohtumees Iwan Mikku
Tunnistusmees Wassili Marripu
Tunnistusmees ​​​​​​​Aleksei Silla
