Laimjalla kogukonna wallitsusse ette astus ülemal nimetud päwal selle kogukonna liige Mart Pippu, ning tema abbielu naene Elisawetta, ja palusid siia Protokolli üleskirjutada, et nemad olla oma tahdlikkult, selle pärast et neil omas abbielus koguniste lapsi ei ole, Jaani kirriku wallast Jaen Lokneri poega, Pridud, 1870mal aastal omale ainikuks pojaks ja toitiaks wötnud, ning seda mööda se Prido Lokner nende lapseks juba ühheksa aastad olnud, ja nende toitiaks ning pärijaks peab jäema, ja ainsa poja õigust perima, kuida talu säetusseramat, aastast 1819 kinnitab, ning Nekrudi Reeglement Ülemast Walitseiast kinitud seädust mööda sesugustele §45 järele issiõigust annab.
Et need ülemal nimetud abielu paar rahwast selged töt seie kogukonna walitsusse ette tunnistanud on siie alla sest et tema siin kogukondas ei ella, sealt kogukonnast töelikud tunnistusmehhed omad nimed kirjutanud.
Jüri Eewrat XXX
Joan Lukner XXX
Jaen Ong XXX
Jaen Lukner XXX
