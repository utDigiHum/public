Sest et se Kapra külla wabbat lesknaene Madli Nurk ärrasurri ning üht 16ne aastast poega Wassili järrele jättis, issi agga ni waene olli, et mitte omma jõuga ei wõinud mahhamaetud sada, siis wõttis temma suggulane Willem Tiits sedda mattust omma holeks, ning toimetas sedda keik omma kulluga. Selle kullu tassumisseks olli Madli Nurk ennese surma wodi peäl töotanud et se laggund wabbatmaja selle Willem Tiits holeks piddi jäma, selle hinna eest, mis Koggokonna Kohhus sedda arwab wäärt ollewad.
Kui nuud Tallitaja ning Koggokonna eestseisjad sedda maia leidsid 15 Rubla wäärt ollewad, siis kui ta sinnasammase kohha peäle selle ma tükki tännise prukimisse õigustega, siis andis Koggokonna Kohhus sedda maia selle Willem Tiits jäuks selle seädusse al, et selle maja hinnast 4 Rubla 50 Kop. Willem Tiitsile mattukse kullu tassumisseks jääb ning need 10 Rbl. 50 Kp. peab temma nipea, kui wõimalik Koggokonna Maggasini laeka sisse toma, mis ka sedda waest last Jwan Nurk tarwidust möda aidetakse kaswatada.
Üleselle töotas Willem Tiits sedda waest last Iwan Nurk siis omma jure wötta, kui temma se wanna laggunud maja ni kaugele on harrinud, et seäl sees ellada sünnib, ning töotab tedda seäl kannata, ning tedda wannematte assemel juhhatada, ni kaua kui issiennese eest jouab hoolt kanda.
