Siis astusid ette Kingli wallast Jurri Trei ja Miina Kõrge, ning kaebasid et põllu waht minewa sügise nõnde lambad oraselt kinni ajanud, ja pool karnist wilja nõnde käest lamba pealt wõetud, aga nõnde lambad olnud kordas, ning Juhan Trei käes olnud kord, aga tema põlla omi lambud sel päewal teiste sekka ajanud, ega polla ka teiste lambud hoidnud, ja nemad epolla ka isi teadnud neid walitseda, ja peale selle olid lambad weel kordas olnud, ja nemad on maksnud kumpkid 1 külemit ja üks karnits wilja trahwi.
 Se peale kutsuti ette Juhan Trei kes selle kaebtuse peale wastust andis, et sel päewal wäga paha ilm oli olnud ja paljast kolme pere lambad olnudgi wäljal, ja tema nõnde kolme pere lambud üksi pole ka hoidnud.
Sis mõistis kohus selle trahwi pooleks poolt trahwi karjatsele, pool trahwi peremehele, sest et seal mõlemil ooletust on olnud.
Kogukonna Kohtu pea Alekseij Silla &lt;allkiri&gt;
Kingli kogukonna Kohtumees Iwan Rüütel XXX
kirjutaja Jakow Suster &lt;allkiri&gt;
