Laimjala mõisas kogukonna majas sel 8al oktoobril aastal 1880 olid Tallitaja Prido Mast kutsumisse peale kogukonna Wallitsusse liikmed kokku tulnud, ning sai selle Ülemast Walitsejast seatud Ukassi järele mis 25ast Julist aastal 1867 No 73,270 kästud. Et siin Goubermangus tule Assekurants talu rahwa priwat hoonete pärast ueste tuleb seada.
Sellepärast walitsetti wälja need mehhed kes seda taksationi pidid toimetama, nõnda kuida see Reeglement Art 12 on ette pannud.
Ning olid need sinatsed.
1 Tallitaja Prido Mast
2 Kohtumees Mihkel Wälli
3 Eestseisja Ado Redikson
4 Eestseisja Aleksander Mägi
Kui nüüd need sammad kõik selle kogukonna talu rendi maade pealt oliwad majad ja hooned üles arwanud, siis leiti et need hooned kelle sees siit kogukonna talumaade rentnikkud elawad, mitte nõnde omad, waid mõisa Herra päralt on ühtlase nende rehhe kui ka elatismajadeks ehhitud sest et siin ehhituse matterjalist suur puudus on, ei joua keegi üht isseäraliku elamise maja ehhitada, ni samma oli lugu nende wäiksematte hoonetega, mis nii wäiksed on et Asekurants summa alla ei ulata. Ülesele olid need wabanikude urtsikud mis ühtlase nende rehhe kui ka elatismajad on, ning seepärast ei wõinud siis ka neid nenda kuidas se Asekurantsi aluste majadeks arwatud, muud kui need kaks Magasini aita 1 Laimjala mõisa piiris kiwwist ja kiwist katusega ehitud, sai arwetud 300 rubla wäärt, ning kelle eest seaduse järele pool protsenti maksa tuleb, mis aasta kohta 1 rubl 50 kp kannab, ning 2ne selle samma kogukonna teine weike ait kapra küla maa peal kiwist ja rookattusega ehitud 50 rubla wäärt, kelle pealt aastas nii samma pool protsenti maksa tuleb, ning aastas 25 kop wälja teeb.
Peale selle arwati Ülemast Walitsejast seatud kässu järele need wabanikude hooned mis weel leiti kolmkumend rubla wäärt olewad ning kelle eest üks protsent 30 koppikud aastas maksa tuleb.
Need on nimelt need sinatsed1			Mihkel Walli			1 puumaja			30 rubla		2			Wassili Kööts			1 puumaja			30 rubla		3			Jakob Redikson			1 puumaja			30 rubla		4			Jegor Tumma			1 puumaja			30 rubla		5			Mihkel Sepp			1 puumaja			30 rubla		6			Jurri Ait			1 puumaja			30 rubla		7			Fädai Kallas			1 puumaja			30 rubla		8			Alekseij Särel			1 puumaja			30 rubla		9			Alekseij Padik			1 puumaja			30 rubla		10			Wassili Riuhk			1 puumaja			30 rubla		11			Aleksander Maripu			1 puumaja			30 rubla		12			Mihail Pipu			1 puumaja			30 rubla		13			Mart Kiip			1 puumaja			30 rubla		14			Kiril Silla			1 puumaja			30 rubla wäärt		15			P von Rennenkampff			1 puumaja			30 rubla wäärt		16			Mart Kapp			1 puumaja			30 rubla wäärt		17			Jakow Suster			1 puumaja					18			Antoni Kesküla			1 puumaja					19			Mart Redikson			1 puumaja					20			Iwan Pöldes			1 puumaja			30 rubla wäärt		
Tallitaja Prido Mast XXX
Kohtumees Mihkel Walli XXX
Eestseisja Ado Redikson XXX
Eestseisja Aleksander Mägi XXX
Kirjutaja J. Suster &lt;allkiri&gt;
