Olli koggokonna kohus koos. Siis tulli Liisu Rehhi ja ütles et taal ole sada 5 wak rugid ja 1½ waka odri oma wena Iwan Rehe käest. Ja se Iwan ütles et tema ole sest wiljast ära maksand ½ waka rugid ja waka odri. Ning se Liisu ütles et üks wak rugid on ära makstud ja ½ waka odri. 
Siis kohus mõistis neile nenda et se Iwan peab oma õele sest wiljast weel maksma 3½ waka rugid ja 1 wak odri ning igga asta 1 wak ära maksta. Nii kaua kui se willi selgeks saab. Aga kui se Liisu ära sureb jääb se willi Iwan Rehele.
Pea kohtumees Aleksei Põlluär
II              "          Wladimer Koppel
