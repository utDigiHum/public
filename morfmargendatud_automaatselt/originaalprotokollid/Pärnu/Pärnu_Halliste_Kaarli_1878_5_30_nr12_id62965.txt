Kusta Siemanni kaebduse asjas Jaan Reimanni wastu peksmise pärast olid kohtu käiad ja mõned tunnistajad sia ilmunud ja sai ette nõutud. Johann Allik, kes tunnistas: ta ei olle mitte näinud et Reimann Siemanni löönud, waid kül, et Jaan Reimann Kusta Siemanni mitto korda lükkanud üteldes "minne ära sinna tüllitegija Urrita tallu maa pealt".
Märt Kokk tunnistas, ta ei olle midagi tülli egga löömist näinud, waid kül, et Kusta Siemann roppud sõnnad ühe Urrite tallu küllalise Hans Leimani wastu ütelnud: "Kes kurrat sind sia toonud," mis peale siis Jaan Reimann Kusta Siemannil ütelnud "sinna oled ropp ja ei kõlba sia, minne ära Urite tallo maa pealt;" peale selle olla Reimann Siemanni ka lükkanud, aga mitte löönud.
Et teised tunnistajad wälja jäänud sai arwatud: assi poolele jätta ja kohtu käiad ja tunnistajad teiseks kohtu päewaks ette tallitada.
Jaak Raegson [allkiri]
Jaan Mõrd XXX
Johann Lensin [allkiri]
