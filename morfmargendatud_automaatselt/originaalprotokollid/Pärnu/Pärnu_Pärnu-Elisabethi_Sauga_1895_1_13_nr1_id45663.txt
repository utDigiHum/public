Ette tuli Sauga walla Männiku talu No 27 omanik Hendrik Juhani p. Jäärats ja palus ülesse kirjutada et tema nimetatud talu koha pidamist ühes seal juures olewa liikumata ja liikuwa warandusega oma poja Hendrik Jaaratsi kätte annab ning saab Hendrik Jäärats oma naese Leenuga selle eest oma poja Hendriku käest seal majas elu aegse prii ülespidamise. 
Poeg Hendrik Jäärats on sellega rahul.
Tähendab Hendrik Jäärats XXX
Hendrek Jäärats
Siin ees pool nimetatud inimesed on Sauga walla kohtule tuntud.
Eesistnik: H. Ainisson.
Kohtunik: M. Hanson.
" M. Rabbi
" P. Kuter
Kirjutaja: J. Tomson
