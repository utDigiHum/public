Tomaadu Jaan Palu tulli kohtu ette ja räkis,et Tohwri Madli Eppen on tema paadi wötnud ja annud oma pojale Toomale Eppenile ja körtsi Adule Jansonile Riiga minna kiwi laewa juure ja tema nöuab selle paadi ette nüüd 7 rubla.Sai Madli Eppen kohtu ette nöutud ja tema räkis,et tema pole mitte paati wötnud,waid poisid on ise selle paadi wötnud ja selleks Jani Palu ennese käest luba sanud ja nüüd on naad ühes laewa ja paadiga ära kadunud-ära uppunud.Kohus möistis:et nenda kui need poisid:Tomas Eppen ja Ado Janson ühes laewaga ja paadiga ära kadusid-ära uppusid,-siis se Jani Palu nöudmine paadi üle tühjaks arwata.
Kihno Walla kogukonna Kohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Juri Wahkel XXX
Jaan Pull XXX
