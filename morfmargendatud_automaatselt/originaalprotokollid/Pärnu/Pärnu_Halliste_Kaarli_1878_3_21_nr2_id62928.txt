Marri Põdder tulli ette ja kaebas, ta endine perremees Märt Leppik ei tahta kaebaja palga wälja maksa ja nimmelt			1873 aasta eest			4 rubla		üks jakk			1873 aasta eest			3 rubla		üks paar sapid			1874 aasta eest			3 rubla		palka rahha			1874 aasta eest			4 rubla					Summa			14 rubla		
Kaebaja palluda et kaebatu Saaks sunnitud seddamaid eesnimmetatud palk wälja maksma tunnistajaks andis kaebaja ülles Anno Neljantik.
Märt Leppik wastas kaebtuse peale ta olla keik palk kaebajal välja maksnud. kui ta ärra läinud ja ei olla kaebajal ennam middagist wõlgo.
Anno Neljantik antis tunnistust, ta olla jures olnud kui Märt Leppik kaebatud lubbanud omma kulluga leeri panda ja keik kullud maksa mis maksa on ehk tulleb ja wimsel aastal kui Marri Põdder ärra läinud olla Leppik üttelnud et nüüd wõttan 4 Rubla leeritamise eest kinni ja puid ei maksa ma ka mitte ennam Halliste Kostrile.
Kui kohtu käiadel ennam middagid ette tua ei olnud ühhendasid kohtu liigmed endid selle Otsusele: Tunnistaja põhjusel peab Märt Leppik Marri Põdral 4 rubla rahha wälja maksma ja pool sülda puid Halliste Köstri jure arra wiima. 8 päeva sees.
otsus sai seddamaid kohtu käiadel kulutud ja Märt Leppik pallus Appelatsionitähte eddasi kaebamisse tarwis ja Marri Neljantik olli rahul otsusega.
arvatud keik kuidas sündinud kirja panna
Pea kohtumees: Jaak Raegson [allkiri]
kõrwalistnik: Jaan Mõrd XXX
kohtumees: Petter Leppik [allkiri]
