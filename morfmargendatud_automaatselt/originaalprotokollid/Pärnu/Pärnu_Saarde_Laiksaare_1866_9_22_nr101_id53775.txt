Laiksaare Koggokonna Kohhus sel 22 September 1866
Kohtus ollid: Kohtomees Hans Tins
-,,- Tennis Baers
Abbi Hans Kamdron
Tulli ette Loigo mees Ado Andersohn ja pallus las Kohhus annawa selle ülle üks Moistus, kas sii lahkiüttlemenne sel 25. August ollewa seadusejärrele ja kas temma peawa sedda wastu wattama jau § 116 järgi peawa üks tallo - nenda sama sii pool tallo maad mis temma pruhgiwa - Jakobi peiwas, 25. Juli, lahkiüttletut sama. Ilma sedda neitawa Kreiskommissar Herra kirri d.d. 15. April c. sub Nr 1183 kui temma wend Jacob Andersohn ei tahtma wänale maad andma, ja peawat neid maad mis Jacob essi ei pruhgib, temma Issä ja wenna poles pruhgitut sama; aga Jacob ollewa Johann Possil 8 tükki pellu maad ja 3 tükki einamaad wella anno.
Jacob Andersohn wastas, Adol eijollewa tallo kähs, kui peawa Jacobi peiwas lahki üttlema, neid maad woiwa igga aijas lahki üttleda. Johann Poss pruhgiwa küll maad temma juhres, aga ei maksawa rahha rent selle ihst, tettawa peiwast.
Kohhus moistis: kui § 116 järgi seadusse rahmattist sii lahkiüttlemenne iljaks jähno, ja tullewa Kewwahdi Jacob temma wenna ei woi erra aijat, ilma wahdamatta, kas Kreiskommissar Herra kirri järgi Nr 1183 Jacob temma Wenna Ado middagi woib erra aijada, woi ei.
Sii moistus sai ette lohetut ja õppuse kirri Jacobil kätte antut.
Kahekse peiwa järgi sel 29. September olli Jacob Andersohn walla kirjutaja juhres tullo üttlemas, kui eijollewa rahhu.
