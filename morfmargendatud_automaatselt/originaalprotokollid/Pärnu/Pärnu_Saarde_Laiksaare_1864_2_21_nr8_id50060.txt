Laiksaare Koggokonna Kohtus sel 21mal Febr. 1864
Kohtus ollid Pea Kohtomees Juhann Petersohn
Kohtomees Jürri Orraw
Jaan Kurm
Tulli ette Possa Tefre Poeg Jürri ja pallus Kohut sedda Protokolli ülles wõtta et see Teffre Magga koggone selle õemehhe nimme peale saab ümmerkirgotud, et temma tedda ilmas kii ommale ei gaua piddada ja teiseks koggoni ei tahha.
Moisteti. Sedda Tarto Reisrihtre Kohtusse Kirjutada, ja küskida kas Reisrihtre Kohus annab sedda lubba, et meie koggokonna Kohtul küll wastopannemest ei olle.
