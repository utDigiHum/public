Astus ette Perno linnast soldati lesk Madli Jurik oma käemehe Jüri Juurikuga, jakaebab et Wõlla Walla Walitsus, tema ja tema kahe lapsele mitte seda lubatud jagu ülespidamist wilja ja rahaga kätte ei anna, nõuab et Walla kohus selle üle kuulaks, et temal ta lastega, kõik Walla poolest üles pidamine kätte saaks antud.-
Kutsuti ette Wõlla Walla Talitaja Hans Pirson ja annab wastuseks, et see Madli Jurikas üsna tühja kaebdust Walla Walitsuse wasto tõstab, sest et tema minewa aasta 1881 aastast kuni 1882 aastani päna pääwani Madli Jurikale kõik täieste wälja maksnud ja kätte saatnud on. -
1881sel aastal Tõnise laada aeg Padu Jaaniga saatnud 2 tset. Ruk.
1881sel aastal jalle Raba Daviga saatnud 2 tsetw Ruk.
1881sel aastal 8/XI kirjutaja kaest 1. Tsetwerti rukide eest 10 Rubla
1881sel aastal on isi selged raha maksnud 11. Jun. 6 "
1882. aastal 12/III    Rukkide eest kirjutaja käest saanud 10 "
1882 aastal Märtsikuus jälle kirjutaja läbi selged raha 6 " saand
1882 aastal täna 8mal Aprillil jälle 1 Tsetwert Ruk 1. Tsetwert Rukkid saatnud. -
Kutsuti ette Madli Juurik oma käemehe Jüri Jurikuga, ja päriti kas ta on nii palju wilja raha Tallitaja H. Pirsoni käest saanud. - Ja Madli Juurik tunnistab ise jutt kohtu ees, et täieste 1881 aastast senni kui tänapaawani on Wallast kätte saanud 4 Tsetwerti Rukkid kahe Tsetwerti eest kirjutaja J. Simonsoni käest 20 Rubla raha, ja minewa aasta 6 Rubla selged raha ja tanawa aasta ka 6 Rubla raha, aga ütleb, et tal ikka weel endiste aastade eest saada on olnud. -
Selle peale annab Tallitaja wastuseks et Madli Jurik oma käemehe Jüri Jurikuga seda ei wõi tõeks teha, missigiste aastade eest, enne tema ameti aega, temal weel Wallast saada jäänud, siis ei wõi tema sest ühtigi teada. - sellepärast. -
Sai mõistetud: et selle Madli Jurikal ikka omalt poolt kaebdust ja kulu Wõlla Walla wasto on üksi enda poolt siis peab selle Madli Juurika ja tema laste Wöörmünder Jüri Jurikas kes ka Perno linnas elab. - kõik siit Wõlla Walla Walitsuse poolt saadetud wilja ja raha. - selle Madli Jurikal wasto wõtma, ja selle wasto wõtmise üle iga kord ühe kwitungi tagasi andma, mis Tallitaja Hans Pirson iga kord oma kätte saab ja tema Wöörmünder Jüri Jurikas on kõikis asjus tema seletaja ja nõudja ja mitte lesk M. Jurik, et selle naise läbi enam tüli ega pahandust ei tule.
Peakohtumees: Jaan Reimets XXX
Kohtumees Hans Pirson XXX
Kohtumees Jüri Moppel XXX
Kirjutaja: J. Simonson
