152. Tüdruk Tiiu Kants kaebab, et temal Peeter Jääratsiga on üks laps olnud, ja nõuab, et Peeter Jäärats temale peab nüüd ülespidamist andma.
Peeter Jäärats ütles seda tõsi olewat, et tema Tiiu Kantsiga on ühte elanud.
Kohus mõistis: Peeter Jäärats peab Tiiu Kantsile 12 aastat järgi mööda, s. on 1878 aastast kuni 1890 aastani lapse ülespidamiseks iga aasta 10 Rbl maksma ja ka edespidi, kui laps on kooli aruliseks saanud, koolitamise eest muretsema ja abi andma. Pääle seda, et nad häbemata tööd on teinud saab Peeter Jäärats 20 hoopi witsu ehk maksab 4 Rbl raha ja Tiiu Kants laheb 24 tunniks wangi. Peeter Jäärats maksis 4 Rbl. ära. Tiiu Kantsile maksis P. Jäärats 5 rubla.
Nr 194 siit              
kohtu kinnitatud 30 Okt 1878
Jakub Kants oli 8" Septembril s.a. käemeheks.
Peter Jäärats maksis Tiiu Kantsi eaks 25 rubla. 5 Mail 1880
Sellest 20 rubla kätte saanud 8 Mail 1880
Sellest 5 " " " 16 Januaril 1881
Peter Järats sisse maksnud 10 rubl 27 Nov 1881 - 10 Dec. 81 Tio Kantsi    
Peeter Jäärats maksis wiimast 40 rubla sai siin Tiiu Kantsi kätte 16 Märtsil 1890.
