Astus ette Mari Aitaja käemehe Jüri Moppeliga ja kaebab et Karl Tomingas on temaga lihalikult mineva aasta ümber käinud, ja temal sest üks poeg laps on sündinud, nõuab et kohus selle lapsele üles pidamist maaksa mõistaks 100 Rubla. -
Kutsuti ette Karl Tomingas, ja ei salga et tema kül selle Mari Aitajaga lihalikkult ümber käinud, waid teised poisid on ka olnud Tamme Tani Erm, ja Muho Hans Kodasma ja Jaan Kuusik-
Kutsuti ette Jaan Kuusik ja salgab, et tema pole selle Mari Aitajaga lihalikkult ümber käinud.
Sai mõistetud: Tamme Tani Erm ja Muho Hans Kodasma teisel kohtu pääwal ette tellida.
