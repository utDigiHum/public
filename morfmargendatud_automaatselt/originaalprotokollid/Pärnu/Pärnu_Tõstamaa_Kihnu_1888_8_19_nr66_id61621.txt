Kohtu ette tulli Kurase Mihkel Sutt ja kaibas,et Kära Enn Türk tema aitas käinud ja halli rawa hulk ära warastanud,ja et sedda tunnistab Tanila Mihkel Leas.Sai Enn Türk kohtu ette nöutud ja tema räkis:et tema pole raswa mitte warastanud,muido aitas käinud,agga wiimaks ütles,et ta Kurasel pole olnudki.Tunnistaja Mihkel Leas sai kohtu ette nõutud ja tema räkis:et Enn Türk tulnud enne päwa töusu Kurase Mihkel Sutti tua nurga tagast wälja ja olnud halli rasw käes ja peale sedda sai ka kirjutaja pada awalikkuks,mis Enn Türk ära warastas.Kohus möistis:et Kära Enn Türk kümnikku ammetist lahti on ja nende warguste tembude ette peab saama-15 hoopi witsu et ta selle warguse ammeti peab ära kautama ja edespidi mitte enam sedda tegema ja Mihkel Suttile peab 60 kop.maksma kahju tasumiseks;ja se sai täidetud.
Kihno Walla kogukonna kohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Jurri Wahkel XXX
Jaan Pull XXX
