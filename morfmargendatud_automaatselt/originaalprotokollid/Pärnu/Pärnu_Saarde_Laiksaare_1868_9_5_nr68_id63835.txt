Laiksaares wallakohtus sel 5 Sept. 1868
Jures ollid: peak. M. Rast, wannemk. H. Tins ja H. Kamdron
Metsapäliku härra kaebtusse asjus, antud 20 Aug c.e. Nr 278 et Sakki metsas olla tulli kronometsa läinud ja metsa ärra põlletanud, selle iggatsemissega, et se assi peab järrele kulatud, mis läbbi se tulli akkanud olla. - Selle peale sai metsa waht Jaan Kask ette kutsutud ja temmalt küssitud, kedda arwab metsa põllema pannud wastas: temma metsas olla tulli 4 kohha peal pollenud, agga nisugguste kohhade peal, kus karjatsed ei pudu, - selle pärrast temma ei teada kellegi peale arwata ja ka keddagi selle pärrast üllesse anda, kes seal peaks süiallune ollema.
Arwati kõige häeledega: et Sakki Rewiers mitte põllemisse pärrast ei tea keddagi süialluseks arwata.
