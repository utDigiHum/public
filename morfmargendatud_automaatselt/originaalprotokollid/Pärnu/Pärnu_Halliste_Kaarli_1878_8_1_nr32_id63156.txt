Kusta Siemanni kaebtuse asjas Jaan Reimanni wasto peksmise pärrast ollid kohto käiad ja kaebato wenna poeg Märt Reimann sia ilmunud, agga tunnistajad Andres Loom ja Peter Sapas walja jänud.
Kusta Siemann andis ülles ta olla Märt Reimannist Urrite talluse Ue-Karristest ühhes kutstutud arwata et nimmelt selle pärrast ehk tarwis et kaebato seal piddi tappetud saama.
Sepeale wastas Märt Reimann se olla tõssi et ta kaebajad ühhes kutsunud, agga mitte selle pärrast et kaebajal se läbbi keige  wähhemad kahjo olleks ta tehja tahtnud lasta.
arwatud: et tunnistajad wälja jänud siis assi teiseks kohtu paewaks jätta ja kohtu käiad kui ka waljä jänud tunnistajad ette telli ja assi otsusele sada.
arwatud: keik kuidas sündinud kirja panda.
Peakohtumees: Jaak Raegson XXX
kohtumees  Peter Leppick XXX
     - " -          Peter Kogger XXX
