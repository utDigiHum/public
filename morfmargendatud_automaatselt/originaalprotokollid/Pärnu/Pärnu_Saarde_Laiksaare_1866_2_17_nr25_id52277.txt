Laiksaare Koggokonna Kohhus sel 17. Februar 1866
Kohtus ollid: Pea Kohtomehhe Abbi Johann Tals
Kohtomees Hans Tins
-,,- Tennis Baers
Sai ette kutzutut Tõnnis Lied (vide Prot. Nr 15) ja üttles: "Temma sest assjaks keddagi ei tähdwa, ja kus temma middagi rummalast räginu, sis las temma selepeal ka wastutab.
Kohhus moistis: Tullewa kord Hans Lied ette kutzuda.
