Ann Rast ütles küssimisse peale: kui Kadri Pridik neil käinud, kaebanud et Mamma olla enne 2 korda ja Willem Unnifer üks kord peksnud, - temmal saddanud weel lüpsik omma lauda ukse eest kaest mahha kui Mamma peksnud, - ka näidanud omma peksu hopid, pihhade peal olnud werrised ripsud, puusa ots olnud sinnine ja mokkad paistetanud, - agga persed ei olla hielanud naitata, - ja issi ikkenud kui üks wäike laps.
Arwati kõige häledega:
1. et Willem Unnifer maksab omma naesega Kadri Pridikule peksu eest 5 Rub. wallo rahha ja Kadri Pridik maksab sell eeest 1 Rub. trahwi walla ladiku, et ülle keelo lainud lehmi lüpsma.
Selle otsusega ei olnud kegi rahhul.
Mõistetud et appellatsioni kirjad wälja anda.
