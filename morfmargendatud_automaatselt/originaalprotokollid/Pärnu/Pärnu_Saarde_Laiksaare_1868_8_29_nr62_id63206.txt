Laiksaares wallakohtus sel 29. August 1868
Jures ollid: peak, Mats Rast, wannemk. Hans Tins ja noremkohtum. Hans Kamdron.
Sai Johann Tals ette kutsutud ja temmalt küssitud, et Appellationi kirja olli wõtnud ja ei olla kaugelmale kohtu läinud, mikspärrast ei olla siis hobbust ärra toonud, wastas et temma olla hobbose ärramüinud ja tahhab teada saada, mis hinna peale se hobbone olla üllese wõtmisse aegas takseritud, et wõiks rahhuga ärramaksa.
Selle kordne walla kirjotaja ei olla sedda mitte protokolli pannud.
Arwati kõige häledega: et Johann Talts peab warsa ja wanna hobbuse eest 25 Rubla tänna 8 päwa sees wälja maksma.
Sellega olli Johann Tals rahhul agga pallus pikkemad termini, - sai 29 Sp: antud.
