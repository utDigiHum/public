Ain Saarts (nekrut) kaebas et Sanga rentnik Thimm teda sulaseks palganud ja palka lubanud:raha			65 rubl			- kop.		palga põllu eest raha			5 "			- "		ja kui sui odrad haste korda lähewad siis weel			5 "					Summa			75 Rbl			- kop		
14 Mail hakkanud teenima kunni 17 Oktoobrini siis lasknud härra Thim, et ühe pääwa ära wiitnud, lahti ega ei ole see eet rohkem palka annud kui 27 rubla. ja nõuab nüüd täit aastast palka.
Mihkel Reimann ütles et kõik palk muidu õige on aga et odrade hinna tõusmise läbi weel 5 rubla pidanud juure saama, ei olla mitte tõsi. Ka olla härra Thimm teda seepärast lahti lasknud, et ilma luba küsimata kottu ära läinud, ega kodos mitte hobuste talitajad ei ole olnud.
Mõistis kohus: Odrade hinna tõusmise juures see pärimine saab tühjaks arwatud. aga et kuus 5 rubl 83 kop peaks saama see on 5 kuu pealt 29 rubl 16 kop ja 27 rubla kõigist saanud on weel saada 2 r 16 kop.
poolest pastlanahast mis on lasnud maksab Ain Saarts poole tagasi 1 R. 25 kop.
Jääb weel saada 92 kop.
(Mihkel Reimann maksis seda ära.)
