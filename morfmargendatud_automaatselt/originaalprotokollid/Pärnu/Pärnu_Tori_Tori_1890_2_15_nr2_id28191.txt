On Ado Tamme wõla uskujad Mihkel Andrekson,Endrek Wolmerson,Mihkel Tilk,Jaak Tasane,Karel Künnapas,Jüri Lossmann,Mihkel Kiisk,Mihkel Juurikas ja Jüri Gerberson astusid ette ja andsid üles et Ado Tamme weski rendi aeg ümber olla ja tegid endi kui ka Ado Tamme wahel järgmist kontrakti:
Ado Tamm lubab isi oma tuule weski wõla uskujate käest kaheksateistkümne aasta peale ära renti ja töötab selle eest iga aasta ükssada rubl./100 rubl./ renti maksta.Rendi aeg hakkab 1 Märtsist 1890 peale ja peab pool renti siis kohe ette ära maksma,niisama ka teine pool renti 1 Septembril iga aasta ette.
Selle kontrakti täitmise kindluseks paneb Ado Tamm niihästi oma liikuwa kui liikumata waranduse wõla uskujate kätte pandiks sisse.
Asekurandi raha selle weski eest maksab Ado Tamm,mis mitte wõla uskujatesse ei puudu.
Seda kontrakti kinnitasid oma alkirjadega:
Rendi peale wõtja:Ado Tamm/allkiri/
Rendi peale andjad:M.Andrekson/allkiri/  Mihkel Juurikas/allkiri/  Hindrek Wolmerson/allkiri/  Karel Künnapas/allkiri/ Mihkel Kiisk XXX  Jaak Tasane XXX  Mihkel Tilk /allkiri/  Jürri Losman/allkiri/  Jüri Järberson/allkiri/
Peakohtumees:Tõnis Tehwer/allkiri/
Kohtumehed.:Peet Tammann/allkiri/  Hans Kask/allkiri/  Jaak Tommongas/allkiri/  Mart Ollino/allkiri/
Kirjutaja ARadin /allkiri/
