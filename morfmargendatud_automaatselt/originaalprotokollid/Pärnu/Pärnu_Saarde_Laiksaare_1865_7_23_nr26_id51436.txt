Laiksaare Koggokonna Kohhus sel 23. Julil 1865
Kohtus ollid: Peakohtomees Karl Tals
Kohtomees Hans Tins
-,,- Tõnnis Bärs
Auliko metsa Herra Captain von Peters kässo peäle piddi saama kohto ees järrele kulutut selle metsa põlletamisse pärrast mis Massiarro ja Kiwwiarro metsa sees 11mal Julil sündinud, ja selle järrele sai siis ette kutsutut Massiarro metsawaht Hans Liet kes wälja räkis.
Ehk ta on näinud kui enne lõunat sel 11 Julil seält suits üllestousnud, silma pilk sinna leinud ja leitud ehk ta 10m ja 17m jaggus Laiksaare metsa sees põlles, kus ta nende liggidamalt tallopoegat ni kui metsawahhtegat katsusit kustutama, enne pääwa loja minnik olli ka tulli kustutut. Suurem jaggo puid mis põlletamisse kohha peäle olli on ni suur 10 Dessätinit 817 ruutsülda polle kahjo mitte sanud, waid 20 ruudsüülda aggo on ärrapõllenud.
Kül temma on keige murre mis wähhe woib olla katsunut täda saada kudas ja kelle läbbi se mets on põllema leinud, agga polle ühtegi täda sanud, nüüd ta woib muid keddagi, kui arwama karja laste ooletusse läbbi on ta sündind kes omma karja ilma lubba metsa on ajand, waid teekäjatelle ooletusse läbbi.
Sai ette kutsutud Tauste perremees Ado Tals ja wälja küssitut, kelle peäle ta wastas: ehk metsawahhe kässo peäle ta warsti põlletamisse metsa juure einud kustutta, agga middagi ei tea woib olla ehk Karl Andersohn Otti poeg rohkem teab ta on enne jubba seält olnud.
Karl Andersohn sai ette kutsutud ja wälja küssitut: kelle peäle ta wastas: ehk ta ühhtegi ei tea kuddas mets on põllema leinud, kui ta juure tulnud on jubba suur tulli ollnud.
Said ette kutsutut Ado Mälksohni karja mees Jurry Pridrik ja karja naine Kert Joost kes mõllemad wälja räkisit: ehk need karjatsemisse juure polle tulli ühhtegi näinud, kui need karj jubba koddo ajaset ja tallu liggi ollid, siis need on suits tõusma näinud, agga kuddas tulli on wälja tulnud need ei teadwad mitte.
Tõnnis Liet sai wälja küssitut ja wastas: ehk ta on 11m Julil hommiko kella 5 se teed käinud agga tullist ühhtegi näinud.
Killinga walla mees Johann Reiland sai ette kutsutut ja räkis wälja ehk ta on öösi 10 sell 11m peäle se teed metsa läbbi Kiwwiarro poole wähhja püüdma leinud agga polle mitte tulli waid innimessed metsast näinud, nenda samma temma ka ei tea kus tulli on wälja tulnud.
Moistetud sai: Need innimessed koddo lasta, ja se protokolli wälja kirjutama ja auliko metsa Herra Capitaine von Peters kätte saata, ehk se assi woib suurema kohto mõistusse alla anda.
