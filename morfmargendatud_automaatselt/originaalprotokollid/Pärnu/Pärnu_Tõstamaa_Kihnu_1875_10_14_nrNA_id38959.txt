Tulli kohtu ette Kihno walla tallopoeg Lina küllast Kalmo perremees Jaan Lamend ja andis teada walla kohtule,et temma nenda kui perremees Kalmo kohha peal,agga et temma Emma on wanna ja ellatand,ei wöinud ennam sellega öigeks sada egga maja perrega toimendada;sellepärrast wöttis nöuks ommale abbit-se on,naist wötta.
Kohhus möistis:Agga et nenda kui temma alles liisko allune mees on ja kui juhtub temmal wäetenistusse minna,siis ei woi temma kellegi peale pahhandada egga nuriseda ja sest armu lodata,siis jäeb temma naene ikka perrenaeseks ja ei woi ükski tedda selle kohha pealt wälja ajada,kui ta agga kronu säedussed ja kässud täidab,maksud ärra maksab,mis tullewad kronu ja koggokonna kässu peale wälja nöutud sama.Jani Lamendi teine wend Enn jäeb agga siis temma naesele abbiks.
Pea Kohtumees:Laas Oad XXX
Kohtumehhed:Mihkel Larents XXX
   "  Gustaw Köster XXX
   "  Enn Laus XXX
   "  Jaan Äpp XXX
Wollimehed:Mihkel Allas XXX
Tomas Rand XXX
