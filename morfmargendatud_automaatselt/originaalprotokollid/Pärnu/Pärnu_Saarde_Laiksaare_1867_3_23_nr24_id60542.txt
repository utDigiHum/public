Laiksaar Gemeindegericht, den 23. März 1867
Gegenwärtig: Gemeindegerichts Vorsitzer Subst. Johann Tals
Beisitzer Hans Tins
-,,- -,,- Tõnnis Behrs
Auf Antrag des Herrnstelle Forstmeisters E. Leuckfeld ward vergefordert der Buschwächter Endrek Tomsohn und degerte derselbe:
Er habe ein Walde gefunden, dass geschene Schlittensollen und 2 junge Baumenholze ganz frisch gehauen und als er mit dem hinzugerufene Gemeindevorsteher Jaan Kurm der Spur gefolgt, seien sie zur Wohnung des Kottisoo Lostreibers Jurr Mardek gekommen, er sie auch das Holz gefunden und abgenommen.
Jurr Mardik gab auf befragen auch zu, dieses Holz gestellen zu haben.
Der Herr stelle Forstmeister gab an, dass weil des Holz der Dieb abgenommen werden, auch der Tage eine geweigene Strafe zu brenchene dei und zwar
für 9 Schlittenfallen a 36 Cp 3.24 Cp S.
-,,- 2 junge Eschen a 12 Cp - 24 
3.48 Cp.
Es wände erkennt für Recht:
Dass Jurr Mardek zu verfeischen sei, das Strafegeld wan 3 R. 48 bei dem Herren Forstmeister zur Kronslägte einzuzollen.
Nachdem dieses Urtheil bekannt gewacht worden ware gab Jurr Mardek an, so wem zu sein, dass er diese Summa nicht begelten könne; welcher Umstand des ganzen Gemeinde bekennt sei.
Da aus es diesem Gemeindegerichte allerdings bekannt, dass Jurr Mardek sehr arm, als wurd mit zersteurung des Herrn Forstmeisters
Versägt:
Dass Jurr Mardek ein käuftigen Sommer bei denLamsterbreiten 7 Tage zu leisten habe.
Gemeindegerichts Vorsitzer Subst. Johann Tals
Hans Tins xxx
Tõnnis Behrs xxx
