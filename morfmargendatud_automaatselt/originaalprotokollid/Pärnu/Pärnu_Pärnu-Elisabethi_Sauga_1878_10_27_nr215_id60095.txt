Walla walitsus kaebas: Illu Jüri, Illu Peet, Illu Hendrek, Kalma Peet ei ole mitte käsu järele Tahkuranda sillule läinud. 
Ulli Hendrek wabandas, et küüdi kord ka nendesama pääwade sees täita olnud, aga tõistel ei olnud midagi wabandust.
Mõistus: Iga üks peab 5 Rbl trahwi maksma muud kui Ulli Hendrek üksinda 3 Rbl, sest et tal ka teist käsku täita oli.
Maksid ära: Illu Jüri			5 Rbl		Illu Peet			5 "		Illu Hendrek			5 "		Kalme Peet			5 "		Ulli Hendrek			3 "		
