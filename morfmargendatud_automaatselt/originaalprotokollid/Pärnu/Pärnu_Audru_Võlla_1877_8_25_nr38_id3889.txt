Astus ette Jakob Achtmann ja kaebab et see Wöllamees Mulla Ado Tamm on teda ähwardanud peksa ja ruusikaga pea pihta pörutanud, palub et Kohus neid seletaks.
Kutsuti ette Mulla Ado Tamm ja salgab, et tema pole Jakob Achtmanni puutunud, aga seda tunnistab, et Achtmann teda tubaka wargaks söimanud.
Kutsuti ette tunnistust ei ole kummagil.
Sai möistetud: et Jakob Achtmann peäb selgemad tunnistust üles andma ja ette tooma Tio Adamson ja Urruste Jürri Tomast.
Pea Kohtomees Karl Allikiwwi +++
Kohtomees Jaan Reimets +++
Kohtomees Jaan Kodasma +++
Kirjutaja J. Simonson
