Toi edde omma kaebdus ille Moisa Sullasset öbbematta illesse piddamest,nenda kui
Mölder Joop Eppen 1.Weski wargusse perrast,Tallopoijadest ning Moisast.
                                   2.Laupaewade Oesi rido ning Tullekaikadega peksmest.
                                   3.Pühhapaew ommiko Kortsi Jooksta ja Obboset toimendamatta jetta.
Ans Telwen 1.Sure soimamest ja rido otsida Prits norerraga ja keigi wimaks soimamest Lui norerraga,mis julgust illelia wotnut ja keddadist ei kule egga oli,lakkomessega teis ja sedda mida sindiwat wargussel,nago siis essimesse mees Kortsis joksta ja ennast teis lakkuda ja tülli ja rido tehha.
Sai se kord nomimessega ja oppidussega lahti,et ta teist kord ennast parremasti illesse peab,ja omma suurd Suud kui ta jonut on kattap,jubba tall monikord sedda andes antut.
Tomas Oad olli Kortsi Naist ja Kubjas arrota soimand ja sai selle est 15 opi Witsad.
Moisa Joop olli Weski Wargusse peal kohto ees ja itles et se leitut Willi Moise Anso jeggo olli,Ans itles et ta se Willi Köster Mardi kaest sanut,ja piddant Anso Michkel senna Kotti jure panna selle perrast se Koit Weskile widut,jei se assi jerrele kulamesse peal.
