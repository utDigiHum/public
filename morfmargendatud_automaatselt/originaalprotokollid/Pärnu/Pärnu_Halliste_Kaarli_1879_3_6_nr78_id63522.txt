Tännasel päewal sai Keiserliko Willandi Silla kohtu kässo peale - Magisterahti kohtu otsuse põhjusel kinni pantud Märt Kowite palg Marret Marguse heaks Märt Kowite kui ka temma perremehe Mats Ruble jures olemisega selletud.
Mats Rubel (Kowiti perremees) andis kohtu küssimise peale wastust:
Märt Kowit saanud 1878 suwwe pealt kigis palka 50 Rb. Aga Kowit olla Rublil palju kahju teinud. - keige pakkilisemal aial olla ta kohtu käimisega ja muu käimisega 70 päewa ärra wiitanud ja palka olla Kowit enne palka kinni pannemist wälja wotnud- ni kuidas järrel nähha20 Febr 1878 on Märt Kowit palga raha wõtnud			13 Rb - Cp		Märtsil                  - " -			7 " 50 "		23 April                - " -			5 " 		Mail                       - " - 			1 " 26 "		 - " -                      - " - 			1 " 46 "		Aprillil üks kirwes ärra kaotanud 			3 "		üks piip katki teinud			-  " 25 "		kohtu Leppituse põhjusel			2 "		70 päewa ärra wiiwituse kahju			70 "		Summa			103Rb 47Cp		
Mats Rubel pallub kohhud Märt Kowit nisuguse kahju tassumisele tompata et temma palgast nisugune kahju tassumine kinni saaks wõetud.
Märt Kowit tunnistas eesseiswad Mats Rubli ülles antud selletust ja noudmist tõeks.
arwatud: eesseiswa assi tõiseks kohtu päewaks siis wälja jänud tunnistajad kui ka kohtu käiad ette telli ja assi otsusele saata.
Kohtumees Peter Kogger [allkiri]
      - " -        Peter Leppik [allkiri]
      - " -       Johan Lensin [allkiri]
