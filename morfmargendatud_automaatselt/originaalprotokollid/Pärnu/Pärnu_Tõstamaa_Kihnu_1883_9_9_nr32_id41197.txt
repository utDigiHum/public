Kohtu ette tulli Tohwri Toma lesk Mari Wesikas ja kaibas oma poja Peter Wesika peale,et se teda olewa wägga peksnud ja ajanud wälja,sellepärast et ta kaks wana mäda puu tükki wötnud ja winud kartowli keldri salweks.Peter tulli ette ja räkis,et ta Emat sellepärast peksnud,et ta need puu tükkid,mis weski puud olewa olnud,ilma tema teadmata wötnud,pole tema käest küsinud ja peale sedda weel,et Ema tedda raha wargaks söimanud.Ema räkis selle peale,et Peter olewa selle tüli ise alustanud,et ta Madli Eppen lastega tema raha waras olnud.Kohhus möistis:et ta Emaga peab rahuliste ja ausaste elama ja tedda mitte enam peksa,agga Ema ei wöi ka teadmata enam puid wötta.
Kihno Kogokonna Kohhus.
Peakohtumees Enn Alas XXX
Kohtumehed:Mihkel Larents XXX
Juri Laus XXX
Jaan Sutt XXX
Mihkel Kott XXX
