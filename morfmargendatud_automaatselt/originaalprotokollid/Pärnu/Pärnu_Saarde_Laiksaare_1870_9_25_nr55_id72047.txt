Kaebas Jaan Melkson et wend Peter olla tedda sõimanud, aida lõhkuja, widdusilm etc; sellega ei olla temma rahhul.
Peter Melkson ütles; wend Jaan olla wanna aida ärralõhkunud, siis olla ka silmad widdus kui lubbanud tedda kinni sidduda.
Mõistetud: et Peter Melkson tõrreleda ja keelda mitte ennam ühte egga teist sõimada.
Sellega ollid rahhul.
