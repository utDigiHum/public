Kohtu ette sai nöutud Liiwa Mart Kott ja tema käest päritud,et ta selle strahwi raha 1 rubla ära maksab,mis Walla Kohus 26mal Augustil 1883 aastal möistnud.Sedda raha on Walla Kohus juba mitto korda nöudnud,agga Mart Kott on igga kord wastu pannud ja seni ajani mitte sedda täitnud.Kui nüüd Mart Kott ka selle korra Kohtu pärimise peale sedda raha ei maksnud ja oma wastupanemist awaldas,siis Walla Kohus möistis:et Mart Kott mitmekordse Walla Kohtu kässu wastu pannud-tedda 24 tunni peale torni panda ja wiimaks se raha 1 rubla riisumise kombel sisse wötta,mis ka sai täidetud.Pandiks sai wöetud 1 kirwes ja kasukas seniks kui ta raha ära maksab.
Kühno Walla kogokonna Kohus
Pea Kohtumees:Enn Alas XXX
Kohtumehed:Mihkel Larents XXX
Juri Laus XXX
