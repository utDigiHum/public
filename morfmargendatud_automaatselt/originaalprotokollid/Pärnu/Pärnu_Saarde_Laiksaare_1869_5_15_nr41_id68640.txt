Laiksaares wallakohtus sel 15. Mail 1869.
Jures ollid: peakohtumees M. Rast, wannemabbi Hans Tins ja noremabbi Hans Kamdron.
Keiserlikko Darto Bezirksverwaltungi kohtu kässu peale sai Tõnnis Pridik kohtu ette kutsutud ja wastas küssimisse peale: se heinamad mis temma ja temma issa olla liggi 20 aastad jubba krono metsas teinud, olla 4 wakkamad suur ja hea rohho kaswataja maa, - temma pallunud 12. Dec. 1867 Domainhowi kohhust, et temmale sedda maad jälle eddaspiddi niita annaks, maksu eest.
Metsawaht Karl Koesel, kelle piiris se maa tükk olla wastas küssimisse peale: tõssi, seal olla kül üks tükki laggetad maad, 4 wakkamaad suur, agga se olnud temma karjama olnud, ja mis lomatest olla järrele jäänud, sedda olla siis Tõnnis Pridik niitnud.
Arwati kõige häledega: et se maa, mis Tõnnis Pridik ja temma issa olla niitnud, olla Pappisilla Hindreko tallo heinama kõrwes, ja et Tõnnis Pridikul mitte heinamad ei olle, olleks wegga tarwilinne, et ta ka temma kätte eddaspiddi prukitada jäeks.
