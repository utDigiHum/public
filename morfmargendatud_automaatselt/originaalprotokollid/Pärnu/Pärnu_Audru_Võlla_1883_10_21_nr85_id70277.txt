Mari Madisson (käemees Karl Seiman) kaebab et temal 10 Rbl. raha 8 aasta eest mis tema Mart Kärpile laenanud nõuda olla!
Mart Kärp ütles et see 10 Rbl laenatud raha Mari Madissonile keik on tagasi makstud sannud, 5 Rbl eest kartohwled ja sea porsa 3 Rbl. eest annud ja 2 kangast 3 Rbl. eest kaetud saanud, sel järgil olla temal tagasi nõudmist.
Mõistetud: Et neil kumbkil selles asjas tunnistust ei ole, siis neid senniks rahule sundida kuni kohtule tunistust ette toowad. 
