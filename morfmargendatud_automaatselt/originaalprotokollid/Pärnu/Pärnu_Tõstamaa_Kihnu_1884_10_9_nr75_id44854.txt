Kohtu ette tulli Kalmu Enn Lamend ja kaibas:et tema wend soldat Danil Lamend ei anna temale tema nöudmise tema mördu kätte kalade püidmiseks ja peale sedda se raha 20 Rubla ka olewa weel saamata.Sai Danil Lamend Kohtu ette nöutud,räkis tema:et ta on Enn Lamendi käest oma kala wörkusi kätte nöudnud ja Enn Lamend pole neid mitte kätte annud,sellepärast tema ka pidanud Enn Lamendi mörrad kinni.Kohus möistis:et kui naad mölemad töine töise mere püised kinni pidasid,siis peawad ka seddawiisi jäma,Ennule wörgud ja Danilile mörrad,agga Danil peab Ennule 20 Rubla raha ära maksma,10 Rubla Mardi päwaks selle 1884 aastal ja töine 10 Rubla Jani päwas tulewa 1885 aastal.
Kühno walla kogokonna kohus
Pea Kohtumees:Enn Alas XXX
Kohtumehed:Mihkel Larents XXX
Jüri Laus XXX
