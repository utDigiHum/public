eodem dato.
Kaebas Hans Tamson, temma tellinud ennast Liiwa Casperi jure ellama, ni kaua kui temma lapsed olleks innimesseks saanud, agga et selle toal ei olla kattust peal olnud, teinud temma omma olletest kattusse peale 31 Rubla ja 11 kubbo wõtnud issi omma loaga ärra, kaks aastad ellanud siis ka temma omma ellotus, ka sealt aeanud tedda wälja.
Casper Petersohn wastas küssimisse peale: suiwilja põhk piddanud kaupa mööda sinna jääma, agga temma olla ärrawiinud, tahhab sedda taggasi saada.- selle peale ütles kaebaja et süggisi sinna talluse läinud ja sui omma korjatud põhhu sinna tellinud, - ja nüüd jälle wiinud krono walla maa peale Orrajale. - Selle peale ei wastanud Casper Peterson middagi.
Hans Tamson olli Liiwale wiinud nipaljo, et saanud hubbone ja 2 lehma ülle talwe peatud, - ja sealt ärra wiinud: 1 koorm hõlge ja 3 koormad põllu põhku, weel järrele olla et kord hobbusega wiia saab.
Arwati kõige häledega: et 31 Rubla ei wõida kaebaja iggatseda, agga 11 Rubla õlge ja 1 koorma põllu põhku annab Casper Peterson kaebaja Hans Tamsonil kätte. Sellega olli kaebaja rahhul agga Casper Peterson mitte ja lubbas Riiga minna.
Mõistetud: et se mõistus saab üllewal seatud.
