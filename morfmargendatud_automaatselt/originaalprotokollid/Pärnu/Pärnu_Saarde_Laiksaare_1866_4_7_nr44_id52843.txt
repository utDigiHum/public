Laiksaare Koggokonna Kohhus sel 7. April 1866.
Kohtus ollid: Pea Kohtomees Karl Tals
Kohtomees Hans Tins
-,,- Tennis Baers
Pea Kohtomees lassi Prottokolli mahha kirjutat, temma, Kohtomehhe abbi Johann Tals ja Walla-kirjutaja ollewa Kiwwiarros 3. Aprili kohs ollu, kus Loigo Saunik Jurr Toff ja Waltinberga Moisa Sillsemmneek Perremees Ado Peide ette tullo, ja räginu: Nemmaht ollewa omma wahhel sii kaup teino, kui Jurr Toff omma naistega ja lapsega lehwa Jurripääw Sillsemmneekus tehnima; Sillsemmneek wastama (gewatert) selle wõlga mis Toff Poija Tennise ihst 9 R. 14 Cp ja omma ja Poija Jahni ihst kirrikurahha 48 Cp., ülle ültsi 9 Rubla 62 Cp. wõlga, nendasama selle maksude ihst mis tullewa Aastas wehl tulleb. Sillsemmneek ollewa 2 Rubla Passirahha erramaksno, ja ollewat nemmaht sii Toffil naistega ja Lapsega Aastapasi nikawa kui Jurripääw 1867 wella anno.
Kohhus moistis: sedda prottokolli üllespannema.
