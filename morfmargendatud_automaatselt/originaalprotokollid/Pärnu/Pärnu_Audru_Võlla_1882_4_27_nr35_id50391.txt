Astus ette Tallitaja abi Jaan Wichmann ja Karl Seimann, ja andsid üles, et minewa aasta enne Jõulu on nemad leidnud, et Walla Magaski aida uks, on taba lukkuga üksi kinni olnud, ja suur lukk on üsna lahti olnud see olnud esimest korda, aga see kewadi on ta jälle teist korda leidnud, et see sama lukk on lahti olnud ja seda sama wiisi esite taba lukk on kinni olnud ja teine suur lukk lahti, ja kui salwest wilja järgi waatanud, siis on leidnud, et ka salwe wiljal auk sees on olnud, nenda arwata et inimestele pole nii palju wilja wälja annud. - siis on küll märgi pannud et sest selgemad aru saaks, aga peale selle pole enam ühtigi leidnud. -
Tallitaja Hans Pirson on seda asja teiste Abilistele rääkinud, ja nõuks wõtnud, hoolega selle järgi walwata, aga ei ole siia saadik ühtigi aru saanud kes nende lukkude lahti tegija on olnud. -
Ka päriti järgi, kas need lukkud wana moodi on selle Magaski aida ukse ees, aja andsid kõik üles, et need lukkkud päris halwad wana moodi ja puust on, ja muud ei ole rauast kui üks telg ja wõti - sellepärast
Sai mõistetud: et need Wõlla Magaski aidas ukse lukkud, täitsa halwad on, siis peawad uued aida lukkud tehtud saama ja see asi peab hoolega järgi kuulatud saama, ja sest Protokollist saab üks ära kiri, keiserlikko Perno kihelkonna kohtule sisse saata, ja Magaski ait peab igaöösi ikka walwtud saama, senni kui uued lukkud ette saawad tehtud.
Peakohtumees: Jaan Reimets XXX
Kohtumees: Mart Kodasma XXX
Kohtumees: Hans Pirson XXX
Kohtumees: Jüri Moppel XXX
Kirjutaja: J. Simonson
