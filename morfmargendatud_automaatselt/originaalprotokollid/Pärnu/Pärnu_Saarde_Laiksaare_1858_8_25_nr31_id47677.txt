Laiksaare Koggokonna kohtus sel 25mal Augustil 1858.
Kohtus ollid Johann Peterson, pea kohtomees.
-,,- Jaan Joosti, kohtomees.
-,,- Jurry Orraw, kohtomees.
Metsa Herra kässo peal kutsiti kohto ette Metsawaht Hans Liet, ja küissiti kust se tulli temma Massi Metsa akkanud, ja temma räkis et ta algu Laiksaare rahwaga piri peal ees olnud kui Orraja wastolikone mets põllenud, ja selle piri peale rawi teinud, agga tulli on õhto suure tuulega ülle wissatud Orraja metsast Laiksaare metsa sisse. Teise hommigo on nad Orraja mehhete abbiga sedda tuld sanud jälle sisse pirida ja kustuda, ja seal on ka pirikubjas Jacob Anderson ja kohtomees ja woormünder olnud.
Walla woormünder Ado Talts ja kohtomees Jaan Joosti on ka seal jures olnud, ja perrast weel waatnud et se tulli illusti saab kustutud ja wahhitud et ta mitte ehmalle ei lähhä.
Moisttei: Sedda kirja panna ja Protokoll Metsa Herra kätte saata.
Johann Peterson, pea kohtomees.
