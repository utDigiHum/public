Karl Maddison Arre walla mees ja temma naine Ann tullid kohto ette ja pallusid kohto Protokolli ülles wötta, et nemmad sedda last Kristian Abrami poeg Nael ommale poeaks tahtwad wötta ja lubbawad tedda omma lihhase lapse assemel kaswatada ja omma (ainsaks) warra pärriaks kinnitada.
Selle lapse lihhane issa Abram nael Sauga walla mees lubbab siin koggokonna kohto ees, et temma selle kaubaga wägga rahhul on ja (sedda) hea melega sowib ning tunnistawad nemmad möllemilt poolt sedda omma käe kirjaga
Sesamma üllemal nimmetud Karl Maddison on sündinud 3" Octobril 1820 aastal, temma naine Ann on sündinud 19" Nowemb. 1827 nende kassopoeg Kristian Abrama poeg Nael on sündinud 15" Juni 1872.
Abram Nael lapse lihhane issa XXX
Karl Maddison lapse kassu issa XXX
" naine Ann XXX
Walla wannem /Hans Lasn/
kirjutaja
