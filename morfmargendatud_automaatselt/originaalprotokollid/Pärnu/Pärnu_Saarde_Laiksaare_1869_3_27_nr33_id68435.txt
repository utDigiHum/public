eodem dato.
Metsapäliku Herra kaebtusse asjas 13 Märtsist Nr 61 Jacob ja Karl Andressoni wasto metsa wargusse pärrast, et ilma loata raiunud ja risto teinud
1			Jacob Andresson			2			Reggi			5 R			16 Cp								1			saare jallas 1 s. 1 1/2 w						36		2			Karl Andresson			5			saart 1 s. 1 1/2 w			1			80								5 1/2			pärnapuud 1 s. 1 w			9			55		
Said ette kutsutud ja metsawahhi G. Reinwaldti jures ollemissega jarrele kulatud ja wastasid küssimisse peale:
ad 1. Jacob Andresson: temmal olnud üks saare puitest reggi, jallaste ei mäleta temma, need puud olla temma ommast kruntist heinama laastetnikkust raiunud.
Selle peale ütles G. Reinwaldt, et temmal 2 regge olla olnud.
ad 2. Karl Andresson: temma olla saanud G. Reinwaldti käest üks sella täis nieni saanud mis Jürri Toff olla warrastanud, ja temma käest ärra wõttnud, sest ei olla temmal mitte lubbast olnud, et neid olleks wõinud ärrawia, arwata 200 ja liggi 300 tallu kruntist raiunud, - need wiis /5/ saare puud olnud enne teda ja ammust aeast jubba temma sauna peal, kust nemmad raiutud sedda temma ei teada.
G. Reinwald ütles: et aasta taggasi Jürri Toffi käest nieni ärra wõtnud, ja wõib wõimalik olla et niened, agga õiete ei teada, metsast ei olla tedda mitte kätte seanud, agga jäet olla kül sinna poole wälja läinud, - saare raiumisse jurest ei olla mitte kätte saanud, kronometsas olla käinud läinud ja temma sauna pealt kuiwatud jallased. Kohtumees Karl Tals olla sedda metsa wargust järrele watanud.
Mõistetud: et se assi jääb teise kohtupäwaks kunni Karl Tals saab kohtu tellitud.
