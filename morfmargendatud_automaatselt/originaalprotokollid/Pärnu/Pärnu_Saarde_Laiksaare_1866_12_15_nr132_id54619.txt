Laiksaare Koggokonna Kohhus, sel 15. December 1866
Kohtus ollid: Pea Kohtomees Karl Tals
Kohtomees Hans Tins
-,,- Tennis Behrs
Sai ette kutzutut Jurr Mardik ja küstut temma Pearahha-wõlla perrast, 6 Rub. 45 Cp. mis temma 1860 liggi 1862 Aasta ihst wõlgu, kele peal wastas, sii rahha ollewa walla wöhrmünder Ado Talsi kätte, kes kui temma Possa tallohs üks Maija ehhitanu, 7 Rubla errawottnu ja lubbanu Pearahha erramaksat.
Wöhrmünder Ado Tals wastas, temma ollewa küll selle toha ehhitama rahhast 7 Rubla errawottnu, aga selleperrast kui Jurr on nenda palla Krohno Willi ihst wõlgu ollu.
Sellepeal akkas Jurr Mardik suhre ehlaga karjuma, temma nähwa küll kui siin oigus ei sahwa ja kohto lauajuhres üks suggu-selts ihstuwa.
Kohhus moistis: Jurr Mardik selle sõnnade ihst 24 tundi peal torni panna; ja peab temma omma pearahha-wõlg erramaksma, nenda kui neid 7 Rubla ollewa Krohno willi-wõlla ihst arwatut.
