Kaebas Ado Toots, et temmal olla saada Jürri Mardiku käest 1 wakk rükki.
Jürri Mardik wastas küssimisse peale: temma olla ärrmaksnud, tunnistus Jürri Saul.
Jürri Saul tunnistas: temma teada sedda selgeste, et Jürri Mardik olla Pihke Ado Tootsi rükkid maggasini ärramaksnud.
Mõistetud: et se kaebtus saab tuhjaks arwatud.
Selle otsusega ei ollnud Ado Toots mitte rahhul.
Mõistetud: et se assi wähhem kui 5 rubla ei sa mitte appellationi wälja antud.
