Kaebas Michkel Kurm, et temma rentinud endale Hindrek Motti sullaseks, annud temmale 1 Rub kaerahha ja nüid ei tulla teenima, aasta piddanud akkama 22. Aprilil 1870. tunnistus suggula Leno Orraw.
Hindrek Mott wastas küssimisse peale: tõssi, kaup olnud kül nõnda, agga temma olla wanna ei jõuda ka mitte teenida, - ka kaup olnud nõnda et kaebaja piddanud 23. Apr. järrele tullema, agga ei olla tulnud, selle pärrast rentinud ennast teiseks aastaks Jaan Kurmile. - pärrast Jürri päwa.
Jaan Kurm ütles: et jubba Hindrek Mottil 3-4 rubla rahha annud ja temmal kartowlid mahha teinud, - temmal ei olla muud sullast ja selle pärrast ei wõida tedda mitte lahti laska anda teenistussist.
Mõistetud: teiseks korraks.
