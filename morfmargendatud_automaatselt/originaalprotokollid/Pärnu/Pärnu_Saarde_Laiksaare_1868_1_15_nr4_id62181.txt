Laiksaare koggokonna kohtus sel 15 Janwar 1868
Kohtus olli
Kohmees Hanz Tinz
Tõnnis Behrs
Hanz Tinz
Tulli ette Karli Andersson, ja kaebas et temma wend Willem Andresson ej kuulwa, wäikes äätt, ja koggoni kuulmatta ollowa sündimesses saadik.
Sai ette kutsutud tunnistus Ado Tals, ja tunnistas et see ollewa kül tossi, mes see Karli Anderssoon omma wenna ulle raakiwa ja ta ollawa kül sündimesses saadik kuulmatta.
