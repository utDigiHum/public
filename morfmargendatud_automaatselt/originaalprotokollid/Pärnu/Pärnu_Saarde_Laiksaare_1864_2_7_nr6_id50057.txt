Laiksaare Koggokonna Kohtus sel 7mal Febr. 1864
Kohtus ollid Pea Kohtomees Juhan Petersohn
Kohtomees Jürri Orraw
-,,- Jaan Kurm
Tulli Rendi maksmese Paewal Possa Perremees Jurri Teffer ette ja pallus Kohut et temma ei woiwa sedda Magga ennamb piddada et Kohus lubbab sedda Magga omma Weimehhe kette anda, se perrast et temmal 2 ainust Last on, ja Poeg weel wegga noor on ni kaua weimehhe kette anda kui Poeg nii wannaks saab.
Sai sisse kutsutud temma Poeg Jürri ja temma kaest küssitud kas temma sellega rahhul on, mipeale temma wastas et taa sehja wegga rahhul on.
Moisteti. Et Jürri Teffer woib selle omma Magga küll weimehe kette anda.
