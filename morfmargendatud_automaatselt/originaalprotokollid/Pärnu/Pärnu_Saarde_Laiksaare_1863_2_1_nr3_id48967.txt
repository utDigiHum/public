Laiksaares sell 1sel Webruaril 1863
Tännasel päewal tulli sia koggokonnakohtu ette se siit walla mees Jürri Blumer ja pallus lubba, et temma se Aedemeeste walla mehhe Dawid Martinsoni eest 275 rubla rahha eest soldatiks tenima tahhab minna.
Se üllemalnimmetud palwe peale andis siit wallakohhus temmal selle wisiga lubba:kui temma omma emma toitmisseks			65 rubla		ja pearahha sennikui rewisioni kirjutmisseni			50		se on ühte kokko			115 rubla		
sia walla laeka sisse maksab ja Aedemeeste wald temma naese eest, mis alles kolm kuud siin waldas Jürri Blumri naeseks olnud, - kui nikrudi naese eest murret peab, siis tunnistab Laiksaare Mõisawallitsus ja koggokonna kohhus, et neil selle temma soldatiks minnemisse wasto middagi ei olle.
