eod. dato.
Kaibas Loigo mees Ado Andersohn temma wenna, Loigo Perremehhe Jacob Andersohni peal, ehk ollewa tend selleperast kui ühhe peiwas eijolle temma Karjane karjas ollu, irmus kissanu ja peksno.
Jacob Andersohn wastas, Ado Andersohn ei sahdawa middagi karjane rohkemb karjas, ja temma küssinu, miskperrast sedda, on Adol temmal ühhe kiwwiga wiskanud, selleperrast nemmaht ollewa küll rihdu lenno, ja kus üks teistel rinnajuhres aijanu, on Mina Andersohn, Ado naine, wehl Adol üks tük puu anno, las akkawa Jacobi sellega löhma.
Kaddri Tins tunnistas: temma sedda eijollewa neino, mis mohda sii kisklemenne akkanu, aga kus temma juhre tullo, on Jacob Adol kähnu üks rehhal kaest erra, ja nimotto maas kisklenno, Ado on alla, Jacob peal ollu.
Nenda kui wehl tunnistlejaht tarwis, sis
Kohhus moistis: sii assi tullewa kord selletat.
