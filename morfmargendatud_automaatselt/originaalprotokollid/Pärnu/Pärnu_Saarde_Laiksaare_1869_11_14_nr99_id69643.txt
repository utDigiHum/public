Johann Sigor kaebas, et Peet Kuk olla temmale õtlenud hobbose jurkiga. Tunnistus J. Grenstein ja Joh. Liesment.
Peet Kuk wastas küssimisse peale: temma ei olla mitte kaebajad sõimanud agga kaebaja olla tedda sõimanud litsiks, horaks, kõrtsimehhe narraks etc, agga temma ei olla kaebajat mitte hobbuse jurgega ütlenud, kui ka temmale muud santid sõnnad ehk ütlenud, siis olla mitte temma, waid lits ja hoor ja kõrtsimehhe narr.
Johann Grenstein ja Johann Liesment ei teadnud middagi tunnistada.
Jacob Meutus tunnistas et kaebaja kui ka Peet Kuck olla endi wastastikku sõimanud.
Peet Mardik: tunnistus nõnda sammuti. Wiimati ütles, et Peet Kuck ütlenud sinna tallad kõik se sui minnu hobbuse selgas kui wennelane tarre äres litsi selgas.
Arwati kõige häledega: selle peale leppisid ärra.
Mõistetud: et se protokolli kirjotud kuida siin on sündinud.
