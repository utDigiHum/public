Laiksaares wallakohtus sel 30. Januaril 1869
Jures ollid: peakohtumees M. Rast, wannemabbi H. Tins ja noremabbi H. Kamdron.
1. Jacob Ilwes kaebas, et metsawaht Jaan Kask ka need heinad mis temma perremehhe kruntist teinud 1 koorm nende temmast kronometsa allt tehtud 2 koorma heindega ärra wiinud, tahhab nüid neid kätte anda, kui walla kohhus temmale mitte neid ei lubba kätte ehk ei woi kätte lubbada, mis kroono metsa allt teinud.
2. Jacob Andresson kaebas nõndasammuti, et temmal 2 koormad perremehhe kruntist ja kroonometsa allt 4 koormad wargsil wiisil ärra wiinud, et kül wõera walla innimestele annud kronometsa alt, heino tehja ülle 100 Sao, temma pannud perremehhe kruntist heinad selle pärrast kronometsa maade peale pannud, et perremehhe krund olla wagga maddal olnud kuhja assemele.
3. Marri Ilwes kaebas nõndasammuti, et ka nenda temmal ärrawitud, mis mehhe wennaga olla ühhes teinud.
Jaan Kask wastas küssimisse peale:
ad 1. temma arwast peawad need heinad kõik kronometsa maa pealt tehtud ollewad, sest sedda teada ka perremees - ja kronometsa maa peale temma arwust nõndasamma maddal ollema kui perremehhe krund.
ad 2. wabbantas ennast nõnda sammuti, ja saanud heinad kõikest 8 kimpu.
Perremees Hans Tins ütles, et temma annud kül nende kaebaja monnilestele umma kruntist heinu tehja annud wasto kronometsa.
Arwati kõige häledega:
ad 1. Jaan Kask peab Jacob ja Marri Ilwesele 10 puuda heino andma ja mis krono metsa alt teinud, selle ülle käsku metsa pealiku Härrale kaebtust.
ad 2. Jaan Kask peab Jakob Andressonil 20 puda heino andma, mis perremehhe krundi pealt tehtud, - ja kronometsa alt tehtud heinade ülle wõib metsa päliko Härra jures kaebtust tõsta.
Kaebajad ollid sellega rahhul agga Jaan Kask mitte ja iggatses õppusse kirja.
Mõistetud: et Jaan Kassel õppuse kirri wälja anda ja ka metsa päliku Härral se ülle üks mahha kirri läkkitada.
