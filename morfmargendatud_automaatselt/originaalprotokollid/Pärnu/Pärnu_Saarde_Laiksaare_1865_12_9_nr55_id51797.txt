Tulli ette Loigo Perremehs Jacob Andersohn ja pallus, las kohhus Loigo sepp Johann Poss üllekuhlatab, temma woiwa sedda tunnistat, kui temma Toffile 1/3 wakk linnasehmätti anno (wahta Prottokol Nr 45).
Johann Poss sissekuzzutut tunnistas: Temma nenda pallo tähdwa, ülleminnewa süggisi (1864) ollewa Jurr ühhe pühhapääw achta omma linnasehmätti puhastnu ja üttleno, temma nenda pallo sahwa - 1/3 wakk - kui Jacobil wõlgu taggasi maksat.
Tunnistleja wällaüttlemenne sai ette lohatut, kele peal Toff ütles, sedda eijollewa mitte eige.
Kohhus moistis: Selle 4 Rublast mis Jurril Jacobi kähst sahda on, sahwat 1/3 wakka linnasähmätta ihst 1.30 Cop mahha arwatut, ja peab Jacob temmale sis 270 Cp maksma.
Sii moistmenne sai ette lohatut ja Jurril õppuse kirri wälla antut; ja (maksas) Jacob 270 Cp panni laua peale, kele järgi kohtomehhet kokko rägis, nenda kui Jurr Toff omma Poijaga 2 Tage peiwaht wõlgu on, mittu käsku sahnu, aga ei kuhlip - ja
Kohhus moistis: selle rahhast 1 Rubla nikawa kinni panna kui Metzaherra jelle Metzapeiwaht küssip ja Jurr Toff omma ja omma poiade ihst 2 peiwat erratenno.
Sai ette lohatut, kele peal Jurr Toff üttles, temma middagi rahha nuid wastu ei wottawa.
