Astusid ette Wõlla wallast Puutöömehed ja kaebasid et neil 1876 aastast Wõlla Kohtomaja ehitaja Hans Tamme käest raha saada:
Mihkel Jessiwerel 5 Rubla 50 koppik
Jaan Kõlwardil 17 Rubla
Mihkel Tammel 13 Rubla
Paluwad et Kohus sunniks Hans Tamme neile seda tööraha ära maksma.
Kutsuti ette Hans Tamm ja ei salga, et need Mihkel Jessiwere, Mihkel Tamm ja Jaan Kõlwart on tema juures töös olnud ja neil raha saada on, aga sellepärast ära ei maksa, et, need mehed on töö juurest päiwe ära wiitnud ja nimelt: Mihkel Jessewere 3 pääwa, Mihkel Tamm 2 pääwa ja Jaan Kõlwart 7 pääwa ja tema üksi pitand kõik müüri töö walmis tegema:
Mihkel Jessiwere annab aga üles, et kõigest üks pääw tööst ära olnud.
Mihkel Tamm annab üles, et kõigest ka üks pääw tööst ära olnud, ja Jaan Kõlwart ütleb kõigest 2 pääwa tööst ära olnud.
Kutsuti ette tunnistust kes just teab kui mitto pääwa keegi mees tööst ära on olnud. Ei ole molemilt poolt ühtegi. Sellepärast:
Sai mõistetud: et Hans Tamm seda ei salga et need kaebajad Mihkel Jessiwere, Mihkel Tamm ja Jaan Kõlwart on tema töömehed olnud ja neil temalt üles antud raha saada on, aga et need moned pääwad tööst ära on olnud, aga selged tunnistust ei ole kui mitto pääwa keegi ära on olnud, siis arwatakse need meistre Hans Tamme poolt üles antud pääwad pooleks, esiteks, et Mihkel Jessiwerel 1 1/2 pääwa puudu on jään, sellest jääb 1 Rubl 12 koppik maha, Mihkel Tammel 1 pääw puudu on siis jääb 75 koppik maha ja Jaan Kõlwardil 3 1/2 pääwa puudu siis jäeb 2 Rbl 62 koppik maha ja Hans Tamm peäb Mihkel Jessiwerel sest nõutawast 5 Rubla 50 koppikust 4 Rubl. 38 koppik ära maksma, ja Mihkel Tammel tema 13nest Rublast 12 Rubla 25 koppik on Hans Tammel ära maksa ja Jaan Kõlwardile 17 Rublast 14 Rubla 38 koppik on Hans Tammel ära maksa, nenda maksab Hans Tamm ühte kokko 31 Rubla 1 kop: kolmes terminis ära, see on 14 pääwa pärast 10 Rubla, Mihklepääw teist 10 Rubla ja Jõulu aeg 11 Rubla 1 koppik. Hans Tamm ei ole selle mõistusega rahul, siis sai järgi antud suuremad kohut nõuda.
Pea kohtomees: Jaan Reimets XXX
Kohtomees Mart Kodasma XXX
Kohtomees Hans Pirsen XXX
" Jüri Moppel XXX
Kirjutaja J. Simonsen
