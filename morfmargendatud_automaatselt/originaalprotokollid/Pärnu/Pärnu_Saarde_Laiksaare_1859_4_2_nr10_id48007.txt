Laiksaare Koggokonna Kohtus sel 2 April 1859.
Moisas ollid Johann Peterson pea kohtomees
Jurry Orraw kohtomees
Jaan Joosti, kohtomees.
Laiksaare Metsa Herra kässo peal sai Patti walla tallopoeg Kõwwere Johann Puck kohto ette kutsutud, ja küssiti temma kaest, kas ta on Laiksaare Mustajoe Metsas wargal keinud.
Johann Puck tunnistab et temma omma pojaga Jaan Pukkiga on Laiksaare metsas olnud ja nelli saard mahha raijunud ja ühhe ka poolest erra raijonud, se on agga weel kanno peal seisma jänud, ja siis on temma weel 9 noort saared jallaste tarwis raijonud.
Moisteti: Nenda kui tunnistud kirja panna ja Protokoll Metsa Herra kätte saata.
Johann Peterson, Pea kohtomees.
