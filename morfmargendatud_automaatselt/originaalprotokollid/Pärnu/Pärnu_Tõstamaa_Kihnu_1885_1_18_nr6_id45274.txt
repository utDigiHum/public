Kohtu ette tulli Liiwa Jaan Karjam ja kaibas,et Mart Kott oma naese Mallega kodu tulles 15mal Januaril sel 1885 aastal,hakkanud söimama ja kiruma ja ähwardanud peksa ja ka kiskunud tedda oma naesega tooli pealt maha.Mart Kott oma naese Mallega on ühhed rijakad ja tülitsejad inimesed ja ei anna neile kogoniste rahus elada.2 aastad on naad temaga ja tema perekonnaga tülitsenud ja riidlenud.Kohus möistis:et Mart Kott kui üks kangekaelne inimene on,Kohtu käskusi ei täida mitte.Walla Kohus on tedda-Mart Kotti ja tema naest Malle strahwinud torniga,on strahwinud rahaga,agga nemad mitte mingisugust ei hooli-tedda Mart Kotti ja tema naest Malle Austud Keiserlikku Kihhelkonna Kohtule ette panda,et Austud Keiserlik Kihhelkonna Kohus saab neid noomima ja karistama.
Kühno Walla kogokonna Kohus
Pea Kohtumees:Enn Alas XXX
Kohtumehed:Mihkel Larents XXX
Juri Laus XXX
