Laiksares sel 8al Mail 1868
Kohtus ollid: Kohtomees Hants Tints
-,,- Tõnnis Bäers
Abbi Hans Kandron
1. Tullid kohto ette Laulaste Rino Leinatsaare käemehhed ehk eestkostjad Sillaotsa Jurri Orraw ja Kamma Hermann Kandron ja pallusid Jakobi Leinatsaari Oksioni rahhast ärra maksa allamal nimmetud asjade eest ja nimmelt
1			35 wakka rukki			selle eest, et Leinatsaar kassinaminne külwanud kui eest on sanud ja ka selle eest, et temma on ühhe tükki külwamatta jatnud		2			Karja poisi söötmisse eest 6 naddali ajal			6 rub		3			Karja poisi rided ja nimmelt			3 särki 3 rub, 3 püksi 1 rub 20 kop, 3 kindad 75 kop 3 sukkad 75 kop ja 1 mantel 3 rub 50 kop		4			3 1/2 sülla puude eest			2 rub. 10 kop		5			1/2 sulla lõmmude eest			60 kop		6			5 koorma teibi eest			1 rub. 25 kop.		7			Lesse Rino Leinatsaare mona eest mis kohhus jubba mõistis ja nimmelt:			1 1/2 wakka Rukkide eest 4 rub 50 kop, 1 lesika räimede eest 75 kop ja ühhe leesika soola eest 40 kop.		
Jacob Leinatsaar selle jures olles ei üttelnud middagi.
Kohhus mõistis:1			24 wakka Rukki se on neljas terra.		2			Karja poisi söötmisse eest 6 rub.		3			Karja poisi rided ja nimmelt: 3 särgi eest 1 rub. 5 kop, 3 pukside eest 1 rub 20 kop, 3 kindade eest 60 kop, 3 paari sukkade eest 60 kop, 1 mantle eest 3 rub.		4			3 1/2 sülla pude eest 2 rub		5			1/2 sülla lõmmude eest 60 kop		6			5 koorma teiwaste eest 1 rub.		7			Lesse Rino Leinatsaari moon ja nimmelt 1 1/2 wakka rukkide eest 4 rub. 50 kop. 1 lesika räimede eest 75 kop ja ühhe lesika soola eest 40 kop.		
Nõnda kui Oksioni rahha summa kassin on sepärrast kohhus mõistis üksi wälja maksa: 1. Karjapoisile Kaarli Leinatsaarile 12 rub. 90 kop 2. Lesse Rino Leinatsaare mona eest 5 rub. 60 kop. 
2. Tulli ette Willem Trey Laulaste Maja perremehhe Jakobi Leinatsaari pois ja ütles et temmal on Jakobi Leinatsaari käest sada
1			25 Rub. palga rahha		2			12 Rub wõlla rahha		3			3 saargi eest 2 rub. 40 kop, 3 pukste eest 1 Rub. 50 kop		4			1 mantle eest 6 rub.		
Jakob Leinatsaar selle jures olles ei üttelnud middagi Trey nõudmisse ülle.
Kohhus mõistis: 3 särgi eest 2 rub 25 kop, 3 pükste eest 1 rub 50 kop ja 1 mantle eest 5 rub. Agga wälja maksa kohhus mõistis Oksioni Rahha summa kassinusse pärrast ükspäines 33 rub. 75 kop.
3. Tulli kohto ette Willem Joets ja ütles et Jakob Leinatsaar peab temma poja Johhanile maksma
1			Palga rahha			15 rub.					2			temma ennesele 15 naela sia lihha eest ja			2 rub			25 kop		3			60 naela soola eest			1 rub			20 kop		
Jakob Leinatsaar selle nõudmisse jures ütles: 3 1/2 sulla puude pudus on tulnud Juhhani Jõeksi läbbi ja sepärrast pallus puide rahha 2 rub kinni piddada.
Kohhus mõistis: Ükspäinis nüüd walja maksu: 13 rub. ja 2 rub. palga rahhast senniks sisse jätta kui Juhhan Jeets ette tulleb ja sedda selletab.
Peale selle kohhus mõistis weel wälja maksa
1			Pearahha 3 mehhe eest			9 Rub. hõb					2			Rukkide eest mis Maggasist sai wälja antud Kadri Trey ja			4 rub					3			mahhakulutamisse rahha						45 kop		
Ühte kokko Kohhus maksis wälja Oksoni rahha summa üllewel nimmetud päwal 78 rub 75 kop õb.
