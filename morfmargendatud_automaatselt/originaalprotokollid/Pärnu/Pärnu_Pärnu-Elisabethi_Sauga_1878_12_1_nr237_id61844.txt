Karl Karelson, Mihkel Janson Hans Madisson Hans p. Kustaw Johannson Kustase p., Mari Sarw ja An Kubja olid kahes wooris käinud, aga et koormad lõhutud olnud, muud kui An Kubjal ja Mari Sarwel olnud terwed - seepärast woetud woori raha ära, ega põle nad rohkem saand, kui 5 rubl.
Mõistus: kumbagi, Mari Sarw ja An Kubja, et nende koormad terwed olnud, saawad a´ 2 r. ja teised 4 ülemal nimetatud a´ 25 kop.
Karl Karelson, kes sel korral potritskin olnud peab sea 8 pääwa sees ära õiendama.
