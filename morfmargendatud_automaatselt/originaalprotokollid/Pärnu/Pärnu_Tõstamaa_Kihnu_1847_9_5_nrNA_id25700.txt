Kaewas Laas Oad,et temma Odra ja Kartufle pöld Kaerametsa serwas,olli Lemsikülla Ellajattest ärra sötud,ja Lause Enn olli need Ellajad sealt wilja pöllult wälja aijanud,ja olli 24 Ellajad ja 2 Lause Jürri hobbest ka holkas olnud ja on se kahju Möisa=Wallitsus kohtomeestega ülle waatnud ja on leitud et se kahju nenda on tehtud kui kaewatud olli,möistud:
et Lemsikülla rahwas need kelle Ellajad seal wiljas ollid olnud,peawad igga Ellaja ja nenda hästi nende kahhe nimmetud hobbese pealt 1  1/2 karnitsed Odre ja 1  1/2 karniz kartuflid,selle kahju tassumesseks Lasule maksma.
Kihno=Saare Möisas sel 5mal Septembre kuupäewal 1847
Daniel Riewil kohtowannem +++,Tomas Pull kohtomees +++,Jaan Uetoa kohtomees +++
