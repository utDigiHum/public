Auf Antrag des hier amwesenden stelle Forstmeister Hl. E. Leuckfeld, ward die Klage des Kronsbuschwächters Wango Willem Kartau zu Protocoll genommen, wie folgt.
Als seinen Reine seine 13 eschen Schlittenfollen defordiert worden nur sei aus Allen zu schlichten. Dass der Lostreiber Jurr Mardik der Diebzeuge hätten nämlich gesehen. Dass derselben mit einem Beil in den Wald gegangen nur andere, dass derselbe aus den Walde gekommen und als derauf sein Degenunters Junge, die Spuren des Jurr Mardik gefolgt, habe sich ergeben, dass derselben in den Wald gegangen, die 13 Sollen gehauen und wieder herausgekommen.
Jurr Mardik nahm in Abrede, die Sollen desewudert zu haben und sei er mit den Beil nur zum Strand zum Scmidt gegangen.
Buschmeister Karl Kamdron bezeugt: Er habe den Jurr Mardik mit einem Beil in den Wals gesen gesehen und die Frage, wo er seigehe, habe derselbe geantwortet, zum Stande zum Schmidt.
Buschwächter Karl Kösel bezeugt: Er habe den Jurr Mardek mit mein Beil den aus dem Walde folgende Klag kommen gesehen und als der Knecht des Wango Buschwächters, Johann Orraw, der Spur in den Wald gefolgt, habe sich angeben, dass dieselbe dortsei gefahrt, wo die Sollen defrudiert waren.
Versägt:
Den Johann Orraw zur nächten Sitzes zu citiren.
Gem. Gr.Vorsitzer Karl Tals xxx
Hans Tins xxx
Johann Tals xxx
