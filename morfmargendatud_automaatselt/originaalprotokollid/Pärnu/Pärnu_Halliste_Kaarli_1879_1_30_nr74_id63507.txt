Hans Labi Heimtalist tulli ette ja kaebas, Sel 15ndamal Jaanuaril olla kaebaja Masa kõrtsi juure soitnud hobusel tekki peale pannud ja ise kõrtsi läinud. Kui kaebaja mõne aia pärrast kõrtsist wälja tulnnd, olnud tema hobuse tekk ära warastud. Teisel pääwal tulnud kaebaja Rimmo kõrtsi juure tekki warast otsima ja leidnud seal oma tekki Kaarli mehe Mats Rubeli saanist, kes ka Rimmu kõrtsis olnud. Sel järgel paluda kaebaja et tekki warras Mats Ruubel saadusliku trahwi alla saaks tõmbatud ja kaebajale kahju tasumisele tekki otsimise pärast sunnitud.
Mats Ruubel wastas kaebduse peale, ta olla kül sel 15 Januaril Masa kõrtsis olnud ja sealt kodu tulles olla ta tee pealt ühe tekki leidnud, mis Hans Labi teisel pääwal Rimmo kõrtsi juures omaks teinud ja mis ta Hans Labil kätte annud. Warastanud ei ole kaebatu mitte seda tekki. Tunnistajaks andis kaebatu ülles Märt Käär ja Mihkel Kondel, kes näinud, kui kaebatu nimetud tek leidnud.
arwatud: assi poolele jätta, kohtu käiad ja ülesantud tunnistajad teiseks kohtu päewaks ette tellida
Jaak Raegson [allkiri]
Jaan Mõrd XXX
Petter Leppik [allkiri]
