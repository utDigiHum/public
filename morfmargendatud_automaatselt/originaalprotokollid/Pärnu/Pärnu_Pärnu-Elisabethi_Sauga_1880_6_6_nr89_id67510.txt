Wallawanema abi Andres Peterson kaebas, et Räägo koolmeister Johann Jaanson on oma woliga koolimaja ehituse puudest 2 palki oma küüne seinaks wõtnud ja 2 palki kõrwale wiinud ja palgi otsi kööki warjule peitnud, nagu seda tema sulane Jakob Willemson rääkinud.
Johann Janson ütles kaks palki, mis ehituseks kõlbmatud olnud, küüne seina pannud, aga omalt poolt jälle 2 palki asemele annud aga kaks palki on ta muidu õuest kõrwale wiinud, et õues tüliks ees olnud.
Jakob Willemson wastas kui tunnistus, et Johan Janson 2 palki küüne seina puuks on wõtnud aga jälle 2 palki omalt poolt asemele annud ja 2 palki koplisse on wiinud ja ka köökis pooled palgid on olnud, milledest pärast kartuli koobas ehitud.
Mõistus: Et Johan Janson oma lubaga 2 palki küüne sisse on pannud maksab ta nende eest 3 rubla ja et neid ilma lubata wõtnud ja ka muist palka kõrwale wiinud, mis kui wargus näitab olewat, maksab ta 3 rubla trahwi.
Johan Jaanson kaebas edasi.
