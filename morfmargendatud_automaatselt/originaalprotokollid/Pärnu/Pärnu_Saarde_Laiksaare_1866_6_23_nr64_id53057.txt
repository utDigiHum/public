Laiksaare koggokonna kohhus, sel 23. Juni 1866
Kohtus ollid: Pea Kohtomees Karl Tals
Kohtomees Hans Tins
-,,- Tennis Baers
Pallus Kiwwiarro saks G. Reinwald sii assi üllekuhlatut, sel 20. Juni ollewa temma Metzas Kiwwiarro rundisisse seuke kohapeal kus karjased ei keiwat, tuld ollu, ja rohkemb kui 10 suld puud mis talwel walmis raijutut, errapolleno. Temma arwawa sii teiste töhd eijollewa kui Loigo sauniko Jurr Toffi töhd, kes temmaga rihdos ehlawa; jau ollewa selgeste nähja kui tuld pantut, nenda kui kahhes kohas tuld pantut, ja keskpaikas maapollematta jähnu.
Pea kohtomees Karl Tals, Abbi Johann Tals ja Walla-wöhrmünder Jaan Jooste, kes tuld wahtamas keino, tunnistab, sedda mohda ollewa küll kui kahhes kohas tuld pantut ja keskpaikas pollematta jähnu, ja kikke wähhemb 10 suld puud errapolleno, nenda kui 10 puidu-asseht nähja, monnis kohas woib olla rohkemb kui 10 suld puid alla.
Loigo sullane Karl Andersohn tunnistab, temma ollewa sellekord Loigo sahdus (Wädung) Metzaähres töhd teino, kus Jurr Toff selle sihkimehda mis Kiwwiarro poolt metzamehda lehb, tullo, ja kus temma küssinu, kus keino, on wastanu: "Niniti otsmas!" Nattuke aig perrast ollewa Kiwwiarro poolt suiz neino, ja arwawa temma küll, kui Toff tuld pannu.
Jurr Toff ette kutzutut, üttles, temma eijollewa tuld pannu, ja ollewa sellekord niniti otsmas keino.
Nenda kui selge tunnistus eijolle sis
Kohhus moistis: sii assi nikawa jätta, kui parremb tunnistus saab.
