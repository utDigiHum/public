Laiksaare koggokonna kohtos sel 29 Decembril 1859.
Kohtos ollid: Ado Tals pea kohtomees
Jaan Joosti kohtomees
Jürry Orraw dito.
Jaan Toots kaibab et Hans Leesment on tend kangenste pühhapäewal kõrtsis akkanud ilma asjata peksma, ni samma ka sedda poisi Jürry Nõmmikut, ja kui ta wend mitte ei olleks appi tulnud, siis olleks Hans Leesment neid wiggaseks lönud.
Tunnistajas kolmeister Willem Unifer ütleb, et Hans Leesment on neid koggoni ilma süta peksnud.
Moisteti: Hans Leesment saab selle est 20 opi witsu.
Ado Tals peakohtomees
moisa wallitsus H. Strahlberg
