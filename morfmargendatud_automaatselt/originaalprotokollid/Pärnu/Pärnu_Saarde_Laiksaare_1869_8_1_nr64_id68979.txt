Kaebas Ann Grenstein, et Ewa Koddar olla temmal 2 saealla heinamad ärra söötnud ja löönud tedda pühha paw 2 korda kappaga.
Ewa Koddar wastas küssimisse peale: kaebaja tulnud pühha päw kirriko aegas temma majase riedlema, lükkanud tedda rüssina mahha, agga peksnud ei olla mitte, temma loomad ei olla kaebaja heinama peal mitte käinud.
Arwati kõige häledega: et Ann Grenstein saab 12 tunniks ja Ewa Koddar 24 tunniks wangi selle pärrast kaebaja et pühha päw riedu teine ja kaebtav et löönud.
Selle otsusega ollid mõllemad rahhul.
