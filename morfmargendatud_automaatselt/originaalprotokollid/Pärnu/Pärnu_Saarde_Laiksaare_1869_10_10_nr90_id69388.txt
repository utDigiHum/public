Kaebas Peter Melkson, et temma pannud omma linnad likko ühhe lohhukesse sisse, mis jubba wannast aeast selle tarwis prukitud, ja temma wend Jaan Melkson pillanud wälja, se läbbi saanud temma kahjo 20 Rub. õb. sest et linnad läinud pallawaks.
Jaan Melkson wastas selle peale: temma pillanud linnad issa kässu peale wälja, - ja selle peale pannud wend Peter jälle linnad taggasi auku.
Mõistetud: et se kaebtus saab tühjaks arwatud.
