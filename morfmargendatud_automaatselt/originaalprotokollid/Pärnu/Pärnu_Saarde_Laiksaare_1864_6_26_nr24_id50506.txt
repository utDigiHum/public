Laiksaare Koggokonna Kohtus sel 26mal Junil 1864.
Kohtus ollid Pea Kohtomees Juhann Peterson
Kohtomees Jürri Orraw
Tulli kohto ette Moisa Wallitsus ja kaibas selle Magaski Müriseppa peale et temma ei olle moisa wallitsuse kesko kulnud ja roppowisiga wasta pannud, teiseks on temma omma loaga omma Obuse siamale moisa põlla peal ulla piddanut, ja Moisal sest mitte teada annud, 3mandaks ellab wasto Moisa kesko kõrtsis Poistega kellel siamale weel Passi ei polle.
Moisteti et Kohtomehed ei tea selle peal keddagi Moista.
