Astus ette Eassarust Juhan Sooper ja kaebab et ühe aasta eest on temal koplest üks kask maha raijutud, mis 12 tolli jäme on ja nõuab et Walla kohus temale selle eest kahju tasumist oma arwamise järgi mõistaks nende raijujatele.
Raijujad on Poisikse Kõrtsi Jaan Kuusk
" Soomrast Ado Sang
"Ruusi Jaan Lindmann
Eassarust Kingiseppa Jüri Reimets
Kutsuti ette Jaan Kuusk ei ole tulnud, olla Pernus wangis.
Kutsuti ette Aado Sang ja ei salga et ka raiumas olnud.
Kutsuti ette Jaan Lindmann ja ei salga ka seda raiumist.
Kutsuti ette Jüri Reimets ja ei salga ka seda puuraiumist.
Sai mõistetud: et see Juhan Sooperi kaebdus tõsi on ja need ees nimetatud raijujad seda salata ei saa, siis maksab iga üks raijuja selle Juhan Sooperile 1 Rb 25 Cop see teeb üle nelja mehe 5 Rubla ja et see esimene nimetatud, mitme kordse ette kutsumise peale pole tulnud, siis saab see kahju nõudmine ilma temata mõistetud ja ette kuulutud ja raha on 14 pääwa pärast sisse maksta, kaebaja ei ole sellega rahus.
