Kaebas Michel Abot, et temmal Tõnnis Toffi käest 8 Rub. mis naene laenanud, ehk tellinud, - Tuski kortsi reppi peal annud weel kätte, seal olnud ka Tõnnis Toff issi omma naesega.
Mõistetud: et peab ommale tunnistust nõudma.
Selle peale antis tunnistust Hans ja Peet Kurm, ka W. Tins.
W. Tins ütles üks kord 3 Rub. näinud laenanud.
Mõistetud: teiseks korraks.
