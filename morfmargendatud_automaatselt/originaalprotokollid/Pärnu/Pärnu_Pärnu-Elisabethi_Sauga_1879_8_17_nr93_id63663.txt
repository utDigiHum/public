Mihkel Prins kaebas, et Andres Lorentson - kui oma kartulete ja naerte pärast, mis Andres Lorentsoni poeg Jaan on ära tallanud, rääkima läinud - teda on peksnud ja nõuab tasumist.
Andres Lorentson wastas, et Mihkel Prins on õhtu hilja neile tulnud käratsema - ta käskinud teda tuast wälja minna, aga ei ole läinud - siis lükkanud ta teda wälja, aga ei ole teda mitte löönud.
Mõistus: Et Mihkel Prins ise hilja õhtul teise majasse on läinud käratsema ja wälja põle läinud, kui teda kästi ja löömise üle tunnistust ei olnud, saab see kaebtus tühjaks mõistetud.
