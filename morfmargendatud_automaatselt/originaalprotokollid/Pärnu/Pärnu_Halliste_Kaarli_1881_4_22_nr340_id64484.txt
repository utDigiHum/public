Hans Kirrik (Wanna Karistes) tulli ette ja kaebas et Surnud wingi walla peremees Peter Koger kolme asta eest on Kaebaja naese käest 5 Rb ja kaebaja käest 14 Rb 77 Kopik laenanud kaebaja pallub kohud et surnud Koger pärandusest saaks eestähentud wõlg 19 Rb 77 Kop sisse woetud ja kaebajal wälja maksetud tunistajaks andis kaebaja ülles Peter Lintsi ja Hans Leimann
Peter Lints andis tunnistust ta on juures olnud kui Peter Koger kaebaja naese käest 5 Rubla laenanud.-
Hans Leimann andis tunistust ta on juures olnud kui Koger kaebaja käest 14 Rb laenanud ja 77 kopik eest spiritust ja kaks pudelid pairist wotnud.-
Kaebatu naene Kärt Koger wastas kaebduse peale ta ei tea keige wähemad tunnistust eesnimetud asjas anda, ene kui ta mees ärra surnud on ta keik wõllad üles andnud aga seda ei ole mite ette tulnud et kaebatu kaebajal 19 RB 77 Cp wõlgneb.-
Peter Kobbi andis tunistust kahe aasta eest on Peter Koger Risti Kõrtsis kaebaja kätte 25 rubla waheta antnud ja kõrtsi rehknungi ära maksnud peale selle on Peter Koger küssinud kas nüüd on tassa - on kaebaja wastanud et on tassa.-
Margus Röand surnud Peter Koger naese käemees wastas kohtu küssimese peale ta ei tea keige wähemad eesnimetud kaebduse assast.-
Kui kohtu käiadel keige wähemad ennam ette tua ei olnud ühentasid kohtu liigmed endid selle otsusele:
Otsus: surnud Peter Kogra wöörmündrid Margus Röand peab Peter Kogra päranduset 8 päewa aia ses 19Rb 77 Kop wälja maksma Hans Kirrik heaks tunistuse põhjusel
Otsus : sai sedamaid kohtu käiadel kuulutud Peter Kogra pärrandajade wöörmünder ei ole Otsusega rahul pallus Apelatsioni kirja edasi kaebamise tarwis
arwatud: keik kuidas sündinud kirja panna.
Peakohtumees: Kusta Leppik [allkiri]
Kohtumees: Maert Reimann [allkiri]
       - " -        Andres Sarme [allkiri]
