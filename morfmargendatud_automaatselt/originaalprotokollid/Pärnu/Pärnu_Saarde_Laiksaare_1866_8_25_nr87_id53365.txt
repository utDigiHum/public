Laiksaare koggokonna Kohhus sel 25. August 1866
Kohtus ollid: Pea Kohtomees Karl Tals
Kohtomees Hans Tins
Abbi Johann Tals
Tullid ette Lalaste Perremees Jurr Trei ja sauna mees Willem Jootz ja sai sellele Aulik Kihhelkunda koht kirri d.d. 19. August 1866 Nr 1074 ette lohetut.
Perrast rägis Willem Joetz, temma ollewa nuid jau nikawa kui Kewwahdi 44 peiwat tenno - ja pallus las neid muid arwawa rahhaga ümber, nenda kui temma sis peiwat nuid rohkemb ei tahtwa tehja.
Jurr Trei üttles: Willem ollewa küll 44 peiwat teino. Nemmaht kui Nemmaht issi omma wahhel ei leppisit, sis
Kohhus moistis:
Neid 44 peiwat saab 25 Cp. peiwas - 11 Rubla arwatut, ja peab sis Willem wehl 1 Rubla rahha juhre maksma ja on sis rent temma maide ihst tassa.
Sii moistus sai ette lohetut ja õppuse kirri wella antut, kele peal Jurr Trei rägis; temma eijollewa rahho ja tahtwa suhrema kohtus minne - mis lubba temmal ka sai antut.
