Mihkel Loob tulli kohtu ette ja ütles:et nenda kui tema Isa,Annuse talukoha peremees N 6 al,Peter Loob on sel 1885 aastal ära surnud ja tema on tema wanem poeg ja säeduse järele pärija,siis et ta selle Annuse N 6 al talukoha peremeheks saaks kinitud.Walla Kohus järele wadates tema öiguse ettepanemist ja leidis et se öige on,sest ära surnud Annuse N 6 al talukoha peremees Peter Loob jättis 3 poega järele,wanem tema-Mihkel Loob-nüüd 44 aastad wana,töine-Juri-37 aastad ja kolmas-Jakob-30 aastad wana ja selle järele möistis:kinnitada Mihkel Loobi peremeheks N 6 al Annuse talukohase kui säeduse pärijat Isa järele ja paluda selleks kinnitamist ka Balti Domeni kohtu polest ja temale Regulirimise Akt saata ülesmärkimise järele,sest Regulirimise Aktid Kühno Walla peremeeste üle seiswad weel Balti Domeni Kohtus.
Kühno Walla kogokonna kohus.
Peakohtumees:Enn Alas XXX
Kohtumehed:MIhkel Larents XXX
Jaan Sutt XXX
