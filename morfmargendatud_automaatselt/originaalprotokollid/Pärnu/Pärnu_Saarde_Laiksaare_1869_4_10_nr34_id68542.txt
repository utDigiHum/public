eodem dato.
Lalaste Karl Andresson olli wälja ilmunud ja wastas küssimisse peale: temma olla 14 saart mis temma jurest leitud õigeusse kolmeistre kruntist saado seest raiunud koolmeistre loaga, nõnda sammuti ka need pärna puud sealt raiunud.
Tallitaja abbi Ado Tals ütles et metsas ei olla kändu leidnud, muud middagi kui agga Kastrenemme peal ühhe asseme leidnud, agga õieti ei teada kas seal olla üks inime magganud wõi olla seal lõhmusi maas olnud ja kas nemmad olla sealt wiitu.
Selle peale ei teadnud ka G. Reinwald middagi wastada.
Mõistetud: et se assi tulleb teine kohtu päw ette.
