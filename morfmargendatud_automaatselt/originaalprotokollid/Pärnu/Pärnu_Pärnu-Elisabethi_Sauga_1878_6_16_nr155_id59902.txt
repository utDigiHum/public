155. Hindrek Ainisson kaebas: Mihkel Kuutmann on temaga juba s. a. Märtsi kuus sulase kauba teinud ja lubanud Jüri paawast kunni üks nädal pärast Mihkli pääwa teda teenida, mis eest ta 30 Rbl raha ja 1 leisika rukkid palgaks pidanud saama, ka olla ta pääle 1 Rbl käsi raha saanud, aga ometi mitte tulnud. Pääle seda on ta palgast 4 Rbl wälja wõtnud.
Mihkel Kuutman ei wõinud seda wabandada.
Kohus mõistis: Mihkel Kuutmann peab Hindrek Ainisonile kahekordse käsiraha ja ka see 4 Rbl tagasi maksma, kui seda mitte ei saa täidetud, siis peab Hindrek Ainissoni juure teenima minema.
