Astus kohto ette:
Metsawaht Jakob Palm ja andis ülles: se olnud sel 20mal Januaril s. a. lainud temma Sanga mõisa juure, ja leidnud seal kolm meest pussidega käiwad kellest üks Saarde Juhhani poeg Jürri ja 2 temmale tundmata saksikut olnud, temma tahtnud siis nende käst püssid ärrawõtta agga nemmad oidnud ennast temma est ärra hemale, ja lainud wiimati Saarde Juhhan Järatsi majasse, temma metsawaht lainud kül järrele, ja tahntud ka seal nende käst pussid ärrawotta, agga mitte sanud, waid üks neist saksadest tedda ähherdanud pualloga luija, sedda et nemmad püssiga lasknud kuulnud temma mitto korda. ja teada ka sedda Sanga herra Thison ja Saarde Jürri Saarts tunnistada.
Sai siis ette kutsutud:
Sanga herra Thissun ja tunnistas sesamma et temma ka nimmetud pääwal seal Saarde heina maal pussilaskmist kuulnud, ja et seal saksakeelt rägitud, kes nemmad olnud ei teada temma mitte üttelda.
Sai mõistetud: Sedda protokoli mahhakirjutaa, ja teiseks kohtopäwaks Jürri Järats ette tellida.
