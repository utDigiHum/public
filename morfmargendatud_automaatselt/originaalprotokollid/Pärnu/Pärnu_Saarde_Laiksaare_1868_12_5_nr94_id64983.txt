eodem dato.
Peter Melkson iggatses omma issa käest sedda kahjo tassutud, mis issa hobbosed temma rukki pollule kahjo teinud.
Mats Rast watanud sedda kahju järrele ja arwas 2 külimitto rukki ja 2 külm kaero, agga et kaebaja hobbosed olla issa rukki põllule kahjo teinud saab 4 külmit taggasi arwatud.
Ado Melkson ütles et temma hobbosed ei olla mitte poea rukkis olnud, agga seal põllul heinama peal kül.
Arwati kõige häledega: Ado Melkson peab 1 külmit rukki ja sullased 2 küllimitto kaero kaebajale maksma.
Ado Melkson ei olnud sellega mitte rahhul ka ei wõtta ka õppuse kirja ka mitte.
Mõisteti: et se willi peab 2 näddali sees maksetud ollema.
