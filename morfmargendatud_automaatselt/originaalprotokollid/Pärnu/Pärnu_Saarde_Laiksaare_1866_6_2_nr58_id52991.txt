Kaibas Kasse saunik Peter Orraw, temma ollewa kewwahdi 1865 Tuhba tallohs erraleino, ja sis selle pellupeal mis taggasi jättnu, 1 willi errawottnu; aga nuid ei lubbawa Tubba teine willi külwet.
Tuhba Perremees Ado Kamdron wastas, sedda mohda ollewa küll, ja ollewa minnemwa Aasta Peter lubbanu peiwat tehja, selleperrrast temma annu keik maad kätte, aga olleiwa 2 peiwat üksni temmo, ja temma jau minnewa suwwel Peteril muidu lubbanu sauniku kätte ja Kasse tallohs errawieja. Peter ollewa temmal ninede ihst mis wottnu, 120 Cp rahha wõlgu.
Peter Orraw üttles, sedda mohdu ollewa küll
Kohtus moistis: Peter Orraw ei woi Tuhba mahpeal rohkemb külwet, selleperrast kui sii wastu sähdus, ja peab Perremehhil wõlgu 120 Cp. maksma.
