Laiksaare koggokonna kohtos sel 27 Künla kuus 1860
Kohtos ollid: Ado Tals peakohtomees
Jaan Joosti kohtomees
Jürri Orraw dito.
Tulli kohto ette Killinga walla mees Jacob Tohw ja kaibab et tend on Laiksaare kõrtsis Gustaw Saul ja Jacob Lesment läbbi wägga sõimatud.
Tunnistajas Killinga mees Rae Märt Oksermann ütleb et need poisid Gustaw Saul, Jacob ja Ans Leesment on tend ropposta sõimanut, ja Jacob Leesment kiidnut et temma tahhab weel Jacob Tohwiga ühhe tempo tehha.
Jacob Leesment tahhab salgata ja ajab keik nende teiste peale, Gustaw Saul ütleb et Jacob Leesment on ka sõimanut ja Hans Leesment on temmale need sõnnad ette ütlenut.
Hans Leesment tunnistab et Gustaw Saul ja ta wend Jacob on Jacob Tohwi soimanud, agga ta ei olle keddagi ette ütlenud.
Moisteti: Selle sõimamisse eest saab Jacob Leesment 15 opi witsu ja Gustaw Saul 10 opi witsu.
Ado Tals peakohtomees.
