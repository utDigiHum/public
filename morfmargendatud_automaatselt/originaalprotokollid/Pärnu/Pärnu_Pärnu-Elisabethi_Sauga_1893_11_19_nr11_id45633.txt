Ette tuliwad siit walla liikmed Andres Madise p. Holter Ridalõpest Koogi talu nt 13 mees ja sealt samast Koogi talu nr 15 omanik Hendrik Aado p. Koger ning palusiwad eneste wahel maha tehtud leppingud ülesse kirjutada, nimelt: Hendrik Koger on Andres Holtre eest Pärnu Willandi II j. Rahu kogu Pristawi Herrale, Pärnu linna wõla tasumiseks 8. Nowembril s.a 94 rubl (üheksa kümmend neli rubla) ära maksnud, mis peale Andres Holter selle liikuwa waranduse mis selle ülewal nimetatud wõla tasumiseks kinni olli pandud, aga peale selle wõla tasumise jälle lahti on tehtud, selle wõla ära maksjab Hendrik Kogerile annab selle 94 rubla tasumiseks, nimelt:1			punane			mära			hobune			wäärt			15			rubl		1			"			täkk			"			"			20			-		1			"			mära			"			"			15			-		2			raudassi			wankert						"			16			-		1			must			lehm						"			12			-														Kokku			78			-		
See ees pool nimetatud warandus jääb Andres Holtre kätte hoiu peale kunni tema selle wõla 94 rubla Hendrik Kogerile ära maksab nimelt kunni Jüri pääwani 1894 aastani.
A. Holter /allkiri/
H. Koger /allkiri/
Siin ees pool nimetatud inimesed on Sauga walla kohtule tuntud.
Otsus: Siin eespool kirjutatud protokoli kirjutada.
Eesistnik: M. Peterson /allkiri/
" H. Ainisson /allkiri/
Kirjutaja: J. Tomson /allkiri/
