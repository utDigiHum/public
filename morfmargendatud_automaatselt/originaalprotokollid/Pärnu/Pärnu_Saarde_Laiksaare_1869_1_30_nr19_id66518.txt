eodem dato.
Sowie Tinkus kaebas et Michel Tals olla temma tütre ommale aasta peale teenistusse wõtnud ja nüd enne aastad lahti lasknud, tahhab tallist moona tütrikule saada.
Michel Tals wastas küssimisse peale, tõssi temma lasknud selle pärrast lahti, ette ei olla leiba tüdrikule anda, 10 näddalid jäänud puuda aastast.
Leppus olnud:1			köört		1			wams		1			puud linnu		2			naela willo					kaerahha 1 R		
kätte annud 2 naela willade eest 1 kuub ja kaerahha.
Arwati kõige häledega: et Michel Tals peab kaebajale maksma
1			köört		1			wams		1			puuda linnu		1			nael willo		
tänna 2 näddali sees wälja maksetud ja tunnistusse ei pruki mitte ennam taggasi wõtta.
Selle otsusega ollid mõllemad rahhul.
