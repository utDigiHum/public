Kaebas kolmeister W. Unnifer, et Tuba Ado Kamdron olla tedda Urrisaare kõrtsis sõimanud: seaks, wassikaks, loontrusseks etc. selle peale löönud 2 korda sapaga põlwe silma otsa ja siis wiskanud wiina temmale wasto silmi - weel peale sedda tahtnud tedda russikatteka peksa, siis pallunud kõrtsimeest, et peaks rahha eest ommetigi wähha hoolt kandma. Se keelnud siis tedda ärra, - sellega ei wõida temma mitte rahhul olla.
Ado Kamdron wastas küssimisse peale: temma ei teada mitte selgeste, agga arwab et se walle on.
Tunnistussed wastassid küssimisse peale:
1 Johann Tals: tõssi, Ado Kamdron olla kaebajad sõimanud seaks, wassikaks ja lontrusseks, rabbanud ka tedda kaebajad löia, wiskanud temmal wiina wastu silmi, agga ennamiste tulnud temma peale ja ka löia ähwartanud ja temma lahhutanud weel ärra.
2. Jaan Tals: temma ei teada middagi sest temma olnud eeskõrtsis penki piale sirruli.
3. Jaan Melkson ei olla tulnud.
4. Joh. Lõhmus ei olla tulnud.
Selle peale kaebas ka W. Unnifer et temma olla 4 korda käinud siin ja Tuba Ado Kamdron ei olla wasto olnud, - tahhab omma aeg makstud.
Wahhimees ütles et igga kohtu päw olla käsku annud Ado Kamdronile.
Mõistetud: teiseks korraks.
