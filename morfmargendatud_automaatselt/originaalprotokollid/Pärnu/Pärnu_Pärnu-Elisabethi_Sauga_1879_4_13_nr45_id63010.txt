Hendrik Prints kaebas, et Jakub Sarapik see 3 rubla, mis 2 Junil 1878 sub No 154 on mõistetud, ei ole ära makstud.
Jakub Sarapik wastas, et see raha puuduse pärast maksmata on jäänud.
Mõistus: Jakub Sarapik peab see raha 3 rubla 20' Aprilliks ära maksma ja peab edespidi iga wiidetud pääwa eest, mis selle raha pärimise juures ette saaksiwad tulema 50 kop maksma.
