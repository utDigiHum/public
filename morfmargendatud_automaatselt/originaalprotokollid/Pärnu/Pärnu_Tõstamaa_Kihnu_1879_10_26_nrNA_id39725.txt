Madli Köster kaebas,et wennad Tõnis ja Kustaw wõtnud lauda ära,mis ema temale oli kinkinud.Tõnis pannud oma hobuse senna lauta,kus hobu waka kartohwlid ära oli söönud.Tõnis Köster tunnistas,et Kustaw ajanud neid Madliga wälja;üksnes wähikese sauna on tema saanud,mis ka Kustaw oli lubanud.Kustaw,esiteks ei tulnud ette ja pärast oli liig kange,nonda et ei lasknud kohtu mehi rääki ja käskis kohtu kulli maha wõtta.Kohus mõistis,et Tõnis maksab Madlile 50 kop.kardohwli kahju,aga kustaw annab wanemate majast Madlile üks laut,mis emast on lubatud,ja saab 15 napsu witsu selle kange kaeluse ja kohtu toas wastu hakkamise eest.
Pea kohtu mees Jaan Wesik XXX
abid:Jaan Arget XXX
Kustaw Sut XXX
