Kohtu ette tulli Ala Jakob Alas ja räkis,et tema ära surnud poja Jaani naene Ann olewa ütlenud et tema 30 Rubla laewa müimise raha olewa ära warastanud;tema ei olewa sedda mitte warastanud,waid maja peale ära kulutanud 23 Rubla maksudes ja mattuse peale ja 7 Rubla olewa weel järele jänud.Tema poja lapsele mitte rohkem ei sa andma waeste abiks kui 40 Rubla laewa rahast.Ja soowis,et se laewa müimise raha oleks ära jaotud sanud köige tema-Jakob Alase lastele ja se wiimane 100 rubla kõik temale,agga mitte waese lapsele.Ann,Jakob Alase surnud poja Jani naene tulli kohtu ette ja räkis:et se on(olewa)wale,et tema wana Isat on 30 Rubla raha wargaks nimetanud.Tema mees Jaan Alas,Jakobi poeg,on laewa ostnud ja tedda ehitanud ja parandanud kui ta terwe olli,ja mis Jakob Alas sedda laewa aris,siis tema sai selle laewa prahi rahad omale,agga Jaan on ka omast teenistusest ka majase raha annud,ja nüid Jakob Alas tahab köik selle laewa raha omale pärida,kui agga üksnes paljalt 40 Rubla waese lapsele,se on,Jani Alase lapsele ja pealegi ka wiimane 100 Rubla omale kõik wötta.Se laew sai 400 Rubla eest ära müidut.
Kohhus möistis:et sest 100 Rublast jäeb Jakob Alasele 65 Rubla ja Annele,waese lapse jaos,35 Rubla ja peawad ühes elama ja ühte talukoha tööd tegema.
Kihno walla kogokonna Kohhus
Pea Kohtumees:Enn Alas  
Kohtumehed:Mihkel Larents XXX
Jüri Laus XXX
Jaan Sutt XXX
