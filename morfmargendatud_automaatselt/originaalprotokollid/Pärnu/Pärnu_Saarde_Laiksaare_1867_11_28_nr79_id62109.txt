Laiksaare Koggokonna Kohhus sel 28. Nowembril 1867
Kohtus olli:
Pea Kohtomees Karl Tals
Kohtomees Hans Tinz
Tonnis Behrs
Kaebas se Marret Orraw Pihke tallost temma ollewa minnewa teisipaew kuulnud koera aukumise, ja et uks innimene temma aida juures käinud, agga pelganud waatama minna, selle pärrast et uksi olnud, temma mees olnud Wango tallos pulmas, agga kui ta ommiko waatama läinud, leidnud et Aeda uks lahti, kes allati seisnud paelaga kinni seotud, ja temma kirst mes hea uije lukkuga lahti olnud lõhhutud ja murtud, ja seal wälja wõetud ehk ärra warrastud se warrandus mes seija on üllese pantud,
2			põlle wäärt			1			1/2		1			rättig			-			50 kop		2			naela warwitud longat			1			50		9			Lama nahka			5			50					selget rahha			-			5					koko			7 R			55 kop		
Agga weel ollewa ühhe kangestükki ärra lõikanud mes 50 kuenart pik ollewa ilma mõõtmatta weel.
Agga sinna aida juure üks kepp mahha jäänud, kedda palju tunnistaks Jurri Mardiko keppi ollewad.
Agga weel ilma selleta leidnud Mart Orraw Jürri Mardiko heina kuhjas need 9 lamma nahka, walla tallitaja Ado Kamdron, ja walla wüürmänder Ado Talsi ka tunnistawad, et nemmad ollewa ka selle korra Mõisa olnud, kui se kaebaja tulnud kaebama, ja Jürri Mardik olnud kõrtsis kel nemmad ärra üttlenud et temm peab paigal seisma, senni kui se Marret Orraw selle keppi koddus ärra toob, ja need tunnistused ärra liggi toowa, agga ta on ärra põggenenud, agga nemmat on saatnud kaks mees järrel, kes tedda on kinni wotnud ja taggasi toonud, selle pärrast nemmad mõtlewa ja arwawa, et ta süi allune ollewa, ja et ka se Jurri Mardik ollewa tuntaw warras enne.
Mart Orraw, wannutud Metsa Wahhe abbi, ka tunnistab, et kuhja seis wargus olnud, ta koddos wõtnud jälgi ülles, ja jälgi wööda läinud. Jurri Mardiko kuhjas leidnud nee 9 lammenahhad, ja jället läinud otse ka Jürri Mardiko sauna juure.
Siis ollewa tamma selle kõrtsi mehhe Peet Kukk, ja Mango Meutus juur kutsunud ja nee nahhat wälja wõtnud kuhjast, agga nenda samme kes tunnistawad, sai ette wõetud ja näitatud se sarrapuu kepp mes aida ukse eest leitud, kelle peale weel tunnistusend Peet Kukk, Mango Möitus, Mart Orraw nemmat tunnistawat et se kepp ollewa Jurri Mardiko iaggo, ja ollewa mittu korda ta käes näinud selgeste.
Sai ette kutsutud Jurri Mardik, salgas et ta ej ollewa warrastanud. See kepp wõiwa olla et mõnne korra ka temma käe olnud, temma mõnda korda wõtwa mes tahhes puu kätte, ja jälle wiskawa mahha, üks kord ühhe, teine kord teise, agga nenda et õigge tunnistuse, ja märgid läbbi nähha et Jurri Mardik se warras on, sai
Mõistud
Sedda asja Keiserliko SIll kohtu kaugema mõistuse ja ülle kulamisse alla.
