Märt Lepiko kaebtuse asjas Jaak Raegsoni wasto linna koorma põletamise pärrast.
Kaebato Jaak Raegson kaebas et endised kohtumehed walet on tunistanud.- sest et kaebatu sel korral pea kohtu mehe ameti peal olnud.- ja mitte Jaan Mõrda, Peter Lepikud  ja Johan Lensini ei ole käskinud Lepiko juure linno takserima minna sest et ta mitte sel korral koddus ei olnud waid see 11 Aprillil 1878 aastal kirriko tee soitmise juures on olnud.- ja pallub kaebatu tunistajad tõeks tehja oma wälja ütlemist
Endine kohtumees Peter Leppik wastas se peale et se olla nimelt kohtu päew olnud, kui (endi peakohtumees neid peale kohtu piddamist tedda (se on tunistajad) Lepiko juure linno takserima saadnud aga mis kuu päewal seda ei teada Peter Leppik.- mite enam se pärast et ta seda ärra unustanud on. tunistaja pallub tänase päewa eest kahju tassumist 3 Rb päewa wiiwituse ja laimamise eest
Endine kohtumees Johann Lensin wastas see peale, et kaebato (endise peakohtumehe) kässo peale ühel kohtu päewal on ta Leppiko juure Lino takserimas käinud arwata et on Aprilli kuul 1878 aastal olnud.- aga nimelt kuu päewa ta ei mäleda ja palub 3 Rb kahju tassumist wiiwituse ja laemamise eest.-
Endine kohtumees Jaan Mõrd tunistas ni sama kui Peter Leppik ja Johan Lensin.- ja pallus tänase päewa eest 3 Rb kahjo tassomist päewa wiwituse ja laimamise eest.-
Johann Pillberg andis tunistust Jaak Räegson on tunistaja käest küskinud kas Perno tee on hea kus peale tunnistaja wastanud et tee on hea ja et ta minewal nädalal on Pernus käinud ja sel nädalal jälle lähab ja muud ta ei olle kulnud ega ei tea.
arwatud: eesseiswa assi tiiseks kohtu päewaks jätta siis üles andud tunnistaja Alexander Leppik ja et kohtu ette telli ja assi otsusele saata.-
Hans Röand [allkiri]  Maert Reimann [allkiri]
Andres Saarme [allkiri]
