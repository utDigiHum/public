Ette tuliwad siit walla Mokka talu nr 52 omanik Peeter Jüri p. Saarts ja Hans Hanso p. Holm siit walla liige ning palusiwad eneste wahel maha tethud leppingud ülesse kirjutada: Peeter Saarts annab oma krundist kaks wakkamaad maad /12 aast./ kaheks teist kümneks aastaks so 23. Aprillist 1894 kunni 23. Aprillini 1906 aastani Hans Holmi kätte rendi peale, ning maksab Hans Holm selle eest renti Peeter Saartsile kuus esimest aastad 1 rubla 75 kopp ning kuus wiimast aastad 2 rulba 50 kopp. aastas wakkamaa eest renti, ehitab ise senna omale mja ning kui ta sealt ära läheb wõib oma maja sealt jälle ära wiia.
P. Saarts /allkiri/
H. Ollm /allkiri/
Siin eespool nimetastud inimesed on Sauga walla kohtule tuntud.
Eesistnik: M. Peterson /allkiri/
Kohtunik: J. Rabbi /allkiri/
" H. Ainisson /allkiri/
Kirjut. J. Tomson /allkiri/
