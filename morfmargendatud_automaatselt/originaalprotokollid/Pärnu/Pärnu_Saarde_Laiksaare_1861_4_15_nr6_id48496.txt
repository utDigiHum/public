Laiksaare koggokonnakohtus sel 15mal Aprillil 1861
Meie siin allamal nimmetud kohtomehhed ja walla wöölmündid tunnistame, et meie sepärrast, et se meie endine wallakirjotaja Metsahärra E. Leukfeld ennast wallakirjotamissest on lahti üttlenud, sedda Mõisa wallitsejad Adam Friedrich Peterson temma assemelle walla kirjotajaks olleme wallitsenud, ja on ka keik walla perremehhed temmaga rahhul olnud.
Temma palk on aastas kuuskümmend rubla hõbbedad, peale sedda peawad igga perremees ja Metsawahk mõisale igga sui kaks peawa omma leiwakottiga teggema, se eest et wallakohhus mõisa hones saab petud.
Et selle kaubaga ka A. F. Peterson on rahhul olnud tunnistawad:
Koggokonnakohto Peamees Ado Tals xxx
Koggokonnakohtomees Jürry Orraw xxx
do do Jaan Joosti xxx
Koggokonna Wöölmünder Ado Toots xxx
[allkiri] Kogookonna kohtokirjotaja
