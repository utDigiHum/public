Laiksaare Koggokonna Kohtus sel 22mal Novbr. 63
Kohtus ollid Pea Kohtomees Juhann Peterson
Kohtomees Jürri Orraw
-,,- Jaan Kurm
Kaubaarro Maitze ja Mustagoe Tõnnis Kaibusse peal, et Pendi Jacob Killingast ollewa nende käest 50 Rubl Rahha lainanud, agga ei ollewa sedda selged teadust millas kette maksab, sai ette kutsutud Pendi Jacob, ja temma käest küssitud millas temma lubbab sedda Rahha erra maksa, kus peale temma wastas et temma lubbab 25 Rubl. Gauluks kette maksa, ja teine 25 Rbl tunewa Süggise 1864., Killinga ladal kette anda.
Moisteti sedda Protokolli üllesee wõtta.
