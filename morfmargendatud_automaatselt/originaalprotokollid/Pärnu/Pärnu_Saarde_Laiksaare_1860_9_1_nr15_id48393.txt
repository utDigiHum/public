Laiksaare Koggokonna kohtus sel 1mal Septembril 1860.
Kohtus ollid: Ado Tals pea kohtomees
-,,- Jaan Joosti kohtomees
-,,- Jaan Kurm kohtomehhe abbi.
Ans Tins tulleb kohto ette ja kaibab omma poja Tõnnisse peal, et temma ei olle ta jures toes olnud waid 3 neddalid ümber ulkunud; mis Tonnis Tins ka mitte salgab, ja ta issa ei olle temmal ka mitte irmo lubbanud, waid ollewa muido agga ulkunud.
Moisteti: Tõnnis Tins saab ulkumisse eest 15 opi witsad.
Moisawallitsus [allkiri]
Ado Tals peakohtomees
