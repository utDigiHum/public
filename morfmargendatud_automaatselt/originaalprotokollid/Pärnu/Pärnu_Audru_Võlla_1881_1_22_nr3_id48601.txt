Keiserlikko Perno I Kihelkonna Kohto ette pantud sundmise peäle 10 Detsembrist 1880. sub N: 2445. Perno teise Metsa Härra Michaelise kaebtuse protokolli 14nd Nowembrist, kus üles antud on et, Urruste Köster Kusuwsky arwatud Kronu Metsast kuiwi puid warastanud olewad, et metsa waht Jüri Tilk on näinud, et säält köstre ligitalt metsast kuiwi puid känno peält maha raiutud ja senna poole kooli õue peäle wiitud ja ka üks kooli pois Seppa Mardi poeg on köstre wenda näind kuiwi puid korjawad.
Kutsuti ette Köster Kusowsky ja salgab et tema pole metsast puid wargil käind, kus keegi selle üle tunnistust anda wõib, ega pole ka tema wend puid wargil käinud.
Nõuti tunnistust Prauski Härra D. Tamme poolt kuidas on seda arwata wõinud selle metsawahe Jüri Tilga wasto, et Köster puuid metsast warastanud oleks?
Annab wastuseks, et tema seda pole Metsa wahe Jüri Tilka wasto ütelnud, et köstert puu wargaks arwand, waid ütelnud Tilgale et küll järgi otsida wõib kas sealt puuid leidma peaks.
Kutsuti ette Seppamardi poeg Jüri Sepp ja ütleb et tema küll seda metsa wahti Juri Tilka näinud sääl metsa äres käiwad ja temalt küsind, kas kedagi näinud on puid toowad metsast ja tema ütelnud, et köstre wenda on näind Kuiwi puid toowad metsast, ja seda wälja ütlemist on ka Tildre Jaan Pillmann kuulnut.
Kutsuti ette teist selgemad tunnistust ei ole ja walla kohus käis sellepärast Urrustes Köstre ja Prausti Härra juures kas seält ehk selgemad otsust woi põhja leitud saaks, aga ei leitud kedagi, sellepärast
Sai mõistetud: et seda Köstert Kusowskit awalikkuks metsa puu wargaks tunnistuste läbi tehtud ei wõi saada ja ka see Metsawaht Jüri Tilk selle köstre juurest ka selle korra pole warastud puid eest leidnud ja ka nüüd jälle mitte, siis ei wöi siit kohus seda köstert Kusowskid puu wargaks mõista et seda nõutud trahwi 9 Rbl 46 koppik maksma peaks.
Pea Kohtomees Jaan Reimets XXX
Kohtomees Mart Kodasma XXX
" Hans Pirsen XXX
" Jüri Moppel XXX
Kirjutaja J. Simenson
