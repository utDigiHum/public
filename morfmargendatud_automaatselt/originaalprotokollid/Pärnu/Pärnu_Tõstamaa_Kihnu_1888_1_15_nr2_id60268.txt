Kohtu ette tulli Kihno Preestri Herra tüdrik Mari Juhkam ja kaibas,et Johann Tungen,siis kui Preestrid pole kodu olnud wõtnud kõik tua uksed lahti ja käinud oma lubaga tuades ja warastanud leiba ja kui tema sedda üleannetamat tegu keelma hakkanud,siis Johann Tungen peksnud tedda wägga rängaste.Sai Johann Tungen kohtu ette nõutud ja tema awaldas kohtu ees,et se köik kül öige on,agga et tema köht tühi olnud,siis tema otsinud leiba.Kohus möistis:et Johann Tungen üleannetumat tegu on teinud,et ta oma lubaga köik tua uksed lahti teinud ja tuades käinud ja Mari Juhkami selle üle ja leiba warguse pärast keelmise üle rangaste peksnud mis täijesti ülekohus on,-Johann Tungenile 13 hoopi witsa ja mis ka sai täidetud.
Kihno Walla kogukonna kohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Jurri Wahkel XXX
Jaan Pull XXX
