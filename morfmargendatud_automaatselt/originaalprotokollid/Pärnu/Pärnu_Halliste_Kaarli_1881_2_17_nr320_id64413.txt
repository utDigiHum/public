Willem Laarmann (Saksamoisa walitseja) kaebuse asjas Johann Prantsuse wasto adra warguse pärast olid kohtu käiad ja tunnistaja Hans Kalja sia ilmunud.-
Hans Kalja 11 aast (Uue_Karistes) andis tunistust ta on ühte adra Märt Roosi Rükki kõrre sees näinud kelle ader se olnud ei tea tunistaja.-
Kui kohtu Käiadel keige wähemad enam ette tua ei olnud siis sai arwatud: eesseiswa assi Keiserliko Wilandi Silla kohtu üle kuulamise ja otsusele saata.-
Peakohtumees: Kusta Leppik XXX
kohtumees: Hans Röand XXX
                      Andres Saarm [allkiri]
