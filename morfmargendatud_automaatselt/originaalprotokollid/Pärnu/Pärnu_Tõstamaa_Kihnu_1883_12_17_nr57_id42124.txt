Kohtu ette tulli Kühno körtsimehe Jakob Jansoni Naene Riino Janson ja kaibas körtsi wiina wölgnikude peale,et naad wiina eest wölgu on mis naad wötnud ja pole weel ära maksnud rehnungi järele.Said siis need wölgnikud kohtu ette nöutud:
1;Tomapetri Jaan Larents ja tema räkis:et Riinu Janson rehnungis tema peale üles annud wölga 3 Rubla 56 kop.,agga temal ei olewa rohkem wölga kui 2 Rubla 50 kop.Jakob Janson olnud tema pulmas ja lepinud selle wölast 2 Rubla maha ja selle järele temale enam ei jänud wölga kui 50 kop.Agga siiski kui Jakob Janson temale 2 Rubla tagasi maksab,mis ta pulmas ära kulutanud,siis ta maksab 2 Rubla 50 kop.
2;Mikku Mihkel Mättas räkis:et temal agga 20 kop.wölga on,agga mitte 42 kop.kuidas Riinu Janson üles annud.
3;Karune Enn Walm räkis:et temal mitte 2 Rubla 72 kop.wölga ei ole,waid agga 1 Rubla 60 kop.mis Riino Janson ise seletamise ajal ütlenud.
4;Aru Joob Täll räkis:et wöib olla temal 3 Rubla 13 kop.Riina Jansoni ülesandmise järele wölga on,tema sest ei tea,wöib olla jonust peast wöetud.
5;Koksi Toma Rand naene Liis ja poeg Mihkel räkisid:et nemad on oma wöla köik äraseletanud,mis neil Riinuga tegemist olli,agga Tooma wölast nemad ei teadwa midagi,et temal 7 Rubla 50 kop.wiina wölga on,sellest ei olewa Tomas midagi räkinud ja nüid on ta surnud.
6;Juhma Jakob Köster räkis:et temal mitte 4 Rubla 72 kop.wölga ei olnud,waid 3 Rubla 50 kop.ja se on ära maksetud.1 Rubla maksnud rahaga Riinu kätte,1 Rubla annud Pernus Jakobi-Riinu mehe kätte,ja 1 Rubla 50 kop.ette maksnud muu kramiga.
7;Tohwri Peter Wesikas räis:et temal mitte 1 Rubla 63 kop.wölga ei olnud,agga siiski kui Riinu Janson se 2 Rubla kätte annab,mis ta Peter Lamendi käest olewa muido kinni pidanud,siis ta maksab.
8;Arwi Jaan Allik lubas 26 kop.äramaksa.
9;Pao Jaan Wahkel räkis:et temal mitte 4 Rubla 70 kop.wölga ei ole,waid 2 Rubla 10 kop.,mis ta lubab Möisa rehnungist maha arwata laska.
10;Mikku Jakob Mättas lubas 24 kop.ära maksa.
11;Tomapetri Jakob Larents maksis 1 Rubla ära.
12;Ala Peter Sutt räkis:et temal mitte 2 Rubla wölga ei olnud,waid 1 Rubla 64 kop.,1 Rubla olewa juba ära maksnud ja 64 kop.olewa weel maksa.
13;Kurase Mihkel Sutt lubas 3 Rubla 60 kop.maksa.
14;Tungi Tenis Wesikas lubas 39 kop.ära maksa.
15;Koksi Jakob Oad räkis:et temal ei olewa mitte ühte krossi körtsi wölga.
16;Tanila Jaak Leas räkis:et Riinu Janson tema peale üles annud 3 Rubla 13 kop.körtsi wölga,agga Riinu olewa tema käest 2 korda 1/2 korteri wiina wötmise ajal raha kinni wötnud,kummagi kord 1 rubla,se teeb 2 Rubla,agga kas temal weel wölga on sedda tema ei tea,sest et Riinu pole kunagi temaga rehnungi teinud.
Et neid inimesi köiki ei tulnud mis rehnungis üles antud olli,siis jäi se kohhus teiseks korraks.
Kihno walla kogokonna Kohhus
Pea Kohtumees:Enn Alas  XXX
Kohtumehed:Mihkel Larents XXX
Jüri Laus XXX
Jaan Sutt XXX
Mihkel Kott XXX
