Laiksaare Koggokonna Kohhus, sel 5. August 1867
Kohtus ollid:
Pea Kohtomees Karl Tals
Kohtomees Hans Tins
-,,- Tõnnis Behrs
Kaibas Moisa sullane Johann Lesmend, et temma Issa Mats ja wenna Jakobi hobboset ollewa temma odrat üsna errasohnu ja errasekknu. Ilma sedda ta peawa ka selle ülle kaibama, et Jakob Lesmend, temma wend, temma juhres wabbrikus ehlawa pollu mahd pruhkiwa, aga missuggust eat selle iist ei tehwa. Temma ollewa küll käsknu, ka monnid päwat moisas tehja, aga Jakob ei kuhlwa.
Mats Lesmend wastas: temma hobbone ollewa küll wilja peal ükskord ollu.
Jakob Lesmend: temma hobbone ollewa küll peal olnu. Päwat ta ollewa küll tahtnu tehja, aga Johann ollewa wannat reinut liggi annu.
Walla wöörmünder Jaan Kurm, kes se kahju wahtamas keinu, tunnistab: temma arwawa se kahju x wakk suur ollema, aga nende oddra peal ollewa Johanni omma hobbone ka olnud.
Kohhus moistis
Jakob Lesmend peab Johannil 1 Rubla panti-rahha ja 1/2 wakk odrat maksma, ja saab selle iist et ta ilma tehnistus ümber ulgib 15 opi witsu.
