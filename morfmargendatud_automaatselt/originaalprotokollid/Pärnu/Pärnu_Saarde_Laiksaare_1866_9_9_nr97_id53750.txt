Laiksaare koggokonna Kohhus sel 9. September 1866.
Kohtus ollid: Pea Kohtomees Karl Tals
Kohtomees Tennis Baers
-,,- Abbi Hans Kamdron
Tullid ette Tennis Kollama, Lihse Tomsohn temma wennaga Endrek Thomson ja neitas Tennis neid asjaht ette mis ta sahno (vide Nr 86 Prot. von 25. August) aga ollid puhdus üks paar kindaht ja üks üh (1 Gurte).
Nenda kui nemmaht omma wahhel ei tahtsit leppida, sis
Kohhus moistis: Lihso Thomson peab Tennisel 4 Rubla taggasi maksma, ja annab Tennis temmal neid assjaht ka taggasi.
Pea Kohtomees Karl Tals xxx
Kohtomees Tennis Baers xxx
H. Kamdron xxx
Kirjutaja [allkiri]
Sii moistus sai ette lohetut ja Tennise õppuse kirri kätte antut.
