Wallawalitsuse kaebduse asjas Jaan Kowiit wastu pearaha noudmise asjas 1879/80 aasta ette ühes 6% protsendiga
wastas kaebtuse peale ta ei olle mitte saduse põhjusel üks asta prii olnud peale selle wälja tulemist wäe teenistusest.-
Kui kohtu käiadel keige wähemad ette tua ei olnud ühentasid kohtu liikmed endid selle
Otsusele: Et Jaan Kowit keige wahemad tunistust ette toonud ei olle, kunas ta nimelt wälja tulnud peab siis kaebatu walla kogu arwamisele jarrele 1878/80 aasta pearaha wälja maksma 6Rb7½  ühes 6%.-
Kohtumehed: Maert Reimann  [allkiri]
                         A Saarme [allkiri]
                         Jaak Raegson [allkiri]
