Keiserlikko Perno Sillakohto kirja peale sest 11. Augustist see aasta No 5465 sai ette  kutsutud Panso Jaan Winter ja Põhja Jüri Paugus ja päriti kas nemad selle wana Jüri Pajule on rähe tegemise puid annud, mis peale  Pansu Jaan Winter ette astus ja tunnistas et tema selle Jüri Pajule iga aasta ja niisamuti ka tänawu aasta saare puu tükka rähede tegemiseks on annud.
Niisamuti tunnistab ka see Põhja Jüri Paugus et tema selle wana Jüri Pajule on saare puiu tükka ja ka rähe warre puid rähede tegemiseks on annud.
Sai mõistetud: Keiserlikku Pernu Sillakohtu käsu täitmiseks, seda Pansu Jaan Wintere ja Põhja Jüri Pauguse tunnistust Jüri Paju räha tegemise puudeandmise üle, sest üks Protokolli ärakiri sisse saata, et nemad just toeste rähepuid selle Jüri Pajule on annud.
Peakohtumees Jaan Reimets XXX
kohtomees Mart Kodasma XXX
Kohtomees Hans Pirson XXX
Kirjutaja J. Simenson
