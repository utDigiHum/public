Laiksaare koggokonna kohtus sell 5. Jannuwar 1868
Kohtus olli
Kohtumees Hanz Tinz Tõnnis Behrs Hanz Kamdron
Tulli ette Metsawaht Hans Leid, ja kohhos küssis, miks pärrast ta on kohtu kässo wasto pannud polle Mõisa tulnud kui käsk sai antud, ja ka weel koddas on temmaga tahhetud kokko saada, mes ülle ta piddi wastost andma kudda see trahwi rahha saab ärra maksetud, mes wanna Sallatze koggokonna kohhus on mõistnud, ja mes sell 5mal Rijas kihhelkonnakohtus on kinnitud se mõistus, mes jo aulko Perno I kihhelkonna kohtu kässo järrel, ülle tarminna aija on läinud.
Selle pääle wastas Metsawaht Hanz Leid, et temma ej ollewa õiget käsko saanud, ta ollewa kül üks kord kuulnud agga ta tahtnud weskele tehjat, ja et see käsk oijete ej ollewa antud, ja et temmale põlle ka rahha olnud, tuuwa, ja et ta ej tahtwa ka seida rahha maksta.
Sai tunnistus ette kutsutud, Pea kohtomees Kaarl Tals, kes wastas ja tunnistas, et ta wõib selgeste ülles näidata, et ta essi üks kord on käsko annud ja sel Metsa wahhi abbile Juhhan Orrawal sedda käsko täijeste ärra ütlenud ja kaks korda omma poisiga saatnud, ja kaks korda perris Mõisa ehk Walla kirri on labbi käinud.
Kohhos Mõistis
et see Metsawaht Hanz Leid Essiti se rahha mes se Perno aulik I kihhelkonna kohhos isse nõuwab, ja wimatse termenne aijal piddi ja ärra maksma ja koggokonna kohtu käsko ej olle kuulnud. Peab 24 tundi tornis ollema.
