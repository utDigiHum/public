Mai Baltasar - käemees tema mees Kristjan - kaebab, et Jaan Wilter teda 9. Oktobril s.a. rusikaga ja laua tükiga nõnda hirmsaste peksnud ja ähwardanud teda sootumaks ära tappa, selle peksmise läbi olla tema haigeks jäänud ja põdenud ning pidanud tohtrirohtu tarwitama nõuab selle eest Jaan Wilteri käest 5 rbl. tohtri kulu ja rohu raha ja annab tema kohtu karistuse alla.
Jaan Wilter wastab selle kaebtuse peale, et tema Mai Baltasaart mitte peksnud ei ole waid ainult Mai Baltasare olla tema tisleri tuast m  wälja wiskanud sest et Mai Baltasar sealt laas  wiima tulnud mis Mõisawalitsuse poolt keeldud olnud.
Karl Jäär tunistab, et tema seda pole näinu  et Jaan Wilter Mai Baltasart löönud, aga Jaan Wilter olla teda (Mai Baltasart) tuast wälja ajanud ja ähwardanud teda maha lüüa kui tema tuast wälja ei lähä kui Mai Baltasar siiski mitte wälja pole läinud olla Jaan Wilter teda kaks korda kätt pidi mööda põrmandut taga vedanud ja pärast ühe kase plangiga Mai Baltasaart mööda põrmandut edasi lükkanud.
Karl Jäär tunistus awaldati.
Mai Baltasar näitas ka tohtritunistuse 16 Oktb. 1889 ette kus tunistatud on, et Mai Baltasarel param poolt kopsul wiga olla ja ka parampool õla paistetanud.
Mõistetud:
Kohus arwab otsuseks Jaan Wilter peab Mai Baltasaarele 3 rubla tohtri kuludeks maksma ja langeb 24 tunniks vangi.
Otsus awaldati ja teadustati, et 8 päewa edasi kaebtuse aega on. Jaan Wilter appeleris 17 Novembril 1889
