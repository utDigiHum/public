Mihkel Rapp kaebas, et temalt sulane Jüri Linkmann rohkem palka 12 rubla 20 kop on wälja wõtnud ja kui wooris käindu siis niipalju linu puudu olnud nõnda et 13 rubla 60 kop raha maha wõetud peale seda ühe kirwe ära kautanud mis 2 rubla ja ühe telka mis 1 rubla maksab - ühtekokku 29 rubla 80 kop.
Jüri Linkmann wastas, et see 12 rubla õige olla aga see linade raha 13 rubla 60 kop ei olla mitte tema süi läbi maha wõetud, sest ka mitmel teistel linu puudu olnud - kirwest ega telkad ei olla ka tema mitte kautanud.
Telko  pärast tunnistas Mihkel Laagus, et tee pealt tulles telka on käes olnud.
Mõistus: Jüri Linkmann peab Mihkel Rappile maksma:Ette wõetud raha			12 Rbl 20 kop		Kirwe eest			2 " - "		Telko eest			1 " - "		Summa			15 Rbl 20 kop.		
Linade üle, mis eest 13 rubla 60 kop maha wõetud, jääb mõistus tegemata, sest et see asi teiste juures ka pooleks on ja saab säält otsust arwatud, kas need linad warastud, ehk kaupmehe juurest wähem peale antud.
