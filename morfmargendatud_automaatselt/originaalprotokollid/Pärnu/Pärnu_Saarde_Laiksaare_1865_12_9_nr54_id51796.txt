Laiksaare koggokonna kohtus sel 9. December 1865
Kohtus ollid: kohtomehs Hans Tins
-,,- Tenni Baers
Abbi Hans Kamdron
Jurri Toffi kaibuse assja perrest sai sissekuzzutut Mats Tischler (vide Prottokol Nr 51) ja tunnistas: Johann Mutt ollewa sellekord karjus lenno ja Toffi koer Metzas liggi kuzznu, ja selle sama peiwas ollewa Rägi Metzawaht sii koer mahha lasno.
Jurr Toff küssimenne järgi üttles, temma palluwa selle koera ihst 50 Rubla maksu.
Kohhus moistis: Johann Mutt peab Jurr Toffile selle koera ihst 3 Rubla maksma, selleperrest kui nähja on, kui ta on ta sama metza wihnu.
Moistuse sai ette lohatut ja õppuse kirri Toffile wälla antut, aga Johann Mutt on tarwis tullewa kord ette kutzuda selleperrest kui ta tehmba eijolle siin.
