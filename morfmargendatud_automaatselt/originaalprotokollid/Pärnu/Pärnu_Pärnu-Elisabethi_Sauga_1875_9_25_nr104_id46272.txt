25" Septembril
Rätsep Maddis Jurgens kaebas et Andres Steppand on ennast temma jur öppiaks tellinud ühhe aasta peale selle kaubaga et ta peab Andressel selle aea sees ammeti kätte öppetama ja peab Andres selle aasta se öppetamise eest 5 Rbl temmale maksma ja on nende kauba teggemise käemees Walla wahhemees Laus Thomson olnud. Agga poole aasta pärrast on Andres Steppand temma jurest teenistusest ärra tulnud ja omma kaupa ärra rikkunud.
Astus ette Andres Seppan ja ütles et ta meister Maddis Jurjens on temmaga wägga walli olnud ja ei olle tedda selle kauba teggemise järrele mitte öppetanud, sepärrast on ta siis Maddis Jurjensi jurest ärra läinud.
Kohhus möistis:
Et Andres Steppan peab omma öppimise aasta täis teenima ja peab Meister Maddis Jurgens tedda illusaste öppetama et ammet selle aasta löppetusel selgeks saab, jättab agga Meister omma kohhust täitmata siis jäeb ta sest tingitud 5 Rublast ilma.
