Oskar Lacks kaebduse asjas Johann Soots wastu löömise ja peksmise pärast olid kohtu käiad ja tunnistaja Peter Lensin sia ilmunud.-
Johann Soots wastas Kaebduse peale se olla tõssi et ta (Juuti) kaebajad löönud ja räime peaga wastu nägo wissanud, aga selle pärast et ta Krop olnud ja enne kaebatu peast olla Kaebaja Juksid kiskunud.- nouab kaebatu selle eest 100 Rb. tunnistajaks andis kaebatu Mats Post kes seda näinud.-
Arwatud: eesseiswa assi teiseks kohtu päewaks jätta siis kohtu käiad ja ülesantud tunista ette telli ja otsusele saata.
Peakohtumehed: Kusta Leppik [allkiri]
Kohtumehed: Maert Reimann [allkiri]
                         Andres Saarme [allkiri]
