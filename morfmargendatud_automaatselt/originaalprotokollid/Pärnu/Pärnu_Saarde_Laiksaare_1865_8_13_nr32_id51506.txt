Laiksaare Koggokonna Kohhus sel 13mal Augustil 1865
Kohtus ollid: Peakohtomees Karl Tals
Kohtomees: Hans Tins
-,,- Tõnnis Bärs
Tulli ette soldati naine Ann Ans ja kaibas ehk ta on Jurry Nõmmikol omma rahha 34 R. hõbb. oida antud nüüd on se rahha warrastut ja temmal praego rahha wägga tarwis, ja temma woib nüüd ühtegi saada.
Jurry Nõmmik sai ette kutsutud ja selle assja pärrast wälja küssitut kelle peäle ta wastas: ehk soldati naine Ann Ans on küll temmal 34 R. oidma antut agga nüüd on se rahha temma rahhaga ühhes tükkis ärrawarrastut.
Moistetud sai: Se kahjo pooleks kandma, se on Jurry Nõmmik peab Ann Anssole 17 R. hõbb. maksma.
