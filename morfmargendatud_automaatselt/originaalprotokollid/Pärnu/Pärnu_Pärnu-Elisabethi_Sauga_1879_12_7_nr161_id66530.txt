Mihkel Kilk kaebas, et temal Karl Rabi käest weel saada on
6 wakka rukkid 18 rubl
1 leisikas willu
10 wakka kartulid
2 leisikat lõngu
Karl Rabi wastas, et Mihkel Kilgile 1 wakamaa rukkid nende 6 wakka rukiste eest on annud, willade eest lubab ta 10 rubla, kartuled olnud mädad, neid ei taha ta maksta ja lõngad lubab kätte.
Mihkel Kilk wastas, et ta seda maad Karl Rabi kääst on kui kingituseks saanud ja see eest üle kahe wakamaa 21 rubla töö raha on maksnud.
Karl Rabi wastas, et kõigist 13 rubla on maksnud.
Mõistus: Et Karl Rabi see wakamaa maad Mihkel Kilgile nagu kingituseks on anndu, seepärast ei wõi seda 6 wakka rukiste eest arwata, waid peab Karl Rabi Mihkel Kilgile maksma:6 wakka rukkid			18 rubla		1 leisikas willu			10 "		10 wakka kartulid, et üles ei ole annud, et mädad olid			10 "		Summa			38 rubla		
