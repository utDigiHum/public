Kohtu ette tulli Mihkli Matt naene Madli Matt ja kaibas,et Mart Kott on tedda roppuste söimanud ja ähwardanud peksa ja sellega on tedda kangeste kohutanud koguniste ilma süita;Mart Kotti hobune on Mihkel Matti wilja sees olnud ja Madli Matt läinud Mardi Kotti ütlema,et korista oma hobune meite wilja seest ära ja Mart Kott sellesama pärast akkanud tedda roppuste söimama ja ähwardanud tedda peksa.Sai Mart Kott kohtu ette nöutud ja ajas ta esite sedda waleks,wiimaks tunnistas,et ta äkkilise meelega sedda teinud.Kohus möistis:et Mart Kott Madli Matti ilma süita roppuste söimanud ja oma ähwardamisega tedda kohutanud,-peab 1he päwa ja öö peale torni pantud saama,et ta edespidi sedda enam ei tee,ja se sai täidetud.
Kihno Walla kogukonna kohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Jurri Wahkel XXX
Jaan Pull XXX
