18 Julil 1875
Perremees Jurri Sommer kaiwab et ta on omma perrega holetuma tö pärrast nurrisenud, agga sullane Peeter Pöllenik kes Jööpre walla mees on hakkanud issiärranis temma wasto wägga häbbemata sönnadega ja lönud tedda wikkati kannaga, selle peale lönud perremees tedda rehhaga wasto ja siis hakkanud nad wikkatidega wastamesi wehklema, peale selle on ta perremehhe teenistusest ärra läinud ja ei olle mitte taggasi tulnud ehk küll siit kohto poolt sedda temmale on sunnitud ni kaua teenistuse jada kui hasti selletud    
Kohhus möistis: 
et sullane Peeter Pollenik peab jälle omma prremehe Jurri Sommer teenistuse taggasi minnema ja selle kohto kässu wastu pannemise pärrast teenistusest ärra jädes peab ta 24 tundi kinni istuma. Pealegi nomis kohhus neid perremehhega mõllemilt poolt eddespiddi rahhulikult ellada. 
(Se protokoll Kihhel. antud 14 Agugstil 1875)
