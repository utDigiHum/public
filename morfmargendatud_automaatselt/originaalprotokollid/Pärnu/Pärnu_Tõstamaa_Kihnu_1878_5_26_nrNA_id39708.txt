Mari Matt tuli ette ja kaebas,et Kustaw Oad ähwartanud ja peksnud teda seepärast,et ta olla rääkinud,et Oad hooranud Jaani Mat naesega.Mari Matt tunistas,et tema hooramist pole näinud,aga et Jaani naene Kustawi juures maganud,on tema rääkinud.Kustawi Oad naene Liis tunnistas,et Jaani Matt naene ise seda juures magamist hoora juttuks teinud.Kohus möistis,et Mari Matt juures magamise juttu kahe abielu wahel rääkis,teda üheks ööks torni kinni,aga Jaani naese selle juttu hoora juttuks tegemise eest üheks ööks ja pääwaks torni,mis kohe sai täidetud.
Kogu konna kohtu nimel:
Kohtu pea:Jaan Wesik XXX
Abid:Jaan Arget XXX
Kustaw Sutt XXX
Kirjutaja:A.Schuman/allkiri/
