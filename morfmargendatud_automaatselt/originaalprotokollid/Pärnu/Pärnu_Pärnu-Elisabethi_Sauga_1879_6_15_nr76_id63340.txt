Jakub Tomson kaebas, Hendrik Wihmanni hobune on kewadel kaks pääwa tema juures kinni olnud ja nõuab nende pääwade eest söötmise kulu ja nüüd on Hendrik Wihmann kaks korda temal kohtu ette wastu tulemata jäänud.
Hendrik Wihmann ütles, et see hobune on kinni wiidud ilma et midagi paha oleks teinud.
Mihkel Rabi, kes seda kahju waatamas käinud, ütles et paha kül ei ole tehtud olnud, aga hobune on ikka orasel olnud.
Mõistus: Hendrik Wihmann maksab Jakub Tomsonile kahe pääwase söötmise eest talurahwasääduse § 1066 järgi 15 kop ja kahe wastu tulemata päwa eest 60 kop kokku 75 koppik.
Kirjutaja J. Weske
