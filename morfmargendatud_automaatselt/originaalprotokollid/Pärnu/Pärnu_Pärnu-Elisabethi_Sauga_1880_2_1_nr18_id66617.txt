Mari Tammekänd kaebas temal on öösel 27 Decembril m. a. sängi peaalusest 7 rubla raha ära kadunud ja sel õhtul on Jõepra poisid Mihkel Lembet ja Jaan Tammik tema sängis maganud ja arwab nüüd Mari Tammekänd, et nendest üks tema raha on ära wõtnud.
Mihkel Lembet ja Jaan Tammik ütlesid endid kül sel öösel sääl olnud aga ei tääda nemad sest rahast midagi.
Ann Lasn tunnistas, et Mari Tammekänd just tema nähes oma raha 7 rubla senna woodi pääluse alla on pannud.
Mõistus: Ehk seda kül selgeste tääda ei ole et nemad seda raha oleks wõtnud, aga et see sel korral kadunud ja nemad kui hulkujad sääl wõeras majas olnud peawad Mihkel Lembet ja Jaan Tammik kumbagi 3 rubla 50 koppik ära maksma ja et öösel hulkunud läheb kumbagi 12 tundi wangi.
