Laiksaare Koggokonna Kohtus sel 13 Dezember 1867
Kohtus olli
Pea Kohtumees Karli Talz
Kohtumees Hanz Tinz
Tennis Behrs
Tulli ette Nepste Johhan Peterson ja kaebas et temma tüddruk Ann Andresso enne leppitud aastad on ärra paggenenud ehk ärra läinud omma tenistusist, ja on jubba 2 kuud aega kaddunud.
Sai ette kutsutud tüddruk Ann Andresson ja wastas et ta on saanud peksa perremehhe käest. Selle pärras ta ollewa ärra läinud. Ja ej tohhi ennam taggasi lähha.
Tulli ette perremees Johan Peterson ja wastas et see on walle ja selle pääle Ann Andresson ej wastanud sõnnagi.
Kohhos Mõistis
Ann Andresson peab 24 tundi tornis ollema.
