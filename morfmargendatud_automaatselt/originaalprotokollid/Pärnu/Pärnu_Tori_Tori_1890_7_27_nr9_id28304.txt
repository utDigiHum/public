Ette olid tulnud Kalmoja Peet Tilka waeste laste käemehed Tõnis Enn ja Mart Pool,kui ka lesk Kadri Tilk ja tema täie healiseks saanud poeg Mihkel Tilk.
Käemehed Tõnis Enn ja Mart Pool andsid üles et äras.Peet Tilka poeg Mihkel Tilk nüid 21.aastad wanaks saanud,sellega siis juba täielik oma wara walitseja ja ütlesid lese Kadri Tilkale ette,kes seniajani seal talus perenaise ametid pidanud,et tulewast Jüri pääwast s.o.sest 23.Aprillist 1891 selle Kalmoja talu pidamise oma täiehealise poja Mihkel kätte,kõige waese lapse warandusega mis sel ajal kui Peet Tilk ära surnud,seal talus olnud,pidada andma.
Kadri Tilk Torist andis üles et tema kuni seie saadik seda talu pidanud,tal olla ka weel weikesi lapsi,kes kaswatada tahta,sellepärast ei lubada tema seda talu weel mitte poja Mihkle kätte anda.
M.Pool/allkiri/  Tonis Enn XXX ei oska kirjutada  Mihkel Tilk/allkiri/  Kadri Tilk XXX ei oska kirjutada
Otsuseks:Seda protokolli kirjutada
Peakohtumees:Tõnis Tehwer/allkiri/ 
Kohtumehed:Jaak Tommingas/allkiri/  Jüri Martson/allkiri/  H Kask/allkiri/  Jüri Järberson/allkiri/ Peet Tammann/allkiri/
