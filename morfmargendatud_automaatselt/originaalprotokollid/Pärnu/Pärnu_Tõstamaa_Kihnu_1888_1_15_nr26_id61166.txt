Kohtu ette tulli Palu Jaan Palu ja kaibas:et soldat Pao Jaan Wahkel on tema maa pealt,agga Jani Wahkeli õue peal kaswanud ühe pajo puu maha raijunud.Jaan Wahkel sai kohtu ette nöutud ja tema räkis:et se ei ole mitte Palu Jani maa,waid üks kölbamata kroonumaa tükk.Selle peale tulli Palu Danil Palu ja ütles:et se on tema maa tükk ja tema on istutanud pajud senna.Wiimaks weikese waidlemise järele-leppisid ära.
Kihno Walla kogukonna kohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Jurri Wahkel XXX
Jaan Pull XXX
