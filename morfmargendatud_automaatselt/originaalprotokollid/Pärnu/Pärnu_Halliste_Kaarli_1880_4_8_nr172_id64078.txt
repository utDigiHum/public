Jaan Kaberel kaebtuse asjas Ott Mägi ja Peter Kalja wastu raha warguse pärrast ollid ulesantud tunnistaja ja kohtu käiad sia ilmunud.
Hendrik Wenneline andis tunnistust ta ei olla mitte muud näinud kui seda Birki wõlla tähte keda Peter Kalja Ott Mägi kätte andnud.
Kui kohtu käidel keigewähemad ennam ette tua ei olnud, siis uhentasid kohtu liigmed n endid selle
Arwamisele: Et eesseiswa assi kaela kohtu wääriline (Krimineller Natur) siis saab tedda Keiserliko Willandi Silla kohtu ülekulamise kui ka otsusele saatetud.
arwatud: keik kuidas sündinud kirja panna.
Kusta Leppik XXX  Andres Saarme [allkiri]
Hans Röand XXX
