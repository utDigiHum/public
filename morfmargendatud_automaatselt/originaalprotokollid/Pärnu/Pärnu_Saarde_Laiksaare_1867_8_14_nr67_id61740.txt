Laiksaare Gemeindegericht, den 14. August 1867
Gegenwärtig:
Gemeindegericht Vorsitzer Karl Tals
Beisitzer Hans Tins
GemeindeAltester Ado Kamdron
Es werden vorgerufen die Laiksaarschen Rekunten der Reihefolge nach und erklärten:
Nr 5. Jaak, Ados Sohn, Orraw: Er habe sich mit seinem Wirtsen berechert und sonst an Niemand ein Gebiete weiten eine Anforderung.
Nr. 6. Tõnnis, Anz's S., Liit: Ist inzerischen auf Grund des Attestes des Gudmannsbachsche besterische Prediger Tärne d.d. 31. October 1866 Nr 86 und des Rehriges Eines Kaiserlichen I. Pernauschen Kristengericht d.d. 31. März 1867 sub Nr. 531 sämmlich als Gebieteschulmeister umgeführt worden.
Nr 7. Johann, Kaspers S. Tamsohn: Er habe sich mit seinem Wirtsen terechent und habe an höhige Gemeindegleider Kann Anforderung.
Nr 8. Ado, Jaan S. Kasse: Wird von der Gemeinde als Ermähr er sowas Kränklühen Vaters Jaan Kask und Mutter Liso bestellt, da dessen älterer Bruder Jurri totde Krüggel, dessen jungerer Bruder Endrek als 13 jährigen Knabe zur Unterhaltung seiner Eltern und sowas Krüggelsafte Bruder Jurri sich nicht wigert.
Nr 9 Tõnnis, Hans S. Ruul: Er habe an die Gleider seiner Gemeinde Kreie Berderungen. Scheweg sei er dagegen dem Possa Peet 2 St. Roggen, Kottisoo Jurr 2 Lt. Geiste, Sillaotsa Jurr 1 Tschetwerk Roggen, 1 1/2 Tschetwerk Hafer und 1/2 Tschetwerk Geeste, dass bleibe sein Vermögen bestehend in 1 Pferd, 1 Küh, 2 Stärkbar, 2 Schafe, welcher sich bei seinem Weibe befinen, zu gesenden.
Nr 10. Tõnnis, Johanns S. Koddar: habe sich sowol mit seinem Wirtsen als mit allen Gemeindegliedere aus einandergesacht.
Danach wurde: Versagt.
Folgende Rekruten Einer Keiserlichen Rekrutenempfangcommision vorgestellen:
Nr 5			Jaak			Ados S.			Orraw		Nr 7			Johann			Kaspers S.			Thamson		Nr 9			Tõnnis			Hans Sohn			Ruul		Nr 10			Tõnnis			Johanns S.			Koddar		
Nr 6 und 8 indes der vorangeführten Gründe wegen von der Rekrutenstellung zu behreien.
Gemeindegerichts Vorsitzer Karl Tals
-,,- Beisitzer Hans Tins
Gemeindeaeltster Ado Kamdron
Gemeindevorsteher Jaan Kurm
-,,- Ado Tals
