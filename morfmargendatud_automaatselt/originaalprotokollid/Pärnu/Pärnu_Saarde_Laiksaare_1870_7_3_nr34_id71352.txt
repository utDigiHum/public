Kaebas Tõnnis Pridik et temma õdde teeninud enne surma Jürri Orrawad 7 näddalid, - tahhab sedda palka makstud saada.
Jürri Orraw ütles et kaebaja õdde teeninud tedda 2 näddalid, kolmanda näddali läinud Taali kohtu ja selle peale surnud ärra.
Tunnistus: Peet Kuk Surjust.
Mõistetud: teiseks korraks.
