Õiso-Kaarli mõtsa härra Joh. Limberg tulli ette ja kaebas: Hans Kaur on Saate mõtsa jao piiri seest 17 kuse roowikud warastanud ja nimelt:1 roowik on 			1½ tolli jämme ja 			12 jalga pikk		6 roowikud on			2         - " -			12      - " -		4         - " -         			2½      - " -			15      - " -		6         - " -			3          - " -			15      - " -		
taksi hinna järele tulewad need nimetud roowikud 1 Rb 70Cp maksma aga et naad warastud, nouab kaebaja nende eest 3 kordne hind se on suma 5.10 Kop.. Kaebaja pallub kohud seda 5 Rb 10 Cp sedamaid sisse nouda kaebatu käest, ja kaebajal wälja maksa.- tunistajaks andis kaebaja ülese Hendrik Taklaja kes seda Jaan Liblikul räginud, et Kaebatu ülesantud roowikud moisa metsast toonud.-
Hans Kaur wastas kaebduse peale ta ei ole mitte eesnimetud roowikud warastanud, waid on 1881 aastal kewade oma mõtsast rowikuid raidunud ja sel 4ndal Augustil s.a. neid oma poisi Hendrik Taklaja läbi wälja tua lasknud muud ta ei tea
Hendrik Taklaja andis tunistust, et see on tõssi et kaebatu tunistajad käskinud neid roowikud wälja wedada, mis naad kahekeisi raidunud, kas see moisa ehk kaebatu mets olnud ei tea tunistaja mitte, muud ta ei tea.
Kaebatu wastas see peale leppa roowikud, on naad tunistajaga ühes omast leppikust raitunud, see woib olla ehk tunistaja (Hendrik Taklaja) kiusu pärast moisa metsast need roowikud raidunud, sest naad on nii mitto korda kässo wastu panemise pärast tüllis olnud.-
Jaak Liblik andis tunistus. Hendrik Taklaja on räginud tunistajal, et naad kaebatuga moisa motsast 17 kuuse rowikud raidunud ja Taklaja on palunud et teda mitte senna sekka ei saaks woetud seest see on halb kui meie oma peremehega tülise lähame.-
Hendrik Taklaja tunistas wiimaks et ta on perremehe (:kaebatu:) Kässo peale moisa metsast 17 kuuse roowikud raidunud
Hans Kaur kaebatu wastas tunistuse peale, ta ei ole mitte oma poisi Hendrik Taklajad moisa metsa roowikuid raiduma saatnud, waid käskinud neid roowikuid oma metsast ärra tua, mis ta kewade maha raiuda lasknud
Hans Widik walla wöörmünder andis tunistust ta on metsa hära Limbergi noudmise peale need üles andud kaebatust warastud rowiku kande ja rowikude otse ühte passimas käinud, aga neid ei ole mitte üks rowiko ots känuga ühte passinud waid kirwe kirjad mis laastutest ja roowiku otsatest näha olnud, - on olnud, üks ainus oks on ühe roowi ladwaga uhte passinud.-
Metsa waht Hans Anni tunistas niisama kui Hans Wiidik -
Hans Kaur pallus et kohus saataks kaebatu metsa järele waatama, sest et tal on nimelt roowikud mwtsas keda ta käskinud oma poisi Hendrik Taklajad kodu tua, see woib olla et pois neid südame täiega moisa metsast on raiunud ja oma roowikud metsa jätnud, pois Taklaja on kaebatud mittu korda hähwartanud, kelle üle ta Els ja Leena Urt tunistajaks andis.-
Hans Widik wallawöörmünder andis tunnistust kaebatu metsas on kuiwad roowikud maas olnud moned neist on 7 tolli jämed ja 8 jalga pitkad olnud ja moned peenemad
Kaebatu tunistas nisamma kui Hans Widik
arwatud : Eesseiswa assi teiseks kohtu päewaks jätta siis üles antud tunistajad ja kohtu käiad ette telli ja assi otsusele saata.-
Peakohtumees: Kusta Leppik [allkiri]
kohtumees: Jaak Raegson [allkiri]
      - " -         Andres Saarme [allkiri]
