Kohtu ette tulli Äppe Jakob Ottenson ja kaibas:et Abrami Jakob Laus ähwardanud tedda peksa ja ajanud tedda taga kuni tema majani teist teed et wastu saada,ja nendasama ähwardanud tedda laewa peal peksa,ja on ka tedda Rotsi külas peksnud ja sest wöib tunnistada Tomas Wesikas.Sai Jakob Laus kohtu ette nöutud ja tema räkis:et tema midagi ei tea,tema pole midagi teinud.Sai tunnistaja Tomas Wesikas kohtu ette nöutud ja tema räkis:et tema midagi ei tea,egga pole näinud.Agga wiimaks sai awalikkuks,et Jakob Laos on laewa peal Jakobi Ottensoni ähwardanud ja need armid mis Jakobi Ottensonil olli,et ta tedda olli peksnud.Kohus möistis:et Jakob Laos selle peksmise ette peab Jakob Ottensonile 2 rubla maksma ja selle ähwardamise ette 1 pääw ja 1 öö torni,mis ka Jakob Laus 7 Mai 1891 a.täitma peab.
Kihno Walla kogukonna kohus.
Peakohtumees:Mihkel Arget XXX
Kohtumehed:Jurri Rand XXX
Jakob Uetoa XXX
