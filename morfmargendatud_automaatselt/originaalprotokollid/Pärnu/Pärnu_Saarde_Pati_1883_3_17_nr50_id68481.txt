Tulli ette Margus Rist ja andis üles tema elumaea kambretega põlenud 9. Märtsil s.a. ära sootomaks kõige kraamiga mis temal ja ta rentnikul ta sees olnud,- tema tule hakkatusest mitte ei tääda seeperast et olla Jäärja wallas teenistuses.
Margus Risti talu rentnik Jaak Bernard wastab: tema ei tääda mitte mille wiisil tuli lahti pääsenud, seeperast et kodunt ära olnud wabrikus haige naisele rohtu toomas. Kui tagasi tulnud siis olnud juba tuha hunik maas
Rätsep Alexander Siemann wastab: tema ei teada mitte kust tuli lahti pääsenud tema olnud kodu, rentniku haige naine, maea peremehe ema ja 16 aastane karja tüdruk, paar puu halukest pölenud wee süendamise katla all ja tema süitanud ahjus puud ka pölema ja siis läinud kambre palwet pidama, poole tunni perast tüdruk läinud kööki ja kutsunud teda kuulama mis pragin see olla, tema läinud wälja waatama ja siis käinud tuli tare pealt mööda korstnad ülesse kattusse.
Ano Rist nõumehega wastab: tema ei tääda ka mitte kust tuli lahti pääsenud et tare ärapõlenud,- kõik kraam mis neil olnud jäänud sisse sest see hakkatus olnud wäga järsku.-
Margus Rist ütleb: tema kraam mis ära põlenud olnud wäärt - - 200 R
Rentnik Jaak Bernard ütleb: tema kraam mis ära põlenud olnud wäärt - 300 R
Rätsepp Alexander Seimann ütleb tema kraam mis ära põlenud olnud Wäärt ---- 40 R
Möistetud: et see protokoll Keiserliku Perno Sillakohtu parema järele uurimiseks ja otsuse andmiseks läkkitada.
Peakohtumees: H.Miländer
Kohtumees: J.Bernard XXX
                       P.Puck XXX
