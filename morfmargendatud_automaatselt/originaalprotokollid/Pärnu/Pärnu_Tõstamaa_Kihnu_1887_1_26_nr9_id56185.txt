Tönise Juri Larents ja Peter Ottenson tullid kohtu ette ja kaibasid,et poisid Juri Wesik,Peter Wesik ja Tomas Wesik on Sangu saare peal käinud ja paha teinud ja nimelt:pöösad ära raijunud ja mere kannud,nende majas tule teinud ja mörra waijad katki raijunud ja öue peale weel oksad maha jätnud,püssi kuulid ära wiinud ja tina ja pastla nöörid ja palusid,et need poisid noomitud saaksid,et naad edespidi ni sugust pahandust enam ei tee.Said poisid kohtu ette nöutud,Juri Wesik,Peter Wesik ja Tomas Wesik ja nemad räkisid:et nemad pole mitte midagi paha teinud ja ka midagi ära wiinud.Kohus möistis:noomida neid poisid strahwi ähwardussega,et naad eddespidi enam niisugust paha ei tee.
Kihno Walla kogukonnakohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Juri Wahkel XXX  Jaan Pull XXX
