Tulli se Abia Perremees Johann Tetzhoff sia kohto peäle ning andis ülles et temmal meie Walla sullase Jaan Baroni käest 10 R.B. sada ollewed.-
Jaan Baron astis ette ja andis ennesest sedda tunnistust et temma mõnne aasta Johann Tetzhoffi käest üks Hobbone ostnud, mispeäle temma kül 10 R.B. wõlga jänud.-
Jaan Baron lubbas se rahha Johann Tetzhoffi otamisse järrel Märdipäewani selsammal aastas ärra maksa.-
Johann Leppik [allkiri]
