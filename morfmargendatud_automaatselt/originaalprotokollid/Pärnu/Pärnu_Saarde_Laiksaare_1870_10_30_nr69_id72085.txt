Tunnistussed ütlessid:
1. Kadri Orraw: üks kord temma tulnud heinamalt piir olnud Hans Tins 3 lehma sees, se on kaeras muud ei teada.
2. Marri Orraw: üks kord nännu temma Hans Petersoni muist lammid kaeras, mis kohhe wälja aeanud.
3. Ado Krischfeldt: 12 aastad wanna: temma ei olla muud nännu kui et üks kord H. Petersoni täk läinud kaera, karja tütre ja ei olla wälja aeanud, - selle peale tulnud kaebaja naene kaera lõikama ja aeanud walja.
4. Mikolai Ilwes: 10 aastad wanna: ei teadnud middagi kõnnelenud.
5. Lieso Kosenkranius: ei olla tulnud.
Mõistetud: teiseks korraks.
