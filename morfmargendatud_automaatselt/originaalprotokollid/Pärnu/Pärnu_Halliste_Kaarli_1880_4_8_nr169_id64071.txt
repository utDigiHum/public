Suurnud Märt Reimanni wöörmündride Jaan Reimann tuli ette jaa kaebas Lõhawerre walla perremehe Märt Lohu käes on järrelseiswad asjad. Kaebaja paluta et järrelseiswad asjad, kaebatud saaks sunitud wälja andma -1 Wanker		1 reggi		1 padda ja		hobuse riistad		üks kop issa Johanni käest		
Märt Loho wastas kaebtuse peale se olla tõssi et ta need eesnimetud asjad (surnud Märt Reimanni) käest saanud oma wõlla eest, keda naad ni kaugel ärra õientanud, et neil ühe teise käest keige wähemad noudmist enam ei olle.
Surnud Märt Reimanni naene Marret andis tunnistust et ta mees on Märt Lohu käest 25 Rb laenanud ja siis selle raha eest eesnimetud asjad annud. ennast ni kaugel ärra öientanud et neil keige wähemad noudmist ühe tõise käest enam ei olle.
Wöörmündri Jaan Reimanni pallumise peale sai eesseiswa assi poolele jätetud se pärrast et teised wöörmundrid Märt Lohu wälja jäänud siis sai
arwatud: teisel kohtu päewal wöörmündrid Märt Lohu, Jaan Reimann ja kaebatu Märt Lohu ette tellida ja assi otsusele saata.-
Kusta Leppik XXX
Andres Saarme [allkiri]
Jaak Raegson [allkiri]
