Kohtu ette tulli Maruse Jakob Karjam ja kaibas,et Wöhka Danila Leas aijast elajad läbi tulnud ja tema kaera pöllu wägga ära tallanud mis üle tema suurt kahju saanud ja nöudis,et tema wöttab Danila Lease pöllust nenda palju pöldu asemele kui palju temal raisatud on.Kohhus möistis:et kui Danil sellega rahul ei ole,et omast pöllust nenda palju pöldu Jakob Karjamile asemele annab kui palju kahju olnud,siis peab 1/2 waka kaeru Karjamile maksma.Daniel Leas olli pöllu wahetamisega rahul.
Kühno Walla kogokonna Kohhus
Pea Kohtumees:Enn Alas XXX
Kohtumehed:Mihkel Larents XXX
Jüri Laus XXX
Mihkel Kott XXX
