Märt Kahu (rätsep) tulli ette ja kaebas Jaan Räim on 1878 aastal kaebaja käest 5 Rb laenanud, ja weel peale selle weel rätsepa töö eest 140 Kop wõlgneb, kui kaebaja mitme kordse noudmise peale eesseiswa sumad ärra ei taha maksa palluda kaebaja kohud sundida ees nimetud 640 Cp kaebatud kaebajal wälja maksma.
Jaan Räim wastas kaebtuse peale ta ei olla mitte 5 Rb kaebaja käest laenanud aga töö palk 140 Cp on kaebatul kaebajal maksa. -
Reet Räim andis tunnistust ta ei olle seda mitte nainud et Jaan Räim Märt Kahu käest raha laenud.-
Leena Lont tunnistas nisama kui Reet Räim
Jaan Mõrd tunnistas nisama kui Reet Räim.
kui kohtu käiadel keige wähemad enam ette tua ei olnud siis ühentasid kohtu liikmed endid selle 
Otsusele: Putuliku tunistajade põhjusel saab Märt Kahu oma 5 Rb nõudmise asjas rahul nomitud. isse tunnistanud (Jaan Räim) et ta 140 Cp rätsepa töö eest wolgneb Märt Kahul peab ta Wiimne suma sedda maid wälja maksma
Otsus sai kulutud Jaan Raim maksis Mart Kahul 140 Cp sedamaid wälja. peale selle tegid weel seda leppitust et
Kohtumees: Maert Reimann [allkiri]
     - " -          Andres Saarme [allkiri]
     - " -          Hans Röand XXX
