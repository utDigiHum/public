Kaarli walla perremees Jaak Räegson tulli ette ja kaebas, ta olla Märt Leppikul sel 17 Januaril s.a. linno müünud, et kaebatul se kord mitte woimalik ei olnud linnade eest raha wälja maksa, olla ta siis üks kwitung wälja andnud, et ta (kaebajal) 100 rubla wõlga linnade eest sel 2. Februaril  s.a. wälja maksab.
Et Kaebaja ni mitme kordse nõudmise peale sedda 100 rubla ei olle wälja maksnud palluda kaebaja kohhud et kaebatu saaks sunnitud nimetud raha seddamaid wälja maksma.
Arwatud: Eet kaebato wälja jänud siis kohtu käiad teisel kohtu päewal ette telli ja asi otsusele saata.
Peakohtumees: asemel kõrwalistnik Jaak Mõrd XXX
kohtumehhed Johann Lensin [allkiri]
                          Peter Kogger XXX
