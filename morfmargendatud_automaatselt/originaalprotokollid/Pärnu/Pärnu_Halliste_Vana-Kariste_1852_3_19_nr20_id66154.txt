Kaebas Endrik Ihles, et perremees Jaan Sepp temmal ei lubba sedda sullaste tarret, mis Endriku issa omma jõu ja nõuga ehhitanud, seält ärra  wia.-
Perremees Jaan Sepp kostis, et temma küll ei kela sedda maja ärra wia, kui temma tõeks teeb, et tallu selle tarre jures middagi polle awwitanud.-
Astus ette tunnistaja Sossi Enn Lussik ning tunnistas, et temma sedda selgeste näinud ja ka teab, et Endriku issa sedda sullaste taerret seppiliste abbiga, kes temmal seppüst lasksid tehha, ülles ehhitanud, ning et tallu selle tarre jures middagi polle teinud.-
Sedda samma tunnistust andsid Enn KIwwi ning Kullimatsi Jürri Mäggi.-
Mõistis kohhus, et Endrik Ihlessel lubba on se tarre ärra wia, kui perremee temmaga mitte ei leppi ning temmale selle maja eest middagi ei pakku.-
