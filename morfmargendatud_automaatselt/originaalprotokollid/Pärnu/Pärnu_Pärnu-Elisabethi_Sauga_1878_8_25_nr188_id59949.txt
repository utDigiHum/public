Ette astus Aabram Holter ja kaebas: Tema hobune on Jüri Tohwre käes 5 pääwa olnud sõitus ja nõuab 1 1/2 Rubla pääwas.
Jüri Tohwer ütles kõigist 3 pääwa olnud pühapääwast kunni teisipääwa õhtuni ja olla temal kaup olnud pühapääwa eest 60 ja järgmise kahe pääwa eest à 70 kop maksta.
Mõistus: Et Tunnistust ei olnud, kes nende kaupa selgeste oleks täädnud, seepärast arwati kohtu poolt nende nõudmised pooleks ja on siis Jüri Tohwrel Aabram Holtrele 375 kop maksta.
