Andres Petermann Wana Pärnust utli ette ja kaebas, et tema 1884 aastal Mai kuus 12 päewa on Hans Kommendega leppinud, et Hans Kommende tema wõla 24 rubla selle sama aasta sees pidanud ära maksma aga seie maani mitte maksnud ei ole ja nõuab seda ühes wie aasta intressiga kokku 30 rubla.
Hans Kommende ütleb selle wõla õige olewat ja palub sellega kannatada, et kahe kuu aea sees muist sellest tahab ära maksta.
Andres Petermann ei luba selle maksmisele pikendamist.
Mõistetud:
Hans Kommende peab wõla 30 rubla Andres Petermannile kahe kuu aea sees ära maksma.
Ette öeldud ja kaebtus järg tutawaks tehtud.
