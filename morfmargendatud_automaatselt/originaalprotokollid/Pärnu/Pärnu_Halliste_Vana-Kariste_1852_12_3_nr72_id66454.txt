Metsahärra von Brocki kirja järrel sest 18 Oktobrist № 160 said need Koiksare sullased Hans Kõll ning Märt Kink sia Kohtu ette kutsutud, ning neilt küssitud, kas nemmad on Krono metsas 9 kuuske ärrakorinud?
Kostsid mõllemad et nemmad omma perremehhe Hans Lohhusoni kässo peäle kül 9 kuuske ärrakorinud, ning et perremees ned kohhe omma linnopuie peäle pandnud.
Perremees Hans Lohhuson tunnistas, et temma omma sullastelle sedda käsko mitte polle andnud ning et temmal ka kusekoske waja ei olle olnud.
Se peäle tunnistasid Hans Kõll ning Märt Kink, et nemmad hendale õitselisse maja tahtnud tehha ning selle tarwis need puud ärra korinud.
Mõistetud
Eesseisaw Protokolli Kirri metsa Härra von Brocki kätte sata.
