Arasurnud Sauga valla Saarde talu rentniku Mihkel Saardsoni päriate eestvõtja Mihkel Mõttus ja Johann Koger ette tulnud ja teatanud et nemad 1885 aastal, kui Jaan Hints Mihkel Saardsoni lese Mari enesele naeseks - wõttis on lubanud Saardse talu kohapidamist, kõige selle warandusega, mis seal protokolli järele 21 Novembril 1885 on ülesse kirjutatud, Jaan Hintsu pidada on lubanud, kunni päriad ealiseks saavad. Et aga 1887 aasta sügisel naene Mari ära surnud, tahab Mihkel Mõttus selleaegist tingimist muuta, nii et Jaan Hints selle varanduse kindluseks ja selle eest ühte jäu hinda ja nimelt kolmsada rubla /300rubl/ sisse maksab, mis Mihkel Saardson  pärijate heaks saab kasude peale pandud. Üle üldine varandus oli 1885 aastal 569 rubl 22 kop. senna juure tuleb veel arwata 23 rubl kautsjoni raha nii et üle üldine summa on 592 rubl 22 kop. Selle peal võlga 109 rubl ja isa Ain Saardsonile 30 rubl Hendrik Kogerile wakk kaeru 2 rubl kokku 141 rubl. nii et üleüldine waranduse summa on 451 rubl 22 kop. peale 300 rubla sisse maksu jääb varandusest Jaan Hintsu kätte 151 rubl 22 kop eest ja jääb Jaan Hints Saardse talu pidamisele, kuni pärijad easeks saavad, nende tingimistega mis linna valitsus selles asjas ette paneb. Jaan Hints annab pärijatele tarwilist ülesse pidamist ja seisuslikst kaswatamist ja on tema Jaani Hints kogus seda talukohta kõigesti heas korras pidada.
Leppijad: M. Mõttus
Juhan Koger
Jaan Hints
Leppijad on Sauga wallakohtule tuntud
Eesistnik Hans Kärats
Seega tunnistame oma all kirjaga et siin protokolis nimetatud wõla olen kätte saanud
H, Roger 2 rubl
