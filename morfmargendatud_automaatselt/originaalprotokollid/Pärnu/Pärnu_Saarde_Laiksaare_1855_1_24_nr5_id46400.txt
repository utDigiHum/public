Kohto lubbaga tulli ette Kadri Wachter ja pallus et kui temma on nenda sandi juttude lebbi, mis Mats Leinasaari päle on aetud rapi maksnu et se tüdruk Kört Liwa sab ette noutud selle perrast et temma keik ne juttut on tõstnud ja selle tüdrukull Mina Poss räkinud et Mats Leinasaar on temmaga ja Liso Pridrikoga ellanud ja et Kadri Wachter peab raske ollema.
Sai ette kutsutud (Kört Liwa) Mina Poss ja küstud kas Körd Liew on temmale räkinud et Mats Leinasaar ei olle mitte üksi nende tüdrukudega Kadri Wachtrega ja Liso Pridrikoga ellanud, et ka temma issa Mats on nendega ellanu ja et Kadri Wachter peab raske ollema.
Sai ette kutsutud Kört Liw ja küstud kas temma on nisugused juttud räkinud, - wastas et temma on ned juttud küll räkinud mis temma on teiste käst kulnu agga mitte toeks ei woi tehha:
moistetud sai
et Kört Liwa peab nende roppo juttude est ja et ta wel kohto es rop olli kakskümmend witsa obid sama.
