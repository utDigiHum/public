Tulli se mõisa sep Peter Sobik sia kohto ette ja kaewas, et kui temma mõisas olnud tööd teggemas, mõisa aidamees tulnud ja tedda naelarauaga  pähha lönud, selle et temma ei olle tööd teinud, ja on weidikesse wina wõtnud.-
Astus ette aidamees Johann Jlwes ja kostis küssimisse peäle, minna ollen tedda kül kord lönud naelarauaga, agga mitte pähha, waid pusa peäle, ja kui temma käe ette pandis, läks se hoop temma käe peäle. Tunnistajad seäl jures ollid kutsar Ain Punna ja äestaja pois Jaan Sossi.
Kutsuti mõllemad ette ja küssiti neilt, kas nemmad näinud, et Aidamees Peter Sobikut lönud. Nemmad kostsid, ja temma lõi tedda küll üks kord pusa peäle, selle et temma joobnud olli ja tedda ennast jodikuks sõimas.
Mõisteti
Selle et Aidamees Johann Jlwes Peter Sobikut raudkeppiga lönud, nenda, et Peter Sobiku kässi on weidi hawatud, maksab Johann Jlwes Peter Sobikulle üks Rubla hõbbedat.
