Laiksaare Koggokonna Kohhus sel 30mal Aprilil 1865
Kohtus ollid Pea Kohtomees Juhann Petersohn
Kohtomees Jurry Orraw
-,,- Jaan Kurm
Kaibas Willem Trei Kamma perremehhe Hans Kamdroni peäle et temma 5 naela linno ees temmale 7 R hõbb. on wõlgo jänud.
Hans Kamdron ette kutsutut räkis ärra et temma on Willemale selle pärrast mitte ärra maksnud et temmale on linno mümisse ees 25 R. hõb. wähhem maksma.
Moistetut sai et Hans Kamdron peab 2 näddale pärrast need 7 R. hõbb. Willem Trei wälja maksma.
