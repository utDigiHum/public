Jürri Wist Kaubist tulli ette ja kaebas Jaak Liblik on sel 23ndal Augustil s.a. Kaebaja hobust aianud ja peale selle on hobusel kel kaelast ühes pikka rihmaga ära wõetud olnud, Kaebaja palub asja seletuse alla wota ja süi alust sundida nimetud kella ja rihma eest 4 Rb 3 Cp ja kahju tassumist 40 Rb kaebajal wälja maksma.-
Jaak Liblik wastas kaebduse peale, ta olla kaebaja hobuse omma maa pealt Odra põllu lähetalt ära aianud kaebaja hohusel on siis kel ühe walge rihmaga kaela jänud kus ta jäänud ei tea kaebatu.
Andres Saarme andis tunistust peale selle  ½ tunni parast kui kaebatu kaebaja hobust aianud, on kaebaja hobusel kaelast ära woetud olnud ja kaebatu on sel aial kaebaja hobuse ümber kõndinud kui se kõnesseiswa kel hobuse kaelast ärra katunud peale selle kui kaebaja kaebatu juurest kella kuulamas käinud on Jaak Liblik ütelnud, se woib olla et see kel ka minno käes on.
Sepeale kui kohtu kaiadel keigewähem enam ette tua ei olnud, uhentasid kohtu liikmed endid selle otsusele
Otsus: Puduliko tunistuse pärast kaebaja ommas kaebduses rahul nomida.-
Otsus sai kohtu kaiadel seda maid kuulutud, naad ei olnud seega rahul.
Peakohtumees Kusta Leppik [allkiri]
Korwalistnik Andres Saarme [allkiri]
Korwalistnik Jaak Raegson [allkiri]
