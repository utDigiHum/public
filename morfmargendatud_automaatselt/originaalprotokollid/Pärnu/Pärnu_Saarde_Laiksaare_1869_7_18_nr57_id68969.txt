eodem dato.
Kaebas tallitaja Jaan Kurm, et Karl Tals olla Urrisaare kõrts kõnnelenud, temma olla mõisast 1. mullikas ärrawarrastanud, kui ka Johann Sigori käest rahha wõtnud mõisamaade eest.
Karl Tals wastas küssimisse peale: temma ei olla middgi kõnnelenud, agga teiste olla Jaan Kurm Orrawa wasto ütlenud et Johann Sigori käest 10 Rub. rahha mõisa lehmade eest saanud, ja sedda olla Urrisaare kõrtsis kõnnelenud Woltwidi mees olla tallitajale wassika annud ja selle eest jahho wasto saanud.
Tallitaja ütles et wassika wahhetanud ja 10 naela leiba peale annud ja se olnud 1868.
Selle peale leppisid ärra.
Mõistetud: et se protokolli kirjotud kuida siin on sündinud.
