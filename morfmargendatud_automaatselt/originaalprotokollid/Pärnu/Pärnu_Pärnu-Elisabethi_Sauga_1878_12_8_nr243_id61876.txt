Ette astus:
Ann Liemann ja kaebas: Aadu Kogeri poeg Jaan on oma koeraga tema sea surnuks puretanud.
Jaan Koger wastas Ann Liemann on käskinud siga koeraga õue wärawa tagast ära aeada, mis peale ta koera sea peale assitanud ja kui koera tagasi kutsunud, siis surnud siga warsi ara.
Tunnistuseks tuli Riinu Lasn Hansu tütar ja rääkis: Ann Liemann assitanud koera oma sea peale, aaga et koer pole läinud, käskinud ta Jaan Kogeri koera assitada mis see ka teinud ja kui siga pärast koera eest lahti pääsnud, siis siga kukunud maha, Ann Liemann löönud weel korra käega sea külle peale, siga röhkinud korra ja surnud ära.
Madis Saartson tunnistas just nõndasammuti.
Hendrek Prints oli seda surnud siga waatamas käinud ja arwab seda 15 rubla wäärt olewat. Koerast ei ole ta seal muud tehtud leidnud olewat, kui saba natukene hammustatud.
M. Rapp tunnistas sedasammust.
Mõistus: Ann Liemanni siga on kül 15 rubla wäärt arwatud, aga et ta ise esiteks koera on sea peale assitanud, seeparast on ta seal juures süüdlane ja et Aadu Kogeri koer seda siga on taga ajanud seeparast maksab Aadu Koger Anna Limannil selle sea pool hinda, mis on 7 Rbl 40 kop.
