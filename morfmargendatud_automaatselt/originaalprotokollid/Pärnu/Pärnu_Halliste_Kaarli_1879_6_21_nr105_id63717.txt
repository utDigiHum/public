Peter Kogra kaebtuse asjas Jaak Sirkle wastu sulase teenistuse kauba murdmise pärrast ollid kohtu käiad kui ka tunistajad sia ilmunud.
Jaak Sirkel wastas kaebtuse peale ta ei olle keige wähhemad kaupa kaebajaga teinud.
Peter Lintsi andis tunnistust kaebaja olla kaebatu oma juure  kutsunud ja tema käest küssinud, kas ta tulleb popsiks, kui mitte siis kaebaja wottab Kusta Pütsik, kes praegu siin on ja popsiks tulla tahab. Kaebatu olla nimelt se peale wastanud, et ta tulleb ja seda kohta mitte enam oma käest ärra ei anna.
Peter Kobbi andis tunnistust Jürri päewa homikul saatis kaebaja oma karja pois kaebatul järrele, ja küssis kaebatu käest kas sa jääd minnu juure popsiks, kui mitte, siis wõttab sedda kohta üks teine mees, sepeale olla kaebatu wastanud et ta sedda kohta mitte ärra ei anna omast käest ja lubanud isse seda kohta wotta, sepeale olla Sirkel ja Koger molemad 50 Kop koku pantnud ja selle eest kauba kinnituseks liigud joonud, kaebatu piddi kaebaja käest kartohwli ja kapuste maad saama ja iga päew, mis kaebatu kaebaja jures töös on saab ta 25 kopik ja 30 Copik ja pakkilisse töö aial 50 Cp päewaks.
Peter Koger pallus Kohud et ta poisid kaks kohtu päewa ärra wiwitanud ja isse kaks päewa selle eest kaebatud 10 Rubla kahju tassumisele tompada.
Kui Kohtu käiadel keige wähemad ennam ette tua ei olnud, ühentasid kohtu liigmed endid selle otsusele:
Otsus: Et tunistajade põhjusel selgesti näha olli, et Jaak Sirkel se kindel kaub teinud et ta Kogra juure tenistuse /popsiks/ jääb - peab Sirkel sedamaid Kogra jure teenistuse minema ehk 50 rubla kahju tassumist 8 päewa sees maksma kaebajal.
Otsus sai kohtu käiadel sedamaid kulutud ja kohtu käiad ollid sega rahul.
arwatud: keik kuidas sündinud kirja panda.
Kohtumees: Jaak Reagson [allkiri]
      - " -         Jan Mõrd XXX
     - " -          Peter Leppik [allkiri]
