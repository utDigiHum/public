Laiksaare koggokonna Kohtus sel 15mal Mai 1864
Kohtus ollid Pea kohtumees Juhan Petersohn
Kohtumees Jürri Orraw
Jaan Kurm
Tulli ette (kutsutud) Juhan Grenstein ja pallus Kohhut et wannemad jubba gewad wannaks temma peab keik toitma ning puhha Moisa paewad üksinda teggema et Kohhus teb ühhe selletusse nende wahhele miss temma jaus gaeb et maksab teistele wenna ja õedel welga.
Moisteti 2 Lehma, 2 Erga, 1 3 aastane Mullikas, 1 1 aastane Mullikas gäewad nende 4 Lapse gauks neid makso taa neile welga ehk andko neile kui naad nii wannaks saawad taggasi.
