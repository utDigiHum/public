83. 13" Janoar 1878
Kustas Peterson kaebas et ta tüdar Tio on 4 aastad tagasi peremees Jago Mihkli 19me nädalad teeninud aga pole midagi selle eest saanud. Peremees Jago Mihkel ütles et tüdruku temaga aasta pääle kauba teinud aga 17ne nädala pärast on Kustas Peterson oma tüttart ara wiinud; palka olla ta kätte saanud 2 särki 1 Rubl raha 1 naeste wö. 
Jäi tulewa kohto pääwaks, tüdruk ise peab ka olema.
Tio Peterson ütles et peremees Jago Mihkel teda omale aasta pääle tellinud siis on ta oma tahtmisega senna teenima läinud, aga on isa Lätti maalt oma sui teenistusest kodu tulnud ja siis teda säält ära wõtnud.
Kohto poolt sai se asi nenda läbi räägitud et Jago Mihkel, pääle selle, kui isa temal oma tütre esimest kaupa oli tühjaks teinud, pääle selle jälle tüdrukuga teist korda kaubelndu, peab ta 1 kuu päält 3 Rubl. ning kokko 4ja kuu eest wälja maksma ja ei wõi ta oma aasta kaubast ühtegi pärida. 3 r 20 kop on tüdrukul käes, saab weel 8" 80 kop.
