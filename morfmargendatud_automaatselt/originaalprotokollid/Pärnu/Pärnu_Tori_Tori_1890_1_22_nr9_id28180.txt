Kihelkonna kohtu käso kirja pohjusel sest 16.Okt.1889 No 4383 sai Liiso Jürwetsoni järel jäänud warandus tema kahe pärandaja Mart Jürwetson ja Tiina Treublut (sünd.Jürwetson) wahel järgmisel wiisil ära jägatud:
Pihkwa Pangast raha intressi peal olnud 600 rubl.
Intressi saadud                                                31 "      82 k.  Summa 631 rubl.82 k
Sellest summast läheb maha:
Mattuse kulud    162 rubl.91 kop.
Risti kulud           31   "       82 =194 " 73
Jääb jägamiseks üle: 437 rubl.09 kop.
mille järel kumbki pärandaja 218 rubl.54 1/2 kop.saab.
Otsuseks:Seda protokolli kirjutada ja Mart Jürwetsonile tema jägu kätte anda sest et täna kohtu ette põle tulnud.
Peakohtumees:T.Tehwer/allkiri/
Kohtumehed:H.Kask /allkiri/  J.Martson/allkiri/  M.Ollino/allkiri/  Jüri Järberson/allkiri/
