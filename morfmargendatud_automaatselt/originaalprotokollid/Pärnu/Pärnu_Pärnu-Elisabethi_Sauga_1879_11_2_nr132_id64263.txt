Anna Johanson kaebas tema olnud Mudu Jüri Lillese juures aasta kauba teinud ja senna karjatseks läinud ja lubanud Jüri Lilles temale palka
2 körti
2 särki
1 palito ja 
1 rättik nelja rublane.
Jüri Lilles on teda nüüd lahti laksnud, aga mitte kõik lubatud palka kätte maksnud.
Jüri Lilles wastas, et teda Pendre talu karjutseks on wõtnud ja et Pendre talu temalt ära saanud, seepärast ei ole temal teda enam tarwis ja on ta temale palka kätte annud 1 kört üks särk 2 rättikud sukkad ja 40 kop raha.
Mõistis kohus Jüri Lilles peab Anna Johansonile see suise teenistuse eest weel ligi maksma, palitu eest 1 rubla ja rättiku eest 1 rubla = 2 rubla. Ja et ka Anna Johanson enam Jüri Lillest ei taha teenida siis wõib temast lahti jääda.
