Peter Ruuse tulli ette ja kaebas, et Marri Karwand olla kaebaja heinu warrastanud, keda kaebaja naene Marri Ruus kinni wotnud. Kaebaja paluda et kaebatu saaks sädusliku trahwi alla tompatud.
Marri Karwand tulid ette omma mehe Jaakuga ja wastas ta ei olle mitte üks kõrs kaebaja heina warastanud.
Kaebaja naene Marri Ruuse andis tunistust ta olla sel korral juure sanud kui kaebatu kaebaja heina pööninga pealt heinu warrastanud.
Miina Pütsik andis tunnistust Marri Karwand olla tunnistajal seda räginud, Mis minna waene tänna tõin, minna wotsin Ruuse heinu ja tahtsin oma lampale ette wisada ja Ruuse Marri wottis mind warguse pealt kinni.
Peter Leppik (kohtumees) andis tunnistust et Jaak Karwand on tunnistajal räginud et ta naene on heinu warastannd.
Kui kohtu käiadel keige wähemad ennam ette tua ei olnud siis ühentasid kohtu liikmed endid selle
Otsusele: Et tunnistuse põhjusel selgesti näha olli, et Marri Karwand Peter Ruuse heinu warastanud, saab Marri Karwand heina warguse pärrast 48 tunnis wangi heitetud.
                          Jaak Raegson [allkiri]
kohtumehed:  Jan Mõrd 
                         Johan Lensin [allkiri]
