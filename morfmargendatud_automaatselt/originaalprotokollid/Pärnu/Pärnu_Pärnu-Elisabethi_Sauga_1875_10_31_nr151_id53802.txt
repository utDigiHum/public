Andres Reimann kaebab et ta pealt rentnik Tamme Abram Lipmann kelle käest ta Kuusiku kohta reninud nüüd hilja 8" Oktobril tedda ommast kohhast on ülles üttelnud ja arwab temma et tedda kui maa piddajad Jakobi päwal olleks piddanud lahti öödlama ja ei tahha ta sedda ütlemist nüüd wasto wõtta.
Abram Lipmann ütles: et Andres Reimann ei olle temmal mitte kui ühhe kontrakti piddaja maa rentnik olnud waid ilma kontraktita sullase kohha piddaja kedda kaks ehk kolm kuud enne aasta löppemist wöib ülles üttelda.
Se assi jäi weel tullewaks kohto päwaks.
