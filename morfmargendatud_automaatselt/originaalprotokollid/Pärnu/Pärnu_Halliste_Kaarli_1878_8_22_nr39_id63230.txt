Johann Seimann (Meose tallo ommanik) tulli ette ja kaebas, ta olla omma tallo Tõnnis Wiilil rendi peale wälja andnud, agga rentnik ei tahta kontrahti punktid mitte täita, kelle läbbi kaebaja paljo kahjo sanud. Kaebaja palluda et kaebatu saaks nisuguse kahju tassumisele noudud, ni kuidas järrel nahha  ja nimmelt:
punkt I  rentnik on kahe kolmandikko aia ma peale kontraki wasto linno mahha küllinud, kus ta sõnnikud piddanud pannema. selle eest nõuda kaebaja 28 Cop.
punkt II rentnik on keelatud 2 kolmantiku põllu ma peale sõnnikud weddanud ehk pannud kelle eest kaebaja nouda 3 rub
punkt III  rentnik on kaks sülda seitsme jalla puid 3 wakkama heina ma ülle weddanud kus läbbi heina ma kui ka hein lausa ärra rikkutud sanud se eest nouda kaebaja  10 rubla.
punkt IV rentnik on heinamaa peal (aia maad peal) 9 päewa kolm wassikad ja üks 1 Lehm hoidnud. wassika pealt päewaks 50 Cop ja Lehma pealt 1 rubla. nõuab kaebaja kahjo tassomiseks. summa 18 rub 50 cop
punkt V rentnik on kolm näddalad tallo karja kiwwi ullitsa peal piddanud, ja ei olle mitte ühte aida põllo peale teinud kus läbbi põlt wäetud olleks woinud saada se eest nouda kaebaja  5 rub. tunnistaja Hans Ismid ja Tiina Karwand andis kaebaja ülles
punkt VI rentnik on aias kaswawade marja puhmad ja ouna puud ärra rikkunud kui ka nende wilja ärra sönud. se on 16 marja puhmast a puhma pealt 15 Cop 1 ouna puud a  3 rubla. nõudatu summa 540 Kop. tunistajaks Hans Ismidt andis kaebaja ülles
punkt VII rentnikonaene on keik tallo karja ja lambad kaebaja kaera aianud se eest nouda kaebaja 8 ellaja ja 25 lamba pealt summa 3 rubla. tunnistajaks Ann Rae andis kaebaja ülles
Kaebaja wastas: kaebtuse peale eesitiks pkt I peale. Et nimmelt kondrati sees ei seisa se mitte et rentnik woida linno aia ma peale küllida agga peale selle olla Siemann lubbanud linno aia ma peale küllida ehk mahha tehja tunnistajaks andis kaebatu ülles Hendrik Saksniit ja Hans Hismidt.
pkt II kaebato olla selle ma tükki peale kaebaja lubbaga sõnnikud weddanud.
pkt III kaebaja olla ülle heina ma need puud ärraweddada lubbanud, tunnistajaks antis kaebato ülles Hans Ismidt
pkt IV kaebato käes olla nimmetud koppel olnud kontraki järrele renti peale antnud. ja needt wassikad sest olla kaks wassikud kaebaja ja üks kaebatu omad olnud ja Lehm on üksi 2 tundi koples olnud.
pkt V se ei olla tõssi et kaebato karja 3 näddalad karja ulitse peal piddanud waid suwwi sõnniko weddamise aial olla üksi wahhed rihha all ja rihhaallutse ees  karri seisnud.
pkt VI kaebatu ei olla mitte ühdegi marja puhmast kui ka ouna puid ärra rikkunud egga ka nende wiljase pudunud.
pkt VII se ei olle mitte tõssi et karri kaeras olnud.
arvatud: Eesseiswa assi teiseks kohtu päewaks jätta siis kohtu käiad kui ka üllesantud tunnistajad ette telli ja siis assi otsusele saata
Peakohtumees: Jaak Raegson [allkiri]
Kohtumees: Peter Kogger XXX
      - " -         Jaan Mõrd XXX
