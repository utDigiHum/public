Kaarli walla mees Mats Rubel tulli ette ja kaebas Jürri päiwal se aasta olla kaebaja Willandist tulles Rimmo Kõrtsi sisse läinud ja olla selle aia sees kui kaebaja Kõrtsis olnud, Rits Tikku kaebaja wankrist ühe uue mütsi, mis ta Willandist ostnud, ärra warastanud, ja ühe wanna asemele pannud. Kui kaebaja ligi kodu olnud olla ta mütsi waio leidnud ning kohe Rimmo kõrtsi tagasi sõitnud ja leidnud, et se müts Rits Tikku peas olnud, kaebaja nõudmise peale ei olle Tikku mitte mütsi kätte annud. Alles teisel pühapäewal olla Tikku nimetud müts tagasi toonud ning 15 Kp. kaebaja poial annud. Nisugusel järgil paluda kaebaja asjas teha mis kohus säduslikuks arwab.
Rits Tikku wastas kaebuse peale, se olla tõssi et ta Mats Rubeli mütsi tema wankrist ära wahetanud.
Sepeale sai tehtud Otsuseks: Et Rits Tikku ise tõeks tunnistanud mütsi wahetanud olla, mis nipaljo tähendab, et ta seda nago warastada tahtnud, siis saab temo 12 tunniks wangi heidetud.-
Otsus sai sedamaid kohtu käiatel kuulutud ja ollid kohtu käiad sellega rahul.-
Jaan Mõrd XXX
Johann Lensin [allkiri]
Peter Leppik [allkiri]
