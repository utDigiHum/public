Tullid kohtu ette Pattist Hans Pumpernikel ja kaebas, et Hans Mitt löönud 2 korda temma issa, - tunnistas Johann Grenstein, Jurri Mardik ja Peet Puck, - 2 ja Jaan Tals löödnud tedda 1 kord ja teine kord lükkanud tedda wasto seina, - tunnistus Karl Mõttus, - 3 Karl Tals sõimanud tedda krono ja koera warkaks tunnistus: Tippu Gustas ja Jürri Orraw.
Sai ette kutsutud ja wastassid küssimisse peale.
ad 1. Hans Mitt: se kaebtus olla walle, - mikspärrast piddanud ta kül teist lööma, se olla selge walle.
ad 1 Tunnistussed:
Johann Grenstein ei olla tulnud.
Jürri Mardik, ei olla tulnud.
ad 2. Jaan Tals: temma ei olla löönud, se olla walle.
tunn Karl Mõttus: ei olla tulnud.
ad 3. Karl Tals: temma ei tunda naggoni kaebajad, kuida wõida tedda sõimada.
Tunnistussed:
Tippu Gustas: ei olla tulnud.
Jürri Orraw: temma ei olla mitte nännu et Jaan Pumpernikle silmi werrised olnud, agga sedda olla Hans Pumpernikel ütlenud: et temma tahta Laiksaare mehhi õppetada.
ad 2. tunnistus:
Peter Rentnik: temma pea olla soe otsas olnud, temma ei teada keddagi.
Mõistetud teiseks korraks.
