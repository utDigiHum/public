Pärnu II Metsa jaukonna Kihlepa Metsa herra kaebduse kirja järele sest 23 Maist s.a.N 108 al puide warguse ja raijumise pärast Kihno kroonu metsast sai Enn Oad kohtu ette nöutud ja tema räkis:et tema pole metsast mitte ühte puud toonud,ei ole metsawaht tedda mitte metsas näinud egga tema metsawahti,egga wöi ka metsawaht mingisugust tunnistust temale ette panda,se on metsawahi polest tühi undrehti tema peale kaibdus.Kohus möistis:anda sellest kirjaga teada Metsa herrale ja ette panda,et nüüd on nende puude raijujate asi Walla kohtus löppetud ja saata se Protokol mis Metsa herra polest saadetud olli tagasi ja se sai täidetud.
Kihno Walla kogukonna kohus.
Peakohtumees:Jakob Mättas XXX
Kohtumehed:Jaan Pull XXX
Kustaw Sutt XXX
