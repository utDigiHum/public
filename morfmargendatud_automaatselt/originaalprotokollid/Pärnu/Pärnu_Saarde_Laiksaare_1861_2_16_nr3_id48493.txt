Koggokonna Kohtus sel 16mal Künla kuus 1861
Kohtus ollid: Ado Tals pea kohtomees.
Jurri Orraw kohtomees
Jaan Joosti d.
Kihhelkonna kohto kässo peal sai ette kutsutud Jurri Teffer ja küssiti temma kaest mis Karl Joeks temmale teinud, et temma kihhelkonna kohtole on temma ülle kaebamas keinud, kus peal temma wastab, et Karl agga rido tewa, temma on agga ütlenud et ollegs parrem kiwwi toas olla kui üks pahhur inniminne siis Karl tend irmsaste soimanud.
Karl Joeks rägib küssimisse peal et temma ollewa küll Jurid soimanud, agga Jurri on akkanud soimama. Tunnistus mitmele ei olle.
Moisteti: Et Tunnistus ei olle saawad kinnitud rahholiste ellada.
Ado Tals pea kohtomees
