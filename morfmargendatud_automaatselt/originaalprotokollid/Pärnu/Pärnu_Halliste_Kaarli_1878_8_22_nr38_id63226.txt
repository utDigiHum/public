Ann Pärsmann tulli oma käemehhe Tõnnis Wiil'ga ette, ja kaebas Johann Oija ei tahta kaebajal 1877 aasta teenistuse palka wälja maksa se on oPalka rahha			10 Rb		Pastalde rahha			3 Rb		3 naela Willo					30 naela Linno					1 willane kuub (kört)					
Kaebaja olla haigeks jänud, ja siis perremehhe Johan Oija naese kässo peale kes niwiisi üttelnud minne peale omma emma jure kes sind siin tallitab. - ja peale selle kui kaebaja parremaks sanud, olla ta oma ema kaebatu jure järrele küssimise sadanud kas kaebatu kaebajad weel teenistusse tahhab woi mitte. Kaebato olla se peale wastanud mis ma nüüd temaga weel teen. - Kaebaja palluda et kaebato saks sunnitud eesseiswa palk wälja maksma.
Johan Oija wastas se peale kaebato olla sel 25 Nowembril 1877 aastal haigeksjänud ja siis olla ta emma tedda omma jure põetama wiinud, ja peale selle kui kaebaja terweks sanud ei olla kaebaja mitte teenistuse taggasi tulnud. - ei ka mitte omma palga selletusele seläbbi laemada kaebaja kaebatud.
Kaebaja tunnistas et ta 4 Rb kätte sanud 1877 aas. palgast.
Kohtu käiad teggid järrelseiswad Leppitust: kaebaja Lubbab 1879 astal Jürri päewal kaebatu jure teenistuse minna, ühhe asta peale ja maksab siis Johan Oija 1877 aasta palk keik wälja ja weel 1879 aasta eest annab Oija weel se samma suur palk mis ta 1877 aasta eest saab.
arwatud: keik kuidas sündinud kirja panna
Peakohtumees: Jaak Raegson [allkiri]
kohtumees:  Peter Kogger XXX
      - " -          Johann Lensin [allkiri]
