Jüri Rentnik kaebab et Mari Kangur tema saunast kui mitte isi kodu ei ole olnud oma saapad ning tema 4 Rbl 40 kp. raha mis kirstus olnud ära warastanud.
Mari Kangur (käemees Juhan Sooper) ütles et tema kül saapad kirstust mis mitte lukus olnud ära wiinud aga mitte seda raha warastanud ei ole
Jakob Ots ütles et Mari Kangur saapad tema kirstu toonud, et kui Jüri Rentnik taga otsima tuleb et neid siis leida ei ole; sai siis:
mõistetud: Mari Kangur saab warguse eest 24 tunni peale wangi pantud ja peab seda 4 R. 40 kp. Jüri Rentnikule wäljamaksma 2 Rbl. 40 Kp. 8 päewa aea sees ja 2 Rbl. kuni 1 Januar 1884 mõlemad olid sellega rahul.
