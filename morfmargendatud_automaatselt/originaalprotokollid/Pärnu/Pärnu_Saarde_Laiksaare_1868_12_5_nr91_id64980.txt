eodem dato.
Kaebas Jürri Orraw, et temma Ado Wahhert teninud ja palka saada 1 Rub. ja 1 paar püksa.
Ado Wahher wastas küssimisse peale: peab sellepärrast palga kinni et 1. 3 toonise lassi ärra põlletanud ja 2. küllimitto rukki, mis Kalkunile maksnud, sest pois ollu hobbuste jures olnud ja lasknud hobboseid pahha peale, - sest lõuna söömast wälja minnes jäänud pois maggama, kui piddanud hobboste järrele watama.
Jürri Orraw ütles et hobbosed olla siis pahha peale läinud kui lõunat söönud, - kui söömast taggasi läinud siis aeanud temma hobbused pahha pealt ärra.
Ado Wahher ütles et kaup olnud: mäntel aastas agga pois tahtnud kube saada ja temma teinud ka agga kaupa ei olla mitte olnud.
"Selle peale leppid nõnda: kaebaja jättis püksid mahha ja perremees maksab 1 Rub. öbbedad.
Mõistetud: et se protokolli kirjotud, kuida siin on kirjotud.
