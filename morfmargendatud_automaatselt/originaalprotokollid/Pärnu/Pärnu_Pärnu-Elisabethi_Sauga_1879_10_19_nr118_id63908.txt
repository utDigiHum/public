Mihkel Põhakas kaebas, et Saarde Jüri Saarts oma linu üle tema heinamaa wette wiinud ja heinamaad tallanud, nagu metsawaht Jaan Pajumann seda tunnistada.
Jüri Saarts wastas, et mööda teed on käinud, aga mitte heinamaale saanud.
Jaan Pajumann heinamaa waht ütles näinud Saarde Jüri sealt heinamaalt minewad, aga põle ta ligi saanud ega temaga rääkinud, ega ka kinni wõtnud.
Mõistis kohus: Et kahju waadatud ei oldud, ega ka tõsist tunnistust, et Jüri Saarts sealt oleks üle tallanud jääb see kaebtus tühjaks.
