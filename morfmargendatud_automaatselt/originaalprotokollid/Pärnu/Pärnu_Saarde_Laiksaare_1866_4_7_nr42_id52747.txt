Laiksaare Koggokonna Kohtus sel 7. April 1866.
Kohtus ollid: Pea Kohtomees Karl Tals
Kohtomees Hans Tins
-,,- Tennis Baers
Olli Kohtus tullo Jurr Männiks ja Karl Kösell (vid. Prot. Nr 35).
Karl Kösel küssimenne peal üttles, temma eijollewa kaks kord Kohtus tullo, selleperrest kui eijollewa aig ollu. Temma naiste õddet Lihso ja Lehno eijollewa sahno Kohtus tuhwa, selleperrest kui selle Issä, Jurr Männiks, eijollewa sellele lubbanu tulla.
Jurr Männik pallus, temmal Karli kähst selle kahhe korra ihst mis eijolli Kohtus tullo, maksu moistat, ja palluwa temma, sii assi nikawa kui süggisi jätta, selleperrast kui temma lehwa suwwel erra Krahwi töhses.
Kohhus moistis: Karl Kösel peab Jurr Männikul §526 järgi selle kahhe Kohtu keimesse ihst, kui eijolli wastu tullo, 1 Rubla maksma; ja saab sii assi sis süggise pool selletama woetut, kus Jurri jelle pallub.
Karl maksis Jurril 1 Rubla erra, ja antsi ülles, kui neid tunnistejat ehlawat: Lihse Sillin - Ehdamestes, ja Lehno, Sauniko Pedo naine - Orrajas.
