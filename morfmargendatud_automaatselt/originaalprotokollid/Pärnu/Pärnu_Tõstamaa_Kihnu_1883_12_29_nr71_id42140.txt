Kühno Walla otstawku soldat Mihkel Tungen ja andis üles,et nenda kui temal mitte ühtegi last ei ole ja isse on naad mölemad nasega juba wanad inimesed,siis soowisiwad omale Mattu Tooma Mättas poega Mihklit omale pojaks(kaswu pojaks)wötta sellega,et pärast nende surma,köik nende elu maja kraam ja warandus jäeb temale-Mihklile kui pojale ka nende elumaja,Mihkel agga peab hea sönakuulelik laps olema ja truu nende,kui wanemate wastu.Kogokonna Kohtul selle wastu midagi wasturäkimist ei ole ja tunistab nende mölemate lepimist ja soowimist kindlaks oma alkirjaga.Tomas Mättas Mihkle isa olli selle soowimissega täitsa rahul ja annab oma poja Mihkli,-Mihkli Tungenile pojaks.Mihkel Mättas on sündinud 25al Septembril 1876 aastal.
Kihno walla kogokonna Kohhus
Pea Kohtumees:Enn Alas  XXX
Kohtumehed:Mihkel Larents XXX
Jüri Laus XXX
Mihkel Kott XXX
