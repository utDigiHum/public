Kaarli walla peremees ja lina kaupmees Märt Leppik tulli ette ja kaebas: Sel 16 Juunil olla üks Perno linnas elaw mees nimega Jaan Deffant Kaebaja maiasse tulnud ja linna woori palunud Pernu Bostroemi Konturisse. Et Kaebaja ise se kord Kodus ei olnud, siis olla Kaebaja pois Hans Kiin nimetud Jaan Deffantil 3 kaalu linu peale annud, et ta neid Perno Bostroomi Kontorisse pidi wiima. Jaan Deffant aga ei olla nimetud linnad mitte tänini Boströömi Kontorisse wiinud, waid neid ära warastanud se  wiisil.
Niisugusel järgil paluda kaebaja asja üles wõtta, ja siis kaiba kohtu ülekuulamisele ja otsusele saata. Need linnad olla wäärt olnud 180 Rb, se on 60 Rb kaal.
Jaan Deffant Perno linnast wastas kaebduse peale, ta ei ole mitte Märt Leppiku juurest linna woori peale wõtnud, ja ei tea asjast midagid.
Hans Kiin Kaarlist tunnistas: sesama Jaan Deffant olla 3 kaalu linu sel 16 Junil 1879 Märt Leppiku juurest peale saanud ja olla lubanud neid Boströmi Kontorisse wia.
Els Taal Kaarlist tunnistas niisamma, kui Hans Kiin ja lisas weel juure, et Jaan Deffant se sama mees olla, kes sel 16 junil 1879 Märt Leppiku juures linnu peale wõtnud.
Jaan Waga Kaarlist 16 aastad wanna tunnistas niisama kui Hans Kiin ja Els Taat.
Kui Kohtu käiatel muud enam ette tua ei olnud, siis sai 
arwatud: et asi kaelakohtu wäriline, siis seda Keiserliku Wiliandi Silla Kohtu ülekuulamisele saata.
Jaak Raegson [allkiri]
Jaan Mõrd XXX
Johan Lensin [allkiri]
