Ette astus kaebaja Kolluwerre küllast Andres Wistis ja andis ülles et potretsik Simo Lat, ei olle temma wälja tenitud palka mitte täieste kätte maksnud waid ühhe kuu palk 8 Rubla kinni piddanud.
Agga Simmo Lat tunnistab kohto laua ees, et Andres Wistis ei olle mitte nenda teninud, kuidas nende kaub on olnud, waid on enne kauba lõppemist tennistusest lahti wõtnud.
Selle ülle on kohhus:
Mõistnud: Et Andres Wistis peab omma kuu täis tennima ja peale selle maksab 50 koppikad walla laeka, ja Simo Lat maksab tema palk wälja.
Peawannem: Willem Müllerbek XXX
abbi: Karel Loune XXX
teine: Jürri Martson XXX
