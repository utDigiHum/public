22. Kohtu ete astusiwad need, kellel ära-upunud Nigulas Kloppi käest midagi saada ehk maksta jäai ja andsiwad ülesse, mis neil kellelgi tegemist oli:
1) Tõnis Kimsen andis üles, et temal on Nigulas Kloppilt 47 rubla 50 koppikat raha saada.
2) Jaakob Andest andis üles, et on Nig. Kloppilt 8 rubla 50 kop. saada.
3) Kohaomanik Juhan Lass Wastalt, andis üles, et Nigulas Klopp ostis tema käest 23 ?? wöid a 27 kop. teeb 6 rubla 21 kop. Raha jäid saamata.
4) Mihkel Rätsep, Unukselt, andis üles, et on Nig. Kloppilt 6 rubla 25 kop. kartohwli raha saada.   
5) Jüri Paadik, Padast, andis üles et on Nig. Kloppi käest saada: 
laenatud raha: 25 rubla - kop.
maja ehitus: 25 "  - -
3 tündrit kartohwlid: 5 - 40 -
koku: 55 rubla 40 kop.
6) Kaarel Kasker andis üles, et on Nig. Kloppi wõtnud tema käest 45 Tb wõid, a 25 kop. jäi wõlgu 9 rubla 75 kop.
7) Mihkel Laur andis üles, et on Nig. Kloppilt 1 rubla 72 kop. saada.
8) Kustaw Troment andis üles, et on Nig. Kloppi käest 8 rubla 15 kop. raha saada.
9) Simu Lassik andis üles, et on Nig. Kloppilt 4 rubla 25 kop. saada.
10) Juhan Klopp andis üles, et on Nig. Kloppi käest 26 rubla saada; 20 rubla on palga raha ja 6 rubla oli tema oma raha Nigula Käes. 
11) Mihkel Klopp (Nigula isa) andis üles, et on Nigulas Kloppi käest 20 rubla saada; kewade Nigulas wõtis laenuks
12) Mihkel Paadik, Padast, andis üles, et tema andis 50 Soome marka (Wene raha järel 20 rubla) Nigulas Kloppi käte, et pidi ümber wahetama, aga ei saanud enam tagasi. 
13) Simu Tomson andis üles, et Nig. Klopp wiis tema sea Soomes ära müia ja pidi selle hinna tooma, mis säält pidid saama, pääle selle oli 2 rubla 30 kop. muud raha.
14) Toomas Kaldak andis üles, et on Nigulas Kloppi käest rubla 20 kop.
15) Kaupmees Aleksander Kustawson andis üles, et on Nig. Kloppilt 19 rubla 70 kop. raha saada. 
16) Jüri Muttik andis üles, et on Nig. Klopp'i käest 60 rubla saada.
17) Juhan Asper andis üles, et Nigulas Klopp ostis tema käest 7 tündrit 1 1/2 waka kartohwlid. 1 Rbl 40 koppikat andis käte ja 10 rubla 10 kop. jäi wõlga.  
Kui Nigulas ära upus, siis müüs tema kohtumehe juures olemisel kartowholid ära - need oliwad weel Kaupsaarel maas ja sai neist 11 Rbl. 50 kopikat. Raha on Kohtumehe käes
18) Anton Nortmann andis üles, et tema müüs Niguas Kloppi silku 5 puuda 23 naela - puud 1 rubla 50 kop. - koku 8 rubla 40 kop. See raha on tema käes. 
Nigulas Kloppilt on temal saada 3 rubla endist ja 2 rubla söögi raha - koku 5 rubla hõb.
