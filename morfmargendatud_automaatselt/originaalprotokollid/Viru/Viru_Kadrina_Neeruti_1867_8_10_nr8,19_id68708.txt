Ette tulli kaebaja Peter Röhem, kes moisa kubjas, temma einama metsas olli üks heina lado, mis moisa jäggo, agga et moisi selle heinama keige laoga, Jaan Freibachile olli anud, agga et Peter olli omma hõlgedest laule ue katukse teinud, tunnis meest ei olle kui paljo Peter holga on kattuksele pannud.
Siis on kohhus arwanud et Jaan peab 60 kümmend koppikad Petrile maksma.
