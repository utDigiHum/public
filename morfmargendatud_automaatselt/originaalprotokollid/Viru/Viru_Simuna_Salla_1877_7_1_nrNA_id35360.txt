Ette astus Hans Mein Turro kohha pärris-ommanik, ja kaebas walla kohtule, et temma kohha rentnikko Jaan Noppasoni naene Kadri, on tedda wägga hirmsaste sõimanud mõrtsukaks, ja innimese tapjaks, ja olle lubbanud tedda omma kambri uksest mitte lubbanud  läbbi käja, ja on ähwardanud nenda, et kui sinna tulled uksest siis lõõn minna sinnule kerwega sisse, ja sedda tõlli ja rido hakkanud se naene Hanso lapse pärrast, et Hans Meini 2 aastane laps temma lapse järrrele on rukkise löinud Jaan Noppasoni wäljale. Et se assi õige on mis kaebatud tunnistas Willem Elleneberg kes sedda on seäl pealt kuulnud. 
Ette sai kutsutud Kadri Noppason ja järrel küssitud kuidas se tülli teil tulnud, siis tunnistas temma isse ka laste, ja selle uksest käimise pärrast, ja et, Hans on ka temmale wasta räkinud. Siis on temma sõimanud ja ähwardanud Hanso lüa kui temma ülle kelo ukst wälja käib. Siis kohhus
Mõistnud: et Kadri Noppason maksab 1 Rubla 50 Cop. walla laeka ja Hans Mein 50 Cop. walla laeka-
Peawannem: Jaan Tedrekull XXX
abbimees: J. Tikerber XXX
teine: Jürri Klaas XXX
Kirjot: HTamm  [allkiri]
