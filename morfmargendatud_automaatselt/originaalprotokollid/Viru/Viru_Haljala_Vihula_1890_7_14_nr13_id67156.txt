2.Metsiku walla kohtusse.See läbi saab Metsiku möisa walitsuse poolt ülewal nimetud kohtu läbi,neile siin nimetud Pihlasbä ranna Metsiku möisa maade rentnikudele enne Jaagupi pääwa üles ööldud,et nende kondrakid uued saawad.Mardi koht Kuurman,Kõrtsi koht Olman,Jaagupi Kiigalot
Metsiku moisa päris Proua
Sel 30 Juuli k.p.1890
Keiserliku Maiesdedi uukasi järel kuulutas Metsiku walla kohus seda Metsiku moisa proua kirja(Ükskül)nende kohtade üles ütlemiseks Metsiku ranna peremestele ette,
Mardi koht Juhan Kuurman
Jaagupi =   Madis Kegalot
Kortsi    =   Jaan Olman
K.Wilberg/allkiri/ kohtu eesistuja
T.Lillenberg/allkiri/ kohtu mõistja
W.Willandberg/allkiri/ järgimees
kirjutaja K.Prindweld/allkiri/
