Tulli ette Oljo mõlder Jürri Kruusberg ja kaebas walla kohtule, et ta on kuulda sanud, et teliskiwwi tegja wenelane Simo Jaksin, on räkinud et tema olla mõisa Türgi nisso weskist warrastanud. Tulli ette Simon Jaksin ja tunnistas et tema ei olle muido räkinud sest mõisa herra on sedda temale räkinud, et mõisa kottid olnud puudo weskil jänud, siis kohus ja mõlder on sedda herra kest järrel perinud, agga herra ei olle mitte ütelnud, et mõlder sedda warastanud ja need kotti puudo teinud, ehk nood woisid ka enne selle kauge ma tomisi läbbi puudus tulnud.
Tulli ette tunnistuse mehhed Juhhan Kallas, ja Hans Tõnison, ja tunnistasid selgeste kohto ees, et wenelane on üttelnud et mõlder tõeste waras on, nende wasto. Agga sedda on tema kohto ees ärra salganud et ta põlle räkinud, sest wõis nähha et Simon süü alla ni olli, et tal kahtlased juttud ollid.
Siis kohus mõistis, Et Simon Jaksin maksab 4 Rubla 50 Cop walla laeka trahwi, et ta ei tohi mitte tuhja juttusid tehha egga teise innimese auu risoda.
Peawannem: J. Klaas XXX
abbimees: H. Mein XXX
tein: J. Elberg XXX
