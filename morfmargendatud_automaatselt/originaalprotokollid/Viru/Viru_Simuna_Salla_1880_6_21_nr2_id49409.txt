Tulli ette Hans Tõnison, ja kaebas kohtule et Hermann Weirmani sead on tema wilja wäljale läinud, ja tema on need sead oma jure kinni ajanud, agga peale sele on Hermann siggade järrele tulnud, ja lasnud wäggise sead Hanso jurest lahti ja tahtnud ära ajada, siis ei olle Hans tahtnud lasta ajada ja nõudnud trahwi. siis on Hermann weel oma naesega tedda  sõimanud. Tulli ette Hermann Weirmann, ja sai küssitud kas tema olla seda wisi teinud ja soimanud, siis tunnistas et sedda wisi olla tema kül teinud et neid sigu tahtnud ajada. Teiseks kaebas weel Hans, et Hermanni aed on lagunenud olnud, ja seält hobused tema rukis käinud.
Kohus mõistnud: Et Herman Weirmann maksab 1 Rub walla laeka trahwi. Ja hobuste peremehed maksawad kahjo eest Hansule 50 Cop. ja Hermann maksab weel selle oma laugnenud aja eest 50 Cop walla laeka.
Peawannem: J.Klas XXX
abbi: H.Mein XXX
Teine: J.Elberg XXX
