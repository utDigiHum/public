Teiseks tuleb kohtu ette Karula walla Tallitaja Ottu Truuberg ja kaebas Karula kogo konna Liikme Aabram Wihtmanni peale et Abram Wihtemann on kunda möldri käest (woi tema juurest) üks paslek warastanud sest Kunda mölder on temale kaebanud, ja see Paslik olla Abrami käest leitud.
Kohtu ette tuli kaebataw Abram Wihtman kohus küsis tema käest Sina oled üks paslik Kunda möldri juurest warastanud kaebataw Abram Wihtmann tunnistas seda taeki et on warastanud seda Paslikki
Kohtu mõistus
Abram Wihtmann ise oma suuga tunnistanud et tema selle pasliki on warastanud siis ei olnud mingid tunnistust enam tarwis
Kohus moistis et Abram  Wihtmann maksab üks rubla selle paslikki ette Kunda möldrile kahjutasumist ja peale selle maksab tema (2 Rubla) kaks rubla trahwi warguse ette.
See trahw on kohe wälja maksetud kohtu lau peal
Kohta pea
Kohtu korra pärast olid Karrula Annikwere kogo konna kohtuse kokko tulnud
Kohtu pea M. Etwerk
K. Tönnujani
T. Lemburg
