Kaebaja: Tulli ette tüdruk Mari Leiten ja kaebas kohtule, et tema on läinud õhto oma kambri laka magama ja õõse on tulnud mõisa poisid Hans Puurmann ja Hans Ehwert senna, ja tonud teise toa jurest redel, ja tulnud lakka tema jure,  kiskunud teki tema pealt ära panud ta suu peale, ja lubanud tema säre luud katki murda, Hans Puurmann olnud see kiskuja H. Ehwert  anud nõu.
Kaebduse kandja: Hans Puurmann sai essite ette kutsutud ja küssitud kas tema seäl lakkas on käinud tema räkis, et nemad on teisse poistega essite Turu talluse läinud, ja pärast on tema ja Hans Ehwert seält ara tulnud, ja teised on senna jänud jutto aiama, siis on nemad sealt maia jurest läbi tulnud kus see tüdruk ellb. Leiten Mar siis on nemad senna lakka läinud ja tüdruk olnud seal siis naad selle tüdrukuga seält tulitsema, ja tema on tõmanud see tekk tüdrukul pealt ära, ja tema kallale tulnud, ja tekk ära wõtnud, aga muud hada ei olle tema tüdrukule teinud.
Hans Ehewert tulli ette, ja tunnistas nenda samuti kui kui see Puurmann, et nemad on essite ühes seltsis teistega läinud, ja pärast on naad ära tulnud ja senna lakka läinud ja neil on niisugune tülli selle tudrukuga tõusnud, kus tema kaa ühes on olnud, aga tema ei olle selle tüdrukule midagi teinud ei loonud ega lükanud, waid aga seäl jures  seisnud, kui nemad kahekesi on seäl mollanuwad. 
Kohtu. Otsus, et Hans Puurmann maksab 2. Rubla ja Hans Ehwert 1. rubla kirjko oreli heaks trahwi, et nemad ei tohi öösete mööda lakasid ümber hulkuda.
Kohtuwannem: J. Kullisaar
I abimees: J. Jõukas
II abimees: M. Klaas 
Kirjot: JTamm
