Tulli ette kaebdus Lassinorme mõisa walitsuse polest, et Jürri Weirmann ja Willem Kinberg on tuma Mart Kõrwe peksnud mõisa pimedas tallis, nenda tummal need peksmise märkid temma silmis ja näo peal olli nähha.
Ette sai kutsudud Jürri Wirmann ja sai küssitud kas tema on tumma peksnud, siis tunnistab tema et ta ei olle mitte tedda peksnud, waid lükkanud, sepärrast, et tumm tema hobbust lõnud. Siis sai ka Wilm Kinbergi käest küssitud, kas tema ka tallis on tumma lõnud ja sedda näinud kui se Jürri tedda pesnud, siis tunnistas temma, et ta ei olle näinud Jürrid tedda peksma, sest et tema ei olle tallis olnud, agga et ütles õues tummaga tüllis olnud siis ütles õues tedda ühhe kora lõnud.
Ette sai kutsud Juhhan Steinberg kes se kord seält olnud, siis tunnistas tema, et tumm on tallis kissendama hakkanud, se peale on tema talli läinud, ja näinud kui Jürri Wirmann olnud tumma peal olnud, ja tedda peksnud ja kiskunud. Siis on tema seält Jürri ärra ajanud, ja tummal olnud werised märkid näo peal nähha ja need marid ollid ka kohtus temal tunda.
Se peale kohus-
Mõistnud: Et Jürri Wirmann saab 30 witsa hopi, ja Willem Kinberg maksab 1 Rubla wala laeka trahwi, et ei mitte ühhe nisugguse sandise putuma. 
Peawannem: J. Tedrekull XXX
1.abbimees: J. Tikerber XXX
2, abbi: S. Pilemann XXX
