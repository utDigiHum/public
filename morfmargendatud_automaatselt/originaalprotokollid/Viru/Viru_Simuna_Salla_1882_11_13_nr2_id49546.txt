Tulli ette Mõisama kaubmees Hindrek Schumann, ja kaebas kohtule, et temal Lassinorme moisa poisi Hans Lepiko käest sada kauba reihnungi ja tema ei taha seda mitte wälja maksta.
Tuli ette Hans Lepik ja salgas esite seda wõlga, et tema ei olle wõtnud, aga wimaks tunistas, et on se reihnung ta wõitnud, seda olli 9 R 13 Copik.
Siis kohus mõistnud, et Hans Lepik peab se wõlg 9 R. 13 Kopik sel 27 Novembril wälja maksma. 
