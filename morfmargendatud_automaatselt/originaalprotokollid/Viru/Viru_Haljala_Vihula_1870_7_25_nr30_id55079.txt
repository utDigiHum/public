16.Kohto ette tulli moisa wallitseja Karel Leetberg ja pakkus neile allamal nimmetud Wihola walla numa kohha perremeestele kue ette tullewa aasta peäle kontrahtid,perremehhed olliwad kohto ette kutsutud ja wastasiwad moisa wallitseja pakkumisse peäle kohto ülle kulamisse alnenda:
Mätta perremes Jaan Harmipaik:ei wõtta kontrahti
Wanda Juhhan Wanda "           "   ei wõtta kontrahti
Leppiko Jakob Leetberg               ei wõtta  "
Oiaotsa Juhan Rusalep                 ei wõtta  "
Auslikko Tanel Rusalep                 ei wõtta  "
Kiwitoa Kusto Uhkiwi  ei wõtta kontrahti
Kähhi Josep Rämetti ei wõtta  "
Warna Jaan Tingas ei wõtta      "
Lomse Maddis Tingas ei wõtta "
Wolbri Tannel Tingas(ei wõtta)(tahhab puhta rahha renti maksta)
Seppa Josep Sepp ei wõtta kontrahti
Lauri Maddis Uhkado ei wõtta kontrahti
Leuska Kusto Summataweti ei wõtta "
Otsa Kusto Rämetti ei wõtta "
Wero Juhan Harmans ei wõtta "
Nuggise Josep Pendri ei wõtta "
Wastoia Josep Nöps ei wõtta "
Puseppa Jakob Bergström wõttab kontraht 6e aasta peäle
Rami Kusto Uhkiwi wõttab kontraht 6e aasta peäle
Kalda Josep Haili wõttab kontraht 6e aasta peäle
Summa Kusto Summa wõttab kontraht 6e aasta peäle
Küllaotsa Kai Pahkwerk ei wõtta kontrahti
Mäe Jürri Wikholm ei wõtta kontrahti
Käspri Jürri Loosberg ei wõtta "
Mängi Tomas Lepwimann ei wotta "
Wannaperre Andres Kallepraun ei wõtta "
Padi Jako Nakström ei wõtta "
Kili Josep Pöör ei wõtta "
Pressi Juhan Romman ei wõtta "
Neme Tomas Wigar ei wõtta " "
Muhhulase Jaan Hall ei wõtta "
Uustallo Liso Sprit ei olnud kohtus
Wili Mihkel Tihkane ei olnud kohtus
Liwa Kusto Plunkwest ei olnud kohtus
Hemmalase Jaan Wikholmä ei olnud kohtus
Ankru Jaan Pluhm ei wõtta kontrahti
Lottika Jaan Koukberg ei olnud kohtu ees.
Jaan Harmans XXX
Kristjan Leetberg XXX
Kusto Uhkiwi XXX
