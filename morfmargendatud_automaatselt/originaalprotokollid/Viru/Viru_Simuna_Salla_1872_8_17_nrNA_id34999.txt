Ette astus kaebaja Abram Tapmann, ja andis ülles, et Marri Landendemann on tema naest ilma süta warraks teinud. Aga Marri Landemannil tunnistab, et temmal on ühhed käerid ärra kaddunud ja temma on neid otsinud ja ei olle kuskelt leidnud, siis on temma Kai Lapmanni käest küssinud, et sinna käisid siin toas ja wist need kärid ärra. Agga seäl on nemmad wasta mesi riidlema ja teine teist sõimama hakkanud, ja Kai Lapmann tunistab et temma mitte neid kärid ei olle warrastanud ja wimaks Marri Landemann ueste otsinud ja on neid oma toast kätte sanud.
Selle peale on kohhus:
Mõistnud, Et selle riidlemise pärrast maksawad Kai Lapmann 1 Rub ja Marri Landemann 1 Rubla walla laeka rahha.
Peawanem: Willem Müllerbek XXX
abbi: Karel Lõune XXX
teine: Jurri Martson XXX
