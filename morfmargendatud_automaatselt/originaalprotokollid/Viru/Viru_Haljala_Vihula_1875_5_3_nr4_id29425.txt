Kawasto moisa walitsus kaebab et Anton Klas kes moisa ärgade juures,on pu kuhja juurest kütte puid warastanud.Nelja moisa käsu peale pole Ant.Klaas ennam kui kolm algu tagasi wiinud,teised jätnud maha.Kui taale ööldud et ta kohto saab kaebatud,naernud ta,et kogu konna kohut ei tunnegi.
Anton Klaas sai ette kutsutud ja ta käest selle kaebtuse pärast küsitud:Anton Klaas tunnistas et on puid keedu tarwi wiinud ja pole keiki tagasi wiinud.Et ta kogukonna kohhut ei ööld tundma woi kartma,seda waitles Anton Klaas waleks.
Otsus sai moistetud:Anton Klaas peab 20 hoopi(kaks kümend) naha peale saama et on moisa käsu wasta pannud ja esteks üle kulu puid toonud.
Hans Plunt/allkiri/  Kustaw Klaas XXX  Antun Kallenbärg/allkiri/
