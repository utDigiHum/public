Kawasto mois lasi Tallitaja läbi kaebada et Kristjan Tombek on 18 Mais moisa kartuhli hunniku juurest üks kubu hõlgi tahtnud warastata,wõtnud jo kätte,kui Herrad näinud wiskand maha.Kristjan Tombek sai selle kaebtuse pärast küsitud,wastas et ta ühe kubu on tahtnud wõtta,et kraami koorma wahele panna,koied lõdwale läinud.
Otsus sai moistetud:Kristjan Tombek peab 1 rubla(üks rubla) trahwi walla laeka maksma et oli läinud wõtma.(N 1238,Kristjan Tombek wigane) Otsus kohe sai täidatud.
Hans Wolmüller/allkiri/  Kustaw Klaas/allkiri/ Jaan Surkaew XXX
