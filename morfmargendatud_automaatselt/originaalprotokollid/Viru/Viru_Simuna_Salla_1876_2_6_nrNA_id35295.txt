Ette astus Hindrik Juhkam, ja kaebas kohtule, et Willem Ellenberg temma pois on toast temma kue taskus 3 Rubla 50 cop. rahha warrastanud; Tulli ette Willem, ja sai küssitud kas temma sedda rahha on wõtnud, agga ta waidles wasto, et temma sest ühtigi ei tea, agga pärrast ütles temma 50 cop. wõtnud, ja on selle taggasi Hindrikile maksnud, ja Hindrik ütles - selle tõssi ollewad, et ta 50 copik. taggasi maksnud, ja lubbanud teine kord 3 Rubla maksta, ja pallunud, et Hindrik sedda kohto peale ei anna.
Kohhus mõisits, et Willem Ellenberg maksab se 3 Rubla Hindrikile wälja, ja saab trahwiks 15 witsa hopi.
Peawannem: W. Müllerbek XXX
abbi: K. Lõune XXX
teine: H. Turro XXX
