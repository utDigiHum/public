Tulli ette Hans Meiel Turro kohha pärris-piddaja, ja kaebas kohtule, et temma kohha rentnik Jaan Noppason, ei olle kohta mitte nende kauba ja kohtrakti järrele piddanud, ja on essimisel aastal, ilma pärris-perremehhe lubbada seiso metsast palka rajunud, teiseks kessa põldo on ilma lubbada rohkem wilja al prukinud, ja ühhe õue kopli mis heina ma olnud üllese künnud, ja weel kui pärris-perremees tedda kelanud, on Jaan tedda sõimanud. Tulli ette Jaan Noppason, ja tunnistas et temma sedda kül teinud olla, agga mitte nende kuidas Hans temma peale kaebanud, siis on Hans temale ülles annud, et ta tullewal kewadel peab lahti sama, siis kohhus nende kontraktid läbbi watanud, ja kohto laekase pannud, ja sedda asja selgemalt tahtnud järrel kulata.
Kohhus ei olle siis ühtegi mõistnud.
Peawannem: W. Müllerbek XXX
abbimes: K. Lõune XXX
teine: H. Turro XXX
