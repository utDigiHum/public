Tulli ette Hans Tobal ja kaebas kohtule, et Juhan Pilberg on tema 2 siga oma wäljalt kini ajanud, ja on teda ilma et sead temale miski kahjo teinud muido 1 Rubla trahwinud. Ette sai kutsutud Juhan Pilberg ja küsitud, kas need sead temale kurja teinud, ta ütles, et sead ei ole mitte kurja teinud waid tema wilja wäljal olnud, sepärast on tema kini ajanud, ja teda se Rubla trahwinud.
Kohus mõistnud:
Et Juhan Pilberg maksab 60 Cop. Hans Tobalile tagasi.
Peawanem: JKlaas XXX
abimees: H.Mein XXX
teine J.Elberg XXX
