# Automaatselt normaliseeritud ja morfoloogiliselt märgendatud vallakohtuprotokollid

Kaustas on **69 604** vallakohtuprotokolli originaaltekstid, automaatselt normaliseeritud ja morfoloogiliselt märgendatud failid. Sisendtekstid pärinevad väljavõttest `'vallakohtud_11_mar_2022.csv'`, mis sisaldab 71 877 protokolli. Sellest on välja jäetud praeguse Läti aladele jääva kunagise Volmari (Valmiera) maakonna Ruijena (Ruhja) kihelkonna protokollid. Normaliseerimiseks on kasutatud statistilist masintõlget ning materjali ei ole käsitsi parandatud, mistõttu esineb normaliseeritud ja märgendatud protokollides ka üksjagu vigu. Masintõlke eksperimendid on kirjeldatud repositooriumis https://github.com/gerthjaanimae/csmt-parish-court-records.   

* Kaustas **originaalprotokollid** on Rahvusarhiivi 2022. aasta väljavõte ühisloome käigus sisestatud txt-formaadis protokollidest (ajalooliste) maakondade kaupa.  
* Kaustas **normaliseeritud_protokollid** on automaatselt normaliseeritud ehk tänapäeva eesti keelele lähendatud protokollide xml-failid, kus on eraldi märgendatud päis (`head`), selle sees murdeinfo (`vald`), protokolli koostamise aeg (`aeg`), ning protokolli sisuosa (`sisu`).  
* Kaustas **normaliseeritud_margendatud_protokollid** on tsv-failid, mis on saadud normaliseeritud protokollide automaatsel morfoloogilisel märgendamisel. Iga (normaliseeritud) tekstisõne on eraldi real, sõnele järgevad lemma, pöörde- või käändelõpu, kliitiku, sõnaliigi ja morfoloogilise info väljad, mis on eraldatud tabulaatoriga (`\t`). Märgendamisel on kasutatud [EstNLTK-d](https://github.com/estnltk/estnltk) ja seal kasutatud [märgendeid](https://github.com/estnltk/estnltk/blob/main/tutorials/nlp_pipeline/B_morphology/00_tables_of_morphological_categories.ipynb). Analüüse ei ole ühestatud, mis tähendab, et ühel sõnel võib eraldi ridadel olla mitu erinevat analüüsi, näiteks  

|   |   |   |   |   |   |
|---|---|---|---|---|---|
|sai|saa|i||V|s|
||sai|0||S|sg n|  
  

Failinimed on struktureeritud kujul *Maakond_Kihelkond_Vald_aasta_kuu_päev_number_idkood.laiend*. Mõnel protokollil võib olla puudu info kuu, päeva või protokolli numbri kohta, mispuhul on failinimes vastaval kohal tekst `NA`.    
Failid on UTF-8 kodeeringus.    