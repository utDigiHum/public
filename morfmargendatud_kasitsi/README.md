# Käsitsi kontrollitud morfoloogiliste analüüsidega vallakohtuprotokollid

Kaustas on **587** käsitsi kontrollitud vallakohtuprotokolli, kokku **211 662** sõnet (sh kirjavahemärgid ja numbrid).  

Failid jaotuvad (ajalooliste) maakondade põhjal järgmiselt:  
Harju	61  
Järva	74  
Lääne	57  
Pärnu	50  
Saare	66  
Tartu	59  
Viljandi	70  
Viru	43  
Võru	107  

Failinimed on struktureeritud kujul *Maakond_Kihelkond_Vald_aasta_kuu_päev_number_id_idkood.tsv*.  
Failid on UTF-8 kodeeringus. Väljasid eraldab tabulaator (`\t`).  
Failidel puudub päiserida. Väljad näitavad järgmist infot:  
`originaali	tekstisõne->standardiseeritud tekstisõne->lemma->lõpp->kliitik->sõnaliik->vormiinfo`

