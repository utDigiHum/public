# README #

Siin kaustas asuvad riikliku programmi "Eesti keel ja kultuur digiajastul" projekti "Ajalooliste tekstide automaatse analüüsi võimalused Eesti 19. sajandi vallakohtuprotokollide näitel" avalikud materjalid.
Materjalide aluseks on Eesti Rahvusarhiivi ühisloomeprojekti "[Vallakohtud](https://ra.ee/vallakohtud)" andmebaasi versioon 2019. aasta detsembri seisuga.

### Projekti lühikirjeldus ###
Eesti 19. sajandi vallakohtuprotokollid on tänu oma temaatikale, iseloomule ja süstemaatilisusele üheks olulisemaks eesti keele ja kultuuri ajaloo uurimise objektiks, peegeldades ühtviisi nii tollase talurahva eluolu, majanduslikku seisundit, kõikvõimalikke üleastumisi ja elukorralduse häireid, üldist mentaliteeti kui ka eesti kirjakeele kujunemise protsessi. 2019. a kevadel Rahvusarhiivis käivitatud ühisloomeprojekt, mille käigus on sisestamisel tuhandeid protokolle, avas võimaluse selle rikkaliku allikmaterjali mitmekesisemaks uurimiseks. Käesoleva projekti põhieesmärgiks on vallakohtuprotokollide kasutusvõimaluste oluline laiendamine. Selleks töötatakse välja ajalooliste tekstide automaattöötluse metodoloogia, rakendades tekstide normaliseerimist, morfoloogilist analüüsi, automaatse nimetuvastuse jm lahendusi. 

### Kaustas on (seisuga 12.02.2024) ###

* Kasutatava andmebaasi ülevaatlikud joonised ja skript (kaust *andmebaasi_ylevaade*)  
* Automaatselt normaliseeritud ja morfoloogiliselt märgendatud protokollid (kaust *morfmargendatud_automaatselt*)  
* Käsitsi parandatud ja kontrollitud morfoloogiliselt märgendatud protokollid (kaust *morfmargendatud_kasitsi*)  
* Automaatselt nimeüksustega märgendatud protokollid (kaust *nimeuksustega_automaatselt*)  
* Käsitsi nimeüksustega märgendatud protokollid (kaust *nimeuksustega_kasitsi*)  
* Nelja kihelkonna (Kiltsi, Kirna, Parila ja Võnnu) hagejate ja kostjate võrgustikuanalüüsi sisend- ja väljundfailid, Pythoni skriptid võrgustikuanalüüsi mudelite tegemiseks, väljundjoonised, kihelkondade kaart ja analüüsi töövoo kirjeldus (kaust *vorgustikuanalyys*).  
* Tabelkujul ülevaade andmebaasi failidest ning nende sõnade arvust (*sonade_arv_failides.txt*)  
* Failides esinevate sõnavormide sagedusloendid maakonniti (kaust *sagedusloendid*)  
* Sagedamate morfoloogilisele analüsaatorile tundmatute sõnavormide käsitsi lisatud analüüsid (kasutajasõnastik) (*teisendused_2018_analyysiga_2020.srt*)  
* Tabelkujul ülevaade andmebaasi failidest, nende sõnade ning analüsaatorile tundmatuks jäänud sõnade arvust pärast analüüside täiendamist lihtsama kasutajasõnastikuga (*sonade_ja_tundmatute_arv_failides_parast_asendusi.csv*)  
* Vallakohtuprotokollide 2022. aasta väljavõte (fail vallakohtud_11_mar_2022.csv)  
* 2023. aasta normaliseerimise jätkuprojekti käigus loodud uus korpus masinõppe-põhiste normaliseerimise katsete jaoks (kaust *jatkuprojekt_2023/normaliseeritud-protokollid*)    
* 2023. aasta normaliseerimise jätkuprojekti käigus läbi viidud masinõppe-põhise normaliseerimise katsete tulemused (kaust *jatkuprojekt_2023/normaliseerimise-tulemused-testhulgal*)  

### Viitamine ###

Materjalid on kasutamiseks CC-BY-SA litsentsiga, mis tähendab, et materjale võib muuta ja jagada samadel tingimustel, ent nende kasutamisel palume viidata Rahvusarhiivile ning ETAGi projektile [EKKD29 "Ajalooliste tekstide automaatse analüüsi võimalused Eesti 19. sajandi vallakohtuprotokollide näitel"](https://www.etis.ee/Portal/Projects/Display/290ac93b-8f98-4fd1-b2cf-9b1b5deee24e).  

Projektiga on seotud järgmised publikatsioonid:  

* Pilvik, Maarja-Liisa; Muischnek, Kadri, Jaanimäe, Gerth; Lindström, Liina; Lust, Kersti; Orasmaa, Siim; Türna, Tõnis (2019). Möistus sai kuulotedu: 19. sajandi vallakohtuprotokollide tekstidest digitaalse ressursi loomine. _Eesti Rakenduslingvistika Ühingu aastaraamat = Estonian papers in applied linguistics_ 15, 139−158. DOI: http://dx.doi.org/10.5128/ERYa15.08
* Jaanimäe, Gerth (2021). Ajalooliste tekstide normaliseerimine. _Eesti Rakenduslingvistika Ühingu aastaraamat = Estonian papers in applied linguistics_ 17, 47−59. http://dx.doi.org/10.5128/ERYa17.03
* Lust, Kersti, Türna, Tõnis (2021). Valdade iseseisvumise raske algus: vallakirjutajad 1866-1891. _Tuna. Ajalookultuuri ajakiri_ 24(3), 10−32. https://www.ra.ee/tuna/valdade-iseseisvumise-raske-algus-vallakirjutajad-1866-1891-lk-10-32/    
* Kalkun, Andreas; Kersti Lust (2021). Külahoorad folkloori ja kohtumaterjalide peeglis. _Keel ja Kirjandus_ 64, 187−210. https://doi.org/10.54013/kk759a1
* Lust, Kersti (2021). A not so undesirable status? Widowhood options and widows' living conditions in post-emancipation rural Estonia. _The History of the Family_ 26(1), 74−99. https://doi.org/10.1080/1081602X.2020.1796749
* Poska, Kristjan (2021). Nimeolemite tuvastamine 19. sajandi vallakohtu protokollides. Bakalaureusetöö, Tartu Ülikool. https://dspace.ut.ee/handle/10062/74471
* Taimre, Liisi; Rahi-Tamm, Aigi; Lepa, Sven; Türna, Tõnis (2022). From Community Involvement to Research Interests: Crowdsourcing Projects of the National Archives of Estonia. In Katarzyna Pepłowska, Magdalena Wiśniewska-Drewniak (Eds.), _Documents, Archives, Contexts_, 167−182. Göttingen: Brill, Vandenhoeck & Ruprecht Verlage. (Geschichte im mitteleuropäischen Kontext; 3). https://doi.org/10.14220/9783737014724.167
* Jaanimäe, Gerth (2022). Challenges Of Using Character Level Statistical Machine Translation For Normalizing Old Estonian Texts. In Karl Berglund, Matti La Mela, Inge Zwart (Eds.), _Proceedings of the 6th Digital Humanities in the Nordic and Baltic Countries Conference (DHNB 2022)_, 235−243. Uppsala: CEUR-WS. (CEUR Workshop Proceedings; 3232). https://ceur-ws.org/Vol-3232/paper21.pdf
* Orasmaa, Siim; Muischnek, Kadri; Poska, Kristjan; Edela, Anna (2022). Named Entity Recognition in Estonian 19th Century Parish Court Records. In _Proceedings of the Language Resources and Evaluation Conference_, 5304−5313. European Language Resources Association. https://aclanthology.org/2022.lrec-1.568
* Lust, Kersti; Orasmaa, Siim; Maarja-Liisa Pilvik (2023). Kes kellega kohut käis? Vallakohtuprotokollide analüüs. _Acta Historica Tallinnensia_ 29(1), 35−64. https://doi.org/10.3176/hist.2023.1.02


### Kontakt ###

Projekti juht Aigi Rahi-Tamm (aigi.rahi-tamm[at]ut.ee).  
