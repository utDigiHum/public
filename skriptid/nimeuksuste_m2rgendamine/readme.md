## Nimeüksuste automaatmärgendamine

Siin kaustas on töövoog nimeüksuste automaatmärgendamiseks vallakohtu protokollide korpuses. Sisendtekstid pärinevad vallakohtu protokollide väljavõttest `'vallakohtud_11_mar_2022.csv'`. Märgendamisel kasutatakse peenhäälestatud Est-RoBERTa mudelit (kood: [https://github.com/soras/vk_ner_lrec_2022](https://github.com/soras/vk_ner_lrec_2022)) ning tulemused salvestatakse [EstNLTK json formaadis](https://github.com/estnltk/estnltk/blob/main/tutorials/converters/json_exporter_importer.ipynb).

### Nõuded

* Python 3.9+
* estnltk 1.7.2+
* estnltk_neural 1.7.2+
* transformers   4.0.0+
* sentencepiece
* tqdm

Märgendamiseks vajalik mudel laetakse töövoo käivitamisel alla automaatselt. 
Vajadusel võib mudeli alla laadida ka käsitsi `huggingface_hub` abil: 

```
from huggingface_hub import snapshot_download
snapshot_download('tartuNLP/est-roberta-hist-ner')
```

### Töövoo käivitamine

Töövoo käivitamiseks peab sisendkaustas olema protokollide CSV väljavõtte fail (vaikimisi: `'vallakohtud_11_mar_2022.csv'`). Käivitamine:

```
python process_protocols_with_ner.py
```

Tulemused salvestatakse vaikimisi asukohta `'../../nimeuksustega_automaatselt'`.