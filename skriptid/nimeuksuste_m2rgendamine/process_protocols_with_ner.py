#
#   Applies BERT-based named entity recognition on communal 
#   court protocols. Uses the best model from experiments: 
#   https://github.com/soras/vk_ner_lrec_2022/
#
#   Requires:
#      estnltk        1.7.2+
#      estnltk_neural 1.7.2+
#      transformers   4.0.0+
#      sentencepiece  
#      tqdm
#
#   The CSV snapshot file of communal court protocols must 
#   be in the same directory as this script. Defaults to 
#   using file 'vallakohtud_11_mar_2022.csv', but you can 
#   change the file name/path in the in_file parameter below.
#
#   By default, saves outputs to path: 
#   '../../nimeuksustega_automaatselt'.
#   For each county ("maakond"), a subdirectory will be 
#   created, which contains annotated files in EstNLTK's  
#   json format.
#

from collections import defaultdict
import os, os.path
import sys
import re
import csv
from tqdm import tqdm
from datetime import datetime

from estnltk import Text
from estnltk.converters import text_to_json

in_file = 'vallakohtud_11_mar_2022.csv'
assert os.path.exists(in_file), '(!) Missing input file {in_file!r}'

output_dir = '../../nimeuksustega_automaatselt'
os.makedirs(output_dir, exist_ok=True)

# If set, then keeps only Bert's NER layer and 
# removes all other annotation layers
remove_redundant_layers = True

# If set, then skips processing existing files
skip_existing_files = True

# Load preprocessing utils
from data_preprocessing import TokenizationPreprocessorFixed
preprocessor = TokenizationPreprocessorFixed()

# download the model from huggingface.co (or use the model from transformers cache, if already downloaded)
from bert_ner_tagger import BertNERTagger
hf_model_id = 'tartuNLP/est-roberta-hist-ner'
bert_ner_tagger = BertNERTagger(bert_tokenizer_location=hf_model_id, 
                                bert_ner_location=hf_model_id, output_layer='bert_ner',
                                token_level=False, do_lower_case=False, use_fast=False)

# 
def normalize_file_name( fname ):
    fname=fname.replace('ä','2')
    fname=fname.replace('Ä','2')
    fname=fname.replace('õ','6')
    fname=fname.replace('Õ','6')
    fname=fname.replace('ü','y')
    fname=fname.replace('Ü','Y')
    fname=fname.replace('Ö','8')
    fname=fname.replace('ö','8')
    fname=fname.replace(' ','')
    fname=fname.replace(')','')
    fname=fname.replace('(','-')
    return fname

start = datetime.now()
_counter = defaultdict(int)
with open( in_file, 'r', encoding='utf-8', newline='' ) as csvfile:
    csv_reader = csv.reader(csvfile, delimiter='|', quotechar='"')
    first_row = []
    data = []
    rows = 0
    tqdm_progress = tqdm(unit="docs")
    for row in csv_reader:
        if not first_row:
            first_row = row
            continue
        # 1) Gather document content and metadata. Clean content from HTML
        assert len(row) == len(first_row)
        items = dict()
        for rid, key in enumerate(first_row):
            items[key] = row[rid]
            if key == 'text':
                # Remove HTML annotations
                items[key] = re.sub('</p>', '</p>\n', items[key])
                items[key] = re.sub('<[^<>]+>', '', items[key])
                items[key] = re.sub('\n{3,}', '\n\n', items[key])
                items[key] = re.sub('\t', ' ', items[key])
        tqdm_progress.set_description("Processing %s" % items['id'])
        
        # Determine output file name & path
        output_subdir = os.path.join(output_dir, 
                                     normalize_file_name(items['maakond']))
        os.makedirs(output_subdir, exist_ok=True)
        loc = '_'.join([items['maakond'], items['kihelkond'], items['vald']])
        norm_fname = normalize_file_name( loc+'_id'+items['id']+'_'+items['year']+'a.json')
        output_fpath = os.path.join(output_subdir, norm_fname)
        if skip_existing_files and os.path.exists(output_fpath):
            # Skip existing file
            tqdm_progress.update(1)
            continue
        
        # 2) Make estnltk's Text object & detect named entities
        text = Text( items['text'] )
        for rid, key in enumerate(first_row):
            if key != 'text':
                text.meta[key] = items[key]
        # Preprocess text and apply NER
        try:
            preprocessor.preprocess(text)
            bert_ner_tagger.tag(text)
        except Exception as err:
            print(f'Failed processing row {rows!r}, document {items["id"]!r} : {err}')
            raise err
        # 3) Remove all redundant layers
        if remove_redundant_layers:
            for layer in text.layers:
                if (layer != bert_ner_tagger.output_layer) and (layer in text.layers):
                    text.pop_layer(layer)

        # 4) Save annotated json file
        text_to_json(text, file=output_fpath)
        _counter[output_subdir] += 1
        tqdm_progress.update(1)

        rows += 1
        # for testing
        #if rows >= 10:
        #    break
    tqdm_progress.close()

print()
print(f'Documents processed: {rows}')
print(f'Total time elapsed:  {datetime.now()-start}')

