# ==========================================================
#   Utilities for:
#
#   * Creating fixed tokenization layers ('words', 
#     'sentences') for the NER experiments;
#
#   Requires:
#      estnltk      1.7.0+
#      transformers 4.0.0
#      tqdm
#
#   Original source: 
#      https://github.com/soras/vk_ner_lrec_2022
#
# ==========================================================

import sys, os, os.path
import regex as re
from shutil import copy2

from collections import defaultdict
from tqdm import tqdm

from estnltk.converters import text_to_json
from estnltk.converters import json_to_text

from estnltk import Layer
from estnltk.text import Text
from estnltk.taggers import Retagger
from estnltk.taggers import TokenSplitter
from estnltk.taggers import CompoundTokenTagger

# ====================================================
#   Token splitting from the first experiments:
#    https://github.com/pxska/bakalaureus/tree/main/experiments
# ====================================================

token_splitter = TokenSplitter(patterns=[re.compile(r'(?P<end>[A-ZÕÄÖÜ]{1}\w+)[A-ZÕÄÖÜ]{1}\w+'),\
                                         re.compile(r'(?P<end>Piebenomme)metsawaht'),\
                                         re.compile(r'(?P<end>maa)peal'),\
                                         re.compile(r'(?P<end>reppi)käest'),\
                                         re.compile(r'(?P<end>Kiidjerwelt)J'),\
                                         re.compile(r'(?P<end>Ameljanow)Persitski'),\
                                         re.compile(r'(?P<end>mõistmas)Mihkel'),\
                                         re.compile(r'(?P<end>tema)Käkk'),\
                                         re.compile(r'(?P<end>Ahjawalla)liikmed'),\
                                         re.compile(r'(?P<end>kohtumees)A'),\
                                         re.compile(r'(?P<end>Pechmann)x'),\
                                         re.compile(r'(?P<end>pölli)Anni'),\
                                         re.compile(r'(?P<end>külla)Rauba'),\
                                         re.compile(r'(?P<end>kohtowannem)Jaak'),\
                                         re.compile(r'(?P<end>rannast)Leno'),\
                                         re.compile(r'(?P<end>wallast)Kiiwita'),\
                                         re.compile(r'(?P<end>wallas)Kristjan'),\
                                         re.compile(r'(?P<end>Pedoson)rahul'),\
                                         re.compile(r'(?P<end>pere)Jaan'),\
                                         re.compile(r'(?P<end>kohtu)poolest'),\
                                         re.compile(r'(?P<end>Kurrista)kaudo'),\
                                         re.compile(r'(?P<end>mölder)Gottlieb'),\
                                         re.compile(r'(?P<end>wöörmündri)Jaan'),\
                                         re.compile(r'(?P<end>Oinas)ja'),\
                                         re.compile(r'(?P<end>ette)Leenu'),\
                                         re.compile(r'(?P<end>Tommingas)peab'),\
                                         re.compile(r'(?P<end>wäljaja)Kotlep'),\
                                         re.compile(r'(?P<end>pea)A'),\
                                         re.compile(r'(?P<end>talumees)Nikolai')])

# ========================================================

try:
    # EstNLTK 1.6.9(.1)
    from estnltk.taggers.text_segmentation.compound_token_tagger import ALL_1ST_LEVEL_PATTERNS
    from estnltk.taggers.text_segmentation.compound_token_tagger import CompoundTokenTagger
except ModuleNotFoundError as err:
    # EstNLTK 1.7.0
    from estnltk.taggers.standard.text_segmentation.compound_token_tagger import ALL_1ST_LEVEL_PATTERNS
    from estnltk.taggers.standard.text_segmentation.compound_token_tagger import CompoundTokenTagger
except:
    raise

def make_adapted_cp_tagger(**kwargs):
    '''Creates an adapted CompoundTokenTagger that exludes roman numerals from names with initials.'''
    try:
        # EstNLTK 1.6.9(.1)
        from estnltk.taggers.text_segmentation.patterns import MACROS
    except ModuleNotFoundError as err:
        # EstNLTK 1.7.0
        from estnltk.taggers.standard.text_segmentation.patterns import MACROS
    except:
        raise
    redefined_pat_1 = \
        { 'comment': '*) Names starting with 2 initials (exlude roman numerals I, V, X from initials);',
          'pattern_type': 'name_with_initial',
          'example': 'A. H. Tammsaare',
          '_regex_pattern_': re.compile(r'''
                            ([ABCDEFGHJKLMNOPQRSTUWYZŠŽÕÄÖÜ][{LOWERCASE}]?)   # first initial
                            \s?\.\s?-?                                        # period (and hypen potentially)
                            ([ABCDEFGHJKLMNOPQRSTUWYZŠŽÕÄÖÜ][{LOWERCASE}]?)   # second initial
                            \s?\.\s?                                          # period
                            ((\.[{UPPERCASE}]\.)?[{UPPERCASE}][{LOWERCASE}]+) # last name
                            '''.format(**MACROS), re.X),
         '_group_': 0,
         '_priority_': (4, 1),
         'normalized': lambda m: re.sub('\1.\2. \3', '', m.group(0)),
         }

    redefined_pat_2 = \
       { 'comment': '*) Names starting with one initial (exlude roman numerals I, V, X from initials);',
         'pattern_type': 'name_with_initial',
         'example': 'A. Hein',
         '_regex_pattern_': re.compile(r'''
                            ([ABCDEFGHJKLMNOPQRSTUWYZŠŽÕÄÖÜ])   # first initial
                            \s?\.\s?                            # period
                            ([{UPPERCASE}][{LOWERCASE}]+)       # last name
                            '''.format(**MACROS), re.X),
         '_group_': 0,
         '_priority_': (4, 2),
         'normalized': lambda m: re.sub('\1. \2', '', m.group(0)),
       }
    new_1st_level_patterns = []
    for pat in ALL_1ST_LEVEL_PATTERNS:
        if pat['comment'] == '*) Names starting with 2 initials;':
            # Replace this pattern
            new_1st_level_patterns.append( redefined_pat_1 )
        elif pat['comment'] == '*) Names starting with one initial;':
            # Replace this pattern
            new_1st_level_patterns.append( redefined_pat_2 )
        else:
            new_1st_level_patterns.append( pat )
    assert len(new_1st_level_patterns) == len(ALL_1ST_LEVEL_PATTERNS)
    if kwargs is not None:
        assert 'patterns_1' not in kwargs.keys(), "(!) Cannot overwrite 'patterns_1' in adapted CompoundTokenTagger."
    return CompoundTokenTagger( patterns_1=new_1st_level_patterns, **kwargs )

# ========================================================
#   A shortcut class for fixed tokenization preprocessing   
# ========================================================

class TokenizationPreprocessorFixed:
    '''
    Provides fixed tokenization layers ('tokens', 'words', 'sentences') for NER experiments.
    Partly based on:
    https://github.com/pxska/bakalaureus/blob/main/experiments/modules/preprocessing_protocols.py
    
    Note that these tokenization fixes are by no means complete and do not solve all 
    the tokenization issues present in the historical language.
    '''

    def __init__(self):
        self.token_splitter = token_splitter
        self.cp_tagger = \
            make_adapted_cp_tagger(tag_initials=True, tag_abbreviations=True, tag_hyphenations=True)

    def preprocess( self, text ):
        text = text.tag_layer(['tokens'])
        self.token_splitter.retag( text )
        self.cp_tagger.tag( text )
        text.tag_layer('sentences')
        return text

