Rieseperre walla kohhus olli kohto korra pärrast walla kohto maiase kokko tulnud, kohto ette astus Liiwa jaama posti herra Juhhan Rikko, ja kaewas: Minno pöllo aia pealt kaddusid mitto aastad ikka roikad ärra, ja ka tännawu kaddusid sealt roikad ärra, ja minna ütlesin Pruli koddakondse Juhhan Sucharowi wasto kes sealt aia juurest ikka möda käis, sinna olled minno roikad ärrawinud? ja temma ütles et ta ei olle mitte wiinud, ja ükskord tulli Juhhan Sucharow jälle meile ja ütles: tulle waata kus teie aia roikad on, Pruli Marti juurdes, ja minna läksin Prulile, ja wötsin Sullase ja Seppa ka tunnistusseks kasa, ja leidsime Pruli Marti juurest nelli roigast, ja üks neist olli üks köwwer roigas, kellest teine weel mo pöllo aia peal on mis ühhest puust on löhhutud, ja minna pärrin et Pruli Mart selle aia peab ärra parrandama ja uued roikad jälle peale tooma.
Teiseks, on minnul kolm rubla rahha Pruli Marti käest saada, mis kolme aasta kui ta minno käest obbose ostis wölgo jäggi, ja üks wak kaero wöttis temma ka siis minno käest ja se on praego wölgo, sedda pärrin minna temma käest:
Kaebtusse kandja Pruli Mart Wöörberg astus kohto ette loetud, temma ütleb selle wasto: Minna ei olle mitte ühte roigast posti herra pöllo aia pealt toonud, ja minna ei tea kes need sealt on wiinud, ja üks köwer roigas olli Nissi laada aeges minno öue todud, ja minna ei tea kas on koddakondne Juhhan Sucharow sedda tonud wöi kes on toonud, sedda ma ei tea.
Js se kolm rubla rahha, ja üks wak kaero on mul posti herrale maksta, ja praego mul wöimalik ei olle sedda ärramaksta.
Kohhus mõistis sedda kaebtusse asja nende aia roigaste ülle teisel eestullewal kohto päwal kohto ees selletada wötta, siis peab ka Juhhan Sucharow kohto etteb kutsutud saama selle ülle tunnistust andma.
Teiseks möistis kohhus: sedda 3 rubla rahha, ja üks wak kaero peab Pruli Mart Wöörberg Küünla päwaks 1888a. posti herrale ärramaksma.

Kohtowannem: Jaan Sarik XXX
korwamehhed: Jürri Simson XXX
Jaan Lehbert XXX
Kirjutaja: KWiik [allkiri]
