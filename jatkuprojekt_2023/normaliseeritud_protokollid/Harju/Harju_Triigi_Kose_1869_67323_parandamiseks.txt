#Hakenrihtri kohto Härra kässo järrele sawad need Nömme jama piddaja kaebtussed Ado Pudro wargusse pärrast walla kohtost (uueste) läbbi kuulatud.


#Ette tulli


#Jula Sikkeme, ja tunnistab et Ado Sikkema on temmale räkinud, et Jürri Sikkeme ja Ado Pudro ja Tedo ollid (neljakesti) Wannamoisa rehhe jurest 12 tündred kartuhwlid warrastanud ja Ado Pudro lubband igga ühhele ühhe rubla anda, agga Jürrile 1 tünder kartuhwlid.


#Agga Jula Sikkeme ütles, et Jürri ei olle ennam kartuhwlid tonud, kui kaks wakka.


#2) Marri Murtel tunnistab et Ado Pudro on temmale ka kaks küllemitto kartuhwlid annud, kartuhwli teggemisse eest, sest temma aidand Ado Pudrul kaks tündred Nömme kartuhwlid mahha tehha.


#3) Marri Linder tunnistab et ta Ado Pudro käest on sanud 6 toopi sedda modi kartuhwlid kui Nömmes on olnud. 


#Waidleminne


#Hans Tas läks Ado Sikkeme jure metsast, ja ütles, mis sest Wannamoisa asjast tulleb, Kui Ado Pudro ikka mees on. Ado Sikkeme ütles Hans Tassi wasto, minne sa Wannamoisu, ja ütle Ado Pudrole, et ta poisike ei olle, et ta üllesse ütleb, ja Ado ütles, et need kartuhwlid on ta käe.


#Agga Ado Sikkeme ajas sedda taggasi, et ta pölle sedda üttelnud, egga metsawaht poisike ei olle.


#4) Jürri Paimets tunnistab minna aitasin kewwade Ado Pudro kartuhwlid augost wälja wötta, ja ei olnud ennam kui kaks tündred.


#Ado Pudro ütles, minna ei olle sind mitte näinud, kui ma omma kartuhwlid augost wälja wötsin.


#Nüüd kutsus kohhus Ado Pudro ette, ja ütles ärra aja ennam taggasi, ütle üllesse, et sa olled kartuhwlid warrastanud. Ado Pudro ütles, minna wannun selle peale, et minna ei olle warrastanud.


#Ado Pudro ütles essimesse korral, et ta 6 tündred kartuhwlid on mahha teinud.


#2) Ado Pudro ütles, et need tunnistajad rahha ette palgatud, mis Nömme jama piddajal on.


#Paunkülla koggokonna kohhus ei wöinud moista selle peale ühtegi, waid jättis sedda asja surema kohto holeks selletada.


#Peakohtomees Erik Metsis XXX


#Korwasmehhed Magnus Elbaum XXX


#ja Karel Marnot XXX


#Rein Joor walla kirjotaja


