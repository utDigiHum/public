#Tallitaja Jaan Staub kaebas kogg.kohto ette,et Allo perremees Jürri Ridebach ei maksa omma Postirahha 1.Rbl.40.Kop mitte ärra.
Talitaja Jaan Staub kaebas kog.kohtu ette,+et Allo peremees Jüri Ridebach ei maksa oma postiraha 1.+rbl.+40.+kop mitte ära.

#Ette kutsutud:Jürri Ridebach.Se wastas,minna maksin selle Rahha walla Kirjutaja Mart Kerdi kätte,se olli kolmandamal Nell Lihhawetti pühhal peale löunat Ladamäe körtsi kambris laua peal,ja Tallitaja Abbimees Jürri Lipstahl tunnistas:kui minna Jürri Ridebahil käsko andsin selle Rahha pärrast,siis se wastas kohhe ilma koggeldamatta,minna ollen Rahha ärra maksnud Walla kirjutaja kätte,ja arwas et Jürri Ridebahil wist öigus on.
Ette kutsutud:+Jüri Ridebach.+See vastas,+mina maksin selle raha valla+ +kirjutaja Mart Kerdi kätte,+see oli kolmandal ¤ lihavõtte pühal peale lõunat Ladamäe kõrtsi kambris laua peal,+ja talitaja abimees Jüri Lipstahl tunnistas:+kui mina Jüri Ridebahil käsku andsin selle raha pärast,+siis see vastas kohe ilma kogelemata,+mina olen raha ära maksnud valla+ +kirjutaja kätte,+ja arvas et Jüri Ridebahil vist õigus on.

#Ette kutsutud:Mart Kerdi.Kohhus küssis temma käest,kas ta on 1 Rbl.40 Kop wasto wötnud Jürri Ridebahi käest?Se wastas,ma ei mälleta mitte,kas ta on maksnud wöi ei,kui ta olleks minno kätte maksnud,siis se olleks ka kirjas olnud,ja ma arwan kül et Jürri üks pettis mees ei olle,agga kui ma temmal wälja maksan,siis wöiwad mitto teist ka pärrima tulla.
Ette kutsutud:+Mart Kerdi.+Kohus küsis tema käest,+kas ta on 1 rbl.+40 kop vastu võtnud Jüri Ridebahi käest?+See vastas,+ma ei mäleta mitte,+kas ta on maksnud või ei,+kui ta oleks minu kätte maksnud,+siis see oleks ka kirjas olnud,+ja ma arvan küll et Jüri üks petis mees ei ole,+aga kui ma temal välja maksan,+siis võivad mitu teist ka pärima tulla.

#Kohto otsus:Kohhus tundis Jürri Ridebahi juttust,et temmal wöib öigus olla,agga et tal tunnistajad ei olnud,kes olleks näinud,et ta Rahha on Mart Kerdi katte annud,sellepärrast sundis kohhus neid ärra leppima.Mart Kerdi jäi köwwaks,ja ütles ma ei maksa mitte üks koppik,löi russikaga wasto lauda ning läks uksest wälla.
Kohtu+ +otsus:+kohus tundis Jüri Ridebahi jutust,+et temal võib õigus olla,+aga et tal tunnistajad ei olnud,+kes oleks näinud,+et ta raha on Mart Kerdi kätte andnud,+sellepärast sundis kohus neid ära leppima.+Mart Kerdi jäi kõvaks,+ja ütles ma ei maksa mitte üks kopikat,+lõi rusikaga vastu lauda ning läks uksest välja.

#Tulli teine kaebaja ette Allo perremees Mart Martin,ja ütles temma maksnud omma Pearahha 1.Rbl.55 Kop ka Mart Kerdi kätte,agga se ei olle sedda mitte kirja pannud,ja Tallitaja nöuab Pearahha Tagga!Mart Kerdi aias sedda ka walleks,sellepärrast andis kohhus neile lubba omma asja Keiserlikko Kihhelkonna kohto ette wiia.
Tuli teine kaebaja ette Allo peremees Mart Martin,+ja ütles tema maksnud oma pearaha 1.+rbl.+55 kop ka Mart Kerdi kätte,+aga see ei ole seda mitte kirja pannud,+ja talitaja nõuab pearaha taga!+Mart Kerdi ajas seda ka valeks,+sellepärast andis kohus neile luba oma asja keiserliku kihelkonna+ +kohtu ette viia.

#Kohtomees:Jaan Tiismann +++,Abbimehhed:Mihkel Wiikberg +++,Jaan Esner +++,Kirjutaja A.Eichen
Kohtumees:+Jaan Tiismann +++,+abimehed:+Mihkel Viikberg +++,+Jaan Esner +++,+kirjutaja A.+Eichen

