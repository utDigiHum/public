II) Tulli kohto ette Rieseperre moisa wallitsuse kaebdus (sest 25 Aprillist 1877) et Wiso Pritso perremees Hans Wiik on oma heina koplist puid warrastanud, nenda kui allamal ülles tähhendud on; sellepärrast sai ette kutsutud metsawaht Kiwisilla Hans Enoh kes tunnistas: et se moisa wallitsuse kaebduse kirja peal ülles antud pude summa öige on ja et tema need kännud keik ülle möetnud ja nenda kui siin ülles antud, on leidnud ja et need kännud keik ennamist kinni maetud olnud.
Kohto ette kutsuti Pritso Hans Wiek, kes üllemal kaebduse järrel oma heina koplest need puud on warrastanud se on:

17 kaske
			7 kunni 13 tolli kännust läbbi möet
		
5 Awa
			9 kunni 11 tolli " "
		
Kohhus küsis? kas Hans Wiek need üllemal tähhendud puud on raijonud; Hans Wiek ütleb: et tema need puud kül keik on raijonud, agga temma olla need kased raijnud muist linna puuks, mis moisa herra temale on lubband ja muist tarwis puuks ja need hawad peru puuks ja et need olnud sandid puud, kuiwa ladwaga ja heinama rissuks.
Kohhus moistis: sedda asja läbbi katsudes
1) öigust andes: et Hans Wiek oma heina koppelt puhhastand ja sandimad kuiwa ladwaga puud ärra raijonud ja ka et moisa herra polest temal olla lubbatud linna puuks üks süld kasse puid raijoda ja et ka mitte öiete teada ei olle, kas need head woi sandid puud olnud;
2) agga et tema need puud ilma moisale meldimata on raijonud ja ka et tema neid ilma moisale näitamata on koddo winud, selle pärrast peab Hans Wiek moisale kahjo tassumiseks 9 Rubla ja trahwiks walla laeka 1 Rbl maksma; mis ka Hans Wiek kohto laua peale kohhe wälja maksis.
Kohto peawannem: Jakub winkel [allkiri]
Körwamehhed: Jurri Simson XXX
Hans Surow XXX
Kirjotaja; BGildeman [allkiri]
