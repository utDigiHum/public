#Kohtokorra pärast, olid walla kohto honese kokku tulnud.


#Kohtowanem Aadu Wilibert


#Körwamehed Jüri Lusmann


#Jaan Saulep


#


#Kohto ete kaebas möesa lamba sihwer Jüri Kerdi, et wanger, mis ligi kolme aasta eest ära warrastud Kehtna möesa lamba talli jurest, nüid on leitud Piometsa mehe Hans Kaltmanni käest.


#Kutsuti kohto ete Piometsa walla mees Hans Kaltmann ja küssiti; kusts sa selle wankre wötsid? Wastus: ma ostsin Keawa walla mehe Jüri Trumanni käest. Küssiti, kas sa teadsid, et se warrastud wanker oli? Wastus: ei teadnud, ta ütles et on Lallilt Wannamatsi peremehe käest ostnud. Küssiti Millal sa ostsid? Wastus: Küinlapä sai aasta.


#Kutsuti ete Jüri Trumann ja küssiti, kas sa müisid tale wankre? Wastus: Jah ma müüsin. Küssiti kas ütlesid tale kust sa selle ostsid? Wastus: ma ütlesin et Lalli mehe käest ostsin. Küssiti, kust sa selle wankre siis said mis sa tale müisid ja Kehtna möesa wankreks tunistakse? Wastus ma ostsin Hans Watmanni käest. Küssiti Kus kohtas? Wastus: rohhuaija taga mandi peal. Kui palju sa tale selle eest andsid? Wastus: 3 rubla 50 kopiki.


#Kutsuti ette Hans Watmann ja küssiti ka sa müisid Jürile wankre? Wastus: kes teab seda müimast, eks se saand seltsis toemetud. Küssiti, kas pakkusid tale? Wastus: eks ma ikka pakkund kül, aga ega ma ta käest raha kohhe saand, nattuke awalt sain, ja nüid on ta mulle juba nipalju kui 1 rubla 55 kopiki selle eest maksnud.


#Kohto otsus sai möistetud. Jüri Trumann maksab 6 ruba 75 kopiki Hans Watmann 6 ruba 75 kopiki 6 rubla saab Sihwer wankre hinda mis ta herrale wankre eest maksnud ja 1 rubla wankre otsimese eest 50 kopik sai Aadu Andres selle eest et kirjaga Piometsas käis 6 rubla walla laeka Hans Watmannile 40 hopi witso nuhtlust ja kohhe kätte antud.


#


#öiendud


#


#Kohtowannem Aadu Wilibert XXX


#Körwamehed Jüri Lusmann XXX


#Jaan Saulep XXX


#Kirjutaja T: Weidenbaum


