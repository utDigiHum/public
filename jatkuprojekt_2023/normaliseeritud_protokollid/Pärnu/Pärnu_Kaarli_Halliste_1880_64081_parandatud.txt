#Kaarli mõtsa härra J Limberg tulli ette ja kaebas 13 Aprillil as.a. õhtu olla Sallako mõtsa waht Johann Päsuke Kaarli moisa metsas Sallaku mõtsa jäo sees Püssi paukud kuulnud ja se peale on jahi (neid) waatama läinud ja leidnud, et kaks meest Sallako metsas olnud. teine neist on ärra jooksnud ja Urrite tallo perremehe Mats Leimani on tema käte saanud. kelle püssi Päsuke mõisa hära kässo ehk Sääduse peale ärra pidanud wotma, mõtsa säätuse rikuja käest.
Kaarli metsa+ +härra J Limberg tuli ette ja kaebas 13. aprillil as.a. õhtu olla Sallako metsa+ +vaht Johann Pääsuke Kaarli mõisa metsas Sallaku metsa+ +jao sees püssi+ +paukusid kuulnud ja see+ +peale on vahi (neid) vaatama läinud ja leidnud, et kaks meest Sallako metsas olnud. teine neist on ära jooksnud ja Urrite talu peremehe Mats Leimani on tema kätte saanud. kelle püssi Pääsuke mõisa+ +härra käsu ehk seaduse peale ära pidanud võtma, metsa+ +seaduse rikkuja käest.

#Kui Päsuke (tema) Leimanni käest püssi ärra wõtta tahtnud olla Leimann püssi otsa Päsukse poole kändnud nende sanadega Kui sa püssi lahti ei lasse, et siis püssi lahti lassen sino peale ja sind maha ja mõtsa härra Limberg pallus Sallaku Johannes Päsukse kui tunnistajad üle kulata ja asja ülles wotta ja Silla kohtu üllekulamisele saata.
Kui Pääsuke (tema) Leimanni käest püssi ära võtta tahtnud olla Leimann püssi+ +otsa Pääsukese poole kandnud nende sõnadega Kui sa püssi lahti ei lase, et siis püssi lahti lasen sinu peale ja sind maha ja metsa+ +härra Limberg palus Sallaku Johannes Pääsukese kui tunnistajat üle kuulata ja asja üles võtta ja Silla kohtu ülekuulamisele saata.

#Mats Leimann wastas kaebtuse peale, ta ei olle mitte Kaarli mõtsas Jähi peal käinud, ja tunistab eeseiswa kaebtuse waleks.
Mats Leimann vastas kaebuse peale, ta ei ole mitte Kaarli metsas jahi peal käinud, ja tunnistab eesseisva kaebuse valeks.

#Mõtsawaht Johann Päsuke antiis tunnistus 13 Aprillil s.a. Kaarli mõtsas Sallako mõtsa jao sees on tunnistaja 3 Püssi pauko kuulnud ja se peale watma läinud ja leidnud kaks jahi meest Kaarli mõtsast teine olla ärra jööksnud Kedda ta mitte ohtu pimeduse pärrast selgeste näha ning tunda ei woinud. Ja Urrite perre mehe Mats Leimanni on tunnistaja kätte saanud ja tema kiha ümber olewa Rihma sisse oma käe pnntnud ja ütelnud et se kowaste keeltud on et mitte Kaarli mõtsa jähi peale kegi ei pea tulema ja moisa härra von Siversi on nimmelt nende käest püssid ärra wõtta käskinud kes püssiga mõtsas on. ja tahtnud tunnistaja Leimanni käest püssi ärra wotta, siis olla kaebatu Püssi tunnistaja rinda wastu pantnud nende sanadega siis ei olle muud, püssi ma kätte ei anna kui meist teine peab surema.
Metsavaht Johann Pääsuke andis tunnistuse 13. aprillil s.a. Kaarli mõisas Sallako metsa+ +jao sees on tunnistaja 3 püssi+ +pauku kuulnud ja see peale vaatma läinud ja leidnud kaks jahi+ +meest Kaarli mõisast teine olla ära jooksnud keda ta mitte õhtu+ +pimeduse pärast selgesti näha ning tunda ei võinud. Ja Urrite pere+ +mehe Mats Leimanni on tunnistaja kätte saanud ja tema keha ümber oleva rihma sisse oma käe pannud ja ütelnud et see kõvasti keelatud on et mitte Kaarli metsa jahi peale keegi ei pea tulema ja mõisa+ +härra von Siversi on nimelt nende käest püsid ära võta käskinud kes püssiga metsas on. ja tahtnud tunnistaja Leimanni käest püssi ära võtta, siis olla kaevatu püssi tunnistaja rinda vastu pannud nende samadega siis ei ole muud, püssi ma kätte ei anna kui meist teine peab surema.

#Kui kohttu käiadel keige wähemad enam ette tua ei olnud ühentasid kohtu liigmed endid selle
Kui kohtu+ +käijadel kõige vähemat enam ette tuua ei olnud ühendasid kohtu+ +liikmed endid selle

#Otsusele: Kaarli mõtsa härra J Limbergi pallumise peale saab eesseiswa assi Keiserliku Willandi Silla kohtu selletuse ja ülle kulamisele saatetud.
Otsusele: Kaarli metsa+ +härra J Limbergi palumise peale saab eesseisva asi keiserliku Villandi Silla kohtu+ +seletuse ja üle+ +kuulamisele saadetud.

#arwatud: keik kuidas sündinud: kirja panna.
arvatud: kõik kuidas sündinud: kirja panna.

#Peakohtumees: Kusta Lepik [allkiri]
Peakohtumees: Kusta Lepik [allkiri]

#kohtumees: Andres Saarme [allkiri]
kohtumees: Andres Saarme [allkiri]

# Jaak Raegson [allkiri]
 Jaak Raegson [allkiri]

