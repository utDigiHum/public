#Sel 13mal Septembril tullid need perremehhed Peter Koop ning Jaan Masing sia kohto ette, ning kaebasid, et Abia walla 3 Tõzimoisa perremeeste karri nende heinamaad, 10 wakkamaa alla iggal ühhel, ni ärra sõknud, et sealt pealt ennam middage ei sa. Ka ütlesid nemmad ommad kuhjad lõhhutud ollewad.
Sel 13mal septembril tulid need peremehed Peeter Koop ning Jaan Masing siia kohtu ette, ning kaebasid, et Abia valla 3 Tõsimõisa peremeeste kari nende heinamaad, 10 vakamaa alla igal+ +ühel, nii ära sõtkunud, et seal pealt enam midagi ei saa. Ka ütlesid nemad omad kuhjad lõhutud olevad.

#Teiste aastade sees on nemmad 2, ning ka 3 sagi häddalad wakkamaa peält sanud, ning nüüd on nemmad neist heinadest kohhald ilma.- Kui Peter Koop omma heinamaid watamas käinud, on temma liggi sada ellajad omma ning ka Jaani heinamaa sees luggenud.
Teiste aastade sees on nemad 2, ning ka 3 saaki nädalat vakamaa pealt saanud, ning nüüd on nemad neist heinadest kohe ilma.- Kui Peeter Koop oma heinamaid vaatamas käinud, on tema ligi sada elajat oma ning ka Jaani heinamaa sees lugenud.

#Peäle se käisid sedda kahjo watamas, 2 kohtomehhed ning need 2 Sempre perremehhed, Wanna Karriste poolt, kui ka 2 kohtomehhed ning need 3 Tõzimoisa perremehhed Abia poolt. Se perremees Tõzimoisa Peter, kel ni suur tallu on, kui neil teistel kahhel kokko, olli seäl sammas paigas selle Wanna Karriste perremehha Sempre Jaan Masinguga pole kahju peäle ärra leppinud, ning ni hennast sest asjast lahti teinud.-
Peale selle käisid seda kahju vaatamas, 2 kohtumeest ning need 2 Sempre peremeest, Vana Kariste poolt, kui ka 2 kohtumeest ning need 3 Tõsimõisa peremehed Abia poolt. See peremees Tõsimõisa Peeter, kel nii suur talu on, kui neil teistel kahel kokku, oli seal+ +samas paigas selle Vana Kariste peremehe Sempre Jaan Masinguga poole kahju peale ära leppinud, ning nii heinast sest asjast lahti teinud.-

#Kutsuti teistkorda ette Sempre Peter Koop, ning küssiti, mis temma siis selle temmale tehtud kahjo eest nõuab.
Kutsuti teist+korda ette Sempre Peeter Koop, ning küsiti, mis tema siis selle temale tehtud kahju eest nõuab.

#Peter Koop ütles, et temma kige wähhemalt üks 25 sagi häddalad olleks sanud, nüüd, et temma mitte middagi ei olle sanud, nõuab temma 15 Rblhõbbedad.
Peeter Koop ütles, et tema kõige vähemalt üks 25 saaki nädalat oleks saanud, nüüd, et tema mitte midagi ei ole saanud, nõuab tema 15 rbl+hõbedat.

#Se peale ütlesid need 2 Tõzimoisa perremehhed, et nemmad sedda kahjo mitte üksinda ei wõi maksa, sest selle kolmandama perremehhe, Tõzimoisa Petre karri, kel ni samma paljo karja, kui neil mõllembil kokko, ka ommetegi seäl heinamaa sees olnud.
See+ +peale ütlesid need 2 Tõsimõisa peremeest, et nemad seda kahju mitte üksinda ei või maksta, sest selle kolmanda peremehe, Tõsimõisa Peetri kari, kel nii+ +sama palju karja, kui neil mõlemil kokku, ka ometi seal heinamaa sees olnud.

#Se peäle selletas neile Kohhus, et nende karri ka Sempre Jaani heinamaa sees olnud, ning Tõzimoisa Peter nende eest Sempre Janile ärra maksnud, ning et nüüd nende kohhus on, ka Tõzimoisa Petre eest jälle Sempre Petrele maksa.
See+ +peale seletas neile kohus, et nende kari ka Sempre Jaani heinamaa sees olnud, ning Tõsimõisa Peeter nende eest Sempre Jaanile ära maksnud, ning et nüüd nende kohus on, ka Tõsimõisa Peetri ees jälle Sempre Peetrile maksta.

#Sest nüüd, et kui Kohhus neid 2 Tõzimoisa perremehhi ning Sempre Petert käsksid issekeskis leppida, nemmad isse mitte toime es sa, sis mõistis Kohhus, et need 2 Tõzimoisa perremehhed selle kahjo eest, mis nende ellajad Sempre Petrele teinud, temmale peawad maksma.
Sest nüüd, et kui kohus neid 2 Tõsimõisa peremehi ning Sempre Peetrit käskisid isekeskis leppida, nemad ise mitte toime ei saa, siis mõistis kohus, et need 2 Tõsimõisa peremeest selle kahju eest, mis nende elajad Sempre Peetrile teinud, temale peavad maksma.

#1. 20 sae heinade eest (60 Kop h. saag) 12 Rbl hbed.
1. 20 sea heinade eest (60 Kop h. saag) 12 Rbl hbed.

#2. selle et se heinamaa wäega ärra sõkkutud on 2 - " -'
2. selle et see heinamaa väga ära sõkutud on 2 - " -'

#Summa 14 Rblhõbd
Summa 14 Rbl+hõb

#Tulleb kummagil 7 hõbbe rubla maksa.-
Tuleb kummalgi 7 hõbbe rubla maksta.-

