Jaak Neder tulli ette ja kaebas Paikuse Rätsep Märt Oija olla kaebajal üks kassuk teinud kus jures kaebaja üks terwe ranka nahk andnud kellest ka weel Märt Peipsi kassukul ranka nahk ümber pidanud pantud, kaebato olla aga sedda nahka mitte kaebaja kahe kasuka ümber keik ära pantnud waid üks jago sealt ärra warrastanud ja Jaan Kullama kassuka ümber pantnud. selle eest nouab kaebaja 8 Rb kahju tassumist. teiseks õlla kaebato weel üks kõwast ärra warrastanud kelle eest kaeba 50 Cop nouab. Kaebaja andis Jaan Kullama tunnistajaks ja Märt Peips.
Sepeale wastas kaebato se ranka nahk mis Kullama kassuka ümber pantud olla kaebatu omma olnud ja ta ei olle mitte üks rihm kaebaja ranka nahhast ärra warrastanud. ja kõvaskid kedda kaebaja ülles andnud ei olle ta mitte ärra warrastanud. tunnistajaks andis kaebatu ülles Jaak Lutsep.
Jaan Kullamaa andis tunnistust kui tunnistaja omma kassuka mõõtu andnud siis olla Needre kassuk töös olnud. kus peale siis tunistaja pallunud rätseppat ommast käest ranka nahka kassuka ümber panna. agga kaebatu lubbanud sedda ja pallunud tunnistajad sedda kellegit rägida et ta (rätsep) rangad ommast käest pannud.
Jaak Lutsep andis tunnistust ta olla jures olnud kui Kullama kassukud rätseppa jures tellinud tehja, agga sedda ta ei olla mitte kulnud et kaebatu tunnistajad pallunud sedda kellegil mitte rägida et ta ommast käest ranka nahka panneb.
Jaan Ristjaan ja Märt Peips antsid tunnistust naad olla rätsepa käest küssinud kas tal ranka nahka on, se peale olla rätsep wastanud mul ei olle mitte olleks teie pühha päew minnul sest räginud siis olleks ma ühhes tonud Utti poest sest seal ollid kul illusid nahku.
Jaan Ristjaan Andis tunnistust ta olla sedda näinud kui kaebatu kaebaja kowast ärra wiinud ja kasti pannud.
Märt Peips andis tunnistust ta olla näinud et kaebatu käes kaebaja kõwast olnud.
Kaebato Märt Oija wastas tunnistuste peale ta olla nendega riide rankasst räginud aga mitte nahha rankast. sest riide rankad tal ei olle mitte olnud waid nahk rankad olla ta koddust ühhes tonud ja andis tunnistajaks Jaak Lutsep kes sedda kuulnud.
Jaak Lutsep andis tunnistust ta olla jures olnud kui tunnistajad omast kassukidest räginud, siis olla kül riide ranka kõnnes olnud.
Märt Peips andis isse ka tunnistust et ta riide rangast räginud.
Kohtu käiad teggid sedda:
Leppitust: Märt Oija maksab Jaak Nedrel 2 rubla kahju tassumist et neil keige wähhemad rägimist ei olle pärrast sedda üksteise wasto eesseiswas asjas.
Peakohtumees Jaak Raegson [allkiri]
kohtumees Peter Leppik [allkiri]
 - " - Peter Kogger XXX
