#Johann Türk kaebtuse asjas Jaak Allik wastu noudmise asjas ollid kohtu käiad kui ka ülles antud tunnistaja Ann Petser sia ilmunud.
Johann Türk kaebuse asjas Jaak Alliku vastu nõudmise asja olid kohtu+ +käijad kui ka üles antud tunnistaja Ann Petser siia ilmunud.

#Ann Petser /Kaebatu wõeras tütar) andis kohtu küssimise peale wastust ta ei olle mitte Kaebaja jure Kuude eggad aasta peale teenistuse telinud waid päewade ja a päew 30Cp lubanud kaebaja maksa, Küssimise peale wastas Ann Petser weel et Türk temale ka 25 naela linnu lubanud anda palgast ei tea ta mitte palju Türk lubanud.
Ann Petser /kaevatu võõras+ +tütar) andis kohtu küsimise peale vastust ta ei ole mitte kaebaja juurde kuude ega aasta peale teenistuse tellinud vaid päevade ja a päev 30kop lubanud kaebajale maksta, küsimise peale vastas Ann Petser veel et Türk temale ka 25 naela linu lubanud anda palgast ei tea ta mitte palju Türk lubanud. 

#Kaebaja nouda weel kaebatu käest ühhe paari wenne sapaste eest keda kaebatu Rimmu poodist kaebaja rehknungi peale wotnud.
Kaebaja nõudis veel kaevatu käest ühe paari vene saabaste eest keda kaevatu Rimmu poest kaebaja rehnungi peale võtnud. 

#Kaebatu tunnistas seda tõeks, ja lissas sinna juure et ta need saapat olla isse need saapat ärra maksnud ja lubbas teisel kohtu päewal se ülle kwitung kohtule ette näitada. 
Kaevatu tunnistas seda tõeks, ja lisas sinna juurde et ta need saapad oli ise need saapad ära maksnud ja lubas teisel kohtu+ +päeval selle üle kviitungi kohtule ette näidata. 

#Kaebatu tunnistas kui ta kaebaja juures eesimist korda teenistuses olnud olla ta kaebajal 5 RB 50 Cp wõlgu jänud.
Kaevatu tunnistas kui ta kaebaja juurde esimest korda teenistuses olnud oli ta kaebajale 5 rb 50 kp võlgu jäänud. 

#Rimmu kaupmees Ferdinand Birk andis tunnistust Jaak Allik olla Türki rehknungi peale üks paar saapid ja üks pool toopi tekkutid wotnud sapad 220 Cp ja tekkud 15 kop summa 235 Cp - on praego alles maksmada.
Rimmu kaupmees Ferdinand Birk andis tunnistust Jaak Allik oli Türki rehnungi peale üks paar saapaid ja üks pool toopi ¤ võtnud saapad 220 kp ja ¤ 15 kop summa 235 kp - on praegu alles maksmata.

#Kaebaja wastas Kaebatu poolt ülles antud noudmise peale et se keik mitte tõssi ei olle mis Kaebatu nouda, waid üksi kaebatu naesel Karjas Käimise eest 3 Wakka rüke lubanud keda ta ka kätte maksnud.
Kaebaja vastas kaevatu poolt üles antud nõudmise peale et see kõik mitte tõsi ei ole mis kaevatu nõudis, vaid üksi kaevatu naisel karjas käimise eest 3 vakka rukkist lubanud keda ta ka kätte maksnud. 

#Kaebatu tunnistas et ta järrelseiswad asjad Kaebaja käest sanud, ja nimelt
Kaevatu tunnistas et ta järelseisvad asjad kaebaja käest saanud, ja nimelt

#


#I
I

#			
			

#			Wanna kohtu otsuse põhjusel on
			Vana kohtu+ +otsuse põhjal on

#			Kaebatul Kaebajal sulla rahha maksa 
			Kaevatul kaebajale sula+ +raha maksta			

#			
			

#			5Rb50Cp
			5rb50kop

#		
		

#II
II

#			1 nael tupakud
			1 nael tubakat

#			 " 8 "
			 " 8 "

#		
		

#III
III

#			6 naela soola
			6 naela soola

#			 " 15 "
			 " 15 "

#		
		

#IV
IV

#			üks piip
			üks piip

#			 " 30 "
			 " 30 "

#		
		

#V
V

#			1 wak odre on ta palkaks sanud
			1 vakk otra on ta palgaks saanud

#			2 " 50 "
			2 " 50 "

#		
		

#VI
VI

#			üks pors
			üks põrsas

#			2 " "
			2 " "

#		
		

#päewi ei olle Kaebatu mitte Kaebajal tema noudmise järrele wõlgu jänud.
päevi ei ole kaevatu mitte kaebajal tema nõudmise järele võlgu jäänud.

#Kui kohtu kaiadel keige wähhemad ennam ette tua ei olnud, siis ühentasid kohtu liigmed endid selle
Kui kohtu+ +käijatel kõige vähemad enam ette tuua ei olnud, siis ühendasid kohtu+ +liikmed endid selle

#Otsusele: Et Jaak Allik tõeks tunnistanud et ta Johan Türki käest sanud kelle eest tal maksa on:
Otsusele: et Jaak Allik tõeks tunnistanud et ta Johan Türki käest saanud kelle eest tal maksta on:

#


#1 nael tupakud
1 nael tubakat

#			 8 
			 8

#		
		

#6 naela soola
6 naela soola

#			15
			15

#		
		

#üks piip
üks piip

#			5
			5

#		
		

#üks wakk odre
üks vakk otra

#			210
			210

#		
		

#1 põrs
1 põrsas

#			100
			100

#		
		

#kohtu otsuse põhjusel
kohtu+ +otsuse põhjusel

#			550 Cp
			550 kp

#		
		

#1 paar wenne sapid Birki tunistuse järele makssab
1 paar vene saapaid Birki tunnistuse järgi maksab

#			225 
			225

#		
		

#


#			15
			15

#		
		

#Summa 
Summa

#			Rb 11,28 Cp
			Rb 11,28 kp

#		
		

#Nimetud suma peab Jaak Allik 8 päewa sees wälja maksma, Aga Alliku noudmised sawad keik tühjaks tehtud se pärrast et tal keige wahemad tunnistust ette tua ei olnud.
Nimetatud summa peab Jaak Allik 8 päeva sees välja maksma, aga Alliku nõudmised saavad kõik tühjaks tehtud see+ +pärrast et tal kõige vähemat tunnistust ette tuua ei olnud. 

#otsus sai sedamaid Kohtu käiadel kulutud. Joh. Türk olli rahhul, Allik ei olnud rahul.
otsus sai sedamaid kohtu+ +käijatele kuulutatud. Joh. Türk oli rahul, Allik ei olnud rahul. 

#Jaak Raegson [allkiri]
Jaak Raegson [allkiri]

#Jan Mõrd XXX
Jan Mõrd XXX

#Peter Kogger XXX
Peter Kogger XXX

