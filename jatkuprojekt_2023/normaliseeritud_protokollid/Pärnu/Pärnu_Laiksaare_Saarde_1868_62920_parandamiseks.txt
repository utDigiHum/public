#Laiksaares sel 30 Mail 1868


#Jures ollid: peakohtumees: Mats Rast, wanemk Hans Tins ja noremk Hans Kamdron


#


#Sest et nummer N 3 all kirjotud protokollis ühtigi ei olle Peter Melksoni wastamissed ülles pantud, sest ka nähja et temmale se protokoll, kui ka temma mõistus mitte kulutud ei olle, sai tänna tedda sellega tuttawaks tehtud ja wastas:


#1. temma wannaissa Jürri Melkson ei olla mitte waene olnud, waid kõige rikm tallopoeg wallas, tunnistas olla: Karl Talts, Lieso Grenstein, ja Jaan Joost.


#2. temma arwab, et kül saunas sündinud, agga temmal ommetegi wannema poea õigus, - selle pärrast tahhab omma wannaissa tallu, sest temmal ei olla mitte auksion tehtud, ja temma wend Jaan Melkson olla 8 aastad pärrast sedda sündinud ja temma piddanud seaeg tallus ilma palgata tööd teggema, - selle pärrast pallus temma omma wannaissa tallu kätte saada.


#Said tunnistussed ette kutsutud, ja neilt selle Peter Melku wannaissa rikkuse pärrast küssitud, wastasid


#ad 1. Karl Talts: Peter Melksoni wanna issa ei olla mitte waene olnud kui tallu Ado Melkson kätte annud, - kui peale surma temmale auksion on tehtud, ei mitte wõlla pärrast waid lastel jaggamisse warra pärrast.


#ad 1. Lieso Rennstein, /kaebaja täddi/ kõik see tallo ello kram jäänud talluse ja piddanud pärrast laestele ärra jäggatud saama, agga teised lapsed olla ärra surnud, selle pärrast saanud sedda wend Ado Melkson ommale, karriloomad agga üksi ärramüetud, et se warra lastele ärra jäggada, - agga issa Jürri Melkson ei olla mitte waene olnud, kes sedda räkib, se walletab. Temma wend Ado Melkson, nüidne Tauste tallu issa olla peale puellomaja kramist issa Jurri Melksoni käest saanud: 1 hobbune 1 lehm.


#Sai Ado Melksoni käest küssitud, mis temma eestnimmetud tunnistuste ülle wastab wastas: et temmal ei olla middagi wastuta, kui et se tunnistus olla walle.


#Selle peale wastas Lieso Grennstein: kõike olla Kossi kül temma wälja ütlenud ja wõida agga sedda üksi wandega tõentada, et temma issa ei olla mitte vaen olnud.


#Ka Karl Talts ütles et kül Peter Melksoni äi on ju ehk kohhus arwab, et ehk suggulase poolest tunnistab, et mitte sest olla jubba 61 aastad wanna ja tahhab ja wõib omma tunnistus wandega tunnistada.


#Selle peale antis weel tunnistusse üllesse Jacob Leinatsar.


#


#Mõistetud: et se assi jääb ni kauaks pooleli kunni J. Leinatsar saab kohtu kutsutud.


