#Tulli se Perremees Peter Koop sia kohto ette ning andis ülles, et temma Märzi kuul selsammal aastal läinud Saksnitu omma wimast heinakuhja ärra toma, ning kui jurde sanud, löidnud, et arwata 40 punda heino, mis ka kohhus ülle watanud, ärrawarrasted olnud. Temma on löidnud seal wärskid jälgi ning nende möda läinud senni kui Abia Sarja Kõrtsi kus temma selle seäl ellawe Kengisseppa Jaani jures omma heinad teiste heinde seas on ärra tunnud. Sepeäle on temma selle Kengisseppa Jaani käest küssinud, kust temma need heinad on sanud, sest et tal nisuggused heinad on ärra kaddunud? Nimmetud Kengissepp Jaan on wastusseks annud, et temma need heinad Wanna Karriste Mehhe Peter Kirricku käest ostnud.
Tuli see peremees Peter Koop siia kohtu ette ning andis üles, et tema märtsi+ +kuul selsamal aastal läinud Saksniitu oma viimast heinakuhja ära tooma, ning kui juurde saanud, leidnud, et arvata 40 punda heinu, mis ka kohus üle vaadanud, ära+varastatud olnud. Tema on leidnud seal värskeid jälgi ning nende mööda läinud seni kui Abja Sarja kõrtsi kus tema selle seal elavad Kengissepa Jaani juures oma heinad teiste heinte seas on ära tundnud. Seepeale on tema selle Kengissepa Jaani käest küsinud, kust tema need heinad on saanud, sest et tal niisugused heinad on ära kadunud? Nimetatud Kengissepp Jaan on vastuseks andnud, et tema need heinad Vana Kariste mehe Peter Kiriku käest ostnud.

#Kohhus kutsus sedda Sarja Kengisseppa Jaani ette ning küssis, kas temma Peter Kirriku käest sel talwel heino on ostnud? mispeäle temma wastas, et temma 4 kõrda Peter Kirriku käest heino sanud:
Kohus kutsus seda Sarja Kengissepepa Jaani ette ning küsis, kas tema Peter Kiriku käest sel talvel heinu on ostnud? Mispeale tema vastas, et tema 4 korda Peter Kiriku käest heinu saanud:

#1. on Peter Kirrik temmal ommuku warra 3 wammusse õmblemisse eest 8 punda
1. on Peter Kirik temal hommiku vara 3 vammuse õmblemise eest 8 punda

#2. õhto ämmarikku ajal 3 punda mis eest temma 9 Kop hõbbed maksnud,
2. õhtu hämariku ajal 3 punda mis eest tema 9 kop hõbed maksnud,

#3, jälle õhtul 5 punda mis eest 15 Kop hõbbed maksnud ning
3, jälle õhtul 5 punda mis eest 15 kop hõbedat maksnud ning

#4, ka õhtu hilja peäle arwata 7 punda mikkeeest 45 Kop hõbbed ja ⅙ Wak Kartowlid annud - tonud.
4, ka õhtu hilja peale arvata 7 punda ¤ 45 kop hõbed ja ⅙ vakk kartulid andnud – toonud.

#Nüüd sai Peter Kirrik ette nõutud ning temma käest küstud, kas temma Peter Koopa heinakuhjast on heino warrastanud ning Sarja Kengisseppa Jaanil ärra münud? wastas sesamma, et temma Kegisseppa Jaani üllesandmisse järrel 3 essimest kõrda omma heino on winud ning ärra münud, ning se 4jas ehk wiimne kõrd paljalt Peter Koopa Kuhjast heino on wõtnud ning temmal winud mis temma agga nende heinde eest on sanud, ei olle temmal ännam meles.
Nüüd sai Peter Kirik ette nõutud ning tema käest küsitud, kas tema Peter Koopa heinakuhjast on heinu varastanud ning Sarja Kengisseppa Jaanil ära müünud? vastas seesama, et tema Kengissepa Jaani ülesandmise järel 3 esimest korda oma heinu on viinud ning ära müünud, ning see 4. ehk viimne kord paljalt Peeter Koopa kuhjast heinu on võtnud ning temal viinud mis tema aga nende heinte eest on saanud, ei ole temal enam meeles.

#Kohhus kulles se Kaebus ning need tunnistussed heaste ülle ja arwas mõista ning kinnitada: et Peter Kirrik need arwata ärra kaddunud heinad 30 punda, ehk ka rahha peäle arwatud 3 R. hõbbed ühhe näddali pärrast peab Peter Koopal ärra maksma, ning saab selle wargusse eest 30 witsa löki, se heinaostja ehk Kengisseppa Jaani ülle saab selle asja pärrast se ees seisaw Protokolli Kirri Abia Koggokonna-Kohto kätte sadetud, et sesamma tedda se eest, et temma wõerawalla Mehhe käest, ja peälegi weel öösi, kus temma arwata wõis need warrastud heinad wõiwad olla. on ostnud, mis seädusse järrel keelatud on - trahwi alla panneks.
Kohus kuulas see kaebus ning need tunnistused heasti üle ja arvas mõista ning kinnitada: et Peter Kirik need arvata ära kadunud heinad 30 punda, ehk ka raha peale arvatud 3 r. hõbed ühe nädala pärast peab Peter Koopal ära maksma, ning saab selle varguse eest 30 vitsa+ +lööki, see heinaostja ehk Kengissepa Jaani üle saab selle asja pärast see ees+ +seisav protokolli+ +kiri Abja kogukonna-kohtu kätte saadetud, et seesama teda see eest, et tema võõra+valla mehe käest, ja pealegi veel öösi, kus tema arvata võis need varastatud heinad võivad olla. on ostnud, mis seaduse järel keelatud on – trahvi alla paneks.

