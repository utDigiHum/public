#Kusta Laarsoni kaebtuses (sest 16 Januarist. s.a. № 154 all) Hans Lensini wastu kondrati murdmise pärrast ollid kohtu käiad kui ka Lensini kaemehed sia ilmunud.
Kusta Laarsoni kaebuses (sest 16 jaanuarist. s.a. № 154 all) Hans Lensini vastu kontrahi murdmise pärast olid kohtu+ +käijad kui ka Lensini käemehed siia ilmunud.

#Hans Lensin andis ette, et rentnik mitte omalt poolt jareltähetud punktid ei täita ja pallub kaebatu kaebajad nii suguse kahju tasumisele tompata.
Hans Lensin andis ette, et rentnik mitte omalt poolt järeltäheldatud punktid ei täida ja palub kaevatu kaebajad nii+ +suguse kahju tasumisele tõmmata.

#1, Rentnik on 1 ½ Wakkamaad uut maad kiwidest puhastamatta jaatnud - 'a wakamaa 12 24 Rb Cp
1, rentnik on 1 ½ vakamaad uut maad kividest puhastamata jätnud - 'a vakamaa 12 24 rb kp

#2, - " - on 1 ½ wakkamaad Raentiku oma heina maaks wotnud, ja sealt pealt 2 aasta heinad teinud, mis Laarsonil mitte kontrati järrele ei ole lubatud.
2, - " - on 1 ½ vakamaad rentiku oma heina+ +maaks võtnud, ja sealt pealt 2 aasta heinad teinud, mis Laarsonil mitte kontrakti järele ei ole lubatud.

#3, Rentnik on heinamaa puhastamatta jätnud
3, rentnik on heinamaa puhastamata jätnud

#4, Leppa mõtsa maha raiunud ilma lubata
4, Lepa metsa maha raiunud ilma loata

#5, Saare kaswandik ja Öslakki raentik, sawad karja ja hobuste karjamise eest keelatud rentnikul
5, Saare kasvandik ja Öslakki rentik, saavad karja ja hobuste karjamise eest keelatud rentnikul

#6, Lina ligu mis kaewatud ja tehtud on, saab Rentnikul keelatud linnu leotamast.
6, Lina ligu mis kaevatud ja tehtud on, saab rentnikul keelatud linnu leotamast.

#7, Rentnik ei tohi tükiti tallo põldo wälja anda teiste kätte prukida,
7, Rentnik ei tohi tükiti talu põldu välja anda teiste kätte pruukida,

#8, Se heina maa lohk mis lubja asjo ligidal, karjama küllest niido alla on woetud, jääb rendile andja kätte.
8, Se heina+ +maa lohk mis lubja asju ligidal, karjamaa küljest niidu alla on võetud, jääb rendile andja kätte.

#9 Rentnik pidi kontrati järele Tallo ehituse materiali wedama, mis ta mite ei ole täitnud.
9 Rentnik pidi kontrahi järele talu ehituse materjali vedama, mis ta mitte ei ole täitnud.

#Kui kohtu käiadel keige wähemad enam ette tua ei olnud ühentasid kohtu liigmed endid sele
Kui kohtu+ +käijatel kõige vähemad enam ette tuua ei olnud ühendasid kohtu+ +liikmed endid selle

#Otsusele: Kusta Laarson peab oma kaebtuses asjas Hans Lensini wastu honede noudmise pärrast oma renti kontrati §§ 2 põhjusel rahul nomitud, sest et Laarsonil nimetud para krahwi järele mitte nimelt ülesmargitud ei olle, paljo hoonid rentnik oma kätte saab. waid et niisugune, pärast ühe lissa lehe peale maha kirjuta pidi saama, mis weel tännini tegematta on.
Otsusele: Kusta Laarson peab oma kaebuses asjas Hans Lensini vastu hoonede nõudmise pärast oma rendi+ +kontrahi §§ 2 põhjusel rahul noomitud, sest et Laarsonil nimetud para+ +grahvi järele mitte nimelt ülesmärgitud ei ole, palju hooneid rentnik oma kätte saab. vaid et niisugune, pärast ühe lisa lehe peale maha kirjutada pidi saama, mis veel tänini tegemata on.

#2 Hans Lensin peab nimelt rentnikul 70 Wakamaad põldo ühes aia maga rendi kontrati §§ 18 järele kätte prukida andma
2 Hans Lensin peab nimelt rentnikul 70 vakamaad põldu ühes aia maga rendi kontrakti §§ 18 järele kätte pruukida andma

#3, ja peab Lensin kui ka Laarson ülesantud punktid renti kontrati põhjusel täitma, ja wastu panemise põhjusel saawad naad kahju tasumisele tompatud.
3, ja peab Lensin kui ka Laarson ülesantud punktid rendi+ +kontrahi põhjusel täitma, ja vastu panemise põhjusel saavad nad kahju tasumisele tõmmatud.

#Otsus sai seda maid kohtu käiadel kulutud.
Otsus sai seda+ +maid kohtu+ +käijatel kuulutatud.

#Pea kohtumees Kusta Lepik 
Pea+ +kohtumees Kusta Lepik 

#kohtumees Andres Saarme [allkiri]
kohtumees Andres Saarme [allkiri]

# - " - Jaak Raekson [allkiri]
 - " - Jaak Raekson [allkiri]

