#Hans Musikant Saksamoisast tulli ette ja kaebas, ta olla teisel suwiste pühapaewa ohtul, sel 1 Junil s.a oma magamise tuppa magama heitnud, ja oma kue taskus 4 kümne Rb tüki paperi raha ja 1, kolme 3 Rb tük ja kaks ühe rubla tüki, arwata 2 Rb hobe raha, - suma 47 Rb wask raha on teadmatta olnud,- teisel homikul on Jaan Labbi (kelega naad ühes kordelis elanud) kodu tulnud, ja kaebajad maast üles aianud ja ütelnud et lähame hobusid koplise aiama. Kaebaja arwanud, et Jaan Labi seda raha on ära warastanud, ja on siis seda asja kunni 17 Julini s.a kinni pidanud ja siis Oiso paris härad A von Siversi palunud Jaan Läbbi läbi Otsida lasta.- Kaebajal on ühe 10 Rb tüki Numer meeles olnud mis kaebatu ene otsimist A von Siversi härale üles andnud se on olnud № 279956 ja kellel weel kurra poolt külg lõhki olnud, nisugune raha ja eesnimetud numbri all üks kümne rubla tükk kätte löitud, kus juures ka weel need 3 teist 10 Rb tükki olnud, kaebaja palluda kohud kaebatud sadusliku üle kulamise ja Otsuse alla wotta.-


#Jaan Labbi (Saksamoisast) wastas kaebsuse peale se on keik tõssi et temal eesnimetud rahad on, kapis olnud.- kaebatul on oma 106 Rb ja 18 Kopik raha kappis olnud,- aga wijes kohale selle pärast pantud, et wargad mitte keik ei woiks kätte saada, kaebatu wastas kohtu küssimise peale, oleks need rahad Otsjad nimelt kaebatu käest näha küsinud, oleks ta neile seda ilma otsimatta ette näitanud.- Otsjad on keige eesside ühe 5 Rb tüki ühe spitski toosi seest löitnud,- ühe raha kotti seest 2 Rb tüki paperiraha ja 13 kop wask raha ja weel 2 wannad raha Kopiku tükki, ühe kard karbi seest löitsid Otsjad kaebatu üles andmise järele 9 Rb 5 kop hõbe raha ja ühe õlle klaasi seest keda Otsjad Kapist wälja wotnud on kaebatu ütelnud, et selle klaasis ühe paperi tüki sees on kaebajal 4 kümne Rb tüki, ühe pudeli punnis on kaebajal ühe paperi sees kaks 25 Rb tüki olnud keda ta ise Otsijadel käte antnud - Kaebatu wastas kohtu küssimise peale et tema Kapi wõtti on ikka kappi weere peal olnud kust kaebaja iga kord oleks woinud seda wotta ja kapi juures käia ja kaebatu raha järele waadada ja ommaks tunistada ja need numbrid üles markida ja meeles pidada mis sellest selgesti näha olli, kui kaebatu kap läbi Otsidi on kaebaja nimelt sinna keskmise riijoli peale otsida käskinud kus see 4 kümne rubla tüki olnud. sest kohtu käiadel on kappi wotmad toas kapi peal hoidnud ja ei ole ühe tõise eest keige wähemad hoidnud.-


#Musikant wastas seda ta ei ole mite ütelnud et Otjad keskmise rijoli peale peawad otsima


#Seepeale sai arwatud: Et eesseiswa assi Kaela kohtu wäriline siis teda Wilandi Silla kohtu ulekulamise ja Otsusele saata.-


#arwatud. Keik kuidas sündinud kirja panda.-


#Kusta Leppik [allkiri] XXX


#Jaak Raegson [allkiri]


#Andres Saarm [allkiri]


