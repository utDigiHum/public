#Kohtu ette astus Laimjala wallast Melania Redikson ning kaebas et siitsammast wallast Fedor Tiits temale kajult wet ei anna, ja tema naene oli teda peksnud äge aisa suguse puuga, ning ta mullika olla nemad nõnda peksnud et mittu aega hawad mädanenud, ja hobused tema aeda wilja sisse aianud, ja nõnde lõuk olnud tema käes, lõukuta, ja tema olnud üks nael lõngu nõnde käest saja, selle eest pidanud ta nõnde lõugu kinni, aga nemad olla lõugu tema rihhalast ära wiinud, siis kui teda ennast kodu pole olnud.


#Se peale kutsuti ette Fedor Tiits kes selle kabtuse peale wastust andis, et nemad tema naesega nõiduse pärast olid riido läinud, et se Melania Redikson on tulnud nõnde ukse alt mulda wiima, selle pärast olla tema naene teda witsaga löönud, aga tema mullikad ei olla naad mitte peksnud, kui se nõnde aeda tulnud siis olla nemad teda wälja ajanud, ja nõnde lõuk põlla poolegid tema käes olnud se olla päris wale.


#Aga et nõnde riio asja üle mingisugust tunnistust ei ole andis Kohhus nõnde asjata tüli se kord andeks, aga kui se weel ette tulleb siis neid mõlemid kinni panna. Ja selle kaju wee pärast läks Kohus sõnna seda üle waatma, siis leiti et se kaju oli enne küla karjama sees olnud, aga mõne aasta eest oli kogukonna Kohhus selle kaju Fedor Tiits lasknud oma aeda tösta, sest et tema aed selle kajul kõige lähem olnud, sellepärast et se kaew karjamaa sees kahjulik olnud, ńüüd otsis Kohus otsust kumma wamili järel se kaew peaks olema, aga et seda ei wõinud leida, kui naad issi waidlesid teine ütles oma ja teine oma Wamili päralt. Nüüd möistis Kohus mõlemil õiguse sealt wet wiia, ja Fedor Tiits peab karjama poole aja sisse wärawa tegema kust Melania Redikson wõib wet wiia, aga Melania ei olnud seega rahhul, waid tahtis et kohhus temale Fedor Tiits piirist kaju tee pidi mõistma, aga Kohus seda ei wõinud et nõnde riid siis ilmaski ei lõppe.


#


#Kogukonna Kohtu pea Alekseij Silla &lt;allkiri&gt;


#Kingli kogukonna Kohtumees Iwan Rüütel XXX


