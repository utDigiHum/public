#Laimjala mõisas kogukonna kohtus sel 5al Septembri ku. p. aastal 1881


#Kokku olid tulnud


#1 Kogukonna kohtu pea Wassilij Tiits


#2 Kohtumees kinglist Iwan Rüütel


#3 Tallitaja Prido Mast


#


#Kui 1878al aastal Laimjala magasini ait ära rööwiti, ning 1,50 rubla rahha ära wiidi, ei olnud selle kahhe aasta sees midagid kuulata; Aga nüüd sel aastal kuulti jutto rahwa sega sündiwat, et Laimjala Wallast Wassilij Riuhk ja tema abielu naene Tadjana olla issekeskes riido läinud, ja tema naene olla teiste naeste ees rääkinud, et tema mees olla magasini aida ära kiskunud. Siis otsis kogukonna Walitsus ning kogukonna Kohhus selle juttule otsust. Siis leiti et Keiguste wallast talu tütruk Maria Luukas olla seda juttu Kahetla walla antstahku Soldati naese Anna Kaasik käest kuulnud ning oma sõprade ees raekinud.


#Nüüd sel aastal 5al Septembri kuu päewal telliti neid Laimjala kogukonna Kohtu ette. Kui Keiguste wallast Maria Luukas ette kutsuti, siis tunnistas tema et Kaehtla wallast Anna Kaasik oli tema ees rääkinud nõnda. Willem riidleb peale, ei tee linu maha mitte, ei tea mis nõu mull haitab, ma lähen räägin mõisas üles, et ta magasi aida ära kiskus. Selle järele kutsuti ette Anna Kaasik, kes tunnistas et Wassilij Riuhk naene Tiijo raekinud tema ees nõnda. Et Willem riidleb ja teotab mind teiste inimeste ees, ma tean temast ka, ta kustab mõisa küljest peale mis kätte saab, ega see magasini aida assi temast kaugel pole, ta ehhitas neid asju;


#Selle järele kutsuti ette Laimjala Wallast Mihail Ool naene Tatjana kes ka sest asjast pidi teadma, aga see tunnistas et Wassilij Riuhk naene Tadjana tema ees magasini aida wargusest mitte pole raekinud kui üksi mõisa wargusest. Tema oli kussinud miks teie nõnda riidlete, kas ta sind peksab ka, aga Tiiu oli kostnud, ei ta tohhi mind peksa ühtid, ma lähe räegi mõisas üles, kus kangid kus kõik, mis ta mõisast toonud olla.


#Nüüd kutsuti ette Wassilij Riuhk naene Tadjana, ja küssiti kas ta teab midagid sest magasini aida wargusest raekida, aga tema ütles et ta midagid sest ei tea, et tema mees ei olla tema kätte mitte nööpnõela sugust asja annud.


#Se peale kutsuti neid suud su wasta, aga see Wassilij Riuhk naene ajas need teised naesed köik waleks, et teised ta suu sisse kül ütlesid et ta on raekinud, aga siiski ta wandus waleks, et tema mitte midakid põlla räekinud.


#Siis kutsuti ette Wassilij Riuhk ja loeti kaebtus temale ette, aga tema tunnistas ennast aida wargusest nii puhta olewad kui Ingel taewas, ning kui selge päike mis kõikide peale paistab, ja käskis naesed asja tõeks teha.


#Et nüüd kogukonna kohhus selged otsust nende käest ei wõinud saada, seepärast palub et Auuline Keiserlik Sülla kohhus neid wõttaks kuulda.


