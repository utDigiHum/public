Laimjala Mõisas sel 26. Juli 1872 a.
Üllemal nimmetud päwal ollid koos:

1,
			Koggokonna Tallitaja Alexei Silla
		
2
			Koggokonna Eestseisja Iwan Pöldes
		
3
			Koggokonna Eestseisja Prido Mast
		
4
			Tallitaja kutsmisse peäle selle koggokonna seisuste wollimehhed
		
Siin sai esmalt Tallitaja polest wollimeeste koggo ette pandud et siis Koggokond tännine wägga holetumast omma pearahha tännine on maksnud, nenda et jubba mõnned Koggokonna liikmes seddäsamma mitto aastad wölgo jänud, ning sepärrast nüüd Suur Kohhus nisuggust wõlgu Soldatide läbbi ähwardab sissesundida, mis läbbi agga Koggokonnale suur kallu ka isseerranis maks peäle langeb. Ning et mitto neist wölglassist jubba Rekrudiks widud ja mitto siit hopis ärra üllema sees ennast üllespiddawad, kelle krest ometi ni pea maksu lota ei olle, ja sepärrast wollimeeste moista on, kuida sedda, nisugguste innimeste läbbi wõlgu jänud pearahha, nüüd ommeti kätte sada, et mitte Soldatid walla nuhtlusseks ei saaks seie koggokonda sama pandud?
Sedda wõlgu nende ilma jouta ning Rekrudiks widud innimeste eest olli kirjade järrel 67 Rubla 96 Kop. ja arwas Tallitaja ning wollimehhed sedda heaks et se summa keikida Koggokonna maksjatte hingede peäle saaks ühhe arrulisselt ärrajaggatud, ning kui aegamöda jälle nende käest, kes weel ellus, sedda wõlga kätte saaks, siis sedda rahha jälle koggokonna maksude sekka nenda arwada, et se nende hinge maksust wähhemaks jääb. Selle mõistmissega ollid keik rahhul ning need 67 Rbl. 96 Kop. said selle 1872 aastase I-se pole pearahha kõrwa arwatud ning 140 hinge peäle ärrajaggatud, ning Tallitaja andis köwwad käsku 1seks Augusti Ku päwaks se rahha täwelikkult temma kätte saab makstud, sest et siis sel päwal üks Silla Kohtu Herra Laimjala Mõisa tulleb, issi sedda rahha wasto wõtma, ning kui leiab maksu sisse tullemisse polest ühhegist küljest holetust ehk wastopannemist ollewad, siis ähwardab Soldatid, mis praegu umbes Kahtla waldas, otsekohhe seie Laimjala walla kallale läkkitada.
Sega olli tännane kous olleminne otsas.
