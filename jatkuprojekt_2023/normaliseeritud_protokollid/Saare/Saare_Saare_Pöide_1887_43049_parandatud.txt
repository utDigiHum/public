#Sel tännasel päwal August 19 olli Wälta küllas Winni perres II kohtumees Wladimer Koppel ja tallitaja Feodor Ansber ja kirjutaja Leonti Saar. ning sel päwal sai selle wanna peremehe Jaen Rehe. kes olli ära surnud sel 12nd August. Tema järele jänud warale wahet tehtud kahe lapse peale poeg Mihkel Rehhi ja tüddar Miina. Selle ära surnud Jani oma sõnna järele mis ta enne surma isse olli lubband sel 29 Heinakuus. Tallitaja ning kirjutaja kulder kes sel päwal olli tema iure kutsutud kui ta iubba raskest aige olli. Siis pärast surma sai tema lubbamine Protokooli üles kiriutud nenda et tema lubbas oma pärimise õiguse oma poja Mihklile ning keik see kraam mis ülle Raud warra olli jäi tema norema pojale Mihklile. ning ütles ta oma wanema poja Juhani kohta nenda et tema on minust ära läind riiuga ja oma jäo koik kätte saand. nüid ei olle temal minu kramist ennam ühtegid pärimist. nenda on siis puumaa koht ja keik se kraam tema poja Mihkli jäggu mis ülle tema tütre jäo jääb.
Sel tänasel päeval august 19 oli Välta külas Vinni peres II kohtumees Vladimer Koppel ja talitaja Feodor Ansber ja kirjutaja Leonti Saar. ning sel päeval sai selle vana peremehe Jaan Rehe. kes oli ära surnud sel 12ndal augustil. Tema järele jäänud varale vahet tehtud kahe lapse peale poeg Mihkel Rehi ja tütar Miina. Selle ära surnud Jaani oma sõna järele mis ta enne surma ise oli lubanud sel 29. heinakuus. Talitaja ning kirjutaja kulder kes sel päeval oli tema juurde kutsutud kui ta juba raskesti haige oli. Siis pärast surma sai tema lubamine protokolli üles kirjutatud nõnda et tema lubas oma pärimise+ +õiguse oma poja Mihklile ning kõik see kraam mis üle raud+ +vara oli jäi tema noorema pojale Mihklile. ning ütles ta oma vanema poja Juhani kohta nõnda et tema on minust ära läinud riiuga ja oma jao kõik kätte saanud. nüüd ei ole temal minu kraamist enam ühtegi pärimist. nõnda on siis puumaa koht ja kõik see kraam tema poja Mihkli jagu mis üle tema tütre jagu jääb.

#Ja weel ta ütles sel päwal enne surma et kaks uut maja ma ollin teind. Teine on wabbat platsi peal ja teine puuma õues aidamaja. Se wabbat maja jäeb tütre jäoks ja se aida maja on poja Mihkli. 
Ja veel ta ütles sel päeval enne surma et kaks uut maja ma olin teinud. Teine on vaba platsi peal ja teine puumaa õues aidamaja. See vaba maja jääb tütre jaoks ja see aida+ +maja on poja Mihkli. 

#Siin on need asjad ülles kiriutud mis tema tütre Mina jäggu on.
Siin on need asjad üles kirjutatud mis tema tütre Miina jagu on.

#1. Wabbat maja ja se plats kus peal maja on.
1. vaba maja ja see plats kus peal maja on.

#2. Kaks weist 1 lehm 4 aastad wanna
2. Kaks veist 1 lehm 4 aastat vana

#3. 1 aastane mulikas 
3. 1 aastane mullikas 

#4. 4 lammast
4. 4 lammast

#5. 4 riiete kerstu
5. 4 riiete+ +kirstu

#6. 2 kalla nõu
6. 2 kala+ +nõu

#7. 2 ölle nou.
7. 2 õlle+ +nõu.

#8. 4 räim wõrgu.
8. 4 räime+ +võrku.

#9. 9 paari XXX mõrdu.
9. 9 paari XXX mõrdu.

#10. 4 wikkatid.
10. 4 vikatit.

#11. 1 kaddel.
11. 1 katel.

#12. 1 Raud kang.
12. 1 Raud+ +kang.

#13. 1 Raud labbidas.
13. 1 Raud+ +labidas.

#14. 1 kerwes 1 weetober 1 wann.
14. 1 kirves 1 veetoober 1 vann.

#15. 1 sõnniku ang. 1 heina ang.
15. 1 sõnniku+ +hang. 1 heina+ +hang.

#16. 1 kääi
16. 1 käi

#17. 1 aastane täkut sälg
17. 1 aastane täku+ +sälg

#18. 1 aastane sigga
18. 1 aastane siga

#ja pöitki metsa hoid ja wärawa tagga 20 palgi on ka Mina jäggu.
ja ¤ metsa hoid ja värava taga 20 palki on ka Miina jagu.

#Sedda tunnistawad tõeks
Seda tunnistavad tõeks

#Tallitaja Feodor Ansber XXX
Talitaja Feodor Ansber XXX

#II kohtu mees Wladimer Koppel XXX
II kohtu+ +mees Vladimer Koppel XXX

#Kiriutaja Leonti Saar
Kirjutaja Leonti Saar

