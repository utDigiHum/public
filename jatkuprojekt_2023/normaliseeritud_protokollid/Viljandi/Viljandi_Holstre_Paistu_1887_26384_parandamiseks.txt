#Johann Wiidika kaebduses Janus Rekkandi wastu linamaa nõudmise pärast, /: protokollid 28 Maist, 5 Juunist ja 3 Juulist s.a. No 299, 308 ja 358/ olid mõlemad kohtukäiad tänasel päewal ette tulnud ja tunnistaja Peter Sumbak.


#Kaebataw Janus Rekkand lisas endistele üttelustele weel juure, et ta poisile Johann Wiidikale nüüd koguniste mitte lina anda ei wõiwad, sest et see kewadel lina külwamata jätnud on ja sellepärast ka nüüd lina ei wõiwat nõuda.


#Johann Wiidik jääb aga oma endise nõudmise jure et ta ristikheinamaa odra kõrre lina saaks ja ütleb et neil Rekkandiga 1887 aasta kewadel uut teenistuse kaupa ei olnud.


#Peter Sumbak ette Kutsutud, tunnistab et tema seda ei teadwat missuguste tingimustega Jaanus Rekkand Johann Wiidika endale poisiks tellinud on, aga seda olewat ta kuulnud kui Rekkand Wiidikale kewadel 1887 Rasilla kõrtsis üttelnud: wana pois see on Johann Widik, tahab kõrget palka keda ma anda ei wõi, ja peab ta ennast mõnes asjas parandama. Pärast on ta ka näinud et need ühe teisega kaebdand aga mis tingimustega Johann Wiidik tellitud saanud seda ei ole ta kuulnud.


#Kui nüüd tunnistaja Peter Sumbaku tunnistusest näha et kewadel 1887 aastal Johann Wiidika ja Janus Rekkandi wahel uus teenistuse Kontraht maha tehtud saanud ja nende wiisi see kewadel 1886 aastal Hans Andressoni ja Peter Kikka kuultes maha tehtud kaup enam ei maksa, ning et Johann Wiidik tõeks teha ei jõua et temale mööda läinud kewadel ristikheinamaa odra kõrt ehk ka kahelt poolt nurme otsast odra kõrt, heinamaaks lubatud oleks saanud sellepärast ühendasid koos olewad kohtumehed järgmisele


#Mõistusele: Et pois Johann Wiidik mitte ilma palga lisata jääda ei wõi, ommetige ei saa tema mitte reistikheinamaa odrakõrre peale külwatud lina ega ei ole temal ka õigust kahelt poolt nurme otsast saada, waid peab Janus Rekkand Johann Wiidikale selle lina kätte andma mis ta senna põllu peale külwanud on kelle ta kewadel Wiidika lina maaks määranud ja sellele näitanud on. Ommetige peab Johann Wiidik peremehele Janus Rekkandile külwatud linaseemne tagasi andma.


#Eesseiswa mõistas sai kohtukäiatele kuulutatud ja need Appellatsioni formaliadega tutwustatud.


#Kohtumehed MHansen


# JAndresson


# JTill


#Kirjut Raudsepp


