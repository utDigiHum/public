Ette tuli Holstre Mäeotsa talu peremees Käärik Käriks ja kaebab et tema pois Jaan Maiste tema teenistusest juba 7" Juulil 1887 ära läinud on ehk kül teenistuse aasta alles 22 Märtsil 1888 täis oleks saanud. Kaebaja nõuab nüüd Jaan Maiste käest aasta palka mis järgmiselt tellitud olnud ja kellest ta juba 7 Rubla wälja maksnud olla: Sula raha 25 rubla, üks wakk maa lina, kasuk, 3 hammed (särki) ja 3 paari püksid, linase lõimega, kuub ja püksid.
Jaan Maiste wastab sellesinatse eesseiswa kaebduse peale, et palk kül nenda suur tellitud olnud ja on ta sellest ka 7 rubla kätte saanud aga tema ei ole mitte wallatuse pärast teenistusest ära läinud, waid sellepärast et ta põdema jäänud ja enam teenida ei ole jõudnud. Tema nõuab nüüd Käärik Käärikult selle teenitud aja palka kätte ja peale selle oma käest külwetud 1 1/4 wakka lina seemned.
Käärik Kääriks wastab et Jaan Maiste kül lõkkinud aga mitte haige ei ole olnud sest et taa kaunis isuliselt söönud on, nagu Johann Jaama ja Andres Sonn seda tunnistada wõiwat.
Johann Jaama tunnistab, et ta sääl talus sulane olewat aga ometige mitte ei ole ta aru saanud kas Jaan Maiste siis kui ta ära läinud, haige wõi terwe olnud.
Andres Sonn tunnistab, et Jaan Maiste on haige olnud ja sellepärast ära läinud.
Kui nüüd Jaan Maiste ka haige oleks olnud siis ei tohtinud tema ommetige oma woliga säält talust ära minna, ja sai sellepärast
Mõistetud: Et Jaan Maiste oma sees olewa palga, mis ühte kokku aasta kohta 67 R. 60 kop. suuruseks takseeritud, peremehe Käärik Käärikse heaks kaotab.
Eesseiswa mõistus sai kohtu käiatele kuulutatud ja need Appellatsioni Formaliadega tutwustatud.

Peakohtumees JLuik [allkiri]
Kohtumees JJõgewest [allkiri]
 JJaama [allkiri]
Kirjut Raudsepp [allkiri]
