#Märt Tautsi konkursi raha ära jagamise pärast ollid täna kohtu ette tellimise peale tulnod: Märt Tauts, Märts Kõks, Jaan Ots, Peeter Gussarov, Hans Ronimus, Jaak Anton, Hans Jurjens, Jaan Tamm, Märt Pihlap Johan Maritz ja Mari Tauts, aga Tonis Osi oli puudus.


#Tarwastu kiriku mõisavalitsuse õpetaja härra M. Jürmanni kiri 22 Junist 1890 N 217 sai ette loetud mille järele Märt Tautsil Kiriku valla Laose talus olevade hoonete, majade poolest enam mingit osa ega õigust ei olle. -


#Selle peale ei ollnud kellegil asjalikku üttelda.


#Kohus katsus veel Tal. S. § 900 põhjusel lepitada aga nemad ei saanud mitte leppi.


#Tarwastu kirikuvallavanem Jaan Ots palus et kohus tähele paneks et valla magatsi võlg tõistest võlgadest ees käib ja tegi raamatide järele tõeks, et Märt Tauts on Kiriku valla magatsi aita enditest oostudest wõlgu jaand 9 tsetvrt 3 karnit rukki ja 7 tsetvt 16 kar. keswi nig nõudis setweti pealt 6 Rubl. see teeb ühte kokku hinaks peale 98 Rubl


#Märt Kõks Tarvastu wallast Wiratsi talu peremees palus ka, et kohus tähele paneks, et tema oma raha 125 Rubl Märt Tautsule tema varanduse peale on laenud ja seda võlga koroboratsioni raamatuse kirjutada on lasknd 8. Juulil 1888 N 1 all ja olla seda raha nimelt tema walla wõla maksmiseks annud. Kuresaare kohtu koroboratsioni raamat töentab ka seda üttelust.


#Mari Tauts ütles, et tema võla nõudmise 31 Rubla hulgas olla ka 25 Rubla wiimse a palk raha mis kõigist tõistets wõlgadest tasumise juures ees pidada käima, aga tema ei saanud oma nõudmist mitte selges teha ja selle pärast mõisteti tema nõudmine kogoni tühjaks.


#Konkursi kurator Märt Kõks palus ka oma palka mis saduse jarele 6 R 68 kp wälja teeb nig ka kohtu kulude tasumist oksjoni pidamise juurs 6 R 68 kop.


#Tõised võlanõudjad kordasid ka oma wõla nõudmisi nõnda kui protokollist N 56, 6 aastst 1889.


#Kohus vaatas nüüd läbi kui palju Märt Tautsi kokorsi tompu raha sisse tullnd on ja leidsid:


#


#1,


#			et Oksjoni raha oli


#			167 Ru.68 kop


#		


#2,


#			enne Kuresaare kohtu kätte jäänd raha Märt Tautsi heaks


#			 12 - 8 -


#		


#


#			 Ühtekokku


#			179 R. 76 kp


#		


#Kui nüüd kellegil enam midage asjaliku ette anda ei ole ja kohus neid ka ära lepitada es saa siis asteti ära ja kohtu liikmed ühetasid endid jargmisse


#otsusele:


#Talurahwa sääduse § 904 aastast 1860 põhjusel peavad Märt Tautsi konkursi tompust mis 179 R. 76 kp suur on, saama:


#


#


#			I klassis punkt 2 järele:


#			


#		


#Konkursi kurator Märt Kõks oma palk


#			 6R68kp


#		


#kohtu kulud oksjoni pidamise pärest


#			 6 - 68 -


#		


#


#			IV klassis punt 3 järele:


#			


#		


#


#Tarwastu kirikuvalla wanema Jaan Ots magatsi võla 9 tesert 3 kar. rükki ja 7 tsetwt 16 kar. keswi tasumiseks


#			98 - " -


#		


#IV klassis punkt 4 järele:


#		


#


#Märt Kõks Tarwastu wallast oma sel 8. Juulil 1888, see on ene konkursi anetust korroboreritud wõla vähentuseks kõik Märt Tautsi konkursi tompu üle jäänd raha


#			68-40kp


#		


# Kokku:


#			179R76kp


#		


#Köik tõised wõlad jäävad selle pärast, et konkursi tomp ei hulata - tasumata ja nimelt:


#


#1, 


#			


#Märt Kõksi IV klassi pt 4 wõlg


#			56 R 60 kop


#		


#2,


#			


#Peeter Gusarov IV klassi punkt 5 wõlg


#			13 - " -


#		


#3,


#			


#Hans Ronimus IV klassi pt 5 wõlg


#			15 Ru 


#		


#4,


#			


#Jaak Anton d


#			42 -


#		


#5,


#			


#Hans Jurgens d


#			 5 -


#		


#6,


#			


#Tonis Ots d


#			15 -


#		


#7,


#			


#Jaan Tamm d


#			 7 -


#		


#8,


#			


#Märt Pihlap d


#			 1 - 50


#		


#9,


#			


#Andres Tuuksam d


#			13 - 25


#		


#


#			 Kokku


#			168R35kp


#		


#Nendel kelle wõla tasumata jääwad jääb õigus Märt Tautsi käest oma wõlga tema eestulewast varandusest nõuda.


#Märt Tauts saab trahvist lahti mõistetud, et kohus tunneb ja arvab, et tema oma rumaluse läbi pankrotti (konkursi sisse) on kukund. -


#See otsus sai täna 4. Juulil 1890 kohe ühes edasi kaebtuse wormide tutwustamisega ära kuulutud ning ka teada antud, et asi Riia Ringkonna kohtu kätte kinnituseks saab saadetud. -


#Seda otsust tunistawad kohtu käijad ühe ise eralise kwittugi järele kuulnd ollud.


#Peakohtomees J Loos [allkiri]


#kohtu J. Roosiorg. [allkiri]


#d H Wiira [allkiri]


#Krj. J. Märitz [allkiri]


