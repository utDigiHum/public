#Ette tuli Holstre Härjakoorma talu peremees Jaan Paulson ja kaebas et tema heina küün mis 100 rubla wäärt ning heina ka 100 Rubla eest sääl sees olnud öösel 4/5 märtsil ühest kurjast inimesesest põlema pistetud on saanud.- Et nüüd ühe inimese jäljed kellel sukkad jalas olnud Peedi talu poolt küüni juure tulnud ja ka senna poole tagasi läinud ning et tema kaebaja endine pois Johann Torm sääl Peedi talus teenistuses olewad, kes teda mitmel korral ähwardanud ja laimanud, seepäraast on ta warsi Johann Tormi peale seda kurja tegemist arwanud ja on ka kohtumehed Abram Hansson ja Hans Laane ning wallaeesseisja Jaan Kibe Johann Tormi jalad nende öösiste jälgede sisse passikad.- Et nüüd Peedi talu rahwa juttu järele Johann Torm selle õhtu hilja tarest wäljas käinud, seepärast arwab kaebaja et mitte keegi muu kui Johann Torm selle küüni põlema pistnud on, ja palub et wiimaks nimetatud wangi pantud ja Keiserliku Willandi Sillakohtu kätte saadetud saaks.- See heina küün olnud puust tehtud öle kattuksega ja on seesama põhjani maha põlenud.


#Johann Torm ette kutsutud wastab et tema seda küüni põlema pistnud ei ole ega ka Jaan Paulsoni ilmaski ähwardanud ja laimanud ei olewat. Tema on kõige selle õhtu kodus olnud ja üksinda aida juures pool tundi käinud, nagu seda Jaan Kibe, Mari Purro Epp Ruid, Mall Purro ja Kadri Peet tunnistada . Ka ei olewat tema jalad nende jälgede sisse paslikud.


#Kohtumees Abram Hansson tunnistab, et Johann Tormi jalg kõweram näitand kui need jäljed mis küüni juure wiinud.- Ometigi on Tormi jalg ühe jälje sisse passinud ja üks jälg mis ta astunud on ka öösese jälje moodu olnud. Need öösesed jäljed olnud ühest inimesest astetud kellel ka paljas sukkad ehk pastlad jalgas olnud.


#Et kohtumehel Hans Laanel wõimalik ei olnud täna siia kohtu etter tulla, sai ettekutsutud:


#Wallaeestseisja Jaan Kibe ja tunnistab seesama et Johann Tormi jalad esmalt kül kurjategija jälgedega ühte moodi olnud aga pärast seda on tema arwates Johann Torm omad jalad meelega kõweraks hoidnud nenda et naad öösese jälgede sisse ei ole passinud. Tema arwates olla need sukkade jäljed olnud. Ka ütleb Jaan Kibe ennast kuulnud olema kui Johann Torm Jaan Paulsoni laimanud et wiimne linna litside juures käinud ja prantsuse haiguses olla, ning üttelnud: "Kui ta minu 6 Rubla sööb, siis saab näha mispärast saab"


#Jaan Kibe tunnistab et Johann Torm tõisepaew sel 4 Martsil kuni õhtu pimeduseni kodus olnud, õhtu kella 7 ajal on ta seda aida juure käskinud minna uue poisi kraamile ruumi tegema.- Johann Torm sääl aida juurers käijes nappelt pool tundi aega ärawiitnud ja pärast kodus olnud.- Tema on ise näinud kui Johan Torm aida poole läinud, ning wanad saapad sellel jalas olnud.- Küüni põletamine olnud kella 10 ajal.


#Tunnistajad Mari Purro, Epp Roid ja Mall Purro ning Kadri Peet ütlewad kõik ühtewiisi et Johann Torm sellel õhtul kodus olnud, muud kui arwata poole tundi on see aida juures käinud.- Nemad ei ole seda kül mitte näinud kas aida juures wõi mujal aga aida wõttid on see kül otsinud, see olnud õhtu kella 7 aeal. Ähwardamist ega laimamist ei olewat nad kunagi Johann Tormist kuulnud.-


#Jaan Kibe tunnistab weel et ta kuulnud kui Johann Torm üttelnud, et Paulson tema palgast 6 rubla ilma põhjata kinni pidawad ja et Paulsoni noorikult lialt töösse sunnitawat muud ähwardamist ega laimamist ei ole ta kuulnud.


#Et nüüd Jaan Kibe tunnistuse järele Johann Torm, Jaan Paulsoni laimanud ja ähwardanud on ja et esimene sellel õhtul tarest wäljas käinud ja ka tema jälg mõne öösese jäljega kokkupaslikud sellepärast


#Mõisteti: Johann Tormi wangiwiisil ühes selle protokolli ärakirjaga Keiserliku Willandi Sillakohtu kätte saata.


