Kobbina Jakob Lass kaebab, et temma kotto 108 rubla rahha jure wõtnud ja läinud ommale ühte rendi kohta Kõo kõrsimehhe Hans Rumbiga otsima.
Ösi olnud Parrika kõrsis kust rahhast ühhe kolme rublase senna annud.
Hommiko peale selle läinud omma suggulase Ankama Mihkel Oll jure, wõtnud seal omma westi, kus taskus rahha olnud, seljast ärra, annud Mihkel Oll kätte ja pannud Mihkel Oll se temma west nöri peale ütteldes, siin olgo sul ni paljo rahha sees kui tahhes, siin seisab ta ikka paigal.
Temma Jakob Lass magganud tagga kambris omma westi jures ja kõrsimees Hans Rump ees kambris. Pärrast sedda kui sealt koddo tulnud, wõtnud temma naene temma rahha kotti wadanud rahha järrele ja küssinud, kui paljo temmal rahha jure wõtta olnud, mis peale temma üttelnud, et 108 rubla olnud kust agga 3 rubla ärra annud. Selle watamisse jures leidnud temma, et 20 rubla rahha wähhem on /: 4 wie rubla tükki:/ kui piddanud ollema, ning et temma muial ei olle teadnud rahha näitanud egga wälja annud ollema, läinud Aukamalle ja ussutanud tüdrukud Kai Pais, et ehk temma on wõtnud ja taggasi annab. Tüdruk Kai Pais aianud wasto, et temma rahhast keddagid ei tea ja lubbanud Parrika kõrtsi kulama minna, et ehk seal on purjotanud ja rahha warrastada laskud. Jakob Lass aiamisse peale käinud siis Kai Pais Parrika kõrtsis, ning ei olle seal rahhast middagid kuulnud.
Aukama Mihkel Oll ütles, et Jakob kül omma westi seljast ärra wõtnud ja temma kätte annud, agga mitte põlle üttelnud egga näitanud, kas ja kui paljo temmal rahha westi taskus olnud.
Tüdruk Kai Pais räkis kohto ees, et temma sest rahhast keddagid ei tea, selle aia sees, kui Jakob seal olnud on temma perremehhe poiaga Mart Oll sawwitamisse tö jures olnud, ning ei olle suggugi tahha kambre sanud.
Mõistetud: Jakob Lass kaebdus tühjaks, et tunnistust ei olle, ning et ka omma jomisse läbbi rahha wõib olla kaotanud ehk unrehti luggenud.
Peakohtomees: Jaak Kukmann
Kohtomees: Andrej Krasowskj
dito Jürri Kukk. XXX
dito Tawit Markus XXX
