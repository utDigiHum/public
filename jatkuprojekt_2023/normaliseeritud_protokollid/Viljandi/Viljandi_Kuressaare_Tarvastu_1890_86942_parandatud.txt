#Tulnod ollid Tarwastu kiriku walla praegune vallawanem Jaan Ots ja endine vallavanem Märt Tauts endise wallavöörmündri Andres Tuuksamiga
Tulnud olid Tarvastu kiriku+ +valla praegune vallavanem Jaan Ots ja endine vallavanem Märt Tauts endise vallavöörmündri Andres Tuuksamiga

#Protokoll 30 Januarist 1890 № 6 sai ette loetud
Protokoll 30 jaanuarist 1890 № 6 sai ette loetud

#Kiriku walla magatsi raamat sai läbivaatatud kust näha oli, et see Märt Ronimuse vilja võlg, mis temal kirikuvalla magatsi aita võlgu jäänud, on endisewallawanema Märt Tautsi ja valla wöörmündri Andres Tuuksami ajast.
Kiriku+ +valla magasi+ +raamat sai läbivaadatud kust näha oli, et see Märt Ronimuse vilja+ +võlg, mis temal kirikuvalla magasi+ +aita võlgu jäänud, on endise+vallavanema Märt Tautsi ja valla+ +vöörmündri Andres Tuuksami ajast.

#Kohus soovis kohtu käiadele lepitust, aga sellest ei saanud kedage
Kohus soovis kohtu+ +käijatele lepitust, aga sellest ei saanud kedagi

#Möistus:
Mõistus:

#Et Märt Ronimus mitte Tarvastu kiriku walla liige ei ole, ei ka mitte kunage Tarvastu kiriku wallas ei ole elanud, vaid Tarwastu vallas elab ja ka Tarvastu valla liige on, siis pidid selle aegsed Tar. Kiriku valla walitsuse ametnikud, kes temale Tarwastu kiriku valla magatsi aidast wilja wälja laenasid ka õigel ajal tema käest laena vilja tagasi nõudma ja peawad seesugutse wilja wõlgu andmise eest, mis nemad oma loaga wõerase valda andnud - vastutetama.
Et Märt Ronimus mitte Tarvastu kiriku+ +valla liige ei ole, ei ka mitte kunagi Tarvastu kiriku+ +vallas ei ole elanud, vaid Tarvastu vallas elab ja ka Tarvastu valla liige on, siis pidid selle+ +aegsed Tar. kiriku+ +valla valitsuse ametnikud, kes temale Tarvastu kiriku+ +valla magasi+ +aidast vilja välja laenasid ka õigel ajal tema käest laenu+ +vilja tagasi nõudma ja peavad seesuguse vilja+ +võla andmise eest, mis nemad oma loaga võõrasse valda andnud - vastutama.

#Selle järele mõistab kohus et Märt Tauts ja Andres Tuuksam peavad seda magatsi vilja võlga mis Märt Ronimus wõlga jäänd, Tarwastu kiriku valla magatside 14 p. ses ära maksma ja nimelt
Selle järele mõistab kohus et Märt Tauts ja Andres Tuuksam peavad seda magasi+ +vilja võlga mis Märt Ronimus võlgu jäänud, Tarvastu kiriku+ +valla magasile 14 p. sees ära maksma ja nimelt

#Märt Tauts peab maksma 2 tset 3½ k rükki ja 1 tset kesvi ning
Märt Tauts peab maksma 2 ¤ 3½ k rukist ja 1 ¤ kesvi ning

#Andres Tuuksam peab maksm 2 tset 3½ k. ruki ja 1 tset kesvi.
Andres Tuuksam peab maksma 2 ¤ 3½ k. rukist ja 1 ¤ kesvi.

#Kohus jettab aga veel Märt Tautsile ja Andr Tuuksamile õiguse Märt Ronimuse käst seda vilja, kui selle käest võimalik saada on tagasi nõuda
Kohus jätab aga veel Märt Tautsile ja Andres Tuuksamile õiguse Märt Ronimuse käest seda vilja, kui selle käest võimalik saada on tagasi nõuda

#See mõistus sai täna 27 Martsil 1890 kohe kohtu kaiadele ära kuulutud ühes aplation §§ ﹿ midas nood oma allkirjaga tõentavd:
See mõistus sai täna 27 märtsil 1890 kohe kohtu+ +käijatele ära kuulutatud ühes appellatsioon §§ ﹿ mida nood oma allkirjaga tõendavad:

#Jaan Otsa eest Jaak Tauts. [allkiri]
Jaan Otsa eest Jaak Tauts. [allkiri]

#M Tauts [allkiri]
M Tauts [allkiri]

#Antres Tuksam [allkiri]
Andres Tuuksam [allkiri]

#Kogukonna kohtu nimel: J Loos [allkiri]
Kogukonna+ +kohtu nimel: J Loos [allkiri]

#Peakoht J. Roosiorg [allkiri]
Peakoht J. Roosiorg [allkiri]

#Koh
Koh

# d H Wiira [allkiri]
 d H Wiira [allkiri]

#kr. J. Märitz [allkiri]
kr. J. Märitz [allkiri]

