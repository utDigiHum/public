#Ep Tamm ühes käemehe Juhan Rennitaga tuli ette ja kaebas Kuresaare Kuude talu peremehe Jaan Tamme peale et J Tamm tema ema (kes ka Jaan Tamme ema olnud praegu aga surnud) Kartuhwlid kokku Kümme (10) wakka koopast ära olla warastanud, ja nõuab nüid Jaan Tamme (oma lihase wenna) Käest nende kartohwlide eest 30 Rubla h.
Epp Tamm ühes käemehe Juhan Rennitaga tuli ette ja kaebas Kuresaare Kuude talu peremehe Jaan Tamme peale et J Tamm tema ema (kes ka Jaan Tamme ema olnud praegu aga surnud) kartulid kokku kümme (10) vakka koopast ära olevat varastanud, ja nõuab nüüd Jaan Tamme (oma lihase venna) käest nende kartulite eest 30 rubla h.

#Epp Tamm.[allkiri]
Epp Tamm.[allkiri]

#Jaan Tamm ütles, et tema läinud ükskord seda oma surnud ema kartohwli koopa waatama kus pidanud 16 wakka kartohwlid sees olema, aga ei ole enam kuigi palju näitanud olewad waid weidikene põhja peal ja toonud need kartohwlid ühel päewal sealt ära oma kodu, et nenda kui tema teadnud on tema õde Epp Tamm need kartohwlid sealt ära külasse wõerastele kandnud ja seal (Kitsi Jaagu juures) nende kartohwlidega siga nuumata lasknud
Jaan Tamm ütles, et tema läinud ükskord seda oma surnud ema kartuli+ +koopa vaatama kus pidanud 16 vakka kartulit sees olema, aga ei ole enam kuigi palju näidanud olevat vaid veidikene põhja peal ja toonud need kartulid ühel päeval sealt ära oma kodu, et nõnda kui tema teadnud on tema õde Epp Tamm need kartulid sealt ära külasse võõrastele kandnud ja seal (Kitsi Jaagu juures) nende kartulitega siga nuumata lasknud

#Tema Jaan Tamm arwanud oma surnud ema kartohwlide üle enesel selle pärast rohkem õigus neid pärandada olewad kui õel Epp Tammel, et need kartohwlid tema maa peal kasunud ja tema ise neid oma jõuuga maha teinud olla, ja ei arwa enesel sellepärast süidi et tema neist wähem kui wiies jagu enesele saanud on
Tema Jaan Tamm arvanud oma surnud ema kartulite üle enesel selle+ +pärast rohkem õigus neid pärandada olevat kui õel Epp Tammel, et need kartulid tema maa peal kasvanud ja tema ise neid oma jõuga maha teinud olevat, ja ei arva enesel sellepärast süüd et tema neist vähem kui viies jagu enesele saanud on

#kuna õde Epp Tamm 4/5 osa juba enese heaks ära tarvitanud olevat ilma sellest peremehele Jaan Tammele sõna lausumata.
kuna õde Epp Tamm 4/5 osa juba enese heaks ära tarwitanud olla ilma sellest peremehele Jaan Tammele sõna lausumata.

#Jaan Tamm [allkiri]
Jaan Tamm [allkiri]

#Liis Tammes 13 aastane ühes käemehe Jüri Jõhwikaga ees, ei tea nende kartohwli keetmisest midagi sellest mis Ep Tamm ütleb Jaan Tamme tema Kartohwlidest kaks katla täit ära keetnud olewad.
Liis Tammes 13 aastane ühes käemehe Jüri Jõhvikaga ees, ei tea nende kartuli keetmisest midagi sellest mis Epp Tamm ütleb Jaan Tamme tema kartulitest kaks katla+ +täit ära keetnud olevat.

#Liis Tammes [allkiri]
Liis Tammes [allkiri]

#Et Epp Tamm peale oma ema kartohwlide weel teiste käest nagu Jüri Müüri käest 3 wakka, Ep Kleini käest 3 wakka ja Hans Põder'i käest 1 wakk kartohwlist sinna Kõnesseiswase koopa oma ema kartohwlid sekka pannod olewad ja sellega Jaan Tamme ütelust muuta tahab, palus Epp Tamm tunnistusi juure anda ja tehti
Et Epp Tamm peale oma ema kartulite veel teiste käest nagu Jüri Müüri käest 3 vakka, Epp Kleini käest 3 vakka ja Hans Põder'i käest 1 vakk kartulit sinna kõnesseisvasse koopa oma ema kartulid sekka pannud olevat ja sellega Jaan Tamme ütlust muuta tahab, palus Epp Tamm tunnistusi juurde anda ja tehti

#otsus kuni teise kohtu päewani pooleli jätta ja se üles antud tunnistus Juraku Jaan Ronimois ette tellida -
otsus kuni teise kohtu+ +päevani pooleli jätta ja see üles antud tunnistus Juraku Jaan Ronimois ette tellida -

#Kuulutud
Kuulutatud

#Järg № 26 
Järg № 26 

#Peakohtumees J Loos [allkiri]
Peakohtumees J Loos [allkiri]

#kohtumees Jaak Roosiorg [allkiri]
kohtumees Jaak Roosiorg [allkiri]

# d° H Põder [allkiri]
 d° H Põder [allkiri]

#l. kogkirj. J Ronimois [allkiri]
l. kogukonnakirj. J Ronimois [allkiri]

