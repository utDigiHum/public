Jaan Lillimägi kaebas Sääsla talu perenaese Ann Liht'i peale, et Ann Liht ei maksda temale tema ära teenitud palka mitte kätte. Temal olla saada:

wiimse teenistuse aasta eest
			35 Rubla
		
ja enemine teenitud aasta palgast kaks amet skelle eest ta nüüd nõuda
			2 -
		
 Ühte kokku
			37 Rubl
		
Kaebaja palus seda raha omale An Lihti käest kätte mõista.
Ann Liht käemehe Jüri Jõhvikuga astus ette ja kostis endise aasta palgas ei ole Lillimägile suguge muid tellitud ollnud ja viimsel aastal ollnud Lillimägil küll 35 Rubla palka tellitud ollnd sellest olla aga ära tasutud saanud

Kaerahas antud
			1 Rubla
		
sula raha antud
			3 -
		
1 vakk rukkid
			2 - 50 kop
		
⅓ vakkamaad linamaad
			5 - " -
		
ära viitnud teenistuse ajast 7 päeva 'a 1 R
			7 - " -
		
 Kokku
			
			18 Rb 50 kop
			
		
selle järele olla Lillimael kõiges weel 16 R 50 kp tema käest saada. - 
Jaan Lillimägi kostis selle peale et käe raha olla ta 1 Rubl küll saand, palgas sula raha olla ta umbes küll 3 Rubl saand, pärast ütles käerahaga kokku 3 Rbla kätte saawad mida An Liht pärast ka õiges tunnistas. Jaan Lillimägi ütles esiti, et tema olla 2 wakka rükkid saand, üks wakk jäänd vana aasta palgas ja tõine vakk rukkid jäänud viise aasta palgas, mis peale raha palga tellitud ollnd. Linamaa ütles ta omale kinnitud olewad ja paevi ei olla ta enam ära viitud teenistuse ajast kui 2 päeva.
Jaan Lillimägi ütles, et tema ei ole sellest kingitud linamaast kedage saanud, vaid jäänud veel seeme kaotside. See maa olnud üks veere maa tükk ja selle pärast ei ole kedage peal kasunud - tunnistajas andis ta Jaan Torn
Jaan Torn sai kuulatud, tunnistas et Jaan Lillimägi kedage selle maa pealt ei ole saand.-
Muud tunnistust ess ole kellegi asja juurde kumagil anda.
/astsid ära:/
Moistus:
Kohus mõistab Jaan Lillimäe palgast 35 Rublast, mis selge on, maha 

1) kätte makstud sularaha
			3 R.
		
2, 1 vakk rukkide eest
			2 R
		
3, ära viidetud 2 päewa eest 
			- " 50 kp
		
 Kokku
			5 R 50 kp
		
Sehe järele jääb Ann Lihil veel 29 Rub 59 kp Jaan Lilimäele 8 p. sees ära tasuda, kelega sai kõik nõudmisit ja kaebtused tõine tõise vastu tasa ja rahule peavad jääma.-
See mõistus sai kohe ühes appel. §.§. kohtu käiadele ära kuulutud.
Peakohtumees J Loos [allkiri]
Koht J Roosiorg [allkiri]
 -"- H Wiira [allkiri]
Kr J. Märitz [allkiri]
