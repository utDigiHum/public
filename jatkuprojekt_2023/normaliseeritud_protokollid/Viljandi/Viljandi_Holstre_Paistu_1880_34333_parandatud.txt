#Ette tuli Holstre wallawanem Ado Ritson ja kaebab et Kõo walla mees Josep Luik Holstre wangi tornis olewate wangidega akna al esmalt kõnelenud ja pärast wangikambri luhwt akna katki löönud on.- Kaebaja palub et see nimetatud mees niisuguse süü pärast Keiserliku Willandi Sillakohtu kätte trahwimiseks antud saaks.
Ette tuli Holstre vallavanem Ado Ritson ja kaebab et Kõo valla mees Joosep Luik Holstre vangi+ +tornis olevate vangidega akna all esmalt kõnelenud ja pärast vangikambri luhvt+ +akna katki löönud on.– Kaebaja palub et see nimetatud mees niisuguse süü pärast keiserliku Viljandi sillakohtu kätte trahvimiseks antud saaks.

#Josep Luik ettekutsutud wastab küsimise peale, et tema mitte wangidega jutte ajanud weel wähem wangi torni akna ruutu katki löönud oleks, tema ei täädwat kõigest sellest asjast ühtegi.
Joosep Luik ette+kutsutud vastab küsimise peale, et tema mitte vangidega jutte ajanud veel vähem vangi+ +torni akna+ +ruutu katki löönud oleks, tema ei teadvat kõigest sellest asjast ühtegi.

# Tunnistajas Hans Peterson ütleb ennast aga näinud olema kui Joosep Luik wangi kambri akna al wangidega juttu ajanud ja siis akna kõlates kinni wiskanud on.
 Tunnistajaks Hans Peterson ütleb ennast aga näinud olema kui Joosep Luik vangi+ +kambri akna all vangidega juttu ajanud ja siis akna kõlades kinni visanud on.

#Hans Paulson tunnistab et tema jutte ajamist akna al ei ole kuulnud, aga seda kül näinud on, et see wõeras mees kattusemaa kaugusest akna pihta midagi wiskanud on.
Hans Paulson tunnistab et tema jutte ajamist akna all ei ole kuulnud, aga seda küll näinud on, et see võõras mees ¤ kaugusest akna pihta midagi visanud on.

#Mats Matson ja Jaak Lõo ütlewad aga et naad seda näinud kui Joosep Luik wangi toa akna juures olnud ja nemad siis kuulnud et akkan kõlisenud on.
Mats Matson ja Jaak Lõo ütlevad aga et nad seda näinud kui Joosep Luik vangi+ +toa akna juures olnud ja nemad siis kuulnud et aken kõlisenud on.

#Kaebatawast tunnistajaks ülese antud Saaremaa mees Priido ütleb seda kül näinud olema et Josep Luik wangi torni akna al olnud aga seda ei tea tema kes akna katki löönud on.
Kaevatavast tunnistajaks ülesse antud Saaremaa mees Priidu ütleb seda küll näinud olema et Joosep Luik vangi+ +torni akna all olnud aga seda ei tea tema kes akna katki löönud on.

#Sellepeale tõi Holstre wallaeestseisja Ado Kott järelseiswat kaebtust ette: Temale on teada antud saanud, et Joosep Luik wangi torni akna katko löönud on, kui ta selle mehe waahtide üle waatanuse ala pannud, on seesama siis üttelnud: "Saab näha kes siga mo kinni paneb!"
Sellepeale tõi Holstre valla+eestseisja Ado Kott järelseisvat kaebust ette: Temale on teada antud saanud, et Joosep Luik vangi+ +torni akna katki löönud on, kui ta selle mehe vahtide üle+ +vaatamise alla pannud, on seesama siis ütelnud: "Saab näha kes siga mu kinni paneb!"

#Tunnistaja Märt Peep ütleb ennast kuulnud olema kus Josep üttelnud on, saab näha kes siga see on, kes mo wahi ala paneb.
Tunnistaja Märt Peep ütleb ennast kuulnud olema kus Josep ütelnud on, saab näha kes siga see on, kes mu vahi alla paneb.

#Kui nüüd awalikuks saanud et Joosep Luik wangidega jutto ajanud, wangi torni akna katki löönud ja wallaeestseisjad Ado Kotti sõimanud on sellepärast
Kui nüüd avalikuks saanud et Joosep Luik vangidega juttu ajanud, vangi+ +torni akna katki löönud ja valla+eestseisjad Ado Kotti sõimanud on sellepärast

#Mõisteti: Nimetatud Joosep Luika ühes selle protokolli ärakirjaga wallawalitsuse nõudmise pääle Keiserliku Willandi Sillakohtu kätte saata.
Mõisteti: Nimetatud Joosep Luika ühes selle protokolli ärakirjaga vallavalitsuse nõudmise peale keiserliku Viljandi sillakohtu kätte saata.

