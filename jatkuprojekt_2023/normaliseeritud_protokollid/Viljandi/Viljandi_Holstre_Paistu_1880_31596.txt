Ette tuli Ado Hendrikson ja kaebab, et tema Wiljandi linnast tulles purjus olnud ja Ado Kibe ja Jaak Weibergiga ühe saani peal sõitnud, siis on Ado Kibe Wasila karjamõisa lähedal tema põuest enam kui 20 Rubla raha ühes kottiga ärawõtnud: Mispärast kaebaja oma raha Kibe käest tagasi nõuab.
Ado Kibe wastab et tema Ado Hendriksoni ja Jaak Weibergiga kül ühe saani peal sõitnud, aga selle raha ei olewat ta mitte wõtnud, waid kaebaja on Pirmastu kõrtsis maas purjus olnud. Jaak Weiber on teda ühes katsunud Ado Hendriksoni Wasila karjamõisa saatma.-
Sellepärast et Ado Hendrikson ilma aegu teotuse juttu tema peale ajanud, nõuab tema 300 Rubla auuteotamise eest.
Jaak Weiber tunnistab et tema Ado Kibet ühes kutsunud ei ole, aga naad on kolmekesi kõrtsist tulnud, ning on Ado Hendrikson purjus olnud ja on selle kübara järele toonud on ta rääkinud et Ado Kibe oma peo kinni pigistanud ja taskusse pistnud. Ado Kibe käsi on Pirmastu kõrtsist kuni Wasila karjamõisani Ado Hendriksoni põues olnud, aga raha wõtmist ei ole ta näinud.- Ado Kibe on lubanud kuni hommikuni sääl karjamõisas paigal olla aga on salaja ärapõgenenud.-
Jaan Hendrikson ütleb et seni kui tema Pirmastu kõrtsi raha kuulama läinud, on Ado Kibe ära kadunud olnud.-
Ado Kibe wastab et juba sellest näha et tema waras ei ole, et Pirmastu kõrtsi raha otsima saadetud.
Et nüüd täielt tunnistust ei ole et Ado Kibe raha wõtnud oleks, aga ka Kibe ennast õigeks teha ei saa, sest et tema käsi Ado Hendriksoni põues olnud sellepärast
Mõisteti: Ado Hendrikson ja Ado Kibe ei wõi omast wastastiku nõudmisest ühtegi saada. 
Kui eesseiswa otsus kohtukäijatele kuulutadud ja needsamad Appellationi formaliadega tutwustatud, palusiwad mõlemad kohtukäijad Appellationi mis neile ka lubatud sai.
