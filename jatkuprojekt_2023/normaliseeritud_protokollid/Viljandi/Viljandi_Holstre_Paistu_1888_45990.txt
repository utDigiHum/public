Holstre kogukonnakohtus 11" Märtsil 1888.
Koos olid: Peakohtumees Jaak Luik
 Kohtumehed Mihkel Hansen
 Johann Andresson
 Johann Jõgewest
Istumine algas kella 1/2 11 
enne lõunat
Ette tulid Holstre Pombre talu No. 105 omanik Andres Uint ja tema wäimees Mihkel Willem ning palus Andres Uint oma järgmist lahtimist mis ühel hoobil ka tema töstament on, protokolli panna, et mitte peale tema surma periate wahel kohtukäimisi ja waielusi ette ei tuleksid:
Pombre talu ühes talu sexx kraamiga, peale selle üks hobune kaks lehma, lambad ja kaks siga, lubab Andres Uint Jüripäewal 1889 oma wäimehele Mihkel Willemal ja selle naesele Annele (Jüripäewal) jäädawaks omanduseks kätte anda; neile ja nende lastele igawese aja peale.
Et töised tütred Mari, Epp ja Mall kes Willandi walda mehele on saanud, juba oma jao isa warandusest kätte saanud siis ei ole neil ei Pombre kitsendamata kombel Mihkel Willema ja nende laste omanduseks.
Mihkel Willem lubab oma naese wanematele seni kaua kui nad elawad, peale prii korteri igal aastal ülese pidamiseks ilma hinnata anda:
1. prii kütte ja keedu puud.
2. Kaks töistkümmend wakka rükkid ja kakstöistkümmend wakka otre.
3. Üks lehm ja üks lammas saawad talu laudas talu põhu peal peetud ja käiwad nad talu karjase üle waatamise all talu karjas.
4. Sula raha kolmkümmend wiis rubla.
5. Üks poole töise aastane siga keda talu poolt seitse nädalat nuumatud.
6.Pool wakkamaad kartuhwlemaaks.
7. Talu poolt saab Andres Uint ja selle naene iga tahes hobust kui nad seda tarwitawad.
Andres Uint lubab weel 1889 aasta kewadest kuni lõikuseni Willemale ja selle perele prii leib anda ja kõik wilja seemne anda, lisab aga selle keigele weel juure et kui wäimees Mihkel Willem wõi selle naene temale wõi ta naesele wasta hakkawad, neid halwaste peawad ja pahandawad, tema Pombri talu siis oma kätte tagasi wõttab. Muidugi peab see tagasi wõtmise põhjus enneseda selgeks tehtud saama. Jaak Undi Anne pojale jättab Andres Uint ühe lehma 25 rubla wäärt, Mihkel Willemi kätte.
Mõisteti: Seda leppingut protokolli wõtta ja seda sama kohtu poolest kinnitada ja kõigedest allakirjutada lasta,

Andres Uint xxx
Mihkel Willem [allkiri]
Peakohtumees JLuik [allkiri]
Kohtumehed MHansen [allkiri]
 JAndresson [allkiri]
 JJõgewest [allkiri]
Kirjut Raudsepp [allkiri]
