#Kaebas Kustaw Reinmann, et temma aidast on enne Jõulo pühha kaks willast kangast üks hal ja teine must ja seäl jures ka paelad ja woodri rie keik ühhes ärra warrastud sanud. Peäle selle ütles Kustaw Reinman, et Karel Härma sest wargusest wõib tunnistust anda. Tulli ette Karel Ärma ja tunnistab, et temma on Ranna walla mehhe Jakob Rätsepa jures käinud ja hakkanud temmaga ühhest ja teisest asjast räkima ja wimaks küssinud temma Jakob Rätseppa käest kas sinna ei tea sest Reinmanni wargusest? Jakob Rätsep üttelnud: minna käisin Kadrina mehhe Jürri Lästiga linnas ja müisin selle musta kallewi kanga pakko 9 rubla hõb. eest 30 arsinad, ja teine hal kanga pak jäi Willem Tõnsti kätte ja paelad ja kallingur. Tulli ette Willem Tõnst, sai küssitud temma käest: Kas sinna ei tea Reinmanni wargusest. Willem Tõnst ütles: Minna löön Jakob Rätsepa surnuks selle juttu peäle, minna ei olle temmaga warrastanud. Nüüd küssis Kustaw Reinmann Willem Tõntsi käest, sinnul peab kue rie rätseppa käes ollema, kust sa seda said, Willem wastas: on jah rie ja paelad ja nöbid keik Rätsepa käes.


# Tulli ette Jürri Läst ja ütles wälja, et temma on enne Jõulu Tartu sõitnud ja temmal on 6 leisikat linnu peäl, ja kui temma liggi 3 wersta koddost kaugel tee peäl olli, siis on üks Ranna walla pois Jakob Rätsep tee peäl tedda pallunud tedda Tartu wia, ja lubbanud temmale 3 rubl. h. wori rahha maksta, sel Jakob Rätseppal olnud üks pole küllimittune walge kott ühhes, agga temma polle mitte teadnud, mis seäl kotti sees olnud. Jakob Rätsep on Tartus temmale 3 rubla hõb. wori rahha ärra maksnud.


# Kutsuti ette Jakob Rätsep ja sai temma käest küssitud: kas temma on Jürri Lästiga Tartus käinud, Rätsep ütles, et temma polle 4 aastad, egga ka sel süggiselgi Tartus käinud, ja temma sest wargusest middagi ei tea.


# Tulli ette Peter Ärma Tartust ja sai tema käest küssitud, kas on sesamma mees, kes Tartus sinnu jures käis, ja sind abbiks kutsus willased kangad ärra müima, ja Ärma ütles: Ma arwan, et se samma mees on, agga temmal on nüüd üks teine kuub ja temma worimees olli pitk mees, mustakas pruni habbemega. Se Protokoll sai Tartu Makohtu kätte andud.


# Peakohtumees Janus Peetrus xxx


# abbi kohtumees Juhan Annask xxx


# abbi kohtumees Johan Kirrik xxx


