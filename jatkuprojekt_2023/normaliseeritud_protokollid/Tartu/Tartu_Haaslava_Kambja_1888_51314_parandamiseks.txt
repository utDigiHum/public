#Sel 5 Detsembril 1888


#


#Selle otsuse päle sest 28 Nowembrist Terraski kaibusen wasto Järgawitz olli üllesandmise päle Johan Luha Haslawast ette tullnu ja tunistas, et sis kui Terrask töö jurest ärra lännu, ollnu seal ehituse jures kül weel palke maan ja üts kõrd üttelnu Terrask tema wastu, et tema, Terrask, seda kaubeltut Ellotarre Kristjan Järgewitzale ärra ei tee, et se wõib ka tegemada jäda.


#Kristjan Järgewitz ütles, et Terrask ei olle lubanu Ellotarre tehha ja ei ollewat ka Rehhe tarre kauba perra ommal aijal walmis sanu.


#Terrask ütles, et tema olles ka Ellotarre ärra tennu kui Materiali olles ollnu ja et tema enegi 92 Rubl ehituse raha sanu om.


#Kristjan Järgawitz ütles et


#


#


#			Rubl


#			Kop


#		


#


#Madis Terrask om sanu


#			92


#			


#		


#selle töö mees Johan Pusep om sanu


#			32


#			


#		


#ja selle hone walmistegemisse est


#			10


#			


#		


#ja tömehed om tema käest sönuwa


#			18


#			96


#		


#Selle perra om tema selle ehituse est massnu


#			152 rubl


#			96 kop


#		


#Madis Terrask ütles, et tema selle hone 5 jalga pikemba ja 2 jalga laiemba om tennu ja nõudis selle est 5 rubl sülla pält.


#Kristjan Järgawitz wastutas, et tema selle hone 3 jalga pikembas om teha laske, selle et Meister sis pool kelbast sarast es prugi teha, mis kaubeltu ollnu ja et se hone laijemb ei ollewat.


#Kauba leping sai läbbi loetu ja sest ei seisa midagi kelbasest ja ei olle ka nimitedo kas 6 ehk 7 jalga süld kaubeltu olli ja Järgawitz arwas et neide kaub 7 jallalise süld ollnu.


#


#Mõistetu: et Madis Terrase nõudmisse kaibus wasto Kristjan Järgewitz tühjas om mõista, selle et tema joba rohkemb kui kaubeltu raha tehtut rehe tarre est kätte om sanu ja Ellotarre ehituse kaubast essi taganenu om ni kui Johan Luha tunistas.


#Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu. 


#


#Päkohtumees: Johan Link XXX


#Kohtumees: Jürri Kärn XXX


#" Johan Opman XXX


#" Johan Kliim XXX


