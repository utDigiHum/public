#Awwinorme Metskoggokonnakohtus sel 2 Novembril 1879.
Avinurme Metskogukonnakohtus sel 2 novembril 1879.

#koos ollid
Koos olid

#peakohtomees Josep Laurisson
peakohtumees Joosep Laurisson

#teine kohtomees Mihkel Sild
teine kohtumees Mihkel Sild

#kolmas kohtomes Jaan Pukk
kolmas kohtumees Jaan Pukk

#Astus ette Truta Hallik omma mehhe Jürri Halliku juures ollemissel ja kaebas et Mart Hallik kelle maa peal nemmad ellawad. on rebbinud ja lopinud ning temmal paljo haigid tehnod. ja on temma siis omma werremärgid kohtomehhe Jürri Tomiku nenda kohhe näitanod. ja olnud temma siis parremb kõrw werrine ja parrema käe pealt nahk mas, ja wennitanud Mart Hallik temma käe õllast ärra kellega temma nüid töed tehha ei sa. ja pallub temma selle asja ülle kohto moistust.
Astus ette Truuta Hallik oma mehe Jüri Halliku juures+ +olemisel ja kaebas et Mart Hallik kelle maa peal nemad elavad. on rebinud ja loopinud ning temal palju haiget teinud. ja on tema siis oma veremärgid kohtumehe Jüri Toomiku nõnda kohe näidanud. ja olnud tema siis parem kõrv verine ja parema käe pealt nahk maas, ja venitanud Mart Hallik tema käe õlast ära kellega tema nüüd tööd teha ei saa. ja palub tema selle asja üle kohtu+ +mõistmist.

#Mart Hallik räkis et Truta Hallik on üks wanna lihha astja kaewo äre venud. ja sinna likku panna tahtnud kedda agga temma ei olle lasknud. ja on siis ka temma üks vanna must wee kop kedda Truta Hallik temma aita weenud. sedda wälja wiskanud ja ei olle temma Truta Hallikud mitte näppiga putunod ehk se küll tedda wargast ja masurikust sõimanud, ja tahtnud temma agga se wanna innimenne omma Aita kinni panna mis pärrast se siis kissendanud.
Mart Hallik rääkis et Truuta Hallik on üks vana liha+ +astja kaevu äärde viinud. ja sinna likku panna tahtnud keda aga tema ei ole lasknud. ja on siis ka tema üks vana must vee+ +kopp keda Truuta Hallik tema aita viinud. seda välja visanud ja ei ole tema Truuta Hallikut mitte näpuga puutunud ehk see küll teda vargaks ja masuurikuks sõimanud, ja tahtnud tema aga see vana inimene oma aita kinni panna mis+ +pärast see siis kisendanud.

#Jaan Kimmel tunnistas et temma on omma toa jurest nähnud kes 180 sammu selle Aida juurest kaugel et Mart Hallik on asju Aidast wälja kannud ja et seal suur kissa ja kärra olnud. agga taplust ja löömist ei olle temma nähnud.
Jaan Kimmel tunnistas et tema on oma toa juurest näinud kes 180 sammu selle aida juurest kaugel et Mart Hallik on asju aidast välja kandnud ja et seal suur kisa ja kära olnud. aga taplust ja löömist ei ole tema näinud.

#Kohtomees Jürri Tomik tunnistas, et temma on kohhe peale selle tülli sinna lähnud sedda asja järrel kuulama, ja on Truta Hallikul siis kõrw werrine ja parrema käepealt nahk ärra olnud., ning on Mart Hallik ütlend egga minna parremast käest kinni wõtnud, minna wõtsin pahhemast käest kinni.
Kohtumees Jüri Toomik tunnistas, et tema on kohe peale selle tüli sinna läinud seda asja järgi kuulama, ja on Truuta Hallikul siis kõrv verine ja parema käe+pealt nahk ära olnud., ning on Mart Hallik öelnud ega mina paremast käest kinni võtnud, mina võtsin pahemast käest kinni.

#Mart Hallik kostis selle peale et kohto mees Jürri Hallik walletab ja ei olle temma sellele mitte sedda ütlenud, et temma omma kässi Truta Halliko külge pannud.
Mart Hallik kostis selle peale et kohtu+ +mees Jüri Hallik valetab ja ei ole tema sellele mitte seda öelnud, et tema oma käsi Truuta Halliku külge pannud.

#Moistus
Mõistmine

#Ehk selle tülli juures keddagi tunnismeest ei olle olnud kes woiks tunnistada et kui paljo Mart Hallik sedda vannainnimest on rebbinud ehk sellele haiget tehnud, Kohtomees Jürri Tomik agga tunnistab. et Truta Hallikul kõrw werrine ning käepealt nahk ärra olnud. siis on se selge assi et Mart Hallik on sedda wanna innimest lopinud. ja moistab Koggokonnakohhus et Mart Hallik peab selle wanna innimesse Truta Hallikulle 2 Rubl wallurahha maksab ja saab selle eest et temma Kohtomest Jürri Tomikud wallelikkust sõimab 24 tunni peale kinni pantud.
Ehk selle tüli juures kedagi tunnismeest ei ole olnud kes võiks tunnistada et kui palju Mart Hallik seda vanainimest on rebinud ehk sellele haiget teinud, kohtumees Jüri Toomik aga tunnistab. et Truuta Hallikul kõrv verine ning käe+pealt nahk ära olnud. siis on see selge asi et Mart Hallik on seda vana+ +inimest loopinud. ja mõistab kogukonnakohus et Mart Hallik peab selle vana+ +inimese Truuta Hallikule 2 rbl valuraha maksab ja saab selle eest et tema kohtomeest Jüri Toomikut valelikuks sõimab 24 tunni peale kinni pantud.

#Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastast 1860 §§ 772 ja 773 kuulutud ning nende kohto käijatelle need säduse järrel Appellationi õppetusse lehhed wälja antud. Mart Hallikulle agga Appellation keldud
See mõistmine sai selle Liivimaa Talurahva seaduse põhja peal aastast 1860 §§ 772 ja 773 kuulutatud ning nende kohtu+ +käijatele need seaduse järgi Apellatsiooni+ +õpetuse lehed välja antud. Mart Hallikule aga Apellatsioon keeldutud

#Mart Hallik tunnistas et temma selle moistusega rahhul ei olle et temmale trahvi moistetud. temmal agga ühtegi süid ei olle. ja sai temmale se lubba antud selle moistuse ülle kahheksa päawa sees Kihhelkonnakohto juure kaeptust tosta 
Mart Hallik tunnistas et tema selle mõistmisega rahul ei ole et temale trahvi mõistetud. Temal aga ühtegi süüd ei ole. Ja sai temale see luba antud selle mõistmise üle kaheksa päeva sees kihelkonnakohtu juurde kaebust tõsta 

#Josep Laurisson XXX
Joosep Laurisson XXX

#Mihkel Sild XXX
Mihkel Sild XXX

#Jaan Pukk XXX
Jaan Pukk XXX

