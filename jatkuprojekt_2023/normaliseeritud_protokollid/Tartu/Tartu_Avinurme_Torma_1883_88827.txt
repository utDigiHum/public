Awwinorme Mets Koggokonnakohtus sel 20 Mail 1883.
koos ollid
peakohtomees Maddis Ambos
teinekohtomees Jaan Tomik
kolmas kohtomees Jürri Laurisson
Kaebasid selle walla ärra surnud pobbol innimesse Andres Jõggi tüttred Marri ja Kadri Jõggi Eeskostjatte juures ollemisel et nende kaddunud issa on omma weike krunt Maddis Kiige käes rendi peal hoidnud et siis nende issa Andres Jõggi minnewa talwel ärra surnud siis nõuawad nemmad se krunt enneste kätte.
Maddis Kiik räkis et temma se krunt omma wenna Andres Kiige käest 5 Rubla eest ostnud kuhhu temma jubba ellumaja ja kõrwalised honed ülles ehhitanud ning ka se krunt metsast puhtast puhhastanud mis pärrast temma se krunt Andres Jõggi pärrijattelle wälja anda ennamb ei tahha.
Andres Kiik räkis et se õige et temma se krunt omma wenna Maddis Kiigele müinud 5 Rubla eest ja on temma se rägitud krunt jälle Andres Jõggi käest selle samma hinna eest ommale ostnud, mis järrel temmal lubba olli seddasamma jälle eddasi müija, ehk küll se ostminne Protokolli ülleskirjutud ei seisa.
Maddis Kukk tunnistas et temma kuulnud kui kaddund Andres Jõggi on omma ellu ajal ütlenud egga minna se krunt mis nõmmenukkas Maddis Kiigele müinud ei olle wait rentinud.
Jaan Pomm tunnistas et temma kuulnud kui Andres Kiik on Andres Jõgge koggokonnakohto juure kutsunud ning ütlenud wõtta se krunt wasto mis külla annab mis peale Andres Jõggi se krunt wasto wõtnud ja on Andres Kiik Andres Jõggile 5 Rubla annud agga sedda temma kulnud ei olle et Andres Jõggi olleks ütlenud jah selle hinna eest müin jälle se krunt arra.
Andres Unt tunnistas et temma ka kuulnud kui kaddund Andres Jõggi omma elluajal räkinud et "egga minna se krunt Maddis Kiigele müinud ei olle."
Andres Sildnik tunnistas et temma kuulnud kui Andres Jõggi on räkinud egga minna se krunt Maddis Kiigele ärra müinud ei olle.
Mõistus.
Et see töö mis Maddis Kiik seal krunti peal tehnud koggokonnakohto poolt ärra takseritud peab saama siis jäeb se assi teise kohtopääwa selletada.
Maddis Ambos XXX
Jaan Tomik XXX
Jürri Laurisson XXX
