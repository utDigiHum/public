#Krimanni koggokonna kohtun 17 Febr. 1869.


#


# Jürri Klaos tulli ette ja ütles: Mõisa nõuab minno käest Kultena 14ma wakama kaera külw mis nüüdne Rentnik H. Freyfeldt on Mõisale üllese annud et Temmal sest 14 wakkama külwist on pudus jänud 32 wakka kaero ja 140 leisikat põllo põhko seperrast et se kara seme ei olle mitte kassunud ja minna ollen se seme Jaan Põddersoni käest ostnud ja mõisale saatnud sepärrast et minna wanna Rentniko Jaan Selli eest kässimehheks jäin ja se Kultena Mõisa Inwentaris wilja seme enda käest andma piddin mis ma enda peale tol korral ollin wõtnud. Kui se kaer olli üllese tulnud siis käisin kohtomeestega ülle kaemas ja leidsime et wägga halwast ja arwast plli üllese tulnud ning leppisime siis seal kohtomeeste een ärra et annan Rentnik Freyfeldile 14 wakka seme keswi ja Freyfeldt teeb isse need keswad nende 14 wakkama kaerte peale mahha mis wäega arwast olli üllese tulnud. Peale se ollin Haslawa Magatsist need keswad wõtnud ja Kultena saatnud agga H. Freyfeldt ei olle neid mitte wasto wõtnud ning saatis taggasi.


# H. Freyfeldt tulli ette ja ütles: et minnul ei olle Klaose käest middagi nõudmist sest minno kaup olli et saan Mõisast suwwi wilja Inwertarumi seme ja neist 30 wakkast kaertest mis Klaos on kõik saatnud ja selle 14 wakka ma peale külwasin on agga 100 terra pealt 30 terra kassunud mis minno sullased ja kohtomees Ado Lentzius wõib tunnistata. Ja se ei olle mitte tõssi et ollen Klaosega ärra leppinud et mulle keswad sadab sest meie tahtsime kül tol korral leppida ning minno leppitus olli sel õhtul et kui Klaos annab 16 wakka seme keswi ja sadab teisel päewal need keswad mulle ärra siis olleks leppitud olnud agga Klaos ei lubbanud mitte rohkem kui 14 wakka keswi ja nende 2 wakka keswi perrast olli sel korral meie leppitud katski jänud. Peale se 3mal wõi 4mal Päewal tõiwad Klaose sullased need keswad agga et need keswad wäega kõhnad ollid ja küllimmiene hildast jänud ei wõinud ma mitte neid wasto wõtta.


#Jaan Põdderson tulli ette ja ütles minna ollin need kaerad kül Klaosele müinud ja ütlesin kauba teggemise aego Klaose et head semet mul ei olle sest minno kaerust jäeb 30 terra 100 terra pealt kassumata siis küssis Klaos mis sa endale mahha külwad siis ütlesin iks neid sammo pean külwama ja ollin sest sammast salwest endale ka mahha külwanud, mul olliwad illusad ja parrajad karad kõrre polest kassunud.


#


#Mõistus: Koggokonna kohtun


#Koggokonna kohhus mõistis et Klaos peab se Rentniko kahjo maksma


#1) seperrast et nad leppitust ei olle sel korral (õhtul) keswiga sanud mis koggokonna kohtomehhed tunnistawad ja


#2) et ta halwa kaera seemne olli Mõisale annud mis koggonist seisew Inwentariumi seme piddi ollema.


#


#Kohto lauan olliwa


#Peakohtomees Jürri Bragli XXX


#Kohtomees Juhan Mina 


#" " Ado Lentzius


