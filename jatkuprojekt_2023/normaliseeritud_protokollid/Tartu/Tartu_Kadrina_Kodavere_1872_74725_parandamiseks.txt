#Tulli ette Kadrina mõisa wallitseja Kusik ja kaebas, et temma ollewad minnewa näddala üks õhtu omma tuas laua otsas istunud, siis on tulnud mõisa sullane Jürri Õunap sisse, ja on joobnud olnud, ja küssinud aggu metsast ärra weddada, mis temma kokko on pannud, mõisawallitseja üttelnud: Et wedda wälja platsi peäle, siis wõid seält koddo weddada, peäle selle ütelnud Jürri Õunap, sa kurrat pead neid kohhe laskma koddo wia ja mitte platsi peäle, sa kurrat ei kustuta päwi, sa pead päwad kustutama pole ö aial, kui minna tullen, ja on tahtnud temma rinnust kinni wõtta, siis on wallitseja tedda tuast wälja aianud, ja wäljas on Jürri Õunap weel sõimanud, mis mõisa sullased Jakob Martin, Josep Kirw ja Jakob Wälja on kuulnud. Tulli ette Jürri Õunap ja ütles, temma on wallitseja käest aggusid küssinud ja mõisa wallitseja ollewad temma käe mõegaga mahha lönud, mis Aron Pusep näinud ollewad.Tunnismees Jakob Märtin ütles, et temma on agga koormaga metsast tulnud, ja näinud, et Jürri Õunap on wallitseja haknade all seisnud ja ommale rusikaga wasto rindu lönud, ja hirmus kõwwast rökinud ja üttelnud: Siin seisab Jürri Õunap, mis kurradi wallitseja sinna olled, sa ei olle muud kui mõisa pois, ja peäle selle weel, sa kurradi litse narja, sa kurradi warras, sa olled minnu päiwi warrastanud ja Jürri Õunap tulnud nende jure ja hakkanud nende sullastega ka tülli teggema, sest ta olnud joobnud. Tunnismees Josep Kirw ja Jakob Wälja tunnistasiwad nenda sammati keik. Tunnismees Ann Pusep ütles, et temma ei olle keddagi nähnud, et wallitseja on Jürrid lönud, sest temma toast on kolm ukse kinni wallitseja tuppa, ja temma ei teagi kas Jürri Õunap on toas käinud, waid kuulnud, et Jürri on wäljas mürranud. ja tulnud tema pole tuppa ja ütelnud, temma kurrat ei anna aggusid, egga kustuta päiwi, millal minna tahhan.


# Kohhus mõistis: Et Jürri Õunap peab Mõisawalitseja roppu sõimamise eest 6 Tubla hõb. trahwi walla waeste laeka maksma. Kes sellega rahhul ei olle, wõib Protokoll wälja wõtta. Mõisawallitseja wõttab Protokoll wälja.


# Kadrinal sel 19. Detsembril 1872.


# Pea kohtumees: Janus Peetrus xxx


# abbi kohtumees Johan Annask xxx


# abbi kohtumees Janus Jannast xxx


# kog. kon. kirj. J. Saul.


