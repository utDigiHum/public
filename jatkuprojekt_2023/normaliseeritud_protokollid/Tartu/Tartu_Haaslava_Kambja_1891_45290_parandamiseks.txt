#№ 2


#Sel 7 Januaril 1891


#


#Selle otsuse päle sest 10 Detsembrist s.a tunistas Jürri Opman et tema midagi ähwarusest tunistada ei tea.


#Jüri Opman /allkiri/


#Karl Rosenthal Haslawast tunistas, et kui Jaan Roiland Jaan Pruksa põlleno Küini jurde tullnu sis küssinu Roiland kas teine küin ärra om põllenu ja se teine küin ollnu põllemas ja õhkumas, mis selgest nätta ollnu. 


#Karl Rosenthal ei mõista kirjutada.


#Ann Muga Haslawast Jaan Pruksa tüdruk tunistas, et päle küini põllemist sel päiwal tullnu Jaan Roilandi poeg Kusta Pruksa tallo ja küssinu kas küinit tulle kassan ollid ja päle selle ösel tullnu Kusta Roiland Pruksa poole tuppa ja küssinu kas öwaht wäljas om ja minna ollen ka öwahis ja nägin, et Pruksa tallon tulli ärra kütas ja tullin kaema, tema wastanu, et perremees om öwahin, selle päle üttelnu Kusta Roiland, et olles minna teadnu sis olles minna käinu nukka man näidanu sis olles kätte sanu, mis perremees olles tennu, tema üttelnu, et ärra nida kõnnelku, selle päle wastutanu Kusta Roiland, et ega perremees temale midagi ei olles tennu.


#Ann Muga ei mõista kirjutada.


#Mili Beekman Haslawast 16 astat wanna ja ei olle päkolin käino om ka Jaan Pruksa Sõssara tüttar tunistas, et ösel tullnu Kusta Roiland Pruksa tuppa ja küssinu kas öwaht wäljan ja kas se öwahtimine om kui tulli ärra om kistutedo.


#Emilie Peekmann /allkiri/


#Karl Reili Krimani Roijo kõrzimees tunistas et üts kõrd päle Pruksa küini põllemist ollnu Roijo kõrzin sellest kõnne, sis üttelnu temma, et mis sellega sis Jaan Pruksale ärra om tehtu, Jaan Pruksa jääb iks sesamma Jaan Pruks, selle päle wastutanu Jaan Roiland, kes ka Kõrzin ollnu, et selle südda sai rahul kes seda tegi.


#Karl Reili /allkiri/


#


#Otsus: et Tilba Sepp Masing ette om kutsuta.


#


#Päkohtumees: Jaan Wirro XXX


#Kohtumees: Jakob Saks /allkiri/


#" Peter Hansen /allkiri/


