Hans Tillisson lut. usko tunnist et tema seda pilli lõhkumist mitte nännu, kui paljalt pääle sell nännu et Johan Andersoni pill ärä ollu lõhutu.
Andresse talu peremees Peter Pikson lut. usko tunnist et selle korral õdagul kos tuli ülewel ollu tema Peter Piksoni maija, tulnud Andres Juhkamson, Karl Bloom ja Hans Keerman, kohe pääle selle kah Johan Anderson om pilliga pääle selle sisse tulnud, tooli pääle istund mängi tahtnud, keda tema peremees Pikson keelnud ja kah pääle sell mängi käsknud, keda jälle perenaene keelnud, kelle pääle Karl Poom Johan Andersoni keelnud. Pääle selle karand kaebaja Johan Anderson tooli pääl üles, tõmmanu Ploomil kõrist kinni ja pannu pikkali maha, kos Hans Keermann teda kaetega ja kah Karl Ploom pesma hakkanud kui maast üles tulli, siis wõib olla et kaebaja pill nende kakkeluse wahel jalgu all ärä sai sõkkutus.
Tema Johan Anderson ei ole riide ort mitte maha ajanud wait palja riide küll orre päält maha ajanud.
Johan Tompson lut. usko tunnist, et pääle Andres Juhkamsoni Karl Ploomi ja Hans Keermanni Andresse talule tuleku kah Johan Anderson oma pilliga sell samal korral senna tullu mängi tahtnud mängi keda kah pere mees lubanu, aga perenaene olno käsknu Karl Ploomil mängimist keelda, kelle pääle Karl Poom kaebaja pilli riigöstre kinni litsund kelle pääl Anderson Johan Karl Ploomi rinnust kinni tõmmanu nig maha wisanu, kelle pääle nema mõlema pääle selle kakkelnu ja wastaside kiskunuwad. Hans Keermanni Johan Andersonil löömist ei nännu tema Johan Tompsn kaebaja Johan Anderson olno kaks kõrda wasta riide ort löönu ning (pääle selle) kah maha löönu.
Otsus: Tulewa kohtupäewas Hans Keermann ette telli.
pääkohtumees J. Hunt [allkiri]
kohtumees: A. Tehwand [allkiri]
 " : W. Ehrman [allkiri]
Kirjut: M Jakobson [allkiri]
Jarel tulew leht 69
