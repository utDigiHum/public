#Haslawa Kogukonna Kohus sel 8 Aprilil 1885


#Man ollid: Päkohtumees Jaan Link


#Kohtumees Johan Klaosen


#" Peter Iwan


#


#Kaibas Peter Pier Krimanist, et temal Miko Hansu tallu Perremehe Johan Janseni käest 10 Rubl 65 kop poja päraha, ja 3 wakka Keswi poja palka sada om. Päle selle om temma 1 Wassik wärt 5 Rub 50 kop, ½ wakka Nisso, 1 wakk surmit, 1 suur Sarri wärt 2 Rub ja 1 suur Saag wärt 150 kop Johan Jansenile andno ja pallus et Kohus seda poja päraha ja palka ja ka neid tõisi asju massma sunis.


#Johan Jansen wastutas, et tema kauba perra 2 asta poja Päraha 12 Rubl ärra massnu om, kolmanda asta päraha ei olle temma massa lubanu ja sis ollewat temal kolmanda asta est 1 wakk keswi kül massa, tõiste nõudmiste perrast wastutas Johan Jansen, et 1 wassika om tema kül kassuma wõtnu ja wõib ka iga kõrd selle wassika assemele anda, Nisso om tema ärra massnu ja surmit ei olle tema suguki sanu, ent selle wastu om temma ½ wakka linnasemnit andno ja et Peter Piir om 15 Rub tema käest lainanu, kust 9 Rub tagasi om sanu.


#Peter Piir wastutas, et 4 Rubl om temma Johan Janseni käest sanu ja neid linnasemnit ei olle mitte pool wakka ollnu ja need ollnu ni kui purro ja andmisse jures om Johan Jansen üttelnu et tema neide est midagi ei nõua.


#


#Mõistetu: et Johan Jansen se Päraha 10 Rubl 65 kop, 1½ wakka keswi, wassika est 5 Rubl 50 kop, ½ wakka Nisso ja Saage est 150 kop 8 päiwa sehen Peter Piirile ärra massma peab, surme ja Sarja nõudmine om tühjas mõista ja et sis ka Peter Piir se 4 Rubl Johan Jansenile 8 päiwa sehen ärra massma peab.


#Se mõistus sai kulutado ja õpetus leht wälja antu.


#


#Päkohtumees: J. Link /allkiri/


#Kohtumees:


#Johan Klaosen XXX


#" Peter Iwan /allkiri/


