#Sel 27 Märzil 1889.
Sel 27 märtsil 1889.

#


#Haslawa Nõukogu poolt wollitu Karl Lesta ja Mihkel Rattasep kaibasiwa päle selle kui nemma omma Wollikirri ette ollid näidanu, et Magatsi wilja mõtmisse jures 1888 astal om 15 Tsetwerti 59 Karnist Kaero pudus tullnu ja 2 Tsetwerti 4 Karnist Rükki ja 2 Tsetwerti 33 Karnist Keswi om ülle jänu, nemma palluwa seda pudust wanna ametimeste massa panda.
Haaslava Nõukogu poolt volitatud Karl Lesta ja Mihkel Ratassepp kaebasid peale selle kui nemad oma volikiri ette olid näidanud, et magasi vilja+ +mõõtmise juures 1888 aastal on 15 setverti 59 karnist kaeru puudus tulnud ja 2 setverti 4 karnist rukki ja 2 setverti 33 karnist kesvi on üle jäänud, nemad paluvad seda puudust vana ametimeeste maksta panna.

# Selle päle ollid tännases päiwas enine Wallawanemb Jaan Soo, wallawölmündri Kusta Adamson, Endrik Zobel ja Johan Grossberg ja Magatsi ülle kaija ja wallawolinik Peter Saal ette kutsutu nemma ollid tullnu ja wastutasiwa, et nemma 29 Julil 1888 Magatsi ja wõtme ärra om andno sis ollnu wilja arw täis ja ei wõi selle puduse est wastutada, mis 9 Augustil 1888 om ollnu, wastse ameti mehe om tõise mõõtmise jures kõwaste wissata lasknu ja selle jures kaero kokko sõkunu sis wõis ka pudus tulla, nemma pandno kui tülli tullnu 2 mõõtu kotti, kus sis täieste mõõt wälja andno kui Kihelkonna Kohtu mehe jures ollnu.
 Selle peale olid tänaseks päevaks endine vallavanem Jaan Soo, vallavöörmünder Kusta Adamson, Endrik Zobel ja Johan Grossberg ja magasi üle+ +kaeja ja vallavolinik Peter Saal ette kutsutud nemad olid tulnud ja vastutasid, et nemad 29 juulil 1888 magasi ja võtme ära on andnud siis olnud vilja+ +arv täis ja ei või selle puuduse eest vastutada, mis 9 augustil 1888 on olnud, vastsed ameti+ +mehed on teise mõõtmise juures kõvasti visata lasknud ja selle juures kaeru kokku sõtkunud siis võis ka puudus tulla, nemad pannud kui tüli tulnud 2 mõõtu kotti, kus siis täiesti mõõt välja andnud kui kihelkonna kohtu+ +mehed juures olnud.

#Karl Lesta ja Mihkel Rattasep wastutasiwa, et wannad ameti mehed wäega kõwasti trikita om lasknu, selleperrast ei olle nemma seda wilja wõtnu ja pallunuwa Kihelkonna Kohtumehed jurde ja sis om se pudus tõusnu. 
Karl Lesta ja Mihkel Ratassepp vastutasid, et vanad ameti+ +mehed väga kõvasti triikida on lasknud, sellepärast ei ole nemad seda vilja võtnud ja palunud kihelkonna kohtumehed juurde ja siis on see puudus tõusnud. 

#


#Mõistetu: et sest kaera pudusest 15 Tsetwrti 29 Karnist kaero maha wõtta tulleb arwata se willi mis rohkemb om ollnu ja nimelt 2 Tsetwerti 4 Karnist Rüki est 4 Tstw 6 Ka Kaero, 2 Tstw 33 Ka Keswe est 3 Tstw 22 Ka Kaero, summa 79 Tstw 30 Ka kaero. Selle perra jääb puduses 8 Tsetwerti 29 Karnist kaero, keda enine wallawanemb Jaan Soo, wölmündre Kusta Adamson, Endrik Zobel ja Peter Saal ärra peawa massma nink nimelt peab ega üts 2 Tsetwerti 7¼ karnist Kaero massma. Selle wilja ümbre mõtmise jures om 54 päiwa prukitu, ega päiw om 50 kop arwata, seda peawa need nimitedo 4 ammeti meest ka ärra massma ja tulleb egal üttel 675 kop massa. Wallawölmünder Johan Grossberg om sest massust wabas mõista selle et tema enegi 2 kuud ametin om ollnu.
Mõistetud: et sest kaera+ +puudusest 15 setverti 29 karnist kaeru maha võtta tuleb arvata see vili mis rohkem on olnud ja nimelt 2 setverti 4 karnist rukki eest 4 tstv 6 karni kaerade, 2 tstv 33 karni kesvade eest 3 tstv 22 karni kaerade, summa 79 tstv 30 karni kaeru. Selle perra jääb puuduseks 8 setverti 29 karnist kaeru, keda endine vallavanem Jaan Soo, vöörmünder Kusta Adamson, Endrik Zobel ja Peeter Saal ära peavad maksma ning nimelt peab iga+ +üks 2 setverti 7¼ karnist kaeru maksma. Selle vilja ümber+ +mõõtmise juures on 54 päeva pruugitud, iga päev on 50 kop arvata, seda peavad need nimetatud 4 ameti+ +meest ka ära maksma ja tuleb igal+ +ühel 675 kop maksta. Valla+vöörmünder Johan Grossberg on sest maksust vabaks mõista selle et tema ennegi 2 kuud ametis on olnud.

#Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.
Se mõistmine sai § 773 ja 774 perra kuulutatud ja õpetus+ +leht välja antud.

#


#Päkohtumees: Jaan Wirro XXX
Peakohtumees: Jaan Virro XXX

#Kohtumees: Jaan Pruks /allkiri/
Kohtumees: Jaan Pruks /allkiri/

#" Jaan Toom XXX
" Jaan Toom XXX

#" Jakob Saks /allkiri/
" Jakob Saks /allkiri/

