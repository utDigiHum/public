Haslawa kogukonna kohus, sel 20 Webruaril 1884.
Man ollid:
Päkohtumees: Jaan Link
Kohtumees:
Johan Klaosen 
 Peter Iwan

Haslawa Wallawanemb J. Luha kaibas wallawalitsusse nimel, et ni kui tähed ette om tullon, om endine Päkohtumees Mihkel Klaos Päraha wasto wõtnu ja mitte seda raha tema kätte ärra andnu ja nimelt


1 Aprilil 1881
			
Endrik Waska käest
			5 Rubl 40 Cop
		
13 Mail 1881
			
Josep Willemsoni käest
			10 Rubl
		
7 Mail 1882
			
Johan Adamsoni käest
			7 Rubl 67 Cop
		
9 Mail 1882
			
Jaan Klaus "
			5 Rubl 7 Cop
		

			Summa
			28 Rubl 14 Cop
		

Wallawanemb pallus et Mihkel Klaos sunitu saas seda raha wälja massa.
Mihkel Klaos wastutas et tema need rahad kül wastu wõton om ent om jälle selle raha walla wanembale kätte andno. Mihkel Klaos nõudis oma 1882 asta Kohtumehe palk 20 Rubl ja 8 sülda puid, mis mõisa kohtumaja kütmise tarwis om andno, temma om kohto omma majan 2 astat pidanu ja mõisa poolt saab 4 sülda puid ega asta Kohtomaja kütmises need puud om Wallawanemb ommale wõtnu.
Wallawanemb wastutas, et Mihkel Klaos omma kassu perrast kohtu mõisast üiritu kohtumajast omma jurde om winu, sest Ignatse kõrtsimees ollewat 30 Rubl Mihkel Klaosele selle est massnu ja tema om 25 Rubl ega asta üiri raha mõisan massma pidanu.
Haslawa Kõrtsimees Mihkel Rattasep tunistas, et temma om Mihkel Klaosele 10 Rubl pakkunu kui Kohus mõisa olles jänu ja sis om Klaos wastutanu: mina saan Ignase kõrtsimehe käest 25 kuni 30 Rubl.
Mihkel Klaos wastutas, et Mihkel Rattasep om kül 10 Rubl pakkunu, tema ei olle seda raha wastu wõtan ja ei olle Ignatse kõrtsimehe käest midagi massu sanu se om ilm aigune kaibus.

Mõistetu: et Mihkel Klaos seda wastu wõetut Päraha 28 Rubl 14 Cop 8 päiwa sehen Wallawanembale ärra massma peab, sis wõib omma palk kätte sada ja et Wallawanemb J. Luha pool neist mõisast sanu pudest se om 4 sülda ehk 12 Rubl 8 päiwa sehen Mihkel Klaosele tagasi massma peab, selle et Mihkel Klaose jures 2 asta kohus om ollnu.
Se mõistus sai kulutedo ja õpetus leht wälja antu.

Päkohtumees : J. Link /allkiri/
Kohtumees 
Johan Klaosen XXX
Peter Iwan /allkiri/
