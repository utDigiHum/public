#sel 28al Julil 1878


#koos ollid


#peakohtomees Josep Laurisson.


#teine kohtomes Andres Kiik


#kolmas kohtomes Jaan Mänd.


#Kaebas Kadri Haaw Josep Pukki juures ollemissel et Gustaw Welti lehmad on pilla peal olnud. kedda temma lähnud ärra ajama. ja on siis Gustaw Welt tedda wiis korda lönud kakskorda wasto pead ja kolm korda selga nenda et temma mahha kukkunod. ja pallub temma selle ülle kohto moistust sest et temma sellega rahhul ei olle.


#Gustaw Welt räkis, et Kadri Haaw on tedda wargast sõimanud. ning temma wasto silmi sülge lopinud mis peale temma siis Kadri Haawa ennesest eddemele lükkanud. agga lönud ei olle temma tedda


#Marri Pärn tunnistas et temma on nähnud kui Gustaw Welt on kolmkorda Kadri Haawale kukla lönud nenda et se iggakord otseti kukkunud. ja on siis Kadri Haaw ja Gustaw Welt wastastikku kärratsenud ning sõimanud. kellest temma otsust ei woi anda sest seagga üks kärra olnud. agga sedda temma ei olle nähnud et Kadri Haaw Gustaw Weltile sülge lopinod. 


#Moistus


#Et Marri Pärn tunnistab et temma nähnud et Gustaw Welt on Kadri Haawa järrele joksnod ning sellele kolmkorda kukla löönod nenda et se nennakille kukkunod. sedda agga temma nähnud ei olle. et Kadri Haaw olleks Gustaw Weltile sülge lopinud ehk sedda wargaks sõimanod. siis moistab Koggokonnakohhus et Gustaw Welt peab Kadri Haawale kolme löögi eest 3 Robl wallu rahha maksma ning saab tapluse eest 24 tunni peale trahwist kinni pantud.


#Se moistus sai selle Liwlandi Tallurahwa sädose põhja peal Aastast 1860 §§. 772 ja 773 nende kohto käijatelle kuulutud, ning nendele need säduse järrel Appellationi õppetusse lehhed wälja antud Gustaw Weltile agga Appellationi keeldod.


#Kadri Haaw tunnistas et temma selle moistosega rahhul ei olle. et kohhus temmale 5 ränga löögi eest keigest agga 3 Robla moistnud temma agga rängaste haiget sanod. ning põddeb ja pallus Appellationi wälja suurema kohto minna


#Josep Laurisson XXX


#Andres Kiik XXX


#Jaan Mänd XXX


