#sel 28al Julil 1878
sel 28ndal juulil 1878

#koos ollid
koos olid

#peakohtomees Josep Laurisson.
peakohtumees Josep Laurisson.

#teine kohtomes Andres Kiik
teine kohtumees Andres Kiik

#kolmas kohtomes Jaan Mänd.
kolmas kohtumees Jaan Mänd.

#Kaebas Kadri Haaw Josep Pukki juures ollemissel et Gustaw Welti lehmad on pilla peal olnud. kedda temma lähnud ärra ajama. ja on siis Gustaw Welt tedda wiis korda lönud kakskorda wasto pead ja kolm korda selga nenda et temma mahha kukkunod. ja pallub temma selle ülle kohto moistust sest et temma sellega rahhul ei olle.
Kaebas Kadri Haav Josep Puki juures olemisel et Gustav Welti lehmad on pilla peal olnud. keda tema läinud ära ajama. ja on siis Gustav Velt teda viis korda löönud kaks+korda vastu pead ja kolm korda selga nõnda et tema maha kukkunud. ja palub tema selle üle kohtu+ +mõistmist sest et tema sellega rahul ei ole.

#Gustaw Welt räkis, et Kadri Haaw on tedda wargast sõimanud. ning temma wasto silmi sülge lopinud mis peale temma siis Kadri Haawa ennesest eddemele lükkanud. agga lönud ei olle temma tedda
Gustav Velt rääkis, et Kadri Haav on teda vargaks sõimanud. ning tema vastu silmi sülge loopinud mis+ +peale tema siis Kadri Haava enesest eemale lükanud. aga löönud ei ole tema teda

#Marri Pärn tunnistas et temma on nähnud kui Gustaw Welt on kolmkorda Kadri Haawale kukla lönud nenda et se iggakord otseti kukkunud. ja on siis Kadri Haaw ja Gustaw Welt wastastikku kärratsenud ning sõimanud. kellest temma otsust ei woi anda sest seagga üks kärra olnud. agga sedda temma ei olle nähnud et Kadri Haaw Gustaw Weltile sülge lopinod. 
Mari Pärn tunnistas et tema on näinud kui Gustav Velt on kolm+korda Kadri Haavale kukla löönud nõnda et see iga+kord otseti kukkunud. ja on siis Kadri Haav ja Gustav Velt vastastikku käratsenud ning sõimanud. kellest tema otsust ei või anda sest ¤ üks kära olnud. aga seda tema ei ole näinud et Kadri Haav Gustav Veltile sülge loopinud. 

#Moistus
Mõistus

#Et Marri Pärn tunnistab et temma nähnud et Gustaw Welt on Kadri Haawa järrele joksnod ning sellele kolmkorda kukla löönod nenda et se nennakille kukkunod. sedda agga temma nähnud ei olle. et Kadri Haaw olleks Gustaw Weltile sülge lopinud ehk sedda wargaks sõimanod. siis moistab Koggokonnakohhus et Gustaw Welt peab Kadri Haawale kolme löögi eest 3 Robl wallu rahha maksma ning saab tapluse eest 24 tunni peale trahwist kinni pantud.
Et Mari Pärn tunnistab et tema näinud et Gustav Velt on Kadri Haava järele jooksnud ning sellele kolm+korda kukla löönud nõnda et see ninakile kukkunud. seda aga tema näinud ei ole. et Kadri Haav oleks Gustav Veltile sülge loopinud ehk seda vargaks sõimanud. siis mõistab kogukonnakohus et Gustav Velt peab Kadri Haavale kolme löögi eest 3 rubl valu+ +raha maksma ning saab tapluse eest 24 tunni peale trahviks kinni pandud.

#Se moistus sai selle Liwlandi Tallurahwa sädose põhja peal Aastast 1860 §§. 772 ja 773 nende kohto käijatelle kuulutud, ning nendele need säduse järrel Appellationi õppetusse lehhed wälja antud Gustaw Weltile agga Appellationi keeldod.
See mõistus sai selle ¤ talurahva+ +seaduse põhja peal aastast 1860 §§. 772 ja 773 nende kohtu+ +käijatele kuulutatud, ning nendele need seaduse järel apellatsiooni õpetuse lehed välja antud Gustav Veltile aga apellatsiooni keelud.

#Kadri Haaw tunnistas et temma selle moistosega rahhul ei olle. et kohhus temmale 5 ränga löögi eest keigest agga 3 Robla moistnud temma agga rängaste haiget sanod. ning põddeb ja pallus Appellationi wälja suurema kohto minna
Kadri Haav tunnistas et tema selle mõistusega rahul ei ole. et kohus temale 5 ränga löögi eest kõigest aga 3 rubla mõistnud tema aga rängasti haiget saanud. ning põeb ja palus apellatsiooni välja suurema kohtu minna

#Josep Laurisson XXX
Josep Laurisson XXX

#Andres Kiik XXX
Andres Kiik XXX

#Jaan Mänd XXX
Jaan Mänd XXX

