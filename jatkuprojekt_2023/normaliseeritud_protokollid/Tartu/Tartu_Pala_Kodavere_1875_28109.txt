Pallamõisa Karjamõisa Rentnik Eberhardt Ehrlich kaebas mõisawallitsuse assemel ja volmahtis: Kui mõisawallitsus wanna karjamõisa rentniku Karel Kooli käest koht wastowõtnu, ei olle se Karjamõisa mitte selle kaipuse sees olnu, kuidas kontraht kässeb ja on järelseiswa wiggat leitu:
1. Rukki nurm halwaste külwatu mis eest kahjotassomist nõuab 500 rubla
2. ristikheina külwmata jäetu 12 wakamaad, mis eest 50 rubla
3. Krawit puhastamata 200 rubla
4. aijat parrandamata 50 rubla
5. küini kattus parrandamata 5 rubla
6. Söti kündmata jänu 3 wakkama 200 rubla
7. Wiljade ordnung ei olle mitte piddanu,
si et seal kus kartul piddi minnewa aasta
ollema, linna tehtud on
 ülleültse 1005, Rbl
Tunnistajaks annab kaebaja Kohtumehhed, Josep Rosenberg, Josep Abramson ja Michel Lallo, Karel Tosso, Otto Sallo, Pallalt Jaan Edro, Kadrinalt Karel Hallik Kokkoralt, Jakob Rätsepp Allatskiwwil,Willem Pajo Rannawallast ja Ann Tubbin Pallalt, mis wasto Karel Koolil middagi räkimist es olle, wait neid ka ommale tunnistajateks pallus. Karel Kool andis kaebduse peale wastust:
1. Rukki nurm olla temma kõige parrema seemnega, mis temma wiljasaagis olli külwnu, kõrraperrast ja ka õigel ajal arrinu. 2. Ristikheina olla temma 12 wakkamaad kõrraperrast teinu. 3.Rukki nurmes olla temma egga aasta krawit puhastanu. 4. aijat olla temma keik parrandanu. 5. Küini katttus olla terwe olnu, kui temma ärratulnu. 6. Üks wakkama, mis mitte kõlblik ei olle olnu, on agga kündmata jänu. 7. Enne olnu 10 wakkamaad egga nurm üksi ju andnu heina perrast egga wäljale 2 wakkamaad juurde, mis hilda kätte sanu, ni et keigega walmis ei olle sanu, selle on wilja Ordnung seggi läinu. Kohus mõistis: Kohtokäijat ja tunnistajat teises Kohtopäewas ettekutsu. Need kes wallast wäljas ellawa Keiserl. Kihelkonna Kohto läbbi.
 Kohtomees:Josep Rosenberg
 Kohtomees:Josep Hawakiwi
 Kohtomees:Michel Lallo
 Kirj. allkiri.
