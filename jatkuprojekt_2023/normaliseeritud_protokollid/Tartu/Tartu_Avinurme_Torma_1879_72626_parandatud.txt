#Awwinorme Metskoggokonnakohtus sel 10 Augustil 1879.
Avinurme Metskogukonnakohtus sel 10. augustil 1879.

#koos ollid
koos olid

#peakohtomees Josep Laurisson
peakohtumees Joosep Laurisson

#teine kohtomes Andres Kiik
teine kohtumees Andres Kiik

#kolmas kohtomes Jaan Mänd
kolmas kohtumees Jaan Mänd

#Sai ette kutsutud Karel Andresson ning temmale teada antud et kronometsawallitsus on seie ülles annud et temma kronometsast ostetud tükki sees on tulli lahtipeasenud ning ülle piiri terwemetsa sisse lähnud ja seal 103 puud tullega ärra rikkunud kes krono taksi järrel 43 Rubl ja 8 koppikud maksawad ja et Keiserlik Tartu Maakohhus on sedda asja jo järrel kulanud. ning Karel Andresson ei olle mitte ülles anda woinud kessedda tuld sinna tehnud. siis peab temma issi selle pilla ette wastama ja se takseritud trahw 43 Rubl ja 8 koppikud krono kassase maksma.
Sai ette kutsutud Karel Andresson ning temale teada antud et kroonumetsavalitsus on siia üles andnud et tema kroonumetsast ostetud tüki sees on tuli lahti+pääsenud ning üle piiri terve+metsa sisse läinud ja seal 103 puud tulega ära rikkunud kes kroonu taksi järel 43 rubl ja 8 kopikat maksavad ja et Keiserlik Tartu Maakohus on seda asja ju järele kuulanud. Ning Karel Andresson ei ole mitte üles anda võinud kes+seda tuld sinna teinud. Siis peab tema ise selle ¤ ette vastama ja see takseeritud trahv 43 rubl ja 8 kopikat kroonu+ +kassasse maksma.

#Karel Andresson räkis et se mets on peale 1 Maid kaks näddalad põllema lähnud kui temma vastamisse termin jo mööda olnud; ja on temma sedda põllenud metsa watamas käinud ning need jämmedamad puud keik toored ja kaswawad mõnni peenikenne puu agga ärra kuiwanud; ning ei woi temma sedda küll teada kes sedda tuld sinna tehnud ja ei olle selle pärrast ka Keiserlik Tartu Maakohhus temmale ühtegi trahwi moistnud. ja pallub temma koggokonna kohhut neid põllend puid järrel wadata kedda hopis wähhe on. sest et temma rechendab. et se wallest järrel vadatud ning takseritud on ja ei tahha temma ka selle pärrast sedda trahwi maksa
Karel Andresson rääkis et see mets on peale 1. maid kaks nädalat põlema läinud kui tema vastamise tärmin ju mööda olnud; ja on tema seda põlenud metsa vaatamas käinud ning need jämedamad puud kõik toored ja kasvavad mõni peenikene puu aga ära kuivanud; ning ei või tema seda küll teada kes seda tuld sinna teinud ja ei ole selle+ +pärast ka Keiserlik Tartu Maakohus temale ühtegi trahvi mõistnud ja palub tema kogukonna kohut neid põlenud puid järele vaadata keda hoopis vähe on. sest et tema rehkendab, et see valesti järele vaadatud ning takseeritud on ja ei taha tema ka selle+ +pärast seda trahvi maksta

#Moistus
Mõistus

#Koggokonnakohhus moistab et se assi jubba suurema kohto moistose all on olnud; ning kohhus wist muidogi sedda asja on järrele wadata lasknud. ja ei olle Karel Andressoni nenda kui temma issi ütleb mitte trahwi alla moistnud. ja et se assi temma Summa suuruse järrel Keiserlikku Maakohto agga mitte Koggokonna kohto moistuse all ei seisa, siis ei woi koggokonnakohhus enne sedda metsawallitsusse poolest nõutud rahha 43 Rubl ja 8 koppikud Karel Andressoni käest sisse nõuda enne kui Keiserlik Tartu Maakohhus on seie ette kirjutanud ja sedda trahwi sisse nõuda käskinud, ja woib Karel Andresson Keiserlikku Tartu Maakohhut palluda et Laiuse koggokonnakohhus kelle piiri sees se mets on sedda kahju ülle wataks ja uwwest takseriks L. T. S Aastast 1860 § 884 RA. 
Kogukonnakohus mõistab et see asi juba suurema kohtu mõistmise all on olnud; ning kohus vist muidugi seda asja on järele vaadata lasknud. ja ei ole Karel Andressoni nõnda kui tema ise ütleb mitte trahvi alla mõistnud. ja et see asi tema summa suuruse järel Keiserlikku Maakohtu aga mitte kogukonna+ +kohtu mõistuse all ei seisa, siis ei või kogukonnakohus enne seda metsavalitsuse poolest nõutud raha 43 rubl ja 8 kopikat Karel Andressoni käest sisse nõuda enne kui Keiserlik Tartu Maakohus on siia ette kirjutanud ja seda trahvi sisse nõuda käskinud, ja võib Karel Andresson Keiserlikku Tartu Maakohut paluda et Laiuse kogukonnakohus kelle piiri sees see mets on seda kahju üle vaataks ja uuesti takseeriks L. T. S Aastast 1860 § 884 RA. 

#Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastast 1860. §§ 772 ja 773 kuulutud ning nende kohto käijatelle need säduse järrel Appellationi õppetusse lehhed wälja antud
See mõistus sai selle Liivimaa Talurahva seaduse põhja peal aastast 1860. §§ 772 ja 773 kuulutatud ning nendele kohtu+ +käijatele need seaduse järel appellatsiooni+ +õpetuse lehed välja antud

#Josep Laurisson XXX
Joosep Laurisson XXX

#Andres Kiik XXX
Andres Kiik XXX

#Jaan Pukk XXX
Jaan Pukk XXX

#Selle Protokolli ärrakirri ja Appellation N 276 sai sel 20" Augustil 1879. Metsahärra Genszi kätte sadetud.
Selle protokolli ärakiri ja appellatsioon N 276 sai sel 20. augustil 1879. metsahärra Genszi kätte saadetud.

