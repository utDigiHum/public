Sel 21. märtsil 1880.
 Päkohtomees Ott Illak
 Kohtomees Jaan Allexanderson
 - " - Joh Kedus.
Weskimõisa wallast Johan Reinot kaebap Wana Prangli mehhe Peter Kalbergi päle et ta tema Idta tallo mõtsast 2. sae palki mis 4. 1/3 süld pik ja 1. palk om 19 1/2 tol kannust peddaja palkid ja 12 1/2 tolli laddo jäme ja tõine palk 17 1/2 tolli tüwest, ja 11. tolli laddo jäme, selle man ommawa tunistaja kohtomees Johan Kedus, Weskimõisa wallast ja kohtomees Peter Josep Wana Pranglist, ma nõwa 10. rbl selle kahjo eest.

Wana Pranglist Peter Kalberg üttel, et temma sest middagi ei tija, se om welle assi se ei puttu mulle küssige welle käest, sedda tijap mino welli Johan Kalberg
Wana Pranglist Johan Kalberg üttel, et minna wisi se palki terwelt koddo, ja koddon lõikasi katski ja panni parsile, selle palgi olli Idta Johan Reinot essi tälle lubbanu ja käsk selle 1. rbli eest, mes ta minno welle Jürri Kalbergi käest olli lainanu, (se om nüid soldad), tõist palki ei olle minna winu, et Johan Reinot tälle lubbanu om ei olle keaki kuulnu,

Wana Prangli kohtomees Peter Josep üttel, et se pu halg mees Peter Kalbergi käest kätte saadi, oma tõeste selle 19 1/2 tollilistse kanule täwweste otsa sündinud, ja olli sealt perri agga selle tõise palgi jaost ei olle middagi märki kätte sanud. -
Sedda sama üttel ka Weskimõisa walla kohtomees Johan Keedus, mis Peter Josep om kõnnelnud pude leidmisest. Agga et Johan Reinot issi neid palke kellegile ühhegi asja eest lubbanu olli, es olle sel puude kätte samise kõrral suggugi kuulda.

Kohos mõistis, et Johan Kalberg peap selle awaliggus sanud 19 1/2 tollilitse palgi eest 13. rbl 20. kop 8. päiwani ärrä masma. 1 pukahjo ja trahw kokko, palki hind 4,70 kop, kats kõrd ni paljo trahwi mannu teep 13. rbl 20 kop.
Agga Peter Kalberg peap kohto wasta kroppi ollemise eest 6. rbl waeste hääs masma ehk 30 lögi masma
Peter Kalberg ei olle sellega rahhul, ja pallus prottokoli, 8 päiwani kaebuse edesi kaebuse tärmin sai tälle ka nimmetatu. -
 Päkohtomees Ott Illak XXX
 Kohtom. J. Alexanderson XXX
 -"- Joh Keedus. XXX
