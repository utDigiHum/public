Awwinorme Metskoggokonnakohtus sel. 21 Mail 1882
koos ollid
peakohtomees Maddis Tamm
teine kohtomes Andres Aun
kolmas kohtomes Andres Kallaus
Wallawöölmönder Josep Tomik andis Koggokonnakohtule ülles et Awwinorme Mõisa Wallitsus on temmale teada annud. et üks wõeras innimenne kes omma nimme on ütlenud Kustaw Schulbach ollema ja ennast Awwinorme Mõisa kõrtsis ülles piddanud on sel. 18 Mail õhtu Kõrtsi reia all ühhe noaga ommale rindu torkanud ja pahhema käe soned põigiti läbbi leikanud. mis peale se innimenne teisel pääwal hommiku sel. 19 Mail ärra surnud; ja on temma selle innimesse käest küssinod. et miks pärrast temma ommale üht nenda hirmsad surma tehha tahtnud on se innimenne wastanud, et temma wägga haige ja peawallus olnud ning temma selle wiisi peal omma waewale otsa peale tehha tahtnud.
Jaan Haaw tunnistas et temma on kulnud. et üks innimenne kõrtsi reiaall räkinud kedda temma watama lähnud. ja nähnud et se innimenne reia alluse peal pikkali maas olnud ja werrine mis peale temma siis kõrtsi lähnud ja teisa innimeisa watama kutsunud, ning on siis se innimenne ütlenud et temma ommale pea ja luuwallu pärrast noaga rindo torkanud kolm korda et selle läbbi omma waewale ruttemb otsa peale tehha.
Mõisa Kõrtsmik Aleksander Schobin tunnistas et se innimenne kes omma nimme Kustaw Schulbach on ütlenud ollema, on enne suwwiste pühhasid mõnni pääw Mõisa kõrtsi tulnud ning pallunud tedda pühhatest omma juure jätta ja ütlenud et temma Kurrema wallast pärriti, ja kui temma Aleksander Schobin wiimasel suwwiste pühhal koddust ärra sõitnud ning teisel pääwal sel 19 Mail peale lõunat taggasi tulnud on Gustaw Schulbach jubba surnud olnud, ja ei woi temma sedda arwata et miksparrast se innimenne on ommale otsa tehnud.
Moistus
Et Kustaw Schulbach on issi Wallawöölmönder Josep Tomikulle ning ka teiste innimeste wasto tunnistanud et temma issi ommale kolmkorda noaga rindo torkanud. ning käe soned läbbi leikanud, et selleläbbi ommale surma tehha, siis ei olle selle asja juures ühtegi kahtlaist asja et temma olleks ühhest teisest innimessest tappetud sanud, wait temma issi sedda meele nõdrusses teinud, ja peab se Protokoll Ühhe Keiserlikku Tartu Maakohto kätte ühhe suurema järrele kuulamisseks sisse sadetud saama.
Maddis Tamm XXX
Andres Aun XXX
Andres Kallaus XXX
