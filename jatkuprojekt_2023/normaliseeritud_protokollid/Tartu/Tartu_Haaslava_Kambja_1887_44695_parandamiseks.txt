#Sel 9 Webruaril 1887


#


#Selle otsuse päle sest 22 Aprilist 1885 Johan Klaosen kaibuse asjan wasto Johan Madison ollid mõllemba täna kutsumise päle ette tullnu ja Johan Madison ütles, et tema ¾ jagu pälis massu masnu olli. Magatsi ramatust said need 1882, 1883 ja 1884 asta pälis massu wälja wõetu, selle perra tulli sis wälja et Johan Madisonil weel perra massa tulleb, mis temma wähamb om massnu: 19 Karnist Rüki, 10 Karnist Keswi ja 19 Karnist Kaero.


#Johan Madison, kes Ropkan ellab, andis järrelseiswa kaibuse wasto Johan Klaosen sisse


#1. ei olle Johan Klaosen kontrakti perra maad kätte andno om 84 wakamaad ülle kolme asta pudus jänu nõuab 20 rubl wakkamaa pält.


#2. om Johan Klaosen enne Kontrakti aija lõpmist teda wälja aijanu ja wastu Howi kohtu mõistust ja ka wel 300 rubl selle est ja selle raha 3 asta intressi 6%.


#3. Kohtukäijmisse kullo nõudis 100 rubl.


#4. om tema 7 Wakka rükki wälja küllinu ja nõuab selle est 21 rubl.


#Johan Klaosen, kes Ranitsen ellab, wastutas selle päle, et need nõudmissed keik kohtust läbbi om käino ja et Johan Madison seda juba Kamerin om kaibanu tema ei wõtta neid suguki siin selletuse alla ja et Johan Madison mitte täis rükki nurme ärra ei olle andno selleperrast ei sa tema mitte neid rükki nõuda.


#


#Mõistetu: et Johan Madison need 19 Karnist rükki 10 Karnist Keswi ja 19 Karnist Kaero 2 nädali sehen Johan Klaosenile wälja peab massma, selle et tema Magatsi ramatu perra ¾ jagu pälis massu massnu ei olle ja et Johan Madisoni nõudmisse kaibuse wasto Johan Klaosen seija selletuse alla ei olle wõtnu, selle et Johan Klaosen mitte sijn (ei loe välja) enge ellab Ranitsen.


#Se mõistus sai kulutedo ja õpetus leht wälja antu.


#


#Kohtumees: 


#Jürri Kärn XXX


#Jaan Hansen /allkiri/


#Johan Kliim XXX


#Johan Raswa XXX


