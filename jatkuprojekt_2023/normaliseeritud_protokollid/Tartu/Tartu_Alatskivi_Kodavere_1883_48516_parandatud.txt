#Annab üllesse Allatskiwwi moisawallitsuse nimmel wallitseja Funcke, et Jakob Sõschtschikow, sotnik Iwan Petrow, Mikita Potscherow ja Marja Klütschowa olla 26. Aprillil ühhe hobbuse Kassepäh külla al metsas üllesse leidnuwad, mis Peter Lebedew Kassepealt sinna peitu pannud. Arwata olla, et hobbune olla warrastud.
Annab ülesse Alatskivi mõisavalitsuse nimel valitseja Funcke, et Jakob Sõschtschikov, sotnik Ivan Petrov, Mikita Potscherov ja Marja Klütschova olid 26. aprillil ühe hobuse Kasepää küla all metsas ülesse leidnud, mis Peter Lebedev Kasepäält sinna peitu pannud. Arvata on, et hobune oli varastatud.

#Peeter Lebedew Kassepält kostab, et temma olla selle hobbuse Länitse külla mehhe Andrei kaest omma juure palka weddada wõtnud, Länitse külla olla Ahja mõisa alt. Temma saatnud poea hobbusega metsa palka watama, ja polle temma isse hobbust sinna pannud.
Peeter Lebedev Kasepäät kostab, et tema oli selle hobuse Länitse küla mehe Andrei käest oma juurde palke vedada võtnud, Länitse küla oli Ahja mõisa alt. Tema saatnud poja hobusega metsa palke vaatama, ja pole tema ise hobust sinna pannud.

#Johannes Liiw tunnistab, et Peter Lebedewi poeg pakkunud temmale hobbust, katsuda, kas künnab, ta polle wõtnud, ja siis lähnud pois jälle minnema.
Johannes Liiv tunnistab, et Peter Lebedevi poeg pakkunud temale hobust, katsuda, kas künnab, ta pole võtnud, ja siis läinud poiss jälle minema.

#Wasili Petrow Lebedew ütleb, et temma wõtnud hobbuse koddust, ja lähnud palka watama tahtnud taggasi tulla, agga Kolkja naesed öölnud, et sinno kaest tullasse hobbust ärrawõtma, ta ehmatanud ärra, ja weenud hobbuse metsa.
Vassili Petrov Lebedev ütleb, et tema võtnud hobuse kodust, ja läinud palke vaatama tahtnud tagasi tulla, aga Kolkja naised öelnud, et sinu käest tullakse hobust ära+võtma, ta ehmatanud ära, ja viinud hobuse metsa.

#Kassepäh sotska Iwan Petrow Sidorow ütleb, et Wassili Petrow Lebedew öölnud, et hobbune on 1½ näddalad jubba nende kaes, kättesaadut hobbune olnud metsas talli sees (kuuse oksadest) heinu olnud hobbusel ees. Hobbune on tunkel-kõrw, 4. aastad wanna 5. a peal, ilma märkideta.
Kasepää sotska Ivan Petrov Sidorov ütleb, et Vassili Petrov Lebedev öelnud, et hobune on 1½ nädalat juba nende käes, kätte+saadud hobune olnud metsas talli sees (kuuse+ +oksadest) heinu olnud hobusel ees. Hobune on tunkel-kõrv, 4. aastad vana 5. a peal, ilma märkideta.

#Jakob Sõschtschikow, Mikita Potscherow ja Marja Klütschowa tunnistawad seddasamma.
Jakob Sõschtschikov, Mikita Potscherov ja Marja Klütschova tunnistavad sedasama.

#Selle peale sai Kurrista walla mees Hindrik Hindrikson, Läniste piire weerelt ettekutsutud, kes wälja ütleb, et temma käinud Kassepäh küllas neljapäew peale Lihhawõtte pühhi, ja jätnud Peter Lebedewi kätte näddala peale hobbuse palka weddama. ja piddanud paewas 1. rubla saama. Ta olnud kahhe hobbusega. Hobbune olla ruun, mustjas kõrw, wannadust ei tea, arwab 4. aastasene ta olla selle hobbuse 1. Aprillil 1883 a Tartu turru pealt ostnud ja olla ostmise juures olnud
Selle peale sai Kurista valla mees Hindrik Hindrikson, Läniste piire veerelt ette+kutsutud, kes välja ütleb, et tema käinud Kasepää külas neljapäev peale lihavõtte+ +pühi, ja jätnud Peter Lebedevi kätte nädala peale hobuse palke vedama. ja pidanud päevas 1. rubla saama. Ta olnud kahe hobusega. Hobune oli ruun, mustjas kõrv, vanust ei tea, arvab 4. aastane ta oli selle hobuse 1. aprillil 1883 a Tartu turu pealt ostnud ja oli ostmise juures olnud

#Jaan Rässa, Kurristalt
Jaan Rässa, Kuristalt

#Hans Klaos, -"-
Hans Klaos, -"-

#Jaan Tael, Ahja
Jaan Tael, Ahja

#Peter Armi, Mäksa
Peter Armi, Mäksa

#Hobbusel olla isseärralikult märgid, pahhema puusa peal kramm, seddulga al pahhemal pool 2. weikest muhku., pahhema pihha peal karw ärraõerutud, ninna peal ka kramm., Hind 97. rubla. Rauad olnud 3. haagiga., lak parremal pool.
Hobusel olid iseäralikult märgid, pahema puusa peal kramm, sadula all pahemal pool 2. väikest muhku., pahema piha peal karv ära+hõõrutud, nina peal ka kramm., Hind 97. rubla. Rauad olnud 3. haagiga., lakk paremal pool.

#Peeter Lebedew ütleb , et temma polle hobbust rautada lasknud.
Peeter Lebedev ütleb , et tema pole hobust rautada lasknud.

#Otsus: tullewa kohtupaewaks tunnistajad ettekutsuda
Otsus: tuleva kohtupäevaks tunnistajad ette+kutsuda

#J. Pringweld [allkiri]
J. Pringveld [allkiri]

#J. Kokka [allkiri]
J. Kokka [allkiri]

#Karel Kaddak [allkiri]
Kaarel Kadak [allkiri]

#Karrel Torruwerre [allkiri]
Kaarel Toruvere [allkiri]

