Awwinorme Metskoggokonnakohtus sel 25 Julil 1875.
koos ollid
peakohtumees Josep Kask
teine kohtumees Juhhan Koppel
kolmas kohtumees Maddis Sildnik
Jürri Traks kaebas et Josep ja Mihkel Part on temma aja mahha raijunud. ja kui temma kelama lähnud on Josep Part lubbanud tedda ärra tappa. ning nenda tehha et temma asset järrele ei jäe ning ei lubba temma sellepärrast et Josep ja Mihkel Part ommale sinna uus Maja ülles ehhitawad.
Kohtumees Jaan Mänd kaebas et Jürri Traks on tedda sinna waatama kutsunud. ja on seal Mihkel Part aja mahha raijunud. ning kui temma sedda järrel küssinud. kudda wiisi temma sedda tehha tohhib on Mihkel Part ütlenud ma rajun se aed keik mahha ja ärra tulle sinna seie kissendama. Kassi minnema. ja pallus kohhut et kui igga innimenne temma wastu nenda Krop on siis ei woi temma ennamb kuskil asjasid selletamas käija.
Mihkel ja Josep Part räkisid et nemmad üht uut maja ehhitawad ning et se aed ees olnud on nemmad mahha lasknud, pealegi et se enne nende wanna Maja asse olnud.
Moistus
Et Jürri Traks on issi essiti lubba annud et Josep ja Mihkel Part woiwad ommale sinna maja ülles ehhitada siis woiwad nemmad se Maja ülles ehhitada agga neil ei olle seal rohkemb Krunti kui külla perremehhed ja Jürri Traks temmale lubbawad et agga nemmad seal aeda mahha raijunod. ja tüllitehnud ning pealegi Mihkel Part kohtumehhe Jaan Männi wastu Krop olnud. siis saab Josep Part 2 öe ja pääwa peale trahwist kinni pantud ning Mihkel Partile 15 witsa lööki trahwi moistetud.
Josep Part pallus Protokol wälja suurema kohtu minna mis temmale agga keldud sai agga üks tunnistus selle õppetussega lubbatud et temma selle mõistuse ülle kahheksa pääwa sees Keiserlikku kihhelkonnakohtu juures kaebtust woib tõsta.
Josep Kask XXX
Juhhan Koppel XXX
Maddis Sildnik XXX
Mihkel Part meldis sel 1 Augustil et temma selle moistusega rahhul ei olle ja pallus üks tunistus wälja suurema kohtu minna, sest et temma sellega rahhu ei woi olla et temma Maad ei sa.
Se tunnistus sai sel 1. Augustil N 215 all Mihkel Partile wälja antud.
