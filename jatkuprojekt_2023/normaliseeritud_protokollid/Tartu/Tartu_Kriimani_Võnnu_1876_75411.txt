№Krimanni s 28 Februar 1876.

Kohto lauan olliwa:
Päkohtomees Jürri Braakli XXX
Kohtomees Johan Semen XXX
 " Gotlieb Päiw XXX

 Kadri Niggul tulli omma Eestkostja Jürri Klausega ette ja kaebas et Johan Semmel on minno Kirsto lukko eest ärra murdnud ja säält ärra warrastanud 1 Pallito 9 Rb 50 Cop 1 paar Saapid 2 Rb ja üks wõlla täht suur 6 Rb h. ütte Prilli 50 Cop. nink puhhast rahha 60 Cop. Pallito ollin minna weski renni wahhelt ja Saapad weski tambi koast kätte sanud agga 1 weikene werrew kast kus sissen prillid 60 cop rahha ja ka se wõlla täht olli ei ollen ma mitte kätte sanud.
Hans Wäen ja Jaan Sunter on sedda mulle tunnistanuwad et Johan Semmel ollewad se minno kirst lahkunud.
 Johan Semmel tulli 5mal Märtsil kohto ette ja tunnistas just nenda sammoti kui Mihkel Kokka.
Metsawaht Mihkel Kärik tulli kohto ette ja ütles: et minna ei tean sest asjast middagi üttelda ja minna ei ollen mitte Temma Kirsto lahkunud.
Tunnistaja Hans Wäen tulli ette ja ütles se on õige, et Johan Semmel on Kadri Niggula Kirst lahkunud, se on kui meie Rassina metsa läksime siis wõttis Johan Semmel meie nõudmise perra Laensi külla jurest silla alt üks weikene werrew kast wälja ja ütles: et se on Kadri Niggula kast kus sissen 60 Cop rahha ja muud krami olli, nink ütles: ma ollin weski wassaraga se kirst lahti murdnud õhto ollin se kastikene ja saapad ärra tonud ja hommikust ööd kella 3me aego ollin se Pallito ärra tonud, siis ütlesin minna et Temma se kastikene piddi koddo toma kus sissen weel prillid, 2 nõgla toosi ja mõnni lint olliwad.
Jaan Sunter tulli ette ja ütles just nida samoti kui Hans Wäen tunnistas,
Johan Semmel tulli teist korda ette ja ütles se on õige kül et minna Temma Kirstost ollin wälja tonud need üllemal nimmetud asjad, agga Jaan Tedder olli se Kirst enne lahti murdnud ja minna ei olnud mitte se kirst lahkunud ja ütles, et Temma se werrew kast ollewad metsa kuse oksi alla pandnud, kust nüid ärra ollewad widud ja se 60 Cop rahha ollewad Temma ärra prukinud.
Jaan Tedder tulli ette ja ütles et Temma sest asjast middagi ei teadwad egga ei ollewad ka mitte sedda kirsto lahkunud.

Mõistus: 5mal Märtsil 1876.
Koggokonna kohhus pallub et Juhan Semmeli warguse trahwimine saab Keiserliko Silla kohto mõistmise alla seperrast et Ta lukko on murdnud ja lukko taggast ülle 10ne rubla hõb. warrastanud.

Kohto lauan olliwa
Päkohtomees Jürri Brakli XXX
Kohtomees Johan Semen XXX
Abbi" Jaan Luhha XXX

Kirjutaja: M. Baumann /allkiri/
