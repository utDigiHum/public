#Krimanni kogokonna kohtus 5 Aprilil 1885.


#Een istus


#Peakohtomees Johan Mina


#Kohtomees Johan Kärik


#" Johan Kõra


#" Jürri Lipp.


# Tarto linnast Heindrich Lõhmus, ellamas Gildi Ulitsas № 4 astus ette ja palus et Haslawa kogokonna kohto Protokolli perra 30 Julist 1884 saaks kogokonna kohto polest siin Krimanni mõisa perra Roijo Karjamõisa peal Oblicationi wõlg 1500 Rubla suur mis Liiwima Howi Kohto polest 19 Dec 1883 selle Roijo Karjamõisa peale kinnitud on saaks Roijo Karjamõisa perrisomanik Jürri Klaose käest mulle sisse nõuetud ni kui meie lepping Haslawa Kogokonna kohto een olli kui Haslawa Kogokonna kohhus mino palwe peale sel korral se Oblikationi wõlg Jürri Klaosele ülles kulutas siis lubbas Jürri Klaos selle Oblikationi peale wõetud minno käest wõlg mis 1000 Rublad olli 2 Novbril 1884 mulle ärra maksa aga kui Temma mitte sel terminil se 1000 R. ei olle mulle maksnud siis maksab peale se termini selle Oblikationi suruse perra mulle 1500 Rubla ni kui Haslawa kogokonna kohto Protocoll sest 30 Julist 1884 tunnistab nink palun Kogokonna kohhut et se Roijo Karjamõisa peal Oblikationi peal seisew wõlg 1500 R. saaks Roijo Karjamõisa awwaliko wälja pakkumisel lastud minnole sisse nõuda.


#


#Otsus sel 5 Aprilil 1885.


#Et Roijo karjamõisa perris ommanik Jürri Klaosele saab weel siit kogokonna kohto nimelt tännasel päewal kirjalikult termin antud et Temma antud termin kohtomeeste läbbi weel üks kord ette kulutud, et temma tännasest päewast 14 päewa sissen peab se nimmetud Oblikationi wõlg Haslawa Kogokonna Kohto protokolli perra sest 30 Julist 1884 Heindrich Lõhmusele Tarto sisse maksma, aga kui Jürri Klaus mitte sedda termini ei saab täitma, siis saab suremat kohhut palutud, et se nimmetud wõlg Roijo Karjamõisa wälja pakkumisel saab Lõhmusele sisse wõetud.


#


#Kohto lauan olliwa


#Peakohtomees Joh. Mina XXX


#Kohtomees Johan Kärik XXX


#" Johan Kõra XXX


#" Jürri Lipp XXX


#


#Heindrich Lõhmusel sai säeduselikult otsus 5 April ette kulutud.


#Jürri Klausele sai tännasel päewal kogokonna kohto nimelt se otsuse kirri 5 April 1885 № 78 selle ülle kohto läbbi kätte widud.


#Kog. kirjut. M. Baumann /allkiri/


