Krimanni Kogokonna kohtus 22 Febr. 1880.

 Johan Sünter tulli ette ja kaebas et minna ollin ülle minnewa suwwel Roijo Rentnik Karl Massalil keik suwwi hobbused wahhiks olnud ja olli kaup et saan palka 25 Rubla rahha 1 paar saapid 2 wakka kartoflid mahha panda 10 toopi linna seemnid ja wak rükkid Temma maa peale mahha tehha kohhe minna ka enda sõnniko ollin peale weddanud ja ollin keik kuu palga kätte sanud agga Rukkid ei olnud ma mitte sanud sest Temma läks kewade ärra ja tulli teine Rentnik ja se wõttis rükki ärra nink nõuan nüid selle rükki eest Massali käest 6 semet se on 6 wakka rükkid.
 Radi mõisast Carl Massal sai Keiserliko Kihelkonna kohto läbbi 4 Aprilist seie kohto ette tallitud agga Temma ei olnud tulnud.
Carl Massal sai K. Kihelkonna kohto läbbi 30 Maist seie kohto ette tallitud agga Temma ei olnud tulnud.
Carl Massal sai Keiserliko Kihelkonna kohto läbbi 27 Junil seie kohto ette tallitud agga Temma ei olnud tulnud.
Johan Sunter tulli ette ja pallus et Kogokonna kohhus Temmale üks otsus saaks teggema.

Mõistus: 27 Junil 1880.
Et Karl Massal mitte kolme korra kutsumise peale ei olle seie kohto ette tulnud on kogokonna kohhus mõistnud Massal peab Sünterile 3 wakka Rükkid maksma ehk 9 Rublad rahha mis 14 päewa sissen peab ärra makstud ollema.

Kohto lauan olliwa:
Kohtomees Hans Tekkel XXX
" Peter Krünthal XXX
" Jaan Kurg XXX

Johan Sünterile sai sel sammal päewal se on 27 Junil mõistus ette kulutud.
Johan Sünter olli 27 Junil Protokolli kogokonnast wälja wõtnud et tahta suremat kohhut nõuda.
Karl Massal ei olnud mitte 27 Junil omma kohto otsust seie kuulma tulnud.

Keiserliko 4 Tarto Kihelkonna kohto 
Se allam kogokonna kohhus pallub Keiserliko Kihelkonna kohhut et Radimõisast Carl Massal saaks 17 Octobril seie kog. kohto ette tallitus nink kohto otsust Johan Sünteri wasto kuulma ja kui tullemata jättab siis jäeb Kogokonna kohto otsus kindmas ja ei lubbata ennam ses asjas suremat kohhut nõuda.
Krimanis 13. Septembril 1880 № 220
Kohtomees Hans Tekkel XXX

Kirjut. M. Baumann /allkiri/
