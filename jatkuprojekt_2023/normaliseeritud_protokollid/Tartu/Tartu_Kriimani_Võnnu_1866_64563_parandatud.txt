#Krimanni Koggokonna kohtus 18 Nowbr 1866.
Kriimani Kogokonna+ +kohtus 18 nov 1866.

#


# Mihkel Läett tulli 18 Nowembril koggokonna kohto ette ja kaebas, et pühhapäew ösi 6 Novembril on temma aedast ärra warrastud 1 Reiwas ja 10 hamet ja arwan et Karel Aman ja Jaan Mällo on warrastanuwad sest nemmad on kattekeisi sel õhtul Roijo kõrtsi man salla jutto ajanuwad ning pärrast sealt mõllemad ärra kaddunuwad.
 Mihkel Lätt tuli 18 novembril kogukonna+ +kohtu ette ja kaebas, et pühapäev öösel 6 novembril on tema aidast ära varastatud 1 rõivas ja 10 hamet ja arvan et Kaarel Aman ja Jaan Mällo on varastanud sest nemad on kahekesi sel õhtul Roiu kõrtsi man sala+ +juttu ajanud ning pärast sealt mõlemad ära kadunud.

#Siis sai Jaan Mällo 25 Novembril ette kutsutud ja ta ütles: minna ollin kül sel õhtul Roijo kõrtsi man ja tullin sealt üksinda küllase ning läksin Arraka tallo küini ja heitsin senna maggama ja ütles Karel Aman jäi minnust kõrtsi juurde kui ma sealt ärra tullin ja Mihkel Läetti hamme wargusest ei tean ma middagi üttelda.
Siis sai Jaan Mällo 25 novembril ette kutsutud ja ta ütles: mina olin küll sel õhtul Roiu kõrtsi man ja tulin sealt üksinda külasse ning läksin Araka talu küüni ja heitsin sinna magama ja ütles Kaarel Aman jäi minust kõrtsi juurde kui ma sealt ära tulin ja Mihkel Läti hame vargusest ei tea ma midagi öelda.

#Selsammal päewal sai Karel Aman ette kutsutud ja ta tunnistas et temma sel õhtul polle mitte Roijo kõrtsi manno sanud egga ka Jaan Mällud silmaga sel õhtul näinud ja ütles: et temma on Roijole (koddo) läinud ja weski mant otse kohhe ilma Kõrtsi manno minnemata Rehte maggama läinud ja ütles: et ta mitte Pekste külla kaudo polle koddo läinud.
Selsamal päeval sai Kaarel Aman ette kutsutud ja ta tunnistas et tema sel õhtul pole mitte Roiu kõrtsi manu saanud ega ka Jaan Mällot silmaga sel õhtul näinud ja ütles: et tema on Roiule (kodu) läinud ja veski mant otse+ +kohe ilma kõrtsi manu minemata rehte magama läinud ja ütles: et ta mitte Pekste küla kaudu pole kodu läinud.

#Tunnistajad Jaan Ossep ja Jürri Steinberg said 6 Decembril kutsutud ja nad tunnistasiwad mõllemad üttest suust et Karel Aman olli sel õhtul Roijo kõrtsi man ja ostis Jaan Mällole ½ kortenid wina ja kaddusiwad peale se mõllemad sealt kõrtsi mant ärra.
Tunnistajad Jaan Ossip ja Jüri Steinberg said 6 detsembril kutsutud ja nad tunnistasid mõlemad ühest suust et Kaarel Aman oli sel õhtul Roiu kõrtsi man ja ostis Jaan Mällole ½ kortenit viina ja kadusid peale see mõlemad sealt kõrtsi mant ära.

#Juhan Klaos tulli 12 Decembril ette ja tunnistas: et sel õhtul 6mal Novembril olli Karel Aman ütte teise mehhega Päkste tännawal Mihkel Laetti tarre kohhal agga teisest mehhest ei sanud ma mitte arwo kes ta olli sest ta tõmbas enda pea kassuka sisse agga Karel Amanni tundsin ma selgeste ärra.
Juhan Klaos tuli 12 detsembril ette ja tunnistas: et sel õhtul 6ndal novembril oli Kaarel Aman ühe teise mehega Päkste tänaval Mihkel Läti tare kohal aga teisest mehest ei saanud ma mitte aru kes ta oli sest ta tõmbas enda pea kasuka sisse aga Kaarel Amanni tundsin ma selgesti ära.

#Karel Aman sai ette kutsutud 12 Decembril ja ta salgas ärra et tema ei olle mitte sel õhtul 6mal Novembril Päkste külla tännawale sanud.
Kaarel Aman sai ette kutsutud 12 detsembril ja ta salgas ära et tema ei ole mitte sel õhtul 6ndal novembril Päkste küla tänavale saanud.

#


#Mõistus:
Mõistmine:

#12 Decembril Koggokonna Kohhus mõistis Karel Amani wargaks sepärrast et temma
12 detsembril kogukonna+ +kohus mõistis Kaarel Amani vargaks seepärast et tema

#1) wallet olli tunnistanud et temma mitte sel õhtul polle Roijo kõrtsi man käinud egga Jaan Mällud suggugi näinud,
1) valet oli tunnistanud et tema mitte sel õhtul pole Roiu kõrtsi man käinud ega Jaan Mällot sugugi näinud,

#2) Juhan Klaose tunnistuse peale et temma sel ösil on ütte teise mehhega Mihkel Lätti wärrawas saisnud kui need reiwad warrastud sanud 
2) Juhan Klaose tunnistuse peale et tema sel ööl on ühe teise mehega Mihkel Läti väravas seisnud kui need rõivad varastatud saanud 

#maksab Karel Aman Mihkel Laettile nende reiwaste eest 10 Rubla, 2 näddali aja sissen peab se rahha mastud ollema.
maksab Kaarel Aman Mihkel Lätile nende rõivaste eest 10 rubla, 2 nädala aja sees peab see raha makstud olema.

#


#Man olliwa:
Man olid:

#Peakohtumees Josep Kerge 
Peakohtumees Joosep Kerge 

#Abbikohtumees Peter Püir
Abikohtumees Peeter Püür

#" " Jaan Müirsep
" " Jaan Müürsepp

