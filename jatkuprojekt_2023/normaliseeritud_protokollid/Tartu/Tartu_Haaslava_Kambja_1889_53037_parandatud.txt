#Haslawa Kogukonna Kohus sel 7 Augustil 1889.
Haaslava kogukonna+ +kohus sel 7. augustil 1889.

#Man ollid: päkohtumees Jaan Wirro
Man olid: peakohtumees Jaan Virro

#Kohtumees: Jaan Pruks
Kohtumees: Jaan Pruks

#" Jakob Saks
" Jakob Saks

#


#Selle otsuse päle sest 22 Augustist 1888 Mihkel Tominga asjan wasto Johan Widik tunistas Möldre tüdruk Ann Klaos et tema neist päiwist midagi ei tea, mis Toming pudus om ollnu. Mihkel Toming ollewat õhtult kodust ärra lännu ja öse kodu tullnu ent keaki ei olle nännu kost tema sisse sanu, sest nemma pandnuwa õhtult usse kinni. Widiko ja Tominga wahel ollnu se kaub, et Toming pidanu hobuse tallitama enge tema tallitanu hobuse ja sanu selle est need sabad.
Selle otsuse peale alates 22. augustist 1888 Mihkel Tominga asja vastu Johan Vidik tunnistas möldri tüdruk Ann Klaos et tema neist päevist midagi ei tea, mis Toming puudus on olnud. Mihkel Toming olevat õhtul kodust ära läinud ja öösel koju tulnud ent keegi ei ole näinud kust tema sisse saanud, sest nemad pannud õhtul ukse kinni. Vidiku ja Tominga vahel olnud see kaup, et Toming pidanud hobuse talitama enge tema talitanud hobuse ja saanud selle eest need sabad.

#Karl Peiker tunistas, et tema ollnu üts kõrd 3½ päiwa Tominga est Weskin, kelle est Toming 50 Kop päiwa est massa lubanu, mis weel massmada om. Jürripäiwa aigo jonuwa Widik ja Toming kõrzin liiku ja kõnnelnu selle jures et neide rehnungi wastatside tassa om ja nemma illuste lahkunu.
Karl Peiker tunnistas, et tema olnud üks+ +kord 3½ päeva Tominga eest veskis, kelle eest Toming 50 kop päeva eest maksta lubanud, mis veel maksmata on. Jüripäeva aegu joonud Vidik ja Toming kõrtsis liiku ja kõnelnud selle juures et nende rehnungi vastastsi tasa on ja nemad ilusti lahkunud.

#Märt Oessu tunistas, et tema Mihkel Tomingaga 1 päiw sõitnu, selle est ei olle Widik midagi nõnda lubanu.
Märt Oesu tunnistas, et tema Mihkel Tomingaga 1 päev sõitnud, selle eest ei ole Vidik midagi nõnda lubanud.

#Jaan Pirk Tabrist tunistas, et Märzi kuul ollnu tema Weskil sis lönu Toming raud kangega ni et Weski wõlli otst tük ärra lännu ja kiwi lännu maha.
Jaan Pirk Tabrist tunnistas, et märtsi+ +kuul olnud tema veskil siis löönud Toming raud+ +kangiga nii et veski võlli otsast tükk ära läinud ja kivi läinud maha.

#Johan Preier Jama wallast tunistas et Toming ollnu Makohtu kutsutu sis ollnu tema 2 päiwa assemel, kohes Toming teda kutsunu. Kirwest ei tea tema midagi.
Johan Preier Jaama vallast tunnistas et Toming olnud maakohtu kutsutud siis olnud tema 2 päeva asemel, kuhu Toming teda kutsunud. Kirvest ei tea tema midagi.

#Jaan Toming Tähkwerest tunistas, et tema päiwade wiedmisest midagi ei tea.
Jaan Toming Tähtverest tunnistas, et tema päevade viitmisest midagi ei tea.

#Johan Woika, kes Tarto linnan Aida ulitzen Nr 3 ellab, tunistas nisamma kui Ann Klaos et Toming öselt kül kodust ärra om käinu ja nägemada ösel kodu tullnu.
Johan Voika, kes Tartu linnas Aida uulitsas nr 3 elab, tunnistas niisama kui Ann Klaos et Toming öösel küll kodust ära on käinud ja nägemata öösel koju tulnud.

#


#Mõistetu: et Johan Widik se 10 rubl palga raha Mihkel Tomingale 8 päiwa sehen ärra peab mssma ja et Mihkel Toming 9 wakka rükki ja 1 kotti est 13 rubl 8 päiwa sehen Karl Lestale ärra peab massma ja päle selle peab Mihkel Toming 3 rubl waeste heas 8 päiwa sehen trahwi massma, selle et tema öselt kodust ärra om käinu. Widike ja Tominge tõised nõudmised om tühjas mõista.
Mõistetud: et Johan Vidik see 10 rubl palga+ +raha Mihkel Tomingale 8 päeva sees ära peab maksma ja et Mihkel Toming 9 vaka rukki ja 1 koti eest 13 rubl 8 päeva sees Karl Lestale ära peab maksma ja peale selle peab Mihkel Toming 3 rubl vaeste heaks 8 päeva sees trahvi maksma, sest et tema öösel kodust ära on käinud. Vidiku ja Tomingu teised nõudmised on tühjaks mõista.

#Se mõistus sai § 773 ja 774 perra Johan Widikele kuluedo ja õpetus leht wälja antu.
See mõistus sai § 773 ja 774 perra Johan Vidikule kuludeks ja õpetus+ +leht välja antud.

#Se mõistus sai 21 Augustil s.a § 773 ja 774 perra Mihkel Tomingele kulutedo ja õpetus leht wälja antu.
See mõistus sai 21 augustil s.a § 773 ja 774 perra Mihkel Tomingale kuludeks ja õpetus+ +leht välja antud.

#


#Päkohtumees: Jaan Wirro XXX
Peakohtumees: Jaan Virro XXX

#Kohtumees: Jaan Pruks /allkiri/
Kohtumees: Jaan Pruks /allkiri/

#" Jakob Saks /allkiri/
" Jakob Saks /allkiri/

#


#Ollen kümme rubl wasto wõtnu sel 25 Junil 1890.
Olen kümme rubl vastu võtnud sel 25. juunil 1890.

#Kaarel Lesta asemel Jaan Mõtsar /allkiri/
Kaarel Lesta asemel Jaan Mõtsar /allkiri/

