#sel. 26 Novembril 1876. 


#koos ollid


#peakohtomees Josep Kask


#teine kohtomees Josep Laurisson


#kolmas kohtomees Jaan Mänd


#Astusid ette Jaan Kiik ja temma sõssarad kes mehhel Anno Pomm, Marri Paas ja Ann Pihkwa omma meiste kostmisse all ning kaebasid et nende wõeras emma Leno Kiik on ärra surnud. ning paljo riidid ja kangaid järrele jätnud mis agga selle surnu sõssarad kohhe peale temma surma on issi keskis ärra jagganud. ja et nemmad selle surnule wõerad lapsed olnud ning se wõeras emma keik sedda warrandus nende issa majast sanud, kuhhu juure ka issa omma jau annud siis nõuawad nemmad et sest järrele jänud warrandussest ka nemmad ossa sawad


#Selle ärra surnud wõera emma lihhalik wend ja sõssar Andres Kallaus. Marri Haaw ja wannema sõssara mees Tomas Laurisson räkisid, et Jaan Kiik on omma wõera emma omma juurest ärra ajanud ning se siis kahheksa Aastad Tomas Laurissoni juures ja 2 1/2 Aastad Andres Kallause juures olnud. ning nemmad tedda toitnud ja on selle pärrast se warrandus nendele sanud. toitmisse eest.


#Jaan Kiik räkis et Tomas Laurisson sedda wõeras emmad temma juurest warrandusse pärrast ärra petnud


#Moistus


#Et se wõeras emma majast wälja minnes on omma jau ning ka omma mehhe jau mis se issi temmale annud on kaasa weenud. ning 10 1/2 Aastad wäljas ellanud. ja selle aja sees Tomas Laurissoni ning Andres Kallause käest ülles piddamisse tarwis abbi sanod. siis ei olle nende woera lastel küll ühtegi jaggu temma järrele jänud warrandussest nõuda. et agga Andres Kallaus ja temma sõssarad Anno Laurisson ja Marri Haaw kohhe peale selle kui surnud mattetud olli omma lubbaga sedda warra jaggamist on ette wõtnud, ning ei olnud ärra otanud kui kohhos nende asja selletab, siis maksawad nemmad igga üks, Andres Kallaus. Anna Laurisson ja Marri Haaw 1 Robla walla laeka trahwi omma wolliga warra jautamisse eest. sest et neil tunnistust ei olle et se surnod keik omma warrandust nendele lubband


#Se moistus sai selle Liwlandi Tallurahwa säduse põhja peal Aastal 1860 §§ 772 ja 773 kuulutud ning nende kohto kaijatelle need sääduse järrel Appellationi õppetusse kirjad wälja antud.


#Jaan Kiik ja Anno Pomm. Marri Paas, ning Ann Pihkwa tunnistasid omma meiste juures ollemisse et nemmad selle moistusega rahhul ei olle et nemmad omma wõera emma warrandussest jaggu ei sa. kedda woeras emma olli nende issa majast wälja wenod. ja pallusid Appellationid wälja suurema kohto minna


#Josep Kask XXX


#Josep Laurisson XXX


#Jaan Mänd XXX


#Need lubbatud Appellationid said sel 27 Novembril N.320, 321. 322. ja 323. all. Jaan Kiigele, Anno Pommile Marri Paasile ja Ann Pihkwale walja antud.


#Josep Kask XXX


