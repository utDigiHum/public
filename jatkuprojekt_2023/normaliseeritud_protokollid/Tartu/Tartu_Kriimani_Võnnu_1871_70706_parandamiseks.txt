#Krimani koggokonna kohto 14mal October 1871.


#


# Lies Tull tulli sel 24mal Septembril kohto ette ja kaebas et Karel Ahmann minnole 25 rubla h. wõlga on ja lubbas jo ammogi ärra maksa ja ei maksa mitte kunnagi.


#Karel Ahmann tulli sel 8mal Octobril kohto ette ja maksis 10 rubla h. kohto laua peale Lies Tullile ärra ja ütles: kui ma jälle rahha saan siis maksan jälle sest minna ollin 23 rubla Lies Tullile wõlga agga mitte 28 Rbl mis Protokolli ramato 2. aijasta eest ülles kirjutud sai.


# Lies Tull tulli 8. Ovtobril ka kohto ette ja ütles: Kotta Paeiw olli man kui minna siin kohto wörussen to kord kui ülles kirjutud sai et Ta mulle wõlga olli andsin talle jälle 5. rubla mannu, sest ta pallel jo wäega ja lubbas minno endale naeses wõtta.


# Kotta Paeiw sai sest 14mast Oktobrist kohto ette tallitud ja ta tunnistas: to om õige minna olli wöörussen siis kutsus Lies Tull minno enda mannu ja ütles: Kotta sa näet et ma Karel Ahmanile 5. rubla annan, se rahha andminne olli ma arwan enne kohto ette minnemist ja Lies ei olle wist sedda rahha kohto laua üllese annud, agga ma immetsesin ennast et missuggune luggo rahha ülle nüid ees olli, käiwa kohhud ja annab jälle pealegi.


#Karel Ahmann sai sest 14. Octobrist kohto ette talllitud ja ta ütles: minna olles tedda kül ennda naeses wõtnud, agga et ta kurja tõbbe sisse jäi ei wõinud minna tedda mitte wõtta ja ei wõtta ka mitte, ma ollen temmale 1. paar särtega ja üks paar pooli Sabid linnast ostnud ja annud, kedda ma nüüd taggasi nõuan.


#Lies Tull tulli ette ja ütles: minna ei olle üttegi paari Sabid Karel Ahmanni käest sanud.


#Karel Ahmann ütles: sedda wõib Jürri Ossep tunnistada kes om nännu et ma Liesole 1 paar pooli Sabit ja üks paar wenne Sabad andsin.


#


#Mõistus: Krimani koggokonna kohtus 14mal October 1871.


#Koggokonna kohhus mõistis: et misperrast Lies Tull siis üllese ei annud et Karel Ahmann 28 rubla wõlga on kui kohto üllesse andis, mis ta siis 23 rubla kirjutata laskis. Karel Ahmann peab kui rahha saab 13 rubla Lies Tullile ärra maksma ja siis mõllemilt poolt rahho jäma.


#


#Kohto lauan olliwa:


#Koggokonna kohto peamees: Johan Mina XXX


#" Kohtomees Karel Poedderson XXX


#" Kohtomees Johan Raswa XXX


#


#Koggokonna kohto kirjutaja W. Kogi /allkiri/


