#kaibas Walgeso Körtzmik Ado Ein ülle Walgeso Weski ja Maa Perrisommaniko Kristjan Kiwwi et se ei andnud temmale 2 Wakka alla Maad pruki, ni kui neil Kontrakti perra leppitud; päle too ollewad Ado Ein 1 Wakka Rükki Weskile weenud, ja kah neid mitte kätte saanud, ni kui Ado Einil 1 lehm piddinud Kristjan Kiwwi karjaga hoietud ja södetud saama, ja kah sellega ollewad allati tülli ja pahhandust, selle et Kiwwi Karjus ei tahhawad sedda lehma hoita, pallel nimmitud 2 Wakka alla Maa est Kahjutassumist, se 1 Wakk Rükki Kätte saada, ni kui et lehm saas Kiwwi Karjaga ühhes hoietud.
kaebas Valgesoo kõrtsmik Ado Ein üle Valgesoo Veski ja maa pärisomaniku Kristjan Kivi et see ei andnud temale 2 vaka alla maad pruuki, nii kui neil kontrahi pärast lepitud; peale too olevad Ado Ein 1 vakk rukki veskile vedanud, ja kah neid mitte kätte saanud, nii kui Ado Einil 1 lehm pidanud Kristjan Kivi karjaga hoitud ja söödetud saama, ja kah sellega olevat alati tüli ja pahandust, selle et Kivi karjust ei taha seda lehma hoida, pallel nimetatud 2 vakka alla maa eest kahjutasumist, see 1 vakk rukki kätte saada, nii kui et lehm saaks Kivi karjaga ühes hoitud.

#Kristjan Kiwwi ettekutsutud ja se Asja perrast küssitud wastotas, et temma om Ado Einile Maad pakkunud, ent ei ollewad se sedda mitte wasta wõtnud. Se 1 Wakk Rükki perrast ei teadwad temma middagi; Ado Eini lehma agga lubbas Kiwwi omma Karjusse läbbi hoita laske
Kristjan Kivi ette+kutsutud ja selle asja pärast küsitud vastas, et tema on Ado Einile maad pakkunud, ent ei olevat see seda mitte vastu võtnud. See 1 vakk rukki pärast ei teadvat tema midagi; Ado Eini lehma aga lubas Kivi oma karjuse läbi hoida lasta

#Ado Ein näitas Kontrakt ette, kos seisäp et Kristjan Kiwwi peab temmale 2 Wakka alla Mötsmaad andma ja lehm omma karjaga hoitnu laske; nink selletas Ein, et se 2 Wakka alla Mötsmaa wasta Kristjan Kiwwi temmale paknud 2 Wakka akka Kannustiko, kea weel ei olle Põllus tettud, ja selle perrast ei woinud temma sedda mitte Mötsmaa est wasta wõtta, selle et sedda eddimetsel ajastal ei woi põllas tetta.
Ado Ein näitas kontrahi ette, kus seisab et Kristjan Kivi peab temale 2 vakka alla metsamaad andma ja lehm on karjaga hoidnud lasta; ning seletas Ein, et see 2 vakka alla metsamaa vastu Kristjan Kivi temale pakkunud 2 vakka akka kannustikku, keda veel ei ole põlluks tehtud, ja selle pärast ei võinud tema seda mitte metsamaa eest vastu võtta, selle et seda esimesel ajastul ei või põlluks teha.

#Kohtomees Jakob Hanson teadis selletada, et kui Ado Ein 1 Wakka Rükki Weski Koast ärraKaddunu, om Kristjan Kiwwi sedda kiige holega perra otsinud, kah Kohtomees Abbiks wõtnud, ent ei saanud mitte kätte.
Kohtumees Jakob Hanson teadis seletada, et kui Ado Ein 1 vakka rukki veski+ +kojast ära+kadunud, oma Kristjan Kivi seda kiige hoolega perra otsinud, kah kohtumees abiks võtnud, ent ei saanud mitte kätte.

#Sai moistetud: Kristjan Kiwwi peab Ado Einile puduwa 2 Wakka alla Maa est 30 Rbl maksma, ni kui 1 Wakk Rükki, kea Weski Koast ärrawarrastetud saanud, taggasi andma; kah Ado Eini lehm omma karjaga hoita laskma.
Sai mõistetud: Kristjan Kivi peab Ado Einile puuduva 2 vaka alla maa eest 30 rbl maksma, nii kui 1 vakk rukki, keda veski+ +kojast ära+varastatud saanud, tagasi andma; kah Ado Eini lehm oma karjaga hoida laskma.

#Se Moistminne sai §772 &amp; 773 järrele kulutud.
See mõistmine sai §772 &amp; 773 järele kuulutatud.

#Hindrik Torg XXX Jakob Hanson XXX Hindrik Lipson XXX Peter Kulp XXX Joh. Klaosson XXXHindrik Torg XXX Jakob Hanson XXX Hindrik Lipson XXX Peter Kulp XXX Joh. Klaosson XXX

