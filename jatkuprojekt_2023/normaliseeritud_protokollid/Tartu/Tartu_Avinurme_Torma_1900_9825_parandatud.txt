#Tuli ette Awwinorme Walla ja Kasiku talu peremees Mart Kalaus Mihkli poeg ja palus Wallakohut, et tema tahab oma kahe poja Mihkel Kalause ja Jaan Kalause wahel kes olid ka kohtu ette tulnud pärantuse leppingud teha et kuda wiisi tema pojad tema liikuvad ja liikumatta wara peale tema surma päriwad. sest et tema noorem poeg Jaan nüüd tänawo Aasta Krono tenistuse läheb ja se teadmatta et kas tema sedda aega ära näeb kui tema poeg Kronotenistusest tagasi tuleb. ja on se lepping järgmisel viisil 
Tuli ette Avinurme valla ja Kaasiku talu peremees Mart Kalaus Mihkli poeg ja palus vallakohut, et tema tahab oma kahe poja Mihkel Kalause ja Jaan Kalause vahel kes olid ka kohtu ette tulnud pärandus+ +lepingud teha et kuidas+ +viisi tema pojad tema liikuvad ja liikumata varad peale tema surma pärivad. sest et tema noorem poeg Jaan nüüd tänavu+ +aasta kroonu+ +teenistusse läheb ja see teadmata et kas tema seda aega ära näeb kui tema poeg kroonuteenistusest tagasi tuleb. Ja on see leping järgmisel viisil 

#Oma Kasiku tallu mis Awwinorme Wallas ja Kaewosaare küla talumaade pirites seisab N 192: suur 28 42/100 dessatini suur luban mina oma kahe pojale Mihkel ja Jaanile pooliti pärida ja nimelt nendawiisi et wanem poeg Mihkel need maad ja heinamaad pärib mis selle tee äres mis tema kruntist läbi Wästriku läheb. Wenewere pool küljes ja saab Mihkel weel kaks välja teisel pool teed suo pool küljest. üks põld wastu Josep Kuuse krunti ja teine põld toa eest kust küllest nooremb poeg Jaan. alt serwa küllest pool teist wakka rukki kulwi alla poldu omale saab. 
Oma Kaasiku talu mis Avinurme vallas ja Kaevusaare küla talumaade piirides seisab N 192: suur 28 42/100 dessatini suur luban mina oma kahe pojale Mihkel ja Jaanile pooliti pärida ja nimelt nõndaviisi et vanem poeg Mihkel need maad ja heinamaad pärib mis selle tee ääres mis tema krundist läbi Västriku läheb. Venevere pool küljes ja saab Mihkel veel kaks välja teisel pool teed soo pool küljest. Üks põld vastu Joosep Kuuse krunti ja teine põld toa eest kust küljest noorem poeg Jaan. alt serva küljest pool+ +teist vakka rukki+ +kulvi alla põldu omale saab. 

#Jaan saab omale need väljad mis vastu sood on ning ka se heinam mis wastu sood kunni selle teeni mis Wästriku läheb
Jaan saab omale need väljad mis vastu sood on ning ka see heinamaa mis vastu sood kuni selle teeni mis Västriku läheb

#Karjamaa soab nendele poolest. se jaggu karjamaad mis vastu Andres Sildniku krunti soab Mihklile ja se jaggu karjamaad mis wastu Muroku sood. saab noorema poja Jaanile sest metsa tükkist mis Mihkli krunti sees soawad molemad pojad jaggu pooleti se on mets pooliti ja woivad molemad pojad sealt ahju küttet roijuda maapõhi on agga Mihkli oma. 
Karjamaa saab nendele pooleks. See jagu karjamaad mis vastu Andres Sildniku krunti saab Mihklile ja see jagu karjamaad mis vastu Muraka+ +sood. saab noorema poja Jaanile sest metsa+ +tükist mis Mihkli krundi sees saavad mõlemad pojad jagu pooleti see on mets pooliti ja voivad mõlemad pojad sealt ahju+ +kütet raiuda maapõhi on aga Mihkli oma. 

#Krono renti ja muud teised maksud maksavad nemad pooliti ühesurusel jaul ja teewad teejaud pooleti. 
Kroonu+ +rendi ja muud teised maksud maksavad nemad pooliti ühesuurusel jaol ja teevad teejaod pooleti. 

#Kui nooremb poeg. Jaan hakkab omale maja ehitama se on elu maja siis maksab wanemb poeg Mihkel noorema wenna Jaanile kuuskümmend rubla ehituse raha ja pooled kattuse hõled, nenda kaua kui Jaan ei ole omale weel maja ehitanud, elawad nemad ühes talumajas ja prugiwad honed pooliti.
Kui noorem poeg. Jaan hakkab omale maja ehitama see on elu+ +maja siis maksab vanem poeg Mihkel noorema venna Jaanile kuuskümmend rubla ehitus+ +raha ja pooled katuse+ +õled, nõnda+ +kaua kui Jaan ei ole omale veel maja ehitanud, elavad nemad ühes talumajas ja pruugivad hoonet pooliti.

#2. Keik liikuw warantus mis peale minu surma pärida jaeb, päriwad minu molemad pojad pooliti. tüttred Lena Laurison kes mehel, ja tüttar Anna Puas ei sa minu warantusest enamb ei ühtegi jaggu ei liikuwast egga liikumatta warantusest sest et nemad on mehele minnes omad jaud kätte sanud.
2. Kõik liikuv varandus mis peale minu surma pärida jääb, pärivad minu mõlemad pojad pooliti. Tütred Lena Laurison kes mehel, ja tütar Anna Paas ei saa minu varandusest enam ei ühtegi jagu ei liikuvast ega liikumata varandusest sest et nemad on mehele minnes omad jaod kätte saanud.

#3. Kui mina peaksin enne ärasurema kui Jaan on Krono tenistusest tagasi tulnud siis prugib wanemb poeg se krunt üksi maksab agga norema wennale Aastas kaks kümmend rubla ja maksab ka Krono rent.
3. Kui mina peaksin enne ära+surema kui Jaan on kroonu+ +teenistusest tagasi tulnud siis pruugib vanem poeg see krunt üksi maksab aga noorema vennale aastas kaks+ +kümmend rubla ja maksab ka kroonu rent.

#se pärantuse lepping hakkab agga peale minu surma maksma ja olen mina oma krunti peremees oma surmani.
See pärandus+ +leping hakkab aga peale minu surma maksma ja olen mina oma krundi peremees oma surmani.

#Selle leppinguga olid molemad pojad Mihkel Kalaus ja Jaan Kalaus rahul ja on nemad keik omad nimed oma käega alla kirjutanud.
Selle lepinguga olid mõlemad pojad Mihkel Kalaus ja Jaan Kalaus rahul ja on nemad kõik omad nimed oma käega alla kirjutanud.

#Wiimaks leppisid pärijad selle üle kokku et kui se peaks olema et wanemb wend Mihkel Kallaus need pooled talumaad mis Jaanile parida oma kätte tahab suada kas kohtu teel ehk teise wiisi peal, siis maksab tema oma noorema wenna Jaan Kalausele üheksa sadda rubla kahju tasumist (loe ülewal mis Jaanile pärida lubatud).
Viimaks leppisid pärijad selle üle kokku et kui see peaks olema et vanem vend Mihkel Kalaus need pooled talumaad mis Jaanile pärida oma kätte tahab saada kas kohtu teel ehk teise viisi peal, siis maksab tema oma noorema venna Jaan Kalausele üheksa+ +sada rubla kahju tasumist (loe üleval mis Jaanile pärida lubatud).

#Mart Kallaus ei moista kirjutada. tema palwe peale kirjutas alla Madis Tamm (Allkiri) Tamesarest 
Mart Kalaus ei mõista kirjutada. Tema palve peale kirjutas alla Madis Tamm (Allkiri) Tammesaarest 

#Mihkel Kallaus ei moista kirjutada tema palwe peale kirjutas alla Madis Tamm Josepi poeg (Allkiri)Allekerelt
Mihkel Kalaus ei mõista kirjutada tema palve peale kirjutas alla Madis Tamm Joosepi poeg (Allkiri)Alekerelt

#Jaan Kalaus (Allkiri)
Jaan Kalaus (Allkiri)

#1900 Aastal Nowembris kuu 3 pääwal on eesseisaw pärantuse lepping Awwinorme wallakohtule Awwinorme walla majas Kohtu protokolli üles kerjutamiseks ja kinnitamiseks ette kantud Awwinorme talu peremehe Mart Kallaus Mihkli poja ja tema poegade Mihkel Kalaus Marti poja ja Jaan Kalaus Marti poja poolt Awwinorme wallas olema Kasiku talu N 192. pärimise pärast. ja on need leppingu tegijad walla kohtule tuntud ja teatud ja nemad õiguse woimelised leppingud teha. ja on nende lepping tanasel pääwal kohtu Akti ramatuse N 23 all üleskirjutud. ja kinnitud sanud ning on Mart Kalause eest kirjutada ei moista tema palwe peale Awwinorme inimene Madis Tamm Tammesarest ja Mihkel Kallause eest kes kirjutada ei moista tema palwe peale Awwinorme walla inimene Maddis Tamm Josepi poeg Alekerelt allakirjutanud Jaan Kalaus Mihkli poeg on agga oma nime oma käega allakirjutanud
1900 aastal novembris kuu 3 päeval on eesseisev pärandus+ +leping Avinurme vallakohtule Avinurme valla+ +majas kohtu protokolli üles kirjutamiseks ja kinnitamiseks ette kantud Avinurme talu peremehe Mart Kalaus Mihkli poja ja tema poegade Mihkel Kalaus Mardi poja ja Jaan Kalaus Mardi poja poolt Avinurme vallas olema Kaasiku talu N 192. pärimise pärast. ja on need lepingu tegijad valla+ +kohtule tuntud ja teatud ja nemad õigus+ +võimelised lepingut teha. ja on nende leping tanasel päeval kohtu Akti+ +raamatusse N 23 all üles+kirjutud. ja kinnitatud saanud ning on Mart Kalause eest kirjutada ei mõista tema palve peale Avinurme inimene Madis Tamm Tammesaarest ja Mihkel Kalause eest kes kirjutada ei mõista tema palve peale Avinurme valla inimene Madis Tamm Joosepi poeg Alekerelt alla+kirjutanud Jaan Kalaus Mihkli poeg on aga oma nime oma käega alla+kirjutanud

#Jüri Reisenpuk (Allkiri) 
Jüri Reisenpuk (Allkiri) 

#Mihkel Toming (Allkiri)
Mihkel Toming (Allkiri)

#Jaan Laud (Allkiri)
Jaan Laud (Allkiri)

#Jaan Laud (Allkiri)
Jaan Laud (Allkiri)

