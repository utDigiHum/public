#№ 1
№ 1

#Haslawa Kogukonna Kohus sel 7 Januaril 1891
Haaslava kogukonna+ +kohus sel 7. jaanuaril 1891

#Man ollid: päkohtumees Jaan Wirro
Man olid: peakohtumees Jaan Virro

#Kohtumees Jaan Pruks
Kohtumees Jaan Pruks

#" Jakob Saks
" Jakob Saks

#" Peter Hansen
" Peter Hansen

#


#Kaibas Dawit Muhli Wassulast, et temal 1890/91 asta renti 90 rubl Mihkel Ruusa käest sada om, sealt jääb 29 rubl Mihkel Ruusale tagasi selle et tema Kontrakti perra honit kätte ei olle sanu ni et temal 61 rubl renti sada om ja pallus et kohus Mihkel Rusat sunis seda raha temale ärra massma 1 Oktobril 1890 om lepitu termin ollnu.
Kaebas Davit Muhli Vasulast, et temal 1890./91. aasta renti 90 rubl Mihkel Ruusa käest saada on, sealt jääb 29 rubl Mihkel Ruusale tagasi sellepärast et tema kontrahi perra hooneid kätte ei ole saanud nii et temal 61 rubl renti saada on ja palus et kohus Mihkel Ruusat sunniks seda raha temale ära maksma 1. oktoobril 1890 on lepitud tärmin olnud.

#Tõiselt andis Dawit Muhli ülles, et Mihkel Ruusa temale Tarton ülles om üttelnu ja nõuab et Mihkel Ruusa kohtu een temale ülles üttlep.
Teiselt andis Davit Muhli üles, et Mihkel Ruusa temale Tartus üles on ütelnud ja nõuab et Mihkel Ruusa kohtu ees temale üles ütleb.

#D. Muli /allkiri/
D. Muli /allkiri/

# Mihkel Ruusa wastutas, et tema 90 rubl renti ärra om massnu ja neide leping om ollnu 3 rubl põllu wakkama est ja Muhli lubanu maad ärra mõõta laske mis temal massa om, et temal kontrakti ei olle ja ka honit lepingo perra tema kätte ei olle sanu. Kui maa mõdetu saab ja tema Kontrakti kätte saab sis lubas seda renti massa mis tulleb ja mitte enne.
 Mihkel Ruusa vastas, et tema 90 rubl renti ära on maksnud ja nende leping on olnud 3 rubl põllu vakamaa eest ja Muhli lubanud maad ära mõõta lasta mis temal maksta on, et temal kontrakti ei ole ja ka hooneid lepingu perra tema kätte ei ole saanud. Kui maa mõõdetud saab ja tema kontrakti kätte saab siis lubas seda renti maksta mis tuleb ja mitte enne.

#Mihkel Ruusa /allkiri/
Mihkel Ruusa /allkiri/

#Dawit Muhli wastutas, et tema essite lubanu kül maa ärra mõõta laske, ent perrast lepinu nemma ja arwanu, et umbes 60 wakkamaad põllu maad om, selle est pidanu Mihkel Ruusa 180 rubl maa est renti massa. Se leping ei olle kül eenkontrakti peal, ent seal jures ollnu tunistaja Peter Treial, kes Tarto linnan Allee ulitzen Nr 10 ellab, Eduart Herman kes Küni ulitzen N 25 ellab ja pallus et tunistaja ülle saaks kulatu. Kontrakti pidanu Mihkel Ruusa temma jurest kodust ommale ärra toma, ent Mihkel Ruusa ei olle kontrakti perra tullnu ja nüid konnas renti aeg pea ümbre om ei anna temma enamb kontrakti.
Davit Muhli vastas, et tema esiteks lubanud küll maa ära mõõta lasta, ent pärast leppinud nemad ja arvanud, et umbes 60 vakamaad põllu+ +maad on, selle eest pidanud Mihkel Ruusa 180 rubl maa eest renti maksma. See leping ei ole küll ees+kontrakti peal, ent seal juures olnud tunnistaja Peter Treial, kes Tartu linnas Allee uulitsas nr 10 elab, Eduart Herman kes Küüni uulitsas n 25 elab ja palus et tunnistaja üle saaks kuulatud. Kontrahti pidanud Mihkel Ruusa tema juurest kodust omale ära tooma, ent Mihkel Ruusa ei ole kontrahi perra tulnud ja nüüd konnas rendi aeg pea ümber on ei anna tema enam kontrahti.

#D. Muli /allkiri/
D. Muhli /allkiri/

#Mihkel Ruusa wastutas et tema Muhlile ülles ei olle üttelnu enge tahab seda renti kohta edesi pidada, selle et Muhli temale säduse perra ülles ei olle üttelnu. Kui Muhli temale asta rent wälja mineki est massab, sis tahab tema se koht 23 Aprilil 1891 ärra anda.
Mihkel Ruusa vastas et tema Muhlile üles ei ole ütelnud enge tahab seda renti kohta edasi pidada, selle et Muhli temale seaduse perra üles ei ole ütelnud. Kui Muhli temale aasta rent välja mineku eest maksab, siis tahab tema see koht 23. aprillil 1891 ära anda.

#Mihkel Ruusa /allkiri/
Mihkel Ruusa /allkiri/

#


#Otsus: et tunnistaja Peter Treial ja Eduard Hermann ette om kutsuta.
Otsus: et tunnistaja Peter Treial ja Eduard Hermann ette on kutsuda.

#


#Päkohtumees: Jaan Wirro XXX
Peakohtumees: Jaan Virro XXX

#Kohtumees: Jakob Pruks /allkiri/
Kohtumees: Jakob Pruks /allkiri/

#" Jakob Saks /allkiri/
" Jakob Saks /allkiri/

#" Jaan Pruks /allkiri/
" Jaan Pruks /allkiri/

