#Sel 27 Märzil 1889.


#


#Haslawa Nõukogu poolt wollitu Karl Lesta ja Mihkel Rattasep kaibasiwa päle selle kui nemma omma Wollikirri ette ollid näidanu, et Magatsi wilja mõtmisse jures 1888 astal om 15 Tsetwerti 59 Karnist Kaero pudus tullnu ja 2 Tsetwerti 4 Karnist Rükki ja 2 Tsetwerti 33 Karnist Keswi om ülle jänu, nemma palluwa seda pudust wanna ametimeste massa panda.


# Selle päle ollid tännases päiwas enine Wallawanemb Jaan Soo, wallawölmündri Kusta Adamson, Endrik Zobel ja Johan Grossberg ja Magatsi ülle kaija ja wallawolinik Peter Saal ette kutsutu nemma ollid tullnu ja wastutasiwa, et nemma 29 Julil 1888 Magatsi ja wõtme ärra om andno sis ollnu wilja arw täis ja ei wõi selle puduse est wastutada, mis 9 Augustil 1888 om ollnu, wastse ameti mehe om tõise mõõtmise jures kõwaste wissata lasknu ja selle jures kaero kokko sõkunu sis wõis ka pudus tulla, nemma pandno kui tülli tullnu 2 mõõtu kotti, kus sis täieste mõõt wälja andno kui Kihelkonna Kohtu mehe jures ollnu.


#Karl Lesta ja Mihkel Rattasep wastutasiwa, et wannad ameti mehed wäega kõwasti trikita om lasknu, selleperrast ei olle nemma seda wilja wõtnu ja pallunuwa Kihelkonna Kohtumehed jurde ja sis om se pudus tõusnu. 


#


#Mõistetu: et sest kaera pudusest 15 Tsetwrti 29 Karnist kaero maha wõtta tulleb arwata se willi mis rohkemb om ollnu ja nimelt 2 Tsetwerti 4 Karnist Rüki est 4 Tstw 6 Ka Kaero, 2 Tstw 33 Ka Keswe est 3 Tstw 22 Ka Kaero, summa 79 Tstw 30 Ka kaero. Selle perra jääb puduses 8 Tsetwerti 29 Karnist kaero, keda enine wallawanemb Jaan Soo, wölmündre Kusta Adamson, Endrik Zobel ja Peter Saal ärra peawa massma nink nimelt peab ega üts 2 Tsetwerti 7¼ karnist Kaero massma. Selle wilja ümbre mõtmise jures om 54 päiwa prukitu, ega päiw om 50 kop arwata, seda peawa need nimitedo 4 ammeti meest ka ärra massma ja tulleb egal üttel 675 kop massa. Wallawölmünder Johan Grossberg om sest massust wabas mõista selle et tema enegi 2 kuud ametin om ollnu.


#Se mõistus sai § 773 ja 774 perra kulutedo ja õpetus leht wälja antu.


#


#Päkohtumees: Jaan Wirro XXX


#Kohtumees: Jaan Pruks /allkiri/


#" Jaan Toom XXX


#" Jakob Saks /allkiri/


