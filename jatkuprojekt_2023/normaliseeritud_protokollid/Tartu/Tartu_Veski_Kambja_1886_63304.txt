Sel 6tal junil 1886.
 Man oliwa Päkohtomees, M Karru
 Kohtomees Jakob Kulberg
 abi -" - Jaan Numa
Weskimõisa walla mees Jürri Reinot om oma poja Johan Reinotile antud waranduse selle walla Päkohtumehe Märt Karole, ja kohtumehe Jakob Kulbergile kõnelnu ja üles andnu, mis kohtumehed om kirja pannud Et tema om oma poja Johanile andnu oma maja warranduse, mes karja polest nimelt om sanud, 7. lehma 2 härga 2. wallalist eläjät 2 aasta wanna, 2 was-
kat, 6 lammast, 7. zikka, ja kõik üle-ültne täis maja raudwarra, 2. hobest ja söögi, ja seemne wilja warra, tükki wiisi ei tea ma kõik mitte üles nimetama. Aga ülle-ültselt , oli minu kraam, mis Johanile om sanud 800. rbl wäärt, sest ma nõwwan 200. rbl Johani käest tagasi. Et se kraam Johanile om sanud teap tunistada, Jaan Illak Krüüdnerist, ja Jaan Kister, Wana Pranglin Ott Illak Orikust. - Ja kohtu kirjas om ta wolitanud oma wanatuse ja nõrga jõu perast oma wäimehe Peter Wiruka. 

Johan Reinot, üttel, et tema ei ole oma essa käest midagi sanud, ta om enamb essa päle kullutanud, kui ta om essä käest sanud. - seda teawat, Jaan Irt Johani poig ja Jürri Kulberg, mõlemat Orikult, Peter Kedus Orikult. 
l Tunistaja Jaan Illak Krüidnerist, tulli ette, ja üttel, et sel kõrral kui Jürri Reinot omale pojale Johan Reinotile Ita talu pi-
damise otsuse kätte andse om arwata rohgemb kui 10. aastat, ja sel kõrral oli Jürri Reinotil kõik ülle-ültne maja kraam täwwelik, täl oli 3. hobest 11-12 karja elajat, 6-7. zikka, 6. lamast, ja kõik tarwiliku riista wilja seeme ja söögi willi, täwwelik, ülle-ültselt 800. rbl wäärt. -
2. Tunistaja Peter Kedus üttel, et tema ei tija sest mitte midagi. kui palju warandust Jürri Reinot oma poja Johan Reinotile om andnu. -
3. Tunistaja, Jaan Irt üttel kui mina Idal teenistusen oli 13. aasta tagasi, siis oli Jürri Reinot kohtomees, ja perremees, ka kodun, masse perre palga ja andse töö kässu Johanile ja ka muile Ja sell kõrral oli kõik üle ültne, maja kraam täwelik, kõige karja hobeste ja riistu ja wilja polest. Kui palju Johan Reinotile om sanu sest ei tija mina midagi. -

 Otsus tõises kõrraks tun, pudusse peräst jänu
