#Astus ette koolmeister Hans Hansen ja räkis wälja: 31sel Märtsi k.p. tulnud Jaan Lansmann ja Willem Willik tema elu tuppa ja Jaan Lansmann üttelnud, so, nüüd on se röwel kä, kes minu mütsi käis löhkumas ja mind peksmas; ja lubanud teda siduda ja wahi alla panna. Ta tellind neid wälja minna ja ense üle kaebada, naad pole seda kuulnud, waid käskinud teda wälja minna ja üttelnud selle walla maia ja temal mitte mingisugust öigust neid wälja aiada olewad. J. Lansmann üttelnud ennast kooli maja ema olewad ja koolmeistre kohus olla wälja minna.


#Astus ette J. Lansmann ja tunnistas Liisu Norkroosi räkimise peäle koolmeistre kallale tulnud ja teda süüaluseks üttelnud olewad, kes teda ösel sellesamma L. Norkroosi jures olles lönud, ja tema mütsi löhki kiskunud; ta lubanud teda sellepärast siduda, wahi alla panna ja weel kroonu mundert kandma panna, ja et ta kooli maja ehituse jures abitöline olnud, arwanud ta se läbi enesel koolimajas suurema öiguse olewad kui koolmeistrel.


#Astus ette Willem Willek ja räkis wälja: J. Lansmann aianud seda lömise süüd tema peäle sepärast läinud ta tamaga seltsis koolmeistre tuppa L. Norkroosi juttu peäle, teda süüaluseks tegema; aga ta on enese rumalust koolmeistre käest andeks tellinud ja se on tale andeks annud.


#Astus ette L. Norkroos ja ütles J. Lansmann nimetud ösel enese jures juttu ajamas käinud olewad, aga et ta seäl lödud saanud, pole ta näinud, keski on seal sel korral käinud, mis ta pole tunnu, ja koolmeistre süüks pole ta teadnud J. Lansmanni wastu seda tunnistada.


#Kog. kohus moistis, et J. Lansmann peab selle kölbmatta ö hulkumise, koolmeistre peäle tulemise, tema teutamise ja alwa sanade ette walla laeka heaks 2 rubla höbb. raha trahwi maksma. J. Lansmann maksis wälja.


