#Karel Siimann astus kohtu ette, ja andis ülles: Kui temma Palluse kohha rentnik olli, ja Karl olli 1874 ehhitanud 1 uus Ait, puud olli Paurer herra lubbaga omma kohha heinama pealt weddand, ja 13 jalga se Ait on pitk, ja 10 jalga lai, ja 75 leisikat õlgi on kattuseks peale wäljast ostnud, ja 1 leisika eest 8 kopik maksnud: Se teeb kokko 6 Rubla ja uste laude eest on maksnud Mihkel Reichartil 1 Rubl, ja uste hingede ja tappideks 10 naela rauda: Seppa tööga kokko 1 Rubl, ja 20 töö päwa on selle Aida jures ärra läinud: Kohhus takseris 70 kopik päwas: Se teeb 8 Rubla.


#2) Karl olli ehhitanud 1 heina Küün postide otsa, 14 jalga pitk ja 9 jalga lai, ja 200 wihko roogo ostnud küüne kattuseks, ühhe wihho eest maksnud 1½ kop: Se teeb kokko 3 Rubla, ja 6 töö päwa selle küüne jures. Kohhus arwas 40 kop päwa hind: Se teeb kokko 2 Rubla 40 kop.


#3) Karl on üks Toa uks teinud, laude eest maksnud 50 kop, ja 6 paari uste hinged ja tappid 20 naela rauda on nende sisse läinud: Seppa tööga kokko 2 Rubla, ja 2 Toa hakent raamide eest maksnud 45 kop, ja Glaaside eest 10 kopik ruut 8 ruutu se on 80 kop. Ja kui Karl selle kohha pealt ärra läinud, siis kutsund walla tallitaja abbimehe Jürri Reitli keik sedda ehhitust järrel waatma.


#Ja Jürri Reitel tunnistas kohtu ees, et se keik nenda on, kui Karl on ülles annud: Sedda ehhituse ja Materiali rahha tulleks ühtekoko 25 Rubla 15 kopik.


#* Kohus ei wõi miski otsust mõista. Nõudis suremad kohut.


