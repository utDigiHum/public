#Astus ette Mihkel Kellmet ja kaewab et tema 1870mal aastal Mihkel Brinitse omale toitjaks wõtnud ja Kehhelkonna kohtust ka tunnistuse selle peale sanud; ja selle samma tunnistusega sai Mihkel Brinits Lisust lahti mõistetud, et sa selle wanna innimesse eest piddi murret piddama ja temma toitja ollema. Sedda omma töotust kinnitas tema 1873mal a. ka wollimeeste koggo ees, kus Mihkel Brinits lubbas wanna Mihkel Kellemetti surmani toita.


#Agga kui toitja sai naise wõtnod ja naise wannematte kääst maad sanud, siis kutsus temma wanna mees omma jure Puise ellama, selle peale tulli wanna omma naise ja lomade ja keige kramiga omma toitja jure, et piddid surmani ühhes ellama. Agga et se koos ellaminne kauem ei olnud, kui ühheks aastaks, mullo kewwade tuldud tännawo kewwade mindud jälle omma wanna sauna. Aetud ei ütle temma mitte ennast ollewad waid se ellamine ühhes wägga sant olnud.


#Ja Mihkel Brinitse naine tedda keige ennam ärra tütanud. Muud asjad ütleb wanna keik jälle ärra winud, kui agga need allamal nimmetud söma kramid, weel sada, mis ta sinna tonud ja nüüd ei tahha Mihkel Brinits neid ennam taggasi maksta. Need on: 1 Tünder odrad 2 Tundrid Rukid 1 wak sola 1 koorm heino 3 Tündert kartohwlid 1 küllimet ubbe Ride kramid: 3 weste tehtud 9 künart toimest linnast riet 10 1/2 künart sinnest willast riet.


#Astus ette Mihkel Brinits ja tunnistab sedda keik tõssi ollewad, mis wanna ütleb ennesel olnud; agga ei lubba temmale ühtigid ennam maksa; sest et tema ei olle wanna meest mitte ärra aianud, waid isse selle pärrast läinud, et ta lomad polle ni paljo jahho ja heino sanud süa, kui naad on tahtnud ja on ja wanna est 20 Rbl. rendiks maksnud ja 7 küllimetto odrad ja 4 küllimetto Rukid teised aastad enne, kui ta olles ommas saunos olli (Törmil)


#Koggokonna kohhus ei mõistnud ühtigid waid pallub sedda kehhelkonna kohtus ärra selletada.


#


#Kohto wannem: Mihkel Saro XXX


#Kõrwa mehed Juhhan Jaksmann XXX Andrus Wiltah XXX


#Kirjotaja A J Martenson &lt;allkiri&gt;


#


#Sedda asja on Kehelkona Kohhus ärra selletanud.


