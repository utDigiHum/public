#Koos olid, Tallitaja H. Pisa, abimehed: W. Mölder ja W. Poola kohtupääwa pidamas.
Koos olid, tallitaja H. Pisa, abimehed: V. Mölder ja V. Poola kohtupäeva pidamas.

#Willem Dewid astus ette ja kaebab, et eile õhtu 15 Januar. tulnud Gustav Waldmann joobnud peaga wabriku, pressikambri, kus tema tööd teeb, ja lükkanud pressitöömehe P. Ankru seljali laua peale. Tema läind senna juure ja küsind: Kuulge Waldmann, on see tõsi et mo isa wenna poeg Wilh. Dewid Teid täna tee peal tahtnud püstoliga lasta? Waldmann saanud kurjaks, hakkanud teda häbemata sanadega sõimama, öölnud: Mis sina sest tahad, oled ise ka üks neist kuratitest! Mine Höespää mehe mullikade juure ja maksa Hapsalus oma liiderliku elu wõlg ära, - ja lükkanud ka teda laua peale seljali. Ta lükkanud teda jalaga ennesest eemale. Gus. Wladmann tulnud jälle ta kallale, käristanud tema kuue kolmest kohast katki ja öölnud: "Mina tahan so Tirektor ja Baron olla ja sind leiwast lahti teha.
Villem Devid astus ette ja kaebab, et eile õhtu 15 jaan. tulnud Gustav Valdmann joobnud peaga vabriku, pressikambri, kus tema tööd teeb, ja lükanud pressitöömehe P. Anki selili laua peale. Tema läinud sinna juurde ja küsinud: kuulge Valdmann, on see tõsi et mu isa venna+ +poeg Vilh. Devid Teid täna tee peal tahtnud püstoliga lasta? Valdmann saanud kurjaks, hakanud teda häbemata sõnadega sõimama, öelnud: mis sina sest tahad, oled ise ka üks neist kuraditest! Mine ¤ mehe mullikate juurde ja maksa Haapsalus oma liiderliku elu võlg ära, - ja lükanud ka teda laua peale selili. Ta lükanud teda jalaga enesest eemale. Gus. Valdmann tulnud jälle ta kallale, käristanud tema kuue kolmest kohast katki ja öelnud: "Mina taha su Direktor ja parun olla ja sind leivast lahti teha.

#Gustav Waldmann astus ette ja sai see asi tall ette pandud, rääkis wälja, et see nii just ei ole kui W. Dewid kaeband. Et tema wähe joobnud olnud on see teda pahandanud et Dewid teise Dewiti asja hakkanud küsima. Tema on siis öölnud: "Oled ise ka üks neist" ja käskind Höespää mehe mullikade järel waadata (et W. Dewid neid peab suil olema karjasmaal järelajanud.) aga häbemata sanadega pole tema teda just mitte sõimanud. Tirektori ega Baroni pole tema nimetanud, waid öölnd aga: küll sind saab leiwast lahti tehtud. Kord on ta temase kinni hakkand, siis on kuub katki kärisenud.
Gustav Valdmann astus ette ja sai see asi talle ette pandud, rääkis välja, et see nii just ei ole kui V. Devid kaevanud. Et tema vähe joobnud olnud on see teda pahandanud et Devid teise Devidi asja hakanud küsima. Tema on siis öelnud: "Oled ise ka üks neist" ja käskinud ¤ mehe mullikate järel vaadata (et V. Devid neid peab sul olema karjamaal järele+ajanud.) aga häbemata sõnadega pole tema teda just mitte sõimanud. Direktori ega paruni pole tema nimetanud, vaid öelnud aga: küll sind saab leivast lahti tehtud. Kord on ta temasse kinni hakanud, siis on kuub katki kärisenud.

#Suud suud wasto jääwad waidlema, siiski soowis G. Waldmann lepitust. Naad leppisid wiimaks nenda, et Gustav Waldmann maksis Willem Dewidile kuue eest kahjutasumist ja sõimamise eest 3 rub. hõb.
Suud suud vastu jäävad vaidlema, siiski soovis G. Valdmann lepitust. Nad leppisid viimaks nõnda, et Gustav Valdmann maksis Villem Devidile kuue eest kahjutasu ja sõimamise eest 3 rub. hõb.

#


#Seega sai kohtupääw lõpetud.
Seega sai kohtupäev lõpetatud.

