#Sel pääwal astus Karel Landmann kohto ette ja kaewas et Tiina Ilwes on Meste wasto räkind et temma tahhab temma ello maiad ärra kaodada ja tedda laggeda taewa laska, Ja need peawad siin sedda tunnistama kuida ta on mind häwardand.


#Kohhos kutsus need mehhed ette.


#Mihkel Rehkad astus kohto ette ja ütles et Tina Ilwes olli üttelnd ühhe tükki tahhan minna tehha Pakkama Toa ma põlletan ärra, Ta warrastas mo rahha, kahhe rubla eest on ta mulle kahhjo teinud agga kahhe tuhhande rubla eest peab ta issi kahjo saama, mis on luppatud se saab tehtud.


#Kustaw Eichbaum astus ette ja ütles, et Tina Ilwes olli üttelnd ühhe tükki tahhan minna tehha Pakkama Toa tahhan minna ärra põlledada Ta warrastas mind, kahhe rubla eest on ta mulle kahjo teinud agga kahhe tuhhand rubla eest ta peab issi kahjo saama, ja mis on luppatud se saab tehtud.


#Jurri Landman astus ette ja ütles et Tina Ilwes olli üttelnd ühhe tükki ma tahhan tehha Pakama Toa ma tahhan põlledada Ta on mind warastand kahhe rubla eest on ta mulle kahjo teinod agga kahhe tuhhande rubla eest peab ta issi kahjo sama Ja mis on luppatud se saab tehtud.


#Tina Ilwes astus kohto ette, Kohhos küssis mis olled sinna räkkinud Pakkama kohta nende meste wasto? Tina Ilwes ütles: Ma põlle muud ühtige räkind: Kui ütlessin pahha meelega, ühhe tükki ma tahhan tehha Pakkama Toa ma tahhan ärra põlledada, Ta warrastas mo käest kaks rubla agga kahhe tuhhande rubla eest peab ta isse kahjo saama, Kohhos küssis mis wihha sul on siis Karle peale et sa tedda nenda ähwartanud. Tina ütles Ma ollin pühhawa kirrikus kirriko aedas lapse haua jures, Ma mõtlessin piddin tedda hauast wälja kaewama ja piddin taale ue puhta särgi selga pannema Ja kirriko mees astus minno jure ja ütles mis sinna siin teed? Ja Karel astus kirrikomehhe kõrwa ja ütles ta tahhab tedda üllesse kaewada ja taale sargi selga panna, ja pärrast läksid ülles toa juure õbbetajale rääkima, Ja siis lõi wihha kohhe mo süddames temma peale, Ja ma ollin jubba suist saatik temmaga wihhane sui nende tütrok Tina toi mulle kolm leiba kahhe leiwa eest ma andsin Tinale 80 kop rahha ja ühhe leiba eest ma õmblesin taale ühhe särgi pihha, ja Kaarel wõttis weel peale selle 1 rubla rahha, nende leiwade eest, ja sui wõtsin taale pääwa looga ja ta põlle mulle selle eest ühtige tassond.


#Ja Karel Landman ütles minna põlle sulle mitte sedda pääwa palga keelnud, agga sinna ütlessid minna ei tahha sinno käest selle pääwa eest ühtige makso et sinna olled mind toimetanud, Ja selle rubla ma wõtsin sinno käest nende leiwade inda mis tütrok meilt so kätte toi, ja ma põlle weel middage sind nende leiwade eest trahwind, ja milal minna kaks rubla sinno kaest ollen warrastanud.


#Agga Tina Ilwes ütles ma ei tea muud warrastand ollen ma üttelnd woi mitte ma arwan agga sedda mis ma nüüd ollen nende leiwade eest wälja maksnud.


#Kohhos moistis. Meie ei woi sulle selle irmsa ähwartamisse peale ühtige mõista, kui olleme kuuland et sa sedda omma suga ka olled tunnistand et se tõssi on et so wihha Karli peale kange on ja olled üttelnd et sa Pakkama Toa tahhad ärra põlledada Ja sinno tunnistust mööda on saand protokoli kirjotud ja suurem kohhos peab sedda hirmost ähwartamisse asja sinnoga selletama.


