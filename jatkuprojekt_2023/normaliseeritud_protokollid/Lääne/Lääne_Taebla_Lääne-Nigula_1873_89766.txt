Astus ette Ado Kruus Wonnust ja kaebas, et Taeblast soldati lesk naene Anno Krisp on tedda ennesele tännawo sui päwalisseks pallunud ja päwa palgaks 30 Kop. iggapääw lubbanud peale sedda weel pri söminne; Janipäwast on temma sinna läinud ja nendawisi kunni 2 kuud aega tööd teinud, üks ainus pääw se on Heinamaaria pääw seisnud ja Ann Krisp ei lubba, egga maksa temma palka, mis 47me päwade kohta 14 rubla 10 Kop. wäljateeb, kätte, waid on temmale agga 1 paar pastlide hinnaga kokko 6 rubla 30 kop tassunud, ja ülle sedda muist wolgo jätnud.
Sellepeale sai Anno Krisp ettekutsutud ja räkis wälja et temma kül Ado Krusile 30 Kop päwa palka lubbanud, agga heina aegas üksi, ja ta on temmale selle aia päwa palkad wäljamaksnud, kül et ta üks wanna innimenne liggi 70 aastane, polle igga pääw ja päwad otsa joudnud tööd tehha; 2 näddala peale on ta ennese essiteks töle pallunud, 30 Kop päwa palka lubbanud, kui need näddalad möda olnud, tellind ta tedda nikauaks weel ennese jure jäma, kui temmal tööd on, ja lubbanud selle ette 2 rubla rahha ja 1 wak rukkit, ja sellega on Ado Kruus rahhul olnud.
Kohto küssimisse peale räkis Ado Kruus wälja, et kui need 2 näddalat möda, lubbanud Anno Krisp temmale eddespiddise aia ette, nikaua, kui temmal weel tööd on, rukkide assemel rahha 2 rubla, ja pärris 2se näddala rahha, mis Anno, kes sedda isse ei salganud, wõlgo jätnud olli.
Ado Krusil olli sada

12 päwa ette a 30 Kop ühtekokko
			3 R 60 Kop
		
eddespiddise aia ette 2 rub ja wak rukit, on
			4 R
		
ühtekokko
			7 R 60 Kop
		
Kätte olli ta sanud
			6 R 30 Kop
		
weel sada
			1 R 30 Kop
		
Kog. kohhus moistis, et Anno Krisp Ado Krusile nee 1 R 30 Kop. 2 näddala sees peab wäljamaksma.

Wata Protk. 1875 lissa A pag 49.

Kog. kohtowanem: Mart Pruel XXX
korwasmehhed: Ado Malw XXX Jurri Tomei XXX
Kirjutaja: W. Enock &lt;allkiri&gt;
