#Koos olid, Tallitaja H. Pisa, abimehed W. Mölder ja W. Poola, seatud kohtupääwa pidamas.


#Christine Ecker astus ette ja kaebab, et Neljapääw õhtul olnud tema lehm õues kartufli maa peal ja A. Nurk lehm, P. Nabi lehm ja A. Westerholm lehm olnud ka seal. A. Nurk lapsed hakkanud tema lehma kihutama. Tema saatnud oma lapse ütlema et naad lehma rahule jätwad. A. Nurk ise olnud ukse peal ja oolnud: Kes seda kihutab, ta ei söö jo mitte! Wõtta suu lahti, aja wägise sisse. A. Nurk poeg öölnud Westerholmi pojale: aja Nabi lehm ka seia ja ajame kõik kolm lehma Eckri lehma peale. Tema hüüdnud oma aknast, et naad lehma rahule jättaks. Nürga tüttar Marie hõiskanud seal teda pilgates: Ikka wana on joobnud, wana on joobnud! Tema öölnud talle: Sina tattiwasikas tohid mulle öölda et ma joobnud olen! Kas sina jootsid mind? Nurga poeg öölnud talle: Ole waid, sina kuradi Narwa mära, haista putsi. Õhtul olnud tema oma lauda juures, siis on A. Nurk naene Mari ka tema kohta halwad sanad annud ja tema waesust talle ettewiskanud.


#Marie Nurk räägib, kui tema senna tulnud siis kihutanud Eckri tüttar Pauline nende lehma. Tema öölnud: Mis sa kihutad lehma? Pauline Ecker wastanud: Ta oli mei maa peal. Tema öölnud: Teie oma lehm jo ka siin ikka söömas. Christine Ecker öölnud: Kui minu lehm siin ei ole, ei tohiks ka teised minu kare peal süüa. Tema öölnud: Sa söötsid jo suil juba oma kare ära. Wist oled sa joobnud. Siis hakkanud Christine Ecker teda sõimama: sina tattninawasikas, wabrikust said sa wälja wisatud, nüüd tuled seia koju naesi ristima.


#A. Nurk naene Mari räägib wälja, et kui tema koju tulnud ja lapsed seda asja temale rääkinud, on tema öölnud: Ma pole mitte wasika waid lapse ilmale töönud, seepärast pole ma sellega rahul et ta mo tüttart wasikaks sõimab.


#A. Nurga weiksed pojad ütlewad, nemad pole lehma kihutanud. Westerholmi ja Nabi lehmad on Eckri lehma kihutanud. Pole nemad ka Chr. Eckert mitte sõimanud.


#Gustav Malw tunnistab, et tema kuulnud kui Marie Nurk Chr. Eckrile on öölnud. Sa oled wist joobnud. Chr. Ecker öölnud Marie Nurgale: Director saatis sind wabrikust wälja, nüüd tuled seia naesi sõimama.


#Suud suud wasto jääwad waidlema.


#Otsus: Kõik said noomitud et edespidi rahus peawad elama. Kui weel riidlewad, langewad trahwi alla.


#


#Seega sai kohtupääw lõpetud.


