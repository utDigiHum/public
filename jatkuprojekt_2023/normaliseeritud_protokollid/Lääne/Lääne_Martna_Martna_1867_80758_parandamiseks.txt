#Sure Lähtrust Lesk Marri Kaokülla astus kohto ette ja kaewas omma poia Karli peale, et poeg Karli polle tedda mitte aitnud kui temma aige olnud ja suur leiwa pudus olnud. Selle pärrast ei lubba mitte lesk Marri sedda poolt sauna jaggo, mis temma mees jubba mitme aasta eest Jaan Kitali naese issaga ühte kokko 40 Rubla panko eest ostnud, agga et Jaan Kital on temmale aiguse aial abbiks olnud, leiwa annud, selle pärrast müi mitte omma poia Karlile waid müib omma aitajale Jaan Kitalile


#Lesk Marri poeg Karel sai kohto ette kutsutud, ja temma tunnistab, kui Emma sedda poolt sauna jaggo koggoni temmale ei müi 10 Rubla eest siis tahhab selle ette makso 4 Rubla mis pärrast jure ehhitanud, üks uus sein kus sisse seitse kahhe süllas palki pannud ja üks tük uut kattust teinud.


#Jaan Kital sai kohto ette kutsutud ja temma tunnistas, et temma naese issa jaggo on teine pool sauna, ja et on jubba Lesse Marrile kuus Rubla annud teise pole sauna jao peale, ja lubbab ja kinnitab kohto pegli ees kui temma selle pole sauna jao osta saab, siis temma lasseb selle wanna Lesse Marri Kaokülla surma tunnini seal ellada ja lubbab tedda toita nenda kui omma lihhalikko Emma, et ilmaski wallal sest tülli ei olle.


#Kohhus moistis.


#Et Jaan Kital Lesk Marrit aiguse aial on aitnud ja toitnud, ja et weel lubbab ni kaua toita kui ta ellab saab selle sauna pole jao osta, peab agga Marri poia Karlile wälja maksma, mis sauna parrandanud on, 7 palki 2 Sülda pitkad, igga palk arwatud 20 Cop ja üks tük uut kattust teinud 1 Rubla 40 Koppikud ja tö palka 90 Koppikud Summa 3 Rubla 30 Koppikud


#Marri poeg Karel kaotab selle pärrast omma oigust, et polle mitte Emma eest murret piddanud.


#


#I Pea kohtowannem Karel Nappus XXX


#II Kohto korwamees Jaan Teinberk XXX


#III Kohto korwamehhe Prido Laur XXX


#Prottokolli kirjotaja J Mendell &lt;allkiri&gt;


