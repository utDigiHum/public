#Protokoll üllespodud leitud wabbadmehhe Hans Trikki ülle:


#Hakenrichtri herra kässo järrel 22sest Nowembrist s.a. No 1650, sissetulnud sel 27mal Nowembril s.a. kutsus Taebla walla kog. kohhus Koelakülla abbimehhe Mart Kalm teadaandmist möda ette, need mehhed, kes wabbad meest Hans Trikki, temma omma saunas üllespodud leidnud, need ollid: Trikki perremees Mihkel Korps ja rätsep Willem Semm (se wiimne olli haigusse pärrast tullematta jänud) ja Mart Kalm, kelle wasto need nimmetud mehhed ommad essimeised tunnistusse teinud ollid; ja nemmad räkisid omma tunnistussed wälja, nenda kuida allamal kirjutud seisab.


#1 Mart Kalm tunnistas, et perremees Mihkel Korps ja rätsep Willem Semm on temma wasto tunnistanud, et naad Hans Trikki pärrast sedda, kui naad Trikki perre honed keik läbbiotsinud, Pühhapääw ohto sel 18mel Now. ohto kel 9, temma omma saunas leidnud jämmeda nöri sauna lae külges rippuwad, nenda et parranda ja temma jalge wahhel 1/2 jalga wahhet olnud, ja on nemmad tedda nipea kui leidnud, nöri otsast lahtipeastnud selle lotussega, et piddanud weel hinges ollema, ehk tousma, agga ta olnud ja jänud jubba surnud.


#2. Mihkel Korps tunnistas, et 18mal Nowembril s.a. Pühhapääw ohto kel 9, rätsep Willem Semm wäljast tulles tedda unnest ülles ärratanud; temma tousnud üllesse ja leidnud, et Hans Trik, kes temma jures korteris seisnud, polle toas nähha olnud, ta hakkanud rätsep Willem Semmiga otsima, essiteks ommad honed läbbi, ja kui tedda mitte leidnud, siis keigeliggema Kirriko walla Perdi perre honede ümber, ja kui sealt ka mitte leidnud, siis wimaks temma omma sauna läinud ja tedda sealt leidnud, et ta rippunud sauna lae külges, jämme nöör ümber kaela, 1/2 jalga wahhet parranda ja temma jalge wahhel, peastnud tedda kohhe lahti selle lotusega, et ta olleks weel hinges, ehk touseks ellusse; agga ta olnud ja jänud üsna surnud.


#Kog. kohhus moistis, sedda Protokolli Hakenrichtri herra kohtule ettepanna.


#


#Kohtowannem: Mart Pruel XXX


#Kirjutaja: W. Enock &lt;allkiri&gt;


