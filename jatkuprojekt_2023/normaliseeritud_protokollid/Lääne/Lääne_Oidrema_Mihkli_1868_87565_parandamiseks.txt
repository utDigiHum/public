#1. Tulli Oiderma Wallamees Nömme Mihkel Metsmann ja kaebas, et temma omma poia Otto käest on peksa sanud, ja se olli 29mal Märtsil Minna käisin Panga Körtsis, ja läksin omma poia jure sisse; ja wisin lapsele saia, ja rääkisin selle sauna ostmisse ja rahha pärrast mis minna piddin sama 30 Rubla. Agga nüüd pead sinna mulle 5 rubla rahha jure pannema sellepärrast et sinna ollid sedda wanna asja keik jure arwanud siis pärin minna ka rohkem rahha sest minna ollen ka omma poiale mitto asja teinud ja temma oügus on mind jo aidata, siis wiis poeg mind wälja, ja andis mulle puga sest minno körb ja silm on mollemad wiggased ja haigedja ei kule mitte heaste sest ma ollen ni paljo peksa sanud; sest silm on sinnine, ja Marri Kurgman näggi pealt ehk tunnistusse Mees.


#II Kutsus Kohhus Otto ette ja kussis sa olled issa peaksnud, ja poeg Otto ütles ja kaebas, et issa on Laupääw Moisast Panga Körtsi läinud ja sealt ärra tulles on temma minno naisega riidlema tulnud, ja sest 5est rublast ikka räkinud, ja siis tulli temma jälle Pühhapäwa hommingo minnoga riidlema, ja siis tahtsin minna tedda toast wälja aiada sest temma hakkas jälle sest 5est räkima, ja temma ei läinud mitte, siis wisin tedda wälja ja temma olli minno Kue hõlmas kinni, ja temma ei lasknud mitte lahti, siis loin minna puga temma kae pihta siis hakkas minno jälga kinni egga lasknud mitte lahti egga ma isse ei woinud mitte alla heita mis piddin ma siis haddaga teggema ma wõtsin pu ja löin.


#Siis kutsus kohhus Marri Kurgmanni ette ja Marri ütleb, et issa tulli sinna, ja ütles poeg sa olled mulle rohkem rahha lasknud kirja panna kui sa olled annud, ja Otto ütles kule issa mis sa jälle sie tulled, egga minna sinno saunas ei olle, ja läksid siis kahhekessi kokko, ja läksid uksest wälja ja minna pannin ukse kinni, laps hakkas karjoma ja ma laksin lapse jure siis läksin minna öue ja ollid kahhekessi püsti ja issa silma olli sinnane ja pea katki, ja issa ütles minna olle ärra maksnud keik mis sa olled annud.


#


#Mihkel Metsman ja poeg Otto rio asja pärrast.


