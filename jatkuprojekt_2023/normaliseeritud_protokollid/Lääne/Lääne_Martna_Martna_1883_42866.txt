Kohtusse kaebas pea kohtumees: Jaan Rechkalt, et Ohtla mõisa omanik: Isand Hirsch olla täda ja taa poegi wargaks ja masurikaks sõimanud, sest mõisa aidamees: Priidik Buschmann olla Isand Hircshele seda kaebanud.
Sai ette kutsutud Priidik Buschmann, et taa oma üle seletust annaks ja tunnistas, "et Kustas Silwerwelt olla temale seda rääkinud, et Jaan Rechkalti poeg Kustas ja Willem Nöget olla mulle Tönisepääwa laada aegus Karl Nögeti hobosega mõisa küinest kaks (2) koormad heinu toonud ja K. Nögeti küine wiinud ja pärast säält jälle Hapsallu wiinud. Niisammuti olla Kustas Rechkalt ja W. Nöget ka mõisa rehest kaera warastanud, kus nad rehe ullualt sisse läinud, aga kõigest sest wargusest ei olla tema ise ühtigi märki saanud, et waeaga oleks olnud."
Sellepääle sai ette kutsutud Kustas Silwerwelt ja ta tunnistas, et nemad olnud Jaan Rechkaltiga mullu talwel mõisa rehe juures kortohwleid kühweltamas sääl hakanud naas issekeskes juttu ajama, et mehed tihti tugewaste purjutawad, kus see wälja tuleb? ega palk seda ommetigi sisse ei tee!" Jaan Rechkalt /Kustase wend/ wastanud sellepääle:"Eks naad saa mõisa küinest kaa!" "Minu wend Kustas ja Willem Nöget tõid Tõnispaawa laada aegus mõisa küinest kaks (2) koormad heinu toonud ja siit rehe ullu alt on naad kaa sees käinud ja kaeru warastanud, mis naad minu isale töis ja kolm rubla raha said."
Sai ette kutsutud Jaan Rechkalt ja ta tunnistas wabandusega, et tema ei olla Kustas Silwerweltile ühtigi rääkinud, muud kui taa wend Kustas olla Willem Nögetid Tõnispääwa laada aegus aitanud oma heinu mõisa künest tua."
Sai ette kutsutus Willem Nöget ja ta tunnistas, et taa olla mõisa hobusega Tõnispääwa laada aegus oma heinu mõisa küinest toonud ja Kustas Rechkalt olnud täda seääk juures aitamas, aga kewade wangriga wiinud taa Karl Nögeti hobusega neid heinu, mis temale üle jäänud ühe korma Hapsallu. Niisammuti tunnistas kaa Kustas Rechkalt kohtu ees.

Kohtu mõistus.
Kohus mõistis selle kaeptuse niikauaks tühjaks, kunni selgemad tunnistust selle üle ette tuleb.

Kohtuwanemaasemik: Karl Peiken XXX
Körwamehed: Peeter Kindel XXX
Körwamehed: Mart Reinberg XXX
Kirjutaja: C Allaberg &lt;allkiri&gt;
