#Koos olid: Tallitaja Aleksander Kuusik abbimehed Aleksander Niman ja Madis Laasman seatud kohtupääwa piddamas.


#Kaebas Harjo kaubamees Feodor Ustinow et Kustaw Ekker on tema käest 2 massina nahka ja raud arschina ärrawarastanud mõda teed minnes kui ta tedda on kortli aitanud, ma ollin isse ni jõude et pole suutnud oma kauba sani widada.


#Astus Kustaw Eker ette ja rägis wälja et ma tulin mõda suurt postmaantee ja leidsin trumi kohast 1 wasika naha teise leidsin Seina ja Kusiku maja wahelt, Karel Kerma näggi kui ma maast wõtsin siis wiisin nahad koddo siis läksin jälle wälja Karel Sõlge naene ütles waata mis seal maas on, ma laksin senna watama, seal olli üks harjakas maas lume sees joobunud, kölg oli tal kramidega ümber ta palus mind ütles olle hea aida mind Lülli juure ma maksan sulle 50 kop, siis ma akasin eest widama ta käis tagga läksime Püss Jürri warawase, Harjakas Wasilil ja üks teine ollid seal ja waidlesid issekeskes teised läksid ees ja ma läksin nende järel ja harjakas käis kölgo tagga, kramid läksid tee peal mitto korda ümber siis läksime Lülli jure ta pallus et wiiks kramid tubba neid olli 2 pakki ja 2 kasti need ma wiisin tubba ja küsisin oma waewa ette mis ta lubbas 50 kop, selle peale ta ütles anna mo nahad kätte ta lükkas mind siis ma läksin õue ta lõimas weel wasika naha wargaks, ma ma andsin tallitajale teada et ma need nahad ollen leidnud tee pealt.


#Tunistus Karel Kerma rägis et ta on näinud kui K. Eker on 1 wasika naha maast wõtnud Kusiko ja Seina maja wahhest.


#Tunistus Karel Lehtma rägis et ma tulin kodund läksin Kustaw Ekkerist mõõda, siis ma nägin kui ta ühe naha maast wõttis ja läks kojo poole.


#Andrus Sein rägis et ta on Juhan Endre wärawa jurest maantee pealt leidnud arschina hamingo 1/2 6 kui ta tööle läinud ma andsin Juhan Tülpi kätte.


#Otsus: Ustinow peab Ekerile 2 wasika naha leidmise ette maksma 40 kop Andrus Seinale arschina leidmise ette 15 kop. ja 2 rubla maksab trahwi walla laega selle ette et jobnud peaga omad kramid ärrakautab ja teiste au teotab üteldes et on warastatud agga se polle tõssi sest Eker poleks siis tallitajale ülesannud kui ta warastanud olleks ja nisamoti on ka 2 tunnistust kes näinud kui Eker mast ühe naha wõtnud Eker saab se wargusest priiks mõistetud.


#Ustinow ei olnd kohtootsusega rahul nõuidis suremat kohut, kohus arwas et se assi ei olle sedda wäärt ja jääb kohtu otsus nõnda kuidas ta mõistetud on.


