#Ette astus Söero sauna Gustaw Kasper ja kaebas: Rehhe perremehe Jürri Uesild sead on minno odra pöllo 2 1/2 küllimato maad suutumaks ärra söönud. Seäl sees on ka teiste loomad käinud, agga pärrast sedda, kui Rehhe sead jubba ollid ärra söönud. Ja minno kahjo 2 Tündrid odre.
Ette astus Sõõru sauna Gustaw Kasper ja kaebas: Rehe peremehe Jüri Uuesilla sead on minu odra põllu 2 1/2 külitud maad sootumaks ära söönud. Seal sees on ka teiste loomad käinud, aga pärast seda, kui Rehe sead juba olid ära söönud. Ja minu kahju 2 tündrit otra.

#Ette tulli Rehhe perrenaene Liisa Uesild ja tunnistas: kaks kord on meie wiis sigga seal odra pöllal käinud. Essiminne kord külwi järrele. Teine kord Jaanipääwa Laupääw. Peale sedda ei olle Gustaw Kasper mitte meile sest enne löikust räkinud. Siis olleks wöinud kahjo näha. Ja seäl sees on weel käinud
Ette tuli Rehe perenaine Liisa Uuesild ja tunnistas: kaks kord on meie viis siga seal odra põllul käinud. Esiminne kord külvi järel. Teine kord jaanipäeva laupäev. Peale seda ei olle Gustav Kasper mitte meile sest enne lõikust rääkinud. Siis oleks võinud kahju näha. Ja seal sees on veel käinud

#


#1,
1,

#			
			

#Jurika körtsmiko Mart Wiilmani sead
Juurika kõrtsmiku Mart Viilmani sead

#		
	

#2,
2,

#			
	

#Mardi perremehe Hans Kassem hobbone
Mardi peremehe Hans Kassem hobune

#		
		

#3,
3,

#			
			

#Likko perremehe Willem Jög härg
Likko peremehe Villem Jög härg

#		
		

#Siis tunnistas tallitaja Hans Selg: Minna käisin siis neid odre watamas, kui jubba Gustaw neid maha niitnud, söödud ollid naad kül, nenda, et neist muud ei sa, kui pöhko üksi.
Siis tunnistas tallitaja Hans Selg: Mina käisin siis neid odre vaatamas, kui juba Gustav neid maha niitnud, söödud olid nad küll, nõnda, et neist muud ei saa, kui põhku üksi.

#Siis tullid Willem Jög, Mart Wiilman ja Hans Kassem ette ja tunnistasid, et nende sead, hobbone ja härjad ollid ka seäl pöllal käinud.
Siis tulid Villem Jög, Mart Viilman ja Hans Kassem ette ja tunnistasid, et nende sead, hobune ja härjad olid ka seal põllul käinud.

#Kohto otsus: Et nende üllemal nimmetud meeste sead, hobbone ja härg on pöllal käinud, on jo selkeks tunnistatud. Agga kui suur kahjo on tehtud? ei olle selge, ja sest Gustaw Kaspergil süid, et ta polle enne mahha niitmist kellegile ilmutanud, selle eest möistab kohhos temma kahjo poleks, nenda et Gustaw peab rahhul ollema, et ta üks tünder makstud saab. Ja et Rehe perremehhel ja Mart Wiilmannil sead käinud, mis paljo sepärrast ehk rohkem kahjo on teinud peawad ka rohkem kahjotassuma. Tassuma peawad:
Kohtu+ +otsus: Et nende ülemal nimetud meeste sead, hobune ja härg on põllul käinud, on ju selleks tunnistatud. Aga kui suur kahju on tehtud? ei ole selge, ja sest Gustav Kaspergil süüd, et ta pole enne maha niitmist kellelegi ilmutanud, selle eest mõistab kohus tema kahju pooleks, nõnda et Gustav peab rahul olema, et ta üks tünder makstud saab. Ja et Rehe peremehhel ja Mart Viilmannil sead käinud, mis palju seepärast ehk rohkem kahju on teinud peavad ka rohkem kahjutasuma. Tasuma peavad:

#


#Jürri Uesild
Jüri Uuesild

#			1 wak odre
			1 vakk otri

#			ehk raha 1 R. 50 kop.
			ehk raha 1 r. 50 kop.

#		
		

#Mart Wiilman
Mart Viilman

#			1 wak odre
			1 vakk otri

#			ehk raha 1 R. 50 kop.
			ehk raha 1 r. 50 kop.

#		
		

#Hans Kassem
Hans Kassem

#			1/2 wak odre
			1/2 wak otri

#			ehk raha 75 kop.
			ehk raha 75 kop.

#		
		

#Willem Jög
Villem Jög

#			1/2 odre
			1/2 otri

#			ehk raha 75 kop.
			ehk raha 75 kop.

#		
		

#Koko:
Kokku:

#			3 waka odre
			3 vakka otri

#			ehk raha 4 R. 50 kop.
			ehk raha 4 r. 50 kop.

#		
		

#


#


#Täidetud rahaga, makstud ka.
Täidetud rahaga, makstud ka.

