#Sel 8mal Märtsil olli Putkaste Enniwerre ja Martna koggokonna kohhus koos:
Sel 8ndal märtsil oli Putkaste Ennivere ja Martna kogukonna+ +kohus koos:

#Kohtopeawannem T: Kuu
Kohtu+peavanem T: Kuu

# " kõrwamees J: Täht
 " kõrvamees J: Täht

# " " " J: Selart
 " " " J: Selart

#Astus kohto ette tüdruk Mai Marjapu Putkastest ja kaebas, et Mart Marjapu naene Mina Putkastest on tedda hirmsal kombel sure lapse kätki alluse puga peksnud, ütleb peale 40 hobi tedda lönud olewad, nenda, et temma reis üsna sinnine olla warsi pärrast sedda tülli on temma ( Mai Marjapuu) sedda ka tallitajal ja kohtomehhel näitamas käinud, kus jures temma silmad alles werrised olnud, mis Mina ennese tütriga on lõhki kiskunud. Se tülli on selle läbbi hakkanud, et Mai on Minat keelnud omma wähhikest nõdra last peksmast.
Astus kohtu ette tüdruk Mai Marjapuu Putkastest ja kaebas, et Mart Marjapuu naine Mina Putkastest on teda hirmsal kombel suure lapse+ +kätki aluse puuga peksnud, ütleb peale 40 hoobi teda löönud olevat, nõnda, et tema reis üsna sinine olevat varsti pärast seda tüli on tema ( Mai Marjapu) seda ka talitajal ja kohtumehel näitamas käinud, kus+ +juures tema silmad alles verised olnud, mis Mina enese tütrega on lõhki kiskunud. See tüli on selle läbi hakanud, et Mai on Minat keelanud oma väikest nõtra last peksmast.

#Sedda tunnistawad ka nimmetud Tallitaja Karl Laal ja kohtomees Jaan Selart mõllemad, et se peks ränk, ja Mai Marjapu kaebus tõssi on.
Seda tunnistavad ka nimetatud talitaja Karl Laal ja kohtumees Jaan Selart mõlemad, et see peks ränk, ja Mai Marjapuu kaebus tõsi on.

#Mart Marjapu naene Mina sai ette kutsutud, ja ütles, et Mai Marjapu on tedda enne lönud, mispeale temma ka pu on wõtnud, ja wasto annud.
Mart Marjapuu naine Mina sai ette kutsutud, ja ütles, et Mai Marjapuu on teda enne löönud, mispeale tema ka puu on võtnud, ja vastu andnud.

#Sai ka ette kutsutud Mart Marjapu, ja temma käeat pärritud, miks ta omma naist ei wallitse? Sellae peale ütles Mart wabbanduseks, et ta sel päwal mitte koddo ei olle olnud ja sellepärrast sest üllemal nimmetud tüllist ühtigi ei tea. Ütles agga ka ühtlasi, et se Mai issi ka wägga surelinne on ja nimmelt selleülle, et saun, kus sees mõllemad ühhes koos ellawad, temma nimme peal kirjas seista kihhelkonna kohtus. Sai ka awwalikkuks, et nimmetud Mai tühja kuulmisse ja nõdra mõistussega innimenne on.
Sai ka ette kutsutud Mart Marjapuu, ja tema käest päritud, miks ta oma naist ei valitse? Selle peale ütles Mart vabanduseks, et ta sel päeval mitte kodus ei ole olnud ja sellepärast sest ülemal nimetatud tülist ühtegi ei tea. Ütles aga ka ühtlasi, et see Mai ise ka väga suureline on ja nimelt selle+üle, et saun, kus sees mõlemad ühes+ +koos elavad, tema nime peal kirjas seisab kihhelkonna kohtus. Sai ka avalikuks, et nimetatud Mai tühja kuulmise ja nõdra mõistusega inimene on.

#Kohhus mõistis: Mardi naine Mina Marjapu maksab 1 rubl. hõbbe Maiele selle pekso eest, ja pool rubl.trahwi walla laeka, et ta sedda nõdra mõistusega innimest on peksnud.
Kohus mõistis: Mardi naine Mina Marjapuu maksab 1 rubl. hõbe Maiele selle peksu eest, ja pool rubl.trahvi valla laekasse, et ta seda nõdra mõistusega inimest on peksnud.

#Kohto peawannem: Tawet Kuu XXX
Kohtu peavanem: Taavet Kuu XXX

# " kõrwmees: Jürri Täht XXX
 " kõrvamees: Jürri Täht XXX

# " " Jaan Selart XXX
 " " Jaan Selart XXX

