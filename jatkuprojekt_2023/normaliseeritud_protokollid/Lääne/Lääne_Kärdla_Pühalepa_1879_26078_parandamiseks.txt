#Koos olid Tallitaja. P. Naaber, abimehed: S. Läck ja W. Kutser seatud kohtupääwa pidamas.


#Johannes Amber (Toidukraami poodi kaupmehe N. Mähle sulane - kes temal lihuniku ammetis oli - kaebab, et tema peremees N. Mähle temale mitte täit palka pole wälja maksnud, kui ta tema juurest ammetist ära saanud.


#Nende kaup olnud nõnda: Kui see pood weel tema majas üüis olnud, saanud tema 20 rub. kuus palka ja tema olnud nende sulane lihuniku ammetis. Kui naad aga Septembri kuu hakkatusel tema juurest ära, poodi oma majase kolinud, olnud seega ka tema teenistus otsas. Et neil aga muud lihuniku polnud, lasknud naad teda omale pärast seda aega weel 2 härga tappa. Kui ta selle teise härja juba lahtinud olnud, tulnud kaupmees N. Mähle oma wallitsuse-nõuga kokku, mis palka tema selle eest nõuab, mis ta pärast oma teenistuse aega neile lahtinud?


#Tema nõudnud 5 rub. hõb. härja pealt lahtimise eest. Seda on naad paljuks arwanud, aga lubanud siiski temale seda maksta.


#Tema küsinud: Kas naad teda eespidi weel tarwitawad? Selle peale on wastus antud, et naad teda weel saawad tarwitama. Tema ootab käsku, aga 14 wõi 15 Sep. kuuleb, et härg juba lahitud ja pole teda enam tarwitud. Pärast seda aega saadab kaupmees N. Mähle tema aga ühe härja lahtimise palga 5 rub., kus ta kahe härja pealt 10 rub. oleks pidanud saatma.


#Nüüd mõistnud tema, et teda ses ammetis enam ei saa tarwitud. Ütleb et ta seeläbi suurt kahju saanud; sest muidu oleks tema warsi majale wõinud minna teenistust otsima, aga et naad temale weel lubanud teenistust anda, on ta nende sana peale ootma jäänud.


#Kaupmees N. Mähle astus ette ja sai see asi tall ettepandud, rääkis tema wälja: Et Joh. Amber pärast oma teenistuse aega mitte 2 waid aga ühe härja on tapnud. 6 Sep on tema kuu täis saanud ja 5 Sep. on ta selle esimese härja lahtinud, kus ta jo weel kuu wiisi palka sai. Üksi selle eest mis ta 10 Sep. lahtinud olnud tall õigus 5 rub. saada ja seda on tema wäljamaksnud. N. Mähle ütleb, et tema teda seepärast enam ei lasknud lahtida, et ta wäga suurt palka nõudnud.


#Kohus tegi otsust: Et Joh. Amber selle esimese härja lahtimise pealt see 5 rub. mitte ei wõi nõuda ega saada, et tema jo siis weel kuu wiisi teenistuses oli; aga et kaupmees N. Mähle temale weel teenistust lubas anda ja pärast ometi ei annud, ega temale selge sanaga öölnud, et ta tema teenistusest lahti on ja Joh. Amber ilmaasjata pidi ootama ja ajakahju saama, mõistis kohus: et Kaupmees N. Mähle Joh. Ambrile kahjutasumiseks peab poole kuu palka maksma tema endise palga järel, se on: 10 rub. hõb; kellest tema 5 rub. hõb. juba maksnud on, aga 5 rub. hõb. weel juure peab maksma.


#Et muud toimetust polnud, sai kohtupääw lõppetud.


