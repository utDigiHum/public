#Kohto ette tulli Liigwallamees Hindrik Kruusbärg, ja kaebas: et temma olnud Einmanni Wiina Wabrikus tööl, ja Masgi pump olli lahti läinud, ja Wiina meister Hindrik Märsin, käsgis mind sedda Pumpa toppida, ja kui ma selle Pumba toppitud sain, ja unnustasin selle külma Raua peitli senna mahha, kellega pumpa toppisin, ja kui Meister sedda Peitlit sealt leidis, siis küssis kuhhu sa selle Petli pannid, ja minna ütlesin wist unnustasin senna Pumba jure mahha, siis käsgis Meister sedda petlid maast ärra wõtta, ja kui ma hakkasin Peitlit maast wõtma, wõttis Meister minno Karwost kinni, ja lõi kaks kord wasdo kõrwu, ja sedda teab ka tunnistada selle Wabriku töömees Jaak Kirs.


#Hindrik Märsin tunnistas nenda: et temma on Hindrik Kruusbärgi, Karwustanud, ja ka üks kord russigaga Kuklase annud; agga nimmelt selle pärrast: kui ma Peitli leitsin, ja küssin temma kääüst, kuhhu Sa Peitli pannid, kui Pump toppitud sai, siis kosdab Kruusbärg kappi; siis wõtsin Karwust kinni, ja wisin tedda senna kus Peitel mudda sees maas olli, ja selle peale ütles H. Kruusbärg mule: mis sa siis selle pärrast mulle woid tehha, siis lõin ka takka kuklse. Sest se olli mul töö meestele ette kulutud, et kolle järrelt ma töö riista maast leian, se trahwitud saab, sago mis modi saab, sest nemad kautasid palju töö riistu ärra, ja kui tarwis kesgi ei tea. Ja hind. Kruusbärg kautas üks külma Raua Peitel ärra päriselt, maksab: 50 kop. ja Jaan Kirsiga, kahekesdi ühhe töö jures, kautasid Rihma labbi lömisse Raua ka maksab 50 kop, sedda ma pärin temma käest.


#Tunnismees Jaak Kirs tunnistas, et temma polle sedda lömist näinud, muud kui läinud sealt mööd, kui Meister H. Kruusbagiga tappelnud. Jaak Kirs Ja noudis omma ilma aegist pääwa wiit misse palka, sest temmal olnud palgatud mees assemel.


#Koggukonna Kohhos moistis nenda: et Hindrikus Märsin maksab 1 Rb 50 kop Hindrik Kruusbärgile, lömisse ette; ja Hind. Kruusbärg maksab ärra kautud töö riistade ette: 75 kop. Ja Jaak Kirsi pääwa, 50 kop. pääwa palgaks.


#Ja Hindrik Märsin maksab: Trahwi walla Laeka 50kop.


#Koggukonna Kohto nimel: M. Holland xxx H. Mein xxx Juhan Anni xxx


