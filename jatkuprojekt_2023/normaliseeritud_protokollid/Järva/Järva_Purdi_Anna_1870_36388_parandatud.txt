#Sel 13mal Nowembril 1870 tullid Tallittaja Mart Tougo Abbimees Jürri Rebbas, Kohto korwamees Mart Wenno Kohto toa jure kokko Maddis Kasse surma pärrast ja lasksid sedda nimmel nenda prottokoli panna.
Sel 13ndal novembril 1870 tulid tallitaja Mart Tougu abimees Jüri Rebas, kohtu kõrvalmees Mart Venno kohtu+ +toa juurde kokku Madis Kase surma pärast ja lasksid seda nimelt nõnda protokolli panna. 

#Jaan Wenno tunnistas et minno öemees Maddis Kask läks sel 12mal Nowembril 1870 päewa loja aial Purdi moisa, omma hobbose ja wankrega, olli terwe, Moisast on temma omma wenna jurest kel 1/2 8 ärra tulnud ja sealgi ei olle temma keddagi haigust egga wigga kaewanud. Öhtu arwata kel 10 on temma naene tedda öue wärrawa tagga surnud leidnud ja kohhe tulli rägis minnule, siis minna wisin tedda naesega tuppa ja öerusime seal, siis läksin kohhe öse ja rääkisnin sedda kohtumehhele, 
Jaan Venno tunnistas et minu õemees Madis Kask läks sel 12ndal novembril 1870 päeva looja ajal Purdi mõisa, oma hobuse ja vankriga, oli terve, mõisast on tema oma venna juurest kell 1/2 8 ära tulnud ja sealgi ei ole tema kedagi haigust ega viga kaevanud. Õhtu arvatavasti kell 10 on tema naine teda õue+ +värava taga surnud leidnud ja kohe tuli rääkis minule, siis mina viisin teda naisega tuppa ja öerusime seal, siis läksin kohe öösel ja rääkisin seda kohtumehele, 

#Tallittaja Mart Tougo ja Abbimees Jürri Rebbas ja kohto körwamees Mart Wenno tunnistasid, kui meie seda surnud läbbi watasime, ei leidnud meie keddagi wöerast märgi sealt jurest temma naene Kai tunnistas meile 5 koppik rahha, kintad ja pibo wöttis kotto minnes jure ja need keik ollid ta jures alles silma alt olli sinnine mis tema wist wasto wankred on kukkonud rited ollid puhtad. muud kui müts olli kattund, se müts on 1 werst Kabbeli körtsist koio pole tulles maas olnud teisel hommikul sel 13 ndal Nowembril on haua teggiad Mart Kaggowerre ja Jürri Lauri sedda leidnud maas ja sealgi jures kus müts maas olli ei olnud mingisuggust märgi leida.
Tallitaja Mart Tougu ja abimees Jüri Rebas ja kohtu+ +kõrvalmees Mart Venno tunnistasid, kui meie seda surnud läbi vaatasime, ei leidnud meie kedagi võõrast märki sealt juurest tema naine Kai tunnistas meiele 5 kopikat raha, kindad ja piibo võttis koju minnes juurde ja need kõik olid ta juures alles silma alt oli sinine mis tema vist vastu vankrit oli kukkunud riided olid puhtad. Muud kui müts oli kadunud, see müts on 1 verst Kabeli kõrtsist koju poole tulles maas olnud teisel hommikul sel 13 ndal novembril on haua+ +tegijad Mart Kaguvere ja Jüri Lauri seda leidnud maas ja sealgi juures kus müts maas oli ei olnud mingisugust märki leida.

#Nüüd olleme meie need jälled ja surnu läbbi watand ja need tunnistussed järrel küssinud. Ja pallume Aulikkud Haggendrehhi kohto Herrad kuidas temmaga saab, meie ei tohtinud tedda nenda matta et se surm ni lühhikesse aiaga on olnud. Moisast temma sana jure on 5 wersta maad kus temma on haigeks jäenud ja ärra surnud.
Nüüd oleme meie need jäljed ja surnu läbi vaadanud ja need tunnistused järele küsinud. Ja palume aulikud Hagendrehi kohtu härrad kuidas temgaga saab, meie ei tohtinud teda nõnda matta et see surm nii lühikese ajaga on olnud. Mõisast tema sõna juurde on 5 versta maad kus tema on haigeks jäänud ja ära surnud. 

#Tallittaja Mart Tougo XXX
Tallitaja Mart Tougu XXX

#Abbimees Mart Wenno XXX
Abimees Mart Venno XXX

#Körwamees Mart Wenno XXX 
Kõrvalmees Mart Venno XXX

#Kirjotaja M.Arrak
Kirjutaja M.Arrak

