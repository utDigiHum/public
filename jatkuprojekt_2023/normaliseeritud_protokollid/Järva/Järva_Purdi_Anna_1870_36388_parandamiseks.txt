#Sel 13mal Nowembril 1870 tullid Tallittaja Mart Tougo Abbimees Jürri Rebbas, Kohto korwamees Mart Wenno Kohto toa jure kokko Maddis Kasse surma pärrast ja lasksid sedda nimmel nenda prottokoli panna.


#Jaan Wenno tunnistas et minno öemees Maddis Kask läks sel 12mal Nowembril 1870 päewa loja aial Purdi moisa, omma hobbose ja wankrega, olli terwe, Moisast on temma omma wenna jurest kel 1/2 8 ärra tulnud ja sealgi ei olle temma keddagi haigust egga wigga kaewanud. Öhtu arwata kel 10 on temma naene tedda öue wärrawa tagga surnud leidnud ja kohhe tulli rägis minnule, siis minna wisin tedda naesega tuppa ja öerusime seal, siis läksin kohhe öse ja rääkisnin sedda kohtumehhele, 


#Tallittaja Mart Tougo ja Abbimees Jürri Rebbas ja kohto körwamees Mart Wenno tunnistasid, kui meie seda surnud läbbi watasime, ei leidnud meie keddagi wöerast märgi sealt jurest temma naene Kai tunnistas meile 5 koppik rahha, kintad ja pibo wöttis kotto minnes jure ja need keik ollid ta jures alles silma alt olli sinnine mis tema wist wasto wankred on kukkonud rited ollid puhtad. muud kui müts olli kattund, se müts on 1 werst Kabbeli körtsist koio pole tulles maas olnud teisel hommikul sel 13 mal Nowembril on haua teggiad Mart Kaggowerre ja Jürri Lauri sedda leidnud maas ja sealgi jures kus müts maas olli ei olnud mingisuggust märgi leida.


#Nüüd olleme meie need jälled ja surnu läbbi watand ja need tunnistussed järrel küssinud. Ja pallume Aulikkud Haggendrehhi kohto Herrad kuidas temmaga saab, meie ei tohtinud tedda nenda matta et se surm ni lühhikesse aiaga on olnud. Moisast temma sana jure on 5 wersta maad kus temma on haigeks jäenud ja ärra surnud.


#Tallittaja Mart Tougo XXX


#Abbimees Mart Wenno XXX


#Körwamees Mart Wenno XXX 


#Kirjotaja M.Arrak


