#Kohto ette tulli: Mart Parstein, ja Kaebas: et Juhhan Koppas, on tulnud kaewus wet tooma, mis temma krundi sees olnud, ja se kaew on lukkus olnud, ja wötti Mart Pärsteini käes olnud, Moisa kässo peale; siis on Mart Pärstein öölnud et kui Mois, on jubba omma jäggo wet ärra wiinud, siis saad, ja selle peale on Juhhan Koppas, küssinud: kas sa siis ei annagi wet, ei anna; siis on Juhhan Koppas öölnud; et kui sa wet ei anna siis teine pääw pead sa siit ärra minnemas ollema; ja teine pääw peale sedda, on Juhhan Koppase poead, läinud jälle senna Kaewule, ja Mart Parstein, on läinud teole moisa, ja tee peal sanud kokko, ja Mart Pärstein on öölnud, nende postele: et teie ei pea mitte nisugguse sea püttiga kaevust minnema uvet tooma, ja wöt selle ämbri, poeste käest, ja pannud mahha, siis on poisid tedda hakkanud söimama ja siggu luggema; ja peale sedda kui jubba Mart Pärstein, on moisa töös olnud, on Juhhan Koppas temma jure tulnud, ja hakkanud temma käest küssima: kas Sinnu Naene, on minnu Laste peksja, siis on Mart Pärstein öölnud: minna ei tea, sest minna ollen jo Moisa töös, agga sina ei pea sea püttiga kaevu eest wet wiima, siis on Juhhan Koppas, tedda lönud kerwa põhjaga, vasto puusa ja peale sedda vötnud: karvust kinni, ja tömmanud pikkali p mahha, ja röhhunud põlvedega rinde peale; ja Mart Pärsteini poeg on ka senna tulnud selle Rio poale, ja hakkanud abbi karjuma, siis vötnud poisi karvust ka kinni, ja kisgunud poisi.


#Juhhan Koppas tunnistas: et temma on läinud küssima, Mart Pärsteini käest, et kudda temma ja temma Naene on thtind minu Lapsi peksta, Mart Pärstein on öölnud: sest sinna ei pea mitte seapüttiga kaevust vet viima, siis on Juhhan Koppas, Mart Parsteini, üks kord lönud, karve varrega, mis temma käes olnud, Mart Parsteinile vasto puusa, ja tömmand karvu piddi mahha ja poisi ka karvustanud, kas temmale siggu hakkanud luggema.


#Juhhan Koppase poead tunnistavad: et essite kui nemmad läinud vet tooma, on M. Parstein, nende vasdo tulnud, ja vötnud nende ämbri; ja wisganud mahha; wankri pealt, ja öölnud: teie ei pea mitte, nisugguse seal püttidega, minnema vet tooma, ja selle poale on Josep Koppas, öölnud M. P. egga sulgi parramad sea püttid pölle, siis wötnud Mart P. temma Josep Koppase, karvust kinni, ja kisgunud tedda; ja kui kaewule sanud, ja on omma wadi wet täis panud, hakkanud tullema, ja wötnud selle, kaevo vötme, et piddand sedda kaeva moisa, karjatse kätte wiima, kust se vötti olli vöetud, ja kui tahtnud tullema hakkata, on Mart Pärsteini, Naene Kai, tulnud ja hakkanud seda wöttit Josep Koppase, käest ärra kisguma, ja J. Kopas põlle tahtnud anda; et se wötti saab senna, widud kust ma ollen saanud; siis on Kai Pärstein, wõtnud Josep Koppase karvust kinni, ja tömmanud pikkali mahha; ja veddanud jää peal karvupiddi, ja wötnud wötma ärra.


#Tunnismees: Andres Kolga, tunnistas: et temma on näinud, kui Juhhan Koppas, on Mart Pärstein, lönud wasto puusa, agga mitte just wägga kõvva hoopi, ja tömmanud karvupiddi mahha, ja pisi ka karvustanud.


#Koggukonna Kohhos mäistis; et Juhan Koppas maksab Mart Parsteinile 3 rb lömisse ja kisgumisse ette ja Mart Pärsteni poiale 50 kop. Hakenrihtri kohus mahha wötnud Laeka trahi 50 k.


#Ja valla Laeka 4 Rb 50 kop trahvi.


#Ja Kai Parstein maksab 50 kop J. Koppaselle lomisse tete Ja Mart Pärstein maksab: 50 kop. valla Laeka Trahvi.


#Koggukonna Kohto nimmel: H. Kassekamp xxx, M. Holland xxx Hans Mein xxx


