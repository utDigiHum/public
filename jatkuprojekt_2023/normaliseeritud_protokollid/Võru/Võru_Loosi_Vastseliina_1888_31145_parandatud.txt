#Ette tulli Wallawöölmündre Ado Turwas ja tõst kaibust Jaan Udra pääle. et wiimano wiise sel 18 Detsembril õddango Wõrro liinast minno wastse täwwelise hobbese riista ärra, wärt 13 rupla 25 kop - all sieswal wiisil:
Ette tulli valla+vöörmünder Ado Turvas ja tõstis kaebust Jaan Udra peale. Et viimane viis sel 18 detsembril õhtul Võru linnast minu vastsed täielikud hobuse+ +riistad ära, väärt 13 rubla 25 kop - all+ +seisval viisil:

#Sel 18 Detsembril m.a. ollin minna Werro Jaama puid käest andma lännu ja wõtse omma hobbese riista saidlseppa poolt koddo tuwwa, ja tahtsen omma küllamehe Kristjan Reedga ärra koido sõita, agga kunna Jaan Udras minno pool wägisi taggasi hoidse, et sõita minnoga, et küll minna sinno koddo saatan ommalt poolt ja saiwa siis. Udras wõtse siis minno hobbese riisto omma saani, ja sõitsime siis linna ja Jaan Udras ja temma welli Hindrik ütte hobbesega ja Loosi walla wöölmündre Jaan Raudik ja Johann Parm tõise hobbsesega, kunni Apteki ette, sääl karksin minna Apteki ja wõtse 10 kop eest seppa ja tullin 1 minut ao sisse jo wälja ja kai siis Jaan Udras umma welle Hindrikuga ärra olli sõitnu kõigi minno hobbese riistoga, aga Jaan Raudik ja Johhan Parm olli sääl, kelle käest mina siis küssen kos Udras sõitnu om, kes minnol wastust andsiwa, et Jaan Udras sõitsiwa oma wellega ärra, ja kos meije siis tedda kõige liina läbbi otsime, ja wiimalt saime Udra kaupmees Jürgensoni +++++ kätte, ja kon temma siis üttel, et sino hobbene om kõige sino hobbese riistoga ärra warrastud, sis palle kohud, et temma minno hobbese riisto wäggisi käest ärra om wiinu, et temma minno kahjo kätte tassup kui nimmelt riisto eest 13 rupl 25 kop. 2 kõrra Wõrrol sõitmise eest 2 rupl ja et minno hobbene ilmo riistolda seeni aajani seisap ja ette panda ei sa, ja ommi talli laisi hallitada, selle eest egga päiw 1 rupl.
Sel 18 detsembril m.a. olin mina Võru jaama puid käest andma läinud ja võtsin oma hobuse+ +riistad sadulsepa poolt koju tuua, ja tahtsin oma külamehe Kristjan Reediga ära kodu sõita, aga kuna Jaan Udras minu pool+ +vägisi tagasi hoidis, et sõita minuga, et küll mina sinu koju saadan omalt poolt ja said siis. Udras võttis siis minu hobuse riistu oma saani, ja sõitsime siis linna ja Jaan Udras ja tema veli Hindrik ühe hobusega ja Loosi valla vöörmünder Jaan Raudik ja Johann Parm teise hobusega, kuni apteegi ette, seal kargasin mina apteeki ja võtsin 10 kop eest sepa ja tulin 1 minut ¤ sisse ja välja ja kaesin siis Jaan Udras oma velle Hindrikuga ära oli sõitnud kõigi minu hobuse+ +riistadega, aga Jaan Raudik ja Johan Parm olid seal, kelle käest mina siis küsisin kus Udras sõitnud on, kes minul vastust andsid, et Jaan Udras sõitsid oma vellega ära, ja kus meie siis teda kõige linna läbi otsisime, ja viimalt saime Udra kaupmees Jürgensoni +++++ kätte, ja kus tema siis ütles, et sinu hobune on kõige sinu hobuse+ +riistadega ära varastatud, siis palusin kohut, et tema minu hobuse+ +riistu vägisi käest ära on viinud, et tema minu kahju kätte tasub kui nimelt riistade eest 13 rbl 25 kop. 2 korra Võrul sõitmise eest 2 rbl ja et minu hobune ilma riistadeta seni+ +ajani seisab ja ette panna ei saa, ja omi talli+ +lasi talitada, selle eest iga päev 1 rbl.

#Otsus: Jaan Udras, Johann Parm ja Jaan Rauntik tunnistajas ette kutsu.
Otsus: Jaan Udras, Johann Parm ja Jaan Raudik tunnistajaks ette kutsuda.

#Päkohtumees: Johan Kanz
Peakohtumees: Johan Kants

#Kohtumees: Jaan Järg
Kohtumees: Jaan Järg

#Kohtumees: Peter Juk
Kohtumees: Peter Jukk

#Kirjutaja: Piirisild
Kirjutaja: Piirisild

