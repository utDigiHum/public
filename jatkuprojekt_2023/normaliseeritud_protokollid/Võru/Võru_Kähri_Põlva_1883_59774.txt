Saije see poolele jäänu protokoll № 2 edesi seletedus kon puudunuist tulnu olliwa.
Kukke perijide wöörmündri ütlewa kätte antu laste ja ema kraami täitsa käes olewad nink ei ole määrastki kaibust ette laste kui ema poolt tulnu.
Andres Nikopensius edimatse abi elo laste wöörmündri Jaan Roos Jaan Kelt ütlewa: Et nee kelle eest nema Wöörmündri omma, omma nüüd täis ealisis saanu ja nõudwa oma jagu kätte saada. Et 28 rubla saasto protsendidega. Et aga raha mitte protsenti kandnu ei ole saab nüüd ennegi see sisse antud raha 28 rubla wälja antud protsendi jääwa nii kauges kui seeaigse ametniku Jaan Luik ette tulewa.
Peetri läsk Liis Jugowa wöörmündri Andres Nikopensius ütles asja kõik kõrran olewad wana wiisi ent siski palles henda tulewikus oma hädise terwise perast lahti muistema ka wahest saada wõib.
Soldari läsk Mari Kahre wöörmündri ütlewa et tema om kõik iga kaemise aiga korran löüdnu ja ei ole kaibusi tema poolt ette tulnu.
Läsk Hels Raig woormündri ütlewa et neil seeni midagi ütlemist ei ole tulnu oma ulewalitsewa läsja poolt. Woormundri Jakob Liblik pallel henda wallale, sest et tema joba pääle 60 aastad wana om.
Ewa Paidra woormunder Jakob Liblik ütles et ta oma poja wõiwa juba ta eest wastata, mis ta juba minewal aastal om utelnu.
Läsk Mari Troon wöörmündri ütlewa kõik hään kõrran olewad ja kraam täitsa allale olewad.
Läsja Lies Rattas woormündri utlewa kraami kõik alali olewad, nink ka kelle eest nema wastotase olla ütelnu henda oma paiwaliku leiba saawad.
Andrei Westberg wöörmundri ütlewa kõik korran ollew sest et temal omal henda weerel latse omma kellega ta koik oma töö teeb kes ta eest muretsewa.
Läsk Eewa Kroon wöörmündri ütlewa kõik ollew kui muido et täl leiba om puud saab ka muretedus kel tarwis peas tulema.
Kadonu Mihkel Seeba wöörmündri ütlewa omad asjad heas korrs olema.
Jaan Kuhi wöörmündri ütlewa, Et nema luba andnu nink esi nõus olnu maija ära müwwa ja Jaan Kuhi esi ka oma sõsarele ära müünu. Kraam mes tema kadonust wanast emast perinu om täwweste alale.
Peep Wals perijide wöörmündri ütlewa: Et kraam muido küll koik alali om siski mes wõla eest hööwli penk ja ratta ära müüdu saiwa kihelkonna kohtu wälja sundmise perra. 
Peep Wals poig Johani perast ütlewa, et wele naine nõudwad et ta teda ei jõwwa pidada ja nõudwad moona ja maja kütmist.
Läsk Mai Kirberi wöörmündri tuliwa ette ja andsiwa oma wälja saamisse raamatu ette ja nõudsiwa nüüd wiimast osa 22 rubla wälja mis ka wälja antud sai.
Jaan Morast wöörmündri ütlewa edimalt et na ei teedwad, kas see rahapaber alale peas olema mes temal kogo konna kohtu kätte sai. Ka nõudsiwa ees tulewas aajas tema kasu esa käest leeri minna kõlwulika rõiwid et ta auusaste koolitedus saama. Kohus and wastust et paber alale ka and tema kasuesa Kärbe omalt poolt tunistust kui kolmas wöörmunder et see tema hool om, ja pois om jo praegu kui mino lats kanna ma koik täwweste hoole.
Läsk Mari Wals wöörmündri ütlewa et neil kõik hään kõrran om päälegi om temal ka poig kes juba tema eest muretsep et emal koik korran nink täwwelikult om.
Läsk Liis Troon wöörmündri utliwa oma asja hään kõrran olewad, sest et poig temal om kes ta eest muretseb.
Karl Seeba perijide wöörmündri ütlew et neide ülekaemise walitsus konnas ühtegi kaibust laste kui emapoolsest ei ole ette tulnu sest asja om kõik hään kõrran.
Klaani perijide wöörmundri es ole ka täämba mitte tulnu.
Otsus
Keiserliku kihelkonna kohtule see asi teeda anda et na tulnu ei ole ja tõises kõrras wahi all ette tallita.
 Neide üle kuulatuste üle aga protokolli ärakiri kihelkonna kohtule teada anda, et üle kuulatu ja nõutu omma nink heasti enamiste heas korras saisawa ja leitud.
