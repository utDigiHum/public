#Ii Wõru Ülema Talorahwa kohtu jauskonna 15 walla kohus sel 27 januaril 1897 a.


# Juures ollid Kohtu Eesistuja J. Plak


# Kohtu liige J. Raag


# Kohtu liige J. Jõgewa


#Ette tullid Wõru walla liige nink talo koha piddaja Peter Ermel nink Sommerpalo walla liige Jaan Samarüütli nink andsid üles et nemad eneste wahel järelseiswa rendi leppingu suusonal labbi kõnelnu nink kindmas teinud om midda walla kohtu akti raamatuse sisse kirjotada nink kinnitada paluwa


#


#Lepping om järgmine


#Peter Ermel annab oma Wõru walla talu koha Nr 186 Sommerpalo walla liikme Jaan Saamarüütli kätte (4) neljas aastas rendile se om 23 aprilist 1897 kooni 23 aprilini 1901 aastal. Jaan Saamarüütli massab Peter Ermelile selle tema käest renditu talo koha eest (35) kolm kümmend wiis rbl renti aastas edimatse aasta rent 35 rbl saab ühe korraga leppingu tegemise ajal ette ära mastud ja wiimaste kolme aastade rendid saawad kahes terminis ette ära mastud nimelt iga aasta edimane pool renti 23 aprilil ja teine pool 23 oktobril.


#Põldu woib Jaan Saamarüütli oma tahtmise järele piddada ainuld linna ei woi rohkemb aastal tetta kui üks wakkamaa Hoonete kattuse peab rentnik paran-


#dama nink hään korran pidama Rentnik paneb rendi peale andjale Peter Ermelile üks wak kartohwlid maha sitta maa peale Rendi peale andja Peter Ermel annab 10 koormad sõnnikud ja seemne ent rentnik arrip maa, paneb kartohwli maha nink wõttab oma kuluga üles. Antud sõnniku weab rentnik ent enne aprili kuu algust peab sõnnik kätte näidatud saama. Mai Juuni ja juuli kuul ei woi rentniku sõnnikut weddama sundi. Rentnik teep omal kulul rendi peale andja tarre raud ahju sisse ent ahjo materiali annab rendi peale andja Peter Ermel ja massab 50 kop tööraha ent rentnik weab mano ja kannab muud kulud. Peas ette tulema et rendi peale andja Peter Ermel talokoha ära müimisega kaubale saab sis om rendi kondraht murtu ja peab rentnik talo koha käest ära andma. Kui talo koht ära müimise perast enne rendi aja lõppu rentniku käest ära lähab sis massab rendi peale andja Peter Ermel rentnikule ahjo eest tasomises 2 rbl 50 kop. - Leppingut kinnitasid alkirjadega Jaan Samarütel


# Peter Ermel


#


#Et eenseiswa lepping walla kohtus pooltele ette om loetu ja nema sellega rahule ollid nink oma käega leppingule ala kirjotasid nink et leppingu tegijad walla kohtu liikmedele palelikuld tuntawad ollid tunistawa alkirjadega Woru walla majas sel 20 webruaril 1897 aastal.


# Kohtu Eesistuja J. Plak


# Kohtu liige J Raag


# Kohtu liige J Jõgewa


# Kirjotaja /allkiri M Melz/.


