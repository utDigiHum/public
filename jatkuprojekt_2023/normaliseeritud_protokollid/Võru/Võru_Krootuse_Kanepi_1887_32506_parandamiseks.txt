#Lepping


#Kohto ette tullid Jaan Loodus ja tema poig Johan Loodus, nink palusid all saiswad leppingut Raamatude üles kirjotada, nink kui siin all saisab.


# "Mina Jaan Loodus luban siin oma wanemba poja Johan Loodusele kokko leppimise teel pool omast Karaski wallas olewa Mäe Roni talust, nink annan temale priitahtlikult weel pool maad, ni kuis se Ronil wälja annab. poole talu hooned, nimelt, pool Elumaja, pool aita, pool Reht, nink keigest mino Roni talu maa peal olewate muu hoonetest. Peale selle weel pool seeme wilja wälja külimise tarbis, toido wilja, tema perele poole aasta peale, poole põllo töö riista, poole elaja lamba ja Zea, nisama Mõts ja hainamaa poles. Peale selle piab mino poig Johan Loodus keik need maksud Krootuse wallale ja Kerikule poole Roni talu eest omast käest wälja maksma. Wilja seemet luban mina siis Johanile, kui mul teda on, ja osta ei pruugi. Maja luban mina siis Johanile kätte, kui üks kõrd surm saab ära kutsma, ehk kui mino terwis ja wanadus mul enamb ei luba ise oma maja asjo toimetada.


#Ka teen mina pühas kohuses mino poja Johanile, et kui mino poig Michel Loodus piaks Kodo tagasi tulema siis mino poig Johan ilm wasta pandmata, mino norema poja Michklile omast poole Roni talu maadest 3 wakkamaad põllu maad, nink raasike hainamaad lubab anda.


#Seda olen mina targa meele ja moistusega ja ilma kellegi sundmata lubanud, ja lubanu seda pidada ja tunnistan seda oma alla pantud Käekirjaga.


#Lubaja isa: Jaan Loodus +++ 


#


#Mina, Johan Loodus tunnistan siin samas oma isa Jaan Looduse lubamist õiges, nink toowotan mina, ni kaua kui mino isa elab, alati temaga ühes meeles ja mõttes elada, nink keik need temast ette pandut ette wõtmise hääs kiita ja ennast kui hää poig tema tahtmise alla heita. Seda luban mina siin oma alla pantud Kaekirjaga.


#J. Lodus [[allkiri]


# Tunnistaja M Wõso


# " A. Muhle


#Kogokonna kohtole es ole selle leppingo wasta midagi ütlemist, nink kinnitas seda leppingut Kogokonna kohto Nimel.


#Pääkohtomees


#Kohtomees 


# "


#Kirjotaja A. Muhle [allkiri]


