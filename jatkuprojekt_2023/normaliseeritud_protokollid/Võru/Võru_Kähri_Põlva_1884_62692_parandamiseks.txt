#Sai see protocoll 16 Junist m.a. № 114, mis Keiserliku Wõru Silla kohtupoolt kihelkonna kohtu läbi kogukonna kohtule uuesti seletamiseks ja mõistmiseks tagasi saadetud - puuduwa magasi wiljapärast kohtuskäijate - ette loetud ja järele nõutud, kas kellegile nendest weel midagi selle protocolli kohta midagi weel üles anda ehk muud ütlemist on.


#Lepason palus, et magasi mõõdud, mis tema kui mite õiged mõõdud on ülesannud saaksid ülewaadatud ja selle järele ära arwata, kui palju tema kuue aasta kohta nende läbi sell wiisil on kahju saanud, et ametnikude palk ja niisama ka waeste wili on wakaga mis üks karnits suurem on kui õigus - on wälja antud saanud ja üks 1/4 tsetwertiline mõõt mis tsetwerti päält 2 1/2 karnitset wähem on, millega sisse sai wõetud.


#Endine wöörmünder Karl Matzen palus protocolli wõtta et tema 2 wiimist aastad Kähri magasist ei ole mitte käinud wäljaandmas ei ka sissewõtmas; sest 2 aastad enne sääl magasi wilja puudus leidmist wõtis wallawanem säältse magasi wõtme minu käest ära.


#Nõumehed ei ütle enestel muud midagi ütlemist olewad, kuid nõuawad seda puudusleitud wilja selleaegse wöörmündrite ja walla wanemakäest wäljanõuda; nimelt endise wallawanemba Jaan Lepasonilt wöörmündritelt Karl Matzenilt Jacob Kibenalt ja Aadam Kroonilt. 


#Wöörmünder Aadam Kroon wastab: Mai kuus 1881 aasta waliti mind wöörmündriks. Siis oli Suwewili koik magasist wäljalaenatud juba wallawanema Lepasoni ütlemise järele. MIna ei wõtnud enne magasi wõtid wastu kui sügise kus magasi wilja ümber mõõdeti kus siis 149 waka rükid magasis leiti olewad. 1882 aastal Webruari kuu sees oli magasi wilja ümbermõõtmine ja siis leiti puudus olewad. Sellest on jo selgeste näha et mina selle aja sees mingisugust puudust ei magasi wilja juure wõinud teha.


#Mõistus. Kogu konna kohus mõistab, et endine wallawanem Jaan Lepason ja wöörmündrid peawad see magasisse puuduw wili 114 Tsetwerti 10 karn. rükid; 16 Tsetwerti 19 karn. keswi ja 55 Tsetwerti 62 karn. kaaru walla magasisse järgmise arwu järele äramaksma. Ja nimelt: walla wanem Lepason kohalt poole ümber mõõtmise juures puudus leitud summast see on 47 Tsetwerti 59 karn. rükid, 3 Tsetwerti 6 1/2 karn. keswi ja 15 Tsetwerti 3 karn. kaaru sinna juure arwatud 18 Tsetwerti 20 karn. rükid, 10 Tsetw. 6 kar. keswi ja 25 Tsetw. 56 karn. kaaru, wähem sisse kirjutud oodus; ja inimeste pääle kirjutud wõlad mis kohtu läbi temale mõistetud, see on summa 66 Tsetwerti 15 karn. rük; 13 Tsetw. 12 1/2 karn. keswi ja 40 Tsetw. 59 kar. kaaru.


#Wöörmündrid: Karl Matzen, Jacob Kibena ja Aadam Kroon peawad aga kokko teise poole ümbermõõtmise juures puudus leitud sumast magasi maks see on igaühelt: 15 Tsetwerti 62 1/3 kar rük 1 Tsetwert 2 karn keswi ja 5 Tstwerti 1 karn kaaru. Endise wallaw. Lepasoni wabandamine, et mõõdude läbi wilja puudus on sündinud saab tühjaks arwatud, sest kui tema niisugused mõõdud pidas siis peab tema ka nende läbi sündinud kahjude eest wastutama. 


#Otsus sai T.r.s.r. §§ 773 ja 774 järele kohtuskäijatele etteloetud. Aadam Kroon ei olnud mite otsusega rahul. Lepason palus 8 pääwa mõtlemise aega.


#Et Jacob Kibena mite tulnud ei ole siis 10mal Mail ette talitada otsust kuulma.


#Eesistuja Johan Waks XXX


#Kohtumees Jaan Salomon XXX


#do: Johann Wasser [allkiri]


#Kirjutaja: M. Bergmann [allkiri]


#Ärakiri 22 Mail kihelk.kohtu sissesaadetud.


