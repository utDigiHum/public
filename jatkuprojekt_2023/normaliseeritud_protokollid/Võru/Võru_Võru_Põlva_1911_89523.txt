Ado Lätti nink Mai Lätti peranduse jaotamise Proektist ärakiri 

Peranduse jaotamise projektist Wõru walla majas sel 18 juunil 1911 aastal
 Juures olli Kohtu Eesistuja M. Rocht.
Wõru wallas ära surnud talupoja Ado Johani poeg Lätti perijatest üks perija walla kohtuse palwe sisse andnu, milles palub walla kohut surnud Ado Lätti pe-
rijatele peranduse jaotamise juures abis olla ja walla kohus oma otsuse järele sest 9 juunist 1911 aastal selle sama Wõru walla kohtu Eesistujale waranduse jaotamise projekti tegemise peale pandnu sis ilmusin mina, Wõru walla kohtu Eesistuja M. Rocht perijate soowil tänasel ülewal nimitedu päewal ühes kirjotajaga Wõru walla majasse peranduse jaotamise projekti kokko säädma. - 
Nagu Wõru walla kohtu aktist N 275 1895 aastal näha om peranduse waras Wõru wallas ollew Wanalukka talo N 64 14, 27 desjatini kõlbliku nink 0,36 tesja-
tini kõlbmata maad mida perijates ilmunud isikud 500 rubla peale takserisid ent säädusliku takseerimise järele 14 tesjatini kõlbliku maa arwu järele 980 rubla peale saab arwatud. - Seaduse jõulises saanu walla kohtu otsuse järele akt N 275 1895 aastal omma Ado Lättilt järele jäänu peranduse peale perimi-
se õigustes kinnitedu: peranduse jätja lesk Mai Lätt peranduse jätja poeg Kusta Lätt poeg Joosep Lätt nink tüttar Juuli Lätt keik ühe suurustes osades. -
Peale perijate perimise õigustes kinnitamise om Ado Lätti perijatest üks perija Mai Ado lesk Lätt ära surnud nink Wõru walla kohtu akti järele N 131 1910 aastal omma tema osajao peale perimise õigustes kinnitedu tema lapsed poeg Kusta Lätt nink tüttar Juuli Loosu sündinu Lätt, keik ühe suurustes osades. 
Perimise õigustes kinnitedu perijatest omma täna ilmunud, peranduse jätja poeg Kusta Lätt nink Juuli Ado tüttar Loosu sündinu Lätt nink Joosep Ado poeg Lätt asemel tema wolinik Michkel Johani poeg Samoson Wõru liinast Pernu liina Notariuse juures N 873 al 1910 aastal 8 juulil kinnitedu wolikirja ette näidates nink seletasid et nemad, Ado Lätti peranduse wara wäärtuse 500 rubla peale takseeriwad. -
Ilmunutest perijatest andsid Juuli Ado tüttar Loosu sundinu Lätt nink Joosep Ado poeg Lätti wolinik Michkel Johani poeg Samoson üles et nemad Kusta Ado poeg Lättiga peranduse jaotamise asjus kokko omma leppinud niiwiisi et Joosep Ado poeg Lätt nink Juuli Ado tüttar Loosu sündinu Lätt ommi peri-
mise õiguisi ei tarwita ja henda perimise õigustest lahti ütlewa nink lubawa et Kusta Ado poeg Lätt üksinda kõige peranduse wara peale, mes om Wõru wallas ollew wana Lukka talu koht N 64 Kreposti jaoskonnas ära kinnitedud woib saada nink temale Jurjewi Wõru Kreposti jaoskonnast Kreposti akt Wõru walla Wanalukka talu peale wälja antud woib saada mille juures Kusta Ado poeg Lätt henda peale wõttab kõik kroonu nink walla maksud mes peranduse talu eest nõwwetud saawa, ära maksa.
 Kusta Ado poeg Lätt /alkiri/
 Juuli Ado tüttar Loosu /alkiri/
 Joosep Ado tuttar Loosu 
 Joosep Ado poeg Loosu wolinik Michkel Samoson /alkiri/
 Kohtu Eesistuja M. Rocht /alkiri/
 Kirjotaja M Melz /alkiri/

1911 aastal juuni kuu 27 päewal om eenseiswa peranduse jaotamise akt surnud Ado Johani poeg Lätti perijate Kusta Ado poeg Lätti, Juuli Ado tüttar Latti nüüd mehe järgi Loosu nink Joosep Ado poeg Lätti woliniku Michkel Johani poeg Samosoni poolt walla kohtule kinnitamises ette pantud selle juures sai eenseiswa peranduse jaotamise akt Wõru walla kohtu koosistumisel sel 27 juuli kuu päewal 1911 aastal perijatele ette loetud mille järele perijad Kusta Lätt, Juuli Loosu sündinu Lätt nink Joosep Lätti wolinik Michkel Samoson seleti et nema peranduse jaotamisega rahule omma nink selle kinnituses iga üks oma käega oma nime peranduse jaotamise aktile ala kirjotasid.
Siia juure tõendab walla kohus et Ado Lätti perijad walla kohtu otsuse läbi 13 nowembrist 1895 aastal akt N 275 nink Mai Lätti perijad walla kohtu otsuse järele sest 18 augustist 1910 aastal akt N 134. perimise õigustes kinnitedu omma mille järele neil täijelik õigus om perandust ära jaotada nink et Joosep Lätti wolinik Michkel Samosonil asja ajamises Joosep Lätti nimel Pernu Notariuse juures 1910 aastal 8 julil kinnitedu register N 837 al. wolikiri ette naidata olli, et perijad keik täisealised nink õiguse wõimulised sis peranduse jaotamise projekt walla kohtu leppingu raamatusse sisse kirjota nink makswas tunista. Perijad omma kõik walla kohtu kogole palelikuld tuntawad. -
Templi maksu sissenõudmise tõenduses om algusakti peal (5) wije rublane tempel mark Algusakt Kusta Lättile wälja antu.
 Ärakiri om alguskirja järele õige
 Kohtu Eesistuja M Rocht
 Kirjotaja M Melz.

Ääremärkus: Ärakiri .... Kusta tütar Lättile wälja antud 9. VIII 1923 a N 52 al. 62 .......1924 a N 434 al ärakiri Kusta ....................
