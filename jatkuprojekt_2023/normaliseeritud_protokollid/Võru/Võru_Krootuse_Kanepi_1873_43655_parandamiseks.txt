#Jusal sel 19 Januaril 1873.


#Ette tulli Moosikatsist kaiwo kaiwaja Juhann Kirristaja ja Peter Kelt, nink kaibsiwa Jaan Lepasooni pääle, et se ei maswat neile neide teenitud palka, nellitõistkümend rublat, mitte ärra. Kutsuti Jaan Lepasoon ette ja küssiti, meles sa wõõraste töömeeste palka mitte ärra ei massa? Selle kaiwo kaiwamise eest. Jaan üttel: Meil om se assi nida lepitu: Kui sen tõisen kaiwon kolme ehk nelja süle takan wessi tullep, sis massa ma sele kaiwo eest 3 rublat kuwwejalalise sülle päält, ja kui kolme ehk nelja sülle takkan wessi ei tulle, sis massa ma egga mehhe päiwa päält üts pool rublat; minno söök ja üts töö mees weel man minno poolt. Neide katte kaiwo man omma ne mehhe 16 päiwa tööd tennu ja Jaan Lepason om neile ka sedda rahha ülle katte mehhe 16 rublat wälja masnu. Ne mehhe ütliwa: Meije ei olle mitte tõiste lepnu, enge kolm rublat egga kaiwo sülle päält; 10 süld om kaiwetu, se teep 30 rublat. Jaan Leppasoni tunistaja, Jürri Riit, üttel: Se lepingo teiwa na henda wahhel nida: "Jaan Leppasoon massap 1/2 rublat päiwa päält, kumalegi mehhele, kooni Jaan tööd lassep tetta; ja selle pääle lätsiwa ne mehe tööle. Tõine tunnistaja, Johann Sõõrd, om kats kohtu päiwa haige olnu, selle ei olle tema tunistust siin üllewan. 


#Kohus mõist: Et ne kaiwo mõllemba kuiwas omma jäänu nink neil mõlembil töötegijil ja kaiwo peremehel, Jaan Lepsonil, hendide wahhel lepitu olli, ni kui tunistus mees kõnnelep; omma nema, üts pool rublat hõbb. egga mehhe päiwa eest kätte saanu; sis olgo selle palgaga rahhule.


#Kohtumõistmine ha §.§. 773 n. 774. sai kohtun käüjile ette loetus; ent Juhan Kiristaja ja Peter Kelt, es olle sellega rahhu, et se ei olew mite õige, et nema ütspool rublat päiwapäält oma lepnu, ja palsiwa Protokoli wälja, mes kohus lubas anda. 


#Pä kohtu mees Jaan Tullus XXX


# Kohtu mees Jaan Wärs XXX


# do " Peter Mandli XXX


#(LS)


#Protokol wälja antu sel 19 Januaril 1873.


