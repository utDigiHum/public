#Üks tuhat üheksa sada kolmandamal aastal aprilli kuu kuueteist kümnendamal päewal ilmus Krootuse wallakohtu ette Kioma wallamajasse Kioma walla talupoeg Wiido Johani poeg Wisse ja awaldas, et tema soowib oma testamenti teha, mis järgmine on: Isa, Poja ja Püha Waimu nimel. Amen! Mina Wiido Johani poeg Wisse arwasin heaks oma surma juhtumise korraks oma waranduse kohta korda seadmist teha järgmiselt: Minu päralt on omanduse õigusega õigel teel kogutud warandus, nimelt Kioma mõisa all olew Kanariku №8 talukoht taaldri arwu järgi 18 taaldrid wäärt, mida oma puhta südame teedmise järgi 2400 rbl alla takseerin ja selles talus olew liikuw warandus - nagu hobused, kariloomad, põlluseeme ja tööriistad, mis kokku 600 rbl wäärt. Selle nimetud waranduse luban mina oma armsa poja Jaanile pärast minu surma igaweseks waranduseks sellega, et tema maksab oma wendadele 1, Johanile kakssada rublad 2. Augustile seitse sada rubla ja 3. Rudolphile kuussada rubla. Need sumad on Jaanil oma wendadele ilma protsendideta wälja maksa neli aastat pärast minu surma ja ei ole minu poja Jaani käest teistel tema wendadel mingit muud nõudmise õigust. Juhtub, et poeg Jaan päranduseks jäänud talukohta halwaste walitseb ja raiskajaks saab, nõnda et tema kaaspärijatel kindlus kautsisse ähwardab nendele tulewaid osajagusid kätte saada, siis on nendel õigus oma osa jagusi wenna Jaani käest enne nimetud tähtaega nõuda. Poeg Jaani pääle jääb maksa wõlg Liiwimaa mõisnikude Kredit Seltsile ja kolmsada rubla minult wõetud Wõlga Johann Aganale. Poja Jaani ülespidamise peale jääb ka minu naene ja tema oma Katri, kellele ta peale korralise ülespidamise ja riide sooja walgustud korteri kuni ema surmani andma peab. Ei sünni aga ühiselu, siis annab poeg Jaan emale peale prii sooja korteri Kanariku talus, kuni ema surmani iga aasta 7 wakka rükkid, 8 wakka keswi ja kolmkümmend rubla sularaha. Ema wõib talus üks lammas ühes poegega talu söödaga pidada ja ühe sia jaus saab ta talus ruumi. Tunnistan oma allkirjaga, et ma selle testamendi tegemise juures terwe mõistusega ja kindla meeles-pidamisega olen, et see testament wallakohtu poolt minu prii tahtmise järgi üles säetud ja minule sõna sõnalt ette loetud kuuldawalt. Kustutatud sõna "ja" mitte lugeda. Wiida Juhani poeg Wisse ise ei oska kirjutada siis kirjutas tema isikliku palwe peale alla: Carl Sawisaar . [allkiri]


#


#Üks tuhat üheksa sada kolmandamal aastal Aprilli kuu kuueteistkümndamal päewal päewal on talupoeg Wiido Johani poeg Wisse eespool seiswat testamenti Krootuse wallakohtule Kiuma Wallamajas, eesistuja asemel Daniel Palosoni ja kohtuliikmede Mihkel Puna ja Jüri Traati osawõtmisel kirjutaja Kornelius Kuddu juures olekul awaldanud, wiimase poolt üles säetud ja testamendi tegijale kuuldawalt ette loetud, mis juures Wallakohus tunnistab, et testamendi tegija Wido Johani poeg Wisse kohtule isikulikult tutaw, täie mõistuse ja kindla meeles-pidamise juures olli ja et tema isikliku palwe peale, peale testamendi ette lugemise, Karl Sawisar testamendile alla kirjutanud on.


#Kohtueesistuja as: D Paloson [allkiri]


#Kirjutaja KKuddo [allkiri]


#


#1904a. 11 weebr Jaan Wissele № 53all ärakiri wälja antud ja 15 kop. tempel maksu sisse nõutud-


#Eesistuja as: D. Paloson [allkiri]


