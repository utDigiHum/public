#Ette tulli Michel Torop Wastse Koiolast ja kaibas Jakob Trath pääle, kelle käest temä Trati tütre Marri hendäle weel üttes aastas oli tütrekus kaubelnu, nüüd oli see tütruk ärä lännü tõise peremehe man. Palk olnu 8 rubla ja koolitamine.


#Jakob Trath ette kutsutu ja see kaibus kuulutetu, ütlep: et temä lats ei olewad kogonist Michel Torrop manu kaubeldu, Michel käünü kül temä man ja üttelnu et Marri om mino man olnu ja jääb nüüd ka, mina ütli jäägu pääle, sis and Michel minole 20 kop. selle eest et mino tüttar jo aasta temä man oli olnu.


#Marri Trath oma esäga küsitu ütlep et temä ei olewad käe-raha saanu ei midagi, ütte rubla oli temä kül küsinu ja Michel oli andnu palgas muido olewad mööda lännu aasta palk olewad weel Michkli käen. Michel Pihho man olewad nüüd temä.


#Pääle sedä selletab Jakob Traat et Jaan Torop kaubelnu temä tütre hendäle, ja peräst andnu oma poja mannu, mes kõik ilma temä teedmata ja lubata.


#Tunistaia Jaan Patrail tunistab et temä kauba tegemist ei ole kuulnu, tuud oli kuulnu kui Michel Torrop oli Jakob Tratil üttelnu mina ole sino tütre kaubelnu, Jakob üttelnu, mino peräst olgu kos taht. Weel tunistab Patrail et Michel Torrop om Tratile selletanu et Pihho om jo kaubelnu ja käe raha andnu. m.a taha ka hendäle ja Marri lubas ka, Kui lubatu sis saagu minole. Ja massa ka tu palga mes Piho mas. 


#Otsus:


#Kogokonna kohus mõistab Michel Torrop kaibus Jakob Traat ja temä tütre wasta tühjas.


#II. Marri Traat jääb Michel Pihho teenistuste, et Michel Pihho ene kaubelnu om. Ja Michel Pihho massab selle eest 2 rubla trahwi waeste kassa § 368 perrä, et ilma lubata kaulnu om.


#Otsus kuuluteti § 773 ja 774 perrä.


#Michel Torrop and üles et temä rahul ei ole. Saie opus kiri selsamal päiwal antus. Piho masse 2 rubla trahwi ärä sels. päiwal.


#Pääkohtumees Jaan Luik


#Kohtumees


# " Jaan Hingo


#20 Junil s.a. wälja antu.


