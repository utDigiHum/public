Kaibas mötsa Esand Jakob Rosneck, et 8 Juunil olewat möisa mötsast 3712 löhmuse niidse ära kistud, Jaan Weber on selle kurja töö pääl kätte saadu. Töiselt om Hermann Weber sellesama kurja töö päält kätte saadu ja tema käest 150 löhmuse niidse kätte saadu.
Kinni wötja oli mötsawaht Jaan Leppikow, neid mölembit kurjategijt om kätte saanu ja neidega könelnu sääl. Nowab nimitatus kahjutegijate käest


Jaan Weber 3713 niidse eest a' 1 Kop.
			37 r. 13 k.
		

Herman Weber 150 niidse eest a' 1 kop.
			1 r. 50 k.
		
Wastutas selle kaibuse pääle Jaan Weber: Tema ei olewat neid niidsi wötnu ja ei tija sest midagi.
Wastutas hermann Weber: Tema olewat löhmuse niidsi küll kisknu Orawa mötsan aga mitte Kahkwa mötsan, sest ei tija tema midagim wöib olle, et karja lats om mõne niidse kisknu. Need 15 niidse puud, mis Kahkwa mötsawaht toll körral üle lugenu, olnuwa küll tema kistud.
Tunistus kohtumees Jaan Kirhäiding ja Johann Lepland, et toll körral neide niidse puude lugemise manu paarkümmend sammu üle piiri Orawa maa pääl lugenuwa nema 150 niidse puud wälja, Orawa mötsawaht tunistas sääl näide een, et need puud ei ole mitte Orawa mötsast kistud, üts jagu wähämbid niidse puid olnu weel sääl neid tunistanu see mötsawaht Orawa mötsast kistud olewad.
Tunistas mötsawaht Jaan Leppikow: et tema om toll körral oman Kahkwa mötsa ökwa manu saanu kui Jaan Weber om niidsi kisknu; Weberil om weel 5 töist seltsi mees ka kaasan olnu. Möned neist lännuwa ökwa pakku, tema wötnu üte naisterahwa kinni sääl tulnu Jaan Weber manu ja nakanu suure wihaga könelema, pääle länuwa nema ära kõiki kistuide niidsiga. Töise seltsi mehi ei ole tema Leppikow, mitte tundnu. Jaan Weber ähwardanu küsiden waite käen, mis sa tahat, mis sa otsit ja mis sull siin asi om? Et naid palju olnu, ei wöinu tema midagi naile nakata wasta könelema, waid wötnu üte sületäwwe niidsi ära ja lännu ära.
Jaan Weber kostis selle tunistuse pääle, et Jaan Leppikow ei ole teda kinni wötnu, ja ei ole tema tunistus mitte tötte, tema ei ole Jaan Lepikowi ialgi nännu ei ka tunne teda.

Otsus.
Kohhus möistis: 1. Hermann Weber peab neide temast Kahkwa mötsan kistude 150 löhmuse eest 1 rbl. 50 kop. mötsaesandalee Jakob Rosnikule wälja masma.
2. Et Jaan Weberi asi suuremb om kui kogukonna kohus seda ära möista wöib, selle perast tegi kohus otsuses, seda protokolli Keiserliku Maakohtu kätte saata.

Pääkohtumees Joh. Lepland
Kohtumees J. Kirhäiding
Kohtumees J. Oinberg
