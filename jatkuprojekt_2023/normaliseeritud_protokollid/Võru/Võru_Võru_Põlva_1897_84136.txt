II Wõru Ülema Talorahwa kohtu jaoskonna 15 walla kohus sel 21. aprilil 1897 aastal
 Kohtu liige J. Raag
 Kohal ollid Kohtu liige J. Plado
 Kohtu liige J. Jogewa.
Ette tullid Wõru walla talonikud Katri Kaarli lesk Roos nink Kusta Kaarli poeg Pund nink andsid üles et nemad eneste wahel suusõnalise leppingu kokko on konelnud mida walla kohtu akti raamatuse sisse kirjotada nink kinnitada paluwa

Lepping om järgmine
1, Katre Roos annab oma s.o. Wõru walla talu Nr 133. Wõru walla talopoja Kusta Pund kätte üttes, eestulewas aastas poole tera peale järel seiswa tingimiste al.
2, Kõigest põllu saagist, wälja arwatu selles leppingus punkt 4 nimitedud kartohwli nink punkt wijendamas nimitedu linna, annab pool teranik Katre Roosele pool terra, wälja arwata seeme. -
3, Poolteranik saab kõigest talo peralt olewast heinadest, olgu ädal ehk wana hain, pooled heinad omale midda tema heina ajal nimitedu talo maa päält ära soowib wija. Kõik pollult saadu põhk jääb Katre Roosele ent aganatest nimelt rüa ja tõu wilja aganadest saab pooltera kolmanda jao ent herne, ua ja lina seemne aganatest saab pool jago midda ka pooltera kik ära woib wija. -
4, Poolteranik paneb ühe wakka kartohwliid nimitedu talo maa peale kellest tema mitte Katre Roosele poolt ei anna, muido kiik kartohwlid harrib pooltera- nik kõik ütte wiisi nink saawa ne pooles jäetud
5, Pool teranik täidab kõik walla orjuslised nõudmised mes walla walitsus selle talo päält saab nõudma ja massab Katre Roosele maa maksude maksmise tarwis (14) nelli teistkümmend rubla puhast raha mille eest tema nimitud talu nurmest, kost pool teranik esi tahab üks wakkamaa maad omale linas saab millest tema Katre Roosele midagi ei anna. -
6, Pool teranik ei ela mitte Katre Roose talu ruumides ainult pruukib neid rehe peksmise tarwis Rehe kütmine om pool teraniku asi. Katri Roosele weab palutus materiali pool teranik juure.
 Kusta Pund
 Katre Roos

Et eenseiswa lepping walla kohtus leppingu tegijadele pooltele ette om loetud nink kui nemad henda sellega rahule olewad tunistasid sis kirjotasid sellele ala. - Kusta Pund nink Katre Roos iga üks oma käega nink et leppingu tegijad pooled walla kohtu liikmedele tuntawad on tunistawa alkirjadega
 Kohtu liige J Raag
 Kohtu liige J Plado
 Kohtu liige J Jõgewa
 Kirjotaja /allkiri M. Melz/
