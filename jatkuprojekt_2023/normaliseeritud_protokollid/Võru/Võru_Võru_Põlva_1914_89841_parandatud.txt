#Wõru walla kohus 12 märtsi 1914 aastal.
Võru valla+ +kohus 12. märtsi 1914. aastal. 

# Koos olid: Eesistuja K. Käämbre
 Koos olid: eesistuja K. Käämbre

# Kohtu liikmed J. Udras, J. Saks
 Kohtu liikmed J. Udras, J. Saks

#


#Ette tullid Wõru wallast, Puusepa N 121 talu omaniku Hindrik Ado poeg Jõggeda, kes Ria Riigikorra kohtu poolt raiskajaks tunnistatud - hoolekandjad Ado Jakobi poeg Treial ja Jaan Johani poeg Jõeweer, nimi Mihkel Hindriku poeg Jõggeda, andsid üles, et nemad on heneste wahel suusõnalise lepingu teinud, mida walla kohtu akti raamatusse paluwad sisse kirjutada ja kinnitada
Ette tulid Võru vallast, Puusepa n 121 talu omaniku Hindrik Ado poeg Jõggeda, kes Riia riigikorra kohtu poolt raiskajaks tunnistatud - hoolekandjad Ado Jakobi poeg Treial ja Jaan Johani poeg Jõeveer, nimi Mihkel Hindriku poeg Jõggeda, andsid üles, et nemad on eneste vahel suusõnalise lepingu teinud, mida valla+ +kohtu akti+ +raamatusse paluvad sisse kirjutada ja kinnitada

#Leping on järgmine:
Leping on järgmine:

#


#1, Hindrik Jõggeda hoolekandjad Ado Treial ja Jaan Jõeweer annawad, kohtu luga järgi, nende hoolekandmise al olewast Puusepa N 121 talu maast järgmised maatükkid:
1, Hindrik Jõggeda hoolekandjad Ado Treial ja Jaan Jõeveer annavad, kohtu loa järgi, nende hoolekandmise all olevast Puusepa n 121 talu+ +maast järgmised maatükid:

#1, ümbre Mihkel Jõggeda elumaja olewast maast kolm wakamaad nurme ehk põllu maad, 2, Jõresoo niidu, Wõru walla talude N 128, 129 ja 114 talude heina maade wahel umbes kolm wakamaad suur 3, üks wakamaa Oja niidust N 125 talu piirist risti läbi mööta N 119 talu heinamaa piiri, - Mihkel Hindriku poeg Jõgge-
1, ümber Mihkel Jõggeda elumaja olevast maast kolm vakamaad nurme ehk põllu+ +maad, 2, Jõresoo niidu, Võru valla talude n 128, 129 ja 114 talude heina+ +maade vahel umbes kolm vakamaad suur 3, üks vakamaa Oja niidust n 125 talu piirist risti läbi mõõta n 119 talu heinamaa piiri, - Mihkel Hindriku poeg Jõgge-

#dale ühe aasta pääle rendile, se om 23 aprillist 1914 aastast kuni 23 aprillini 1915 aastani.
dale ühe aasta peale rendile, see on 23. aprillist 1914. aastast kuni 23. aprillini 1915. aastani.

#2, Renti massab Mihkel Jõggeda nende nimitedu maa tükide eest aastas wiistäistkümme (15) rublid hoolekandjate kätte tänasel päwal kõik korraga ette ära. -
2, Renti maksab Mihkel Jõggeda nende nimetatud maa+ +tükkide eest aastas viisteistkümme (15) rubla hoolekandjate kätte tänasel päeval kõik korraga ette ära. -

#3, Maa peab korraliku maapidamise korra järgi aritud ja peetud saama
3, Maa peab korraliku maapidamise korra järgi haritud ja peetud saama

#4, Hainu ja õlgi ei ole rentnikul luba maa päält ära wia ehk müwwa. -
4, Heinu ja õlgi ei ole rendnikul luba maa pealt ära viia ehk müüa. -

#5, Maa päält nõwwetawad keriku ja jaama massud massab rentnik oma maa suuruse järgi nink orjab ka kiik walla töö päewa - maa suurust mööda.
5, Maa pealt nõutavad kiriku ja jaama maksud maksab rendnik oma maa suuruse järgi ning orjab ka kõik valla töö+ +päevad - maa suurust mööda.

# Ado Treial
 Ado Treial

# Jaan Jõewer
 Jaan Jõever

#Rentnik Mihkel Jõgeda, et aga kirja ei oska, siis kirjut tema palwe pääle ala: Gusta Kasak.
Rentnik Mihkel Jõggeda, et aga kirja ei oska, siis kirjutab tema palve peale alla: Gustav Kasak.

#


#1914 aastal märtsi kuu 12 päewal om eesnseiswa leping lepingu tegijate pooltele Ado Treialile, Jaan Jõewere ja Mihkel Jõggeda poolt suusõnal walla kohtule ette pantud, nende seletuste järele sisse kirjutedu, lepingu tegijate pooltele walla kohtu koosolekul ette loetu, selle juures tõendab walla kohus, et lepingu tegijad Ado Treial, Jaan Jõeweer, Mihkel Jõgeda walla kohtu liikmetele palelikult tutawa omma nink et nemad, Ado Treial ja Jaan Jõeweer omma oma käega lepingule ala kirjutanud, nink Mihkel Jõgeda eest, kes esi kirja ei oska- Kusta Kasak ala kirjutanud. -
1914. aastal märtsi+ +kuu 12. päeval on eesseisev leping lepingu tegijate pooltele Ado Treialile, Jaan Jõevere ja Mihkel Jõggeda poolt suusõnal valla+ +kohtule ette pandud, nende seletuste järele sisse kirjutatud, lepingu tegijate pooltele valla+ +kohtu koosolekul ette loetud, selle juures tõendab valla+ +kohus, et lepingu tegijad Ado Treial, Jaan Jõeveer, Mihkel Jõggeda valla+ +kohtu liikmetele palelikult tuttavad on ning et nemad, Ado Treial ja Jaan Jõeveer on oma käega lepingule alla kirjutanud, ning Mihkel Jõggeda eest, kes ise kirja ei oska- Kusta Kasak alla kirjutanud. -

# Kohtu Eesistuja K. Käämbre
 Kohtu eesistuja K. Käämbre

# Kohtu liige J Saks
 Kohtu liige J Saks

# - " - J Udras
 - " - J Udras

# as. kirjutaja /allkiri/
 as. kirjutaja /allkiri/

