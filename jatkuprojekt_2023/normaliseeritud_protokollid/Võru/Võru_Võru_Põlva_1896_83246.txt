II Wõru Ülema Talorahwa kohtu jaoskonna 15 walla kohus 9 webruaril 1896 a.
 Juures olli Kohtu Eesistuja M Parweots
 Kohtu liige H. Sibbol

Ette tulli Wõru walla Tupitsa talu Nr 17 omanik Peter Purjemotz, ja and üles et tema Liinamae walla mehe Peter Halleriga kes ka tänases päiwas kohtu ette olli tulnu, jargmise leppingu omma tennu midda molemba walla kohtu akti raamatuse sisse kirjotada nink kinnitada paluwa

Lepping om järgmine.
1, Peter Purjemõtz annab omast Tupitsa talo maast kaks osa Liinamae walla mehe Peter Hallerile kolmes järjestikku aastas rendile nimelt 23 aprilist 1896 aastast kooni 23 aprilini 1899 aastal.
2, Rentnik Peter Haller massab Peter Purjemotsale selle maa eest renti 70 /seitse kümmend rubla aastas nimelt pool renti se om 35 rubla 23 aprilil ja II pool 
1 oktobris se om 35 rubla ette ära
3, Rendile andja Peter Purjemotz annab rentnikule Peter Hallerile rendi kotta sisse tuleku kõrral 2 jago külwetu rüa nurmest nisama ka rüa kõrrest nink kesast 2 jago umbes 15 wakkamaad.
4, Karja maad ja heinamaad saab nisama kats jago rentnik oma kätte pruukimises. Rentnik woib karja maast mis woimalik künda om põllus tetta ilma omaniku ise eralise lubata.
5,Hoonid annab rendile andja rentniku elu tarest ja köögist poole, keldri päälik, karja laut ja õle küün jääb kõik rentnikule pruuki
6, Usaija woi hoowi aija awitab rentnik ära teha omaniku kraamiga Olgi kattuste taranda annab annab omanik esimesel aastal kus neid tarbis om
7, Rentnik peab oma katte antud nurmed häste harima hää seemnega kulwama pääle selle annab omanik esimesel aastal s.o 1896 aasta jaoks kaks jago sõnnikud pollu wäetamiseks Rentnikul ei ole õigust mitte enamb õlgi ei ka heino talost ära wija kui neid ka peas üle jääma.
8, Talo omanik Peter Purjemõtz ehitab lauda rentnikule walmis 1896 aast. enne sügiset oma kraamiga teep muuri ja paneb lae paale ja usse ette kui tarwis peab rentnik ka selle too mann abis olema
9, Rentnik annab omanikule walja mineku kõrral kätte antu nurmed selles samas seisus nink külwi korras kätte kui tema neid rendile andja kaest kätte sai
10, Peale selle peab rentnik kõik selle talu peal seiswad walla tööd tegema nisama ka teed säädma.
 Peter Haller
Peter Purjemotz selet et kirja ei oska tema palwe peale kirjod ala K Jarwpold

Et eenseiswa leppingule leppingu tegijad pooled nimelt Peter Haller oma kaega ja Peter Purjemotsa asemel Kaarli Jarwpold Purjemotsa palwe peale oma käega oma nime ala kirjotasid Peter Purjemots om palelikuld nink Peter Haller ette toodu tunistuse jarele walla kohtu liikmile tutwa
 Kohtu Eesistuja M Parweots
 Kohtu liige H. Sibbol
 Kohtu liige.
 Kirjotaja /allkiri M Melz/
