#15 Walla kohus II Wõru Ülema talorahwa kohtu Jaoskonnas 8 Januaril 1892.
15 Valla+ +kohus II Võru Ülema Talurahva+ +kohtu jaoskonnas 8 jaanuaril 1892.

#Man olli Kohtu Eesistuja H. Jõperra 
Man olid kohtu eesistuja H. Jõepera 

# kohtumõistja M. Ritsar
 kohtumõistja M. Riitsaar

# kohtumoistja M Sarnit
 kohtumõistja M Saarniit

#Ette tulli Wõru wallast Hindrek Mattus Wõru walla Mattuse talo omanik ja palles oma järel seiswad lubbamist üles kirjotada. Se om järgmine Mina Hindrek Mattus, Mattuse talo omanik olen mõttelnud oma Mattuse talo oma poegatele Hindrek ja Jaanile pooles anda ja et minol weel teisi latsi om kes ka mino perandusest osa peawa saama ent üks mino poegadest Widrik Mattus om kroonu Wäe teenistuste ära lannu sis tahan mina temale tema osa jakko ära määrata ja se osajaggo om järgmine mina arwan et mino pooja Jaan nink Hindrek kellele mina oma talo peran mino surma ära anda motlen Widriki jakko rahaga walja massa ei jõwwa sis lubban mina Widrikile Mattuse talo maast ühe tukki heina maad, nimelt se niit mes seisab Woo jõe weeren Koroli küla ja Trei talo heina maade wahel,
Ette tuli Võru vallast Hindrek Mattus Võru valla Mattuse talu omanik ja palus oma järel+ +seisvat lubamist üles kirjutada. See on järgmine Mina Hindrek Mattus, Mattuse talu omanik olen mõelnud oma Mattuse talu oma poegadele Hindrek ja Jaanile pooleks anda ja et minul veel teisi lapsi on kes ka minu pärandusest osa peavad saama ent üks minu poegadest Vidrik Mattus on kroonu väe+ +teenistusse ära lainud siis tahan mina temale tema osa+ +jagu ära määrata ja see osajagu on järgmine mina arvan et minu pojad Jaan ning Hindrek kellele mina oma talu pärast minu surma ära anda motlen Vidriku jagu rahaga valja maksta ei jõua siis luban mina Vidrikule Mattuse talu maast ühe tüki heina+ +maad, nimelt see niit mis seisab Voo jõe veeres Koroli küla ja Trei talu heina+ +maade vahel,

#9 (üttesas) aastas ja saab Widrik Mattus koige se heinamaa saagi kasu nink tulu sest 1894 aastast saadik kooni 1904 aastani oma kätte ja kui mino perija Hindrek nink Jaan sedda mino lubbamist peran mino surma täita ei taha sis peawa nema igga aasta kooni lubbatud ajani kui heina maa Widrik Mattuse käes ei ole 40 rbl nelli kümmend rbl pooles Widrik Mattusele masma ja peale 1904 aasta saab heina maa neile taggasi ja sis ei ole neil enamb Widrikile ei mingi sugust massu, ja peale selle kui Hindrek ja Jaan selle mino lubbamise ara tautnu ommma sis ei ole Widrikil isa warandusest enamb üttegi peranduse osa jao noudmiste oigust.
9 (üheksaks) aastaks ja saab Vidrik Mattus kõige see heinamaa saagi kasu ning tulu sest 1894 aastast saadik kuni 1904 aastani oma kätte ja kui minu pärijad Hindrek ning Jaan seda minu lubamist pärast minu surma täita ei taha siis peavad nemad iga aasta kuni lubatud ajani kui heina+ +maa Vidrik Mattuse käes ei ole 40 rbl neli+ +kümmend rbl pooleks Vidrik Mattusele maksma ja peale 1904 aasta saab heina+ +maa neile tagasi ja siis ei ole neil enam Vidrikule ei mingi+ +sugust maksu, ja peale selle kui Hindrek ja Jaan selle minu lubamise ara täitnud on siis ei ole Vidrikul isa varandusest enam ühtegi päranduse osa+ +jao noudmiste õigust.

#Oma lubbamist kinnitan oma käe alkirjaga.
Oma lubamist kinnitan oma käe allkirjaga.

#Et Walla kohtu liikmedele teeda ja tuutaw Hindrek Mattus eenseiswa lubbadusele oma käega oma nime ala om kirjotanu tunistawa oma alkirjadega walla kohtu liikme
Et valla+ +kohtu liikmetele teada ja tuttav Hindrek Mattus eesseisva lubadusele oma käega oma nime alla on kirjutanud tunnistavad oma allkirjadega valla+ +kohtu liikmed

# Kohtu Eesistuja H. Jõeperra
 Kohtu eesistuja H. Jõepera

# Kohtumoistja Miga Laanmõts
 Kohtumõistja Mika Laanmõts

# Kohtumoistja T. Müürsep.
 Kohtumõistja T. Müürsepp.

# Kirjotaja M Melz.
 Kirjotaja M Melts.

