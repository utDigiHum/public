#Ette tulli Mango Kütt Laswa wallast ja kaibas Losi walla mees Rein Sikk wasta, et see teda Wastse-Kasseritsa Utra kõrtsi läwe pääl pudeliga päha löönu ni et pää ära weristedus saanu ja raskeste löödus saanu; tema nõudwat walu raha 50 Rbl. ja 2 päiwa eest kuna Rein Sik ette es ole tulnu 1 Rbl.


#Mango Kütt and tunnistjas üles kes nännu, et tema pää weritses olnu lüüdu Leeno Sikk, Peep Sawwi ja kõrtsi mees Karl Lipping.


#Wastut kaibuse pääle Losi walla mees Rein Sikk, et tema mitte Mango Kütti ei ollew löönu, enge ollew kül esi kõrtsist wälja minnen õlle pudelidega maha sadanu ja olnu see kõrd joobunu.


#Katte kõrra kohtu erre wälja jäämise perast wastut Rein Sikk, et üttel paiwal tema Loosi kandora man olnu ja tõisel päiwal Jaama moona hainu Wõrrole weeman olnu, selle perast jäänu temal kohtu ette tulemata.


#Tunnistaja Karl Lipping 46 aastat wana Lutteri usku, tunnist, et Mango Kütt tulnu kõrtsi ja olnu pää werine ja joosknu pallet mööda weri maha ja üttelnu, et Rein Sikk teda löönu. Kui pää werest ära saanu puhtas mõstus, sis olnu tunda, et pää lahki ehk hoob olnu löödu, kes teda löönu ei teedwat tema. Sell samal ajal lännu tema (tunnistaja) kõrtsist wälja ja hõiganu Rein Sikkale perra et see tema pudeli ära toos, mes tema kõrtsist olla wälja weenu, ja see hõiganu temale, et tule, sis saat sina ka seda.


#Tunnistaja Leno Sikk 26 aastat wana Lutteri usku tunnist, et kui Mango Kütt kõrtsi tulnu, olnu täll pää werine ja kui werest puhtas saanu mõstu, olnu ka haab tettu, kes teda löönu olnu, ei ollew tema mitte nännu kui Mango Kütt ollew esi üttelnu, kui kõrtsi sisse tulnu, et Rein Sikk teda löönu.


#Tunnistaja Peep Sawi 59 a. wana Lutteri usku tunnist nisamate kui Leno Sikk.


#


#Otsus:


#Rein Sikk saije süüdlases arwatus ja peap Mango Küttile masma walu raha 3 Rbl. ja 2 päiwa kohtu ette wälja jäämise eest 1 Rbl. Summa 4 Rbl. 14 päiwa sisen a dato.


#


#Otsus om ette loetu.


#


#Pääkohtumees: J. Kabist [allkiri]


#Kohtumees: P. Sillaots [allkiri]


# " P. Klais [allkiri]


#


#Rein Sikk ja Mango Kütt omma henne wahel 20 januaril 1888 ära lepnu nida wiisi et Sik om masnu Küttile 1 Rbl.


