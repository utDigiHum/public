#Kusta Kolk tulli kohto ette ja kaibas, et temä ollew oma maja rahwaga riht pesnu, kuna sis ka nende kodapooline An Wiga, kes hoopis tõisen taren elawat, rehe ala tulnu, peran seda, kui K. Kolk tütrike kutsnu tapno weeri heitma - ja nakanu sääl Kolga keelu wasta wõõriti tööd tegema, üttelden, et tema näitawat, kuis maschinaga riht pessetawat. Kui An Wiga mitme kõrralise keelu pääle rihä ärä ei ole pandnu, sis om K. Kolk riha tahtnu käest wõtta, aga An Wiga ollew Kolga habemist kinni wõtnu ja nakkanu sõimama ja tahtnu weel koodiga Kolgale lüüa, selle man tulnu An Wiga tütär ja tõise rihelise appi, kes Anne käest koodi ärä ollew wõtnu. Pääle selle lännu K. Kolk tarre ja kui ta tagasi tulnu, olnu An weel sääl, lännu aga pea pääle selle ärä.


#An Wiga kutsuti kohtu ette ja kui temale Kolga kaibdus ette loeti, sis wastutas seesama, et temä oma tütre asemele töhe om lännu. Kolga kutsmise pääle ja nakkanu weeri heitma, kes teda aga seda tegemast keelnu ja tedä rehe alt wälja ajanu, temä aga ei ollew enne lännu kui tema tütär sinna ollew tulnu, sis lännu ja istnu rihe läwe posti kõrwale. Kui temä Kusta Kolgaga tahtnu selletada, mis eest Kolk tedä ärä ajanu, sis tulnu Peeter Kolk, wõtnu tedä olast kinni ja llöönu maha ja käsknu weel malka tuwwa. Selle man ollew tunnistaja Johan Kull ja Jaan Pannel.


#Johan Kull kutsuti kohto ette, kos seesama tunnistas, et An Wiga tulnu rehe ala ja nakkanu weeri heitma, selle man aga ka lärmitsema ja sõimama. Kui Kusta Kolk Annel ärä käsknu minna, see aga seda käsku mitte ei ole kuulnu, sis tahtnu Kolk Anne käest rihä ärä wõtta. Sedä ei ollew temä mitte nännu, kas An Kolga habemest kinni om wõtnu, ehk mitte, sedä kül kuulnu kui Kolk Annele üttelnu, "Lase mo habene wallale:" Tapelust ei ollew temä nännü.


#Jaan Pannel tunnistas, et temä ei ollew midagi nännü, muud kui kuulnu, et An K. Kolka rihe al sõimanu ja sedä ka kuulnu, kui K. Kolk An Wigale üttelnu: "Lase mo habenist wallale."


#Otsus: Et An Wiga sõimanu, lärmitsenu ja peremehe käsko ei ole kuulnu, sis mõistse kog. kohus An Wiga selle eest 24 tunnis türmi jämatüss et edespäide raho peap pidama.


#Kui kog. kohto otsus ja Sääduse §§ 773 jan774 kohton käüjatele ette saiwa loetu, sis ütles An Wiga, et temä selle otsusega raho ei ole ja pallel Protocolli wälja.


#J. Tagel


#A. Org


#G. Asi


