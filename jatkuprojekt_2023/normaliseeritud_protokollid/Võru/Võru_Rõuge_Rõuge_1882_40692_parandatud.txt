#Ette tuli kellamees Jüri Pruwli ja kaibas, et Tindi küla poisikese oliwa tulnu tema uibid warastama, oliwa nakkanu kiwwega loopima, nink tema penni pesma, kes tema ajan kinni oli. Nema oliwa üttelnu: Mine ajast wälja, et meie sisse wõime tulla. Kellamees oli ütelnu: Tulge päiwa, küll ma sis teile ubinit anna. Nema oliwa tahtnu kellameest nink tema perenaist kiwwega koolus wisata, kellamees oli üte puu taade nink perenaine wäraja posti taade lännu nink niida oliwa nema hend kiwwe eest kaitsnu.
Ette tuli kellamees Jüri Pruuli ja kaebas, et Tindi küla poisikesed olid tulnud tema uibuid varastama, olid hakanud kividega loopima, ning tema peni peksma, kes tema aias kinni oli. Nemad olid öelnud: Mine aiast välja, et meie sisse võime tulla. Kellamees oli öelnud: Tulge päeval, küll ma siis teile ubinaid annan. Nemad olid tahtnud kellameest ning tema perenaist kividega koolnuks visata, kellamees oli ühe puu taha ning perenaine värava+ +posti taha läinud ning nõnda olid nemad end kivide eest kaitsnud.

#Jüri Pruwli palleb, et süüdlase sääduslikult trahwitus saasse.
Jüri Pruuli palub, et süüdlased seaduslikult trahvitud saaksid.

#Aijan käija oliwa Eduard Kull, Tannil Woitka, Jakob Marran, Jaan Kuus, Johan Troska ja Peeter Pritsitaja.
Aias+ +käijad olid Eduard Kull, Tannil Voitka, Jakob Maran, Jaan Kuus, Johan Troska ja Peeter Pritsitaja.

#Ette sai kutsutus Eduard Kull, kes ütleb, et tema sest asjast teedwat. Widrik Woitkal olewat ubinit olnu, kõik se wähemalt 20 tükki. Aijan olewat olnu Jaan Kuus, Johan Troska, Peeter Pitsitaja, Jakob Marran. Eduard Kull es ole mitte ubinit saanu. Kiwwe oliwa kõik wiskanu.
Ette sai kutsutud Eduard Kull, kes ütleb, et tema sest asjast teadvat. Vidrik Voitkal olevat ubinaid olnud, kõik see vähemalt 20 tükki. Aias olevat olnud Jaan Kuus, Johan Troska, Peeter Pitsitaja, Jakob Maran. Eduard Kull ei olnud mitte ubinaid saanu. Kive olid kõik visanud.

#Jakob Marran om Ado Johannsoni oppipois nink elas kerikowallan, Tindi külan, ilma et tema oles wallawalitsusele oma elamist teeda andnu. Oppimise aig 2 ajastaiga.
Jakob Maran on Ado Johansoni õpipoiss ning elas kirikuvallas, Tindi külas, ilma et tema oleks vallavalitsusele oma elamist teada andnud. Õppimise aeg 2 aastat.

#Mõistus:
Mõistus:

#Tannil Woitka, Jaan Kuus, Johan Troska ja Jakob Marran saawa ega üts 5 witsa löögiga ubina warguse perast trahwitus, ent Eduard Kull, kes kohtu een om kõik äratunnistanu, nink mitte aida ei ole saanu, saab noomitus. Peeter Pitsitaja Nogult, tuleb tulewa kõrd kohtu ette nink saab oma trahwi kandma. Egalütel om üte kõrra wäljajäämise eest sääduslik trahw 30 Kop massa.
Tannil Voitka, Jaan Kuus, Johan Troska ja Jakob Maran saavad iga+ +üks 5 vitsa+ +löögiga ubina+ +varguse pärast trahvitud, ent Eduard Kull, kes kohtu ees on kõik ära+tunnistanud, ning mitte aeda ei ole saanud, saab noomitud. Peeter Pitsitaja Nõgult, tuleb tuleva kord kohtu ette ning saab oma trahvi kandma. Igalühel on ühe korra väljajäämise eest seaduslik trahv 30 kop maksta.

#Jakob Marran mas VI. T. Kih Koht. ring kirja perra 16- Marz c. No 1441 perra 5 rbl. trahwi, et ilma luba tähelda siin om elanu.
Jakob Maran maksis VI. T. Kih Koht. ring+ +kirja perra 16- märts c. No 1441 perra 5 rbl. trahvi, et ilma luba+ +täheta siin on elanud.

#Jakob Marran ütleb, et kohus wõlswat, nink lät selle perast 24 tunnis türmi.
Jakob Maran ütleb, et kohus võltsivat, ning läheb selle pärast 24 tunniks türmi.

#


#Pääkohtumees: Jaan Kroon [allkiri]
Peakohtumees: Jaan Kroon [allkiri]

#Kohtumees: J. Meister [allkiri]
Kohtumees: J. Meister [allkiri]

#Abi: J. Kõiw [allkiri]
Abi: J. Kõiv [allkiri]

