#15 Walla kohus II Wõru Ülema talorahwa kohtu Jaoskonnas 8 Januaril 1892.


#Man olli Kohtu Eesistuja H. Jõperra 


# kohtumoistja M. Ritsar


# kohtumoistja M Sarnit


#Ette tulli Wõru wallast Hindrek Mattus Wõru walla Mattuse talo omanik ja palles oma järel seiswad lubbamist üles kirjotada. Se om järgmine Mina Hindrek Mattus, Mattuse talo omanik olen mõttelnud oma Mattuse talo oma poegatele Hindrek ja Jaanile pooles anda ja et minol weel teisi latsi om kes ka mino perandusest osa peawa saama ent üks mino poegadest Widrik Mattus om kroonu Wäe teenistuste ära lannu sis tahan mina temale tema osa jakko ära määrata ja se osajaggo om järgmine mina arwan et mino pooja Jaan nink Hindrek kellele mina oma talo peran mino surma ära anda motlen Widriki jakko rahaga walja massa ei jõwwa sis lubban mina Widrikile Mattuse talo maast ühe tukki heina maad, nimelt se niit mes seisab Woo jõe weeren Koroli küla ja Trei talo heina maade wahel,


#9 (üttesas) aastas ja saab Widrik Mattus koige se heinamaa saagi kasu nink tulu sest 1894 aastast saadik kooni 1904 aastani oma kätte ja kui mino perija Hindrek nink Jaan sedda mino lubbamist peran mino surma täita ei taha sis peawa nema igga aasta kooni lubbatud ajani kui heina maa Widrik Mattuse käes ei ole 40 rbl nelli kümmend rbl pooles Widrik Mattusele masma ja peale 1904 aasta saab heina maa neile taggasi ja sis ei ole neil enamb Widrikile ei mingi sugust massu, ja peale selle kui Hindrek ja Jaan selle mino lubbamise ara tautnu ommma sis ei ole Widrikil isa warandusest enamb üttegi peranduse osa jao noudmiste oigust.


#Oma lubbamist kinnitan oma käe alkirjaga.


# Hindrk Mattus 


#Et Walla kohtu liikmedele teeda ja tuutaw Hindrek Mattus eenseiswa lubbadusele oma käega oma nime ala om kirjotanu tunistawa oma alkirjadega walla kohtu liikme


# Kohtu Eesistuja H. Jõeperra


# Kohtumoistja Miga Laanmõts


# Kohtumoistja T. Müürsep.


# Kirjotaja M Melz.


