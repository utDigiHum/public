#Ette tuli kellamees Jüri Pruwli ja kaibas, et Tindi küla poisikese oliwa tulnu tema uibid warastama, oliwa nakkanu kiwwega loopima, nink tema penni pesma, kes tema ajan kinni oli. Nema oliwa üttelnu: Mine ajast wälja, et meie sisse wõime tulla. Kellamees oli ütelnu: Tulge päiwa, küll ma sis teile ubinit anna. Nema oliwa tahtnu kellameest nink tema perenaist kiwwega koolus wisata, kellamees oli üte puu taade nink perenaine wäraja posti taade lännu nink niida oliwa nema hend kiwwe eest kaitsnu.


#Jüri Pruwli palleb, et süüdlase sääduslikult trahwitus saasse.


#Aijan käija oliwa Eduard Kull, Tannil Woitka, Jakob Marran, Jaan Kuus, Johan Troska ja Peeter Pritsitaja.


#Ette sai kutsutus Eduard Kull, kes ütleb, et tema sest asjast teedwat. Widrik Woitkal olewat ubinit olnu, kõik se wähemalt 20 tükki. Aijan olewat olnu Jaan Kuus, Johan Troska, Peeter Pitsitaja, Jakob Marran. Eduard Kull es ole mitte ubinit saanu. Kiwwe oliwa kõik wiskanu.


#Jakob Marran om Ado Johannsoni oppipois nink elas kerikowallan, Tindi külan, ilma et tema oles wallawalitsusele oma elamist teeda andnu. Oppimise aig 2 ajastaiga.


#Mõistus:


#Tannil Woitka, Jaan Kuus, Johan Troska ja Jakob Marran saawa ega üts 5 witsa löögiga ubina warguse perast trahwitus, ent Eduard Kull, kes kohtu een om kõik äratunnistanu, nink mitte aida ei ole saanu, saab noomitus. Peeter Pitsitaja Nogult, tuleb tulewa kõrd kohtu ette nink saab oma trahwi kandma. Egalütel om üte kõrra wäljajäämise eest sääduslik trahw 30 Kop massa.


#Jakob Marran mas VI. T. Kih Koht. ring kirja perra 16- Marz c. No 1441 perra 5 rbl. trahwi, et ilma luba tähelda siin om elanu.


#Jakob Marran ütleb, et kohus wõlswat, nink lät selle perast 24 tunnis türmi.


#


#Pääkohtumees: Jaan Kroon [allkiri]


#Kohtumees: J. Meister [allkiri]


#Abi: J. Kõiw [allkiri]


