#Kaiwas Jakob Mening, et tema rentnik Kristow Parmak ei tahtwat temale wõlgu jäänu renti 85 rubl. mitte äramassa.
Kaebas Jakob Menning, et tema rentnik Kristof Parmak ei tahtvat temale võlgu jäänud renti 85 rubl. mitte ära+maksta.

#Wastutas Kristow Parmak, et temal ei olewat mitte 85 rubl., enge 55 r. massa, sest kate aasta rent om 230 rubl. Ära massetu om Meninge kwitungide perra 145 r. ja 30 r. ilma kwitungida, kokko 175 r. ära massetu. Üts körd olewat tema, Parmak, Meningile 50 rubl. üte katusse rentmise tarwis, mis Parmak tahtnu hendale wõta Lewon Brauneri käest Pihkwa kubermangun, andnu ja Mening olewat se 50 rubl. selle tarwis Brauneri kätte käerahas sissemasnu. Perast seda ei ole temal, Parmakil, olnu wöimalik seda kotust wõtta, sis nöwwab tema seda raha Meningi käest tagasi saada ehk nüüdse rendi wöla ette arwata. Tunnistaja raha andmise man: Trafim Sidorow ja Lewon Brauner, Pihkwa kreisist 3dast Stanast.
Vastutas Kristof Parmak, et temal ei olevat mitte 85 rbl., enge 55 rbl. maksta, sest kahe aasta rent on 230 rubl. Ära makstud on Menningu kviitungite perra 145 rbl. ja 30 rbl. ilma kviitungita, kokku 175 rbl. ära makstud. Üks+ +kord olevat tema, Parmak, Menningule 50 rbl. ühe kotuse rentimise tarvis, mis Parmak tahtnud endale võtta Levon Brauneri käest Pihkva kubermangus, andnud ja Menning olevat see 50 rbl. selle tarvis Brauneri kätte käerahaks sisse+maksnud. Pärast seda ei ole temal, Parmakul, olnud võimalik seda kohta võtta, siis nõuab tema seda raha Menningu käest tagasi saada ehk nüüdse rendi+ +võla ette arvata. Tunnistajad raha andmise man: Trafim Sidorov ja Levon Brauner, Pihkva kreisist 3ndast Stanast.

#Jakob Mening es salga, et tema 30 rubl. pääle massetust rahast kwitungit ei ole andnu ja nöwwap nüüd weel wölgu jäänu 55 rubl. renti. Ent seda ülewal nimitud 50 rubl. ei olewat Kristow Parmak mitte tema kätte andnu, enge Lewon Brauneri kätte; selle man olewat tema, Mening, ennegi kui nägija man olnu.
Jakob Menning ei salanud, et tema 30 rbl. peale makstud rahast kviitungit ei ole andnud ja nõuab nüüd veel võlgu jäänud 55 rbl. renti. Ent seda üleval+ +nimetatud 50 rbl. ei olevat Kristof Parmak mitte tema kätte andnud, enge Levon Brauneri kätte; selle man olevat tema, Menning, ennegi kui nägija man olnud.

#


#Otsus.
Otsus.

#Kohus moistis: Kristow Parmak peab Jakob Meningele wõlgu jäänu rent 55 rubl. wälja masma. Kristow Parmaki nöudmine Jakob Meningi käest 50 rubl. perrast, ei putu mitte selle kohto ala, selleperast et Jakob Mening Pihkwa kubermangun elab ja se asi ka säälsaman om sündinu
Kohus mõistis: Kristof Parmak peab Jakob Menningule võlgu jäänud rent 55 rbl. välja maksma. Kristof Parmaku nõudmine Jakob Menningu käest 50 rbl. pärast, ei puutu mitte selle kohtu alla, sellepärast et Jakob Menning Pihkva kubermangus elab ja see asi ka sealsamas on sündinud

#Kristtow Parmak es ole selle kohtootsusega rahul ja sai temale tema palwe pääle se protokoll wälja antus.
Kristof Parmak ei olnud selle kohtuotsusega rahul ja sai temale tema palve peale see protokoll välja antud.

#


#Pääkohtomees Johan Lepland
Peakohtumees Johan Lepland

#Kohtomees Jaan Kirhäiding
Kohtumees Jaan Kirhäiding

#Kohtomees Josep Oinberg
Kohtumees Joosep Oinberg

#


#Kirjotaja D. Otsing &lt;allkiri&gt;
Kirjutaja D. Otsing &lt;allkiri&gt;

#​​​​​​​
​​​​​​​

