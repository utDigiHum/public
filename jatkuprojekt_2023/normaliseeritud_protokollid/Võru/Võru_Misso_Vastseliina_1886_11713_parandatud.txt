#Ette tulliwa Kersna Hollo, Wastselinast Andri Peterson, Johann Härm, Mickli Härm ja Peter Jakobson Missost ja kaibsiwa Michli Uibu pääle, et nema om tema all kui poträisnniki all töö tennu ja ruusa wedanu selle tee pääle mis Zerebinast Petserisse lääb, ent Uibu ei ole neile mitte palka wälja masnu neil olewat weel saada
Ette tulid Kersna Hollo, Vastseliinast Andri Peterson, Johann Härm, Mickli Härm ja Peter Jakobson Missost ja kaebasivad Mihli Uibu peale, et nemad on tema all kui poträisnniki all töö teinud ja kruusa vedanud selle tee peale mis Tserebinast Petserisse läheb, ent Uibu ei ole neile mitte palka välja maksnud neil olevat veel saada

#Kersna Hollo 8 Rbl.
Kersna Hollo 8 rbl.

#Andri Peterson 5 Rbl. 80 Kop
Andri Peterson 5 rbl. 80 kop

#Johan Härm 8 Rbl.
Johan Härm 8 rbl.

#Michli Härm 4 Rbl.
Michli Härm 4 rbl.

#Peter Jakobson 7 Rbl.
Peter Jakobson 7 rbl.

# Summa 32 Rbl. 80 Kop
 Summa 32 rbl. 80 kop

#Nüüd pallewa nema, et neile se raha saas ära mastus mis Michli Uibu om lubanu ka ära massa, kui töö om tettu, ja nema omma ruusa ära wedanu ja Uibu olli kõigega rahul.
Nüüd paluvad nemad, et neile see raha saaks ära makstud mis Michli Uibu on lubanud ka ära maksta, kui töö on tehtud, ja nemad oma kruusa ära vedanud ja Uibu oli kõigega rahul.

#Ette tulli Michel Uibu ja wastud kaibuse pääle, et tema om neile üttelnu kui töö om tettu ja ruus weetu, ja kui mina raha ole saanu, sis saab ka ega üts oma kätte ja tema and neile teedmise, et kui üle kaemine tuleb peab ega üts tulema ja mis puudus, manu manu wedawa, ent nema omma kõik jalksi tulnu ja sis sai se töö tee kaejast ära põletus, ja pidä ümbre säetus saama, ja kui nema se 20 süld ruusa, mis maha om wõetu, ära oles wedanu, sis saanu nema ka oma raha kõik kätte, kui mulle ära ols mastu, niida kui neil saada om kui xxx üttelnu xxx nii palju
Ette tuli Michel Uibu ja vastas kaebuse peale, et tema on neile ütelnud kui töö on tehtud ja kruus veetud, ja kui mina raha olen saanud, siis saab ka iga üks oma kätte ja tema annab neile teadmise, et kui üle kaemine tuleb peab iga üks tulema ja mis puudus, manu manu vedava, ent nemad on kõik jalksi tulnud ja siis sai see töö tee kaejast ära põletatud, ja pidi ümber seatud saama, ja kui nemad see 20 süld kruusa, mis maha on võetud, ära oleks vedanud, siis saanud nemad ka oma raha kõik kätte, kui mulle ära oleks makstud, nii kui neil saada on kui xxx ütelnud xxx nii palju

#Tunnistaja Peter Ausgar, lutheri usku tunnist, et sellest ruusast om puudus rehkendedu 20 süld ja sellest rahast om seen 48 Rbl.
Tunnistaja Peter Ausgar, luteri usku tunnistab, et sellest kruusast on puudu rehkendatud 20 süld ja sellest rahast on seen 48 Rbl.

#Peter Ausgar om ruusa wastu wõtja, ja tema elab Schelerowan Panikowitze kantorin (wallan). Ka ütles tema et nüüd ennam kruusa wedamist ei wõi tama lubada, sest tema om tee käest ära andnu ja oma kahju kandnu weel rohkemb kui nema, seda wõiwa nema Panikowitza kandorist perra nõuda wai ka Pihkwast.
Peter Ausgar on kruusa vastu võtja, ja tema elab Schelerovan Panikovitse kantoris (vallas). Ka ütles tema et nüüd enam kruusa vedamist ei või tema lubada, sest tema on tee käest ära andnud ja oma kahju kandnud veel rohkem kui nemad, seda võivad nemad Panikovitza kandorist perra nõuda või ka Pihkvast.

#Mõistedi:
Mõisteti:

#"Neide kaibus tühjas arwatu Michli Uibu wasta, et Ruusa wastu wõtja Peter Ausgar om tunnistanu, et ruusa om puudus jäänu, ja ega üts sest kahju om saanu."
"Nende kaebus tühjaks arvatud Mihkli Uibu vastu, et kruusa vastu võtja Peter Ausgar on tunnistanud, et kruusa on puudu jäänud, ja iga üks sest kahju on saanud."

#Se kohtu otsus om kuulutedu §773-774 perra, ja kaibaja omma Appellationi nõudnu, mis neile ka lubati.
See kohtu+ +otsus on kuulutatud §773-774 perra, ja kaebaja oma apellatsiooni nõudnud, mis neile ka lubati.

#Pääkohtumees K. Lõhmus [allkiri]
Peakohtumees K. Lõhmus [allkiri]

#Kohtumees T. Pild [allkiri]
Kohtumees T. Pild [allkiri]

#Kohtumees Jaan Oppar XXX
Kohtumees Jaan Oppar XXX

#Kirjutaja asemel P. Normann
Kirjutaja asemel P. Normann

#Kaibajille Copia 24.Julil wälja kirjutedu.
Kaebajatele koopia 24.juulil välja kirjutatud.

