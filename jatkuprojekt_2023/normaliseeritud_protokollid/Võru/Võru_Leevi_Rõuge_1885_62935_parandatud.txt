#N 5.
N 5.

#Lewi kogokona kohus sel 11 Januar 1885
Leevi kogukonna+ +kohus sel 11 jaanuar 1885

#Koos olliwa Pääkohtomees Jaan Thalfeld
Koos olid peakohtumees Jaan Thalfeld

#kohtomees Kusta Moistus
kohtumees Kusta Moistus

#" Johann Suits
" Johann Suits

#Ette tulli Samul Wijar Lewi moisast luteri usku ja kaewas Simon Sulg pääle Lewi moisast luteri usku et kui temma präga homigo karja aija päle om ajanu om temma esse sööma lännu ja omma poja Josepi ni kaugus saat nikaugus perra kaema saatnu ja kui tema tagasi om tulnu om tema leudnu et tema poja lõug um werine olnu ja Simon Sulg om pagemise kombel säält wälja joosnu sääl om tema siis teda kinni wõtta tahtnu kos Simon Sulg siis temale raudlapjoga päha om löönu ni et säält paljo werd om wälja joosnu mes Lewi kohtomehe esi üle omma kaenu ja nõwwap selle eest 25 Rubla rohitsemise ja aja wiitmise raha. Tunnistajas and tema üles Jass Morel Lewi moisast.
Ette tuli Samul Vijar Leevi mõisast luteri usku ja kaevas Simon Sulg peale Leevi mõisast luteri usku et kui tema prägu hommiku karja aia peale on ajanud on tema ise sööma läinud ja oma poja Joosepi nii kaugel saatis niikaugul perra kaema saatnud ja kui tema tagasi on tulnud on tema leidnud et tema poja lõug on verine olnud ja Simon Sulg on pagemise kombel sealt välja jooksnud seal on tema siis teda kinni võtta tahtnud kus Simon Sulg siis temale raudlapjuga pähe on löönud nii et sealt palju verd on välja jooksnud mis Leevi kohtumehe ose üle oma kaenud ja nõuab selle eest 25 rubla rohitsemise ja aja+ +viitmise raha. Tunnistajaks andis tema üles Jass Morel Leevi mõisast.

#Jass Morel saije ette kutsudut ja tunnist et tema om ennegi nännu kui Simoin Sulg om karja aija poolt joosnu ja Samul Wijar tagas kel pää wäga werd om joosnu.
Jass Morel sai ette kutsutud ja tunnistas et tema on ennegi näinud kui Simoin Sulg on karja aia poolt joosnud ja Samul Vijar tagas kellel pea väga verd on jooksnud.

#Selle pääle saije ette kutsutud Simon Sulg ja and selle eessaiswa kaebuse pääle wastust et kui temma karja aida um lännu om Samul Wijar poig tedda om praga wargas sõimanu kus tema tedda siis kül tahtnu kinni wotta ja ei ole mitte saanu siis om tema essa tulnu ja om teda kinni wõtnu, lumme maha pandnu ja põlwega rindo pääle surwnu kos tema siis küll hädaga selle lapjuga mes käes olli om lönu ent ei tija kos se löök läts.
Selle peale sai ette kutsutud Simon Sulg ja andis selle eesseisva kaebuse peale vastust et kui tema karja aida on läinud on Samul Vijar poeg teda on praga vargaks sõimanud kus tema teda siis küll tahtnud kinni võtta ja ei ole mitte saanud siis on tema ise tulnud ja on teda kinni võtnud, lumme maha pannud ja põlvega rindu peale survnud kus tema siis küll hädaga selle lapjuga mis käes oli on löönud ent ei tea kus see löök läks.

#Moistmine.
Mõistmine.

#kuna nüüd selge on et Simon Sulg Samul Wijar wäga raske hobi om andnu ja üleuuldse suuret koerastükkid temmast wälja paistawa, saap Simon Sulg 10 witsa lööki ja peap Samul Wijar hääs weel rohtude ja aja wiitmise kulu tarwis 9 Rubl. 4 näddali aja sees wälja masma. Moistmine saije kohtokäijadele 773, 774 perra ette kuulutud.
kuna nüüd selge on et Simon Sulg Samul Vijar väga raske hoobi on andnud ja üleüldse suured koerustükid temast välja paistavad, saab Simon Sulg 10 vitsa+ +lööki ja peab Samul Vijar heaks veel rohtude ja aja+ +viitmise kulu tarvis 9 rubl. 4 nädala aja sees välja maksma. Mõistmine sai kohtukäijatele 773, 774 perra ette kuulutatud.

#Pääkohtomees [allkiri] Hendrik Kikkas
Peakohtumees [allkiri] Hendrik Kikas

#kohtomees [allkiri] Juhan Suits
kohtumees [allkiri] Juhan Suits

#" " [allkiri] Kusta Moistu
" " [allkiri] Kusta Moistu

#Kirjotaj [allkiri] P Perli
Kirjutaja [allkiri] P Perli

#Appellation perra antu
Apellatsioon perra antud

#ja Copia sel 17 Januar 1885.
ja koopia sel 17 jaanuar 1885.

