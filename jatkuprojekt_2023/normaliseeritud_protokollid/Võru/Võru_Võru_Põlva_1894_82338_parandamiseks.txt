#15 Walla kohus II Wõru Ülema Talorahwa kohtu jaoskonnas 27 septembril 1894.


# Man olli kohtu Eesistuja M. Parweots


# Kohtumõistja H. Sibbol


# Kohtumõistja O. Pille


#Ette tulli Wõru wallast Mihkel Lindaju ja tema poeg Jaan Lindaju nink pallewa järel seiswad kirjaliku leppingut leppingu raamatuse sisse kirjutada.


#


# Lepping on järgmine.


#Sel tänasel päiwal om Wõru walla talupoegade Michel Lindaju ja Mihkli poig Jaan Lindaju wahel järgmine lepping kindlas tehtud ja ja kõigis punktides kirja pan-


#tud ni et eddas pidi mingisuggust wasto räägimist ei woi ette toodud saada. 


#Michkel Lindaju annab selle labbi oma Wõru wallas olewa Nr 136 talu maatüki mis tema kõrge kruunu käest omanduses om saanud oma wanema poja Jaan Lind-


#ajule praeguses olekus kõige selle maa tükki peralt olewa õiguste ja tema peal olewa kohustega iggawetses omanduses ära kahe saja rubla eest, missugune raha jobba ära makstud ja mitmesuggumate tasumistega tasa tehtud. 


#Peale leppitud raha wõttab Jaan Lindaju oma peale, wanemate, Michkel ja Ann Lindaju üles piddamist joudu mööda, kunni wiimaste surmani, kusjuures aga ne-


#mad Jaan Lindajule jõudu mööda maja hollitusses igga piddi abbiks peawad olema. Jaan Lindaju toetab ka iggapiddi omad nooremad wennad Nikkolai ja Samuel kui ka õe Sohwie Lindojad konni nende täie ealises saamiseni, peale selle saab ka kõik Michkel Lindaju liikuw warandus midda praegusel korral 64 rubla 70 kop. eest olemas om. Se eesnimitedu warandus saije kiik mõlembide leppingu poolde juuresolemisel üle kaetus ja mõlemade kokko leppimise järele üles takseritus nink leüti et sedda warandust 64 rbl 70 kop wäärtuses olemas om, sest tänasest päiwast Jaan Lindaju omanduses kuid aga wiimane peab igga ühele selles leppingus nimetud inimestele 10 rbl 70 kop, sel kõrral kuna õiguse osalised sedda nüwawad, kohe wälja maksma wastasel korral kui Jaan Lindaju ommi kohuseid, mis leppingus nimetud oma wanemade wasta mitte ei täida massab tem(m)a igga kord kui ka leppingud rikkub 10 rbl tasomist Michel Lindajule lõppeks tõendawad mõleamd leppingu pooled et leppingu alune warandus mitte 300 rbl wäärtusest üle ei kai.


# Een seiswad leppingut kinnitawa alkirjadega Jaan Lindaja 


# Michel Lindaja 


#


#Eenseiswa lepping om pooltele walla kohtus ette loetud ja omma nema peale selle kui nema omma rahul olemist awaldasid selle leppingule oma käega oma nime ala kirjutanu, pooled om walla kuhtule tuntawad, tunistawa alkirjadega


# Kohtu Eesituja M. Parweots.


# Kohtumõistja H. Sibbol.


# Kohtumõistja O. Pille 


# Kirjutaja /M Melz allkri/


