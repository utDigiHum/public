19. Ete astus Koogu walla mees Kustaw Kuldner ja kaebab, et tema käinud 23. Septembril Kaupsaarelt silku toomas, siis on tema wankri päält heina wõrk ära warastatud ja Juhan Tauer ütelnud: "ära otsi mujalt, kui Naaskli Andrese käest." 13nal Oktobril käis tema Lüganuse poest soola toomas, siis on Andres Naaskel olnud Purtse Liiwa kõrtsi ees, tema läinud waatama ja leidis oma wõrgu Naaskli wankri päält. Tema wõtnud säält ära ja wii oma wankrile, siis läks Andresele ütlema, aga Andres tuli oma seltsimeestega ja wõtis wõrgu jälle ära ja ütles selle oma olewad ja lõi weel päälegi. 

Johann Pentson annud nõuu, seda wõrku senna kõrtsi jäta, nii kauaks, kui kohus seda asja seletab, aga Andres ei jätnud. 

Kaewataw Andrew Naaskel ütleb, et tema ei annud mite sellepärast wõrku ära, sest et see tema oma on. Tema on selle Kundas lasknud teha, nööridest. Löömist ei olnud sääl ka mite. 

Tunnistaja Jaan Seil ütleb, et Kustaw Kuldner wõtis Liiwa kõrtsi ees Andres Naaskeli wankrilt heina-wõrgu ja ütlenud, et see warastati Kaupsaarel ära ja nüüd leian siit. Sellepääle kutsus ta Naaskli kõrtsist wälja ja ütles temale, aga Naaskel kutsus oma seltsimehed: Otu Rätsepa ja Kustaw Põdra ja wõtis wõrgu jälle ära ja ütles oma jagu olema ja Kundas teha laskma. Pentson (Nigulast) andis nõu wõrku nii kauaks senna kõrtsi jäta, kui as saab seletud, aga Andrew oma seltsimeestega ei annud seda woli. Nüüd on Andresel üks wõrk siin kaasas, aga ei ole mite see, mis Liiwal oli. 

Tunnistaja Kustaw Põdra ütleb, et tema on tunud kõrtsist wälja, siis on Naaskel ja Kuldner wastastiku wõrgu kallal kiskunud ja mõlelmad õtelnud oma olewad. Tema käskinud kiskumist järel jäta, set ega hein wõi maha wisata ja pärast wõib seletada, kelle oma see on. Tema ei wõi ka tunnistada, kas see wõrk, mis praegu Naasklil siin kaasas on, seesama on, mis Liiwa kõrtsei ees wai ei ole; nöörist on mõlemad, aga säal tema ligemast ei waatnud.

Tunnistaja Otu Rätsep ütleb et Andres Naaskel ja Kustaw Kuldner kiskunud wõrku teine teise käest, aga tema ei teand, kelle oma see oli. Kingsep Johann Pentson annud kel nõu wõrgu senna jäta, et pärast kohus seletab, aga tema käskis wõrgu paigale jäta, et ei wõinud heinu kuhukille panna, ega see kadunud ei saa. Tema ei wõi seda ka tunnistada, kas see wõrk, mis siin on, ka Purtse Liiwa oli wõi oli teine; nöörist olid mõlemad. Löömist tema ei näind.

Jääbe poolele.

Kohtuwanem: Jakob Lokotar [allkiri]
" kõrwamees: Mihkel Pärn [allkiri]
" " Kristjan Samel [allkiri]

Kirjutaja: J Waater
