#7.Leppimine N 6 all Gustaw Lindami ja Joosep Roosenbaumi wahel sai keiserliku Haljala kihelkonna kohtu poolest tühjaks arwatud ja walla kohtu moista antud;aga et Gustaw Lindamil wana asjade kohta ühtigi tunnistust ei olnud,aga ka omal teada,kui palju temalt warastud oli,siis sai Joosep Roosenbaum weel üle kuulamiseks ette kutsutud.Ja tema tunnistas:et aja roikaid on ühe korra seljas ahju toond ja ka wahest reie peksu ajal rukkid tasku pand,aga kahel aastal ei öölnud wilja arimise ajal Lindami tö juures sugugi olema.Siis sai J.Roosenbaumi oma tunnistuse järel G.Lindamile kahju tasumist arwatud wanawarguse ette,wiimsed warastud asjad sai Lindam keik kätte.Pool umbes arwatud 5 aja roigast a,10 kop.se on 50 kop.


#4 aastat iga aasta pool setweriki rukid - 2 r


#Nüüd sügise 1886 a.seale ette antud 1/2 setw.ruk.1/2 set.odre 1 " /3 rubl.50 k.


#71 tö pääwa ütles J.Roosenbaum G.Lindamile tegema aga sellest saab 6 maha arwatud G.Lindamile tasumiseks sauna aluse ma ette 6 pääwa


#Lehma karjama ette 8 pääwa


#1 tünder kartuhwlid maks 9 "


#Kapsa peenar 2 "/25 pääwa


#Gustaw Lindam ütles 68 1/3 pääwa üle pea oled teind,heina ja leiku päiwi 17 1/3 pääwa neist maksan 50 kop.pääwas.


#Lühemaid päiwi mojal tö juures 51 p. a,30 kop.pääwas.


#G.Lindami ütlemine sai oigeks arwatud ja J.Roosenbaum oli sellega rahu.


#Aga G.Lindam päris sauna ette 12 pääwa


#Lehma karjasma ja karjatse ette kokku 14 "


#Kartuhwlid ja kapsad mis maas olid 12 " /kokku 38 pääwa


#ja ütles mullu ka jo nenda olema,aga J.Roosenbaum ütls mullu 25 pääwa tegema,aga tunnistust ei olnud neil kummalgi selle kauba juures ei susanaga ega kirjalikult,siis kohus arwas need waidlemise pääwad pooleks.Nenda et J.Roosenbaumil on G.Lindamile tasuda heina ja leiku tö juure 31 pääwa.Pääwad rahasse arwatud 51 paawa iga pääw 30 kop.s.o.15 r.30.k


# 17 1/3 " a, 50 " " 8 " 66/23.96-19/4.96


#31 pääwa sest ära wõtta a,50 kop.s.o.15 rubl.50 kop.


#Kahju tasumine 3 " 50 "/19


#4 rubla 96 kopika on G.Lindamil weel J.Roosenbaumile maksta.G.Lindam maksis seda kohe ära ja palus protokolli wälja.Ja seda sai temale lubatud.Ka Joosep Roosenbaum maksis enese ja naese eest 3 rubla trahwi wälja mis 25 Oktobril sai moistetud.


