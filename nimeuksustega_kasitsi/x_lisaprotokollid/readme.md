## Käsitsi kontrollitud nimeüksuste märgendusega protokollid (lisa)

Siin kaustas on 228 käsitsi kontrollitud nimeüksuste märgendusega vallakohtu protokolli, kokku ca **80 890** sõnet (sh kirjavahemärgid ja numbrid) ning ca **6670** nimeüksust. 
Kõik need protokollid esinevad ka **käsitsi kontrollitud morfoloogiliste analüüsidega** protokollide seas, seega saab neid kasutada, et uurida seoseid morfoloogilise märgenduse ja nimeüksuste märgenduse vahel. 
 
Märgendused on kahes formaadis:

* [tsv](tsv) failidena, kus iga tekstisõne paikneb eraldi real ning tabulaatoriga (`\t`) on lisatud nimeüksuse märgend BIO-kujul: `O` tähistab nimeüksusest väljajäävat tekstisõnet, `B-` tähistab nimeüksuse alguses paiknevat tekstisõnet ning `I-` tähistab nimeüksuse sees olevat tekstisõnet. `B-` ja `I-` märgendile järgneb nimeüksuse liik (`PER`, `LOC-ORG`, `LOC`, `ORG` või `MISC`).
* [json](json) failidena, mis vastavad [EstNLTK json kujul](https://github.com/estnltk/estnltk/blob/main/tutorials/converters/json_exporter_importer.ipynb) dokumentidele.  Dokumentidel on 3 märgenduskihti: `"gold_ner"`, kus nimeüksused on esitatud fraasidena,  `"gold_wordner"`, kus nimeüksused on esitatud BIO-kujul (samas formaadis, mis tsv failides) ning `'gold_morph_analysis'`, mis sisaldab käsitsi kontrollitud morfoloogilisi analüüse (`2021-06-30` seisuga).

Nii json kui csv failinimed järgivad formaati: `(maakond)_(kihelkond)_(vald)_id(dokumendi_id)_(aasta)a.` 

