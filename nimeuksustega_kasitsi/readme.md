## Käsitsi kontrollitud nimeüksuste märgendusega protokollid

Siin kaustas on 1500 käsitsi kontrollitud nimeüksuste märgendusega vallakohtu protokolli, kokku ca **320 400** sõnet (sh kirjavahemärgid ja numbrid) ning **27 540** nimeüksust. 
Märgendused on kahes formaadis:

* [tsv](tsv) failidena, kus iga tekstisõne paikneb eraldi real ning tabulaatoriga (`\t`) on lisatud nimeüksuse märgend BIO-kujul: `O` tähistab nimeüksusest väljajäävat tekstisõnet, `B-` tähistab nimeüksuse alguses paiknevat tekstisõnet ning `I-` tähistab nimeüksuse sees olevat tekstisõnet. `B-` ja `I-` märgendile järgneb nimeüksuse liik (`PER`, `LOC-ORG`, `LOC`, `ORG` või `MISC`).
* [json](json) failidena, mis vastavad [EstNLTK json kujul](https://github.com/estnltk/estnltk/blob/main/tutorials/converters/json_exporter_importer.ipynb) dokumentidele.  Dokumentidel on 2 märgenduskihti: `"gold_ner"`, kus nimeüksused on esitatud fraasidena, ning `"gold_wordner"`, kus nimeüksused on esitatud BIO-kujul (samas formaadis, mis tsv failides).

Nii json kui csv failinimed järgivad formaati: `(maakond)_(kihelkond)_(vald)_id(dokumendi_id)_(aasta)a.` 

Failide jaotust treening-, arendus- ja testhulgaks, mida kasutati [masinõppe eksperimentides](https://github.com/soras/vk_ner_lrec_2022), kirjeldab fail [train_dev_test_split.csv](train_dev_test_split.csv).

Korpuse loomist ja nimeüksuste tuvastamise masinõppe-eksperimente kirjeldab artikkel ["Named Entity Recognition in Estonian 19th Century Parish Court Records"](https://aclanthology.org/2022.lrec-1.568); eestikeelne märgendusjuhend on failis [nimeyksuste_margendusjuhend.pdf](nimeyksuste_margendusjuhend.pdf).

### Lisaprotokollid

Kaustas [x_lisaprotokollid](x_lisaprotokollid) on 228 käsitsi kontrollitud nimeüksuste märgendusega vallakohtu protokolli, kokku ca **80 890** sõnet (sh kirjavahemärgid ja numbrid) ning ca **6670** nimeüksust. 
Kõik need protokollid esinevad ka **käsitsi kontrollitud morfoloogiliste analüüsidega** protokollide seas, seega saab neid kasutada, et uurida seoseid morfoloogilise märgenduse ja nimeüksuste märgenduse vahel. 