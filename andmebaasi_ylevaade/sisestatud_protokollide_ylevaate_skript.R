# Laadi vajalikud paketid
library(ggplot2)
library(dplyr)
library(ggrepel)
library(gridExtra)
library(grid)
library(sf)

# Loe fail (NB! Avalik kaust ei sisalda sisendfaili!)
fail <- read.csv("vallakohtud_16_dec_2019-tahemarkidega_lihts.csv", 
                 quote = "", header = TRUE, sep = "|", encoding = "UTF-8")

# Lisa tabelisse info kümnendi kohta
fail$dekaad <- NA
fail$dekaad <- ifelse(fail$year <= 1830, "1821-1830",
                      ifelse(fail$year > 1830 & fail$year <= 1840, "1831-1840",
                             ifelse(fail$year > 1840 & fail$year <= 1850, "1841-1850",
                                    ifelse(fail$year > 1850 & fail$year <= 1860, "1851-1860",
                                           ifelse(fail$year > 1860 & fail$year <= 1870, "1861-1870",
                                                  ifelse(fail$year > 1870 & fail$year <= 1880, "1871-1880",
                                                         ifelse(fail$year > 1880 & fail$year <= 1890, "1881-1890",
                                                                ifelse(fail$year > 1890 & fail$year <= 1900, "1891-1900",
                                                                       ifelse(fail$year > 1900 & fail$year <= 1910, "1901-1910", "1911-1921")))))))))

# Kontrolli
head(fail[,c("year", "dekaad")])


# Tee ka perioodide kolmene jaotus (1821-1865, 1866-1888, 1889-1921)
fail$periood <- NA
fail$periood <- ifelse(fail$year < 1866, "1821-1865",
                       ifelse(fail$year >= 1866 & fail$year < 1889, "1866-1888", "1889-1921"))

##############################################################
# 1. Karpdiagrammid tähemärkide arvu jaotumise uurimiseks ####

# Tee karpdiagramm protokollide sõnade arvust kõikide protokollidega
(karp1 <- ggplot(data = fail) +
     geom_boxplot(aes(x = "", y = number_of_characters),
                  fill = "honeydew3", 
                  size = 0.4, outlier.size = 1, outlier.alpha = 0.5) +
     scale_y_continuous(breaks = c(seq(0, 15000, 1000))) +
     theme_bw() +
     theme(title = element_text(size = 10)) +
     labs(x = "", y = "",
          title = "Tähemärkide arvu jaotumine protokollides"))


# Tee karpdiagramm protokollide sõnade arvust kõikide protokollidega kümnendite kaupa
(karp1_aastad <- ggplot(data = fail) +
        geom_boxplot(aes(x = dekaad, y = number_of_characters),
                     fill = "honeydew3", 
                     size = 0.4, outlier.size = 1, outlier.alpha = 0.5) +
        scale_y_continuous(breaks = c(seq(0, 15000, 1000))) +
        theme_bw() +
        theme(axis.text.x = element_text(angle = 45, hjust = 1),
              title = element_text(size = 10)) +
        labs(x = "", y = "",
             title = "Tähemärkide arvu jaotumine protokollides\nkümnendite kaupa"))


# Tee karpdiagramm protokollide sõnade arvust kõikide protokollidega maakondade kaupa
(karp1_maakonnad <- ggplot(data = fail) +
        geom_boxplot(aes(x = maakond, y = number_of_characters),
                     fill = "honeydew3", 
                     size = 0.4, outlier.size = 1, outlier.alpha = 0.5) +
        scale_y_continuous(breaks = c(seq(0, 15000, 1000))) +
        theme_bw() +
        theme(title = element_text(size = 10)) +
        labs(x = "", y = "",
             title = "Tähemärkide arvu jaotumine protokollides\nmaakondade kaupa"))



# Tee karpdiagramm protokollide sõnade arvust, kui < 500 & > 3001 tähemärgiga protokollid on välja jäetud
(karp2_min500 <- ggplot(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels()) +
        geom_boxplot(aes(x = "", y = number_of_characters),
                     fill = "lightgoldenrod2",
                     size = 0.4, outlier.size = 1, outlier.alpha = 0.5) +
        scale_y_continuous(breaks = c(seq(0, 3100, 500))) +
        theme_bw() +
        theme(title = element_text(size = 10)) +
        labs(x = "", y = "",
             title = "Tähemärkide arvu jaotumine protokollides,\nkus 500 <= tähemärgid <= 3000"))


# Tee karpdiagramm protokollide sõnade arvust kõikide protokollidega kümnendite kaupa
(karp2_min500_aastad <- ggplot(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels()) +
        geom_boxplot(aes(x = dekaad, y = number_of_characters),
                     fill = "lightgoldenrod2",
                     size = 0.4, outlier.size = 1, outlier.alpha = 0.5) +
        scale_y_continuous(breaks = c(seq(0, 3100, 500))) +
        theme_bw() +
        theme(axis.text.x = element_text(angle = 45, hjust = 1),
              title = element_text(size = 10)) +
        labs(x = "", y = "",
             title = "Tähemärkide arvu jaotumine kümnendite kaupa\nprotokollides, kus 500 <= tähemärgid <= 3000"))


# Tee karpdiagramm protokollide sõnade arvust kõikide protokollidega maakondade kaupa
(karp2_min500_maakonnad <- ggplot(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels()) +
        geom_boxplot(aes(x = maakond, y = number_of_characters),
                     fill = "lightgoldenrod2",
                     size = 0.4, outlier.size = 1, outlier.alpha = 0.5) +
        scale_y_continuous(breaks = c(seq(0, 3100, 500))) +
        theme_bw() +
        theme(title = element_text(size = 10)) +
        labs(x = "", y = "",
             title = "Tähemärkide arvu jaotumine maakondade kaupa\nprotokollides, kus 500 <= tähemärgid <= 3000"))


# Ühenda joonised
png(filename = "tahemarkide_arvu_jaotused_karpdiagrammil.png", width = 42, height = 24, units = "cm", res = 300)
grid.arrange(karp1, karp1_aastad, karp1_maakonnad,
             karp2_min500, karp2_min500_aastad, karp2_min500_maakonnad,
             nrow = 2,
             bottom = "Karbi sisse jääb 50% protokollide tähemärkide arv. Paks horisontaalne joon näitab mediaani. \nVurrud katavad 75% protokollide tähemärkide arvu. Punktid näitavad erindeid (antud juhul erandlikult kõrgeid väärtusi).",
             left = "Tähemärkide arv protokollides")
dev.off()


########################################################################################
# 2. Tulpdiagrammid protokollide arvude võrdlemiseks kümnendite ja maakondade kaupa ####

fail$koik <- "kõik protokollid"
fail$yle500 <- "protokollid, kus 500 <= tm <= 3000"

# Tee tuldiagramm protokollide arvust kümnendite kaupa
(tulp_aastad <- ggplot() +
        geom_bar(data = fail %>% group_by(dekaad, koik) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame,
                 aes(x = dekaad, y = n, fill = koik), 
                 stat = "identity", color = "grey75", width = 0.6) +
        geom_bar(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels() %>% group_by(dekaad, yle500) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame,
                 aes(x = dekaad, y = n, fill = yle500),
                 stat = "identity", color = "grey75", width = 0.6, position = position_nudge(x = 0.2)) +
        theme_bw() +
        scale_y_continuous(breaks = seq(0, 10000, 1000)) +
        scale_fill_manual(values = c("honeydew3", "lightgoldenrod2")) +
        theme(axis.text.x = element_text(angle = 45, hjust = 1),
              legend.position = c(0.05, 0.95), 
              legend.justification = c("left", "top"),
              legend.background = element_rect(color = "black"),
              legend.title = element_blank()) +
        labs(x = "Kümnend",
             y = "",
             title = "Protokolle kümnendite kaupa"))


# Tee tulpdiagramm protokollide arvust perioodide kaupa
library(stringr)

(tulp_perioodid <- ggplot() +
        geom_bar(data = fail %>% group_by(periood, koik) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame,
                 aes(x = periood, y = n, fill = str_wrap(koik, 22)),
                 stat = "identity", color = "grey75", width = 0.6) +
        geom_bar(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels() %>% group_by(periood, yle500) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame,
                 aes(x = periood, y = n, fill = str_wrap(yle500, 22)),
                 stat = "identity", color = "grey75", width = 0.6, position = position_nudge(x = 0.2)) +
        theme_bw() +
        scale_y_continuous(breaks = seq(0, 18000, 2000)) +
        scale_fill_manual(values = c("honeydew3", "lightgoldenrod2")) +
        theme(legend.position = c(0.05, 0.95), 
              legend.justification = c("left", "top"),
              legend.background = element_rect(color = "black"),
              legend.title = element_blank()) +
        labs (x = "Periood",
              y = "",
              title = "Protokolle perioodide kaupa"))


# Tee tulpdiagramm protokollide arvust maakondade kaupa
(tulp_maakonnad <- ggplot() +
        geom_bar(data = fail %>% group_by(maakond, koik) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame,
                 aes(x = maakond, y = n, fill = koik),
                 stat = "identity", color = "grey75", width = 0.6) +
        geom_bar(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels() %>% group_by(maakond, yle500) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame,
                 aes(x = maakond, y = n, fill = yle500),
                 stat = "identity", color = "grey75", width = 0.6, position = position_nudge(x = 0.2)) +
        theme_bw() +
        scale_y_continuous(breaks = seq(0, 10000, 1000)) +
        scale_x_discrete(limits = c("Tartu", "Harju", "Järva", "Lääne", "Võru", "Viljandi", "Saare", "Pärnu", "Viru")) +
        scale_fill_manual(values = c("honeydew3", "lightgoldenrod2")) +
        theme(legend.position = c(0.95, 0.95), 
              legend.justification = c("right", "top"),
              legend.background = element_rect(color = "black"),
              legend.title = element_blank()) +
        labs(x = "Maakond",
             y = "",
             title = "Protokolle maakondade kaupa"))


# Tee tulpdiagramm protokollide arvust maakondade ja perioodide kaupa
(tulp1_maakonnad_perioodid <- ggplot(data = fail %>% group_by(maakond, periood) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame) +
        geom_bar(aes(x = maakond, y = n, fill = periood),
                 stat = "identity", color = "grey75") +
        theme_bw() +
        theme(legend.position = c(0.95, 0.95), 
              legend.justification = c("right", "top"),
              legend.background = element_rect(color = "black")) +
        scale_y_continuous(breaks = seq(0, 10000, 1000)) +
        scale_x_discrete(limits = c("Tartu", "Harju", "Järva", "Lääne", "Võru", "Viljandi", "Saare", "Pärnu", "Viru")) +
        scale_fill_manual(values = c("honeydew4", "honeydew3", "honeydew2")) +
        labs(x = "Maakond",
             y = "",
             title = "Protokolle maakondade ja perioodide kaupa (kõik)"))



# Tee tulpdiagramm protokollide arvust maakondade ja perioodide kaupa (< 500 ja > 3000 tm välja jäetud)
(tulp2_min500_maakonnad_perioodid <- ggplot(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels() %>% group_by(maakond, periood) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame) +
        geom_bar(aes(x = maakond, y = n, fill = periood),
                 stat = "identity", color = "grey75") +
        theme_bw() +
        theme(legend.position = c(0.95, 0.95), 
              legend.justification = c("right", "top"),
              legend.background = element_rect(color = "black")) +
        scale_y_continuous(breaks = seq(0, 10000, 1000)) +
        scale_x_discrete(limits = c("Tartu","Järva",  "Harju", "Võru", "Lääne", "Viljandi", "Pärnu", "Saare", "Viru")) +
        scale_fill_manual(values = c("lightgoldenrod3", "lightgoldenrod2", "lightgoldenrod1")) +
        labs(x = "Maakond",
             y = "",
             title = "Protokolle maakondade ja perioodide kaupa (500 <= tm <= 3000)"))


# Ühenda joonised
png(filename = "protokollide_arv_tulpdiagrammil.png", width = 42, height = 28, units = "cm", res = 300)
grid.arrange(arrangeGrob(tulp_aastad, tulp_perioodid, tulp_maakonnad, ncol = 3),
             arrangeGrob(tulp1_maakonnad_perioodid, tulp2_min500_maakonnad_perioodid, ncol = 2),
             nrow = 2,
             left = "Protokollide arv")
dev.off()




#########################################################
# 3. Hajusdiagrammid protokollide tähemärkide arvust ####

# Tee hajusdiagramm protokollide sõnade arvust
(king <- ggplot() + 
     geom_point(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000), 
                aes(x = number_of_characters), 
                stat = "count", alpha = 0.3) +
     geom_point(data = fail %>% filter(number_of_characters < 500 | number_of_characters > 3000),
                aes(x = number_of_characters),
                stat = "count", alpha = 0.3, color = "gray75") +
     geom_vline(aes(xintercept = 500),
                color = "red", linetype = "dashed") +
     geom_vline(aes(xintercept = 3000),
                color = "red", linetype = "dashed") +
     scale_x_continuous(breaks = c(0, 500, 1000, 2000, 3000, 4000, 5000, 10000, 15000)) +
     theme_bw() +
     theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
     labs(title = "Tähemärkide arv protokollides",
          x = "Tähemärkide arv", 
          y = "Protokollide hulk"))


# Tee joonis protokollide sõnade arvust vastavalt protokollide dateeringutele
(aastad <- ggplot() +
        geom_point(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000), 
                   aes(x = number_of_characters, y = year), 
                   alpha = 0.3) +
        geom_point(data = fail %>% filter(number_of_characters < 500 | number_of_characters > 3000),
                   aes(x = number_of_characters, y = year), 
                   alpha = 0.3, color = "gray75") +
        geom_vline(aes(xintercept = 500), 
                   color = "red", linetype = "dashed") +
        geom_vline(aes(xintercept = 3000),
                   color = "red", linetype = "dashed") +
        geom_hline(aes(yintercept = 1866), 
                   color = "steelblue", linetype = "dashed") +
        geom_hline(aes(yintercept = 1889), 
                   color = "steelblue", linetype = "dashed") +
        scale_x_continuous(breaks = c(0, 500, 1000, 2000, 3000, 4000, 5000, 10000, 15000)) +
        scale_y_continuous(breaks = c(1825, 1850, 1866, 1875, 1889, 1900, 1925)) +
        theme_bw() +
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
        labs(title = "Tähemärkide arv protokollides vastavalt protokolli dateeringule",
             x = "Tähemärkide arv", 
             y = "Protokollis märgitud aasta"))


(maakonnad <- ggplot() +
        geom_point(data = fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000), 
                   aes(y = maakond, x = number_of_characters), 
                   position = position_jitter(seed = 5), alpha = 0.3) +
        geom_point(data = fail %>% filter(number_of_characters < 500 | number_of_characters > 3000), 
                   aes(y = maakond, x = number_of_characters), 
                   position = position_jitter(seed = 5), alpha = 0.3, color = "gray75") +
        scale_x_continuous(breaks = c(0, 500, 1000, 2000, 3000, 4000, 5000, 10000, 15000)) +
        geom_vline(aes(xintercept = 500), color = "red", linetype = "dashed") +
        geom_vline(aes(xintercept = 3000), color = "red", linetype = "dashed") +
        scale_y_discrete(limits = rev(c("Tartu", "Harju", "Järva", "Lääne", "Võru", "Viljandi", "Pärnu", "Saare", "Viru"))) +
        theme_bw() +
        theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
        labs(title = "Sisestatud protokollide ja nende tähemärkide arvu jaotumine maakonniti",
             x = "Tähemärkide arv", 
             y = "Maakond"))

t <- textGrob("Iga punkt märgib üht sisestatud protokolli.\nPunased jooned märgivad võimalikku tähemärkide arvu vahemikku, mille puhul\nprotokollid on uuestisisestamiseks piisavalt pikad ja piisavalt lühikesed.\nSinised jooned märgivad 1866. aasta valla- ja 1889. aasta kohtureformi.")

# Ühenda joonised
png(filename = "tahemarkide_arvu_jaotused_hajusdiagrammil.png", width = 42, height = 24, units = "cm", res = 300)
grid.arrange(king, aastad, maakonnad, t, ncol = 2)
dev.off()


#################################################
# 4. Diagrammid propotsionaalse juhuvalimiga ####

# Jäta liiga lühikesed ja liiga pikad protokollid välja.
fail_uus <- fail %>% filter(number_of_characters >= 500 & number_of_characters <= 3000) %>% droplevels()


# Sisestatud protokollide arvu proportsioonid maakonniti on
round(prop.table(table(fail_uus$maakond))*100,1)
round(prop.table(table(fail$maakond))*100,1) # natuke erineb terve andmebaasi proportsioonidest

# Kas proportsioonide vahe on statistiliselt oluline?
vec1 <- table(fail_uus$maakond)
vec2 <- table(fail$maakond)
df <- data.frame(vec1, vec2)[-c(1,3)]
rownames(df) <- names(vec1)
colnames(df) <- c("uus", "vana")
test <- chisq.test(df) # Jah.
test$residuals # Lääne, Saare, Viljandi ja Viru osakaal langeb märkimisväärselt, Tartu ja Võru oma tõuseb.

# Praegu ignoreerime seda.

nrow(fail_uus) # 13974
nrow(fail) # 25353
# 1% kõikidest protokollidest oleks u 250

(tab <- round(prop.table(table(fail_uus$maakond))*250))
#Harju    Järva    Lääne    Pärnu    Saare    Tartu Viljandi     Viru     Võru 
#34       34       23        9        7       98       15        3       26  

sum(tab) # 249. Lisame Võrule veel ühe.
tab["Võru"] <- tab["Võru"]+1

tab_df <- data.frame(maakond = names(tab), protokolle = as.numeric(tab))

fail_uus$sample <- NA
fail_uus$y <- NA

# Vali juhuslikud failid
for(mk in levels(fail_uus$maakond)){ # iga maakonna kohta
    f <- fail_uus[fail_uus$maakond == mk, "id"] # võta välja kõik selle maakonna id-d
    set.seed(1)
    indx <- sample(f, tab[mk], replace = FALSE) # leia proportsionaalne arv juhuslikke id-sid (nt 34 "Harju" id-d)
    fail_uus[fail_uus$maakond == mk & fail_uus$id %in% indx, "sample"] <- "yes" # lisa tabelisse selle maakonna ja id-ga ritta tulpa "sample" väärtus "yes"
    for(ix in indx){ # iga id kohta valimis
        fail_uus[fail_uus$maakond == mk & fail_uus$id == ix, "y"] <- sample(10:table(fail_uus$maakond)[mk]-10, 1, replace = FALSE) # genereeri tabelisse selle maakonna ja id-ga ritta tulpa "y" suvaline y-koordinaadi väärtus, mis ei ületaks protokollide arvu vastavas maakonnas
    }
}


# Tee andmetabel, kus on ainult perioodid ja sagedusandmed
fail_uus %>% group_by(periood) %>% summarise(n = n()) %>% as.data.frame() %>% mutate(klass = "kõik protokollid") -> fail_kokku
fail_uus %>% filter(sample == "yes") %>% group_by(periood) %>% summarise(n = n()) %>% as.data.frame() %>% mutate(klass = "valimi protokollid") -> fail_sample

test_df <- rbind(fail_kokku, fail_sample)


# Tee joonis terve uue andmebaasi ja valimi perioodide proportsioonidest
(props <- ggplotGrob(ggplot(data = test_df) +
                         geom_bar(aes(x = klass, y = n, fill = periood),
                                  stat = "identity", position = "fill") +
                         theme_bw() +
                         scale_fill_manual(values = c("lightgoldenrod3", "lightgoldenrod2", "lightgoldenrod1")) +
                         labs(title = "Protokollide jaotumine periooditi\nterves andmebaasis ja valimis",
                              x = "",
                              y = "Osakaalud",
                              fill = "Periood") +
                         theme(plot.background = element_rect(color = "black", fill = "honeydew2"))))


# Tee joonis, kus maakondade tulpdiagrammile on kuvatud juhuvalimisse sattunud protokollide id-d
(random_sample <- ggplot() +
        geom_bar(data = fail_uus, aes(x = maakond), stat = "count", alpha = 0.7, fill = "lightgoldenrod2", color = "grey75") +
        geom_point(data = fail_uus %>% filter(sample == "yes") %>% droplevels(),
                   aes(x = maakond, y = y, size = number_of_characters), 
                   position = position_jitter(seed = 2), alpha = 0.3, color = "honeydew4") +
        geom_text_repel(data = fail_uus %>% filter(sample == "yes") %>% droplevels(),
                        aes(x = maakond, y = y, label = id), 
                        position = position_jitter(seed = 2), alpha = 0.5, color = "grey35", hjust = 1, size = 2.5) +
        geom_text(data = tab_df, aes(x = maakond, y = c(1996, 2014, 1409, 597, 493, 5595, 933, 280, 1557), label = protokolle)) +
        scale_x_discrete(limits = c("Tartu", "Järva", "Harju", "Võru", "Lääne", "Viljandi", "Pärnu", "Saare", "Viru")) +
        scale_y_continuous(breaks = seq(0, 6000, 1000)) +
        scale_size_continuous(name = "Tähemärkide arv") +
        theme_bw() +
        theme(legend.position = c(0.25, 0.95), 
              legend.justification = c("left", "top"),
              legend.background = element_rect(color = "black")) +
        labs(title = "Sisestatud protokollide hulk maakonniti ning juhuslikult valitud 250 protokolli uuesti sisestamiseks",
             subtitle = "Ringi suurus näitab protokolli tähemärkide arvu ning juuresolev number viitab protokolli id-le koondtabelis.\nNumber tulpade kohal näitab vastava maakonna protokollide arvu valimis.",
             caption = "*Välja on jäetud protokollid, milles on vähem kui 500 ja rohkem kui 3000 tähemärki.",
             x = "Maakond",
             y = "Sisestatud protokolle") +
        annotation_custom(grob = props,
                          xmin = 7, xmax = 9.5, ymin = 3000, ymax = 5500))


# Salvesta joonis
png(filename = "juhuvalim1.png", width = 40, height = 30, units = "cm", res = 300)
random_sample
dev.off()


#############################################################################################
# Joonista kaardid, kus on protokollide arv ja nende tähemärkide arv seotud kihelkonnaga ####

# Loe sisse kihelkondade shapefile (NB! Avalik kaust ei sisalda sisendfaili!)
shp <- st_read("Andmebaasi_ylevaatlikud_joonised_ja_skript/estParishDialects/estParishDialects.shp", options = "ENCODING=latin1")
shp <- st_transform(shp, crs = st_crs(3301))


# Vaja on, et murrete polügonid oleksid arvutatud (ja täidetud) terve murdeala ulatuses, aga protokollide arvu peab arvestama kihelkondade pealt.
# Parandame valla kuuluvust kihelkonda.
unique(fail[fail$kihelkond == "Haljala", "vald"]) # Vihula kindlasti Rannamurde all (jääb Haljala)
unique(fail[fail$kihelkond == "Kuusalu", "vald"]) # Kolga ja Kõnnu jäävad mõlemad keskmurde alla (muuta mõlemas kihelkonna nimed Kuusalu_kesk)
unique(fail[fail$kihelkond == "Jõelähtme", "vald"]) # Jõelähtme vald jääb keskmurde alla (muuta mõlemas kihelkonna nimed Jõelähtme_kesk)
unique(fail[fail$kihelkond == "Iisaku", "vald"]) # Iisaku ja Pootsiku on Iis (jääb samaks), Kauksi on IisIda (muuta mõlemas kihelkonna nimed Iisaku_ida)
unique(fail[fail$kihelkond == "Laiuse", "vald"]) # Kivijärve jääb Laiuse alla (jääb samaks)
unique(fail[fail$kihelkond == "Palamuse", "vald"]) # Luua jääb samaks, Kaarepere on PalKesk (muuta mõlemas kihelkonna nimed Palamuse_kesk)

fail$kihelkond <- as.character(fail$kihelkond)
shp$Name <- as.character(shp$Name)
shp[shp$Parish_id == "HljKesk",]$Name <- "Haljala_kesk" 
fail[fail$kihelkond == "Kuusalu",]$kihelkond <- "Kuusalu_kesk"
shp[shp$Parish_id == "KuuKesk",]$Name <- "Kuusalu_kesk"
fail[fail$kihelkond == "Jõelähtme",]$kihelkond <- "Jõelähtme_kesk"
shp[shp$Parish_id == "JõeKesk",]$Name <- "Jõelähtme_kesk"
fail[fail$kihelkond == "Iisaku" & fail$vald == "Kauksi",]$kihelkond <- "Iisaku_ida"
shp[shp$Parish_id == "IisIda",]$Name <- "Iisaku_ida"
shp[shp$Parish_id == "IisKesk",]$Name <- "Iisaku_kesk"
shp[shp$Parish_id == "LaiKesk",]$Name <- "Laiuse_kesk"
fail[fail$kihelkond == "Palamuse" & fail$vald == "Kaarepere",]$kihelkond <- "Palamuse_kesk"
shp[shp$Parish_id == "PalKesk",]$Name <- "Palamuse_kesk"
fail$kihelkond <- as.factor(fail$kihelkond)
shp$Name <- as.factor(shp$Name)

# Kontrolli, millised kihelkonnanimed kattuvad ja millised mitte
levels(fail$kihelkond)[levels(fail$kihelkond) %in% levels(shp$Name)]
levels(fail$kihelkond)[!levels(fail$kihelkond) %in% levels(shp$Name)] # Pärnu-Elisabethi kihelkonda pole shp-failis,
levels(fail$kihelkond)[levels(fail$kihelkond) == "Pärnu-Elisabethi"] <- "Pärnu" # muuda see Pärnuks.

# Tee väiksem shapefile
shp_khk <- shp %>% select(Parish_id, Name, Dialect_et)

# Failide arv kihelkondades
prot_khk <- fail %>% group_by(kihelkond) %>% summarise(n = n()) %>% droplevels() %>% as.data.frame()
prot_khk_shp <- left_join(shp_khk, prot_khk, by = c("Name" = "kihelkond"))

# Murrete piirid
dia_shp <- shp %>% group_by(Dialect_et) %>% summarise()

# Loe sisse Maa-ameti maakonnapiiride kaart (NB! Avalik kaust ei sisalda sisendfaili!)
mk <- st_read("maakond_20200101/maakond_20200101.shp")

# Kihelkondade kaart protokollide arvuga (koos murde- ja maakonnapiiridega)
(prot_khk <- ggplot() +
        geom_sf(data = prot_khk_shp, aes(fill = n)) +
        geom_sf(data = dia_shp, fill = "transparent", size = 1.1, color = "grey35") +
        geom_sf_text(data = prot_khk_shp, aes(label = Parish_id), color = "grey35") +
        geom_sf(data = mk, fill = "transparent", color = "tomato", linetype = "dashed", size = 0.5) +
        scale_fill_gradient2(low = "aquamarine1", high = "aquamarine4", na.value = "grey75", trans = "sqrt", breaks = c(100, 500, 1000, 2000, 3000)) +
        theme_bw() +
        theme(legend.position = c(0.1,0.9),
              legend.background = element_rect(color = "black"),
              legend.text = element_text(size = 12),
              legend.title = element_text(size = 14)) +
        labs(x = "Longitude", y = "Latitude", fill = "Protokolle",
             caption = "Tumedamad jooned märgivad murdepiire, katkendlik punane joon praguseid maakonnapiire."))

png(filename = "protokolle_kihelkondades_kaart.png", height = 24, width = 36, units = "cm", res = 300)
prot_khk
dev.off()

